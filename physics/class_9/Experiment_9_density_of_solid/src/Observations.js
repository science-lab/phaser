var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var count;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var sceneNo;
  var r1;
  var r2;
  var r3;
  var r4;
  var r5;
  var r6;
  var r7;
  var r8;
  var r9;
  var massOfObject;
  var densityOfObject;
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

sceneNo=1;
count=0;
pageCount=1;
massOfObject=0;
densityOfObject=0;
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
 muted = false;
 voice=this.game.add.audio("A_observation",1);

 
 voice.play();
 //setTimeout(this.nextSound,2000);
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  // R1voltage_val1=0;
  // R1voltage_val2=0;
  // R1voltage_val3=0;
  // R2voltage_val1=0;
  // R2voltage_val2=0;
  // R2voltage_val3=0;
  // R3voltage_val1=0;
  // R3voltage_val2=0;
  // R3voltage_val3=0;
  // R1current_val1=0;
  // R1current_val2=0;
  // R1current_val3=0;
  // R2current_val1=0;
  // R2current_val2=0;
  // R2current_val3=0;
  // R3current_val1=0;
  // R3current_val2=0;
  // R3current_val3=0;
  


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

 

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  //////////////////////////////////Graph///////////////////////////
//  graph=this.game.add.sprite(1100,200,'obervation_graph');
//  graph.scale.setTo(.15,.15);

  /////////////////////////////////////////////////////////////////

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  headfontStyle2={ font: "28px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  fontStyle2={ font: "32px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  
 //dialogbox1=this.game.add.sprite(50,30,'dialogue_box3');
 // dialogbox1.scale.setTo(.8,.7);

  dialogbox2=this.game.add.sprite(50,50,'dialogbox2');
  dialogbox2.scale.setTo(.65,.6);
  //dialogbox2.scale.setTo(.8,1);

  base= this.game.add.sprite(49,276,'table01');
  base.scale.setTo(1,.7);
  base1= this.game.add.sprite(49,776,'table02');
  base1.scale.setTo(1,.47);

  base2= this.game.add.sprite(49,100,'table03');
  base2.scale.setTo(1,.7);
 
  base3= this.game.add.sprite(49,655,'table02');
  base3.scale.setTo(1,.7);


  base4= this.game.add.sprite(47,140,'table02');
  base4.scale.setTo(1,1.8);
  base4.visible=false;

  base4_head=this.game.add.sprite(50,80,'obs_head');
  base4_head.scale.setTo(1,.8);
  base4_head.visible=false;
  //base.scale.setTo(1,1);
  //switch(exp_Name){
   // case "Iron with copper sulphate":
   
    
  
    /*inference_text_3=this.game.add.text(950,700,inference_3,fontStyle);
    inference_text_4=this.game.add.text(950,750,inference_4,fontStyle_1);*/
    

   // break;
 // }
next_btn = this.game.add.sprite(1590,860,'components','next_disabled.png');
next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1590,860,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
next_btn.inputEnabled = true;
 next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 prev_btn = this.game.add.sprite(230,860,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(230,860,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
 prev_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrev, this);
  prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
//  result1=2.1;
//   result2=2.1;
//   result3=2.1;
//   result4=80;
//   result5=81;
//   result6=82;

  this.middleresult1= result4-50;
  this.middleresult2= result5-50;
  this.middleresult3= result6-50;
  var sd=(result1+result2+result3)/3;
 
this.mainresult11=Number(sd).toFixed(1);
this.mainresult1=this.mainresult11*1000;
this.mainresult2=Number((this.middleresult1+this.middleresult2+this.middleresult3)/3).toFixed(1);
mainresult=Number(this.mainresult1/this.mainresult2).toFixed(1);
massOfObject=Number(Number(this.mainresult11/9.8).toFixed(4)*1000).toFixed(0)//Number(this.mainresult11/9.8).toFixed(4);
densityOfObject=Number(massOfObject/this.mainresult2).toFixed(2);
previouspress=false;

  this.addTheory();

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        addTheory:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
                     voice.destroy();
              voice=this.game.add.audio("A_observation1",1);
              voice.play();
                if(previouspress==true)
                {
                  observatio_top_text_1.text="";
                  observatio_top_text_2.text="";
                  observatio_top_text_3.text="";
                  sno_text_1.text="";
                  head_text_1.text="";
                  head_text_2.text="";
                  head_text_3.text="";
                  head_text_4.text="";


                
              text_2.text="";
              text_3.text="";
              text_4.text="";
              text_5.text="";
              text_6.text="";
              text_7.text="";
              text_8.text="";
              text_9.text="";
              text_10.text="";
              text_13.text="";
              text_14.text="";
              text_15.text="";
              text_11.text="";
             // text_12.text="";

                  previouspress=false;

                }

                    dialogbox2.visible=false;
                    base.visible=false;
                    base1.visible=false;
                    base2.visible=true;
                    base3.visible=true;


                    sno_text_1=this.game.add.text(125,200,"S.No",fontStyle2);
                    head_text_1=this.game.add.text(300,180,"Initial reading (when object\n is not immersed) (V\u1D62 cm\u00B3)",fontStyle2);
                    head_text_2=this.game.add.text(750,180,"Final reading(when object\n is immersed) (V\u1D73 cm\u00B3)",fontStyle2);
                    head_text_3=this.game.add.text(1300,180,"Volume of the object\n   (V\u1D73 cm\u00B3 - V\u1D62 cm\u00B3)",fontStyle2);

                    sno_2="1";
                    text_2=this.game.add.text(140,320,sno_2,fontStyle2);
                    sno_3="2";
                    text_3=this.game.add.text(140,440,sno_3,fontStyle2);
                    sno_4="3";
                    text_4=this.game.add.text(140,560,sno_4,fontStyle2);
                    sno_5="50";
                    // text_5=this.game.add.text(450,320,sno_5,fontStyle2);
                    // text_6=this.game.add.text(450,440,sno_5,fontStyle2);
                    // text_7=this.game.add.text(450,560,sno_5,fontStyle2);

                    text_5=this.game.add.text(450,320,"V\u2081\u1D62=50",fontStyle2);
                    text_6=this.game.add.text(450,440,"V\u2082\u1D62=50",fontStyle2);
                    text_7=this.game.add.text(450,560,"V\u2083\u1D62=50",fontStyle2);


                    sno_8="V\u2081\u1D73 = "+result4
                    text_8=this.game.add.text(850,320,sno_8,fontStyle2);
                    sno_9="V\u2082\u1D73 = "+result5
                    text_9=this.game.add.text(850,440,sno_9,fontStyle2);
                    sno_10="V\u2083\u1D73 = "+result6
                    text_10=this.game.add.text(850,560,sno_10,fontStyle2);


                       sno_81="\u0394V\u2081 = "+this.middleresult1
                       text_81=this.game.add.text(1300,320,sno_81,fontStyle2);
                       sno_91="\u0394V\u2082 = "+this.middleresult2
                       text_91=this.game.add.text(1300,440,sno_91,fontStyle2);
                       sno_101="\u0394V\u2083 = "+this.middleresult3
                       text_101=this.game.add.text(1300,560,sno_101,fontStyle2);

                    sno_11="Average Volume of the object V= (\u0394V\u2081+\u0394V\u2082+\u0394V\u2083) / 3 cm\u00B3= "+"( "+this.middleresult1+" + "+this.middleresult2+" + "+this.middleresult3+" )"+" / 3 = "+this.mainresult2+" cm\u00B3";//+this.result1//.middleresult3
                    text_11=this.game.add.text(300,700,sno_11,fontStyle2);
                    // sno_12="Density of the Object =  Mass of the Object / Volume of the Object"
                    // text_12=this.game.add.text(300,780,sno_12,fontStyle2);
                    // sno_13="= Wg/ V cm\u00B3 = "+this.mainresult1+" / "+this.middleresult3+" = "+mainresult+" g cm\u207B\u00B3"
                    // text_13=this.game.add.text(608,850,sno_13,fontStyle2);



                 
                    break;





              // voice.destroy();
              // voice=this.game.add.audio("A_observation",1);
              // voice.play();
              // dialogbox2.visible=true;
              // base.visible=true;
              // base1.visible=true;
              // base2.visible=false;
              // base3.visible=false;
              // if(previouspress){
              // observatio_top_text_1.text="";
              // observatio_top_text_2.text="";
              // observatio_top_text_3.text="";
             
              // sno_text_1.text="";
              // head_text_1.text="";
              // head_text_2.text="";
              // head_text_3.text="";
              // text_2.text="";
              // text_3.text="";
              // text_4.text="";
              // text_5.text="";
              // text_6.text="";
              // text_7.text="";
              // text_8.text="";
              // text_9.text="";
              // text_10.text="";
              // text_11.text="";
              // text_12.text="";
              // text_81.text="";
              // text_91.text="";
              // text_101.text="";
              // text_13.text="";
              // previouspress=false;
              // }
              


              // observatio_top_text_1=this.game.add.text(80,120,"Range of the spring balance = 0 - 4 kgf",fontStyle2);
              // observatio_top_text_2=this.game.add.text(80,170,"Least count of the spring balance = 0.1 kgf",fontStyle2);
              // observatio_top_text_3=this.game.add.text(80,220,"Zero correction for the spring balance = 0",fontStyle2);
              // sno_text_1=this.game.add.text(125,330,"S.No",fontStyle2);
              // head_text_1=this.game.add.text(800,295,"Readings of the spring balance",fontStyle2);
              // head_text_2=this.game.add.text(550,360,"Observed(kgf)",fontStyle2);
              // head_text_3=this.game.add.text(1300,360,"Corrected(kgf)",fontStyle2);
   
              // sno_2="1";
              // text_2=this.game.add.text(140,480,sno_2,fontStyle2);
              // sno_3="2";
              // text_3=this.game.add.text(140,580,sno_3,fontStyle2);
              // sno_4="3";
              // text_4=this.game.add.text(140,680,sno_4,fontStyle2);

              // sno_5="0";
              // text_5=this.game.add.text(600,480,sno_5,fontStyle2);
              // text_6=this.game.add.text(600,580,sno_5,fontStyle2)
              // text_7=this.game.add.text(600,680,sno_5,fontStyle2)

              // sno_8="W\u2081 = "+result1
              // text_8=this.game.add.text(1200,480,sno_8,fontStyle2);
              // sno_9="W\u2082 = "+result2
              // text_9=this.game.add.text(1200,580,sno_9,fontStyle2);
              // sno_10="W\u2083 = "+result3
              // text_10=this.game.add.text(1200,680,sno_10,fontStyle2);

              // sno_11="Average Weight of the object W= (W\u2081+W\u2082+W\u2083) / 3 = "+"( "+result1+" + "+result2+" + "+result3+" )"+" / 3 = "+this.mainresult11+" kgf";//+this.result1
              // text_11=this.game.add.text(300,820,sno_11,fontStyle2);
              // sno_12="Mass of the Object (in grams) = "+this.mainresult11+" x 1000 = "+(this.mainresult1)+" Wg"
              // text_12=this.game.add.text(300,880,sno_12,fontStyle2);
            //  break;
              case 2:
                  voice.destroy();
                  voice=this.game.add.audio("A_observation2",1);
                  voice.play();

              if(previouspress==true)
              {

                
                    text_16.text="";

                   
                    text_17.text="";

                   
                    text_18.text="";

                   
                    text_19.text="";

                   
                    text_20.text="";
                   
                    text_21.text="";
      
                   
                    text_22.text="";
      
              }

                  dialogbox2.visible=true;
                  base.visible=true;
                  base1.visible=true;
                  base2.visible=false;
                  base3.visible=false;
                  base4_head.visible=false;
                  base4.visible=false;

                  sno_text_1.text="";
                  head_text_1.text="";
                  head_text_2.text="";
                  head_text_3.text="";

                 
                  text_2.text="";
                  text_3.text="";
                  text_4.text="";

                  text_5.text="";
                  text_6.text="";
                  text_7.text="";

                 
                  text_8.text="";
                  text_9.text="";
                  text_10.text="";


                 
                  text_81.text="";
                  text_91.text="";
                  text_101.text="";
                  text_11.text="";


              observatio_top_text_1=this.game.add.text(80,120,"Range of the spring balance = 0 - 4 kgf",fontStyle2);
              observatio_top_text_2=this.game.add.text(80,170,"Least count of the spring balance = 0.1 kgf",fontStyle2);
              observatio_top_text_3=this.game.add.text(80,220,"Zero correction for the spring balance = 0",fontStyle2);
              sno_text_1=this.game.add.text(125,330,"S.No",fontStyle2);
              head_text_1=this.game.add.text(800,295,"Readings of the spring balance",fontStyle2);
              head_text_2=this.game.add.text(430,360,"Observed(kgf)",fontStyle2);
              head_text_3=this.game.add.text(900,360,"Corrected(kgf)",fontStyle2);
              head_text_4=this.game.add.text(1380,360,"Final weight(kgf)",fontStyle2);


              sno_2="1";
              text_2=this.game.add.text(140,480,sno_2,fontStyle2);
              sno_3="2";
              text_3=this.game.add.text(140,580,sno_3,fontStyle2);
              sno_4="3";
              text_4=this.game.add.text(140,680,sno_4,fontStyle2);


              sno_5="0";
              text_5=this.game.add.text(500,480,sno_5,fontStyle2);
              text_6=this.game.add.text(500,580,sno_5,fontStyle2);
              text_7=this.game.add.text(500,680,sno_5,fontStyle2);


              
              sno_8="W\u2081 = "+result1
              text_8=this.game.add.text(930,480,sno_8,fontStyle2);
              sno_9="W\u2082 = "+result2
              text_9=this.game.add.text(930,580,sno_9,fontStyle2);
              sno_10="W\u2083 = "+result3
              text_10=this.game.add.text(930,680,sno_10,fontStyle2);


              sno_13="\u0394W\u2081 = "+result1
              text_13=this.game.add.text(1430,480,sno_13,fontStyle2);
              sno_14="\u0394W\u2082 = "+result2
              text_14=this.game.add.text(1430,580,sno_14,fontStyle2);
              sno_15="\u0394W\u2083 = "+result3
              text_15=this.game.add.text(1430,680,sno_15,fontStyle2);



               sno_11="Average Weight of the object W= (\u0394W\u2081+\u0394W\u2082+\u0394W\u2083) / 3 = "+"( "+result1+" + "+result2+" + "+result3+" )"+" / 3 = "+this.mainresult11+" kgf";//+this.result1
              text_11=this.game.add.text(300,820,sno_11,fontStyle2);
             // sno_12="Mass of the Object (in grams) = "+this.mainresult11+" x 1000 = "+(this.mainresult1)+" Wg"
            //  sno_12="Mass of the object M = W / g, find m ?";
            //   text_12=this.game.add.text(300,880,sno_12,fontStyle2);
         
            //     voice.destroy();
            //     voice=this.game.add.audio("A_observation1",1);
            //     voice.play();
            //     play.visible=true;
            //     observatio_top_text_1.text="";
            //     observatio_top_text_2.text="";
            //     observatio_top_text_3.text="";
            //     dialogbox2.visible=false;
            //     base.visible=false;
            //     base1.visible=false;
            //     base2.visible=true;
            //     base3.visible=true;
            //     sno_text_1.text=""
            //     head_text_1.text=""
            //     head_text_2.text=""
            //     head_text_3.text=""
            //     text_2.text="";
            //     text_3.text="";
            //     text_4.text="";
            //     text_5.text="";
            //     text_6.text="";
            //     text_7.text="";
            //     text_8.text="";
            //     text_9.text="";
            //     text_10.text="";
            //     text_11.text="";
            //     text_12.text="";
            //     sno_text_1=this.game.add.text(125,200,"S.No",fontStyle2);
            //   head_text_1=this.game.add.text(300,180,"Initial reading (when object\n is not immersed) (V\u1D62 cm\u00B3)",fontStyle2);
            //  head_text_2=this.game.add.text(750,180,"Final reading(when object\n is immersed) (V\u1D73 cm\u00B3)",fontStyle2);
            //  head_text_3=this.game.add.text(1300,180,"Volume of the object\n   (V\u1D73 cm\u00B3 - V\u1D62 cm\u00B3)",fontStyle2);

             
            //   sno_2="1";
            //   text_2=this.game.add.text(140,320,sno_2,fontStyle2);
            //   sno_3="2";
            //   text_3=this.game.add.text(140,440,sno_3,fontStyle2);
            //   sno_4="3";
            //   text_4=this.game.add.text(140,560,sno_4,fontStyle2);
              // sno_5="50";
              // text_5=this.game.add.text(450,320,sno_5,fontStyle2);
              // text_6=this.game.add.text(450,440,sno_5,fontStyle2);
              // text_7=this.game.add.text(450,560,sno_5,fontStyle2);
              // text_5=this.game.add.text(450,320,"V\u2081\u1D62=50",fontStyle2);
              // text_6=this.game.add.text(450,440,"V\u2082\u1D62=50",fontStyle2);
              // text_7=this.game.add.text(450,560,"V\u2083\u1D62=50",fontStyle2);

              // sno_8=""+result4
              // text_8=this.game.add.text(850,320,sno_8,fontStyle2);
              // sno_9=""+result5
              // text_9=this.game.add.text(850,440,sno_9,fontStyle2);
              // sno_10=""+result6
              // text_10=this.game.add.text(850,560,sno_10,fontStyle2);

              // sno_8="V\u2081\u1D73 = "+result4
              // text_8=this.game.add.text(850,320,sno_8,fontStyle2);
              // sno_9="V\u2082\u1D73 = "+result5
              // text_9=this.game.add.text(850,440,sno_9,fontStyle2);
              // sno_10="V\u2083\u1D73 = "+result6
              // text_10=this.game.add.text(850,560,sno_10,fontStyle2);

              // sno_81="\u0394V\u2081 = "+this.middleresult1
              // text_81=this.game.add.text(1300,320,sno_81,fontStyle2);
              // sno_91="\u0394V\u2082 = "+this.middleresult2
              // text_91=this.game.add.text(1300,440,sno_91,fontStyle2);
              // sno_101="\u0394V\u2083 = "+this.middleresult3
              // text_101=this.game.add.text(1300,560,sno_101,fontStyle2);

              // sno_11="Average Volume of the object V= (\u0394V\u2081+\u0394V\u2082+\u0394V\u2083) / 3 cm\u00B3= "+"( "+this.middleresult1+" + "+this.middleresult2+" + "+this.middleresult3+" )"+" / 3 = "+this.middleresult3+" cm\u00B3";//+this.result1
              // text_11=this.game.add.text(300,700,sno_11,fontStyle2);
              // sno_12="Density of the Object =  Mass of the Object / Volume of the Object"
              // text_12=this.game.add.text(300,780,sno_12,fontStyle2);
              // sno_13="= Wg/ V cm\u00B3 = "+this.mainresult1+" / "+this.middleresult3+" = "+mainresult+" g cm\u207B\u00B3"
              // text_13=this.game.add.text(608,850,sno_13,fontStyle2);

                break;

                case 3:

                    voice.destroy();
                    voice=this.game.add.audio("A_observation3",1);
                    voice.play();
                    dialogbox2.visible=false;
                    base.visible=false;
                    base1.visible=false;
                    base2.visible=false;
                    base3.visible=false;
                    base4_head.visible=true;
                    base4.visible=true;

                    observatio_top_text_1.text="";
                    observatio_top_text_2.text="";
                    observatio_top_text_3.text="";
                    sno_text_1.text="";
                    head_text_1.text="";
                    head_text_2.text="";
                    head_text_3.text="";
                    head_text_4.text="";
         
                    text_2.text="";
                    text_3.text="";
                    text_4.text="";
      
                    text_5.text="";
                    text_6.text="";
                    text_7.text="";
                       
                    text_8.text="";                  
                    text_9.text="";                 
                    text_10.text="";
                        
                    text_13.text="";
                    text_14.text="";
                    text_15.text="";
                          
                    text_11.text="";                  
                   // text_12.text="";

                    sno_16="Volume of the object, V       = "+this.mainresult2+" cm\u00B3";
                    text_16=this.game.add.text(120,220,sno_16,fontStyle2);

                    sno_17="Weight of the object. W      = "+this.mainresult11+" kgf";// = "+this.mainresult1+" gf";
                    text_17=this.game.add.text(120,280,sno_17,fontStyle2);

                    sno_18="Mass of the object, m          = W / g,";
                    text_18=this.game.add.text(120,340,sno_18,fontStyle2);

                    sno_19="                                             = "+this.mainresult11+" / 9.8 = "+Number(this.mainresult11/9.8).toFixed(4)+" kg = "+massOfObject+" g";
                    text_19=this.game.add.text(120,400,sno_19,fontStyle2);

                    sno_20="Density of the object, \u03C1        = mass of the object (m) / volume of the object (V) ";
                    text_20=this.game.add.text(120,460,sno_20,fontStyle2);

                    sno_21="                                             = "+massOfObject+" / "+this.mainresult2;
                    text_21=this.game.add.text(120,520,sno_21,fontStyle2);
      
                    sno_22="                                             = "+densityOfObject+" g cm\u207B\u00B3 ";
                    text_22=this.game.add.text(120,580,sno_22,fontStyle2);
      
      
          }
        },

        nextSound:function()
        {

           // voice.destroy();
            //voice=this.game.add.audio("A_observation2",1);
           // voice2.play();

        },
    toNext:function(){
      if(pageCount<=2){
        prev_btn.visible=true;

        pageCount++;
          this.addTheory();
          if(pageCount>=3){
            play.visible=true;
            next_btn.visible=false;  
          }
        }
       
        
        
        },
    toPrev:function(){
      if(pageCount>1){
        next_btn.visible=true;
        previouspress=true;
        pageCount--;
          this.addTheory();
          if(pageCount<=1){
            prev_btn.visible=false;  
          }
        }
        },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice1.destroy();
     
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  update: function()
  {
    // if(sceneNo==1){
    //   count++;
    //   //console.log(count);
    //   if(count==1050){
    //     voice.destroy();
    //     voice=this.game.add.audio("A_observation2",1);
    //     voice.play();
    //   }
    // }
  },
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
      //  voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  