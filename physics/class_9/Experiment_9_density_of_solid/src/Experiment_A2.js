var experiment_a2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;
var fontStyle;
//ip address
var ip;


}

experiment_a2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  this.dragStop=false;
  this.backToWaterPos=false;
this.tookVal=0;
 muted = false;
 this.dialogx=25;
 this.dialogy=60;
 voice=this.game.add.audio("obj",1);

 this.waterraised=false;

 
 bg= this.game.add.sprite(0, 0,'bg');


 

  

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  
  resetButton.events.onInputDown.add(this.resetTheGame, this);

 
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;
  //710,715,720,723,726,726

  this.arraVals=[712,716,720,723,726,731];
  this.arraPointVals=[85,84,83,82,81,80];
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "25px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};


  this.arrangeScene();
  this.intDailog();
  
      



////////////////////////////////////////////////////////////////////

 },
 intDailog:function(){

  dialog_box=this.game.add.sprite(10,20, 'dialogue_box1');
  dialog_box.scale.setTo(.8,.7);
  dialog_text="Lets perform the experiment.";
  dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
  voice=this.game.add.audio("lets_perform_Exp",1);
  this.game.time.events.add(Phaser.Timer.SECOND*2,this.LoadExp,this);
  
 },
 LoadExp:function()
 {  
     voice.play();
     this.game.time.events.add(Phaser.Timer.SECOND*3,this.StartExperiment,this);
 },
 StartExperiment:function(){
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  this.arrow1=this.game.add.sprite(0,0, 'arrow');
  this.arrow1.anchor.set(.5);
  this.arrow1.animations.add("frame1",[0])
  this.arrow1.animations.add("frameall")
  this.arrow1.visible=false;
  this.arrowShow=0;
  this.ProcedureText(1);
  var tweens2=this.game.add.tween(this.dragwater).to({x:this.dragwater.xposed}, 500, Phaser.Easing.Linear.Out, true);
  tweens2.onComplete.add(function () {
    this.arrowShow=1;
    this.ArowShowing();
   this.dragwater.inputEnabled = true;
   this.dragwater.input.useHandCursor = true;
    this.dragwater.input.enableDrag(true);
  }.bind(this))
  
 },
 ArowShowing:function(){
  this.arrow1.visible=true;
  this.arrow1.animations.play("frameall",10,true);
  switch(this.arrowShow){
    case 1:
      this.arrow1.x= this.dragwater.x+100;
      this.arrow1.y= this.dragwater.y-130;
      break;
      case 2:
        case 4:  
        this.arrow1.x= this.tube.x;
        this.arrow1.y= this.tube.y-250;
        break;
    
        case 3:
          this.arrow1.x= this.dragweight.x;
          this.arrow1.y= this.dragweight.y-50;
          break;
  }
  },
 ProcedureText:function(vals){
  switch(vals){
    case 1:
  
    voice.stop();

    voice=this.game.add.audio("A_exp_procedure8",1);
    voice.play();
    dialog_text="";
    dialog.destroy();
    dialog_text="Take the beaker and pour 50 ml of water to the measuring cylinder.";
    dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
    break;
    case 2:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure9",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Hang the object fully immersed in water.";
      dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
  break;
  case 3:
    voice.stop();
    voice=this.game.add.audio("A_exp_procedure10",1);
    voice.play();
    dialog_text="";
    dialog.destroy();
    dialog_text="Observe that the object displaces water hence water level rises.";
    dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
    break;
  case 4:
    voice.stop();
    voice=this.game.add.audio("A_exp_procedure11",1);
    voice.play();
    dialog_text="";
    dialog.destroy();
    dialog_text="Observe the reading of lower meniscus of water.";
    dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
    break;
    case 5:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure12",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Now take 2 more readings.";
      dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
      break;
      case 6:
        voice.stop();
        voice=this.game.add.audio("A_exp_procedure13",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Take one more reading by repeating the same process.";
        dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
        break;
        case 7:

          this.arrow1.visible=false;
          this.arrow1.animations.add("frame1",[0]);
          this.arrow1.animations.play("frame1",10,false);
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure14",1);
          voice.play();
          dialog_text="";
          dialog.destroy();
          dialog_text="Click on the next button to see the observation table.";
          dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
          play.visible=true;
          break;
  }

},
 arrangeScene:function(){
  /*this.ring1=this.game.add.sprite(900,500, 'ring1');
  this.ring1.anchor.set(.5);*/

  this.stand=this.game.add.sprite(850,650, 'stand');
  this.stand.anchor.set(.5);
  this.stand.scale.setTo(.7,.7);

  this.tubeback=this.game.add.sprite(970,672, 'measuring_tubeback');
  this.tubeback.anchor.set(.5);
  this.watertubes=this.game.add.sprite(970,670, 'watertube');
  this.watertubes.anchor.set(.5);
  this.wieght=this.game.add.sprite(970,50, 'wieghttie');
  this.wieght.anchor.set(.5);
  this.wieght.ypos=500;
  this.wieght.originaly= this.wieght.y;
  this.wieghtmask = this.game.add.graphics(970, 900);

  //	Shapes drawn to the Graphics object must be filled.
  this.wieghtmask.beginFill(0xffffff);
  this.wieghtmask.visible=false;
  //this.wieght.mask = this.wieghtmask;
  //	Here we'll draw a circle
  this.wieghtmask.drawRect(-50,-523, 100, 900);
  this.waterlevel1=this.game.add.sprite(970,913, 'water_bottom');
  this.waterlevel1.anchor.set(.5);
  this.waterlevel2=this.game.add.sprite(970,899, 'water_middle');
  this.waterlevel2.anchor.setTo(.5,1);
  this.waterlevel2.scale.setTo(1,2.5);
  this.waterlevel3=this.game.add.sprite(970,this.waterlevel2.y-180, 'water_top');
  this.waterlevel3.anchor.set(.5);
  this.waterlevel3.originalY= this.waterlevel3.y;
  this.tube=this.game.add.sprite(970,670, 'measuring_tube');
  this.tube.anchor.set(.5);

  this.mask = this.game.add.graphics(970, 850);

    //	Shapes drawn to the Graphics object must be filled.
    this.mask.beginFill(0xffffff);

    //	Here we'll draw a circle
  
    this.mask.drawRect(-50,-116, 100, 900);
   console.log(this.mask.y);
   this.mask.y=851;
   this.mask.originalpos=this.mask.y;
  
    //	And apply it to the Sprite
 this.waterlevel2.mask = this.mask;
  this.waterlevel1.visible=false;
  this.waterlevel2.visible=false;
  this.waterlevel3.visible=false;
  this.tube.visible=false;
 this.wieght.visible=false;
 // this.mask.visible=false;
 
  this.dragwater=this.game.add.sprite(-300,750, 'waterpour');
  this.dragwater.anchor.set(.5);
  this.dragwater.events.onDragStart.add(function() {this.onDragStart(this.dragwater)}, this);
  this.dragwater.events.onDragStop.add(function() {this.onDragStop(this.dragwater)}, this);
  this.dragwater.xposed=150
  this.dragwater.xp= this.dragwater.xposed;
  this.dragwater.yp= this.dragwater.y;
  this.collider1 = this.game.add.sprite(90,0,'collider');
  this.collider1.anchor.set(.5);
  this.collider1.scale.setTo(4,3);
  this.game.physics.arcade.enable(this.collider1);
 this.collider1.alpha=.0001
  this.collider1.enableBody=true;
  this.dragwater.addChild(this.collider1)
  this.dragwater.colliders=this.collider1;
  this.game.physics.arcade.enable(this.dragwater);
  this.dragwater.enableBody =true;
  this.collider = this.game.add.sprite(840,350,'collider');
  this.collider.scale.setTo(4,2);
  this.game.physics.arcade.enable(this.collider);
  this.collider.alpha=.0001
  this.collider.enableBody=true;


  this.dragweight=this.game.add.sprite(-800,900, 'wieghttie');
  this.dragweight.anchor.setTo(.5,.5);
  this.dragweight.events.onDragStart.add(function() {this.onDragStart(this.dragweight)}, this);
  this.dragweight.events.onDragStop.add(function() {this.onDragStop(this.dragweight)}, this);
  this.dragweight.xposed=290
  this.dragweight.angle=90
  this.dragweight.xp= this.dragweight.xposed;
  this.dragweight.yp= this.dragweight.y;
 
  this.game.physics.arcade.enable(this.dragweight);
  this.dragweight.enableBody =true;
  this.collider1 = this.game.add.sprite(0,80,'collider');
  this.collider1.anchor.set(.5);
  this.collider1.scale.setTo(3,3);
  this.game.physics.arcade.enable(this.collider1);
  this.collider1.alpha=.0001
  this.collider1.enableBody=true;
  this.dragweight.addChild(this.collider1);
  this.dragweight.colliders=this.collider1

  this.valuetable=this.game.add.sprite(355,500, 'valtable');
  this.valuetable.anchor.set(.5);
  this.valuetable.scale.setTo(.6,.8);
   this.obsline1=this.game.add.sprite(5,-60,'obsline');
  this.obsline1.anchor.set(.5);
  this.obsline1.scale.setTo(1,1.24);
  this.obsline1.angle=90
  this.valuetable.addChild(this.obsline1);


//  this.wttext=this.game.add.text(100,100,"Weight of the Object",fontStyle);
this.wttext=this.game.add.text(-150,-122,"Measurement",fontStyle);
this.valuetable.addChild(this.wttext);

this.wttext1=this.game.add.text(-180,-45,"V\u2081",fontStyle);
this.valuetable.addChild(this.wttext1);
this.wttext2=this.game.add.text(-180,11,"V\u2082",fontStyle);
this.valuetable.addChild(this.wttext2);
this.wttext3=this.game.add.text(-180,71,"V\u2083",fontStyle);
this.valuetable.addChild(this.wttext3);

this.wttext1s=this.game.add.text(-80,-45,"=",fontStyle);
this.valuetable.addChild(this.wttext1s);
this.wttext2s=this.game.add.text(-80,11,"=",fontStyle);
this.valuetable.addChild(this.wttext2s);
this.wttext3s=this.game.add.text(-80,71,"=",fontStyle);
this.valuetable.addChild(this.wttext3s);

result4=0
result5=0
result6=0
this.wttext11=this.game.add.text(2,-45,result4,fontStyle);
this.wttext21=this.game.add.text(2,11,result5,fontStyle);
this.wttext31=this.game.add.text(2,71,result6,fontStyle);
this.valuetable.addChild(this.wttext11);
this.valuetable.addChild(this.wttext21);
this.valuetable.addChild(this.wttext31);

  this.wttext2.visible=false;
  this.wttext3.visible=false;
 this.wttext1s.visible=false;
 this.wttext2s.visible=false;
 this.wttext3s.visible=false;
  this.wttext21.visible=false;
  this.wttext31.visible=false;
 this.valuetable.visible=false;
  this.arrow=this.game.add.sprite(1002,this.waterlevel3.y-100, 'arrow');
  this.arrow.anchor.set(.5);
  this.arrow.tint="0x339900"
  this.arrow.animations.add('allframes');
  this.arrow.visible=false;
  this.arrow.angle=90
 },
onDragStart:function(obj)
  {
    this.dragStop=false;
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
     if(currentobj==this.dragwater){
       this.arrowShow=2;
      this.ArowShowing()
     }
    if(currentobj==this.dragweight){
      this.arrowShow=4;
      this.ArowShowing()
      this.dragweight.angle=0
      this.dragweight.anchor.setTo(.5,.8);
    //  this.dragweight.colliders.x=0
    //  this.dragweight.colliders.y=-400
      
    }
  },
  onDragStop:function(obj)
  {
    this.dragStop=true;
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
   gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    this.arrow1.visible=false;
    this.arrow1.animations.play("frame1",10,false);
    this.collider.visible=false;

    //this.dragweight.visible=false;
    if(currentobj==this.dragwater){
     
      this.dragwater.colliders.visible=false;
    this.dragwater.x=this.watertubes.x+220
    this.dragwater.y=this.watertubes.y-230
    var anim=this.dragwater.animations.add("frameall");
    this.dragwater.animations.play("frameall",10,false)
    this.game.time.events.add(Phaser.Timer.SECOND,this.PourWater,this);
 
    anim.onComplete.add(function () {
     // this.dragwater.visible=false;
     var anims=this.dragwater.animations.add("frame1",[0]);
     this.dragwater.animations.play("frame1",10,false)
     var tweens21=this.game.add.tween(this.dragwater).to({x:this.dragwater.xp,y:this.dragwater.yp}, 500, Phaser.Easing.Linear.Out, true);
     tweens21.onComplete.add(function () {
      var tweens22=this.game.add.tween(this.dragwater).to({x:-300}, 500, Phaser.Easing.Linear.Out, true);
      tweens22.onComplete.add(function () {
        this.dragwater.destroy();
        
      this.dragweight.alpha=1;
      var tweens2=this.game.add.tween(this.dragweight).to({x:this.dragweight.xposed}, 500, Phaser.Easing.Linear.Out, true);
      this.dragweight.inputEnabled = true;
      this.dragweight.input.useHandCursor = true;
       this.dragweight.input.enableDrag(true);
       this.collider.visible=true;
       tweens2.onComplete.add(function () {
          this.arrowShow=3;
          this.ArowShowing();
          this.ProcedureText(2);
       }.bind(this));
       }.bind(this));
       this.watertubes.visible=false;
      this.waterlevel1.visible=true;
      this.waterlevel2.visible=true;
      this.waterlevel3.visible=true;
      this.tube.visible=true;
      this.mask.visible=true;
     }.bind(this));
      
    }.bind(this));
  }  if(currentobj==this.dragweight){
    this.dragweight.visible=false;
    this.collider.visible=false;
    this.wieght.y=this.wieght.originaly;
    this.wieghtmask.visible=false;
this.wieght.mask = null;
    this.wieght.visible=true;
    this.wieght.alpha=1;
    this.dragweight.alpha=0;
    var tweens21=this.game.add.tween(this.wieght).to({y:this.wieght.ypos}, 3000, Phaser.Easing.Elastic.Out, true);
    tweens21.onComplete.add(function () {
    
  
   
     }.bind(this));
   
  /*  this.range=Math.floor(Math.random()*this.arraVals.length);
    console.log(this.range);
    this.tookVal++;
    var tweens2=this.game.add.tween(this.needle).to({x:this.needle.x,y:this.arraVals[this.range]}, 600, Phaser.Easing.Linear.Out, true);
    var tweens2=this.game.add.tween(this.weight).to({x:this.weight.x,y:this.weight.ypos}, 500, Phaser.Easing.Elastic.Out, true);
  
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.AddReult,this);
    */
    this.ProcedureText(3);
    this.game.time.events.add(500,this.WaterRaise,this);
  }
  this.dragStop=false;
    currentobj=null;
  },
  WaterRaise:function(){
  
    this.wieghtmask.visible=true;
     this.wieght.mask = this.wieghtmask;
    this.tookVal++;
    this.randomRange=Math.floor(Math.random()*this.arraVals.length)
    this.waterraised=true;

  
  },
  PourWater:function(){
    this.watertubes.animations.add("frameall");
    this.watertubes.animations.play("frameall",10,false)
  },
  
  update: function()
  {
    deltatime=this.game.time.elapsed / 1000;
  //  this.waterlevel3.y=this.waterlevel2.y
    this.detectCollision();
 
   // if(this.waterlevel2.scale.y>1.75){

   // }else{
    //  this.waterlevel2.scale.y+=deltatime*1
   //this.mask.y-=deltatime*10
      
       //this.waterlevel3.y-=deltatime*10

    //  }
    if(this.waterraised){
      this.mask.y-=deltatime*100

      this.waterlevel3.y-=deltatime*100
     
      if(this.mask.y<this.arraVals[this.randomRange]){
        
        this.game.time.events.add(Phaser.Timer.SECOND,this.CheckingRange,this);
      
        this.arraVals.splice(this.randomRange,1);
        this.waterraised=false;
        
      }
    }
  
    
  },
  CheckingRange:function(){
    
    
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.TookValues,this);
  },
  TookValues:function(){
    this.arrow.y=this.waterlevel3.y-5
    this.arrow.visible=true;
    this.arrow.animations.add("frameall");
    this.arrow.animations.play("frameall",10,true);
    this.valuetable.visible=true;
    this.wttext.visible=true;
   
    this.ProcedureText(4);
    
    if(this.tookVal==1){
      this.wttext1.visible=true;
      this.wttext11.visible=true;
      this.wttext1s.visible=true;
    result4=this.arraPointVals[this.randomRange]
      this.wttext11.text= result4+" ml";
      this.arraPointVals.splice(this.randomRange,1);
    }
    if(this.tookVal==2){
      this.wttext2.visible=true;
      this.wttext21.visible=true;
      this.wttext2s.visible=true;
    result5=this.arraPointVals[this.randomRange];
      this.wttext21.text= result5+" ml";
      this.arraPointVals.splice(this.randomRange,1);
    }
    if(this.tookVal==3){
      this.wttext3.visible=true;
      this.wttext31.visible=true;
      this.wttext3s.visible=true;
    result6=this.arraPointVals[this.randomRange]
      this.wttext31.text= result6+" ml";
      this.arraPointVals.splice(this.randomRange,1);
    }
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.ToShowText,this);
    if(this.tookVal<3){
     
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.GotoTakeNextvalues,this);
    }
  },
  ToShowText:function(){
    this.arrow.visible=false;
    this.arrow.animations.add("framea1",[0]);
    this.arrow.animations.play("framea1",10,false);
  
    if(this.tookVal==3){
      this.ProcedureText(7);
      }
  },

  GotoTakeNextvalues:function(){
    this.dragweight.visible=true;
 
  //  this.backToWaterPos=true;
    this.arrow.visible=false;
    this.arrow.animations.add("frame1",[0]);
    this.arrow.animations.play("frame1",10,false);
    var tweens2=this.game.add.tween(this.wieght).to({alpha:0}, 100, Phaser.Easing.Linear.Out, true);
    var tweens21=this.game.add.tween(this.dragweight).to({alpha:1}, 500, Phaser.Easing.Linear.Out, true);
    tweens2.onComplete.add(function () {
     // this.wieght.visible=false;
     // this.wieghtmask.visible=false;
     // this.wieght.mask = null;
      this.waterlevel3.y= this.waterlevel3.originalY;
      this.mask.y= this.mask.originalpos;
      console.log(this.tube.visible);
  
       this.dragweight.reset(this.dragweight.xp,this.dragweight.yp);//
      
        this.dragweight.anchor.setTo(.5,.5);
        this.dragweight.angle=90;
      
        if(this.tookVal==1){
          this.ProcedureText(5);
          this.game.time.events.add(Phaser.Timer.SECOND*2.5,this.AudioFortakingAnother,this);
          }if(this.tookVal==2){
            this.ProcedureText(6);
            this.game.time.events.add(Phaser.Timer.SECOND*2.5,this.AudioFortakingAnother,this);
            }
           
      /////  
       
    }.bind(this));
    tweens21.onComplete.add(function () {
    
     
      // this.dragweight.colliders.x=0
     //  this.dragweight.colliders.y=-200
    }.bind(this));
   // this.tube.visible=true;
  
  
  },
  AudioFortakingAnother:function(){
    this.arrowShow=3;
    this.ArowShowing();
    this.dragweight.inputEnabled = true;
       this.dragweight.input.useHandCursor = true;
        this.dragweight.input.enableDrag(true);
    this.ProcedureText(2);
    },
  detectCollision:function(){
    if( this.dragStop){
   if(this.collider!=null&& this.collider.enableBody && currentobj!=null)
     {
         this.game.physics.arcade.overlap(this.dragwater.colliders, this.collider,function() {this.match_Obj()},null,this);
        // collider=collider_battery;
     } 


     if(this.collider!=null&& this.collider.enableBody && currentobj!=null)
     {
         this.game.physics.arcade.overlap(this.dragweight.colliders, this.collider,function() {this.match_Obj()},null,this);
        // collider=collider_battery;
     } 
    }

  
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
      if(currentobj==this.dragwater){
        this.arrowShow=1;
       this.ArowShowing()
      }
      if(currentobj==this.dragweight){
        this.arrowShow=3;
        this.ArowShowing()
        this.dragweight.angle=90
        this.dragweight.anchor.setTo(.5,.5);
      //  this.dragweight.colliders.x=0
      //  this.dragweight.colliders.y=-200
       
      }
      this.dragStop=false;
         currentobj=null;
      }
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;

      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A2",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
