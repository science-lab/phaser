var experiment_a1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;
var fontStyle;
//ip address
var ip;


}

experiment_a1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
this.tookVal=0;
 muted = false;
 voice=this.game.add.audio("obj",1);

 this.dialogx=25;
 this.dialogy=60;

 
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(.241,.253);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.lineStyle(10,0xff704d,1);
  maskBg1.beginFill(0xffffcc);
  maskBg1.drawRect(20, 40, 300, 1010);
  maskBg1.alpha=1;*/

  

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  
  resetButton.events.onInputDown.add(this.resetTheGame, this);
  
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

  this.arraVals=[127,132,137,141,145];
  this.arraVals1=[625,629,632,636,639];
  //this.arraPointVals=[2.1,2.2,2.3,2.4,2.5];
  this.arraPointVals=[1.6,1.7,1.8,1.9,2];
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "25px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};


  this.arrangeScene();
  this.intDailog();
  
      



////////////////////////////////////////////////////////////////////

 },
 intDailog:function(){

    dialog_box=this.game.add.sprite(10,20, 'dialogue_box1');
    dialog_box.scale.setTo(.8,.7);
    dialog_text="Lets perform the experiment.";
    dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
    voice=this.game.add.audio("lets_perform_Exp",1);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.LoadExp,this);
   
 },
 LoadExp:function()
 {  
     voice.play();
     this.game.time.events.add(Phaser.Timer.SECOND*3,this.StartExperiment,this);
 },
 StartExperiment:function(){
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  this.arrow1=this.game.add.sprite(0,0, 'arrow');
  this.arrow1.anchor.set(.5);
  this.arrow1.animations.add("frame1",[0])
  this.arrow1.animations.add("frameall")
  this.arrow1.visible=false;
  this.arrowShow=0;
 this.ProcedureText(1);
    var tweens2=this.game.add.tween(this.dragweight).to({x:this.dragweight.xposed}, 500, Phaser.Easing.Linear.Out, true);
    tweens2.onComplete.add(function () {
      this.arrowShow=1;
      this.ArowShowing();
    this.dragweight.inputEnabled = true;
    this.dragweight.input.useHandCursor = true;
    this.dragweight.input.enableDrag(true);
  }.bind(this))

 },
 ArowShowing:function(){
  this.arrow1.visible=true;
  this.arrow1.animations.play("frameall",10,true);
  switch(this.arrowShow){
    case 1:
      this.arrow1.x= this.dragweight.x;
      this.arrow1.y= this.dragweight.y-50;
      break;
      case 2:
      
        this.arrow1.x= this.springbalance.x+20;
        this.arrow1.y= this.springbalance.y+200;
        break;
    
      
  }
  },
 ProcedureText:function(vals){
    switch(vals){
      case 1:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure1",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Hang the object from the hook of the spring balance.";
      dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
      break;
      case 2:
        voice.stop();
        voice=this.game.add.audio("A_exp_procedure2",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Take the reading of the object from the pointer on the scale.";
        dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
    break;
    case 3:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure3",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Now take two more readings by hang the object again from the\nhook of the spring balance.";
      dialog=this.game.add.text(this.dialogx,this.dialogy-25,dialog_text,fontStyle);
      break;
    case 4:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure4",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Take one more reading by hanging the object.";
      dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
      break;
      case 5:
        voice.stop();
        voice=this.game.add.audio("A_exp_procedure5",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Again hang the object from the hook of the spring balance.";
        dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
        break;
        case 6:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure6",1);
          voice.play();
          dialog_text="";
          dialog.destroy();
          dialog_text="Now hang the object one more time to take the reading.";
          dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
          break;
          case 7:
            voice.stop();
            voice=this.game.add.audio("A_exp_procedure7",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Click on next button to perform the volume of the object.";
            dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
            play.visible=true;
            break;
    }

 },
 arrangeScene:function(){
  this.ring1=this.game.add.sprite(1000,550, 'ring1');
  this.ring1.anchor.set(.5);
  this.stand=this.game.add.sprite(900,650, 'stand');
  this.stand.anchor.set(.5);
  this.stand.scale.setTo(.7,.7);
  this.hook=this.game.add.sprite(1000,630, 'hook');
  this.hook.anchor.set(.5);
  this.weight=this.game.add.sprite(1000,655, 'hookweight');
  this.weight.anchor.set(.5);
  this.weight.ypos=656;
  this.blackbox=this.game.add.sprite(1000,550, 'cols1');
  this.blackbox.anchor.set(.5);
  this.blackbox.scale.setTo(1,5);
  this.whitebox=this.game.add.sprite(1000,550, 'collider');
  this.whitebox.anchor.set(.5);
  this.whitebox.scale.setTo(1,5);
  this.whitebox.alpha=.8
 
 // console.log(this.mask1.y);
  this.springbalance=this.game.add.sprite(1000,550, 'springbalance');
  this.springbalance.anchor.set(.5);
  this.mask1 = this.game.add.graphics(1000, 550);

  //	Shapes drawn to the Graphics object must be filled.
  this.mask1.beginFill(0x338899);

  //	Here we'll draw a circle

  this.mask1.drawRect(-50,-55, 100, 900);
  this.whitebox.mask=this.mask1;
  this.mask1.y=558;//558
  this.mask1.mainypos=this.mask1.y;
//60,149,153,157,161,166
  this.needle=this.game.add.sprite(0,60, 'needle');//0,60
  this.needle.anchor.set(.5);
  this.needle.mainypos=this.needle.y;
  this.springbalance.addChild(this.needle);
  this.weight.visible=false;

  this.dragweight=this.game.add.sprite(-100,900, 'weight');
  this.dragweight.anchor.set(.5);
  this.dragweight.events.onDragStart.add(function() {this.onDragStart(this.dragweight)}, this);
  this.dragweight.events.onDragStop.add(function() {this.onDragStop(this.dragweight)}, this);
  this.dragweight.xposed=300;
  this.dragweight.xp= this.dragweight.xposed;
  this.dragweight.yp= this.dragweight.y;
 
  this.game.physics.arcade.enable(this.dragweight);
  this.dragweight.enableBody =true;
  this.collider = this.game.add.sprite(900,720,'collider');
  this.collider.scale.setTo(4,2);
  this.game.physics.arcade.enable(this.collider);
  this.collider.alpha=.0001
  this.collider.enableBody=true;

  this.valuetable=this.game.add.sprite(355,500, 'valtable');
  this.valuetable.anchor.set(.5);
  this.valuetable.scale.setTo(.6,.8);
   this.obsline1=this.game.add.sprite(5,-60,'obsline');
  this.obsline1.anchor.set(.5);
  this.obsline1.scale.setTo(1,1.24);
  this.obsline1.angle=90
  this.valuetable.addChild(this.obsline1);


//  this.wttext=this.game.add.text(100,100,"Weight of the Object",fontStyle);
  this.wttext=this.game.add.text(-150,-122,"Measurement",fontStyle);
  this.valuetable.addChild(this.wttext);

  this.wttext1=this.game.add.text(-180,-45,"W\u2081",fontStyle);
  this.valuetable.addChild(this.wttext1);
  this.wttext2=this.game.add.text(-180,11,"W\u2082",fontStyle);
  this.valuetable.addChild(this.wttext2);
  this.wttext3=this.game.add.text(-180,71,"W\u2083",fontStyle);
  this.valuetable.addChild(this.wttext3);

  this.wttext1s=this.game.add.text(-80,-45,"=",fontStyle);
  this.valuetable.addChild(this.wttext1s);
  this.wttext2s=this.game.add.text(-80,11,"=",fontStyle);
  this.valuetable.addChild(this.wttext2s);
  this.wttext3s=this.game.add.text(-80,71,"=",fontStyle);
  this.valuetable.addChild(this.wttext3s);

  result1=0
  result2=0
  result3=0
  this.wttext11=this.game.add.text(2,-45,"",fontStyle);
  this.wttext21=this.game.add.text(2,11,"",fontStyle);
  this.wttext31=this.game.add.text(2,71,"",fontStyle);
  this.valuetable.addChild(this.wttext11);
  this.valuetable.addChild(this.wttext21);
  this.valuetable.addChild(this.wttext31);

  this.wttext2.visible=false;
  this.wttext3.visible=false;
 this.wttext1s.visible=false;
 this.wttext2s.visible=false;
 this.wttext3s.visible=false;
  this.wttext21.visible=false;
  this.wttext31.visible=false;
 this.valuetable.visible=false;
  this.arrow=this.game.add.sprite(30,this.needle.y-110, 'arrow');
  this.arrow.anchor.set(.5);
  this.arrow.tint="0x339900"
  this.arrow.animations.add('allframes');
  this.arrow.visible=false;
  this.arrow.angle=90
  this.springbalance.addChild(this.arrow);
 },
onDragStart:function(obj)
  {
  
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    this.arrowShow=2;
    this.ArowShowing();
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    currentobj=null;
    this.arrow1.visible=false;
    this.arrow1.animations.add("frame1",10,false)
    this.collider.visible=false;
    this.dragweight.visible=false;

    this.hook.visible=false;
    this.weight.visible=true;
    this.range=Math.floor(Math.random()*this.arraVals.length);
    console.log(this.range);
    this.tookVal++;
    var tweens2=this.game.add.tween(this.needle).to({x:this.needle.x,y:this.arraVals[this.range]}, 600, Phaser.Easing.Linear.Out, true);
    var tweens2=this.game.add.tween(this.mask1).to({x:this.mask1.x,y:this.arraVals1[this.range]}, 600, Phaser.Easing.Linear.Out, true);
    var tweens2=this.game.add.tween(this.weight).to({x:this.weight.x,y:this.weight.ypos}, 500, Phaser.Easing.Elastic.Out, true);
   // if(this.tookVal==1){
    this.ProcedureText(2);
  //  }
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.AddResultWithArrow,this);
  },
  AddResultWithArrow:function(){
    this.valuetable.visible=true;
    this.arrow.visible=true;
   
    if(this.tookVal==1){
     
      this.wttext1.visible=true;
      this.wttext1s.visible=true;
    
    }if(this.tookVal==2){
      this.wttext2.visible=true;
      this.wttext2s.visible=true;
    }if(this.tookVal==3){
      this.wttext3.visible=true;
      this.wttext3s.visible=true;
    }
   
    this.arrow.y=this.needle.y-110
    this.arrow.animations.play('allframes', 10,true);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.AddReult,this);
  },
  AddReult:function(){
    this.valuetable.visible=true;
    this.arrow.visible=true;
    if(this.tookVal==1){
     
      this.wttext1.visible=true;
      this.wttext11.visible=true;
      result1=this.arraPointVals[this.range]
      this.wttext11.text= result1+" kgf";
    }if(this.tookVal==2){
     
      this.wttext2.visible=true;
      this.wttext21.visible=true;
      result2=this.arraPointVals[this.range]
      this.wttext21.text= result2+" kgf";
    }if(this.tookVal==3){
      this.wttext3.visible=true;
      this.wttext31.visible=true;
      result3=this.arraPointVals[this.range]
      this.wttext31.text= result3+" kgf";
    }
    this.arraVals.splice(this.range,1);
    this.arraVals1.splice(this.range,1);
    this.arraPointVals.splice(this.range,1);
   
    if(this.tookVal<3){
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeAnotherraedingText,this);
    }else{
      this.arrow.animations.add('frame1',[0]);
      this.arrow.animations.play('frame1', 10,false);
      this.arrow.visible=false;
      this.ProcedureText(7);
    }
  },
  TakeAnotherraedingText:function(){
  
    this.arrow.visible=false;
    this. arrow.animations.play('allframes', 25,false);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeAnotherraeding,this);
  },
  TakeAnotherraeding:function(){
    this.collider.visible=true;
    this.dragweight.visible=true;

    this.hook.visible=true;
    this.weight.visible=false;
    var tweens21=this.game.add.tween(this.needle).to({x:this.needle.x,y:this.needle.mainypos}, 600, Phaser.Easing.Linear.Out, true);
     var tweens21=this.game.add.tween(this.mask1).to({x:this.mask1.x,y:this.mask1.mainypos}, 600, Phaser.Easing.Linear.Out, true);
    
    var tweens22=this.game.add.tween(this.dragweight).to({x:this.dragweight.xp,y:this.dragweight.yp}, 600, Phaser.Easing.Linear.Out, true);
    tweens22.onComplete.add(function () {
      this.arrowShow=1;
      this.ArowShowing();
      this.dragweight.inputEnabled = true;
      this.dragweight.input.useHandCursor = true;
      this.dragweight.input.enableDrag(true);
    this.dragweight.body.enable=true;
      if(this.tookVal==1){
        this.ProcedureText(3);
       // this.game.time.events.add(Phaser.Timer.SECOND*3,this.AudioFortakingAnother,this);
      } if(this.tookVal==2){
       this.ProcedureText(6);
        }
       

  
     
    
    }.bind(this));
   
  },
 /* AudioFortakingAnother:function(){
   
  if(this.tookVal==1){
    this.ProcedureText(5);
   
  
    } if(this.tookVal==2){
     // this.ProcedureText(6);
      }
  },*/
  update: function()
  {
    deltatime=this.game.time.elapsed / 1000;
    //this.mask1.y+=deltatime*100
    
    this.detectCollision();
  
  },
 
  detectCollision:function(){
   

     if(this.collider!=null&& this.collider.enableBody && currentobj!=null)
     {
         this.game.physics.arcade.overlap(this.dragweight, this.collider,function() {this.match_Obj()},null,this);
        // collider=collider_battery;
     } 

  
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
      this.arrowShow=1;
      this.ArowShowing();
     
         currentobj=null;
      }
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();

      this.state.start("Experiment_A2", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;

      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
