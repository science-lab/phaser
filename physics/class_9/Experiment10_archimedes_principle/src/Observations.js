var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var count;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var sceneNo;
  var r1;
  var r2;
  var r3;
  var r4;
  var r5;
  var r6;
  var r7;
  var r8;
  var r9;
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

sceneNo=1;
count=0;
pageCount=1;
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
 muted = false;
 voice=this.game.add.audio("A_observation",1);

 
 voice.play();
 //setTimeout(this.nextSound,2000);
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

 


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

 

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  //////////////////////////////////Graph///////////////////////////
//  graph=this.game.add.sprite(1100,200,'obervation_graph');
//  graph.scale.setTo(.15,.15);

  /////////////////////////////////////////////////////////////////

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  headfontStyle2={ font: "28px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  fontStyle2={ font: "32px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  
 //dialogbox1=this.game.add.sprite(50,30,'dialogue_box3');
 // dialogbox1.scale.setTo(.8,.7);

  dialogbox2=this.game.add.sprite(50,50,'dialogbox2');
  dialogbox2.scale.setTo(.65,.6);
  //dialogbox2.scale.setTo(.8,1);

  base= this.game.add.sprite(49,370,'table04');
  base.scale.setTo(1,.7);
  
  //base.scale.setTo(1,1);
  //switch(exp_Name){
   // case "Iron with copper sulphate":
   
    
  
    /*inference_text_3=this.game.add.text(950,700,inference_3,fontStyle);
    inference_text_4=this.game.add.text(950,750,inference_4,fontStyle_1);*/
    

   // break;
 // }
next_btn = this.game.add.sprite(1590,870,'components','next_disabled.png');
next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1590,870,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
next_btn.inputEnabled = true;
 next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 prev_btn = this.game.add.sprite(230,870,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(230,870,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
 prev_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrev, this);
  prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

previouspress=false;

  this.addTheory();

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        addTheory:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy();
              voice=this.game.add.audio("A_observation",1);
              voice.play();
              dialogbox2.visible=true;
              base.visible=true;
             
              if(previouspress){
                observatio_top_text_1.destroy();
                observatio_top_text_2.destroy();
                observatio_top_text_3.destroy();
                head_text_1.destroy();
                for(var i=2;i<22;i++){
                  this["text_"+i].destroy();
                }
             
              previouspress=false;
              }
              


              observatio_top_text_1=this.game.add.text(80,120,"Range of the spring balance = 0 - 500 gf",fontStyle2);
              observatio_top_text_2=this.game.add.text(80,170,"Least count of the spring balance = 10 gf",fontStyle2);
              observatio_top_text_3=this.game.add.text(80,220,"Zero correction for the spring balance = 0",fontStyle2);
         
              head_text_1=this.game.add.text(700,320,"Solid weight immersed in tap water",fontStyle2);
              var text1="Weight of the solid \n        in air\n      (W\u2081 gf)"
              this.text_2=this.game.add.text(85,400,text1,fontStyle2);
              var text1="Weight of the solid \nimmersed in water\n        (W\u2082 gf)"
              this.text_3=this.game.add.text(410,400,text1,fontStyle2);
              var text1="Apparent loss in\nweight of the\n    solid\n  (W\u2081 - W\u2082)gf"
              this.text_4=this.game.add.text(739,400,text1,fontStyle2);
              var text1=" Weight of empty\n       beaker\n       (W\u2083 gf)"
              this.text_5=this.game.add.text(1000,400,text1,fontStyle2);
              var text1="Weight of beaker\n with overflowed\n       water\n       (W\u2084 gf)"
              this.text_6=this.game.add.text(1270,400,text1,fontStyle2);
              var text1="Weight of water\n collected in\n    beaker\n    (W\u2084 - W\u2083) gf"
              this.text_7=this.game.add.text(1530,400,text1,fontStyle2);

              var text1="Observed";
              this.text_8=this.game.add.text(70,650,text1,fontStyle2);
              var text1="Corrected";
              this.text_9=this.game.add.text(225,650,text1,fontStyle2);

              var text1="Observed";
              this.text_10=this.game.add.text(385,650,text1,fontStyle2);
              var text1="Corrected";
              this.text_11=this.game.add.text(550,650,text1,fontStyle2);

              var text1="290";
              this.text_12=this.game.add.text(110,770,text1,fontStyle2);
              var text1="290";
              this.text_13=this.game.add.text(255,770,text1,fontStyle2);
              var text1="255";
              this.text_14=this.game.add.text(430,770,text1,fontStyle2);
              var text1="255";
              this.text_15=this.game.add.text(600,770,text1,fontStyle2);
              var text1="290 - 255";
              this.text_16=this.game.add.text(790,650,text1,fontStyle2);
              var text1="35";
              this.text_17=this.game.add.text(830,770,text1,fontStyle2);
              var text1="50";
              this.text_18=this.game.add.text(1100,770,text1,fontStyle2);
              var text1="85";
              this.text_19=this.game.add.text(1380,770,text1,fontStyle2);
              var text1="85 - 50";
              this.text_20=this.game.add.text(1590,650,text1,fontStyle2);
              var text1="35";
              this.text_21=this.game.add.text(1620,770,text1,fontStyle2);

  
              break;
              case 2:
                voice.destroy();
                voice=this.game.add.audio("A_observation1",1);
                voice.play();
                observatio_top_text_1.destroy();
              observatio_top_text_2.destroy();
              observatio_top_text_3.destroy();
              head_text_1.destroy();
              for(var i=2;i<22;i++){
                this["text_"+i].destroy();
              }
              observatio_top_text_1=this.game.add.text(80,120,"Range of the spring balance = 0 - 500 gf",fontStyle2);
              observatio_top_text_2=this.game.add.text(80,170,"Least count of the spring balance = 10 gf",fontStyle2);
              observatio_top_text_3=this.game.add.text(80,220,"Zero correction for the spring balance = 0",fontStyle2);
           //   sno_text_1=this.game.add.text(125,330,"S.No",fontStyle2);
              head_text_1=this.game.add.text(600,320,"Solid weight immersed in strongly salty water",fontStyle2);
              var text1="Weight of the solid \n        in air\n      (W\u2081 gf )"
              this.text_2=this.game.add.text(85,400,text1,fontStyle2);
              var text1="Weight of the solid \nimmersed in water\n        (W\u2082 gf )"
              this.text_3=this.game.add.text(410,400,text1,fontStyle2);
              var text1="Apparent loss in\nweight of the\n    solid\n  (W\u2081 - W\u2082)gf"
              this.text_4=this.game.add.text(739,400,text1,fontStyle2);
              var text1=" Weight of empty\n       beaker\n       (W\u2083 gf)"
              this.text_5=this.game.add.text(1000,400,text1,fontStyle2);
              var text1="Weight of beaker\n with overflowed\n       water\n       (W\u2084 gf)"
              this.text_6=this.game.add.text(1270,400,text1,fontStyle2);
              var text1="Weight of water\n collected in\n    beaker\n    (W\u2084 - W\u2083) gf"
              this.text_7=this.game.add.text(1530,400,text1,fontStyle2);

              var text1="Observed";
              this.text_8=this.game.add.text(70,650,text1,fontStyle2);
              var text1="Corrected";
              this.text_9=this.game.add.text(225,650,text1,fontStyle2);

              var text1="Observed";
              this.text_10=this.game.add.text(385,650,text1,fontStyle2);
              var text1="Corrected";
              this.text_11=this.game.add.text(550,650,text1,fontStyle2);

              var text1="290";
              this.text_12=this.game.add.text(110,770,text1,fontStyle2);
              var text1="290";
              this.text_13=this.game.add.text(255,770,text1,fontStyle2);
              var text1="235";
              this.text_14=this.game.add.text(430,770,text1,fontStyle2);
              var text1="235";
              this.text_15=this.game.add.text(600,770,text1,fontStyle2);
              var text1="290 - 235";
              this.text_16=this.game.add.text(790,650,text1,fontStyle2);
              var text1="55";
              this.text_17=this.game.add.text(830,770,text1,fontStyle2);
              var text1="50";
              this.text_18=this.game.add.text(1100,770,text1,fontStyle2);
              var text1="105";
              this.text_19=this.game.add.text(1380,770,text1,fontStyle2);
              var text1="105 - 50";
              this.text_20=this.game.add.text(1590,650,text1,fontStyle2);
              var text1="55";
              this.text_21=this.game.add.text(1620,770,text1,fontStyle2);

                break;
          }
        },

        nextSound:function()
        {

           // voice.destroy();
            //voice=this.game.add.audio("A_observation2",1);
           // voice2.play();

        },
    toNext:function(){
      if(pageCount<2){
        prev_btn.visible=true;

        pageCount++;
          this.addTheory();
          if(pageCount>=2){
            play.visible=true;
            next_btn.visible=false;  
          }
        }
       
        
        
        },
    toPrev:function(){
      if(pageCount>1){
        next_btn.visible=true;
        previouspress=true;
        pageCount--;
          this.addTheory();
          if(pageCount<=1){
            play.visible=false;
            prev_btn.visible=false;  
          }
        }
        },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice1.destroy();
     
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  update: function()
  {
    // if(sceneNo==1){
    //   count++;
    //   //console.log(count);
    //   if(count==1050){
    //     voice.destroy();
    //     voice=this.game.add.audio("A_observation2",1);
    //     voice.play();
    //   }
    // }
  },
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  //voice.destroy();
      //  voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  