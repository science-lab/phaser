var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var exp_Name;
  var currentScene;
  var previousScene;
  
  }
  
  materials.prototype ={
  
  init: function(exp_nam) 
  {
    //ip = ipadrs;
    //exp_Name=exp_nam;

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1610,880,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,880,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(310,880,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,880,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScenes, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
        //if(exp_Name=="Iron with copper sulphate"){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                mat_img_11.destroy(true);
              
              }
            
             
              mat_img_1 = this.game.add.sprite(280,350,'Spring_blance_0');
              mat_img_1.scale.setTo(1,1);
              mat_name_1=this.game.add.text(550,370,"Spring balance:",headfontStyle);
              mat_content_1="A spring balance is a weighing apparatus used in industries\nto measure the mass of different loads. It is a type of weighing\nscale. It consists of a spring fixed at one end with a hook to\nattach an object at the other.";
              mat_text_1=this.game.add.text(550,490,mat_content_1,fontStyle);
              
              break;
          case 2:
            if(previousScene==3){
              mat_img_0.destroy(true);
            }
           
             mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_11 = this.game.add.sprite(225,300,'Big_Beaker_Back');
              mat_img_11.scale.setTo(.8,.8);
              mat_img_1 = this.game.add.sprite(225,300,'Big_Beaker_without_water');
              mat_img_1.scale.setTo(.8,.8);
            
              mat_name_1=this.game.add.text(550,370,"Overflow can:",headfontStyle);
              mat_content_1="The overflow can is used to measure the volume of an irregular\nobject.";
              mat_text_1=this.game.add.text(550,490,mat_content_1,fontStyle);
              
          break;
          case 3:
             
              mat_img_1.destroy(true);
              mat_img_11.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(200,300,'stand');
              mat_img_1.scale.setTo(.4);
              mat_img_0 = this.game.add.sprite(148,190,'stand_top');
              mat_img_0.scale.setTo(.5,.8);
            
              mat_name_1=this.game.add.text(550,370,"Stand:",headfontStyle);
              mat_content_1="A stand is a piece of scientific equipment intended to support\nother pieces of equipment and glassware for instance,\nburettes, test tubes and flasks.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 4:
            mat_img_1.destroy(true);
            if(previousScene==3){
              mat_img_0.destroy(true);
            }
             
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(320,420,'weight');
            mat_img_1.scale.setTo(1.2,1.2);
          
           
           
              mat_name_1=this.game.add.text(550,370,"Solid object:",headfontStyle);
              mat_content_1="It is a solid object which can be used as an unknown weight.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 5:
            mat_img_1.destroy(true);
             
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(240,420,'Digital_weight');
            mat_img_1.scale.setTo(1.2,1.2);
          
           
           
              mat_name_1=this.game.add.text(550,370,"Weighing machine:",headfontStyle);
              mat_content_1="Weighing machine is a device used to measure mass or weight.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
         
         
          // case 8:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(230,350,'hydrochloric_acid');
          //     mat_img_1.scale.setTo(.8,.8);
          //     mat_name_1=this.game.add.text(700,370,"A piece of sandpaper:",headfontStyle);
          //     mat_content_1="Dilute hydrochloric acid is often used in the \nextraction of basic substances from mixtures or \nin the removal of basic impurities. The dilute \nacid converts the base such as ammonia or an \norganic amine into water-soluble chloride salt.";
          //     mat_text_1=this.game.add.text(700,470,mat_content_1,fontStyle);
          // break;
          // case 9:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(300,400,'Red_Litmus');
          //     mat_img_1.scale.setTo(.8,.8);
          //     mat_name_1=this.game.add.text(700,370,"Litmus solution(blue/red):",headfontStyle);
          //     mat_content_1="Red and blue litmus solution Indicators are used \nto determine whether substances are acids, bases, \nor neutral.";
          //     mat_text_1=this.game.add.text(700,470,mat_content_1,fontStyle);
          // break;
          // case 10:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(230,350,'Zinc');
          //     mat_img_1.scale.setTo(.8,.8);
          //     mat_name_1=this.game.add.text(700,370,"Zinc metal granules:",headfontStyle);
          //     mat_content_1="Zinc granules are the solid crystals of zinc. ";
          //     mat_text_1=this.game.add.text(700,470,mat_content_1,fontStyle);
          // break;
          // case 11:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(230,350,'sodium_bicarbonate');
          //     mat_img_1.scale.setTo(.25,.2);
          //     mat_name_1=this.game.add.text(650,370,"Sodium bicarbonate:",headfontStyle);
          //     mat_content_1="Sodium bicarbonate, commonly known as \nbaking soda or bicarbonate of soda, is a \nchemical compound with the formula NaHCO₃.";
          //     mat_text_1=this.game.add.text(650,470,mat_content_1,fontStyle);
          // break;
          // case 12:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(270,350,'lemonjuice');
          //     mat_img_1.scale.setTo(1.2,1.2);
          //     mat_name_1=this.game.add.text(650,370,"Lemon juice:",headfontStyle);
          //     mat_content_1="The major acid in lemons is citric acid, which \nconstitutes around 5 to 6% of the lemon's juice. \nOther acids are also present, although in much \nlower concentrations than citric acid. Malic acid is \none of these, present at around 5% of the \nconcentration of citric acid.";
          //     mat_text_1=this.game.add.text(650,470,mat_content_1,fontStyle);
          // break;

          
          
          }
          

        //}
      },
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==5){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
        toNextScenes:function()
        {
        voice.destroy();
        this.state.start("Theory", true, false, ip);
        
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  