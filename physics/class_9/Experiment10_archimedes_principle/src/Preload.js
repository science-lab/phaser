var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
 
  //var corrent_val1;
  var resistance_val1;
  
 // var corrent_val2;
  var resistance_val2;
  
  //var corrent_val3;
  var resistance_val3;
  
  //var corrent_val4;
  var resistance_val4;
 
  var R1voltage_val1;
  var R1voltage_val2;
  var R1voltage_val3;
  var R2voltage_val1;
  var R2voltage_val2;
  var R2voltage_val3;
  var R3voltage_val1;
  var R3voltage_val2;
  var R3voltage_val3;

  var R1current_val1;
  var R1current_val2;
  var R1current_val3;
  var R2current_val1;
  var R2current_val2;
  var R2current_val3;
  var R3current_val1;
  var R3current_val2;
  var R3current_val3;

}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
       
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         //this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.atlasJSONHash('arrow', 'assets/arrow.png', 'assets/arrow.json'); //Standardization Buttons
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
     
         this.game.load.image('observation_table','assets/Class9/obs_table-01.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('direction','assets/Direction.png');
//////////////////////////////Experiment images//////////////////////////////////
this.game.load.image('stand','assets/Class9/Stand_bottum.png');
this.game.load.image('stand_top','assets/Class9/Stand_top.png');
this.game.load.image('Spring_blance02','assets/Class9/Spring_blance-02.png');
this.game.load.image('cols1','assets/Class9/col.png');
this.game.load.image('table04','assets/Class9/table04.png');
//this.game.load.image('table02','assets/Class9/table3-01-01.png');
//this.game.load.image('table03','assets/Class9/table03.png');
this.game.load.image('dialogbox2','assets/Class9/dialogbox2.png');
this.game.load.image('valtable','assets/Class9/val_table-01.png');
this.game.load.image('obsline','assets/Class9/obs_table_line-01.png');
this.game.load.image('springbalance','assets/Class9/Spring_blance_front.png');
this.game.load.image('needle','assets/Class9/Spring_blance_needle-01.png');
this.game.load.image('hook','assets/Class9/Spring_blance_hook-01.png');
this.game.load.image('weight','assets/Class9/Weight_with_Tie_v1.png');
this.game.load.image('hookweight','assets/Class9/Spring_blance_hook_weight-01.png');
this.game.load.image('ring1','assets/Class9/Spring_blance-01_ring.png');
this.game.load.image('bg','assets/Class9/Bg.jpg');
//this.game.load.image('tubewithwater','assets/Class9/Measuring_tube_with_Water.png');
//this.game.load.image('measuring_tube','assets/Class9/Measuring_tube.png');
//this.game.load.image('measuring_tubeback','assets/Class9/Tube_back.png');
//this.game.load.image('water_bottom','assets/Class9/Water_bottom.png');
//this.game.load.image('water_middle','assets/Class9/Water_middle.png');
////this.game.load.image('water_top','assets/Class9/Water_top.png');
this.game.load.image('wieghttie','assets/Class9/Weight_with_Tie_v1-01.png');
this.game.load.image('Digital_weight','assets/Class9/Digital_weight.png');
this.game.load.image('Big_Beaker_without_water','assets/Class9/Big_Beaker_without_water.png');
this.game.load.image('Big_Beaker_water','assets/Class9/Big_Beaker_water.png');   
this.game.load.image('Water_to_Beaker0001','assets/Class9/Water_to_Beaker0001.png');   
this.game.load.image('Spoon_Salt', 'assets/Class9/Spoon_Salt.png');
this.game.load.image('Rod', 'assets/Class9/Rod.png');
this.game.load.image('Salt_in_Plate', 'assets/Class9/Salt_in_Plate.png');
this.game.load.image('Spring_blance_290', 'assets/Class9/Spring_blance_290.png');
this.game.load.image('Spring_blance_235', 'assets/Class9/Spring_blance_235.png');
this.game.load.image('Spring_blance_255', 'assets/Class9/Spring_blance_255.png');
this.game.load.image('Spring_blance_0', 'assets/Class9/Spring_blance_0.png');

this.game.load.image('Big_Beaker_Back','assets/Class9/Big_Beaker_Back.png');   
this.game.load.image('Small_beaker_back','assets/Class9/Small_beaker_back.png');
this.game.load.atlasJSONHash('smallbeaker', 'assets/Class9/smallbeaker.png', 'assets/Class9/smallbeaker.json'); //Standardization Buttons
this.game.load.atlasJSONHash('overflowwater', 'assets/Class9/overflowwater.png', 'assets/Class9/overflowwater.json'); //Standardization Buttons
this.game.load.atlasJSONHash('beakersaltwater', 'assets/Class9/beakersaltwater.png', 'assets/Class9/beakersaltwater.json'); //Standardization Buttons
this.game.load.atlasJSONHash('beakersaltwaterrod', 'assets/Class9/beakersaltwaterrod.png', 'assets/Class9/beakersaltwaterrod.json'); //Standardization Buttons
this.game.load.atlasJSONHash('spoonsalt', 'assets/Class9/spoonsalt.png', 'assets/Class9/spoonsalt.json'); //Standardization Buttons




////////////////////////////////////////////////////////////////////

  /////////////////////Experiment_A1_sounds////////////////////////////////////
        this.game.load.audio('A_aim','assets/audio/Experiment_A1/Aim.mp3');
        this.game.load.audio('A_precaution1','assets/audio/Experiment_A1/precaution_1.mp3');
    
        this.game.load.audio('A_procedure1','assets/audio/Experiment_A1/procedure_1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Experiment_A1/procedure_2.mp3');
        this.game.load.audio('A_procedure3','assets/audio/Experiment_A1/procedure_3.mp3');
        this.game.load.audio('A_exp_procedure1','assets/audio/Experiment_A1/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/Experiment_A1/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/Experiment_A1/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/Experiment_A1/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/Experiment_A1/A_exp_procedure5.mp3');
      
        this.game.load.audio('A_exp_procedure6','assets/audio/Experiment_A1/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/Experiment_A1/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/Experiment_A1/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/Experiment_A1/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/Experiment_A1/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/Experiment_A1/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/Experiment_A1/A_exp_procedure12.mp3');

   
      this.game.load.audio('lets_perform_Exp','assets/audio/Experiment_A1/lets_perform_Exp.mp3');

       this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
        this.game.load.audio('A_observation','assets/audio/Experiment_A1/A_observation.mp3');
        this.game.load.audio('A_observation1','assets/audio/Experiment_A1/A_observation1.mp3');
       
      
      },
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Exp_Selection");//Starting the gametitle state
 
 ///this.game.state.start("Experiment_A1");//Starting the gametitle state

   // this.game.state.start("Experiment_A2");


    this.game.state.start("Aim");//Simulation_hot1
   ///  this.game.state.start("Theory");//Simulation_hot1
     ///this.game.state.start("Lab_Precautions");
  //  this.game.state.start("Procedure");
  //this.game.state.start("Observations");//hot
  // this.game.state.start("Result");//Starting the gametitle state
    //this.game.state.start("Materials");//Starting the gametitle state
      //this.game.state.start("Viva");//Starting the gametitle state
	}
}

