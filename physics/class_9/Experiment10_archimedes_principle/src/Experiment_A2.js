var experiment_a2= function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  /////////////////////////////////////////////////////////
  var TestTube_1;
  var TestTube_2;
  var collider;
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var ratio;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  var fontStyle;
  //ip address
  var ip;
  
  
  }
  
  experiment_a2.prototype ={
  
  init: function( ipadrs) {
  
       ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
   {
  
    if(this.game.sound.mute)
    {
      this.game.sound.mute = false;
    }
  this.tookVal=0;
   muted = false;
   voice=this.game.add.audio("obj",1);
  this.startVal=50;
   this.dialogx=25;
   this.dialogy=60;
  
   
   bg= this.game.add.sprite(0, 0,'bg');
   //bg.scale.setTo(.241,.253);
  
    /*var maskBg1 = this.game.add.graphics(0, 0);
    maskBg1.lineStyle(10,0xff704d,1);
    maskBg1.beginFill(0xffffcc);
    maskBg1.drawRect(20, 40, 300, 1010);
    maskBg1.alpha=1;*/
  
    
  
  ////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////
  
  // Button panel backgroud-balck
  
    righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
    righttop_bg.scale.setTo(.5,.5);
    righttop_bg.angle=-90;
    
  // Button panel -Quit button
    
    quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
    quitButton.scale.setTo(.7,.7);
    quitButton.inputEnabled = true;
    quitButton.input.useHandCursor = true;
    quitButton.events.onInputDown.add(this.closeTheGame, this);
  
  // Button panel -Home button
    
    homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
    homeButton.scale.setTo(.7,.7);
    homeButton.inputEnabled = true;
    homeButton.input.useHandCursor = true;
    homeButton.events.onInputDown.add(this.gotoHome, this);
  
  // Button panel -Mute button
    
    muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
    muteButton.scale.setTo(.7,.7);
    muteButton.inputEnabled = true;
    muteButton.input.useHandCursor = true;
    muteButton.events.onInputDown.add(this.muteTheGame, this);
  
  // Button panel - Volume button
    
    volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
    volumeButton.scale.setTo(.7,.7);
    volumeButton.inputEnabled = true;
    volumeButton.input.useHandCursor = true;
    volumeButton.events.onInputDown.add(this.volume, this);
    volumeButton.visible=false;
  
    resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
    resetButton.scale.setTo(.7,.7);
    
    resetButton.events.onInputDown.add(this.resetTheGame, this);
    
  ///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////
  
  // Button panel backgroud-balck
    
    //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
    //leftbottom_bg.scale.setTo(2,2);
  
  ////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////
  
    // if (!this.game.device.desktop)
    // {
    this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    
    fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
    fullScreen1.scale.setTo(2.5,2.5);
    fullScreen1.inputEnabled = true;
    fullScreen1.input.useHandCursor = true;
    fullScreen1.events.onInputUp.add(this.gofull,this);    
    //}
  
    ////////////////////////////////////////Font///////////////////////
    fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
    headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
    labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
    labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
    /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
    interval=0;
    incr=0;
    currentobj=null;
    heatFlag=false;
    fNames=[];
    //this.addItems();
  
    
  
    /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
   
   play = this.game.add.sprite(1800,800,'components','play_pressed.png');
   play.scale.setTo(.7,.7);
   play.inputEnabled = true;
   //play.input.priorityID = 3;
   play.input.useHandCursor = true;
   play.events.onInputDown.add(this.toNextScene, this);
   play.visible=false;
  
    // Button panel group
  
    buttonGroup=this.game.add.group();
    buttonGroup.add(righttop_bg);
    buttonGroup.add(muteButton);
    buttonGroup.add(homeButton);
    buttonGroup.add(quitButton);
    buttonGroup.add(volumeButton);
    buttonGroup.add(resetButton);
    collider=null;
  
 
  //////////////////////////////Values///////////////////////////////
  fontStyle3={ font: "18px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
  fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};
  
  
  this.arrangeScenePourSalt();
    this.intDailog();
    
        
  
  
  
  ////////////////////////////////////////////////////////////////////
  
   },
   intDailog:function(){
  
      dialog_box=this.game.add.sprite(10,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      dialog_text="Lets perform the experiment.";
      dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
      voice=this.game.add.audio("lets_perform_Exp",1);
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.LoadExp,this);
     
   },
   LoadExp:function()
   {  
       voice.play();
       this.game.time.events.add(Phaser.Timer.SECOND*3,this.StartExperiment,this);
   },
   StartExperiment:function(){
    resetButton.inputEnabled = true;
    resetButton.input.useHandCursor = true;
    this.arrow1=this.game.add.sprite(0,0, 'arrow');
    this.arrow1.anchor.set(.5);
    this.arrow1.animations.add("frame1",[0])
    this.arrow1.animations.add("frameall")
    this.arrow1.visible=true;
    this.arrowShow=1;
    this.ArowShowing();
   this.ProcedureText(1);
   this.spoon.inputEnabled=true;
   this.spoon.input.useHandCursor = true;
 
   this.spoon.input.enableDrag(true);
   this.arrow2=this.game.add.sprite(1115,990, 'arrow');
   this.arrow2.anchor.set(.5);
   this.arrow2.animations.add("frame1",[0])
   this.arrow2.animations.add("frameall")
   this.arrow2.animations.play("frameall",20,true);
   this.arrow2.angle=90
   this.arrow2.visible=false;
  
   },
   ArowShowing:function(){
    this.arrow1.visible=true;
    console.log(this.arrowShow);
    this.arrow1.animations.play("frameall",20,true);
    switch(this.arrowShow){
      case 1:
        this.arrow1.x= this.spoon.x;
        this.arrow1.y= this.spoon.y-50;
        break;
      case 2:
        this.arrow1.x= this.beakerbig1.x;
        this.arrow1.y= this.beakerbig1.y-90;
        break;
        case 3:
          this.arrow1.x= this.rods.x;
          this.arrow1.y= this.rods.y-50;
          break;
        case 4:
          this.arrow1.x= this.beakerbig2.x;
          this.arrow1.y= this.beakerbig2.y-90;
          break;
      case 5:
        this.arrow1.x= this.dragweight.x;
        this.arrow1.y= this.dragweight.y-50;
        break;
        case 6:
        
          this.arrow1.x= this.springbalance.x;
          this.arrow1.y= this.springbalance.y+10;
          break;
          case 7:
        
            this.arrow1.x= this.dragwbeaker.x;
            this.arrow1.y= this.dragwbeaker.y-25;
            break;
            case 8:
        
              this.arrow1.x= this.wtbox.x;
              this.arrow1.y= this.wtbox.y-50;
              break;
         case 9:
        
            this.arrow1.x= this.clickStand.x+320;
            this.arrow1.y= this.clickStand.y;
            break;
    }
    },
   ProcedureText:function(vals){
      switch(vals){
        case 1:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure10",1);
          voice.play();
          dialog_text="";
          dialog.destroy();
          dialog_text="Pour salt to the beaker.";
          dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
          break;
          case 2:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure11",1);
          voice.play();
          dialog_text="";
          dialog.destroy();
          dialog_text="Stir the salt in beaker with rod.";
          dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
          break;
        case 3:
        voice.stop();
        voice=this.game.add.audio("A_exp_procedure1",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Hang the object from the hook of the spring balance.";
        dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
        break;
        case 4:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure2",1);
          voice.play();
          dialog_text="";
          dialog.destroy();
          dialog_text="Take the reading of the object.";
          dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
      break;
      case 5:
        voice.stop();
        voice=this.game.add.audio("A_exp_procedure3",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Place the beaker on the weigh machine. ";
        dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
        break;
      case 6:
        voice.stop();
        voice=this.game.add.audio("A_exp_procedure4",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Take the reading of the empty beaker.";
        dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
        break;
        case 7:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure5",1);
          voice.play();
          dialog_text="";
          dialog.destroy();
          dialog_text="Click on the spring balance to immersed the weight in the water.";
          dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
          break;
          case 8:
            voice.stop();
            voice=this.game.add.audio("A_exp_procedure6",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Wait till the water has stopped spilling out of the spout.";
            dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
            break;
            case 9:
              voice.stop();
              voice=this.game.add.audio("A_exp_procedure7",1);
              voice.play();
              dialog_text="";
              dialog.destroy();
              dialog_text="Now take the reading of the immersed object.";
              dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
              
              break;
              case 10:
                voice.stop();
                voice=this.game.add.audio("A_exp_procedure8",1);
                voice.play();
                dialog_text="";
                dialog.destroy();
                dialog_text="Take the reading of the beaker with water collected.";
                dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
               
                break;
                case 11:
                  voice.stop();
                  voice=this.game.add.audio("A_exp_procedure12",1);
                  voice.play();
                  dialog_text="";
                  dialog.destroy();
                  dialog_text="Now check the observation.";
                  dialog=this.game.add.text(this.dialogx,this.dialogy,dialog_text,fontStyle);
                  play.visible=true;
                  break;
                
      }
  
   },
   arrangeScenePourSalt:function(){
    this.groupsalt=this.game.add.group();
   this.beakerbigtop1=this.game.add.sprite(880,830, 'Big_Beaker_Back');
   this.beakerbigtop1.anchor.set(.5);
    this.beakerbigtop1.scale.set(.6);
   this.groupsalt.add(this.beakerbigtop1);
   this.beakerbig1=this.game.add.sprite(880,830, 'beakersaltwater');
   this.beakerbig1.anchor.set(.5);
    this.beakerbig1.scale.set(.6);
   this.beakerbig1.animations.add("frameidle",[0])
   this.beakerbig1.animations.add("frameall");
   this.beakerbig1.animations.play("frameidle",10,false);
   this.groupsalt.add(this.beakerbig1);

   this.beakerbig2=this.game.add.sprite(880,830, 'beakersaltwater');
   this.beakerbig2.anchor.set(.5);
   this.beakerbig2.scale.set(.6);

   this.beakerbig2.animations.add("frameall");
   this.groupsalt.add(this.beakerbig2);
  
   this.beakerbig2.visible=false;
   this.saltinplate=this.game.add.sprite(1200,865, 'Salt_in_Plate');
   this.saltinplate.anchor.set(.5);
   this.saltinplate.scale.set(.5);
   this.groupsalt.add(this.saltinplate);

   this.spoon=this.game.add.sprite(1250,820, 'spoonsalt');
   this.spoon.anchor.set(.5);
   this.spoon.scale.set(.5);
  
 
   this.spoon.xp= this.spoon.x;
   this.spoon.yp= this.spoon.y;
  this.spoon.animations.add('frameidle',[0]);
  this.spoon.animations.add('frameall');
  this.spoon.animations.play('frameidle',10,false)
   this.game.physics.arcade.enable(this.spoon);
   this.spoon.events.onDragStart.add(function() {this.onDragStart(this.spoon)}, this);
   this.spoon.events.onDragStop.add(function() {this.onDragStop(this.spoon)}, this);
   this.groupsalt.add(this.spoon);


   this.rods=this.game.add.sprite(1200,870, 'Rod');
   this.rods.anchor.set(.5);
   this.rods.scale.set(.7);
   this.rods.angle=90;
   this.groupsalt.add(this.rods);
   this.rods.xp= this.rods.x;
   this.rods.yp= this.rods.y;
   this.game.physics.arcade.enable(this.rods);
   this.rods.events.onDragStart.add(function() {this.onDragStart(this.rods)}, this);
   this.rods.events.onDragStop.add(function() {this.onDragStop(this.rods)}, this);

 
   this.colliderbeaker = this.game.add.sprite(775,700,'collider');
   this.colliderbeaker.scale.setTo(4,2);
   this.game.physics.arcade.enable(this.colliderbeaker);
 this.colliderbeaker.alpha=.0001
   this.colliderbeaker.enableBody=true;
   this.groupsalt.add(this.colliderbeaker);
   this.arrangeScene();
  },
   arrangeScene:function(){
    this.mainstand=this.game.add.group();
    this.stand=this.game.add.sprite(650,625, 'stand');
    this.stand.anchor.set(.5);
    this.stand.scale.setTo(.7,.7);
    this.mainstand.add(this.stand);
    this.standgroup=this.game.add.group();
    this.beakerbigtop=this.game.add.sprite(880,830, 'Big_Beaker_Back');
    this.beakerbigtop.anchor.set(.5);
    this.beakerbigtop.scale.set(.6); 
    this.mainstand.add(this.beakerbigtop);
    this.ring1=this.game.add.sprite(870,526, 'ring1');
    this.ring1.anchor.set(.5);
    this.standgroup.add(this.ring1);
  
    this.standtop=this.game.add.sprite(750,535, 'stand_top');
    this.standtop.anchor.set(.5);
    this.standtop.scale.setTo(.7,.7);
    this.standgroup.add(this.standtop);
    this.hook=this.game.add.sprite(870,525, 'hook');
    this.hook.anchor.set(.5);
    this.hook.scale.set(.7);
    this.standgroup.add(this.hook);
   this.weight=this.game.add.sprite(870,530, 'hookweight');
    this.weight.anchor.set(.5);
    this.weight.scale.set(.7);
    this.weight.ypos=540;
    this.standgroup.add(this.weight);
    this.blackbox=this.game.add.sprite(870,493, 'cols1');
    this.blackbox.anchor.set(.5);
    this.blackbox.scale.setTo(1,3);
    this.standgroup.add(this.blackbox);
    this.whitebox=this.game.add.sprite(870,493, 'collider');
    this.whitebox.anchor.set(.5);
    this.whitebox.scale.setTo(1,3.4);
    this.whitebox.alpha=.8
    this.standgroup.add(this.whitebox);
  
   // console.log(this.mask1.y);
    this.springbalance=this.game.add.sprite(870,471, 'springbalance');
    this.springbalance.anchor.set(.5);
    this.springbalance.scale.set(.7);
    this.standgroup.add(this.springbalance);
  
  
    
    this.mask1 = this.game.add.graphics(870, 493);
  
    //	Shapes drawn to the Graphics object must be filled.
    this.mask1.beginFill(0x338899);
  
    //	Here we'll draw a circle
  
    this.mask1.drawRect(-50,-150, 100, 500);
  //560
    this.mask1.y=560
  
    this.mask1.firstpos=645;
    this.mask1.secondpos=628;
    this.mask1.mainypos=this.mask1.y;
    this.standgroup.add(this.mask1);
   this.whitebox.mask=this.mask1;
  //60,149,153,157,161,166
    this.needle=this.game.add.sprite(0,20, 'needle');
    this.needle.anchor.set(.5);
    this.needle.mainypos=this.needle.y;
    this.springbalance.addChild(this.needle);
    this.weight.visible=false;
   /// this.standgroup.add(this.needle);
   this.needle.firstpos=145
   this.needle.secondpos=118
    this.arrow=this.game.add.sprite(30,this.needle.y-110, 'arrow');
    this.arrow.anchor.set(.5);
    this.arrow.tint="0x339900"
    this.arrow.animations.add('allframes');
    this.arrow.visible=false;
    this.arrow.angle=90
    this.springbalance.addChild(this.arrow);
    this.standgroup.y=-80
    this.mainstand.xpos= this.mainstand.x;
    this.standgroup.xpos= this.standgroup.x;
    this.BigBeakerFunc();
   },
   BigBeakerFunc:function(){
    this.overflowwater=this.game.add.sprite(830,835, 'overflowwater');
    this.overflowwater.anchor.set(.5);
    this.overflowwater.animations.add("frameall")
   
    this.overflowwater.visible=false;
    this.beakerbigwater=this.game.add.sprite(880,1013, 'Big_Beaker_water');
    this.beakerbigwater.anchor.setTo(.5,1);
    this.beakerbigwater.scale.set(.6);
    this.beakerbig=this.game.add.sprite(880,830, 'Big_Beaker_without_water');
    this.beakerbig.anchor.set(.5);
    this.beakerbig.scale.set(.6);

    this.wtbox=this.game.add.sprite(1050,970, 'Digital_weight');
    this.wtbox.anchor.set(.5);
    this.wtbox.scale.setTo(1.2,1.2);
    this.wtboxText=this.game.add.text(-13,8,"0:0",fontStyle3);
    this.wtbox.addChild(this.wtboxText);
    this.beakersmalltop=this.game.add.sprite(1012,890, 'Small_beaker_back');
    this.beakersmalltop.anchor.set(.5);
    this.beakersmalltop.scale.set(.8);
    this.beakersmall=this.game.add.sprite(0,0, 'smallbeaker');
    this.beakersmall.anchor.set(.5);
    this.beakersmall.animations.add("frameidle",[0])
    this.beakersmall.animations.add("frameall")
    this.beakersmall.animations.play("frameidle",false)
    //this.beakersmall.scale.set(.7);
    this.beakersmalltop.addChild( this.beakersmall);
    this.beakersmalltop.visible=false;
  
    this.dragwbeaker=this.game.add.sprite(1400,900, 'Small_beaker_back');
    this.dragwbeaker.anchor.set(.5);
    this.dragwbeaker.scale.set(.8);
    this.dragwbeaker.events.onDragStart.add(function() {this.onDragStart(this.dragwbeaker)}, this);
    this.dragwbeaker.events.onDragStop.add(function() {this.onDragStop(this.dragwbeaker)}, this);
    this.dragwbeaker.xposed=1400;
    this.dragwbeaker.xp= this.dragwbeaker.x;
    this.dragwbeaker.yp= this.dragwbeaker.y;
  
    this.game.physics.arcade.enable(this.dragwbeaker);
    this.dragwbeaker.enableBody =true;
    this.dragbeakersmall=this.game.add.sprite(0,0, 'Water_to_Beaker0001');
    this.dragbeakersmall.anchor.set(.5);
  
    this.dragwbeaker.addChild( this.dragbeakersmall);
    this.collidersmall = this.game.add.sprite(1000,870,'collider');
    this.collidersmall.scale.setTo(3,2);
    this.game.physics.arcade.enable(this.collidersmall);
    this.collidersmall.alpha=.0001
    this.collidersmall.enableBody=true;
  
  
    this.dragweight=this.game.add.sprite(1250,950, 'weight');
    this.dragweight.anchor.set(.5);
    this.dragweight.scale.set(.8);
    this.dragweight.events.onDragStart.add(function() {this.onDragStart(this.dragweight)}, this);
    this.dragweight.events.onDragStop.add(function() {this.onDragStop(this.dragweight)}, this);
    this.dragweight.xposed=1200;
    this.dragweight.xp= this.dragweight.x;
    this.dragweight.yp= this.dragweight.y;
  
    this.game.physics.arcade.enable(this.dragweight);
    this.dragweight.enableBody =true;
    this.collider = this.game.add.sprite(770,560,'collider');
    this.collider.scale.setTo(4,2);
    this.game.physics.arcade.enable(this.collider);
    this.collider.alpha=.0001
    this.collider.enableBody=true;
  
    this.clickStand = this.game.add.sprite(550,250,'collider');
    this.clickStand.scale.setTo(10,2);
   this.clickStand.alpha=.0001
    this.clickStand.events.onInputDown.add(this.ClickTomoveStand, this);
  
    this.ReadingBox();
    this.beakerbigtop1.visible=false;
    this.beakerbigwater.visible=false;
    this.beakerbig.visible=false;
   
   
   },
   ReadingBox:function(){
    this.valuetable1=this.game.add.sprite(75,528, 'valtable');
    this.valuetable1.anchor.set(.5);
    this.valuetable1.scale.setTo(.1,.59);
    this.valuetable=this.game.add.sprite(375,500, 'valtable');
    this.valuetable.anchor.set(.5);
    this.valuetable.scale.setTo(.5,.8);
  
  
     this.obsline1=this.game.add.sprite(5,-60,'obsline');
    this.obsline1.anchor.set(.5);
    this.obsline1.scale.setTo(1,1.24);
    this.obsline1.angle=90
    this.valuetable.addChild(this.obsline1);
  
    this.obsline2=this.game.add.sprite(-10,-5,'obsline');
    this.obsline2.anchor.set(.5);
    this.obsline2.scale.setTo(1,.33);
    this.valuetable.addChild(this.obsline2);
  
    this.obsline3=this.game.add.sprite(-351,30,'obsline');
    this.obsline3.anchor.set(.5);
    this.obsline3.scale.setTo(1,.86);
    this.obsline3.angle=90
    this.valuetable.addChild(this.obsline3);
  
    this.wttexts1=this.game.add.text(-640,-35,"In air",fontStyle);
    this.valuetable.addChild(this.wttexts1);
    this.wttexts2=this.game.add.text(-660,50,"In water",fontStyle);
    this.valuetable.addChild(this.wttexts2);
  //  this.wttext=this.game.add.text(100,100,"Weight of the Object",fontStyle);
    this.wttext=this.game.add.text(-425,-122,"Weight of object (gf)",fontStyle);
    this.valuetable.addChild(this.wttext);
  
    this.wttext11=this.game.add.text(65,-122,"Weight of beaker (gf)",fontStyle);
    this.valuetable.addChild(this.wttext11);
    this.wttext1=this.game.add.text(-360,-40,"W\u2081"+"   -  "+"290",fontStyle);
    this.valuetable.addChild(this.wttext1);
    this.wttext2=this.game.add.text(150,-40,"W\u2083"+"   -  "+"50",fontStyle);
    this.valuetable.addChild(this.wttext2);
    this.wttext3=this.game.add.text(-360,55,"W\u2082"+"   -  "+"235",fontStyle);
    this.valuetable.addChild(this.wttext3);
    this.wttext4=this.game.add.text(150,55,"W\u2084"+"   -  "+"105",fontStyle);
    this.valuetable.addChild(this.wttext4);
    this.wttext1.visible=false;
    this.wttext2.visible=false;
    this.wttext3.visible=false;
    this.wttext4.visible=false;
    
  },
 
   ClickTomoveStand:function(){
    this.game.time.events.add(500,this.waterUp,this);
    this.arrow1.visible=false;
    var tweens2=this.game.add.tween(this.standgroup).to({y:this.standgroup.y+230}, 500, Phaser.Easing.Linear.Out, true);
    tweens2.onComplete.add(function () {
      this.ProcedureText(8);
      this.overflowwater.visible=true;
      this.overflowwater.animations.play("frameall",10,false)
      this.game.time.events.add(300,this.Flowwater,this);
    
  }.bind(this))
  },
  waterUp:function(){
    var tweens1=this.game.add.tween(this.beakerbigwater.scale).to({y:.63}, 200, Phaser.Easing.Linear.Out, true);
    tweens1.onComplete.add(function () {
      var tweens11=this.game.add.tween(this.beakerbigwater.scale).to({y:.6}, 1000, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.needle).to({x:this.needle.x,y:this.needle.secondpos}, 600, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.mask1).to({x:this.mask1.x,y:this.mask1.secondpos}, 600, Phaser.Easing.Linear.Out, true);
     
  }.bind(this))
  },
  Flowwater:function(){
    this.overflowwater.visible=false;
    this.beakersmall.animations.play("frameall",10,false)
  
    this.game.time.events.add(300,this.changeTextinWt,this);
  },
  changeTextinWt:function(){
    this.startVal+=1;
    this.wtboxText.text=""+this.startVal;
    if(this.startVal!=105){
      this.game.time.events.add(50,this.changeTextinWt,this);
  
    }else{
      this.game.time.events.add(1000,this.TakeReading2,this);
    }
  
  },
  
  onDragStart:function(obj)
    {
    
      //obj.angle=0;
      obj.body.enable =false;
      currentobj=obj;
      this.arrowShow+=1;
      this.ArowShowing();
    },
    onDragStop:function(obj)
    {
      obj.body.enable =true;
      
    },
  // For Full screen checking.
    
        gofull: function()
        {
  
        if (this.game.scale.isFullScreen)
          {
           this.game.scale.stopFullScreen();
          }
        else
          {
          this.game.scale.startFullScreen(false);
          }  
        },
        
      
        GoBackSpoon:function(){
          this.spoon.reset(this.spoon.xp,this.spoon.yp);//
          this.rods.inputEnabled=true;
          this.rods.input.useHandCursor = true;
          this.rods.input.enableDrag(true);
          this.colliderbeaker.visible=true;
          this.arrowShow=3;
          this.ArowShowing();
          this.ProcedureText(2);
        },
        GotoSpringBalance:function(){
          this.rods.visible=true;
          this.rods.reset(this.rods.xp,this.rods.yp);//
          this.beakerbigtop1.visible=false;
          this.beakerbig2.visible=false;
         // this.arrangeScene();
        //  this.game.time.events.add(1000,this.enableSpringBalancealpha,this);
         /// var tweens2=this.game.add.tween( this.groupsalt).to({alpha:0}, 500, Phaser.Easing.Linear.Out, true);
          this.dragweight.inputEnabled=true;
          this.dragweight.input.useHandCursor = true;
        
          this.dragweight.input.enableDrag(true);
          this.beakerbigtop1.visible=true;
          this.beakerbigwater.visible=true;
          this.beakerbig.visible=true;
          this.arrowShow=5;
          this.ArowShowing();
          this.ProcedureText(3);
        },
    match_Obj:function(){
      
      currentobj.inputEnabled=false;
      currentobj.input.enableDrag(false);
      currentobj.body.enable=false;
     
      this.arrow1.visible=false;
      this.arrow1.animations.add("frame1",10,false)
      if( currentobj==this.spoon){
        this.spoon.x=this.beakerbig1.x+55;
        this.spoon.y=this.beakerbig1.y-135;
        this.spoon.animations.play("frameall",10,false);
        var anim= this.beakerbig1.animations.add("frameall");
        this.beakerbig1.animations.play("frameall",10,false);
        this.colliderbeaker.visible=false;
        anim.onComplete.add(function () {
          this.GoBackSpoon();
         }.bind(this));
     
      //  currentobj.reset(currentobj.xp,currentobj.yp);//
      }
       if( currentobj==this.rods){
        this.beakerbig1.visible=false;
        this.beakerbig2.visible=true;
        this.rods.visible=false;
        this.beakerbig2.loadTexture("beakersaltwaterrod");
        var anim=this.beakerbig2.animations.add('frameall');
        this.beakerbig2.animations.play("frameall",10,false);
        this.colliderbeaker.visible=false;
        anim.onComplete.add(function () {
         this.GotoSpringBalance();
        }.bind(this));
      //  currentobj.reset(currentobj.xp,currentobj.yp);//
      }
      if( currentobj==this.dragweight){
      this.collider.visible=false;
      this.dragweight.visible=false;
      var tweens2=this.game.add.tween(this.needle).to({x:this.needle.x,y:this.needle.firstpos}, 600, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.mask1).to({x:this.mask1.x,y:this.mask1.firstpos}, 600, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.weight).to({x:this.weight.x,y:this.weight.ypos}, 500, Phaser.Easing.Elastic.Out, true);
      this.hook.visible=false;
      this.weight.visible=true;
     
    tweens2.onComplete.add(function () {
      this.game.time.events.add(1000,this.TakeReadingScale1,this);
     
  }.bind(this))
   
    //this.TakeReading()
      }
      if( currentobj==this.dragwbeaker){
        this.collidersmall.visible=false;
        this.dragwbeaker.visible=false;
        this.wtboxText.text=""+this.startVal;
        this.beakersmalltop.visible=true;
        this.game.time.events.add(1000,this.TakeBeakerReading1,this);
        }
    currentobj=null;
     
    },
    TakeReadingScale1:function(){
      this.ProcedureText(4);
      this.circleRound = this.game.add.graphics(870, 423);
      this.circleRound.beginFill(0xFFFFFF);
      this.circleRound.drawCircle(0, 100, 310);
      this.circleRound.scale.set(.5)
      this.springbalance1=this.game.add.sprite(870,500, 'Spring_blance_290');
      this.springbalance1.anchor.set(.5);
      this.springbalance1.scale.set(.5);
      this.mask2 = this.game.add.graphics(870, 423);
      this.mask2.beginFill(0x338899);
      this.mask2.drawCircle(0, 100, 310);
      var tweens1=this.game.add.tween(this.springbalance1.scale).to({x:2,y:2}, 200, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.circleRound.scale).to({x:1,y:1}, 200, Phaser.Easing.Linear.Out, true);
      this.springbalance1.mask=this.mask2;
      this.game.time.events.add(3000,this.ShowReading1,this);
      this.game.time.events.add(5000,this.DeleteReadingScale,this);
    },
    ShowReading1:function(){
      this.wttext1.visible=true;
    },
    DeleteReadingScale:function(){
      this.wttext1.visible=true;
      var tweens2=this.game.add.tween(this.springbalance1.scale).to({x:0,y:0}, 200, Phaser.Easing.Linear.Out, true);
      var tweens21=this.game.add.tween(this.circleRound.scale).to({x:0,y:0}, 200, Phaser.Easing.Linear.Out, true);
      tweens2.onComplete.add(function () {
        this.circleRound.destroy();
      this.springbalance1.destroy();
      this.mask2.destroy();
      this.arrowShow=7;
      this.ArowShowing();
      this.ProcedureText(5);
      
      this.dragwbeaker.inputEnabled = true;
      this.dragwbeaker.input.useHandCursor = true;
      this.dragwbeaker.input.enableDrag(true);
    }.bind(this))
     
    },
    TakeReading2:function(){
      this.ProcedureText(9);
      this.circleRound = this.game.add.graphics(870, 473);
      this.circleRound.beginFill(0xFFFFFF);
      this.circleRound.drawCircle(0, 100, 310);
      this.circleRound.scale.set(.5)
      this.springbalance1=this.game.add.sprite(870,550, 'Spring_blance_235');
      this.springbalance1.anchor.set(.5);
      this.springbalance1.scale.set(.5);
      this.mask2 = this.game.add.graphics(870, 473);
      this.mask2.beginFill(0x338899);
      this.mask2.drawCircle(0, 100, 310);
      var tweens1=this.game.add.tween(this.springbalance1.scale).to({x:2,y:2}, 200, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.circleRound.scale).to({x:1,y:1}, 200, Phaser.Easing.Linear.Out, true);
      this.springbalance1.mask=this.mask2;
      this.game.time.events.add(3000,this.ShowReading2,this);
      this.game.time.events.add(5000,this.DeleteReadingScale2,this);
    },
    ShowReading2:function(){
      this.wttext3.visible=true;
    },
    DeleteReadingScale2:function(){
      this.wttext3.visible=true;
      var tweens2=this.game.add.tween(this.springbalance1.scale).to({x:0,y:0}, 200, Phaser.Easing.Linear.Out, true);
      var tweens21=this.game.add.tween(this.circleRound.scale).to({x:0,y:0}, 200, Phaser.Easing.Linear.Out, true);
      tweens2.onComplete.add(function () {
        this.circleRound.destroy();
      this.springbalance1.destroy();
      this.mask2.destroy();
      this.game.time.events.add(2000,this.TakeBeakerReading2,this);
    
    }.bind(this))
  },
    TakeBeakerReading1:function(){
      this.ProcedureText(6);
      this.arrow2.visible=true;
      this.wttext2.visible=true;
      this.game.time.events.add(4000,this.deleteReading1,this);
    },
    deleteReading1:function(){
      this.arrowShow=9;
      this.ArowShowing();
      this.ProcedureText(7);
      this.clickStand.inputEnabled = true;
      this.clickStand.input.useHandCursor = true;
      this.beakersmalltop.visible=true;
      this.arrow2.visible=false;
     // this.game.time.events.add(500,this.deleteReading,this);
    },
    TakeBeakerReading2:function(){
      this.ProcedureText(10);
      this.arrow2.visible=true;
      this.wttext4.visible=true;
      this.game.time.events.add(4000,this.deleteReading2,this);
    },
    deleteReading2:function(){
      this.ProcedureText(11);
      this.arrow2.visible=false;
    
    },
    update: function()
    {
   
      
      this.detectCollision();
    
    },
   
    detectCollision:function(){
     
  
       if(this.collider!=null&& this.collider.enableBody && currentobj!=null)
       {
           this.game.physics.arcade.overlap(this.dragweight, this.collider,function() {this.match_Obj()},null,this);
          // collider=collider_battery;
       } 
  
       if(this.collidersmall!=null&& this.collidersmall.enableBody && currentobj!=null)
       {
           this.game.physics.arcade.overlap(this.dragwbeaker, this.collidersmall,function() {this.match_Obj()},null,this);
          // collider=collider_battery;
       } 
       if(this.colliderbeaker!=null&& this.colliderbeaker.enableBody && currentobj!=null)
       {
           this.game.physics.arcade.overlap(this.spoon, this.colliderbeaker,function() {this.match_Obj()},null,this);
          // collider=collider_battery;
       } 
       if(this.colliderbeaker!=null&& this.colliderbeaker.enableBody && currentobj!=null)
       {
           this.game.physics.arcade.overlap(this.rods, this.colliderbeaker,function() {this.match_Obj()},null,this);
          // collider=collider_battery;
       } 
       if(currentobj!=null && currentobj.body.enable)
       {
         console.log("dddddddddddddd");
        currentobj.reset(currentobj.xp,currentobj.yp);//
        this.arrowShow-=1;
        this.ArowShowing();
       
           currentobj=null;
        }
      
    },
  //For to next scene
   
        toNextScene:function()
        {
        voice.destroy();
  
        this.state.start("Observations", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
  
        },
  
  // For unmute audio
  
        volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
        gotoHome:function()
        {
          voice.destroy();
          
          this.state.start("Aim", true, false, ip);
        },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  //To reset the game
  resetTheGame:function(){
    voice.destroy();
    
      this.state.start("Experiment_A2",true,false);
   },
   // To quit the experiment
  
  closeTheGame:function()
  {
    voice.destroy();
    
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  