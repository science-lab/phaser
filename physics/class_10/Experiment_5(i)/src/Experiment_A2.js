var experiment_a2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var HCl_step;
var colorChangingAnimstart;
var TostartTesttubeB;
var  animationCounter;

var isRheostatMoving;
var rHead_minimuval;
var ammeter_needle_initial_angle;
var voltmeter_needle_initial_angle;
var resistor_resistance;

// var voltage_val1;
// var voltage_val2;
// var voltage_val3;

var voltage_val_final1;
var voltage_val_final2;
var voltage_val_final3;

// var current_val1;
// var current_val2;
// var current_val3;

var current_val_final1;
var current_val_final2;
var current_val_final3;


var resistance_val1;
var resistance_val2;
var resistance_val3;

var resistance_val_final1;
var resistance_val_final2;
var resistance_val_final3;

var isR1Value1_taken;
var isR1Value2_taken;
var isR1Value3_taken;
var isR2Value1_taken;
var isR2Value2_taken;
var isR2Value3_taken;
var isR1Values_taken;
var isR2Values_taken;
var isR3Value1_taken;
var isR3Value2_taken;
var isR3Value3_taken;



var voltmeter_reading;
var ammeter_reading;
var rheostat_resistance;
var rheostat_head_initial_val;
var pointer_currentx;
var pointer_newx;
var power_State;
var rheostat_Values;
var R1ammeter_Values;
var R1voltmeter_Values;
var R1voltmeter_angles;
var R1ammeter_angles;
var R2ammeter_Values;
var R2voltmeter_Values;
var R2voltmeter_angles;
var R2ammeter_angles;
var Needle_Position;
var am_Val
var vm_Val
}

experiment_a2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
//  voice1=this.game.add.audio("A_exp_procedure1",1);
//  voice2=this.game.add.audio("A_exp_procedure2",1);
//  voice3=this.game.add.audio("A_exp_procedure3",1);
//  voice4=this.game.add.audio("A_exp_procedure4",1);
//  voice5=this.game.add.audio("A_exp_procedure5",1);
//  voice6=this.game.add.audio("A_exp_procedure6",1);
//  voice7=this.game.add.audio("A_exp_procedure7",1);
//  voice8=this.game.add.audio("A_exp_procedure8",1);
//  voice9=this.game.add.audio("A_exp_procedure9",1);
//  voice10=this.game.add.audio("A_exp_procedure10",1);
//  voice11=this.game.add.audio("A_exp_procedure11",1);
//  voice12=this.game.add.audio("A_exp_procedure12",1);
//  voice13=this.game.add.audio("A_exp_procedure13",1);
//  voice14=this.game.add.audio("A_exp_procedure14",1);
 //voice1=this.game.add.audio("obj",1);
 
 this.arrangeScene();
 isRheostatMoving=false;
 rHead_minimuval=1760;
 ammeter_needle_initial_angle=285;
 voltmeter_needle_initial_angle=285;
 //resistor_resistance=6;
 rheostat_resistance=10;
 rheostat_head_initial_val=1730;
 pointer_currentx=0; 
 pointer_newx=0;
 
 isR3Value1_taken=false;
 isR3Value2_taken=false;
 isR3Value3_taken=false;
//  isR2Value1_taken=false;
//  isR2Value2_taken=false;
//  isR2Value3_taken=false;
//  isR3Value3_taken
//  isR1Values_taken=false;
//  isR2Values_taken=false;
 

 am_Val=1.2;
 vm_Val=4.8;
 animationCounter=0;
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, -50,'bg');
 bg.scale.setTo(.241,.253);

  var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.lineStyle(10,0xff704d,1);
  maskBg1.beginFill(0xffffcc);
  maskBg1.drawRect(20, 40, 300, 1010);
  maskBg1.alpha=1;

  



  rheostat_Values=[1,2,3,4,5,6,7,8,9,10];
  R1ammeter_angles=[45,40,37,34,32,30,28,27,26,25];
  R1ammeter_Values=[1.2,1,0.85,0.75,0.66,0.6,0.54,0.5,0.46,0.42];
  R1voltmeter_angles=[160,135,117,105,94,87,80,75,70,65];
  R1voltmeter_Values=[4.8,4,3.4,3,2.64,2.4,2.16,2,1.84,1.68];

  // R2ammeter_angles=[98,90,84,80,75,73,68,65,63,60];
  // R2ammeter_Values=[.55,.5,.46,.43,.4,.38,.35,.33,.32,.3];
  // R2voltmeter_Values=[5.5,5,4.6,4.3,4,3.8,3.5,3.3,3.2,3];

  power_State="switch_off";
  ratio=5.5;
 HCl_step=0;
 colorChangingAnimstart=false;
 delay=0;
 TostartTesttubeB=false;
 Needle_Position_am=-1;
 Needle_Position_vm=-1;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  dialog_box=this.game.add.sprite(340,20, 'dialogue_box1');
      dialog_box.scale.setTo(.64,.8);
      dialog_text="Lets perform the experiment";
      
      //procedure_voice1.play();
      dialog=this.game.add.text(360,60,dialog_text,fontStyle);
      

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

ammeter_value1_text="I1 = ";
ammeter_value1=this.game.add.text(60,140,ammeter_value1_text,fontStyle3);
voltmeter_value1_text="V1 = ";
voltmeter_value1=this.game.add.text(50,195,voltmeter_value1_text,fontStyle3);


voltmeter_value1.visible=false;
ammeter_value1.visible=false;

// voltmeter_value1.visible=true;
// ammeter_value1.visible=true;

ammeter_value2_text="I2 = ";
ammeter_value2=this.game.add.text(60,280,ammeter_value2_text,fontStyle3);
voltmeter_value2_text="V2 = ";
voltmeter_value2=this.game.add.text(50,335,voltmeter_value2_text,fontStyle3);


voltmeter_value2.visible=false;
ammeter_value2.visible=false;

// voltmeter_value2.visible=true;
// ammeter_value2.visible=true;

ammeter_value3_text="I3 = ";
ammeter_value3=this.game.add.text(60,420,ammeter_value3_text,fontStyle3);
voltmeter_value3_text="V3 = ";
voltmeter_value3=this.game.add.text(50,475,voltmeter_value3_text,fontStyle3);


voltmeter_value3.visible=false;
ammeter_value3.visible=false;

// voltmeter_value3.visible=true;
// ammeter_value3.visible=true;

// ammeter_value4_text="I4 = ";
// ammeter_value4=this.game.add.text(60,620,ammeter_value4_text,fontStyle3);
// voltmeter_value4_text="V4 = ";
// voltmeter_value4=this.game.add.text(50,675,voltmeter_value4_text,fontStyle3);


// voltmeter_value4.visible=false;
// ammeter_value4.visible=false;

// voltmeter_value4.visible=true;
// ammeter_value4.visible=true;

// ammeter_value5_text="I5 = ";
// ammeter_value5=this.game.add.text(60,760,ammeter_value5_text,fontStyle3);
// voltmeter_value5_text="V5 = ";
// voltmeter_value5=this.game.add.text(50,815,voltmeter_value5_text,fontStyle3);


// voltmeter_value5.visible=false;
// ammeter_value5.visible=false;

// voltmeter_value5.visible=true;
// ammeter_value5.visible=true;

// ammeter_value6_text="I6 = ";
// ammeter_value6=this.game.add.text(60,900,ammeter_value6_text,fontStyle3);
// voltmeter_value6_text="V6 = ";
// voltmeter_value6=this.game.add.text(50,955,voltmeter_value6_text,fontStyle3);

value_heading1=this.game.add.text(110,60,"R1 + R2",fontStyle3);
value_heading1.visible=false;
// value_heading2=this.game.add.text(140,540,"R2",fontStyle3);
// value_heading2.visible=false;


// voltmeter_value6.visible=false;
// ammeter_value6.visible=false;

// voltmeter_value4.visible=true;
// ammeter_value4.visible=true;



//sample_button_text="Take value";
button_sample=this.game.add.sprite(1100,940,'take_reading_btn');
button_sample.inputEnabled = true;
//play.input.priorityID = 3;
button_sample.input.useHandCursor = true;
button_sample.events.onInputDown.add(this.takeValue, this);
button_sample.visible=false;
button_sample.scale.setTo(2,2);

////////////////////////////////////////////////////////////////////

 },

 takeValue:function()
 {

  // if(voltage_val1==vm_Val&&current_val1==am_Val||voltage_val2==vm_Val&&current_val2==am_Val||voltage_val3==vm_Val&&current_val3==am_Val)
  // {

  //   dialog.text="Please take different values";
  // }
  arrow.visible=false;
  
  if(isR3Value1_taken==false)
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure21",1);
    voice.play();
    R3voltage_val1=vm_Val;
    R3current_val1=am_Val;
    console.log("voltage1="+R3voltage_val1+"current1="+R3current_val1);
    //voltmeter_value1.text="V1 = "+voltage_val1+"V";
    R3voltmeter_val1=this.game.add.text(150,195,R3voltage_val1+"V",fontStyle4);
    //ammeter_value1.text="I1 = "+current_val1+"A";
    R3ammeter_val1=this.game.add.text(150,140,R3current_val1+"A",fontStyle4);
    voltmeter_value2.visible=true;
    ammeter_value2.visible=true;
    dialog.text="Take the second reading";
     isR3Value1_taken=true;

  }
  else if(isR3Value2_taken==false)
  {
    if(vm_Val==R3voltage_val1&&am_Val==R3current_val1)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure12",1);
      voice.play();
      voltmeter_value3.visible=true;
     ammeter_value3.visible=true;
        R3voltage_val2=vm_Val;
        R3current_val2=am_Val;
        console.log("voltage2="+R3voltage_val2+"current2="+R3current_val2);
        //voltmeter_value2.text="V2 = "+voltage_val2+"V";
        R3voltmeter_val2=this.game.add.text(150,335,R3voltage_val2+"V",fontStyle4);
        //ammeter_value2.text="I2 = "+current_val2+"A";
        R3ammeter_val2=this.game.add.text(150,280,R3current_val2+"A",fontStyle4);
        dialog.text="Take the third reading";

        isR3Value2_taken=true;

    }
    
  }

  else if(isR3Value3_taken==false)
  {
    if(vm_Val==R3voltage_val2&&am_Val==R3current_val2)
    {
      
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure24",1);
      voice.play();
      
        //voltmeter_value4.visible=true;
        //ammeter_value4.visible=true;
        R3voltage_val3=vm_Val;
        R3current_val3=am_Val;
        console.log("voltage2="+R3voltage_val3+"current2="+R3current_val3);
        //voltmeter_value3.text="V3 = "+voltage_val3+"V";
        R3voltmeter_value3=this.game.add.text(150,475,+R3voltage_val3+"V",fontStyle4);
        //ammeter_value3.text="I3 = "+current_val3+"A";
        R3ammeter_value3=this.game.add.text(150,420,R3current_val3+"A",fontStyle4);
        dialog.text="You have successfully taken three readings. Click on the\nnext button to see the observations. ";
        button_sample.visible=false;
        play.visible=true;
   }


  }

 },

 onClickOnKey:function()
 {
  // arrow.visible=false;
  // key.angle=45;
  //  key.x=1670;
  //  key.y=750;
  //  key.enableBody=false;
  //       key.input.enableDrag(false);
  //       collider_key.visible=false;
   
  // currentobj=key;
  // currentobj.reset(currentobj.xp,currentobj.yp);

 },
arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
  // circuit= this.game.add.sprite(320,0,'circuit');
  // circuit.scale.setTo(.4,.5);

//////////colliders//////////
 collider_battery = this.game.add.sprite(500,310,'collider');
 collider_battery.scale.setTo(8,3);
 collider_battery.alpha=1;
 collider_battery.visible=false;
 this.game.physics.arcade.enable(collider_battery);//For colling purpose
 collider_battery.inputEnabled = true;
 collider_battery.enableBody =true;

 collider_rheostat = this.game.add.sprite(1410,230,'collider');
 collider_rheostat.scale.setTo(8,3);
 collider_rheostat.alpha=1;
 collider_rheostat.visible=false;
 this.game.physics.arcade.enable(collider_rheostat);//For colling purpose
 collider_rheostat.inputEnabled = true;
 collider_rheostat.enableBody =true;

 collider_key = this.game.add.sprite(1560,600,'collider');
 collider_key.scale.setTo(5,3);
 collider_key.alpha=0;
 collider_key.visible=false;
 this.game.physics.arcade.enable(collider_key);//For colling purpose
 collider_key.inputEnabled = true;
 collider_key.enableBody =true;

 collider_ammeter = this.game.add.sprite(955,520,'collider');
 collider_ammeter.scale.setTo(8,5);
 collider_ammeter.alpha=1;
 collider_ammeter.visible=false;
 this.game.physics.arcade.enable(collider_ammeter);//For colling purpose
 collider_ammeter.inputEnabled = true;
 collider_ammeter.enableBody =true;

 collider_resistor = this.game.add.sprite(460,550,'collider');
 collider_resistor.scale.setTo(3,3);
 collider_resistor.alpha=0;
 collider_resistor.visible=true;
 this.game.physics.arcade.enable(collider_resistor);//For colling purpose
 collider_resistor.inputEnabled = true;
 collider_resistor.enableBody =true;

 collider_resistor2 = this.game.add.sprite(730,550,'collider');
 collider_resistor2.scale.setTo(3,3);
 collider_resistor2.alpha=0;
 collider_resistor2.visible=true;
 this.game.physics.arcade.enable(collider_resistor2);//For colling purpose
 collider_resistor2.inputEnabled = true;
 collider_resistor2.enableBody =true;

 collider_voltmeter = this.game.add.sprite(460,800,'collider');
 collider_voltmeter.scale.setTo(8,5);
 collider_voltmeter.alpha=1;
 collider_voltmeter.visible=false;
 this.game.physics.arcade.enable(collider_voltmeter);//For colling purpose
 collider_voltmeter.inputEnabled = true;
 collider_voltmeter.enableBody =true;

 
 ////////////////////////////////////////equipments//////////////////////////////////
 
 battery = this.game.add.sprite(450,324,'battery');
 battery.scale.setTo(.2,.25);
 battery.alpha=0;

 voltmeter = this.game.add.sprite(500,705,'voltmeter');
 voltmeter.scale.setTo(.25,.25);
 voltmeter.alpha=0;

 voltmeter_needle=this.game.add.sprite(692,871,'needle');
 voltmeter_needle.scale.setTo(.24,.27);
 voltmeter_needle.alpha=0;
 voltmeter_needle.anchor.setTo(.9,.5)
 voltmeter_needle.angle=69;//-75;

 ammeter = this.game.add.sprite(985,415,'ammeter');
 ammeter.scale.setTo(.25,.25);
 ammeter.alpha=0;

//  ammeter_needle=this.game.add.sprite(1176,582,'needle');
//  ammeter_needle.scale.setTo(.24,.27);
//  ammeter_needle.alpha=1;
//  ammeter_needle.anchor.setTo(.5,.9)
//  ammeter_needle.angle=285;
 ammeter_needle=this.game.add.sprite(1176,581,'needle');
 ammeter_needle.scale.setTo(.24,.27);
 ammeter_needle.alpha=0;
 ammeter_needle.anchor.setTo(.9,.5);
 ammeter_needle.angle=15;

 keySet = this.game.add.sprite(1559,600,'keySet');
 keySet.scale.setTo(.2,.25);
 keySet.alpha=0;

//  key = this.game.add.sprite(1670,575,'key');//key inserted diamension
//  key.scale.setTo(.2,.25);

 key = this.game.add.sprite(1670,750,'key');
 key.angle=45;
 key.scale.setTo(.2,.25);
 key.alpha=0;
 //key.visible=false;
 

 rheostat = this.game.add.sprite(1360,225,'rheostat');
 rheostat.scale.setTo(.18,.25);
 rheostat.alpha=0;

//  var line1=this.game.add.graphics(0, 0);
//  //line1.lineStyle(7,0xe5e5e5,1);
//  line1.moveTo(710, 520);
//  line1.lineTo(890, 520);
//  this.drawVLine(1460,380,"1");//0
//  this.drawVLine(1490,380,"2");//35
//  this.drawVLine(1520,380,"3");//70
//  this.drawVLine(1550,380,"4");//105
//  this.drawVLine(1580,380,"5");//140
//  this.drawVLine(1610,380,"6");//175
//  this.drawVLine(1640,380,"7");//210
//  this.drawVLine(1670,380,"8");//245
//  this.drawVLine(1700,380,"9");//280
//  this.drawVLine(1730,380,"10");//315
 
 


 rheostat_head = this.game.add.sprite(1460,295,'rheostat_top');
 rheostat_head.scale.setTo(.20,.25);
 rheostat_head.anchor.setTo(.5,1);
 rheostat_head.alpha=0;
//  rheostat_head.inputEnabled=true;
//  rheostat_head.input.useHandCursor=true;
//  rheostat_head.events.onInputDown.add(this.rheostat_head_move,this);
//  rheostat_head.events.onInputUp.add(this.rheostat_head_stop,this);

 resistor = this.game.add.sprite(450,595,'nichrome_wire');
 resistor.scale.setTo(.20,.25);
 resistor.alpha=0;

 resistor2 = this.game.add.sprite(700,593,'nichrome_wire2');
 resistor2.scale.setTo(.20,.25);
 resistor2.alpha=0;

 wire1= this.game.add.sprite(368,367,'wire8');
 wire1.scale.setTo(.25,.25);
 wire1.visible=false;
 //wire1.visible=true;

 wire2 = this.game.add.sprite(420,640,'wire10');
 wire2.scale.setTo(.22,.26);
 wire2.visible=false;
//wire2.visible=true;

 wire3 = this.game.add.sprite(835,640,'wire11');
 wire3.scale.setTo(.24,.26);
 wire3.visible=false;
//wire3.visible=true;

 wire4 = this.game.add.sprite(895, 636,'wire4');
 wire4.scale.setTo(.102,.25);
 wire4.visible=false;
//wire4.visible=true;

 wire5 = this.game.add.sprite(1320, 636,'wire5');
 wire5.scale.setTo(.164,.25);
wire5.visible=false;
//wire5.visible=true;

 wire6 = this.game.add.sprite(1360, 205,'wire6');
 wire6.scale.setTo(.20,.25);
  wire6.visible=false;
//wire6.visible=true;

 wire7 = this.game.add.sprite(932,368,'wire7');
 wire7.scale.setTo(.20,.25);
 //wire7.alpha=1;
  wire7.visible=false;
  //wire7.visible=true;

// wire8 = this.game.add.sprite(932,368,'wire8');
// wire8.scale.setTo(.20,.25);
// //wire7.alpha=0;
//  wire7.visible=false;
// //wire8.visible=true;

wire9 = this.game.add.sprite(620,636,'wire9');
wire9.scale.setTo(.45,.25);
//wire7.alpha=0;
  wire9.visible=false;
//wire9.visible=true;
//////////////////////////////equipments to drag//////////////////////////////

 temp_battery = this.game.add.sprite(65,80,'battery');
 temp_battery.scale.setTo(.08,.15);
//  temp_battery.events.onDragStart.add(function() {this.onDragStart(temp_battery)}, this);
//  temp_battery.events.onDragStop.add(function() {this.onDragStop(temp_battery)}, this);
//  temp_battery.inputEnabled = true;
//  temp_battery.input.useHandCursor = true;
//  temp_battery.input.enableDrag(true);
//  this.game.physics.arcade.enable(temp_battery);
//  temp_battery.enableBody =true;
 //temp_battery.alpha=.5;
//  temp_battery.visible=false;
 temp_battery.visible=true;

 temp_rheostat = this.game.add.sprite(40,220,'rheostat_full');
 temp_rheostat.scale.setTo(.1,.1);

//temp_rheostat.alpha=.5;
//  temp_rheostat.visible=false;
temp_rheostat.visible=true;

temp_keySet = this.game.add.sprite(90,360,'keySet_full');
temp_keySet.scale.setTo(.15,.15);
//temp_keySet.alpha=.5;
//  temp_keySet.visible=false;
temp_keySet.visible=true;

temp_ammeter = this.game.add.sprite(100,500,'ammeter_full');
temp_ammeter.scale.setTo(.09,.09);
 //temp_ammeter.alpha=1;
  // temp_ammeter.visible=false;
 temp_ammeter.visible=true;

temp_resistor = this.game.add.sprite(110,680,'nichrome_wire');
temp_resistor.scale.setTo(.13,.13);
//temp_resistor.alpha=.5;
//  temp_resistor.visible=false;
temp_resistor.visible=true;

temp_resistor2 = this.game.add.sprite(100,780,'nichrome_wire2');
temp_resistor2.scale.setTo(.13,.13);
//temp_resistor.alpha=.5;
//  temp_resistor.visible=false;
temp_resistor2.visible=true;

temp_voltmeter = this.game.add.sprite(100,890,'voltmeter_full');
temp_voltmeter.scale.setTo(.09,.09);
 //temp_voltmeter.alpha=1;
//  temp_voltmeter.visible=false;


 

direction1=this.game.add.sprite(385,495,'direction');
direction1.angle=90;
direction1.visible=false;
direction2=this.game.add.sprite(978,625,'direction');
direction2.angle=0;
direction2.visible=false;
direction3=this.game.add.sprite(1460,625,'direction');
direction3.angle=0;
direction3.visible=false;
direction4=this.game.add.sprite(1842,445,'direction');
direction4.angle=270;
direction4.visible=false;
direction5=this.game.add.sprite(1150,384,'direction');
direction5.angle=180;
direction5.visible=false;

direction9=this.game.add.sprite(1340,1000,'direction');
direction9.angle=0;

headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
direction_text=this.game.add.text(1380,985,"Direction of current",headfontStyle);


  current_direction_group=this.game.add.group();
  current_direction_group.add(direction1);
  current_direction_group.add(direction2);
  current_direction_group.add(direction3);
  current_direction_group.add(direction4);
  current_direction_group.add(direction5);
  current_direction_group.add(direction9);
  current_direction_group.add(direction_text);
current_direction_group.visible=false;
  
arrow=this.game.add.sprite(1500,450, 'arrow');
      arrow.angle=90;
      arrow_y=480;
      arrow.visible=false;

      // arrow.visible=true;
      // arrow.x=180;
      // arrow.y=820;
      // arrow_y=820;

      //  circuit= this.game.add.sprite(320,0,'circuit');
      //  circuit.scale.setTo(.2,.2);
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
},

loadScene:function()
{
  voice=this.game.add.audio("A_exp_procedure0",1);
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this);
},

startExperiment:function()
{
  voice.destroy();
  voice=this.game.add.audio("A_exp_procedure1",1);
  voice.play();
  dialog.text="Drag the battery-set and place it on the table.";
  temp_battery.events.onDragStart.add(function() {this.onDragStart(temp_battery)}, this);
  temp_battery.events.onDragStop.add(function() {this.onDragStop(temp_battery)}, this);
  temp_battery.inputEnabled = true;
  temp_battery.input.useHandCursor = true;
  temp_battery.input.enableDrag(true);
  this.game.physics.arcade.enable(temp_battery);
  temp_battery.enableBody =true;

},

rheostat_head_move:function()
{
   if(isR3Value1_taken==false)
   {
  pointer_currentx=this.game.input.activePointer.x;
  isRheostatMoving=true;
  arrow.visible=false;
  voltmeter_value1.visible=true;
  ammeter_value1.visible=true;
  button_sample.visible=true;
  
  
  // if(isR1Value1_taken==false)
  // {
    var line2=this.game.add.graphics(0, 0);
      line2.lineStyle(10,0xff704d,1);
      line2.moveTo(20, 115);
      line2.lineTo(320, 115);
     value_heading1.visible=true;

    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure22",1);
    voice.play();
    dialog.text="Take the reading by click on the button. Take upto three\nreadings";
    
    arrow.visible=true;
    arrow.x=1220;
    arrow.y=820;
    arrow_y=820;
   }

else{


  pointer_currentx=this.game.input.activePointer.x;
  isRheostatMoving=true;
  arrow.visible=false;
}
  // dialog.text="Take the first value";
  // value_heading2.visible=true;
  // ammeter_value4.visible=true;
  // voltmeter_value4.visible=true;
  // var line3=this.game.add.graphics(0, 0);
  // line3.lineStyle(10,0xff704d,1);
  // line3.moveTo(20, 530);
  // line3.lineTo(320, 530);

// var line4=this.game.add.graphics(0, 0);
//   line4.lineStyle(10,0xff704d,1);
//   line4.moveTo(20, 595);
//   line4.lineTo(320, 595);    
//   button_sample.visible=true;

 
  

},

rheostat_head_stop:function()
{
  isRheostatMoving=false;
  // console.log("voltmeter_meter"+voltmeter_needle.angle);
  // console.log("voltmeter_reading"+voltmeter_reading);
  // console.log("voltmeter_angle"+voltmeter_needle.angle);
  //console.log("inital_pos"+rHead_minimuval+"final_pos"+rheostat_head.x+"voltmeter_angle"+voltmeter_needle.angle);
  console.log("rheostat_head_pos"+rheostat_head.x);
//rheostat_resistance=rheostat_head.x - rheostat_head_initial_val;
 console.log("rheostat_resistance"+rheostat_resistance);
//console.log("rheostat_head_pos"+ rheostat_head.x);

 
},


drawVLine:function(xp,yp,val){
  var line1=this.game.add.graphics(0, 0);
      line1.lineStyle(2,0xFFFFFF,1);
      line1.moveTo(xp, yp);
      yp+=20;
      line1.lineTo(xp, yp);
      fontStyle2={ font: "20px Segoe UI", fill: "#ffffff", align: "center" };
  var text1 = this.game.add.text(0,0,val,fontStyle2);
      text1.x=xp-10;
      text1.y=yp+10;

},
onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==(temp_battery))
    {
       currentobj.xp=65;
       currentobj.yp=80;
       battery.alpha=.5;
    }

    if(currentobj==(temp_rheostat))
    {
     currentobj.xp=40;
     currentobj.yp=220;
     rheostat.alpha=.5;

    }

    if(currentobj==(temp_keySet))
    {
     currentobj.xp=90;
     currentobj.yp=360;
    keySet.alpha=.5;

    }
 if(currentobj==(temp_ammeter))
    {
     currentobj.xp=100;
     currentobj.yp=500;
    ammeter.alpha=.5;
    ammeter_needle.alpha=1;
    ammeter_needle.visible=true;

    }
if(currentobj==(temp_resistor))
    {
     currentobj.xp=110;
     currentobj.yp=680;
     resistor.alpha=.5;

    }

    if(currentobj==(temp_resistor2))
    {
     currentobj.xp=100;
     currentobj.yp=780;
     resistor2.alpha=.5;

    }

    if(currentobj==(temp_voltmeter))
    {
     currentobj.xp=100;
     currentobj.yp=890;
     voltmeter.alpha=.5;
     voltmeter_needle.alpha=1;
     voltmeter_needle.visible=true;

    }

    // if(currentobj==(temp_resistor2))
    // {
    //  currentobj.xp=100;
    //  currentobj.yp=960;
    //  voltmeter.alpha=.5;
    //  voltmeter_needle.alpha=1;
    //  voltmeter_needle.visible=true;

    // }

    if(currentobj==(key))
    {
    
      key.angle=0;
     currentobj.xp=1670;
     currentobj.yp=750;
     arrow.visible=false;
     arrow.x=1705;
     arrow.y=500;
     arrow_y=500;
     arrow.visible=true;
     
   

    }
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==temp_battery)
    {
        temp_battery.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure2",1);
        voice.play();
        battery.alpha=1;
        battery.visible=true;
        dialog.text="Drag the rheostat to the table and connect with the\nbattery";
        temp_rheostat.events.onDragStart.add(function() {this.onDragStart(temp_rheostat)}, this);
        temp_rheostat.events.onDragStop.add(function() {this.onDragStop(temp_rheostat)}, this);
        temp_rheostat.inputEnabled = true;
        temp_rheostat.input.useHandCursor = true;
        temp_rheostat.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_rheostat);
        temp_rheostat.enableBody =true;
        collider_battery.visible=false;
        collider_battery.enableBody=false;
        temp_battery.inputEnabled =false;
        temp_battery.input.useHandCursor = false;
        temp_battery.input.enableDrag(false);
        //this.game.physics.arcade.enable(temp_battery);
        temp_battery.enableBody =false;
      }

      if(currentobj==temp_rheostat)
      {
        temp_rheostat.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure3",1);
        voice.play();

        rheostat.alpha=1;
        rheostat.visible=true;
        rheostat_head.alpha=1;
        rheostat_head.visible=true;
        
        dialog.text="Connect the key to the rheostat in series";
        wire7.visible=true;
        temp_keySet.events.onDragStart.add(function() {this.onDragStart(temp_keySet)}, this);
        temp_keySet.events.onDragStop.add(function() {this.onDragStop(temp_keySet)}, this);
        temp_keySet.inputEnabled = true;
        temp_keySet.input.useHandCursor = true;
        temp_keySet.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_keySet);
        temp_keySet.enableBody =true;
        collider_rheostat.visible=false;
        collider_rheostat.enableBody=false;
        var line1=this.game.add.graphics(0, 0);
        //line1.lineStyle(7,0xe5e5e5,1);
        line1.moveTo(710, 520);
        line1.lineTo(890, 520);
        this.drawVLine(1460,380,"1");//0
        this.drawVLine(1490,380,"2");//35
        this.drawVLine(1520,380,"3");//70
        this.drawVLine(1550,380,"4");//105
        this.drawVLine(1580,380,"5");//140
        this.drawVLine(1610,380,"6");//175
        this.drawVLine(1640,380,"7");//210
        this.drawVLine(1670,380,"8");//245
        this.drawVLine(1700,380,"9");//280
        this.drawVLine(1730,380,"10");//315
        // rheostat_head.inputEnabled=false;
        // rheostat_head.input.useHandCursor=false;
      }

      if(currentobj==temp_keySet)
      {
        temp_keySet.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure4",1);
        voice.play();
        keySet.alpha=1;
        key.alpha=1;
        key.visible=true;
        
        dialog.text="Connect the ammeter to the circuit in series";
        wire6.visible=true;
        temp_ammeter.events.onDragStart.add(function() {this.onDragStart(temp_ammeter)}, this);
        temp_ammeter.events.onDragStop.add(function() {this.onDragStop(temp_ammeter)}, this);
        temp_ammeter.inputEnabled = true;
        temp_ammeter.input.useHandCursor = true;
        temp_ammeter.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_ammeter);
        temp_ammeter.enableBody =true;
        collider_key.visible=false;
        collider_key.enableBody=false;
      }
           
      if(currentobj==temp_ammeter)
      {
        temp_ammeter.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure5",1);
        voice.play();
        ammeter.alpha=1;
        ammeter.visible=true;
        dialog.text="Connect the resistor R1 to the circuit in series with\n the battery set";
        wire5.visible=true;
        //wire1.visible=true;
        temp_resistor.events.onDragStart.add(function() {this.onDragStart(temp_resistor)}, this);
        temp_resistor.events.onDragStop.add(function() {this.onDragStop(temp_resistor)}, this);
        temp_resistor.inputEnabled = true;
        temp_resistor.input.useHandCursor = true;
        temp_resistor.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_resistor);
        temp_resistor.enableBody =true;
        collider_ammeter.visible=false;
        collider_ammeter.enableBody=false;
      }
         if(currentobj==temp_resistor)
      {
        temp_resistor.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure18",1);
        voice.play();
        resistor.alpha=1;
        resistor.visible=true;
        dialog.text="Connect the resistor R2 to the circuit in series with\nammeter and resistor R1";
        wire1.visible=true;
        //wire4.visible=true;
        temp_resistor2.events.onDragStart.add(function() {this.onDragStart(temp_resistor2)}, this);
        temp_resistor2.events.onDragStop.add(function() {this.onDragStop(temp_resistor2)}, this);
        temp_resistor2.inputEnabled = true;
        temp_resistor2.input.useHandCursor = true;
        temp_resistor2.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_resistor2);
        temp_resistor2.enableBody =true;
        // collider_resistor.visible=false;
        // collider_resistor.enableBody=false;
      }
       if(currentobj==temp_voltmeter)
      {
        temp_voltmeter.visible=false;
        //temp_battery.destroy();
        voltmeter.alpha=1;
        voltmeter.visible=true;
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure7",1);
        voice.play();
        dialog.text="Take the key and insert the key into the key socket";
        wire2.visible=true;
        wire3.visible=true;
       // rheostat_head.inputEnabled=true;
        key.events.onDragStart.add(function() {this.onDragStart(key)}, this);
        key.events.onDragStop.add(function() {this.onDragStop(key)}, this);
        key.inputEnabled = true;
        key.input.useHandCursor = true;
        key.input.enableDrag(true);
        this.game.physics.arcade.enable(key);
        temp_voltmeter.enableBody =true;
        collider_voltmeter.visible=false;
        collider_voltmeter.enableBody=false;
        collider_key.inputEnabled=true;
        collider_key.enableBody=true;
        collider_key.visible=true;
        arrow.visible=true;
        arrow.x=1690;
        arrow.y=650;
        arrow_y=650;
      }

      if(currentobj==key)
      {
        //if(isR1Values_taken==false)
       // {
        rheostat_head.inputEnabled=true;
 rheostat_head.input.useHandCursor=true;
 rheostat_head.events.onInputDown.add(this.rheostat_head_move,this);
 rheostat_head.events.onInputUp.add(this.rheostat_head_stop,this);
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure8",1);
        voice.play();
        arrow.visible=false;
        key.x=1670;
        key.y=574;
        power_State="switched_on";
        dialog.y=40;
        //dialog.text="Change the rheostat value by moving the head, and take voltmeter and\nammeter reading three times by click on the 'take value' button";
        dialog.text="Change the rheostat value by moving the rheostat head,\nand observe the voltmeter and ammeter. ";
        arrow.visible=true;
        arrow.x=1475;
        arrow.y=125;
        arrow_y=125;
        current_direction_group.visible=true;
        
        }
        // else if(isR1Values_taken==true)
        // {

        //   power_State="switched_on";
        //   dialog.text="Now take the readings."
        //   arrow.visible=false;
        // key.x=1670;
        // key.y=574;
        // }
      
      

      if(currentobj==temp_resistor2)
      {

        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure19",1);
        voice.play();
        wire4.visible=true;
        wire9.visible=true;
        temp_resistor2.visible=false;
        resistor2.alpha=1;

        dialog.text="Connect the voltmeter parallel to the resistors R1 and R2";
        temp_voltmeter.events.onDragStart.add(function() {this.onDragStart(temp_voltmeter)}, this);
        temp_voltmeter.events.onDragStop.add(function() {this.onDragStop(temp_voltmeter)}, this);
        temp_voltmeter.inputEnabled = true;
        temp_voltmeter.input.useHandCursor = true;
        temp_voltmeter.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_voltmeter);
        temp_voltmeter.enableBody =true;

    
      }

  },
  
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    if(power_State=="switched_on")
    {
      current_direction_group.visible=true;
        animationCounter++;
        if(animationCounter==1)
         {

          direction1.visible=true;
          }
          else if(animationCounter==20)
         {
          direction1.visible=false;
          direction2.visible=true;
         }
          else if(animationCounter==40)
           {
          direction2.visible=false;
          direction3.visible=true;
           }
          else if(animationCounter==60)
           {
          direction3.visible=false;
          direction4.visible=true;
          }
          else if(animationCounter==80)
           {
          direction4.visible=false;
          direction5.visible=true;
          
          }
          else if(animationCounter>=100)
          {
            direction5.visible=false;
            animationCounter=0;
          }
    }
    else if (power_State=="switch_off")
    {
      current_direction_group.visible=false;
    }
    

    if(isRheostatMoving==true)
    {
      //pointer_currentx=this.game.input.activePointer.x;
      pointer_newx=this.game.input.activePointer.x;
      if(pointer_newx>=pointer_currentx+15){
        rheostat_head.x+=30;
        pointer_currentx=rheostat_head.x;
      }else if(pointer_newx<=pointer_currentx-15){
        rheostat_head.x-=30;
        pointer_currentx=rheostat_head.x;
      }
      //rheostat_head.x=this.game.input.activePointer.x;
      if(rheostat_head.x<1460)
      {
        rheostat_head.x=1460;
      }
      if(rheostat_head.x>1730)
      {
        rheostat_head.x=1730;
      }
      
    
     
    }
    var rs=(rheostat_head.x-1460)/30;
      //console.log(rs+"////");
      //power_State="switched_on";
      if(power_State=="switched_on")
      {
        if(rs!=Needle_Position_am)
        {
          var new_angle_am=R1ammeter_angles[rs];
          am_Val=R1ammeter_Values[rs];
         // var new_angle_vm=R1voltmeter_angles[rs];
          if(ammeter_needle.angle<new_angle_am)
          {
            ammeter_needle.angle++;
           // voltmeter_needle.angle++;
          // voltmeter_needle.angle++;
            //console.log(ammeter_needle.angle+"//"+new_angle);
            if(ammeter_needle.angle>=new_angle_am)
            {
               //am_Val=R1ammeter_Values[rs];
              // vm_Val=R1voltmeter_Values[rs]
              console.log(am_Val+"//"+vm_Val);
              Needle_Position_am=rs;
            }
          }
          else if(ammeter_needle.angle>new_angle_am)
          {
            ammeter_needle.angle--;
           // voltmeter_needle.angle--;
            console.log(ammeter_needle.angle+"//"+new_angle_am);
            if(ammeter_needle.angle<=new_angle_am)
            {
               //am_Val=R1ammeter_Values[rs];
              // vm_Val=R1voltmeter_Values[rs]
              console.log(am_Val+"//"+vm_Val);
              Needle_Position_am=rs;
            }
          }
        
        }
          ///////////////////////////
          if(rs!=Needle_Position_vm)
        {
          //var new_angle_am=R1ammeter_angles[rs];
          var new_angle_vm=R1voltmeter_angles[rs];
          vm_Val=R1voltmeter_Values[rs]
          if(voltmeter_needle.angle<new_angle_vm)
          {
            //ammeter_needle.angle++;
           // voltmeter_needle.angle++;
           voltmeter_needle.angle++;
            //console.log(ammeter_needle.angle+"//"+new_angle);
            if(voltmeter_needle.angle>=new_angle_vm)
            {
               //am_Val=R1ammeter_Values[rs];
               //vm_Val=R1voltmeter_Values[rs]
              console.log(am_Val+"//"+vm_Val);
              Needle_Position_vm=rs;
            }
          }
          else if(voltmeter_needle.angle>new_angle_vm)
          {
            //ammeter_needle.angle--;
            voltmeter_needle.angle--;
            console.log(ammeter_needle.angle+"//"+new_angle_am);
            if(voltmeter_needle.angle<=new_angle_vm)
            {
               //am_Val=R1ammeter_Values[rs];
               //vm_Val=R1voltmeter_Values[rs]
              console.log(am_Val+"//"+vm_Val);
              Needle_Position_vm=rs;
            }
          }
        }
          ///////////////////////////
      }

      
      
  },
 
  detectCollision:function(){
   

     if(collider_battery.enableBody && currentobj!=null)
     {
         this.game.physics.arcade.overlap(currentobj, collider_battery,function() {this.match_Obj()},null,this);
        // collider=collider_battery;
     } 

     if(collider_rheostat.enableBody && currentobj!=null)
     {
      this.game.physics.arcade.overlap(currentobj, collider_rheostat,function() {this.match_Obj()},null,this);
      //collider=collider_battery;
     }

    if(collider_key.enableBody && currentobj!=null)
    {    
    this.game.physics.arcade.overlap(currentobj, collider_key,function() {this.match_Obj()},null,this);
    //collider=collider_battery;
    }
    if(collider_ammeter.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_ammeter,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }
    if(collider_resistor.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_resistor,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }

    if(collider_resistor2.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_resistor2,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }
  if(collider_voltmeter.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_voltmeter,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }
    // if(collider_B.enableBody && currentobj!=null){
    //     this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
    //     collider=collider_B;
    // }
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
       if(currentobj==(temp_battery))
       {
        temp_battery.visible=true;

        battery.alpha=0;
        //battery.visible=false; 
       }
       if(currentobj==(temp_rheostat))
       {
       temp_rheostat.visible=true;
       rheostat.alpha=0;
      // rheostat.visible=false;
       }
       if(currentobj==(temp_keySet))
       {
       temp_keySet.visible=true;
       keySet.alpha=0;
     //keySet.visible=false;
       }   
       if(currentobj==(temp_ammeter))
       {
       temp_ammeter.visible=true;
       ammeter.alpha=0;
       ammeter_needle.alpha=0;
     //keySet.visible=false;
       }
       if(currentobj==(temp_resistor))
       {
       temp_resistor.visible=true;
       resistor.alpha=0;
     //keySet.visible=false;
       }

       if(currentobj==(temp_resistor2))
       {
       temp_resistor2.visible=true;
       resistor2.alpha=0;
     //keySet.visible=false;
       }
      if(currentobj==(temp_voltmeter))
       {
       temp_voltmeter.visible=true;
       voltmeter.alpha=0;
       voltmeter_needle.alpha=0;
     //keySet.visible=false;
       }
       
    //     arrow.x=1500;
    //     arrow.y=450;
    //     arrow_y=450;
    //     arrow.visible=true;
        
    //   }

         currentobj=null;
      }
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A2",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
