var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var mirrorPositionValue1;
  var mirrorPositionValue2;
  var mirrorPositionValue3;
  var screenPositionValue1;
  var screenPositionValue2;
  var screenPositionValue3;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        //this.game.load.atlasJSONHash('ferrous_sulphate', 'assets/ferrous_sulphate.png', 'assets/ferrous_sulphate.json');
        //this.game.load.atlasJSONHash('burner_sprites', 'assets/burner_sprites.png', 'assets/burner_sprites.json');
      
        this.game.load.json('questions','data/questions1.json');
    
        
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/concave_mirror/Bg.png");
        this.game.load.image("bg1","assets/bg1.png");
        // this .game.load.image("bg2","assets/Decomposition_reaction/Bg3.jpg");
        
        this.game.load.image('transBackground','assets/transBackground.png');
         this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");   
        this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        //this.game.load.image('observation_table_decomposition','assets/observation1.png');
        // this.game.load.image('observation_img_1','assets/observation_img_1.png');
        // this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
///////////////concave mirror/////////////////////////////////////////////
        this.game.load.image("scale","assets/concave_mirror/Scale.png");
        this.game.load.image("mirror_side_view","assets/concave_mirror/Mirror.png");  
        this.game.load.image("mirror_front_view","assets/concave_mirror/Mirror_front_view.png");  
        this.game.load.image("blur_image","assets/concave_mirror/Blur_image.png");
        this.game.load.image("image","assets/concave_mirror/Image.png");    
        this.game.load.image("screen","assets/concave_mirror/Image_Stand.png");    
        this.game.load.image("mirror_stand","assets/concave_mirror/Mirror_Stand.png"); 
        this.game.load.image("mirror_in_stand","assets/concave_mirror/Mirror_in_stand.png");
        this.game.load.image("exp_table","assets/concave_mirror/exp_table.png");
        this.game.load.image("button","assets/concave_mirror/take_reading_button.png");
        this.game.load.image("diagram","assets/concave_mirror/ray_diagram.png");
        //////////////////////////for materials/////////////////////////////
        this.game.load.image("m_scale","assets/concave_mirror/Materials/m_scale.png");
        this.game.load.image("m_mirror_stand","assets/concave_mirror/Materials/m_mirror_Stand.png");
        this.game.load.image("m_mirror","assets/concave_mirror/Materials/m_mirror.png");
        this.game.load.image("m_mirror_side","assets/concave_mirror/Materials/m_mirror_side.png");
        this.game.load.image("m_screen","assets/concave_mirror/Materials/m_image_Stand.png");

        this.game.load.image("observation_table","assets/concave_mirror/obs_table.png");


        this.game.load.atlasJSONHash('image_sprite', 'assets/concave_mirror/image_sprite.png','assets/concave_mirror/image_sprite.json');


////////////////////////////////////Audio for concavemirror/////////////////////////

        this.game.load.audio('A_aim','assets/audio/Double_displacement/A_aim.mp3');
        this.game.load.audio('A_precautions','assets/audio/Double_displacement/A_precautions.mp3');
        this.game.load.audio('A_procedure1','assets/audio/Double_displacement/A_procedure1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Double_displacement/A_procedure2.mp3');
        this.game.load.audio('A_procedure3','assets/audio/Double_displacement/A_procedure3.mp3');
         this.game.load.audio('A_observations','assets/audio/Double_displacement/A_observations.mp3');
this.game.load.audio('A_result','assets/audio/Double_displacement/A_result.mp3');
         
        this.game.load.audio('A_exp_procedure1','assets/audio/Double_displacement/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/Double_displacement/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/Double_displacement/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/Double_displacement/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/Double_displacement/A_exp_procedure5.mp3');
        this.game.load.audio('A_exp_procedure6','assets/audio/Double_displacement/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/Double_displacement/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/Double_displacement/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/Double_displacement/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/Double_displacement/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/Double_displacement/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/Double_displacement/A_exp_procedure12.mp3');
        this.game.load.audio('A_exp_procedure13','assets/audio/Double_displacement/A_exp_procedure13.mp3');
        this.game.load.audio('A_exp_procedure14','assets/audio/Double_displacement/A_exp_procedure14.mp3');
  




       
/////////////////////////////Decomposition reaction materials/////////////////////////
        // this.game.load.image('wood_rack','assets/Double_displacement/Materials/Wooden_stand_full.png');
        // this.game.load.image('testtube','assets/Double_displacement/Materials/Testube.png');
        // this.game.load.image('FS','assets/Double_displacement/Materials/FS.png');
        // this.game.load.image('spoon','assets/Double_displacement/Materials/Spoon.png');
        // this.game.load.image('stand','assets/Double_displacement/Materials/Testube_Stand_full.png');
        // this.game.load.image('burner','assets/Double_displacement/Materials/Burner_full.png');
        // this.game.load.image('testtube_holder','assets/Double_displacement/Materials/Testube_Holder.png');
        //  this.game.load.image('HCl','assets/Double_displacement/Materials/M_HCL.png');

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
     ///this.game.state.start("Viva");//Starting the gametitle state
    //this.game.state.start("Experiment_1");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1
     // this.game.state.start("Theory");
     // this.game.state.start("Observations");//hot
   //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");
           this.game.state.start("Aim");
      //this.game.state.start("Exp_Selection");

	}
}

