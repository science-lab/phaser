var experiment_2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;

}

experiment_2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);

 this.arrangeScene();

 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(800,0,'bg2');
 bg.scale.setTo(-1,1);

  maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.lineStyle(10,0xff704d,1);
  maskBg1.beginFill(0xffffcc);
  maskBg1.drawRect(20, 40, 300, 700);
  maskBg1.alpha=1;
 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////

  fNames=[];
  isMirrorStandMoving=false;
  isScreenMoving=false;
  pointerx=0;
  screen_stand_scale_val=0;
  mirror_stand_scale_val=0;
  mirror_stand_minPos=30;
  screen_stand_maxPos=1343;
  image_maxPos=1289;
  screen_and_stand_distance=70;
  currentobj=null;

  isreading1Taken=false;
  isreading2Taken=false;
  isreading3Taken=false;
  isAnyStandMoved=false;
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  dialog_box=this.game.add.sprite(340,20, 'dialogue_box1');
      dialog_box.scale.setTo(.64,.8);
      dialog_text="Let's perform the experiment.";
      
      //procedure_voice1.play();
      dialog=this.game.add.text(360,60,dialog_text,fontStyle);
      

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "28px Segoe UI", fill: "#000000", align: "center"};//fontWeight:"bold" 
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};


exp_table=this.game.add.sprite(350,200,'exp_table');
exp_table.scale.setTo(1,1);

tableHeading1=this.game.add.text(355,230,"S/N",fontStyle3);
tableHeading2=this.game.add.text(415,220,"Position of the concave mirror X(cm)",fontStyle3);
tableHeading3=this.game.add.text(930,220,"Position of the screen Y(cm)",fontStyle3);

sNUmber1=this.game.add.text(370,290,"",fontStyle3);
sNUmber2=this.game.add.text(370,350,"",fontStyle3);
sNUmber3=this.game.add.text(370,410,"",fontStyle3);

mirrorPosVal1=this.game.add.text(620,290,"",fontStyle3);
mirrorPosVal2=this.game.add.text(620,350,"",fontStyle3);
mirrorPosVal3=this.game.add.text(620,410,"",fontStyle3);

screenPosVal1=this.game.add.text(1100,290,"",fontStyle3);
screenPosVal2=this.game.add.text(1100,350,"",fontStyle3);
screenPosVal3=this.game.add.text(1100,410,"",fontStyle3);

table_group=this.game.add.group();
table_group.add(tableHeading1);
table_group.add(tableHeading2);
table_group.add(tableHeading3);
table_group.add(sNUmber1);
table_group.add(sNUmber2);
table_group.add(sNUmber3);
table_group.add(mirrorPosVal1);
table_group.add(mirrorPosVal2);
table_group.add(mirrorPosVal3);
table_group.add(screenPosVal1);
table_group.add(screenPosVal2);
table_group.add(screenPosVal3);
table_group.add(exp_table);
table_group.visible=false;








//sample_button_text="Take value";
// button_sample=this.game.add.sprite(70,940,'take_reading_btn');
// button_sample.inputEnabled = true;
//play.input.priorityID = 3;
// button_sample.input.useHandCursor = true;
// button_sample.events.onInputDown.add(this.takeValue, this);
// button_sample.visible=false;
// button_sample.scale.setTo(2,2);

////////////////////////////////////////////////////////////////////

 },

 takeValue:function()
 {

  if(screen_and_stand_distance>=3&&screen_and_stand_distance<=37)
  {
    arrow.visible=false;
       if(isreading1Taken==false)
        {
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure9",1);
        voice.play();
        sNUmber1.text="1";
        mirrorPositionValue1=mirror_stand_scale_val;
        screenPositionValue1=screen_stand_scale_val;
        mirrorPosVal1.text=mirror_stand_scale_val;
        screenPosVal1.text=screen_stand_scale_val;
        isreading1Taken=true;
        dialog.text="Good, you have taken the first reading. Now take the second reading.";//audio pending
    

          }
        else if(isreading2Taken==false)
        {
          if(mirrorPositionValue1==mirror_stand_scale_val/*||screenPositionValue1==screen_stand_scale_val*/)
          {
            voice.destroy();
            voice=this.game.add.audio("A_exp_procedure12",1);
            voice.play();
            dialog.text="Please change the position of the mirror then adjust the screen \nand take the reading.";
          }
          else
          {
            voice.destroy();
            voice=this.game.add.audio("A_exp_procedure10",1);
            voice.play();
            sNUmber2.text="2";
            mirrorPositionValue2=mirror_stand_scale_val;
            screenPositionValue2=screen_stand_scale_val;
            mirrorPosVal2.text=mirror_stand_scale_val;
            screenPosVal2.text=screen_stand_scale_val;
            isreading2Taken=true;
            dialog.text="Nice, Now take the third reading.";
          }
        }

        else if(isreading3Taken==false)
        {
          if(mirrorPositionValue1==mirror_stand_scale_val/*||screenPositionValue1==screen_stand_scale_val*/||mirrorPositionValue2==mirror_stand_scale_val/*||screenPositionValue2==screen_stand_scale_val*/)
          {
            voice.destroy();
            voice=this.game.add.audio("A_exp_procedure12",1);
            voice.play();
            dialog.text="Please change the position of the mirror then adjust the screen\nand take the reading.";
          }
          else
          {
            voice.destroy();
            voice=this.game.add.audio("A_exp_procedure11",1);
            voice.play();
            sNUmber3.text="3";
            mirrorPositionValue3=mirror_stand_scale_val;
            screenPositionValue3=screen_stand_scale_val;
            mirrorPosVal3.text=mirror_stand_scale_val;
            screenPosVal3.text=screen_stand_scale_val;
            isreading3Taken=true;
            dialog.text="Good job!!! You have successfully taken three readings.";
            this.game.time.events.add(Phaser.Timer.SECOND*5,this.toObservation, this);
          }
        }


  }
  else if(screen_and_stand_distance<3||screen_and_stand_distance>37)
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure14",1);
    voice.play();
    dialog.text="No image is formed on the screen. Please take the reading once an\nimage is visible on the screen.";

  }

 },

 toObservation:function()
 {
  voice.destroy();
  voice=this.game.add.audio("A_exp_procedure13",1);
  voice.play();
 dialog.text="Click on the next button to see the observations.";
 play.visible=true;
 },

arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
  // circuit= this.game.add.sprite(320,0,'circuit');
  // circuit.scale.setTo(.4,.5);

//////////colliders//////////
 
////////////////////////////////////////equipments//////////////////////////////////
 

scale=this.game.add.sprite(10,850,'scale2');
scale.scale.setTo(1,1);
scale.anchor.setTo(0,0);
scale.alpha=1;
scale.visible=true;


screen_stand=this.game.add.sprite(1096,730,'screen');
screen_stand.scale.setTo(1,1.1);
screen_stand.anchor.setTo(.5,.5);
screen_stand.alpha=0;
// screen_stand.inputEnabled=true;
// screen_stand.input.useHandCursor=true;
// screen_stand.events.onInputDown.add(this.screen_stand_move,this);
// screen_stand.events.onInputUp.add(this.screen_stand_stop,this);
// this.game.physics.arcade.enable(screen_stand);


image_sprite=this.game.add.sprite(1042,630,'image_sprite','image3.png');


stand=this.game.add.sprite(412,560,'mirror_stand');
stand.alpha=0;
stand.visible=true;

mirror_stand=this.game.add.sprite(498,705,'mirror_in_stand');
mirror_stand.anchor.setTo(.5,.5);
mirror_stand.alpha=0;
// mirror_stand.inputEnabled=true;
// mirror_stand.input.useHandCursor=true;
// mirror_stand.events.onInputDown.add(this.mirror_stand_move,this);
// mirror_stand.events.onInputUp.add(this.mirror_stand_stop,this);
// this.game.physics.arcade.enable(mirror_stand);



//////////////////temp equipments////////////

 

//  collider_B = this.game.add.sprite(515, 555,'collider');
//  collider_B.alpha=1;
//  this.game.physics.arcade.enable(collider_B);//For colling purpose

// collider_scale=this.game.add.sprite(600,880,'collider');
// collider_scale.scale.setTo(6,2);
// collider_scale.alpha=0;
// this.game.physics.arcade.enable(collider_scale);
// collider_scale.visible=false;

// collider_stand=this.game.add.sprite(420,630,'collider');
// collider_stand.scale.setTo(3,5);
// collider_stand.alpha=0;
// this.game.physics.arcade.enable(collider_stand);
// collider_stand.visible=false;

// collider_mirror=this.game.add.sprite(420,520,'collider');
// collider_mirror.scale.setTo(3,3);
// collider_mirror.alpha=0;
// this.game.physics.arcade.enable(collider_mirror);
// collider_mirror.visible=false;

// collider_screen=this.game.add.sprite(1020,630,'collider');
// collider_screen.scale.setTo(3,5);
// collider_screen.alpha=0;
// this.game.physics.arcade.enable(collider_screen);
// collider_screen.visible=false;

// temp_scale=this.game.add.sprite(45,100,'scale');
// temp_scale.scale.setTo(.17,.3);

// temp_mirror_stand=this.game.add.sprite(120,180,'mirror_stand');
// temp_mirror_stand.scale.setTo(.5,.5);

// temp_mirror=this.game.add.sprite(100,400,'mirror_front_view');
// temp_mirror.scale.setTo(.5,.5);

// temp_screen_stand=this.game.add.sprite(120,550,'screen');
// temp_screen_stand.scale.setTo(.5,.5);





button=this.game.add.sprite(1550,900,'button');
button.scale.setTo(2,2);
button.inputEnabled = true;
button.input.useHandCursor = true;
button.events.onInputDown.add(this.takeValue, this);
button.visible=false;

  
arrow=this.game.add.sprite(1475,450, 'arrow');
      arrow.angle=90;
      arrow_y=130;
      arrow.visible=false;

arrow1=this.game.add.sprite(1475,450, 'arrow');
      arrow1.angle=90;
      arrow1_y=130;
      arrow1.visible=false;

      // arrow.visible=true;
      // arrow.x=540;
      // arrow.y=750;
      // arrow_y=750;

      // arrow1.visible=true;
      // arrow1.x=1140;
      // arrow1.y=750;
      // arrow1_y=750;

      // circuit= this.game.add.sprite(320,0,'circuit');
      // circuit.scale.setTo(.4,.5);
     this.game.time.events.add(Phaser.Timer.SECOND*1,this.loadScene, this);
},


mirror_stand_move:function()
{
isMirrorStandMoving=true;
pointerx=this.game.input.activePointer.x;
arrow.visible=false;
arrow1.visible=false;

if(isAnyStandMoved==false)
{
  // voice.destroy();
  //     voice=this.game.add.audio("A_exp_procedure8",1);
  //     voice.play();
  //dialog.text="Once you get a clear image, click on the button to take the reading.";
  // arrow.x=1680;
  // arrow.y=790;
  // arrow_y=790;
  // arrow.visible=true;
  table_group.visible=true;
  isAnyStandMoved=true;
  button.visible=true;
}



},

mirror_stand_stop:function()
{
isMirrorStandMoving=false;
console.log("position of mirror"+mirror_stand.x);
console.log("mirror position value"+mirror_stand_scale_val);


},

screen_stand_move:function()
{
isScreenMoving=true;
pointerx=this.game.input.activePointer.x;
arrow.visible=false;
arrow1.visible=false;

if(isAnyStandMoved==false)
{
  voice.destroy();
      // voice=this.game.add.audio("A_exp_procedure8",1);
      // voice.play();
  //dialog.text="Once you get a clear image, click on the button to take the reading.";
  // arrow.x=1680;
  // arrow.y=790;
  // arrow_y=790;
  // arrow.visible=true;
  table_group.visible=true;
  isAnyStandMoved=true;
  table_group.visible=true;
  button.visible=true;
}





},

screen_stand_stop:function()
{
isScreenMoving=false;
console.log("position of screen"+screen_stand.x);
console.log("screen position value"+screen_stand_scale_val);
console.log("image position"+image_sprite.x);


},


loadScene:function()
{
  voice=this.game.add.audio("A_exp_procedure1",1);
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this);
},

startExperiment:function()
{
  // voice.destroy();
  // voice=this.game.add.audio("A_exp_procedure2",1);
  // voice.play();
  // dialog.text="Drag and drop scale on to the table.";
  // temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
  // temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
  // temp_scale.inputEnabled = true;
  // temp_scale.input.useHandCursor = true;
  // this.game.physics.arcade.enable(temp_scale);
  // temp_scale.input.enableDrag(true);

},




// drawVLine:function(xp,yp,val){
//   var line1=this.game.add.graphics(0, 0);
//       line1.lineStyle(2,0xFFFFFF,1);
//       line1.moveTo(xp, yp);
//       yp+=20;
//       line1.lineTo(xp, yp);
//       fontStyle2={ font: "20px Segoe UI", fill: "#ffffff", align: "center" };
//   var text1 = this.game.add.text(0,0,val,fontStyle2);
//       text1.x=xp-10;
// //       text1.y=yp+10;

// },
onDragStart:function(obj)
  {
    //obj.angle=0;
    console.log("iiiiii"+obj);
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==temp_scale)
    {
       currentobj.xp=45;
       currentobj.yp=100;
      
       scale.visible=true;
       scale.alpha=.5;
       collider_scale.inputEnabled=true;
       collider_scale.enableBody=true;
       collider_scale.visible=true;
       arrow.x=780;
       arrow.y=790;
       arrow_y=790;
       arrow.visible=true;
       
    }

       
    if(currentobj==temp_mirror_stand)
    {
       currentobj.xp=120;
       currentobj.yp=180;
       stand.visible=true;
       stand.alpha=.5;
       collider_stand.inputEnabled=true;
       collider_stand.enableBody=true;
       collider_stand.visible=true;
       arrow.x=500;
       arrow.y=550;
       arrow_y=550;
       arrow.visible=true;
       
    }

    if(currentobj==temp_mirror)
    {
       currentobj.xp=100;
       currentobj.yp=400;
       mirror_stand.visible=true;
       mirror_stand.alpha=.5;
       collider_mirror.inputEnabled=true;
       collider_mirror.enableBody=true;
       collider_mirror.visible=true;
       arrow.x=510;
       arrow.y=530;
       arrow_y=530;
       arrow.visible=true;
       
    }

    if(currentobj==temp_screen_stand)
    {
       currentobj.xp=120;
       currentobj.yp=550;
       screen_stand.visible=true;
       screen_stand.alpha=.5;
       collider_screen.inputEnabled=true;
       collider_screen.enableBody=true;
       collider_screen.visible=true;
       arrow.x=1100;
       arrow.y=490;
       arrow_y=490;
       arrow.visible=true;
       
    }

  },

  check_image:function()
  {
    screen_and_stand_distance=(screen_stand_scale_val-mirror_stand_scale_val);
    if(screen_and_stand_distance>=3||screen_and_stand_distance<=20)
    {
      image_sprite.alpha=1;
      image_sprite.frameName="image"+screen_and_stand_distance+".png";
    }
    else if(screen_and_stand_distance>20||screen_and_stand_distance<=37)
    {
      image_sprite.alpha=1;
      image_sprite.frameName="image"+screen_and_stand_distance+".png";
    }
    else if (screen_and_stand_distance>37)
    {
      image_sprite.frameName="image3.png";
      image_sprite.alpha=0;
    }
    else if (screen_and_stand_distance<3)
    {
      image_sprite.frameName="image3.png";
      image_sprite.alpha=0;
    }

    if(screen_and_stand_distance==20)
    {
      if(isreading1Taken==false)
      {
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure8",1);
        voice.play();
        dialog.text="Good job, you have obtained a clear image on the screen.\nClick on the button to take the reading.";
        arrow.x=1680;
        arrow.y=790;
        arrow_y=790;
        arrow.visible=true;

      }
    }
    else if(screen_and_stand_distance!=20)
    {

      voice.destroy();
      // arrow.visible=false;
    }

       
    
  
    

    
     
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;

    console.log("jjjjj"+obj);
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==temp_scale)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure3",1);
      voice.play();
      arrow.visible=false;
      collider_scale.inputEnabled=false;
      collider_scale.enableBody=false;
      collider_scale.visible=false;
      temp_scale.visible=false;
      scale.alpha=1;

      dialog.text="Now take the mirrror stand and place it on the table.";
      temp_mirror_stand.events.onDragStart.add(function() {this.onDragStart(temp_mirror_stand)}, this);
      temp_mirror_stand.events.onDragStop.add(function() {this.onDragStop(temp_mirror_stand)}, this);
      temp_mirror_stand.inputEnabled = true;
      temp_mirror_stand.input.useHandCursor = true;
      this.game.physics.arcade.enable(temp_mirror_stand);
      temp_mirror_stand.input.enableDrag(true);
       
    }

    if(currentobj==temp_mirror_stand)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure4",1);
      voice.play();
      arrow.visible=false;
      collider_stand.inputEnabled=false;
      collider_stand.enableBody=false;
      collider_stand.visible=false;
      temp_mirror_stand.visible=false;
      stand.alpha=1;

      dialog.text="Good job, now take the concave mirror and attach with\nthe mirror stand.";
      temp_mirror.events.onDragStart.add(function() {this.onDragStart(temp_mirror)}, this);
      temp_mirror.events.onDragStop.add(function() {this.onDragStop(temp_mirror)}, this);
      temp_mirror.inputEnabled = true;
      temp_mirror.input.useHandCursor = true;
      this.game.physics.arcade.enable(temp_mirror);
      temp_mirror.input.enableDrag(true);
       
    }

    if(currentobj==temp_mirror)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure5",1);
      voice.play();
      arrow.visible=false;
      collider_mirror.inputEnabled=false;
      collider_mirror.enableBody=false;
      collider_mirror.visible=false;
      temp_mirror.visible=false;
      mirror_stand.alpha=1; 
      stand.visible=false;

      dialog.text="Finally, take the screen and place it on the table.";
      temp_screen_stand.events.onDragStart.add(function() {this.onDragStart(temp_screen_stand)}, this);
      temp_screen_stand.events.onDragStop.add(function() {this.onDragStop(temp_screen_stand)}, this);
      temp_screen_stand.inputEnabled = true;
      temp_screen_stand.input.useHandCursor = true;
      this.game.physics.arcade.enable(temp_screen_stand);
      temp_screen_stand.input.enableDrag(true);
       
    }

    if(currentobj==temp_screen_stand)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure6",1);
      voice.play();
      arrow.visible=false;
      collider_screen.inputEnabled=false;
      collider_screen.enableBody=false;
      collider_screen.visible=false;
      temp_screen_stand.visible=false;
      screen_stand.alpha=1; 
      maskBg1.visible=false;
      dialog_box.x=60;
      dialog_box.y=20;
      dialog_box.scale.setTo(.8,.8);
     
      dialog.text="Good job....You have successfully placed all the equipments.";
      dialog.x=75;
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextStage, this);  
    }

       
  },

  nextStage:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_exp_procedure7",1);
      voice.play();
    dialog.text="Move the mirror stand or the screen and try to get a clear image on\nthe screen.";
    arrow.x=540;
    arrow.y=750;
    arrow_y=750;
    arrow.visible=true;

    arrow1.x=1140;
    arrow1.y=750;
    arrow1_y=750;
    arrow1.visible=true;

    screen_stand.inputEnabled=true;
    screen_stand.input.useHandCursor=true;
    screen_stand.events.onInputDown.add(this.screen_stand_move,this);
    screen_stand.events.onInputUp.add(this.screen_stand_stop,this);
    this.game.physics.arcade.enable(screen_stand);

    mirror_stand.inputEnabled=true;
    mirror_stand.input.useHandCursor=true;
    mirror_stand.events.onInputDown.add(this.mirror_stand_move,this);
    mirror_stand.events.onInputUp.add(this.mirror_stand_stop,this);
    this.game.physics.arcade.enable(mirror_stand);

  },

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    if(arrow1.visible){
      arrow1.y+=(3+DeltaTime);
      if(arrow1.y>=(arrow1_y+50))
      {
        arrow1.y=arrow1_y;
      }
    }

    if(isMirrorStandMoving==true)
    {
      this.check_image();
      pointerx_new=this.game.input.activePointer.x;
      if(pointerx_new>=pointerx+10)
      {
        mirror_stand.x+=13;
        pointerx=mirror_stand.x;
      }
      else if(pointerx_new<=pointerx-10)
      {
        mirror_stand.x-=13;
        pointerx=mirror_stand.x;
      }


      if(mirror_stand.x<mirror_stand_minPos)
      {
        mirror_stand.x=30;
      }
      else if(mirror_stand.x>=(screen_stand.x-52))
      {
        mirror_stand.x=screen_stand.x-52;
      }
    }

      if(isScreenMoving==true)
      {
        this.check_image();
        pointerx_new=this.game.input.activePointer.x;
        if(pointerx_new>=pointerx+10)
        {
          screen_stand.x+=13;
          image_sprite.x+=13;
          pointerx=screen_stand.x;
        }
        else if(pointerx_new<=pointerx-10)
        {
          screen_stand.x-=13;
          image_sprite.x-=13;
          pointerx=screen_stand.x;
        }

        if(screen_stand.x>screen_stand_maxPos)
        {
            screen_stand.x=1343;
        }
        else if(screen_stand.x<=(mirror_stand.x+52))
        {

          screen_stand.x=mirror_stand.x+52;
        }

        if(image_sprite.x>image_maxPos)
        {

          image_sprite.x=1289;
        }
        else if(image_sprite.x<=(mirror_stand.x-2))
        {
          image_sprite.x=mirror_stand.x-2;
        }
      }

    mirror_stand_scale_val=(mirror_stand.x-30)/13;
    screen_stand_scale_val=(screen_stand.x-43)/13;

    

  },

  
 
  detectCollision:function(){
   

    // if(collider_B.enableBody && currentobj!=null){
    //     this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
    //     collider=collider_B;
    // }

    // if(collider_scale.enableBody && currentobj!=null)
    // {
    //   this.game.physics.arcade.overlap(currentobj, collider_scale,function() {this.match_Obj()},null,this);
    //   collider=collider_scale;
    // }
    // if(collider_stand.enableBody && currentobj!=null)
    // {
    // this.game.physics.arcade.overlap(currentobj, collider_stand,function() {this.match_Obj()},null,this);
    // collider=collider_stand;
    // }
    // if(collider_mirror.enableBody && currentobj!=null)
    // {
    // this.game.physics.arcade.overlap(currentobj, collider_mirror,function() {this.match_Obj()},null,this);
    // collider=collider_mirror;
    // }
    // if(collider_screen.enableBody && currentobj!=null)
    // {
    // this.game.physics.arcade.overlap(currentobj, collider_screen,function() {this.match_Obj()},null,this);
    // collider=collider_screen;
    // }

    
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
       if(currentobj==temp_scale)
       {
        temp_scale.visible=true;
        scale.alpha=0;
        
       }
       if(currentobj==temp_mirror_stand)
       {
        temp_scale.visible=true;
        stand.alpha=0;
        
       }
       if(currentobj==temp_mirror)
       {
        temp_mirror.visible=true;
      mirror_stand.alpha=0;
      stand.visible=true;
        
       }

       if(currentobj==temp_screen_stand)
       {
        temp_screen_stand.visible=true;
      screen_stand.alpha=0;
      // stand.visible=true;
        
       }
      
       
        // arrow.x=1500;
        // arrow.y=450;
        // arrow_y=450;
        // arrow.visible=true;
        currentobj=null;
      }


      
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
