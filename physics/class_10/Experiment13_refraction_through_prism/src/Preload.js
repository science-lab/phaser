var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrows","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');

///////////////////////////Prism////////////////////////////////////
      this.game.load.atlasJSONHash('arrow', 'assets/Prism/arrow.png', 'assets/Prism/arrow.json');
        this.game.load.image('paper','assets/Prism/White_paper.png');    
        this.game.load.image('dir_arrow','assets/Prism/dir_arrow.png'); 
        this.game.load.image('paper_nail','assets/Prism/Nail.png');    
        this.game.load.image('paper_nail_top','assets/Prism/Paper_nail_top.png');  

        this.game.load.image('nail_top','assets/Prism/Nail_top.png'); 
        this.game.load.image('nail','assets/Prism/Nail_Side_view.png'); 

        this.game.load.image('scale','assets/Prism/Scale.png');
        this.game.load.image('pencil','assets/Prism/Pencil.png');
        this.game.load.image('protractor','assets/Prism/Protractor.png');
      this.game.load.image('prism','assets/Prism/Prism_top.png');
      this.game.load.image('prism_side','assets/Prism/Prism_side.png');
      this.game.load.image('eye','assets/Prism/Eye.png');
        this.game.load.image('prism_diagram','assets/Prism/prism_diagram.png');

        this.game.load.image('bg_top','assets/Prism/Bg_top_view.png');
        this.game.load.image('bg_side','assets/Prism/Bg_Side_view.png');
        this.game.load.image('drawing_board_top','assets/Prism/drawing_board_top.png');
        
        
        this.game.load.image('drawing_board_side','assets/Prism/drawing_board_side.png');
        this.game.load.image('nails_M','assets/Prism/nails_M.png');
        this.game.load.image('Prism_M','assets/Prism/Prism_M.png');
        
        


      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');  
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Procedure_4','assets/audio/Procedure_4.mp3');
      
      

      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('step_0','assets/audio/step_0.mp3');
      this.game.load.audio('step_1','assets/audio/step_1.mp3');
      this.game.load.audio('step_2','assets/audio/step_2.mp3');
      this.game.load.audio('step_3','assets/audio/step_3.mp3');
      this.game.load.audio('step_4','assets/audio/step_4.mp3');
      this.game.load.audio('step_5','assets/audio/step_5.mp3');
      this.game.load.audio('step_6','assets/audio/step_6.mp3');
      this.game.load.audio('step_7','assets/audio/step_7.mp3');
      this.game.load.audio('step_8','assets/audio/step_8.mp3');
      this.game.load.audio('step_9','assets/audio/step_9.mp3');
      this.game.load.audio('step_10','assets/audio/step_10.mp3');
      this.game.load.audio('step_11','assets/audio/step_11.mp3');
      this.game.load.audio('step_12','assets/audio/step_12.mp3');
      this.game.load.audio('step_12_2','assets/audio/step_12_2.mp3');
      this.game.load.audio('step_12_3','assets/audio/step_12_3.mp3');
      this.game.load.audio('step_13','assets/audio/step_13.mp3');
      this.game.load.audio('step_14','assets/audio/step_14.mp3');
      this.game.load.audio('step_15','assets/audio/step_15.mp3');
      this.game.load.audio('step_16','assets/audio/step_16.mp3');
      this.game.load.audio('step_17','assets/audio/step_17.mp3');
      this.game.load.audio('step_18','assets/audio/step_18.mp3');
      this.game.load.audio('step_19','assets/audio/step_19.mp3');
      this.game.load.audio('step_20','assets/audio/step_20.mp3');
      this.game.load.audio('step_21','assets/audio/step_21.mp3');
      this.game.load.audio('step_22','assets/audio/step_22.mp3');
      this.game.load.audio('step_23','assets/audio/step_23.mp3');
      this.game.load.audio('step_24','assets/audio/step_24.mp3');
      this.game.load.audio('step_25','assets/audio/step_25.mp3');
      this.game.load.audio('step_26','assets/audio/step_26.mp3');
      this.game.load.audio('step_27','assets/audio/step_27.mp3');
      this.game.load.audio('step_28','assets/audio/step_28.mp3');
      this.game.load.audio('step_29','assets/audio/step_29.mp3');
      this.game.load.audio('step_30','assets/audio/step_30.mp3');
      this.game.load.audio('step_30_2','assets/audio/step_30_2.mp3');
      this.game.load.audio('step_30_3','assets/audio/step_30_3.mp3');
      this.game.load.audio('step_31','assets/audio/step_31.mp3');
      this.game.load.audio('step_32','assets/audio/step_32.mp3');
      this.game.load.audio('step_32_2','assets/audio/step_32_2.mp3');
      this.game.load.audio('step_32_3','assets/audio/step_32_3.mp3');
      this.game.load.audio('step_33','assets/audio/step_33.mp3');
      this.game.load.audio('step_33_2','assets/audio/step_33_2.mp3');
      this.game.load.audio('step_33_3','assets/audio/step_33_3.mp3');
      this.game.load.audio('step_34','assets/audio/step_34.mp3');
      this.game.load.audio('step_34_2','assets/audio/step_34_2.mp3');
      this.game.load.audio('step_34_3','assets/audio/step_34_3.mp3');
      this.game.load.audio('step_35','assets/audio/step_35.mp3');
      this.game.load.audio('step_35_2','assets/audio/step_35_2.mp3');
      this.game.load.audio('step_35_3','assets/audio/step_35_3.mp3');
      this.game.load.audio('step_36','assets/audio/step_36.mp3');
      this.game.load.audio('step_37','assets/audio/step_37.mp3');
      this.game.load.audio('step_38','assets/audio/step_38.mp3');
      this.game.load.audio('step_39','assets/audio/step_39.mp3');
      this.game.load.audio('step_40','assets/audio/step_40.mp3');
      



      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
      //this.game.state.start("Experiment_3");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

