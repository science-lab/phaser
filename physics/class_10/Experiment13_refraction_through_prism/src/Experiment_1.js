var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;


var line1;
var pencilMoveFlag;
var protractorMoveFlag;
var linecount;
var currentx;
var currenty;
var outlineFlag;
var outLineCount;
var lineName;
var dragFlag;
var prismFlag;
var counter;
//////////////////////lines////////////////////////////
var line_prismLeft;
var line_prismRight;
var line_prismBottom;
var line_NN;
var line_MM;
var line_PE;
var line_EH;
var line_EF;
var line_FS;
var line_GF;
var x1;
var y1;
var x2;
var y2;
///////////////////////////////////////
}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);

 pencilMoveFlag=false;
 protractorMoveFlag=false;
 outlineFlag=false;
 outLineCount=0;
 lineName="";
 dragFlag=false;
 
//////////////////////lines////////////////////////////
line_prismLeft=null;
line_prismRight=null;
line_prismBottom=null;
line_NN=null;
line_MM=null;
line_PE=null;
line_EH=null;
line_EF=null;
line_FS=null;
line_GF=null;
x1=0;
x2=0;
y1=0;
y2=0;


////////////////////////////



 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 linecount=1;
 currentx=0;
 currenty=0;
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg1');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "26px Segoe UI", fill: "#0000FF", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  prismFlag=false;
  prismState1="stage1";
  this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {

        counter=0;
        bg_top=this.game.add.sprite(0,0, 'bg_top');
        bgGroup=this.game.add.group();
        bgGroup.add(bg_top);
        var objBox = this.game.add.graphics(0, 0);
        objBox.lineStyle(8,0xC04000,.8);
        objBox.beginFill(0xE67451,.5);//BCC6CC,FFFFC2
        objBox.drawRoundedRect(0, 0, 300, 820,15);
        objBox.x=30;
        objBox.y=210; 
        
        
        drawing_board_top=this.game.add.sprite(1050,630, 'drawing_board_top');
        drawing_board_top.scale.setTo(.535,.26);
        drawing_board_top.anchor.set(.5);
        drawing_board_top.alpha=1;
        bgGroup.add(drawing_board_top);
        paper1=this.game.add.sprite(1050,620, 'paper');
        paper1.scale.setTo(2.3,1.1);
        paper1.anchor.set(.5);
        paper1.alpha=0;
        bgGroup.add(paper1);
        paper=this.game.add.sprite(60,250, 'paper');
        paper.scale.setTo(.4,.3);
        paper.xp=60;
        paper.yp=250;
        bgGroup.add(paper);

        Nail_top1=this.game.add.sprite(385,220, 'paper_nail_top');
        Nail_top1.scale.set(1);
        bgGroup.add(Nail_top1);
        Nail_top2=this.game.add.sprite(1680,220, 'paper_nail_top');
        Nail_top2.scale.set(1);
        bgGroup.add(Nail_top2);
        Nail_top3=this.game.add.sprite(1680,980, 'paper_nail_top');
        Nail_top3.scale.set(1);
        bgGroup.add(Nail_top3);
        Nail_top4=this.game.add.sprite(385,980, 'paper_nail_top');
        Nail_top4.scale.set(1);
        bgGroup.add(Nail_top4);
        Nail_top1.visible=false;
        Nail_top2.visible=false;
        Nail_top3.visible=false;
        Nail_top4.visible=false;

        paper_nail1=this.game.add.sprite(250,520, 'paper_nail');
        bgGroup.add(paper_nail1);
        paper_nail2=this.game.add.sprite(20,0, 'paper_nail');//270,550,
        paper_nail3=this.game.add.sprite(0,20, 'paper_nail');
        paper_nail4=this.game.add.sprite(20,20, 'paper_nail');
        paper_nail5=this.game.add.sprite(0,0, 'collider');
        paper_nail1.addChild(paper_nail2);

        paper_nail1.addChild(paper_nail3);
        paper_nail1.addChild(paper_nail4);
        paper_nail1.addChild(paper_nail5);
        paper_nail5.alpha=0;
        //paper_nail1.x=1000;
        paper_nail1.scale.set(1.5);
        paper_nail1.xp=250;
        paper_nail1.yp=520;
        // Nail2.scale.set(1.5);
        // Nail3.scale.set(1.5);
        // Nail4.scale.set(1.5);
        prism=this.game.add.sprite(150,600, 'prism');
        prism.anchor.set(.5);
        prism.xp=150;
        prism.yp=600;
        //
        //bgGroup.add(prism);
        //prism.x=paper1.x;
        //prism.y=paper1.y;
        //prism.scale.set(1.5);
        prism1=this.game.add.sprite(1050,620, 'prism');
        prism1.anchor.set(.5);
        prism1.scale.set(1.5);
        prism1.xp=1050;
        prism1.yp=620;
        prism1.visible=false;
        bgGroup.add(prism1);

        pointE=this.game.add.sprite(972,620, 'bullet_b');
        pointE.anchor.set(.5);
        pointE.scale.set(.8);
        pointE.visible=false;

        E=this.game.add.text(975,630,"E",lablel_fontStyle);
        bgGroup.add(E);
        E.visible=false;

        pointF=this.game.add.sprite(1146,652, 'bullet_b');
        pointF.anchor.set(.5);
        pointF.scale.set(.8);
        pointF.visible=false;

        F=this.game.add.text(1140,655,"F",lablel_fontStyle);
        bgGroup.add(F);
        F.visible=false;

        
        bgGroup.add(pointE);
        pointN=this.game.add.sprite(812,527, 'bullet_b');
        pointN.anchor.set(.5);
        pointN.scale.set(.8);
        pointN.visible=false;
        bgGroup.add(pointN);

        N=this.game.add.text(800,532,"N",lablel_fontStyle);
        bgGroup.add(N);
        N.visible=false;
        N1=this.game.add.text(1015,657,"N'",lablel_fontStyle);
        bgGroup.add(N1);
        N1.visible=false;

        pointM=this.game.add.sprite(1303,560, 'bullet_b');//1288,527
        pointM.anchor.set(.5);
        pointM.scale.set(.8);
        pointM.visible=false;
        bgGroup.add(pointM);

        M=this.game.add.text(1300,567,"M",lablel_fontStyle);
        bgGroup.add(M);
        M.visible=false;
        M1=this.game.add.text(1080,687,"M'",lablel_fontStyle);
        bgGroup.add(M1);
        M1.visible=false;

        pointP=this.game.add.sprite(793,619, 'bullet_b');//810,711
        pointP.anchor.set(.5);
        pointP.scale.set(.8);
        pointP.visible=false;
        bgGroup.add(pointP);

        dir_arrow1=this.game.add.sprite(850,604, 'dir_arrow');//810,711
        dir_arrow1.tint="black";
        dir_arrow1.visible=false;
        dir_arrow2=this.game.add.sprite(1050,619, 'dir_arrow');//810,711
        dir_arrow2.tint="black";
        dir_arrow2.angle=10;
        dir_arrow2.visible=false;
        dir_arrow3=this.game.add.sprite(1215,700, 'dir_arrow');//810,711
        dir_arrow3.tint="black";
        dir_arrow3.angle=40;  
        dir_arrow3.visible=false;

        P=this.game.add.text(793,624,"P",lablel_fontStyle);//795,670
        bgGroup.add(P);
        P.visible=false;

        nail_topP=this.game.add.sprite(793,619, 'nail_top');//795,668//
        nail_topP.anchor.set(.5);
        nail_topP.visible=false;


       
        
        
        pointS=this.game.add.sprite(1274,778, 'bullet_b');//1298,683
        pointS.anchor.set(.5);
        pointS.scale.set(.8);
        pointS.visible=false;
        bgGroup.add(pointP);
        S=this.game.add.text(1270,783,"S",lablel_fontStyle);
        bgGroup.add(S);
        S.visible=false;

        nail_topS=this.game.add.sprite(1274,778, 'nail_top');
        nail_topS.anchor.set(.5);
        nail_topS.visible=false;

        nailP=this.game.add.sprite(290,850, 'nail');
        nailP.anchor.set(.5);
        nailP.scale.set(1.4);
        nailP.xp=290;
        nailP.yp=850;
        nailP.events.onDragStart.add(function() {this.gameC(nailP)}, this);
        nailP.events.onDragStop.add(function() {this.gameC1(nailP)}, this);
        this.game.physics.arcade.enable(nailP);
        nailQ=this.game.add.sprite(260,850, 'nail');
        nailQ.anchor.set(.5);
        nailQ.scale.set(1.4);
        nailQ.xp=260;
        nailQ.yp=850;
        nailQ.events.onDragStart.add(function() {this.gameC(nailQ)}, this);
        nailQ.events.onDragStop.add(function() {this.gameC1(nailQ)}, this);
        this.game.physics.arcade.enable(nailQ);
        nailR=this.game.add.sprite(230,850, 'nail');
        nailR.anchor.set(.5);
        nailR.scale.set(1.4);
        nailR.xp=230;
        nailR.yp=850;
        nailR.events.onDragStart.add(function() {this.gameC(nailR)}, this);
        nailR.events.onDragStop.add(function() {this.gameC1(nailR)}, this);
        this.game.physics.arcade.enable(nailR);
        nailS=this.game.add.sprite(200,850, 'nail');
        nailS.anchor.set(.5);
        nailS.scale.set(1.4);
        nailS.xp=200;
        nailS.yp=850;
        nailS.events.onDragStart.add(function() {this.gameC(nailS)}, this);
        nailS.events.onDragStop.add(function() {this.gameC1(nailS)}, this);
        this.game.physics.arcade.enable(nailS);

        H=this.game.add.text(1300,625,"H",lablel_fontStyle);//1175,550
        bgGroup.add(H);
        H.visible=false;
        G=this.game.add.text(1070,585,"G",lablel_fontStyle);586
        bgGroup.add(G);
        G.visible=false;

        pointQ=this.game.add.sprite(923,620, 'bullet_b');//915,635
        pointQ.anchor.set(.5);
        pointQ.scale.set(.8);
        pointQ.visible=false;
        bgGroup.add(pointQ);
        Q=this.game.add.text(920,625,"Q",lablel_fontStyle);//915,640
        bgGroup.add(Q);
        Q.visible=false;

        nail_topQ=this.game.add.sprite(923,620, 'nail_top');//915,635
        nail_topQ.anchor.set(.5);
        nail_topQ.visible=false;
       
        pointR=this.game.add.sprite(1188,693, 'bullet_b');;//1169,635
        pointR.anchor.set(.5);
        pointR.scale.set(.8);
        pointR.visible=false;
        bgGroup.add(pointR);
        R=this.game.add.text(1185,697,"R",lablel_fontStyle);
        bgGroup.add(R);
        R.visible=false;

        nail_topR=this.game.add.sprite(1188,693, 'nail_top');
        nail_topR.anchor.set(.5);
        nail_topR.visible=false;
        
        scales=this.game.add.sprite(180,720, 'scale');
        scales.scale.setTo(.4,.7);
        scales.anchor.setTo(.5,0);
        scales.xp=180;
        scales.yp=720;
        //scales.tint='0x000000';
        //scales.alpha=.5;
        //scales.visible=false;

        pencil_1=this.game.add.sprite(50,950, 'pencil');
        pencil_1.scale.set(.5);
        pencil_1.anchor.setTo(0,1);
        pencil_1.xp=50;
        pencil_1.yp=950;
        

        protractor_1=this.game.add.sprite(200,1000, 'protractor');
        protractor_1.scale.set(.5);
        protractor_1.anchor.setTo(.5,1);
        protractor_1.xp=200;
        protractor_1.yp=1000;
        protractor_1.events.onDragStart.add(function() {this.gameC(protractor_1)}, this);
        protractor_1.events.onDragStop.add(function() {this.gameC1(protractor_1)}, this);
        this.game.physics.arcade.enable(protractor_1);

        protractor=this.game.add.sprite(800,1000, 'protractor');
        //protractor.scale.set(.5);
        protractor.visible=false;
        protractor.anchor.setTo(.5,.95);//
        protractor.xp=800;
        protractor.yp=1000;
        protractor.events.onInputDown.add(this.protractorMove,this); 
        protractor.events.onInputUp.add(this.protractorStop,this);
        protractor.inputEnabled = true;

        //paper1.events.onInputDown.add(this.clickPaper,this); 
        // C1=this.game.add.text(640,550,"C\u2081",lablel_fontStyle);//1040 605
        // C1.visible=false;
        // C2=this.game.add.text(1440,550,"C\u2082",lablel_fontStyle);//1040 605
        // C2.visible=false;

      //////////////////////////////////////////////////////////////////////////////  
        
      collider=drawing_board_top;//this.game.add.sprite(1000,500, 'collider');//
      this.game.physics.arcade.enable(collider);
      //collider.scale.setTo(1.3,2.5);
      collider.inputEnabled = true;
      collider.enableBody =true;
      //collider.alpha=0;//.5
      ///////////////////////////bgGroup.add(collider);
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      
      //arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  


      pencil=this.game.add.sprite(1000,450, 'pencil');
      pencil.scale.set(.75);
      pencil.anchor.setTo(0,1);
      pencil.xp=100;
      pencil.yp=950;
       pencil.events.onInputDown.add(this.pencilMove,this); 
       pencil.events.onInputUp.add(this.pencilStop,this);
       pencil.inputEnabled = true;
       //pencil.
       //pencil.input.enableDrag(true);
       pencil.visible=false;
       lineGroup=this.game.add.group();
       pencilGroup=this.game.add.group();
       
       pencilGroup.add(nail_topP);
       pencilGroup.add(nail_topQ);
       pencilGroup.add(nail_topR);
       pencilGroup.add(nail_topS);
       
       //pencilGroup.add(pencil_1);
       pencilGroup.add(pencil);
      // pencilGroup.add(scales);
       //pencilGroup.add(protractor_1);
       arc_i=this.drawArc(25);
          arc_i.alpha=0;
          arc_i.angle=-200;
          arc_i.x=950;
          arc_i.y=610;
          arc_i_angle=this.game.add.text(910,590,"i",lablel_fontStyle);
          arc_i_angle.visible=false;

          
          arc_r=this.drawArc(20);
          arc_r.alpha=0;
          arc_r.angle=-5;
          arc_r.x=1000;
          arc_r.y=630;
          arc_r_angle=this.game.add.text(1025,625,"r",lablel_fontStyle);
          arc_r_angle.visible=false;

          arc_e=this.drawArc(45);
          arc_e.alpha=0;
          arc_e.angle=-30;
          arc_e.x=1135;
          arc_e.y=658;
          arc_e_angle=this.game.add.text(1185,645,"e",lablel_fontStyle);
          arc_e_angle.visible=false;

          arc_D=this.drawArc(25);
          arc_D.visible=false;
          arc_D.angle=-30;
          arc_D.x=1110;
          arc_D.y=630;
          arc_D_angle=this.game.add.text(1135,618,"D",lablel_fontStyle);
          arc_D_angle.visible=false;

          arc_A=this.drawArc(40);
          arc_A.visible=false;
          arc_A.angle=60;
          arc_A.x=1049;
          arc_A.y=480;
          arc_A_angle=this.game.add.text(1042,520,"A",lablel_fontStyle);
          arc_A_angle.visible=false;

//////////////////////////////////////////////////////////////////////////////////////////////
        bg2Group=this.game.add.group();
        bg2=this.game.add.sprite(0,-100, 'bg_side');
        bg2.scale.setTo(2.5,2.5);
        bg2Group.add(bg2);
        nailC=this.game.add.sprite(920,845, 'nail');
        nailC.anchor.set(.5);
        nailC.scale.set(1.4);
        nailD=this.game.add.sprite(920,885, 'nail');
        nailD.anchor.set(.5);
        nailD.scale.set(1.4);
        bg2Group.add(nailC);
        bg2Group.add(nailD);
        prism_side=this.game.add.sprite(900,800, 'prism_side');
        bg2Group.add(prism_side);
        prism_side.anchor.set(.5);
        prism_side.scale.set(2.5);
        bg2Group.visible=false;

        materialsGroup=this.game.add.group();

        materialsGroup.add(objBox);
        materialsGroup.add(paper);
        materialsGroup.add(prism);
        materialsGroup.add(paper_nail1);
        materialsGroup.add(nailR);
        materialsGroup.add(nailS);
        
       //pencilGroup.add(pencil);


       materialsGroup.add(scales);
       materialsGroup.add(pencil_1);
       materialsGroup.add(protractor_1);
       materialsGroup.add(dialog_box);
       materialsGroup.add(dialog);
      


       arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
        arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
        arrow.animations.play('anim');
        arrow.anchor.set(.5);
      arrow.visible=false;
        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/
//////////////////////////////////////////////////////////
        bmd1 = this.game.add.bitmapData(1920,1080);
        var color = 'black';

        bmd1.ctx.beginPath();
        bmd1.ctx.lineWidth = "1";
        bmd1.ctx.strokeStyle = color;
        bmd1.ctx.stroke();
        sprite = this.game.add.sprite(0, 0, bmd1);
        /////////////////////////////Shortcut///////////////////////////////
      //   //this.drawVLine(pointP.x,pointP.y,pointE.x,pointE.y);//PE
      // //   this.drawVLine(972,620,1172,568);//EH
      // //this.drawVLine(pointE.x,pointE.y,pointF.x,pointF.y);//ef
      // //this.drawVLine(pointF.x,pointF.y,pointS.x,pointS.y);//FS
      // //this.drawVLine(1010,586,pointF.x,pointF.y);//GF
      //     //this.drawVLine(pointE.x,pointE.y,1127,530);//EH
      //   paper1.alpha=1;
      //   pointE.visible=true;
      // //   collider=this.game.add.sprite(paper1.x-100,paper1.y, 'A_Blue');
      // //  collider.alpha=0.1;
      // //  collider.anchor.set(.5);
      // //  this.game.physics.arcade.enable(collider);
      // //   collider.scale.setTo(2,2);
      // //   collider.inputEnabled = true;
      // //   collider.enableBody =true;
      // pointM.visible=true;
      //   this.drawVLine(1303,560,1091,683);//MM'
      //   this.drawVLine(812,527,1030,654);//NN'
      //   this.drawVLine(895,755,1050,485);///left
      //   this.drawVLine(1050,485,1205,755);//right
      //   this.drawVLine(895,755,1205,755);//bottom

      //   scales.events.onDragStart.add(function() {this.gameC(scales)}, this);
      //   scales.events.onDragStop.add(function() {this.gameC1(scales)}, this);
      //  this.game.physics.arcade.enable(scales);
      //  pencil_1.events.onDragStart.add(function() {this.gameC(pencil_1)}, this);
      //  pencil_1.events.onDragStop.add(function() {this.gameC1(pencil_1)}, this);
      //  this.game.physics.arcade.enable(pencil_1);
      //  scales.inputEnabled = true;
      //  scales.input.useHandCursor = true;
      //  scales.input.enableDrag(true);
      //  arrow.x=scales.x+10;
      //  arrow.y=scales.y-50;
      //  //arrow_y=arrow.y;
      //  arrow.visible=true;
      //  collider.x=pointF.x;
      //     collider.y=pointF.y;
      //     pointR.visible=true;
      //     pointS.visible=true;
      //  lineName="GF";

      //  protractor_1.events.onDragStart.add(function() {this.gameC(protractor_1)}, this);
      //  protractor_1.events.onDragStop.add(function() {this.gameC1(protractor_1)}, this);
      //  this.game.physics.arcade.enable(protractor_1);
      //  protractor_1.inputEnabled = true;
      //  protractor_1.input.useHandCursor = true;
      //  protractor_1.input.enableDrag(true);

      
       //arrow.x=protractor_1.x+10;
       //arrow.y=protractor_1.y-140;
       //collider.x=pointE.x;
       //collider.y=pointE.y;



       
      //  /////////////////
      //  P.visible=true;
      //  pointP.visible=true;
      //  Q.visible=true;
      //  pointQ.visible=true;
      //  R.visible=true;
      //  pointR.visible=true;
      //  S.visible=true;
      //  pointS.visible=true;

      // //  nail_topP.visible=true;
      // //  nail_topQ.visible=true;
      // //  nail_topR.visible=true;
      // //  nail_topS.visible=true;
      // //  prism1.visible=true;
      // //  scales.events.onDragStart.add(function() {this.gameC(scales)}, this);
      // //  scales.events.onDragStop.add(function() {this.gameC1(scales)}, this);
      // //   this.game.physics.arcade.enable(scales);
      // //   pencil_1.events.onDragStart.add(function() {this.gameC(pencil_1)}, this);
      // //   pencil_1.events.onDragStop.add(function() {this.gameC1(pencil_1)}, this);
      // //   this.game.physics.arcade.enable(pencil_1);
      // //   this.nextFunction_bg1();
      // //  nailP.inputEnabled = true;
      // //  nailP.input.useHandCursor = true;
      // //  nailP.input.enableDrag(true);
      // //  arrow.x=nailP.x;
      // //  arrow.y=nailP.y-70;
      // //  arrow_y=arrow.y;
      // //  arrow.visible=true;
      // //  collider.x=pointP.x;
      //     collider.y=pointP.y;
        
      // pointF=this.game.add.sprite(1146,652, 'bullet_b');
      //     pointF.anchor.set(.5);
      //     pointF.scale.set(.8);
      
      //     pointR=this.game.add.sprite(1188,693, 'bullet_b');
      //     pointR.anchor.set(.5);
      //     pointR.scale.set(.8);
      //     pointS=this.game.add.sprite(1274,778, 'bullet_b');
      //     pointS.anchor.set(.5);
      //     pointS.scale.set(.8);
       //    lEF=this.drawVLine(pointE.x,pointE.y,pointF.x,pointF.y);
       //    lFS=this.drawVLine(pointF.x,pointF.y,pointS.x,pointS.y);
       //    line_PE=this.drawVLine(pointP.x,pointP.y,pointE.x,pointE.y);
       //    lGF=this.drawVLine(1084,589,pointS.x,pointS.y);
        //   lEH=this.drawVLine(pointE.x,pointE.y,1300,620);
       
      //      protractor.visible=true;
      //      protractor.x=pointF.x;
      //      protractor.y=pointF.y;
      // //     //lineName="";
      //      protractor.inputEnabled=true;
          
    },
    clickPaper:function(){
      
    },
    drawLine:function(){ //this.drawVLine(550,480,1050,480);
      bmd1.clear();
      bmd1.ctx.beginPath();
      bmd1.ctx.beginPath();
      bmd1.ctx.moveTo(550, 480);
      bmd1.ctx.lineTo(this.game.input.activePointer.x , this.game.input.activePointer.y);
      bmd1.ctx.lineWidth = 2;
      bmd1.ctx.stroke();
      bmd1.ctx.closePath();
      bmd1.render();
      
      
      //bmd.refreshBuffer();

      
    },
    drawArc:function(angle){
      var arc1=this.game.add.graphics(0, 0);
      arc1.lineStyle(2, 0x000000);
     // arc1.beginFill(0xFF3300);
      arc1.arc(0, 0, angle, 0, 1, false);
      //arc1.x=xp;
      //arc1.yp=yp;
      return arc1;
    },
    pencilMove:function(){
      //console.log("hhhhhhhhhhhh");
      pencilMoveFlag=true;
      if(linecount==1){
        //arrow.x=1620;
        //arrow.y=500;
        //arrow_y=arrow.y
        arrow.visible=false;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
        
      //pointer_currentx=this.game.input.activePointer.x;
    },

    pencilStop:function(){
      pencilMoveFlag=false;
      //pointer_currentx=this.game.input.activePointer.x;
    },
    protractorMove:function(){
      //console.log("hhhhhhhhhhhh");
      protractorMoveFlag=true;
      if(linecount==1){
        //arrow.x=1620;
        //arrow.y=500;
        //arrow_y=arrow.y
        arrow.visible=false;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
        
      //pointer_currentx=this.game.input.activePointer.x;
    },

    protractorStop:function(){
      protractorMoveFlag=false;
      //pointer_currentx=this.game.input.activePointer.x;
    },
    
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
       voice.destroy(true);
       voice=this.game.add.audio("step_1",1);
       voice.play();
       dialog.text="Drag the white sheet to the drawing board.";
       paper.events.onDragStart.add(function() {this.gameC(paper)}, this);
       paper.events.onDragStop.add(function() {this.gameC1(paper)}, this);
       this.game.physics.arcade.enable(paper);
       paper.inputEnabled = true;
       paper.input.useHandCursor = true;
       paper.input.enableDrag(true);
       arrow.visible=true;
      //////////////////////////////////////////
      //this.ShowReactions();
    },
    
  gameC:function(obj)
  {
    //obj.angle=0;
    //console.log(lineName);
    dragFlag=true;
    obj.body.enable = false;
    currentobj=obj;
    arrow.visible=false;
    //console.log("drag");
      if(obj==pencil_1){
        
       pencil_1.scale.set(.75);
       arrow.visible=true;
       if(lineName=="prismLeft"){
          //pencil_1.angle=-45;
          arrow.x=900;
          arrow.y=700;
        }else if(lineName=="pointN"){
          arrow.x=780;
          arrow.y=510;
          arrow.angle=-60;
          //pencil_1.angle=-45;
          //arrow.visible=false;
        }else if(lineName=="pointE"){
          arrow.x=920;
          arrow.y=620;
          arrow.angle=-90;
          //pencil_1.angle=-45;
        }else if(lineName=="NN'"){
          arrow.x=pointN.x;
          arrow.y=pointN.y-50;
          arrow.angle=0;
        }else if(lineName=="pointF"){
          arrow.x=1180;
          arrow.y=620;
          arrow.angle=90;
          
          //pencil_1.angle=45;
        }else if(lineName=="pointM" || lineName=="pointS"){
          arrow.x=pointM.x+50;
          arrow.y=pointM.y;
          arrow.angle=90;
          //pencil_1.angle=45;
        }else if(lineName=="MM'"){
          arrow.x=pointM.x;
          arrow.y=pointM.y-50;
          //pencil_1.angle=-45;
        }else if(lineName=="pointP"){
          arrow.x=pointP.x-50;
          arrow.y=pointP.y;
          arrow.angle=-90;
          //pencil_1.angle=-90;
        }else if(lineName=="PE"){
          arrow.x=pointP.x;
          arrow.y=pointP.y-50;
          arrow.angle=0;
          //pencil_1.angle=45;
        }else if(lineName=="EH" || lineName=="EF"){
          arrow.x=pointE.x;
          arrow.y=pointE.y-50;
          arrow.angle=0;
          //pencil_1.angle=45;
        }else if(lineName=="FS"){
          arrow.x=pointF.x;
          arrow.y=pointF.y-50;
          arrow.angle=0;
          //pencil_1.angle=45;
        }else if(lineName=="GF"){
          arrow.x=G.x+10;
          arrow.y=G.y-20;
          arrow.angle=0;
          //pencil_1.angle=45;
        }
       //arrow_y=arrow.y;
      
     }else if(obj==protractor_1){
       protractor_1.scale.set(1);
        if(lineName=="NN'"  || lineName=="PE"){
          arrow.x=980;
          arrow.y=500;
        }else if(lineName=="pointN" || lineName=="pointP"){
          arrow.x=pointE.x-60;
          arrow.y=620;
          arrow.angle=-90;
        }else if(lineName=="pointM" || lineName=="pointS"){
          arrow.x=pointF.x+60;
          arrow.y=pointF.y;
          arrow.angle=90;
        }else if(lineName=="MM'"){
          arrow.x=1150;
          arrow.y=500;
          //arrow.angle=90;
        }else if(lineName=="angle_i" || lineName=="angle_r"){
          arrow.x=970;
          arrow.y=560;
        }else if(lineName=="angle_e"){
          arrow.x=pointF.x;
          arrow.y=570;
          //arrow.angle=90;
        }else if(lineName=="angle_A"){
          arrow.x=prism1.x;
          arrow.y=prism1.y-200;
        }else if(lineName=="angle_D"){
          arrow.x=G.x+50;
          arrow.y=G.y-10;
        }

        arrow.visible=true;
     }else if(obj==scales){
       scales.scale.set(1);
      if(lineName=="NN'"){
        arrow.x=950;
        arrow.y=550;
      }else if(lineName=="MM'"){
        arrow.x=1150;
        arrow.y=550;
      }else if(lineName=="PE"){
        arrow.x=pointP.x;
        arrow.y=pointP.y-50;
      }else if(lineName=="FS"){
        arrow.x=pointR.x;
        arrow.y=pointR.y-50;
      }else if(lineName=="EH" || lineName=="EF"){
        arrow.x=pointE.x;
        arrow.y=pointE.y-50;
      }else if(lineName=="GF"){
        arrow.x=pointF.x;
        arrow.y=pointF.y-50;
      }
      arrow.visible=true;
    }else if(obj==nailP){
      arrow.x=pointP.x;
      arrow.y=pointP.y-70;
      arrow.visible=true;
    }else if(obj==nailQ){
      arrow.x=pointQ.x;
      arrow.y=pointQ.y-70;
      arrow.visible=true;
    }else if(obj==nailR){
      arrow.x=nailC.x;
      arrow.y=nailC.y-70;
      arrow.visible=true;
    }else if(obj==nailS){
      arrow.x=nailC.x;
      arrow.y=nailC.y-50;
      arrow.visible=true;
    }else if(obj==prism1){
      arrow.x=prism.x;
      arrow.y=prism.y-150;
      arrow.visible=true;
    }else if(obj==prism){
      //console.log('lllllllllll');
      if(prismFlag){
        arrow.x=prism1.x;
        arrow.y=prism1.y-150;
        arrow.visible=true;
      }
    }
    
  },
  gameC1:function(obj)
  {
    dragFlag=false;
    obj.body.enable =true;
  },
  match_Obj:function(){
    //console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==paper){
      paper.visible=false;
      paper1.alpha=1;

      paper_nail1.events.onDragStart.add(function() {this.gameC(paper_nail1)}, this);
      paper_nail1.events.onDragStop.add(function() {this.gameC1(paper_nail1)}, this);
       this.game.physics.arcade.enable(paper_nail1);
       paper_nail1.inputEnabled = true;
       paper_nail1.input.useHandCursor = true;
       paper_nail1.input.enableDrag(true);
       arrow.x=paper_nail1.x+20;
       arrow.y=paper_nail1.y-50;
       arrow_y=arrow.y;
       arrow.visible=true;
       voice.destroy(true);
       voice=this.game.add.audio("step_2",1);
       voice.play();
       dialog.text="Fix the sheet on the drawing board using drawing pins.";
    }else if(currentobj==paper_nail1){
      
      var xp=paper_nail1.x;
      var yp=paper_nail1.y;
      paper_nail1.visible=false;
      paper_nail1_1=this.game.add.sprite(xp,yp, 'paper_nail');
      paper_nail1_1.scale.set(2);
      tween1=this.game.add.tween(paper_nail1_1).to( {x:385, y:200}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        Nail_top1.visible=true;
        paper_nail1_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail2_1=this.game.add.sprite(xp,yp, 'paper_nail');
      Nail2_1.scale.set(2);
      tween2=this.game.add.tween(Nail2_1).to( {x:1695, y:200}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        Nail_top2.visible=true;
        Nail2_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail3_1=this.game.add.sprite(xp,yp, 'paper_nail');
      Nail3_1.scale.set(2);
      tween3=this.game.add.tween(Nail3_1).to( {x:1695, y:960}, 600, Phaser.Easing.Out, true);
      tween3.onComplete.add(function () {
        Nail_top3.visible=true;
        Nail3_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail4_1=this.game.add.sprite(xp,yp, 'paper_nail');
      Nail4_1.scale.set(2);
      tween4=this.game.add.tween(Nail4_1).to( {x:385, y:980}, 600, Phaser.Easing.Out, true);
      tween4.onComplete.add(function () {
        Nail_top4.visible=true;
        Nail4_1.visible=false;
        //
      }.bind(this));
      prism.events.onDragStart.add(function() {this.gameC(prism)}, this);
      prism.events.onDragStop.add(function() {this.gameC1(prism)}, this);
       this.game.physics.arcade.enable(prism);
       prism.inputEnabled = true;
       prism.input.useHandCursor = true;
       prism.input.enableDrag(true);
       arrow.x=prism.x+10;
       arrow.y=prism.y-130;
       arrow_y=arrow.y;
       arrow.visible=true;
       voice.destroy(true);
       voice=this.game.add.audio("step_3",1);
       voice.play();
       dialog.text="Place the glass prism in the centre of the sheet.";
       
    }else if(currentobj==prism){
      prism.visible=false;
      prism.reset(prism.xp,prism.yp);
      prism.visible=false;
      //prism.destroy(true);
      prism1.visible=true;
      
      if(prismFlag){
        voice.destroy(true);
        voice=this.game.add.audio("step_16",1);
        voice.play();
        dialog.text="Look the images of pins P and Q through the other face AC.";
        this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextFunction_bg2, this);
        arrow.visible=false;
      }else{
        voice.destroy(true);
        voice=this.game.add.audio("step_4",1);
        voice.play();
        dialog.text="Trace the outline of the glass prism using the sharp pencil.";
        pencil_1.events.onDragStart.add(function() {this.gameC(pencil_1)}, this);
        pencil_1.events.onDragStop.add(function() {this.gameC1(pencil_1)}, this);
       this.game.physics.arcade.enable(pencil_1);
       pencil_1.inputEnabled = true;
       pencil_1.input.useHandCursor = true;
       pencil_1.input.enableDrag(true);
       arrow.x=pencil_1.x+50;
       arrow.y=pencil_1.y-180;
       arrow_y=arrow.y;
       arrow.visible=true;
       collider=this.game.add.sprite(paper1.x-150,paper1.y+100, 'A_Blue');
       collider.alpha=0.0;
       collider.anchor.set(.5);
       this.game.physics.arcade.enable(collider);
        collider.scale.setTo(2,2);
        collider.inputEnabled = true;
        collider.enableBody =true;
        bgGroup.add(collider);
      lineName="prismLeft";
      }
    }else if(currentobj==pencil_1){
      //outlineFlag=true;
      pencil_1.visible=false;
      if(lineName=="prismLeft"){
        pencil.x=895;
        pencil.y=750;
        pencil.angle=-45;
        arrow.angle=-145;
        arrow.x=pencil.x+80;
      arrow.y=pencil.y-200;
      }else if(lineName=="pointE"){
        pencil.x=pointE.x;
        pencil.y=pointE.y;
        pencil.angle=-45;
        pointE.visible=true;
        arrow.x=pencil.x-80;
      arrow.y=pencil.y-200;
      arrow.visible=false;
      
        this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointE, this); 
      }else if(lineName=="pointN"){
        arrow.x=pencil.x-30;
      arrow.y=pencil.y-200;
        pencil.x=pointN.x;
        pencil.y=pointN.y;
        pointN.visible=true;
        pencil.angle=-45;
        arrow.visible=false;
        this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointN, this);
      }else if(lineName=="NN'"){
        pencil.x=pointN.x;
        pencil.y=pointN.y;
        x1=pencil.x;
        y1=pencil.y;
        pencil.angle=0;
        arrow.angle=-60;
        arrow.x=pencil.x+150;
      arrow.y=pencil.y-100;
      arrow.visible=true;
      }else if(lineName=="pointF"){
        pencil.x=pointF.x;
        pencil.y=pointF.y;
        //pencil.angle=-45;
        pointF.visible=true;
        arrow.x=pencil.x-80;
        arrow.y=pencil.y-200;
        arrow.visible=false;
        this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointE, this);
      }else if(lineName=="pointM"){
        arrow.x=pencil.x-80;
        arrow.y=pencil.y-200;
        pencil.x=pointM.x;
        pencil.y=pointM.y;
        pointM.visible=true;
        //pencil.angle=-45;
        arrow.visible=false;
        this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointN, this);
      }else if(lineName=="MM'"){
        pencil.x=pointM.x;//1130;
        pencil.y=pointM.y;//600;
        x1=pencil.x;
        y1=pencil.y;
        pencil.angle=-45;
        arrow.x=pencil.x-150;
      arrow.y=pencil.y-150;
      arrow.angle=60;
      }else if(lineName=="pointP"){
        arrow.x=pencil.x-80;
        arrow.y=pencil.y-200;
        pencil.x=pointP.x;
        pencil.y=pointP.y;
        pointP.visible=true;
        pencil.angle=0;
        arrow.visible=false;
        this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointN, this);
      }else if(lineName=="PE"){
        pencil.x=pointP.x;
        pencil.y=pointP.y;
        //pencil.angle=-45;
        arrow.x=pencil.x+180;
      arrow.y=pencil.y-150;
      arrow.angle=-105;
      }else if(lineName=="FS"){
        pencil.x=pointF.x;
        pencil.y=pointF.y;
        //pencil.angle=-45;
        pointF.visible=true;
        arrow.x=pencil.x+150;
        arrow.y=pencil.y-100;
        arrow.angle=-60;
        //arrow.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointE, this);
      }else if(lineName=="EH"){
        pencil.x=pointE.x;
        pencil.y=pointE.y;
        x1=pencil.x;
        y1=pencil.y;
        //pencil.angle=-45;
        arrow.x=pencil.x+150;
        arrow.y=pencil.y-150;
        arrow.angle=-105;
      }else if(lineName=="GF"){
        pencil.x=1086;
        pencil.y=589;
        x1=pencil.x;
        y1=pencil.y;
        //pencil.angle=-45;
        arrow.x=pencil.x+150;
        arrow.y=pencil.y-100;
        arrow.angle=-60;
       // collider.x-=100;
        //arrow.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointE, this);
      }else if(lineName=="EF"){
        //console.log("///////////////");
        pencil.x=pointE.x;
        pencil.y=pointE.y;
        pencil.angle=0;
        arrow.x=pencil.x+150;
        arrow.y=pencil.y-100;
        arrow.angle=-90;
       // collider.x-=100;
        //arrow.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*.5,this.nextFunction_pointE, this);
      }
      pencil.visible=true;
      
      
    }else if(currentobj==protractor_1){
      protractor_1.visible=false;
      protractor.angle=0
      if(lineName=="pointN" || lineName=="NN'"  || lineName=="pointP" || lineName=="angle_i" || lineName=="angle_r"){
        protractor.x=972;
        protractor.y=620;
        arrow.x=protractor.x-80;
        if(lineName=="angle_r"){
          arrow.x=protractor.x+80;
        }
       arrow.y=protractor.y-230;
      }else if(lineName=="MM'" || lineName=="pointM" || lineName=="pointS" || lineName=="angle_e"){
        
        protractor.x=pointF.x;
        protractor.y=pointF.y;
        arrow.x=protractor.x+80;
       arrow.y=protractor.y-230;
      }else if(lineName=="angle_A"){
        protractor.x=1050;
        protractor.y=485;
        arrow.x=protractor.x-80;
       arrow.y=protractor.y-230;
      }else if(lineName=="angle_D"){
        protractor.x=1115;
        protractor.y=620;
        arrow.x=protractor.x+80;
       arrow.y=protractor.y-230;
      }
      protractor.inputEnabled=true;
      protractor.visible=true;
      arrow.angle=0;
       arrow.visible=true;
      voice.destroy(true);
        voice=this.game.add.audio("step_8",1);
        voice.play();
        dialog.text="Rotate the protractor to adjust the angle.";
       
      
    }else if(currentobj==scales){
      if(lineName=="NN'"){
        scales.x=pointE.x;
        scales.y=pointE.y;
        scales.angle=30;
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow.visible=true;
        collider.x=820;
        collider.y=530;
      }else if(lineName=="MM'"){
        scales.x=pointF.x;
        scales.y=pointF.y;
        scales.angle=-30;
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow.visible=true;
        collider.x=pointM.x;
        collider.y=pointM.y;
      }else if(lineName=="PE" || lineName=="EH"){
        scales.x=pointE.x;
        scales.y=pointE.y;
        //scales.angle=-30;
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow.visible=true;
        //collider.x=pointF.x-100;
        //collider.y=point;
      }else if(lineName=="FS" || lineName=="GF"){
        scales.x=pointF.x;
        scales.y=pointF.y;
        scales.angle=45;
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow.visible=true;
        //collider.x=pointF.x-100;
        //collider.y=point;
      }else if(lineName=="EF"){
        scales.x=pointE.x;
        scales.y=pointE.y;
        scales.angle=10;
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow.visible=true;
        //collider.x=pointF.x-100;
        //collider.y=point;
      }
    }else if(currentobj==nailP){
      nailP.visible=false;
      nail_topP.visible=true;
      nailQ.inputEnabled = true;
      nailQ.input.useHandCursor = true;
      nailQ.input.enableDrag(true);
      arrow.x=nailQ.x;
      arrow.y=nailQ.y-70;
      arrow.visible=true;
      collider.x=pointQ.x;
      collider.y=pointQ.y;
    }else if(currentobj==nailQ){
      arrow.visible=false;
      nailQ.visible=false;
      nail_topQ.visible=true;
      Q.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step_15",1);
      voice.play();
      dialog.text="Drag the glass prism to the triangle.";
      prism.events.onDragStart.add(function() {this.gameC(prism)}, this);
      prism.events.onDragStop.add(function() {this.gameC1(prism)}, this);
    this.game.physics.arcade.enable(prism);
      prism.inputEnabled = true;
      prism.input.useHandCursor = true;
      prism.input.enableDrag(true);
      arrow.x=prism.x;
      arrow.y=prism.y-70;
      arrow.visible=true;
      prismFlag=true;
      //dialog.text="Look the images of pins P and Q through the other face AC";
      //this.game.time.events.add(Phaser.Timer.SECOND*2,this.nextFunction_bg2, this);
    }else if(currentobj==nailR){
      nail_topR.visible=true;
      nailR.x=920;
      nailR.y=925;
      arrow.x=nailS.x;
      arrow.y=nailS.y-70;
      nailS.inputEnabled = true;
      nailS.input.useHandCursor = true;
      nailS.input.enableDrag(true);
      collider.y+=30;
    }else if(currentobj==nailS){
      nail_topS.visible=true;
      nailS.x=920;
      nailS.y=965;
      arrow.visible=false;
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextFunction_bg1, this);
      voice.destroy(true);
      voice=this.game.add.audio("step_18",1);
      voice.play();
      dialog.text="Now lets draw the refracted line.";
      //arrow.x=nailS.x;
      //arrow.y=nailS.y-70;
    }else if(currentobj==prism1){
      prism1.visible=false;
      prism1.reset(prism1.xp,prism1.yp);
      prism1.visible=false;
      prism.visible=true;
      if(prismState1=="stage1"){
      
      A=this.game.add.text(1040,440,"A",lablel_fontStyle);
      bgGroup.add(A);
      B=this.game.add.text(880,760,"B",lablel_fontStyle);
      bgGroup.add(B);
      C=this.game.add.text(1200,760,"C",lablel_fontStyle);
      bgGroup.add(C);
      voice.destroy(true);
      voice=this.game.add.audio("step_6",1);
      voice.play();
      dialog.text="Mark a point E on the face AB.";
      pencil_1.inputEnabled = true;
      pencil_1.input.useHandCursor = true;
      pencil_1.input.enableDrag(true);
      arrow.x=pencil_1.x+50;
      arrow.y=pencil_1.y-180;
      arrow.visible=true; 
      lineName="pointE";
      collider.x=970;
      collider.y=620;
      }else{
        arrow.x=nail_topP.x;
        arrow.y=nail_topP.y-50;
        arrow.visible=true;
        nail_topP.inputEnabled = true;
        nail_topP.input.useHandCursor = true;
        nail_topP.events.onInputDown.add(this.RemoveNailTopP, this);
        voice.destroy(true);
        voice=this.game.add.audio("step_19",1);
        voice.play();
        dialog.text="Remove the pins P and Q.";
      }
    }
      //////////////////////////////////////////

    
  },
  nextFunction_bg1:function(){
    nailR.visible=false;
    nailS.visible=false;
    bg2Group.visible=false;
    prismState1="stage2";
    voice=this.game.add.audio("step_40",1);
        voice.play();
        dialog.text="Remove the glass prism from the sheet.";
        prism1.events.onDragStart.add(function() {this.gameC(prism1)}, this);
        prism1.events.onDragStop.add(function() {this.gameC1(prism1)}, this);
        this.game.physics.arcade.enable(prism1);
        prism1.inputEnabled = true;
        prism1.input.useHandCursor = true;
        prism1.input.enableDrag(true);
        arrow.x=prism1.x;
        arrow.y=430;
        arrow.visible=true;
        arrow.angle=0;
          collider.x=prism.x;
          collider.y=prism.y;
    // arrow.x=nail_topP.x;
    // arrow.y=nail_topP.y-50;
    // arrow.visible=true;
    // nail_topP.inputEnabled = true;
    // nail_topP.input.useHandCursor = true;
    // nail_topP.events.onInputDown.add(this.RemoveNailTopP, this);
    // voice.destroy(true);
    //   voice=this.game.add.audio("step_19",1);
    //   voice.play();
    //   dialog.text="Remove the pins P and Q";
  },
  RemoveNailTopP:function(){
    nail_topP.visible=false;
    nailP.x=nail_topP.x;
    nailP.y=nail_topP.y;
    nailP.visible=true;
    arrow.visible=false;
    tween1=this.game.add.tween(nailP).to( {y:1500}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          arrow.x=nail_topQ.x;
          arrow.y=nail_topQ.y-50;
          arrow.visible=true;
          nail_topQ.inputEnabled = true;
          nail_topQ.input.useHandCursor = true;
          nail_topQ.events.onInputDown.add(this.RemoveNailTopQ, this);
          collider.y=50;
        }.bind(this));
  },
  RemoveNailTopQ:function(){
    nail_topQ.visible=false;
    nailQ.x=nail_topQ.x;
    nailQ.y=nail_topQ.y;
    nailQ.visible=true;
    arrow.visible=false;
    pointQ.visible=true;
    tween1=this.game.add.tween(nailQ).to( {y:1500}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          arrow.x=nail_topR.x;
          arrow.y=nail_topR.y-50;
          arrow.visible=true;
          nail_topR.inputEnabled = true;
          nail_topR.input.useHandCursor = true;
          nail_topR.events.onInputDown.add(this.RemoveNailTopR, this);
          //ail_topR.alpha=.1;
          voice.destroy(true);
          voice=this.game.add.audio("step_20",1);
          voice.play();
          dialog.text="Remove the other two pins and label the points R and S.";
        }.bind(this));
  },
  RemoveNailTopR:function(){
    nail_topR.visible=false;
    nailR.x=nail_topR.x;
    nailR.y=nail_topR.y;
    nailR.visible=true;
    arrow.visible=false;
    pointR.visible=true;
    R.visible=true;
    tween1=this.game.add.tween(nailR).to( {y:1500}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          arrow.x=nail_topS.x;
          arrow.y=nail_topS.y-50;
          arrow.visible=true;
          nail_topS.inputEnabled = true;
          nail_topS.input.useHandCursor = true;
          nail_topS.events.onInputDown.add(this.RemoveNailTopS, this);
        }.bind(this));
  },
  RemoveNailTopS:function(){
    nail_topS.visible=false;
    nailS.x=nail_topS.x;
    nailS.y=nail_topS.y;
    nailS.visible=true;
    arrow.visible=false;
    pointS.visible=true;
    S.visible=true;
    tween1=this.game.add.tween(nailS).to( {y:1500}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          arrow.x=scales.x;
          arrow.y=scales.y-30;
          arrow.visible=true;
          scales.inputEnabled = true;
          scales.input.useHandCursor = true;
          scales.input.enableDrag(true);
          collider.x=pointR.x;
          collider.y=pointR.y;
          lineName="FS";
          voice.destroy(true);
          voice=this.game.add.audio("step_21",1);
          voice.play();
          dialog.y=40;
          dialog.text="Join the points R and S, the line meets the boundary of the glass prism \nand label the point F.";
        }.bind(this));
  },
  nextFunction_bg2:function(){
    collider.x=920;
    collider.y=880;
    arrow.x=nailR.x;
    arrow.y=nailR.y-70;
    arrow.visible=true;
    bg2Group.visible=true;
    nailR.inputEnabled = true;
    nailR.input.useHandCursor = true;
    nailR.input.enableDrag(true);
    voice.destroy(true);
    voice=this.game.add.audio("step_17",1);
    voice.play();
    dialog.text="Now place two more pins aligned and in front of the pins P and Q.";
  },
  nextFunction_pointN:function(){
    pencil.visible=false;
    pencil_1.reset(pencil_1.xp,pencil_1.yp);
    pencil_1.angle=0;
    pencil_1.scale.set(.5);
    pencil_1.visible=true;
    protractor.visible=false;
    protractor_1.reset(protractor_1.xp,protractor_1.yp);
    protractor_1.angle=0;
    protractor_1.scale.set(.5);
    protractor_1.visible=true;
    voice.destroy(true);
    
    scales.events.onDragStart.add(function() {this.gameC(scales)}, this);
    scales.events.onDragStop.add(function() {this.gameC1(scales)}, this);
    this.game.physics.arcade.enable(scales);
    scales.inputEnabled = true;
    scales.input.useHandCursor = true;
    scales.input.enableDrag(true);
    collider.x=970;
    collider.y=620;
    arrow.x=scales.x+10;
    arrow.y=scales.y-30;
    arrow.angle=0;
    arrow.visible=true;
    if(lineName=="pointN"){
      voice=this.game.add.audio("step_10",1);
    voice.play();
    dialog.text="Draw a straight line NN' through E using the scale.";
      N.visible=true;
      lineName="NN'";
    }else if(lineName=="pointM"){
      voice=this.game.add.audio("step_24",1);
    voice.play();
    dialog.text="Draw a straight line MM' through F using the scale.";
      M.visible=true;
      collider.x=pointF.x;
      lineName="MM'";
    }else if(lineName=="pointP"){
      voice=this.game.add.audio("step_13",1);
    voice.play();
    dialog.text="Draw a straight line to join P and E.";
      //collider.x=pointF.x;
      P.visible=true;
      lineName="PE";
      collider.x=pointP.x;
      collider.y=pointP.y;
    }
    
  },
  nextFunction_pointE:function(){
    pencil.visible=false;
    pencil_1.reset(pencil_1.xp,pencil_1.yp);
    pencil_1.angle=0;
    pencil_1.scale.set(.5);
    pencil_1.visible=true;
    voice.destroy(true);
    
    protractor_1.inputEnabled = true;
    protractor_1.input.useHandCursor = true;
    protractor_1.input.enableDrag(true);
    arrow.x=protractor_1.x+10;
    arrow.y=protractor_1.y-130;
    arrow.angle=0;
    arrow.visible=true;
    if(lineName=="pointE"){
      voice=this.game.add.audio("step_7",1);
      voice.play();
      dialog.text="Use the protractor to mark 90\u00BA on the surface AB.";
      E.visible=true;
      lineName="pointN";
    }else if(lineName=="pointF"){
      // voice=this.game.add.audio("step_4",1);
      // voice.play();
      // dialog.text="Use the protractor to mark 90\u00BA on the surface AB.";
      // F.visible=true;
      // lineName="pointM";
    }
  },

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    //var currentx1=this.game.input.activePointer.x;
    //var currenty1=this.game.input.activePointer.y;
    //counter++;
    // if(counter>300){
    //   counter=0;
    //   //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    // }
      
    if(dragFlag){
      //console.log(currentobj.x+"/../"+currentobj.y)
    }
    /*if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
      
    }*/
    if(pencilMoveFlag){
      this.draw_Lines();
    }
    if(protractorMoveFlag){
      this.rotation_Protractor();
    }
  },
  rotation_Protractor:function(){
    var flag=false;
    if(this.game.input.activePointer.x>=protractor.x && this.game.input.activePointer.y>=protractor.y){
      if(currentx<this.game.input.activePointer.x || currenty>this.game.input.activePointer.y){
        protractor.angle-=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }else if(currentx>this.game.input.activePointer.x || currenty<this.game.input.activePointer.y){
        protractor.angle+=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
      
    }else if(this.game.input.activePointer.x<=protractor.x && this.game.input.activePointer.y<=protractor.y){
      if(currentx<this.game.input.activePointer.x || currenty>this.game.input.activePointer.y){
        protractor.angle+=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }else if(currentx>this.game.input.activePointer.x || currenty<this.game.input.activePointer.y){
        protractor.angle-=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
    }else if(this.game.input.activePointer.x>=protractor.x && this.game.input.activePointer.y<=protractor.y){
      if(currentx<this.game.input.activePointer.x || currenty<this.game.input.activePointer.y){
        protractor.angle+=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }else if(currentx>this.game.input.activePointer.x || currenty>this.game.input.activePointer.y){
        protractor.angle-=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
      
    }else if(this.game.input.activePointer.x<=protractor.x && this.game.input.activePointer.y>=protractor.y){
      if(currentx<this.game.input.activePointer.x || currenty<this.game.input.activePointer.y){
        protractor.angle-=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }else if(currentx>this.game.input.activePointer.x || currenty>this.game.input.activePointer.y){
        protractor.angle+=5;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
      
    }
    //console.log(lineName+"//"+protractor.angle);
    var flag=false;
    if(lineName=="pointN"){
      if(protractor.angle<=-60){
        protractor.angle=-60;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_9",1);
        voice.play();
        dialog.text="Mark a point 90\u00BA and name it N.";
        collider.x=820;
        collider.y=530;
        if(lineName=="pointP"){
          collider.x=780;
          collider.y=650;
        }
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow_y=arrow.y;
        arrow.visible=true;
      }
    }else if(lineName=="pointP"){
      if(protractor.angle<=-150){
        protractor.angle=-150;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_12",1);
        voice.play();
        dialog.text="Mark a point 30\u00BA and name it P.";
        collider.x=820;
        collider.y=530;
        if(lineName=="pointP"){
          collider.x=780;
          collider.y=650;
        }
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow_y=arrow.y;
        arrow.visible=true;
      }
    }else if(lineName=="pointM" || lineName=="pointS"){
      if(protractor.angle>=60){
        protractor.angle=60;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_23",1);
        voice.play();
        dialog.text="Mark a point 90\u00BA and name it M.";
        collider.x=pointM.x;
        collider.y=pointM.y;
        pencil_1.inputEnabled = true;
        pencil_1.input.useHandCursor = true;
        pencil_1.input.enableDrag(true);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow_y=arrow.y;
        arrow.visible=true;
      }
    }else if(lineName=="angle_i"){
      //console.log(protractor.angle);
      if(protractor.angle<=-150){
        protractor.angle=-150;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_30",1);
        voice.play();
        dialog.text="Note that the angle of incidence is 30\u00BA.";
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.check_angle_i, this);
        
      }
    }else if(lineName=="angle_A"){
      //console.log(protractor.angle);
      if(protractor.angle<=-120){
        protractor.angle=-120;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_31",1);
        voice.play();
        dialog.text="Note that the angle of the glass prism is 60\u00BA.";
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.check_angle_A, this);
        
      }
    }else if(lineName=="angle_D"){
      //console.log(protractor.angle);
      if(protractor.angle>=45){
        protractor.angle=45;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_32",1);
        voice.play();
        dialog.text="Note that the angle of deviation is 45\u00BA.";
        this.game.time.events.add(Phaser.Timer.SECOND*4,this.check_angle_D, this);
      }
    }else if(lineName=="angle_r"){
      //console.log(protractor.angle);
      if(protractor.angle>=30){
        protractor.angle=30;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_33",1);
        voice.play();
        dialog.text="Note that the angle of refraction is 20\u00BA";
        this.game.time.events.add(Phaser.Timer.SECOND*4,this.check_angle_r, this);
        
      }
    }else if(lineName=="angle_e"){
     // console.log(protractor.angle);
      if(protractor.angle>=150){
        protractor.angle=150;
        flag=true;
      }
      if(flag){
        protractor.inputEnabled=false;
        protractorMoveFlag=false;
        voice.destroy(true);
        voice=this.game.add.audio("step_34",1);
        voice.play();
        dialog.text="Note that the angle of emergence is 75\u00BA.";
        this.game.time.events.add(Phaser.Timer.SECOND*4,this.check_angle_e, this);
        
      }
    }
  },
  check_angle_i:function(){
    protractor.visible=false;
    protractor_1.reset(protractor_1.xp,protractor_1.yp);
    protractor_1.angle=0;
    protractor_1.scale.set(.5);
    protractor_1.visible=true;
    protractor_1.inputEnabled = true;
    protractor_1.input.useHandCursor = true;
    protractor_1.input.enableDrag(true);
    arrow.x=protractor_1.x;
    arrow.y=protractor_1.y-150;
    arrow.visible=true;
    //arc_i.visible=true;
    //arc_i_angle.visible=true;
    i_text=this.game.add.text(500,820,"Angle of incidence \u2220i = 30\u00BA",lablel_fontStyle);
    lineName="angle_A"
    collider.x=prism1.x;
    collider.y=prism1.y-150;
    voice.destroy(true);
    voice=this.game.add.audio("step_36",1);
    voice.play();
    dialog.text="Measure the angle of the glass prism \u2220A.";
  },
  check_angle_A:function(){
    protractor.visible=false;
    protractor_1.reset(protractor_1.xp,protractor_1.yp);
    protractor_1.angle=0;
    protractor_1.scale.set(.5);
    protractor_1.visible=true;
    protractor_1.inputEnabled = true;
    protractor_1.input.useHandCursor = true;
    protractor_1.input.enableDrag(true);
    arrow.x=protractor_1.x;
    arrow.y=protractor_1.y-150;
    arrow.visible=true;
     arc_A.visible=true;
     arc_A_angle.visible=true;
    A_text=this.game.add.text(500,880,"Angle of the glass prism \u2220A = 60\u00BA",lablel_fontStyle);
    lineName="angle_D"
    collider.x=prism1.x;
    collider.y=prism1.y-150;
    voice.destroy(true);
    voice=this.game.add.audio("step_37",1);
    voice.play();
    dialog.text="Measure the angle of deviation \u2220D.";
  },
  check_angle_D:function(){
    protractor.visible=false;
    protractor_1.reset(protractor_1.xp,protractor_1.yp);
    protractor_1.angle=0;
    protractor_1.scale.set(.5);
    protractor_1.visible=true;
    protractor_1.inputEnabled = true;
    protractor_1.input.useHandCursor = true;
    protractor_1.input.enableDrag(true);
    arrow.x=protractor_1.x;
    arrow.y=protractor_1.y-150;
    arrow.visible=true;
      arc_D.visible=true;
     arc_D_angle.visible=true;
    D_text=this.game.add.text(500,940,"Angle of deviation \u2220D = 45\u00BA",lablel_fontStyle);
    lineName="angle_r";
    collider.x=prism1.x;
    collider.y=prism1.y-150;
    voice.destroy(true);
    voice=this.game.add.audio("step_38",1);
    voice.play();
    dialog.text="Measure the angle of refraction \u2220r.";
  },
  check_angle_r:function(){
    protractor.visible=false;
    protractor_1.reset(protractor_1.xp,protractor_1.yp);
    protractor_1.angle=0;
    protractor_1.scale.set(.5);
    protractor_1.visible=true;
    protractor_1.inputEnabled = true;
    protractor_1.input.useHandCursor = true;
    protractor_1.input.enableDrag(true);
    arrow.x=protractor_1.x;
    arrow.y=protractor_1.y-150;
    arrow.visible=true;
    //arc_i.visible=true;
    //arc_i_angle.visible=true;
    i_text=this.game.add.text(1000,820,"Angle of refraction \u2220r = 20\u00BA",lablel_fontStyle);
    lineName="angle_e"
    collider.x=prism1.x;
    collider.y=prism1.y-150;
    voice.destroy(true);
    voice=this.game.add.audio("step_39",1);
    voice.play();
    dialog.text="Measure the angle of emergence \u2220e.";
  },
  check_angle_e:function(){
    protractor.visible=false;
    protractor_1.reset(protractor_1.xp,protractor_1.yp);
    protractor_1.angle=0;
    protractor_1.scale.set(.5);
    protractor_1.visible=true;
    i_text=this.game.add.text(1000,880,"Angle of emergence \u2220e = 75\u00BA",lablel_fontStyle);
    voice.destroy(true);
        voice=this.game.add.audio("step_35",1);
        voice.play();
        dialog.y=40;
        dialog.text="Click on the next button to see the refraction of the prism, when the \nangle of incidence is 45\u00BA. ";
    play.visible=true;
    // protractor_1.inputEnabled = true;
    // protractor_1.input.useHandCursor = true;
    // protractor_1.input.enableDrag(true);
    // arrow.x=protractor_1.x;
    // arrow.y=protractor_1.y-150;
    // arrow.visible=true;
    // //arc_i.visible=true;
    // //arc_i_angle.visible=true;
    // 
    // lineName="angle_A"
    // collider.x=prism1.x;
    collider.y=prism1.y-150;
  },
  draw_Lines:function(){
    //console.log(lineName);
    if(pencil.visible){
    if(lineName=="prismLeft"){
        if(currentx<this.game.input.activePointer.x && currenty>this.game.input.activePointer.y){
          pencil.x+=6.25;//2.50;
          pencil.y-=10.625;//4.25;
          currentx=this.game.input.activePointer.x;
          currenty=this.game.input.activePointer.y;
          if(line_prismLeft!=null){
            line_prismLeft.destroy(true);
          }
          line_prismLeft=this.drawVLine(895,755,pencil.x,pencil.y);
         // console.log(pencil.x);
          if(pencil.x>=1039){
            outLineCount=1;
            lineName="prismRight";
            pencil.angle=0;
            pencil.y=485;
            pencil.x=1050;
            arrow.visible=true;
            arrow.angle=-30;
            arrow.x=1150;
            if(line_prismLeft!=null){
              line_prismLeft.destroy(true);
            }
            line_prismLeft=this.drawVLine(895,755,1050,485);//left
            
          }
        }
    }else if(lineName=="prismRight"){
      if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y){
        pencil.x+=6.25;//2.5;
        pencil.y+=11;//4.4;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
        if(line_prismRight!=null){
          line_prismRight.destroy(true);
        }
        line_prismRight=this.drawVLine(1050,485,pencil.x,pencil.y);
        //console.log(pencil.y);
        if(pencil.y>=755){
          outLineCount=2;
          lineName="prismBottom";
          arrow.angle=90;
          arrow.visible=true;
          arrow.y=780;
          pencil.x=1200;
          if(line_prismRight!=null){
            line_prismRight.destroy(true);
          }
          line_prismRight=this.drawVLine(1050,485,1205,755);//right
        }
      }
    }else if(lineName=="prismBottom"){
      if(this.game.input.activePointer.x>800 && this.game.input.activePointer.x<1450){
        pencil.x=this.game.input.activePointer.x;
        //currentx=this.game.input.activePointer.x;
        if(pencil.x<=1205){
          if(line_prismBottom!=null){
            line_prismBottom.destroy(true);
          }
          line_prismBottom=this.drawVLine(1205,755,pencil.x,755);
        }

          
        //console.log(pencil.x);
        if(pencil.x<=900){
          outLineCount=3;
          lineName="";
          pencil.x=948;
          if(line_prismBottom!=null){
            line_prismBottom.destroy(true);
          }
          line_prismBottom=this.drawVLine(895,755,1205,755);//bottom
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          voice.destroy(true);
        voice=this.game.add.audio("step_5",1);
        voice.play();
        dialog.text="Remove the prism from the sheet and label the triangle as A, B, C.";
        prism1.events.onDragStart.add(function() {this.gameC(prism1)}, this);
        prism1.events.onDragStop.add(function() {this.gameC1(prism1)}, this);
        this.game.physics.arcade.enable(prism1);
        prism1.inputEnabled = true;
        prism1.input.useHandCursor = true;
        prism1.input.enableDrag(true);
        arrow.x=prism1.x;
        arrow.y=430;
        arrow.visible=true;
        arrow.angle=0;
          collider.x=prism.x;
          collider.y=prism.y;
        }if(pencil.x>1200){
          pencil.x=1200;
        }
      }
    }else if(lineName=="NN'"){
      if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y){
        pencil.x+=10;//4;
        pencil.y+=5.75;//2.3;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
        this.drawVLine(x1,y1,pencil.x,pencil.y);
        x1=pencil.x+4;
        y1=pencil.y+2.3;
        pencil.x=x1;
        pencil.y=y1;
        // if(line_NN!=null){
        //   line_NN.destroy(true);
        // }
        //  bmd=this.game.add.bitmapData(1920,1080);
        //    bmd.ctx.beginPath();
        //   bmd.ctx.lineWidth = 1;
        //    bmd.ctx.strokeStyle = 'black';
        //    bmd.ctx.setLineDash([20,5]);
        //    bmd.ctx.moveTo(812,527);
        //    bmd.ctx.lineTo(pencil.x,pencil.y);
        //    bmd.ctx.stroke();
        //    bmd.ctx.closePath();
        //   line_NN=this.game.add.sprite(0,0,bmd);
        //   lineGroup.add(line_NN);
        //console.log(pencil.x);
        if(pencil.x>=1040){
          outLineCount=4;
          lineName="";
          /////////////////////////////////////////
          //  bmd=this.game.add.bitmapData(1920,1080);
          //  bmd.ctx.beginPath();
          //  bmd.ctx.lineWidth = 1;
          //  bmd.ctx.strokeStyle = 'black';
          //  bmd.ctx.setLineDash([20,5]);
          //  bmd.ctx.moveTo(812,527);
          //  bmd.ctx.lineTo(1030,655);
          //  bmd.ctx.stroke();
          //  bmd.ctx.closePath();
          // if(line_NN!=null){
          //   line_NN.destroy(true);
          // }
          // line_NN=this.game.add.sprite(0,0,bmd);
          // lineGroup.add(line_NN);
         /// //this.drawVLine(812,527,1030,655);//NN'
          ////////////////////////////////////////////
          //this.drawVLine(812,527,1030,652);//NN'
          N1.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;
          
          ///////////////////////todayAdd///////////////
          lineName="pointP";
          voice.destroy(true);
          voice=this.game.add.audio("step_11",1);
          voice.play();
          dialog.text="Mark an angle 45\u00BA on the surface AB.";
          protractor_1.inputEnabled = true;
          protractor_1.input.useHandCursor = true;
          protractor_1.input.enableDrag(true);
          arrow.x=protractor_1.x+10;
          arrow.y=protractor_1.y-130;
          arrow_y=arrow.y;
          arrow.visible=true;
          arrow.angle=0;
          collider.x=pointE.x;
          collider.y=pointE.y;
          ///////////////////////////////
          ///////////////////////////
          // pointF=this.game.add.sprite(1120,606, 'bullet_b');
          // pointF.anchor.set(.5);
          // pointF.scale.set(.8);
          // lEF=this.drawVLine(pointE.x,pointE.y,pointF.x,pointF.y);
          // pointR=this.game.add.sprite(1185,617, 'bullet_b');
          // pointR.anchor.set(.5);
          // pointR.scale.set(.8);
          // pointS=this.game.add.sprite(1298,638, 'bullet_b');
          // pointS.anchor.set(.5);
          // pointS.scale.set(.8);
          // lFS=this.drawVLine(pointF.x,pointF.y,pointS.x,pointS.y);
          // lGF=this.drawVLine(1010,586,pointF.x,pointF.y);
          // lEH=this.drawVLine(pointE.x,pointE.y,1127,530);
          // line_PE=this.drawVLine(pointP.x,pointP.y,pointE.x,pointE.y);
          // protractor.visible=true;
          // //protractor.x=pointF.x;
          // //protractor.y=pointF.y;
          // lineName="";
          // protractor.inputEnabled=true;
        }
      }
    }else if(lineName=="MM'"){
      if(currentx>this.game.input.activePointer.x && currenty<this.game.input.activePointer.y){
        pencil.x-=10;//4;
        pencil.y+=5.75;//2.3;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
        this.drawVLine(x1,y1,pencil.x,pencil.y);
        x1=pencil.x-4;
        y1=pencil.y+2.3;
        pencil.x=x1;
        pencil.y=y1;
        // bmd1=this.game.add.bitmapData(1920,1080);
        //   bmd1.ctx.beginPath();
        //   bmd1.ctx.lineWidth = 1;
        //   bmd1.ctx.strokeStyle = 'black';
        //   bmd1.ctx.setLineDash([20,5]);
        //   bmd1.ctx.moveTo(pointM.x,pointM.y);
        //   bmd1.ctx.lineTo(pencil.x,pencil.y);
        //   bmd1.ctx.stroke();
        //   bmd1.ctx.closePath();
        //   if(line_MM!=null){
        //     line_MM.destroy(true);
        //   }
        //   line_MM=this.game.add.sprite(0,0,bmd1);
        //   lineGroup.add(line_MM);
        //console.log(pencil.x);
        if(pencil.x<=1070){
          outLineCount=4;
          
          lineName="";
          /////////////////////////////////////////
          // bmd1=this.game.add.bitmapData(1920,1080);
          // bmd1.ctx.beginPath();
          // bmd1.ctx.lineWidth = 1;
          // bmd1.ctx.strokeStyle = 'black';
          // bmd1.ctx.setLineDash([20,5]);
          // bmd1.ctx.moveTo(pointM.x,pointM.y);
          // bmd1.ctx.lineTo(1091,683);
          // bmd1.ctx.stroke();
          // bmd1.ctx.closePath();
          // if(line_MM!=null){
          //   line_MM.destroy(true);
          // }
          // line_MM=this.game.add.sprite(0,0,bmd1);
          // lineGroup.add(line_MM);
          ////////////////////////////////////////////
          //this.drawVLine(1303,560,1091,683);//MM'
          M1.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;
          //////////////////////TodayCmt///////////////////
          //lineName="pointP";
          // voice.destroy(true);
          // voice=this.game.add.audio("step_4",1);
          // voice.play();
          // dialog.text="Mark an angle 30\u00BA on the surface AB.";
          // protractor_1.inputEnabled = true;
          // protractor_1.input.useHandCursor = true;
          // protractor_1.input.enableDrag(true);
          // arrow.x=protractor_1.x+10;
          // arrow.y=protractor_1.y-130;
          // arrow_y=arrow.y;
          // arrow.visible=true;
          // arrow.angle=0;
          // collider.x=pointE.x;
          // collider.y=pointE.y;
          /////////////////////////Cmt///////////////////
          ////////////////TodayAdd/////////////////
          lineName="EF";
          voice.destroy(true);
          voice=this.game.add.audio("step_25",1);
          voice.play();
          dialog.text="Join the points E and F.";
          scales.inputEnabled = true;
          scales.input.useHandCursor = true;
          scales.input.enableDrag(true);
          arrow.x=scales.x+10;
          arrow.y=scales.y-30;
          arrow_y=arrow.y;
          arrow.visible=true;
          arrow.angle=0;
          collider.x=pointE.x;
          collider.y=pointE.y;
          ////////////////////////////
        }
      }
    }else if(lineName=="PE"){
      if(currentx<this.game.input.activePointer.x){
        pencil.x+=16;
        //pencil.y-=0;
        currentx=this.game.input.activePointer.x;
        //currenty=this.game.input.activePointer.y;
        if(line_PE!=null){
          line_PE.destroy(true);
        }
        line_PE=this.drawVLine(pointP.x,pointP.y,pencil.x,pencil.y);
        //console.log(pencil.x+"...........");
        if(pencil.x>=970){
          outLineCount=5;
          lineName="";
          if(line_PE!=null){
            line_PE.destroy(true);
          }
          line_PE=this.drawVLine(pointP.x,pointP.y,pointE.x,pointE.y);//PE
          dir_arrow1.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;

          nailP.inputEnabled = true;
          nailP.input.useHandCursor = true;
          nailP.input.enableDrag(true);
          arrow.x=nailP.x;
          arrow.y=nailP.y-70;
          arrow.angle=0;
          arrow_y=arrow.y;
          arrow.visible=true;
          collider.x=pointP.x;
          collider.y=pointP.y;

          voice.destroy(true);
          voice=this.game.add.audio("step_14",1);
          voice.play();
          dialog.text="Fix two pins, say at points P and Q, on the line PE.";
          // protractor_1.inputEnabled = true;
          // protractor_1.input.useHandCursor = true;
          // protractor_1.input.enableDrag(true);
          // arrow.x=protractor_1.x+10;
          // arrow.y=protractor_1.y-130;
          // arrow_y=arrow.y;
          // arrow.visible=true;
          // arrow.angle=0;

        }
      }
    }else if(lineName=="FS"){
      if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y){
        pencil.x+=8;
        pencil.y+=8;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
        if(line_FS!=null){
          line_FS.destroy(true);
        }
        line_FS=this.drawVLine(pointF.x,pointF.y,pencil.x,pencil.y);
       // console.log(pencil.x+"...........");
        if(pencil.x>=1280){
          lineName="pointM";//EF
          if(line_FS!=null){
            line_FS.destroy(true);
          }
          line_FS=this.drawVLine(pointF.x,pointF.y,pointS.x,pointS.y);//FS
          dir_arrow3.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;
          F.visible=true;
          //////////////////TodayAdd//////////////
          protractor_1.inputEnabled = true;
          protractor_1.input.useHandCursor = true;
          protractor_1.input.enableDrag(true);
          arrow.x=protractor_1.x+10;
          arrow.y=protractor_1.y-130;
          arrow.angle=0;
          arrow.visible=true;
          voice.destroy(true);
          dialog.y=60;
          collider.x=pointF.x;
          collider.y=pointF.y;
          voice=this.game.add.audio("step_22",1);
          voice.play();
          dialog.text="Draw a perpendicular to the refracting surfaces AC through F.";
          F.visible=true;
          lineName="pointM";
          ////////////////TodayCmt///////////////////
          
          // voice.destroy(true);
          // voice=this.game.add.audio("step_4",1);
          // voice.play();
          // dialog.text="Join the points E and F";
          // scales.inputEnabled = true;
          // scales.input.useHandCursor = true;
          // scales.input.enableDrag(true);
          // arrow.x=scales.x+10;
          // arrow.y=scales.y-50;
          // arrow_y=arrow.y;
          // arrow.visible=true;
          // arrow.angle=0;
          // collider.x=pointE.x;
          ///////////////////cmt////////////////
        }
      }
    }else if(lineName=="EH"){
      if(currentx<this.game.input.activePointer.x){
        pencil.x+=16;
        //pencil.y-=3.75;
        currentx=this.game.input.activePointer.x;
        this.drawVLine(x1,y1,pencil.x,pencil.y);
        x1=pencil.x+6;
        y1=pencil.y;
        pencil.x=x1;
        pencil.y=y1;
        //currenty=this.game.input.activePointer.y;
          // bmd2=this.game.add.bitmapData(1920,1080);
          // bmd2.ctx.beginPath();
          // bmd2.ctx.lineWidth = 1;
          // bmd2.ctx.strokeStyle = 'black';
          // bmd2.ctx.setLineDash([20,5]);
          // bmd2.ctx.moveTo(pointE.x,pointE.y);
          // bmd2.ctx.lineTo(pencil.x,pencil.y);
          // bmd2.ctx.stroke();
          // bmd2.ctx.closePath();
          // if(line_EH!=null){
          //   line_EH.destroy(true);
          // }
          // line_EH=this.game.add.sprite(0,0,bmd2);
          // lineGroup.add(line_EH);
        //console.log(pencil.x+"...........");
        if(pencil.x>=1280){
          outLineCount=5;
          lineName="";
          /////////////////////////////////////////
          // bmd2=this.game.add.bitmapData(1920,1080);
          // bmd2.ctx.beginPath();
          // bmd2.ctx.lineWidth = 1;
          // bmd2.ctx.strokeStyle = 'black';
          // bmd2.ctx.setLineDash([20,5]);
          // bmd2.ctx.moveTo(pointE.x,pointE.y);
          // bmd2.ctx.lineTo(1300,620);
          // bmd2.ctx.stroke();
          // bmd2.ctx.closePath();
          // if(line_EH!=null){
          //   line_EH.destroy(true);
          // }
          // line_EH=this.game.add.sprite(0,0,bmd2);
          // lineGroup.add(line_EH);
          ////////////////////////////////////////////
          //this.drawVLine(972,620,1172,568);//EH
        //this.drawVLine(972,620,1130,620);//ef
        
          H.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;

          voice.destroy(true);
          voice=this.game.add.audio("step_28",1);
          voice.play();
          dialog.text="Extend the line FS to G.";
          scales.inputEnabled = true;
          scales.input.useHandCursor = true;
          scales.input.enableDrag(true);
          arrow.x=scales.x+10;
          arrow.y=scales.y-30;
          arrow_y=arrow.y;
          arrow.visible=true;
          arrow.angle=0;
          collider.x=G.x;
          lineName="GF";
        }
      }
    }else if(lineName=="GF"){
      if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y){
        pencil.x+=8;
        pencil.y+=8;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
        this.drawVLine(x1,y1,pencil.x,pencil.y);
        x1=pencil.x+3;
        y1=pencil.y+3;
        pencil.x=x1;
        pencil.y=y1;
        //console.log(pencil.x+"...........");
        // bmd3=this.game.add.bitmapData(1920,1080);
        //   bmd3.ctx.beginPath();
        //   bmd3.ctx.lineWidth = 1;
        //   bmd3.ctx.strokeStyle = 'black';
        //   bmd3.ctx.setLineDash([20,5]);
        //   bmd3.ctx.moveTo(1084,589);
        //   bmd3.ctx.lineTo(pencil.x,pencil.y);
        //   bmd3.ctx.stroke();
        //   bmd3.ctx.closePath();
        //   if(line_GF!=null){
        //     line_GF.destroy(true);
        //   }
        //   line_GF=this.game.add.sprite(0,0,bmd3);
        //   lineGroup.add(line_GF);
        if(pencil.x>=pointF.x){
          lineName="";
          /////////////////////////////////////////
          // bmd3=this.game.add.bitmapData(1920,1080);
          // bmd3.ctx.beginPath();
          // bmd3.ctx.lineWidth = 1;
          // bmd3.ctx.strokeStyle = 'black';
          // bmd3.ctx.setLineDash([20,5]);
          // bmd3.ctx.moveTo(1084,589);
          // bmd3.ctx.lineTo(pointF.x,pointF.y);
          // bmd3.ctx.stroke();
          // bmd3.ctx.closePath();
          // if(line_GF!=null){
          //   line_GF.destroy(true);
          // }
          // line_GF=this.game.add.sprite(0,0,bmd3);
          // lineGroup.add(line_GF);
          ////////////////////////////////////////////
          //this.drawVLine(1086,589,pointF.x,pointF.y);//GF
         
          G.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;

          protractor_1.inputEnabled = true;
          protractor_1.input.useHandCursor = true;
          protractor_1.input.enableDrag(true);

          //e.log("ppppppppppppppp");
          arrow.x=protractor_1.x+10;
          arrow.y=protractor_1.y-140;
          arrow.visible=true;
          arrow.angle=0;
          collider.x=pointE.x;
          collider.y=pointE.y;
          lineName="angle_i";

          voice.destroy(true);
          voice=this.game.add.audio("step_29",1);
          voice.play();
          dialog.text="Measure the angle of incidence \u2220i.";
          // scales.inputEnabled = true;
          // scales.input.useHandCursor = true;
          // scales.input.enableDrag(true);
          // arrow.x=scales.x+10;
          // arrow.y=scales.y-50;
          // arrow_y=arrow.y;
          // arrow.visible=true;
          // arrow.angle=0;
          // collider.x=pointE.x;
          // lineName="EF";
        }
      }
    }else if(lineName=="EF"){
      if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y){
        pencil.x+=13.5;//9;
        pencil.y+=3;//2;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
        if(line_EF!=null){
          line_EF.destroy(true);
        }
        line_EF=this.drawVLine(pointE.x,pointE.y,pencil.x,pencil.y);
        //console.log(pencil.x+"...........");
        if(pencil.x>=1120){
          ///////////
          outLineCount=3;
          lineName="";
          pencil.x=948;
          if(line_EF!=null){
            line_EF.destroy(true);
          }
          line_EF=this.drawVLine(pointE.x,pointE.y,pointF.x,pointF.y);//ef
          dir_arrow2.visible=true;
          pencil.visible=false;
          pencil_1.reset(pencil_1.xp,pencil_1.yp);
          pencil_1.angle=0;
          pencil_1.scale.set(.5);
          pencil_1.visible=true;
          scales.reset(scales.xp,scales.yp);
          scales.angle=0;
          scales.scale.set(.4,.7);
          scales.visible=true;
          ////////////////////////TodayAdd///////////
          voice.destroy(true);
        voice=this.game.add.audio("step_26",1);
        voice.play();
        dialog.y=40;
        dialog.text="Mark the angle of incidence, the angle of refraction and the \nangle of emergence.";
        this.game.time.events.add(Phaser.Timer.SECOND*5,this.markAngles, this);
        tween1=this.game.add.tween(arc_i).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          arc_i_angle.visible=true;
          tween1=this.game.add.tween(arc_r).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
          tween1.onComplete.add(function () {
            arc_r_angle.visible=true;
            tween1=this.game.add.tween(arc_e).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
            tween1.onComplete.add(function () {
              arc_e_angle.visible=true;
              
            }.bind(this));
          }.bind(this));
        }.bind(this));
        }
      }
    }
  }
  },
  markAngles:function(){
    lineName="EH";
    dialog.y=60;
    voice.destroy(true);
      voice=this.game.add.audio("step_27",1);
      voice.play();
      dialog.text="Extend the line PE to H.";
      scales.inputEnabled = true;
        scales.input.useHandCursor = true;
        scales.input.enableDrag(true);
        arrow.x=scales.x+10;
        arrow.y=scales.y-30;
        arrow_y=arrow.y;
        arrow.visible=true;
        arrow.angle=0;
        collider.x=pointE.x;
  },
  prismExit:function(){
    arrow.visible=false;
    tween1=this.game.add.tween(prism1).to( {x:2250}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          A=this.game.add.text(1040,440,"A",lablel_fontStyle);
          bgGroup.add(A);
          B=this.game.add.text(880,760,"B",lablel_fontStyle);
          bgGroup.add(B);
          C=this.game.add.text(1200,760,"C",lablel_fontStyle);
          bgGroup.add(C);
          voice.destroy(true);
         voice=this.game.add.audio("step_6",1);
         voice.play();
         dialog.text="Mark a point E on the refracting surface AB.";
          pencil_1.inputEnabled = true;
          pencil_1.input.useHandCursor = true;
          pencil_1.input.enableDrag(true);
          arrow.x=pencil_1.x+50;
          arrow.y=pencil_1.y-180;
          arrow.visible=true; 
          lineName="pointE";
          collider.x=970;
          collider.y=620;
      //     voice.destroy(true);
      //   voice=this.game.add.audio("step_4",1);
      //   voice.play();
      //   dialog.text="Draw a perpepedicular line(NN') at the refracting surface AB and Mark the point E.";
      //   this.drawVLine(972,620,1130,620);//EF
      //   protractor_1.events.onDragStart.add(function() {this.gameC(protractor_1)}, this);
      //   protractor_1.events.onDragStop.add(function() {this.gameC1(protractor_1)}, this);
      //  this.game.physics.arcade.enable(protractor_1);
      //  protractor_1.inputEnabled = true;
      //  protractor_1.input.useHandCursor = true;
      //  protractor_1.input.enableDrag(true);
      //  arrow.x=protractor_1.x+10;
      //  arrow.y=protractor_1.y-130;
      //  arrow_y=arrow.y;
      //  arrow.visible=true;
        }.bind(this));

  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(2,0x000000,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==paper || currentobj==paper_nail1 || currentobj==prism){
        arrow.visible=true;
        if(prismFlag){
          arrow.x=prism.x;
          arrow.y=prism.y-150;
        }
        
      }else if(currentobj==pencil_1){
        pencil_1.angle=0;
        pencil_1.scale.set(.5);
        arrow.x=pencil_1.x+50;
        arrow.y=pencil_1.y-180;
        arrow.angle=0;
      }else if(currentobj==protractor_1){
        protractor_1.scale.set(.5);
        arrow.x=protractor_1.x+10;
        arrow.y=protractor_1.y-130;
        arrow.angle=0;
        
      }else if(currentobj==scales){
        scales.scale.setTo(.4,.7);
        arrow.x=scales.x;
        arrow.y=scales.y-30;
        
        
      }else if(currentobj==nailP){
        arrow.x=nailP.x;
        arrow.y=nailP.y-70;
      }else if(currentobj==nailQ){
        arrow.x=nailQ.x;
        arrow.y=nailQ.y-70;
      }else if(currentobj==nailR){
        arrow.x=nailR.x;
        arrow.y=nailR.y-70;
      }else if(currentobj==nailS){
        arrow.x=nailS.x;
        arrow.y=nailS.y-70;
      }else if(currentobj==prism1){
        arrow.x=prism1.x;
        arrow.y=prism1.y-150;
      }
      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      voice.destroy();
      
      this.state.start("Experiment_2", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
