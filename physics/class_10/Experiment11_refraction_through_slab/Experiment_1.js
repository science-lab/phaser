var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;

var handle1;
var handle2;
var line1;
var handleMove;
var linecount;
var currentx;
var currenty;
var lineName;
var isPencilEnableDrag;
var isAB;
var isBC;
var isCD;
var isDA;
var activity;


}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
//  voice=this.game.add.audio("step_0",1);

 handleMove=false;
 
 
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 linecount=1;
 currentx=0;
 currenty=0;
 lineName="AB";
//  activity="line_HG";
 activity="line_AB";
//  activity="line_OO'";

// activity="line_MM'";
// activity="line_XO";
// activity="line_EP";
 isPencilEnableDrag=false;
 isAB=false;
 isBC=false;
 isCD=false;
 isDA=false;
 //voice.play();
 bg1= this.game.add.sprite(0, 0,'bg1');
//  bg2=this.game.add.sprite(0,0,'bg2');
//  bg2.scale.setTo(2,2);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "26px Segoe UI", fill: "#0000FF", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  
  this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  buttonGroup.add(fullScreen1);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
  
        bgGroup=this.game.add.group();
        var objBox = this.game.add.graphics(0, 0);
        objBox.lineStyle(8,0xC04000,1);
        objBox.beginFill(0xFFFFC2);
        objBox.drawRoundedRect(0, 0, 300, 850,15);
        objBox.x=25;
        objBox.y=200; 
        
        drawing_board_top=this.game.add.sprite(1045,625, 'drawing_board_top');
        drawing_board_top.angle=90;
        drawing_board_top.scale.setTo(.32,.425);
        drawing_board_top.anchor.set(.5);
        // //collider=this.game.add.sprite(600,500,'collider');
        // collider=drawing_board_top;//this.game.add.sprite(600,500,'collider');
        // this.game.physics.arcade.enable(collider);
        // collider.scale.setTo(1.3,2.5);
        // collider.inputEnabled = true;
        // collider.enableBody =true;

        /////3d ciew assests
         bg2=this.game.add.sprite(0,0,'bg2');
         bg2.scale.setTo(2.25,2.3);

         pin4=this.game.add.sprite(850,850,'pin');
         pin4.anchor.set(.5);
         pin4.scale.setTo(1,1.2);

         pin3=this.game.add.sprite(850,870,'pin');
         pin3.anchor.set(.5);
         pin3.scale.setTo(1,1.2);

         slab_3d=this.game.add.sprite(900,850,'slab3d');
         slab_3d.anchor.set(.5);

         pin2=this.game.add.sprite(850,890,'pin');
         pin2.anchor.set(.5);
         pin2.scale.setTo(1,1.2);
         pin2.visible=false;


         pin1=this.game.add.sprite(850,910,'pin');
         pin1.anchor.set(.5);
         pin1.scale.setTo(1,1.2);
         pin1.visible=false;


        

       
         
        

        paper1=this.game.add.sprite(1055,625, 'paper');
        paper1.scale.setTo(2.3,1.1);
        paper1.anchor.set(.5);
        paper1.alpha=0;

        paper=this.game.add.sprite(58,250, 'paper');
        paper.scale.setTo(.4,.3);
        // paper.xp=40;
        // paper.yp=260;

        Nail_top1=this.game.add.sprite(385,230, 'Nail_top');
        Nail_top1.scale.set(1.5);
        Nail_top2=this.game.add.sprite(1695,230, 'Nail_top');
        Nail_top2.scale.set(1.5);
        Nail_top3=this.game.add.sprite(1695,1000, 'Nail_top');
        Nail_top3.scale.set(1.5);
        Nail_top4=this.game.add.sprite(385,1000, 'Nail_top');
        Nail_top4.scale.set(1.5);

        // Nail_top1.visible=true;
        // Nail_top2.visible=true;
        // Nail_top3.visible=true;
        // Nail_top4.visible=true;

        Nail_top1.visible=false;
        Nail_top2.visible=false;
        Nail_top3.visible=false;
        Nail_top4.visible=false;

        Nail1=this.game.add.sprite(150,500, 'Nail');
        Nail2=this.game.add.sprite(20,0, 'Nail');//270,550,
        Nail3=this.game.add.sprite(0,20, 'Nail');
        Nail4=this.game.add.sprite(20,20, 'Nail');
        Nail5=this.game.add.sprite(0,0, 'collider');
        Nail1.addChild(Nail2);

        Nail1.addChild(Nail3);
        Nail1.addChild(Nail4);
        Nail1.addChild(Nail5);
        Nail5.alpha=0;
        //Nail1.x=1000;
        Nail1.scale.set(1.5);
        Nail1.xp=250;
        Nail1.yp=580;
   
       
        lineGroup=this.game.add.group();
        line2Group=this.game.add.group();
        lineDashGroup=this.game.add.group();

        slab=this.game.add.sprite(1030,640,'slab');
        slab.anchor.set(.5);
        slab.visible=false;
        // slab.visible=true;

        protractor=this.game.add.sprite(919,522,'protractor');//919,522
        protractor.anchor.set(.5,.94);
        protractor.scale.set(1);
        protractor.visible=false;
          // protractor.visible=true;
        //  protractor.angle=-90;
        
        scale=this.game.add.sprite(918,580, 'scale');//918,580
        scale.anchor.setTo(.5,0);
        scale.scale.setTo(1);
        scale.visible=true;
         scale.angle=90;//90
        scale.visible=false;

        pencil=this.game.add.sprite(813,523,'pencil');  //813,523//1007//893,420
        pencil.anchor.set(0,1);
        pencil.scale.set(.65);
        pencil.visible=false;
        pencil.inputEnabled = true;
        pencil.input.useHandCursor = true;
        pencil.events.onInputDown.add(this.clickOnPencil, this);
        pencil.events.onInputUp.add(this.clickOffPencil,this);
      //  pencil.visible=true;
        // pencil.angle=45;

        A=this.game.add.text(785,520,"A",lablel_fontStyle);
        A.visible=false;
        B=this.game.add.text(1250,520,"B",lablel_fontStyle);
        B.visible=false;
        C=this.game.add.text(1250,730,"C",lablel_fontStyle);
        C.visible=false;
        D=this.game.add.text(785,730,"D",lablel_fontStyle);
        D.visible=false;

        pointO1=this.game.add.sprite(920,523,'blackDot');
        pointO1.anchor.set(.5);
        pointO1.scale.set(.5);
        pointO1.visible=false;
        O1=this.game.add.text(897,523,"O",lablel_fontStyle);
         O1.visible=false;  

       
        
        pointN1=this.game.add.sprite(920,343,'blackDot');
        pointN1.anchor.set(.5);
        pointN1.scale.set(.5);
        pointN1.visible=false;
        N1=this.game.add.text(908,400,"N",lablel_fontStyle);
        N2=this.game.add.text(908,610,"N'",lablel_fontStyle);
        N1.visible=false;
        N2.visible=false;

        pointX=this.game.add.sprite(829,363,'blackDot');
        pointX.anchor.set(.5);
        pointX.scale.set(.5);
        pointX.visible=false;
        X=this.game.add.text(829,330,"X",lablel_fontStyle);
         X.visible=false;

        pointE=this.game.add.sprite(817,343,'blackDot');
        pointE.anchor.set(.5);
        pointE.scale.set(.6);
        // pointE.visible=false;
        E=this.game.add.text(820,390,"E",lablel_fontStyle);
        // E.visible=false;

        pin1_top=this.game.add.sprite(850,400,'pin_top');
        pin1_top.anchor.set(.5);
        //  pin1_top.visible=false;

        pointF=this.game.add.sprite(884,460,'blackDot');
        pointF.anchor.set(.5);
        pointF.scale.set(.6);
        // pointF.visible=false;
        F=this.game.add.text(855,450,"F",lablel_fontStyle);
        //  F.visible=false;

        pointO2=this.game.add.sprite(1004,760,'blackDot');
        pointO2.anchor.set(.5);
        pointO2.scale.set(.5);
        pointO2.visible=false;
        O2=this.game.add.text(970,760,"O'",lablel_fontStyle);
        O2.visible=false;

        pointM1=this.game.add.sprite(1004,575,'blackDot');
        pointM1.anchor.set(.5);
        pointM1.scale.set(.5);
         pointM1.visible=false;
        M1=this.game.add.text(1015,660,"M",lablel_fontStyle);
        M1.visible=false;

        pointM2=this.game.add.sprite(920,523,'blackDot');
        pointM2.anchor.set(.5);
        pointM2.scale.set(.5);
         pointM2.visible=false;
        M2=this.game.add.text(970,840,"M'",lablel_fontStyle);
        M2.visible=false;



        pointP=this.game.add.sprite(1147,915,'blackDot');
        pointP.anchor.set(.5);
        pointP.scale.set(.6);
        pointP.visible=false;
        P=this.game.add.text(1150,900,"P",lablel_fontStyle);
        P.visible=false;

      var arc_i=this.drawArc(15);
        arc_i.alpha=0;
        arc_i.angle=-140;
        arc_i.x=915;
        arc_i.y=500;
        arc_i_label=this.game.add.text(903,450,"i",lablel_fontStyle);
        arc_i_label.visible=false;
        // arc_i_angle.visible=false;

        var arc_r=this.drawArc(15);
        arc_r.alpha=0;
        arc_r.angle=40;
        arc_r.x=920;
        arc_r.y=550;
        arc_r_label=this.game.add.text(924,570,"r",lablel_fontStyle);
        arc_r_label.visible=false;
        // arc_r_angle.visible=false;

        var arc_i2=this.drawArc(15);
        arc_i2.alpha=0;
        arc_i2.angle=-140;
        arc_i2.x=1000;
        arc_i2.y=725;
        arc_i2_label=this.game.add.text(924,770,"",lablel_fontStyle);
        arc_i2_label.visible=false;
        // arc_i2_angle.visible=false;

        var arc_e=this.drawArc(20);
        arc_e.alpha=0;
        arc_e.angle=40;
        arc_e.x=1006;
        arc_e.y=780;
        arc_e_label=this.game.add.text(1010,800,"e",lablel_fontStyle);
        arc_e_label.visible=false;
        // arc_e_angle.visible=false;



        temp_point1=this.game.add.sprite(980,692,'blackDot');
        temp_point1.anchor.set(.5);
        temp_point1.scale.set(.6);
        temp_point1.visible=false;

        temp_point2=this.game.add.sprite(1113,945,'blackDot');
        temp_point2.anchor.set(.5);
        temp_point2.scale.set(.6);
        temp_point2.visible=false;

        pin2_top=this.game.add.sprite(884,460,'pin_top');
        pin2_top.anchor.set(.5);
        // pin2_top.visible=false;

        directionArrow1=this.game.add.sprite(867,430,'directionArrow');
        directionArrow1.anchor.set(.5);
        directionArrow1.tint='black';
        directionArrow1.angle=55;

        directionArrow2=this.game.add.sprite(958,630,'directionArrow');
        directionArrow2.anchor.set(.5);
        directionArrow2.tint='black';
        directionArrow2.angle=65;

        directionArrow3=this.game.add.sprite(1063,860,'directionArrow');
        directionArrow3.anchor.set(.5);
        directionArrow3.tint='black';
        directionArrow3.angle=55;
       

        //pointH=this.game.add.sprite(1040,820,'blackDot');

        pointG=this.game.add.sprite(1040,820,'blackDot');
        pointG.anchor.set(.5);
        pointG.scale.set(.5);
        pointG.visible=false;
        G=this.game.add.text(1050,807,"G",lablel_fontStyle);
        // G.visible=false;

        pin3_top=this.game.add.sprite(1040,820,'pin_top');
        pin3_top.anchor.set(.5);
        // pin3_top.visible=false;

        
        pointH=this.game.add.sprite(1080,890,'blackDot');
        pointH.anchor.set(.5);
        pointH.scale.set(.5);
        pointH.visible=false;
        H=this.game.add.text(1090,875,"H",lablel_fontStyle);
        // H.visible=false;

        pin4_top=this.game.add.sprite(1080,890,'pin_top');
        pin4_top.anchor.set(.5);
        // pin4_top.visible=false;

        
        
        sideViewGroup=this.game.add.group();
        sideViewGroup.add(bg2);
        sideViewGroup.add(pin4);
        sideViewGroup.add(pin3);
        sideViewGroup.add(slab_3d);
        sideViewGroup.add(pin2);
        sideViewGroup.add(pin1);

        sideViewGroup.visible=false;




        temp_scale=this.game.add.sprite(170,670, 'scale');
        temp_scale.scale.setTo(.4,.7);
        temp_scale.anchor.set(.5);

        temp_protractor=this.game.add.sprite(220,980,'protractor');
        temp_protractor.anchor.set(.5);
        temp_protractor.scale.setTo(.5);

        temp_slab=this.game.add.sprite(175,600,'slab3d');
        temp_slab.anchor.setTo(.5,.5);
        temp_slab.scale.set(.3,.3);

        temp_pencil=this.game.add.sprite(100,950,'pencil');
        temp_pencil.anchor.set(.5);
        temp_pencil.scale.setTo(.5);

        temp_pin1=this.game.add.sprite(100,760,'pin');
        temp_pin2=this.game.add.sprite(140,760,'pin');
        temp_pin3=this.game.add.sprite(180,760,'pin');
        temp_pin4=this.game.add.sprite(220,760,'pin');

        

       collider=drawing_board_top;//this.game.add.sprite(600,500,'collider');
       this.game.physics.arcade.enable(collider);
      // collider.scale.setTo(1.3,2.5);
      collider.inputEnabled = true;
      collider.enableBody =true;
      //collider.alpha=0;//.5
      bgGroup.add(collider);
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      collider_1=this.game.add.sprite(870,470,'collider');//870,470
      collider_1.scale.set(3);//3
      collider_1.alpha=.5;
      collider_1.anchor.set(.5);
      this.game.physics.arcade.enable(collider_1);
      collider_1.inputEnabled = true;
      collider_1.enableBody =true;
      collider_1.visible=false;

      collider_2=this.game.add.sprite(175,600,'collider');
      collider_2.scale.setTo(6,2);
      collider_2.alpha=.5;
      collider_2.anchor.set(.5);
      this.game.physics.arcade.enable(collider_2);
      collider_2.inputEnabled = true;
      collider_2.enableBody =true;
      collider_2.visible=false;

      
      // arrow=this.game.add.sprite(200,200, 'arrow');
      // arrow.angle=90;
      // arrow_y=200;
      // arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*0,this.loadScene, this);  

      bmd1 = this.game.add.bitmapData(1920,1080);
        var color = 'black';

        bmd1.ctx.beginPath();
        bmd1.ctx.lineWidth = "1";
        bmd1.ctx.strokeStyle = color;
        bmd1.ctx.stroke();
        sprite = this.game.add.sprite(0, 0, bmd1);

        tableGroup=this.game.add.group();
        tableGroup.add(objBox);
        tableGroup.add(temp_pencil);
        tableGroup.add(temp_pin1);
        tableGroup.add(temp_pin2);
        tableGroup.add(temp_pin3);
        tableGroup.add(temp_pin4);
        tableGroup.add(Nail1);
        tableGroup.add(temp_protractor);
        tableGroup.add(temp_scale);
        tableGroup.add(temp_slab);
        tableGroup.add(paper);

        arrow=this.game.add.sprite(170,200,'arrowAnimation','arrow_0001.png');//170,200
        arrow.animations.add('anim',[0,1,2,3,4,5,6,7,8,9],26,true,true);
        arrow.animations.play('anim');
        arrow.anchor.set(.5);
        //  arrow.angle=-28;


       


    },
    drawLine:function(){ //this.drawVLine(550,480,1050,480);
      bmd1.clear();
      bmd1.ctx.beginPath();
      bmd1.ctx.beginPath();
      bmd1.ctx.moveTo(550, 480);
      bmd1.ctx.lineTo(this.game.input.activePointer.x , this.game.input.activePointer.y);
      bmd1.ctx.lineWidth = 2;
      bmd1.ctx.stroke();
      bmd1.ctx.closePath();
      bmd1.render();
     



      
      
      //bmd.refreshBuffer();
    },
   
   
 
    loadScene:function(){
      //console.log("lllllllllll");
      // voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*0,this.startExperiment, this); 
    },
    startExperiment:function(){
      //  voice.destroy(true);
      //  voice=this.game.add.audio("step_1",1);
      //  voice.play();
       dialog.text="Drag the white sheet of paper to the drawing board.";
       paper.events.onDragStart.add(function() {this.onDragStart(paper)}, this);
       paper.events.onDragStop.add(function() {this.onDragStop(paper)}, this);
       this.game.physics.arcade.enable(paper);
       paper.inputEnabled = true;
       paper.input.useHandCursor = true;
       paper.input.enableDrag(true);
       arrow.visible=true;
      //////////////////////////////////////////
      //this.ShowReactions();
      this.drawVLine(813,523,1244,523);//lineAB
      this.drawVLine(1244,523,1244,760);//lineBC
      this.drawVLine(1244,760,813,758);//lineCD
      this.drawVLine(813,760,813,523);//lineDA


     this.drawDashLine(618,130,618,316);//line NN'
     this.drawDashLine(703,383,703,549);//line MM'
     this.drawDashLine(620,223,847,615);//line EP'

     this.drawVLine(817,343,920,523);//line XO
     this.drawVLine(920,523,1004,760);//Line OO'
     this.drawVLine(1004,760,1113,945);//line HG

     

    },
    
    onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    arrow.visible=false;
    //console.log("drag");
     if(currentobj==paper){
       //1050,600
       arrow.visible=false;
        arrow.x=1050;
        arrow.y=500;
        arrow.visible=true;
        currentobj.xp=58;
        currentobj.yp=250;

     }
     else if(currentobj==Nail1)
     {
      arrow.visible=false;
      arrow.x=1050;
      arrow.y=500;
      arrow.visible=true;
      currentobj.xp=150;
      currentobj.yp=500;

     }
     else if(currentobj==temp_slab)
     {
      arrow.visible=false;
      arrow.x=1050;
      arrow.y=500;
      arrow.visible=true;
      currentobj.xp=170;
      currentobj.yp=600;
      

     }
     else if(currentobj==temp_pencil)
     {
       if(activity=="line_AB")
       {
        arrow.visible=false;
        arrow.x=820;
        arrow.y=480;
        arrow.visible=true;
        currentobj.xp=100;
        currentobj.yp=950;
        collider_1.visible=true;
       }
       else if(activity=="point_O")
       {
         arrow.visible=false;
         arrow.x=pointO1.x;
         arrow.y=pointO1.y-40;
         arrow.visible=true;

         collider_1.x=pointO1.x;
         collider_1.y=pointO1.y;
         collider_1.inputEnabled=true;
         collider_1.enableBody=true;
         collider_1.visible=true;
       }
       else if(activity=="point_N")
       {
        arrow.visible=false;
        arrow.x=protractor.x;
        arrow.y=protractor.y-230;
        arrow.visible=true;

        collider_1.visible=true;
        collider_1.x=arrow.x;
        collider_1.y=arrow.y;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        

       }

       else if(activity=="point_X")
       {
        arrow.visible=false;
        arrow.x=pointX.x;
        arrow.y=pointX.y-50;
        arrow.visible=true;

        collider_1.x=830;
        collider_1.y=340;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;



       }

       else if(activity=="point_M")
       {
        arrow.visible=false;
        collider_1.x=1004;
        collider_1.y=550;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;

        arrow.x=collider_1.x;
        arrow.y=collider_1.y-50;
        arrow.visible=true;

       }
       currentobj.xp=100;
       currentobj.yp=950;

     }
     else if (currentobj==slab)
     {
      currentobj.xp=1030;
      currentobj.yp=640;
       
       if(activity=="placeSlabBack2")
       {
        arrow.visible=false;
        arrow.x=temp_slab.x-10;
        arrow.y=temp_slab.y-80;
        arrow.visible=true;

       

        collider_2.visible=true;
        collider_2.inputEnabled=true;
        collider_2.enableBody=true;
       }
       else
       {
        arrow.visible=false;
        arrow.x=temp_slab.x-10;
        arrow.y=temp_slab.y-80;
        arrow.visible=true;
  
        // currentobj.xp=1030;
        // currentobj.yp=640;
  
        collider_2.visible=true;
       }
      
     }
     else if(currentobj==temp_protractor)
     {
      currentobj.xp=220;
      currentobj.yp=980;
       if(activity=="point_N")
       {
        arrow.visible=false;
        arrow.x=protractor.x;
        arrow.y=protractor.y-100;
        arrow.visible=true;
  
        collider_1.visible=true;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
  
       
       }
       else if(activity=="point_X")
       {
        arrow.visible=false;
        arrow.x=protractor.x;
        arrow.y=protractor.y-100;
        arrow.visible=true;
  
        collider_1.visible=true;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        //collider.x=pointO1.x+100;

       }
       else if(activity=="point_M")
       {

        arrow.visible=false;
        
        collider_1.x=1004;
        collider_1.y=720;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;

        arrow.x=collider_1.x;
        arrow.y=collider_1.y-80;
        arrow.visible=true;
        arrow.angle=0;
        
       }

   

     }
     else if(currentobj==temp_scale)
     {
       currentobj.xp=170;
       currentobj.yp=670;
       if(activity=="line_NN'")
       {
        arrow.visible=false;
        collider_1.x=870;
        collider_1.y=520;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;
  
        arrow.x=collider_1.x;
        arrow.y=collider_1.y-80;
        arrow.visible=true;

       }
       else if(activity=="line_XO")
       {

      arrow.visible=false;
      
      collider_1.x=810;
      collider_1.y=470;
      collider_1.scale.set(5);
      collider_1.inputEnabled=true;
      collider_1.enableBody=true;
      collider_1.visible=true;

      arrow.x=830;
      arrow.y=400;

       }
       else if(activity=="line_HG")
       {

        arrow.visible=false;
        arrow.x=1000;
        arrow.y=800;
        arrow.visible=true;

        collider_1.x=1030;
        collider_1.y=830;
        collider_1.scale.set(5);
        collider_1.visible=true;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;

       }
       else if(activity=="line_MM'")
       {

        collider_1.x=940;
        collider_1.y=650;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;

        arrow.visible=false;
        arrow.x=collider_1.x;
        arrow.y=collider_1.y-80;
        arrow.visible=true;
       }

       else if(activity=="line_OO'")
       {
        collider_1.x=920;
        collider_1.y=640;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;

        arrow.visible=false;
        arrow.x=collider_1.x;
        arrow.y=collider_1.y-80;
        arrow.visible=true;

       }

       else if(activity="line_EP")
       {

        arrow.visible=false;

        collider_1.x=940;
        collider_1.y=630;
        collider_1.inputEnabled=true;
        collider_1.enableBody=true;
        collider_1.visible=true;

        arrow.x=collider_1.x;
        arrow.y=collider_1.y-80;
        arrow.angle=0;
        arrow.visible=true;
       }
     

      // temp_scale.visible=false;
      // scale.visible=true;

     }
     else if(currentobj==temp_pin4)
     {
       arrow.visible=false;
       arrow.x=850;
       arrow.y=350;
       arrow.visible=true;

       collider_1.x=880;
       collider_1.y=410;
       collider_1.visible=true;
       collider_1.inputEnabled=true;
       collider_1.enableBody=true;

       currentobj.xp=220;
       currentobj.yp=760;

     }
     else if(currentobj==temp_pin3)
     {
      arrow.visible=false;
      arrow.x=885;
      arrow.y=410;
      arrow.visible=true;

      currentobj.xp=180;
      currentobj.yp=760;

     }
     else if(currentobj==temp_pin2)
     {
      arrow.visible=false;
      arrow.x=pin2.x;
      arrow.y=pin2.y-30;
      arrow.visible=true;

      collider_1.visible=true;
      collider_1.x=pin2.x;
      collider_1.y=pin2.y;
      collider_1.inputEnabled=true;
      collider_1.enableBody=true;
      collider_1.scale.set(3);

      currentobj.xp=140;
      currentobj.yp=760;

     }
     else if(currentobj==temp_pin1)
     {

      arrow.visible=false;
      arrow.x=pin1.x;
      arrow.y=pin1.y-30;
      arrow.visible=true;

      currentobj.xp=100;
      currentobj.yp=760;

      
     }

    },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;

    if(currentobj==paper){
      paper.visible=false;
      paper1.alpha=1;

      Nail1.events.onDragStart.add(function() {this.onDragStart(Nail1)}, this);
      Nail1.events.onDragStop.add(function() {this.onDragStop(Nail1)}, this);
       this.game.physics.arcade.enable(Nail1);
       Nail1.inputEnabled = true;
       Nail1.input.useHandCursor = true;
       Nail1.input.enableDrag(true);
       arrow.x=Nail1.x+30;
       arrow.y=Nail1.y-70;
       arrow_y=arrow.y;
       arrow.visible=true;
      //  voice.destroy(true);
      //  voice=this.game.add.audio("step_2",1);
      //  voice.play();
       dialog.text="Fix the sheet with the drawing pins.";
    }
    else if(currentobj==Nail1){
      
      var xp=Nail1.x;
      var yp=Nail1.y;
      Nail1.visible=false;
      Nail1_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail1_1.scale.set(2);
      tween1=this.game.add.tween(Nail1_1).to( {x:385, y:200}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        Nail_top1.visible=true;
        Nail1_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail2_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail2_1.scale.set(2);
      tween2=this.game.add.tween(Nail2_1).to( {x:1695, y:200}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        Nail_top2.visible=true;
        Nail2_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail3_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail3_1.scale.set(2);
      tween3=this.game.add.tween(Nail3_1).to( {x:1695, y:960}, 600, Phaser.Easing.Out, true);
      tween3.onComplete.add(function () {
        Nail_top3.visible=true;
        Nail3_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail4_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail4_1.scale.set(2);
      tween4=this.game.add.tween(Nail4_1).to( {x:385, y:980}, 600, Phaser.Easing.Out, true);
      tween4.onComplete.add(function () {
        Nail_top4.visible=true;
        Nail4_1.visible=false;
        dialog.text="Drag the glass slab to the white sheet.";
        activity="placeSlab";
        temp_slab.events.onDragStart.add(function() {this.onDragStart(temp_slab)}, this);
        temp_slab.events.onDragStop.add(function() {this.onDragStop(temp_slab)}, this);
         this.game.physics.arcade.enable(temp_slab);
         temp_slab.inputEnabled = true;
         temp_slab.input.useHandCursor = true;
         temp_slab.input.enableDrag(true);
         arrow.visible=false;
         arrow.x=temp_slab.x-10;
         arrow.y=temp_slab.y-50;
         arrow.visible=true;
        //
      }.bind(this));
  
      //  arrow.x=Lens_1.x+10;
      //  arrow.y=Lens_1.y-180;
      //  arrow_y=arrow.y;
      //  arrow.visible=true;
      //  voice.destroy(true);
      //  voice=this.game.add.audio("step_3",1);
      //  voice.play();
      //  dialog.text="Drag the convex lens to the centre of the sheet.";
    }

    else if(currentobj==temp_slab)
    {
      if(activity=="placeSlab")
      {
        arrow.visible=false;
        temp_slab.visible=false;
        temp_slab.x=175;
        temp_slab.y=600;
        slab.visible=true;
        dialog.text="Now draw the outline of the slob by using the pencil";
        temp_pencil.events.onDragStart.add(function() {this.onDragStart(temp_pencil)}, this);
        temp_pencil.events.onDragStop.add(function() {this.onDragStop(temp_pencil)}, this);
         this.game.physics.arcade.enable(temp_pencil);
         temp_pencil.inputEnabled = true;
         temp_pencil.input.useHandCursor = true;
         temp_pencil.input.enableDrag(true);
         arrow.x=temp_pencil.x-10;
         arrow.y=temp_pencil.y-50;
         arrow.visible=true;
         collider.inputEnabled = false;
        collider.enableBody =false;
        activity="line_AB";
      }
      

      else if(activity=="placeSlabBack")
      {
        arrow.visible=false;
        temp_slab.visible=false;
        temp_slab.x=175;
        temp_slab.y=600;

        slab.x=1030;
        slab.y=640;
        slab.visible=true;
        activity="";

        dialog.text="Good job. You have sucessfully completed the first stage\nof the experiment."; 
        this.game.time.events.add(Phaser.Timer.SECOND*1,this.goTo3dView, this);        

      }

    }
    else if(currentobj==temp_pencil)
    {
      if(activity=="line_AB")
      {
      
        temp_pencil.visible=false;
        pencil.visible=true;

        arrow.visible=false;
        arrow.x=880;
        arrow.y=500;
        arrow.angle=-90;
        arrow.visible=true;

        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;
        
        // pencil.inputEnabled = true;
        // pencil.input.useHandCursor = true;
        // pencil.events.onInputDown.add(this.clickOnPencil, this);
      }
      else if(activity=="point_O")
      {
        temp_pencil.visible=false;
        pencil.visible=true;
        pencil.angle=0;
        pencil.x=pointO1.x;
        pencil.y=pointO1.y;

        pointO1.visible=true;
        O1.visible=true;
        this.game.time.events.add(Phaser.Timer.SECOND*1,this.take90onAB, this);    
      }

      else if(activity=="point_N")
      {
        collider_1.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;

        arrow.visible=false;

        temp_pencil.visible=false;
        pointN1.visible=true;
        //console.log("poit Nnnnnnnnnnnnn");
        pencil.x=pointN1.x;
        pencil.y=pointN1.y;
        pencil.visible=true;
        this.game.time.events.add(Phaser.Timer.SECOND*1,this.drawNormalOnAB, this); 
        isPencilEnableDrag=false;   
      }

      else if(activity=="point_X")
      {
        temp_pencil.visible=false;
        pencil.x=829;
        pencil.y=363;
        pencil.visible=true;

        pointX.visible=true;
        X.visible=true;

        collider_1.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;

        arrow.visible=false;
        activity="";
        this.game.time.events.add(Phaser.Timer.SECOND*1,this.drawLine_XO, this); 


      }
      else if(activity=="point_M")
      {

        arrow.visible=false;

        collider_1.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;

        temp_pencil.visible=false;
        pencil.x=1003;
        pencil.y=575;
        pencil.angle=0;
        pencil.visible=true;

        pointM1.visible=true;

        this.game.time.events.add(Phaser.Timer.SECOND*1,this.drawNormalOnDC, this); 
        isPencilEnableDrag=false;   


      }
     
    }


    else if(currentobj==slab)
    {
      
       
      if(activity=="placeSlabBack2")
      {
        arrow.visible=false;
      slab.visible=false;
      temp_slab.visible=true;
      temp_slab.inputEnabled = false;
      temp_slab.input.useHandCursor = false;
      temp_slab.input.enableDrag(false);

      collider_2.inputEnabled=false;
      collider_2.enableBody=false;
      collider_2.visible=false;

      dialog.text="Now remove the pins.";

      arrow.x=pin1_top.x;
      arrow.y=pin1_top.y-50;
      arrow.visible=true;

      pin1_top.inputEnabled = true;
      pin1_top.input.useHandCursor = true;
      pin1_top.events.onInputDown.add(this.clickOnPin1_top, this);

        
      }
      else
      {
        arrow.visible=false;
      slab.visible=false;
      temp_slab.visible=true;
      temp_slab.inputEnabled = false;
      temp_slab.input.useHandCursor = false;
      temp_slab.input.enableDrag(false);

      A.visible=true;
      B.visible=true;
      C.visible=true;
      D.visible=true;

      collider_2.inputEnabled=false;
      collider_2.enableBody=false;
      collider_2.visible=false;

      this.game.time.events.add(Phaser.Timer.SECOND*.5,this.drawPoint_O, this);  
      }
      
     
    }
    else if(currentobj==temp_protractor)
    {
      if(activity=="point_N")
      {
        arrow.visible=false;
        temp_protractor.visible=false;
        protractor.visible=true;
  
        dialog.text="Mark 90\u00B0 on the protractor using the pencil.";
        //activity="point_N";
  
         temp_pencil.events.onDragStart.add(function() {this.onDragStart(temp_pencil)}, this);
         temp_pencil.events.onDragStop.add(function() {this.onDragStop(temp_pencil)}, this);
         this.game.physics.arcade.enable(temp_pencil);
         temp_pencil.inputEnabled = true;
         temp_pencil.input.useHandCursor = true;
         temp_pencil.input.enableDrag(true);
         arrow.x=temp_pencil.x-10;
         arrow.y=temp_pencil.y-50;
         arrow.visible=true;
  
         collider_1.visible=false;
         collider_1.inputEnabled=false;
         collider_1.enableBody=false;

      }
      else if(activity=="point_X")
      {
        arrow.visible=false;
        temp_protractor.visible=false;
        protractor.visible=true;
        protractor.angle=-90;

        collider_1.visible=false;

        dialog.text="Mark 30\u00B0 from the normal using the pencil.";

        temp_pencil.events.onDragStart.add(function() {this.onDragStart(temp_pencil)}, this);
        temp_pencil.events.onDragStop.add(function() {this.onDragStop(temp_pencil)}, this);
        this.game.physics.arcade.enable(temp_pencil);
        temp_pencil.inputEnabled = true;
        temp_pencil.input.useHandCursor = true;
        temp_pencil.input.enableDrag(true);
        arrow.x=temp_pencil.x-10;
        arrow.y=temp_pencil.y-50;
        arrow.visible=true;

      }

      else if(activity=="point_M")
      {
        arrow.visible=false;
        temp_protractor.visible=false;
        protractor.x=1004;
        protractor.y=757;
        protractor.visible=true;
        protractor.angle=0;

        collider_1.visible=false;

        dialog.text="Mark 90\u00B0 form the line DC";

        temp_pencil.events.onDragStart.add(function() {this.onDragStart(temp_pencil)}, this);
        temp_pencil.events.onDragStop.add(function() {this.onDragStop(temp_pencil)}, this);
        this.game.physics.arcade.enable(temp_pencil);
        temp_pencil.inputEnabled = true;
        temp_pencil.input.useHandCursor = true;
        temp_pencil.input.enableDrag(true);
        arrow.x=temp_pencil.x-10;
        arrow.y=temp_pencil.y-50;
        arrow.visible=true;


      }
   

    }
    else if(currentobj==temp_scale)
    {
      if(activity=="line_NN'")
      {
        arrow.visible=false;
        temp_scale.visible=false;
        scale.visible=true;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;

        temp_pencil.visible=false;
        pencil.x=pointN1.x;
        pencil.y=pointN1.y;
        pencil.visible=true;

        arrow.x=pencil.x;
        arrow.y=pencil.y+50;
        arrow.visible=true;

      }
      else if(activity=="line_XO")
      {
        arrow.visible=false;
        temp_pencil.visible=false;
        temp_scale.visible=false;

        scale.x=912;
        scale.y=510;
        scale.angle=60;
        scale.visible=true;
       // 817,343
        pencil.x=817;
        pencil.y=343;
        pencil.angle=45;
        pencil.visible=true;

        arrow.x=870;
        arrow.y=400;
        arrow.angle=-30;
        arrow.visible=true;

        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;
        

      }
      else if(activity=="line_HG")
      {
        arrow.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;

        temp_scale.visible=false;
        scale.visible=true;
        scale.x=1015;
        scale.y=780;
        scale.angle=59.5;

        pencil.x=1094;
        pencil.y=915;
        pencil.visible=true;

        dialog.text="Draw the line.";

        arrow.x=1095;
        arrow.y=800;
        arrow.visible=true;
        arrow.angle=150;

      }

      else if(activity=="line_MM'")
      {
        arrow.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;

        temp_scale.visible=false;
        temp_pencil.visible=false;

        scale.visible=true;
        scale.x=1002;
        scale.y=650;
        scale.angle=90;
        
        pencil.x=1002;
        pencil.y=578;
        pencil.visible=true;

        arrow.x=1020;
        arrow.y=620;
        arrow.visible=true;

      }
      else if(activity=="line_OO'")
      {

        arrow.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;

        temp_scale.visible=false;
        temp_pencil.visible=false;

        scale.visible=true;
        scale.x=940;
        scale.y=580;
        scale.angle=70.6;

        pencil.x=920;
        pencil.y=523;
        pencil.visible=true;

        arrow.x=960;
        arrow.y=560;
        arrow.angle=-22;
        arrow.visible=true;

      }
      else if(activity=="line_EP")
      {
        arrow.visible=false;
        collider_1.inputEnabled=false;
        collider_1.enableBody=false;
        collider_1.visible=false;
        
        temp_scale.visible=false;
        temp_pencil.visible=false;

        scale.visible=true;
        scale.x=1010;
        scale.y=680;
        scale.angle=60;

        pencil.x=883;
        pencil.y=462;
        pencil.visible=true;

        arrow.x=930;
        arrow.y=480;
        arrow.angle=-28;
        arrow.visible=true;

      }

    }
    else if(currentobj==temp_pin4)
    {

      arrow.visible=false;
      temp_pin4.visible=false;

      temp_pin4.x=pin1_top.x;
      temp_pin4.y=pin1_top.y;

      pin1_top.visible=true;
      E.visible=true;
      pointE.visible=true;
      dialog.text="Now fix the second pin.";
      
      activity="fixpin_F";

      temp_pin3.events.onDragStart.add(function() {this.onDragStart(temp_pin3)}, this);
      temp_pin3.events.onDragStop.add(function() {this.onDragStop(temp_pin3)}, this);
      this.game.physics.arcade.enable(temp_pin3);
      temp_pin3.inputEnabled = true;
      temp_pin3.input.useHandCursor = true;
      temp_pin3.input.enableDrag(true);

      arrow.x=195;
      arrow.y=700;
      arrow.visible=true;

    }
    else if(currentobj==temp_pin3)
    {

      arrow.visible=false;
      temp_pin3.visible=false;
      
      temp_pin3.x=pin2_top.x;
      temp_pin3.y=pin2_top.y;

      pin2_top.visible=true;
      F.visible=true;
      pointF.visible=true;
      activity="";
      collider_1.inputEnabled=false;
      collider_1.enableBody=false;
      collider_1.visible=false;
      this.game.time.events.add(Phaser.Timer.SECOND*1,this.bringBackSlab, this);    
    }

    else if(currentobj==temp_pin2)
    {
      arrow,visible=false;
      temp_pin2.visible=false;
      pin2.visible=true;

      temp_pin2.x=pin3_top.x;
      temp_pin2.y=pin3_top.y;

      dialog.text="Now fix the last pin as same.";
      activity="fixpin_H";
    temp_pin1.events.onDragStart.add(function() {this.onDragStart(temp_pin1)}, this);
    temp_pin1.events.onDragStop.add(function() {this.onDragStop(temp_pin1)}, this);
     this.game.physics.arcade.enable(temp_pin1);
     temp_pin1.inputEnabled = true;
     temp_pin1.input.useHandCursor = true;
     temp_pin1.input.enableDrag(true);

     arrow.x=115;
     arrow.y=700;
     arrow.visible=true;

    }
    else if(currentobj==temp_pin1)
    {
      arrow.visible=false;
      temp_pin1.visible=false;
      pin1.visible=true;

      temp_pin1.x=pin4_top.x;
      temp_pin1.y=pin4_top.y;

      collider_1.inputEnabled=false;
      collider_1.enableBody=false;
      collider_1.visible=false;

      collider.inputEnabled=false;
      collider.enableBody=false;
      

      dialog.text="Good job. lets go back to diagram ";
      this.game.time.events.add(Phaser.Timer.SECOND*1,this.backToTopView, this);  

    }
    
  },

  clickOnPin1_top:function()
  {
    pin1_top.visible=false;
    temp_pin4.visible=true;

    tween1=this.game.add.tween(temp_pin4).to( {y:2000}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      
      arrow.x=pin2_top.x;
      arrow.y=pin2_top.y-50;
      arrow.visible=true;
      
      pin2_top.inputEnabled = true;
      pin2_top.input.useHandCursor = true;
      pin2_top.events.onInputDown.add(this.clickOnPin2_top, this);
      
    }.bind(this));

  },

  clickOnPin2_top:function()
  {

    pin2_top.visible=false;
    temp_pin3.visible=true;

    tween1=this.game.add.tween(temp_pin3).to( {y:2000}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      
      arrow.x=pin3_top.x;
      arrow.y=pin3_top.y-50;
      arrow.visible=true;
      
      pin3_top.inputEnabled = true;
      pin3_top.input.useHandCursor = true;
      pin3_top.events.onInputDown.add(this.clickOnPin3_top, this);
      
    }.bind(this));

  },

  clickOnPin3_top:function()
  {
    pin3_top.visible=false;
    temp_pin2.visible=true;

    tween1=this.game.add.tween(temp_pin2).to( {y:2000}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      
      arrow.x=pin4_top.x;
      arrow.y=pin4_top.y-50;
      arrow.visible=true;

      pointG.visible=true;
      
      pin4_top.inputEnabled = true;
      pin4_top.input.useHandCursor = true;
      pin4_top.events.onInputDown.add(this.clickOnPin4_top, this);
      
    }.bind(this));

  },

  clickOnPin4_top:function()
  {
    pin4_top.visible=false;
    temp_pin1.visible=true;

    tween1=this.game.add.tween(temp_pin1).to( {y:2000}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      
     arrow.visible=false;
     pointH.visible=true;

     dialog.text="Draw a line H to G and entend it to the side CD. Name the intercept as\nO'";
     activity="line_HG";

     arrow.x=temp_scale.x;
     arrow.y=temp_scale.y-50;
     arrow.visible=true;

     temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
     temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
     this.game.physics.arcade.enable(temp_scale);
     temp_scale.inputEnabled = true;
     temp_scale.input.useHandCursor = true;
     temp_scale.input.enableDrag(true);
      
      // pin4_top.inputEnabled = true;
      // pin4_top.input.useHandCursor = true;
      // pin4_top.events.onInputDown.add(this.clickOnPin4_top, this);
      
    }.bind(this));
  },


  backToTopView:function()
  {

    sideViewGroup.visible=false;
    
    pointG.visible=true;
    G.visible=true;
    pin3_top.visible=true;
     
    pointH.visible=true;
    H.visible=true;
    pin4_top.visible=true;

    dialog.text="Remove the slab from the paper.";
    activity="placeSlabBack2";

     slab.events.onDragStart.add(function() {this.onDragStart(slab)}, this);
     slab.events.onDragStop.add(function() {this.onDragStop(slab)}, this);
     this.game.physics.arcade.enable(slab);
     slab.inputEnabled = true;
     slab.input.useHandCursor = true;
     slab.input.enableDrag(true);

     arrow.x=slab.x;
     arrow.y=slab.y-100;
     arrow.angle=0;
     arrow.visible=true;
  },

  goTo3dView:function()
  {
    dialog.text="Come on lets see the pins E and F through other side of the slab.";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.show3dView, this); 
   
  },

  show3dView:function()
  {
    sideViewGroup.visible=true;
    activity="fixpin_G";
    dialog.text="Fix two pins such that these pins and the image of the pin E, F\n lie in a straight line. ";
    temp_pin2.events.onDragStart.add(function() {this.onDragStart(temp_pin2)}, this);
    temp_pin2.events.onDragStop.add(function() {this.onDragStop(temp_pin2)}, this);
     this.game.physics.arcade.enable(temp_pin2);
     temp_pin2.inputEnabled = true;
     temp_pin2.input.useHandCursor = true;
     temp_pin2.input.enableDrag(true);

     arrow.x=155;
     arrow.y=700;
     arrow.visible=true;


  },

  bringBackSlab:function()
  {
    arrow.visible=false;
    dialog.text="Now place the slab again on the outline.";
    activity="placeSlabBack";

    temp_slab.events.onDragStart.add(function() {this.onDragStart(temp_slab)}, this);
    temp_slab.events.onDragStop.add(function() {this.onDragStop(temp_slab)}, this);
     this.game.physics.arcade.enable(temp_slab);
     temp_slab.inputEnabled = true;
     temp_slab.input.useHandCursor = true;
     temp_slab.input.enableDrag(true);
     arrow.visible=false;
     arrow.x=temp_slab.x-10;
     arrow.y=temp_slab.y-50;
     arrow.visible=true;

     collider.visible=true;
     collider.inputEnabled=true;
     collider.enableBody=true;

  },

  drawLine_XO:function()
  {
    pencil.visible=false;
    protractor.visible=false;

    temp_pencil.x=100;
    temp_pencil.y=950;
    temp_pencil.visible=true;
    temp_protractor.x=220;
    temp_protractor.y=980;
    temp_protractor.visible=true;

    dialog.text="Now draw a line from X to O by using scale and pencil.";
    activity="line_XO";
    temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
    temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
    this.game.physics.arcade.enable(temp_scale);
    temp_scale.inputEnabled = true;
    temp_scale.input.useHandCursor = true;
    temp_scale.input.enableDrag(true);

    arrow.x=temp_scale.x;
    arrow.y=temp_scale.y-50;
    arrow.visible=true;


  },

  drawNormalOnDC:function()
  {
    pencil.visible=false;
    protractor.visible=false;

    temp_pencil.x=100;
    temp_pencil.y=950;
    temp_pencil.visible=true;
    temp_protractor.x=220;
    temp_protractor.y=980;
    temp_protractor.visible=true;

    dialog.text="Draw a perpendicular MM' through the point O' by using the scale \nand pencil.";
    temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
    temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
    this.game.physics.arcade.enable(temp_scale);
    temp_scale.inputEnabled = true;
    temp_scale.input.useHandCursor = true;
    temp_scale.input.enableDrag(true);

    arrow.x=temp_scale.x;
    arrow.y=temp_scale.y-50;
    arrow.visible=true;

    activity="line_MM'";

  },

  drawNormalOnAB:function()
  {
    pencil.visible=false;
    protractor.visible=false;

    temp_pencil.x=100;
    temp_pencil.y=950;
    temp_pencil.visible=true;
    temp_protractor.x=220;
    temp_protractor.y=980;
    temp_protractor.visible=true;

    dialog.text="Draw a perpendicular NN' through the point O by using the scale \nand pencil.";
    temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
    temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
    this.game.physics.arcade.enable(temp_scale);
    temp_scale.inputEnabled = true;
    temp_scale.input.useHandCursor = true;
    temp_scale.input.enableDrag(true);

    arrow.x=temp_scale.x;
    arrow.y=temp_scale.y-50;
    arrow.visible=true;

    activity="line_NN'";

  },

  take90onAB:function()
  {
    arrow.visible=false;
    collider_1.inputEnabled=false;
    collider_1.enableBody=false;
    collider_1.visible=false;
    pencil.visible=false;

    temp_pencil.x=100;
    temp_pencil.y=950;
    temp_pencil.visible=true;

    dialog.text="Take the protractor and place it on the side AB.";

     temp_protractor.events.onDragStart.add(function() {this.onDragStart(temp_protractor)}, this);
     temp_protractor.events.onDragStop.add(function() {this.onDragStop(temp_protractor)}, this);
     this.game.physics.arcade.enable(temp_protractor);
     temp_protractor.inputEnabled = true;
     temp_protractor.input.useHandCursor = true;
     temp_protractor.input.enableDrag(true);

     arrow.x=temp_protractor.x;
     arrow.y=temp_protractor.y-100;
     arrow.visible=true;

     activity="point_N";

  },
  drawPoint_O:function()
  {
    dialog.text="Mark a point 'O' on the side AB of the glass slab.";

    activity="point_O";

    arrow.x=temp_pencil.x-10;
    arrow.y=temp_pencil.y-50;
    arrow.visible=true;

    temp_pencil.events.onDragStart.add(function() {this.onDragStart(temp_pencil)}, this);
    temp_pencil.events.onDragStop.add(function() {this.onDragStop(temp_pencil)}, this);
     this.game.physics.arcade.enable(temp_pencil);
     temp_pencil.inputEnabled = true;
     temp_pencil.input.useHandCursor = true;
     temp_pencil.input.enableDrag(true);
  },
  

  update: function()
  {
    //DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(isPencilEnableDrag==true)
    {
      if(isAB==false&&lineName=="AB")
      {
        this.drawLineAB();
      }
      else if(isBC==false&&lineName=="BC")
      {
        this.drawLineBC();
      }
      else if(isCD==false&&lineName=="CD")
      {
        this.drawLineCD();
      }
      else if(isDA==false&&lineName=="DA")
      {
        this.drawLineDA();
      }
      else if(activity=="line_NN'")
      {
        this.drawDashLineNN();

      }

       else if(activity=="line_XO")
      {

        this.drawLineXO();
      
      }
      else if(activity=="line_HG")
      {
       // console.log("loooooooo");
        this.drawLineHG();
      }
      else if(activity=="line_MM'")
      {
        this.drawDashLineMM();

      }

      else if(activity=="line_OO'")
      {
        this.drawLineOO();
      }

     else if(activity=="line_EP")
      {

        this.drawDashLineEP();

      }

    }
    // if(arrow.visible){
    //   arrow.y+=(3+DeltaTime);
    //   if(arrow.y>=(arrow_y+50))
    //   {
    //     arrow.y=arrow_y;
    //   }
      
    // }/**/
    //line1.fromSprite(handle1, handle2, false);
    //this.drawLine();   
    
  },
  drawLineHG:function()
  {   
    this.drawVLine(1113,945,pencil.x,pencil.y);
    
    if(currentx>this.game.input.activePointer.x && currenty>this.game.input.activePointer.y)
    {
      
      console.log(pencil.x);
      pencil.x-=1.35;//1.38
      pencil.y-=2.3;//2.3

      currentx=this.game.input.activePointer.x;//-63
      currenty=this.game.input.activePointer.y;//+5.5

      if(pencil.x<=1004)
      {
        // console.log("ppppppppppppp");
        // activity="";
        arrow.visible=false;
        activity="point_M'";
        scale.visible=false;
        pencil.visible=false;

        this.drawVLine(1094,915,1004,760);
        temp_scale.x=170;
        temp_scale.y=670;
        temp_scale.visible=true;

        temp_pencil.x=100;
        temp_pencil.y=950;
        temp_pencil.visible=true;

        O2.visible=true;
        pointO2.visible=true;

      
      this.game.time.events.add(Phaser.Timer.SECOND*1,this.take90onDC, this);

      }
    }
  },

  take90onDC:function()
  {
      dialog.text="Take the protractor and place it on the side DC at point O'";
      temp_protractor.events.onDragStart.add(function() {this.onDragStart(temp_protractor)}, this);
      temp_protractor.events.onDragStop.add(function() {this.onDragStop(temp_protractor)}, this);
      this.game.physics.arcade.enable(temp_protractor);
      temp_protractor.inputEnabled = true;
      temp_protractor.input.useHandCursor = true;
      temp_protractor.input.enableDrag(true);
 
      arrow.x=temp_protractor.x;
      arrow.y=temp_protractor.y-100;
      arrow.visible=true;
      arrow.angle=0;

      activity="point_M";

  },

  drawDashLineEP:function()
  {
    //this.drawVLine(920,523,pencil.x,pencil.y);
    if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y)
    {
      pencil.x+=3.66;//3.6
      pencil.y+=6.3;//6.3
      currentx=this.game.input.activePointer.x-63;
      currenty=this.game.input.activePointer.y+5.5;
      if(pencil.y>=915)
      {
        this.drawDashLine(620,223,847,615);

        temp_scale.x=170;
        temp_scale.y=670;
        temp_scale.visible=true;
        temp_pencil.x=100;
        temp_pencil.y=950;
        temp_pencil.visible=true;

        // pointP.visible=true;
        P.visible=true;

        scale.visible=false;
        pencil.visible=false;
        arrow.visible=false;

        activity="";
      }
    }

  },

  drawLineOO:function()
  {
    this.drawVLine(920,523,pencil.x,pencil.y);
    if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y)
    {
      pencil.x+=2.25;//3.6
      pencil.y+=6.3;//6.3
      currentx=this.game.input.activePointer.x-63;
      currenty=this.game.input.activePointer.y+5.5;
      if(pencil.y>=766)
      {
        activity="";

        temp_scale.x=170;
        temp_scale.y=670;
        temp_scale.visible=true;
        temp_pencil.x=100;
        temp_pencil.y=950;
        temp_pencil.visible=true;

        scale.visible=false;
        pencil.visible=false;
        arrow.visible=false;

        this.game.time.events.add(Phaser.Timer.SECOND*1,this.extendLineEF, this);
      }
    }

  },

  extendLineEF:function()
  {
    dialog.text="Extend the live EF as parallel to GH and name the end point as P.";
    activity="line_EP";

    temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
    temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
    this.game.physics.arcade.enable(temp_scale);
    temp_scale.inputEnabled = true;
    temp_scale.input.useHandCursor = true;
    temp_scale.input.enableDrag(true);
  
    arrow.x=temp_scale.x;
    arrow.y=temp_scale.y-50;
    arrow.visible=true;
    arrow.angle=0;


  },

  drawLineXO:function()
  {
    this.drawVLine(817,343,pencil.x,pencil.y);
    if(currentx<this.game.input.activePointer.x && currenty<this.game.input.activePointer.y)
    {

      pencil.x+=3.6;//3.6
      pencil.y+=6.3;//6.3
      // console.log("pencilX="+pencil.x+"peniclY"+pencil.y);
      // console.log("mouseX"+this.input.activePointer.x+"mouseY"+this.input.activePointer.y);
      currentx=this.game.input.activePointer.x-63;
      currenty=this.game.input.activePointer.y+5.5;
      // this.game.input.activePointer.x=pencil.x;
      // this.game.input.activePointer.y=pencil.y;

      if(pencil.x>=920)
      {
        activity="";
        arrow.visible=false;
        scale.visible=false;
        pencil.visible=false;

        temp_scale.x=170;
        temp_scale.y=670;
        temp_scale.visible=true;
        temp_pencil.x=100;
        temp_pencil.y=950;
        temp_pencil.visible=true;
        this.game.time.events.add(Phaser.Timer.SECOND*1,this.toStartPlacePin_EF, this);
      }
    }

  },

  toStartPlacePin_EF:function()
  {
    dialog.text="Fix two pins on the line XO and label them E and F";
    activity="fixpin_E";

    temp_pin4.events.onDragStart.add(function() {this.onDragStart(temp_pin4)}, this);
    temp_pin4.events.onDragStop.add(function() {this.onDragStop(temp_pin4)}, this);
    this.game.physics.arcade.enable(temp_pin4);
    temp_pin4.inputEnabled = true;
    temp_pin4.input.useHandCursor = true;
    temp_pin4.input.enableDrag(true);

    arrow.visible=false;
    arrow.x=235;
    arrow.y=700;
    arrow.angle=0;
    arrow.visible=true;


  },

  drawDashLineMM:function()
  {
    console.log("draw");
    pencil.y=this.game.input.activePointer.y;

    if(pencil.y<pointM1.y)
    {
      pencil.y=pointM1.y;
    }
    if(pencil.y>=849)
    {
      arrow.visible=false;
      pencil.y=849;
      activity="";
      this.drawDashLine(703,383,703,549);

      scale.visible=false;
      temp_scale.x=170;
      temp_scale.y=670;
      temp_scale.visible=true;
      pencil.visible=false;
      temp_pencil.x=100;
      temp_pencil.y=950;
      temp_pencil.visible=true;

      pointM1.visible=false;
      M1.visible=true;
      M2.visible=true;


      this.game.time.events.add(Phaser.Timer.SECOND*1,this.drawJoinLineOO, this);
     
    
    }

  },

  drawJoinLineOO:function()
  {

    dialog.text="Good.... now join O and O' by using scale and pencil.";
    activity="line_OO'";

    temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
  temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
  this.game.physics.arcade.enable(temp_scale);
  temp_scale.inputEnabled = true;
  temp_scale.input.useHandCursor = true;
  temp_scale.input.enableDrag(true);

  arrow.x=temp_scale.x;
  arrow.y=temp_scale.y-50;
  arrow.visible=true;
  },
  drawDashLineNN:function()
  {
    
   /// this.drawDashLine(618,130,pencil.x-300,pencil.y-300);

    pencil.y=this.game.input.activePointer.y;
    if(pencil.y<pointN1.y)
    {
      pencil.y=pointN1.y;
    }
    if(pencil.y>=616)
    {
      arrow.visible=false;
      pencil.y=616;
      activity="";
     this.drawDashLine(620,130,620,316);
      scale.visible=false;
      temp_scale.x=170;
      temp_scale.y=670;
      temp_scale.visible=true;
      pencil.visible=false;
      temp_pencil.x=100;
      temp_pencil.y=950;
      temp_pencil.visible=true;

      pointN1.visible=false;
      N1.visible=true;
      N2.visible=true;

      dialog.text="Now take the protractor and place it on the normal NN'";

      temp_protractor.events.onDragStart.add(function() {this.onDragStart(temp_protractor)}, this);
      temp_protractor.events.onDragStop.add(function() {this.onDragStop(temp_protractor)}, this);
      this.game.physics.arcade.enable(temp_protractor);
      temp_protractor.inputEnabled = true;
      temp_protractor.input.useHandCursor = true;
      temp_protractor.input.enableDrag(true);

      arrow.x=temp_protractor.x;
      arrow.y=temp_protractor.y-100;
      arrow.visible=true;
      activity="point_X";

    }
  },

 

  drawLineAB:function()
  {
    this.drawVLine(813,523,pencil.x,pencil.y);
    pencil.x=this.game.input.activePointer.x;
    if(pencil.x<813)
    {
      pencil.x=813;
    }
    if(pencil.x>=1244)
    {
      pencil.x=1244;
      isAB=true;
      lineName="BC";
      pencil.visible=false;
      pencil.visible=true;
      this.drawVLine(813,523,1244,523);
      arrow.x=1260;
      arrow.y=560;
    arrow.angle=0;
    arrow.visible=true;
    }
  },
  drawLineBC:function()
  {
    this.drawVLine(1244,523,pencil.x,pencil.y);
    pencil.y=this.game.input.activePointer.y;
    if(pencil.y<523)
    {
      pencil.y=523;
    }
    if(pencil.y>=758)
    {
      pencil.y=758;
      isBC=true;
      lineName="CD";
      pencil.visible=false;
      pencil.visible=true;
     this.drawVLine(1244,523,1244,758);
      arrow.x=1240;
      arrow.y=780;
    arrow.angle=90;
    arrow.visible=true;
    }
  },
  drawLineCD:function()
  {
    this.drawVLine(1244,758,pencil.x,pencil.y);
    pencil.x=this.game.input.activePointer.x;
    if(pencil.x>1244)
    {
      pencil.x=1244;
    }
    if(pencil.x<=813)
    {
      pencil.x=813;
      pencil.angle=-65;
      isCD=true;
      lineName="DA";
      pencil.visible=false;
      pencil.visible=true;
     this.drawVLine(1244,758,813,758);
      arrow.x=800;
      arrow.y=670;
    arrow.angle=180;
    arrow.visible=true;
    }
  },
  drawLineDA:function()
  {
   this.drawVLine(813,760,pencil.x,pencil.y);
    pencil.y=this.game.input.activePointer.y;
    if(pencil.y>760)
    {
      pencil.y=760;
    }
    if(pencil.y<=523)
    {
      pencil.y=523;
      pencil.angle=-65;
      isDA=true;
      //lineName="DA";
      pencil.visible=false;
      pencil.visible=true;
      this.drawVLine(813,760,813,523);
    //   arrow.x=800;
    //   arrow.y=670;
    // arrow.angle=180;
    pencil.visible=false;
    temp_pencil.visible=true;
    temp_pencil.x=100;
    temp_pencil.y=950;
    temp_pencil.inputEnabled = false;
    temp_pencil.input.useHandCursor = false;
    temp_pencil.input.enableDrag(false);
    arrow.visible=false;

    dialog.text="Now remove the slab and label the outline as ABCD.";
    arrow.x=slab.x;
    arrow.y=slab.y-100;
    arrow.angle=0;
    arrow.visible=true;

     slab.events.onDragStart.add(function() {this.onDragStart(slab)}, this);
     slab.events.onDragStop.add(function() {this.onDragStop(slab)}, this);
     this.game.physics.arcade.enable(slab);
     slab.inputEnabled = true;
     slab.input.useHandCursor = true;
     slab.input.enableDrag(true);

    // collider_1.inputEnabled = false;
    // collider_1.enableBody =false;
    // collider_1.visible=false;
    }
  },

  clickOnPencil:function()
  {
   // arrow.visible=false;
    isPencilEnableDrag=true;
    currentx=this.game.input.activePointer.x;
    currenty=this.game.input.activePointer.y;

  },
  clickOffPencil:function()
  {
    isPencilEnableDrag=false;
  },

  nextScene:function(){
    play.visible=true;
    dialog.y=50;
    dialog_box.scale.setTo(.8,.8);
    // voice.destroy(true);
    //     voice=this.game.add.audio("step12_1",1);
    //     voice.play();
    dialog.text="Click on the next button to see the image formation, when \nthe object is placed beyond 2F\u2081.";
  },

  drawArc:function(angle){
    var arc1=this.game.add.graphics(0,0);
    arc1.lineStyle(2, 0x000000);
   // arc1.beginFill(0xFF3300);
    arc1.arc(0, 0, angle, 0, 1, false);
    //arc1.x=xp;
    //arc1.yp=yp;
    return arc1;
  },

  drawVLine:function(xp,yp,xp1,yp1)
  {
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(2,0x000000,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
      
        lineGroup.add(line1);
        
  },

  // drawDashLine:function(xp,yp,xp1,yp1)
  // {
  //   var line2=this.game.add.graphics(0, 0);
  //   line2.lineStyle(2,0x000000,1);
  //   line2.moveTo(xp, yp);
  //   line2.lineTo(xp1, yp1);
  
  //   line2Group.add(line2);
  // },
  drawDashLine:function(xp,yp,xp1,yp1)
  {
    var line2=this.game.add.bitmapData(1920,1080);
        line2.ctx.beginPath();
        line2.ctx.lineWidth = 1;
        line2.ctx.strokeStyle = 'black';
        line2.ctx.setLineDash([10,5]);
        line2.ctx.moveTo(xp,yp);
        line2.ctx.lineTo(xp1,yp1);
        line2.ctx.stroke();
        line2.ctx.closePath();
       line2= this.game.add.sprite(300,300,line2);
        lineDashGroup.add(line2);


  },
  detectCollision:function(){
    
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj,collider,function() {this.match_Obj()},null,this);
        //console.log("fhggddddddddddddd");
    }
    if(collider_1.enableBody && currentobj!==null)
    {
      this.game.physics.arcade.overlap(currentobj,collider_1,function() {this.match_Obj()},null,this);
    }
    if(collider_2.enableBody && currentobj!==null)
    {
      this.game.physics.arcade.overlap(currentobj,collider_2,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==paper){
        // arrow.visible=true;
        arrow.visible=false;
        arrow.x=170;
        arrow.y=200;
        arrow.visible=true;
        console.log("hhfhf");
      }
      else if(currentobj==Nail1)
      {
        arrow.visible=false;
        arrow.x=150;
        arrow.y=500;
        arrow.visible=true;
      }
      else if(currentobj==temp_slab)
      {
        arrow.visible=false;
        arrow.x=temp_slab.x-10;
        arrow.y=temp_slab.y-50;
        arrow.visible=true;
      }
      else if(currentobj==temp_pencil)
      {
        arrow.visible=false;
        arrow.x=temp_pencil.x-10;
       arrow.y=temp_pencil.y-50;
       arrow.visible=true;
      }
      else if(currentobj==slab)
      {
        arrow.x=slab.x;
        arrow.y=slab.y-100;
        arrow.angle=0;
        arrow.visible=true;

      }
      else if(currentobj==temp_protractor)
      {
        arrow.visible=false;
        arrow.x=temp_protractor.x;
        arrow.y=temp_protractor.y-100;
        arrow.visible=true;
      }
      else if(currentobj==temp_scale)
      {
        arrow.visible=false;
        arrow.x=temp_scale.x;
        arrow.y=temp_scale.y-50;
        arrow.visible=true;


      }
      else if(currentobj==temp_pin4)
      {

        arrow.visible=false;
        arrow.x=235;
        arrow.y=700;
        arrow.visible=true;
      }
      else if(currentobj==temp_pin3)
      {

        arrow.visible=false;
        arrow.x=195;
        arrow.y=700;
        arrow.visible=true;
      }
      else if(currentobj==temp_pin2)
      {
        arrow.visible=false;
        arrow.x=155;
        arrow.y=700;
        arrow.visible=true;
      }
      else if(currentobj==temp_pin1)
      {
        arrow.visible=false;
        arrow.x=115;
        arrow.y=700;
        arrow.visible=true;

      }
      // else if(currentobj==len)
      // {

      // }

      
     // arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      // voice.destroy();
      
      this.state.start("Experiment_2", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
//window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
