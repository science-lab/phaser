var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        this.game.load.image("bg1","assets/Glass_slab/Bg_top_view.png");
        this.game.load.image("bg2","assets/Glass_slab/Bg_Side_view.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('blackDot','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
   
        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');

        this.game.load.image('obs_head','assets/obs_head.png');
        this.game.load.image('obs_base','assets/obs_base.png');



///////////////////////////glass slab////////////////////////////////////
        this.game.load.image('paper','assets/Glass_slab/white_paper.png');    
        this.game.load.image('directionArrow','assets/Lens/dir_arrow.png'); 
    
        this.game.load.image('Nail','assets/Lens/Nail.png');    
        this.game.load.image('Nail_top','assets/Lens/Nail_top.png'); 
           
        this.game.load.image('drawing_board_top','assets/Glass_slab/art_board.png');   
        this.game.load.image('drawing_board_side','assets/Lens/drawing_board_side.png'); 
        this.game.load.image('scale','assets/Glass_slab/Scale.png');
        this.game.load.image('pencil','assets/Glass_slab/Pencil.png');
        this.game.load.image('protractor','assets/Glass_slab/Protractor.png');
        this.game.load.image('slab','assets/Glass_slab/glass_Slab_top.png');
        this.game.load.image('pin','assets/Glass_slab/Nail_Side_view.png');
        this.game.load.image('pin_top','assets/Glass_slab/Nail_top.png');
        this.game.load.image('slab3d','assets/Glass_slab/Glass_Slab_side_view_new.png');
       // this.game.load.image('table_side_view','assets/Glass_slab/Bg_Side_view.png')


        // this.game.load.image('img1','assets/Lens/img1.png');
        // this.game.load.image('img2','assets/Lens/img2.png');
        // this.game.load.image('img3','assets/Lens/img3.png');
        // this.game.load.image('img4','assets/Lens/img4.png');
        // this.game.load.image('img5','assets/Lens/img5.png');
        // this.game.load.image('img6','assets/Lens/img6.png');
        // this.game.load.image('img7','assets/Lens/img7.png');
        ////////////////////////for materials///////////////////
        this.game.load.image('m_slab','assets/Glass_slab/materials/m_slab.png');
        this.game.load.image('m_pencil','assets/Glass_slab/materials/m_pencil.png');
        this.game.load.image('m_pin','assets/Glass_slab/materials/m_pins.png');
        this.game.load.image('m_scale','assets/Glass_slab/materials/m_scale.png');
        this.game.load.image('m_protractor','assets/Glass_slab/materials/m_potractor.png');
        this.game.load.image('m_paper','assets/Glass_slab/materials/m_paper.png');
        this.game.load.image('Nail_gold','assets/Glass_slab/materials/Nail_gold.png');
        ////////////////////////Glass slab//////////////////////////
        this.game.load.image('slabDiagram','assets/Glass_slab/glassSlab_diagram.png');
        this.game.load.image('slabDiagram2','assets/Glass_slab/glassSlab_diagram2.png');

        this.game.load.atlasJSONHash('arrowAnimation', 'assets/Glass_slab/arrow.png', 'assets/Glass_slab/arrow.json');




      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');  
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Procedure_4','assets/audio/Procedure_4.mp3');
      this.game.load.audio('Procedure_5','assets/audio/Procedure_5.mp3');  
      this.game.load.audio('Procedure_6','assets/audio/Procedure_6.mp3');
      this.game.load.audio('Procedure_7','assets/audio/Procedure_7.mp3');
      this.game.load.audio('Procedure_8','assets/audio/Procedure_8.mp3');  

      this.game.load.audio('step_01','assets/audio/step_01.mp3'); 
      this.game.load.audio('step_02','assets/audio/step_02.mp3'); 
      this.game.load.audio('step_1','assets/audio/step_1.mp3');
      this.game.load.audio('step_2','assets/audio/step_2.mp3');
      this.game.load.audio('step_3','assets/audio/step_3.mp3');
      this.game.load.audio('step_4','assets/audio/step_4.mp3');
      this.game.load.audio('step_5','assets/audio/step_5.mp3');
      this.game.load.audio('step_6','assets/audio/step_6.mp3');
      this.game.load.audio('step_7','assets/audio/step_7.mp3');
      this.game.load.audio('step_8','assets/audio/step_8.mp3');
      this.game.load.audio('step_9','assets/audio/step_9.mp3');
      this.game.load.audio('step_10','assets/audio/step_10.mp3');
      this.game.load.audio('step_11','assets/audio/step_11.mp3');
      this.game.load.audio('step_11_2','assets/audio/step_11_2.mp3');
      this.game.load.audio('step_11_3','assets/audio/step_11_3.mp3');
      this.game.load.audio('step_11_4','assets/audio/step_11_4.mp3');
      this.game.load.audio('step_12','assets/audio/step_12.mp3');
      this.game.load.audio('step_13','assets/audio/step_13.mp3');
      this.game.load.audio('step_14','assets/audio/step_14.mp3');
      this.game.load.audio('step_15','assets/audio/step_15.mp3');
      this.game.load.audio('step_16','assets/audio/step_16.mp3');
      this.game.load.audio('step_17','assets/audio/step_17.mp3');
      this.game.load.audio('step_18','assets/audio/step_18.mp3');
      this.game.load.audio('step_19','assets/audio/step_19.mp3');
      this.game.load.audio('step_20','assets/audio/step_20.mp3');
      this.game.load.audio('step_21','assets/audio/step_21.mp3');
      this.game.load.audio('step_22','assets/audio/step_22.mp3');
      this.game.load.audio('step_23','assets/audio/step_23.mp3');
      this.game.load.audio('step_24','assets/audio/step_24.mp3');
      this.game.load.audio('step_25','assets/audio/step_25.mp3');
      this.game.load.audio('step_26','assets/audio/step_26.mp3');
      this.game.load.audio('step_27','assets/audio/step_27.mp3');
      this.game.load.audio('step_28','assets/audio/step_28.mp3');
      this.game.load.audio('step_29','assets/audio/step_29.mp3');
      this.game.load.audio('step_30','assets/audio/step_30.mp3');
      this.game.load.audio('step_31','assets/audio/step_31.mp3');
      this.game.load.audio('step_32','assets/audio/step_32.mp3');
      this.game.load.audio('step_33','assets/audio/step_33.mp3');
      this.game.load.audio('step_33_2','assets/audio/step_33_2.mp3');
      this.game.load.audio('step_33_3','assets/audio/step_33_3.mp3');
      this.game.load.audio('step_33_4','assets/audio/step_33_4.mp3');
      this.game.load.audio('step_34','assets/audio/step_34.mp3');
      this.game.load.audio('step_35','assets/audio/step_35.mp3');
      this.game.load.audio('step_35_2','assets/audio/step_35_2.mp3');
      this.game.load.audio('step_35_3','assets/audio/step_35_3.mp3');
      this.game.load.audio('step_35_4','assets/audio/step_35_4.mp3');
      this.game.load.audio('step_36','assets/audio/step_36.mp3');
      this.game.load.audio('step_37','assets/audio/step_37.mp3');
      this.game.load.audio('step_37_2','assets/audio/step_37_2.mp3');
      this.game.load.audio('step_37_3','assets/audio/step_37_3.mp3');
      this.game.load.audio('step_37_4','assets/audio/step_37_4.mp3');
      this.game.load.audio('step_38','assets/audio/step_38.mp3');
      this.game.load.audio('step_38_2','assets/audio/step_38_2.mp3');
      this.game.load.audio('step_38_3','assets/audio/step_38_3.mp3');
      this.game.load.audio('step_38_4','assets/audio/step_38_4.mp3');
      this.game.load.audio('step_39','assets/audio/step_39.mp3');
      this.game.load.audio('step_39_2','assets/audio/step_39_2.mp3');
      this.game.load.audio('step_39_3','assets/audio/step_39_3.mp3');
      this.game.load.audio('step_39_4','assets/audio/step_39_4.mp3');
      // this.game.load.audio('step_10','assets/audio/step_10.mp3');

      

      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
    


      



      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
     // this.game.state.start("Experiment_2");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
     // this.game.state.start("Theory");
      // this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

