var procedure_1 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  var current_scene;
  var previous_scene;
  //bools
  var muted;
  
  //ip address
  var ip;
  
  }
  
  procedure_1.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("Procedure_1",1);
 voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/
  //procedure_audio1=this.game.add.audio('procedure_voice1',1);
  //procedure_audio2=this.game.add.audio('procedure_voice2',1);

 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}
  current_scene=1;
  previous_scene=0;
  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_text=this.game.add.text(250,200,"Procedure:",headfontStyle);
  //exp_Name="decomposition_reaction";
  //console.log(exp_Name+"......");
  
//page1
      procedure_step_1="1. Fix a sheet of white drawing paper on a drawing board using drawing pins. ";
      procedure_step_2="2. Place a rectangular glass slab over the sheet in the middle and draw the\n    outline of the slab with a sharp pencil. Remove the glass slab and label the \n    outline as ABCD.";
     
     
//Page2
       slabDiagram=this.game.add.sprite(500,280,'slabDiagram2');
       slabDiagram.scale.setTo(1,.9);
      slabDiagram.visible=false;
      

//Page3
       
      
      procedure_step_3="3. Mark a point O on the side AB of the glass slab. Draw a perpendicular NN'\n    at the point O.";
      procedure_step_4="4. Draw a line XO which makes an angle of 30\u00BA with the normal NN'.\n    You can make this angle with the help of a protractor.";
      procedure_step_5="5. Take four identical pins. Fix two pins say E and F vertically on the line XO.\n    The distance between the pins should be about 2 to 3 cm.";
      procedure_step_6="6. Again place the glass slab on the outline. Look for the images of the pins E \n    and F through the opposite side of the glass slab. Fix two other pins, say G \n    and H, such that these pins and the images of E and F lie in a straight line.";
      
//Page4
      procedure_step_7="7. Remove the pins and the glass slab. ";
      procedure_step_8="8. Join the positions of the pins G and H and extend it up to the side CD. Let HG\n    meet CD at O'. Draw a perpendicular MM' to CD at O'.";
      procedure_step_9="9. Join O and O'. Also produce EF upto P, as shown by a dotted line in the figure.";
      procedure_step_10="10. Measure the angle of incidence (\u2220i), angle of refraction (\u2220r) and the angle of\n      emergence (\u2220e) with a protractor and record your observations.";
      procedure_step_11="11. Repeat the experiment by taking different angles of incidence such as 40\u00BA.";

//page1
      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,370,procedure_step_2,fontStyle);

//page2
        // slabDiagram.scale.setTo(.6,.64);
        // slabDiagram.visible=true;
//page3

      // procedure_step_text_3=this.game.add.text(300,300,procedure_step_3,fontStyle);
      // procedure_step_text_4=this.game.add.text(300,430,procedure_step_4,fontStyle);
      // procedure_step_text_5=this.game.add.text(300,560,procedure_step_5,fontStyle);
      // procedure_step_text_6=this.game.add.text(300,680,procedure_step_6,fontStyle);

//Page4
        //  procedure_step_text_7=this.game.add.text(300,300,procedure_step_7,fontStyle);
        //  procedure_step_text_8=this.game.add.text(300,380,procedure_step_8,fontStyle);
        //  procedure_step_text_9=this.game.add.text(300,510,procedure_step_9,fontStyle);
        //  procedure_step_text_10=this.game.add.text(300,600,procedure_step_10,fontStyle);
        //  procedure_step_text_11=this.game.add.text(300,730,procedure_step_11,fontStyle);
    
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  next_btn = this.game.add.sprite(1620,890,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1620,890,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 
  prev_btn = this.game.add.sprite(300,890,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(300,890,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  prev_btn.visible=false;
  //next_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrevious, this);
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
    
    // For Full screen checking.
    toNext:function(){
      previous_scene=current_scene;
        current_scene++;
          this.addProcedure();
          if(current_scene==4)
          {
            
          next_btn.visible=false;
          play.visible=true;

          }
           prev_btn.visible=true;
          // next_btn.visible=false;
          // play.visible=true;
        },
        toPrevious:function(){
          previous_scene=current_scene;
          current_scene--;
          this.addProcedure();
          if(current_scene<=4)
          {
            if(current_scene==1)
            {
              prev_btn.visible=false;
            }
            next_btn.visible=true;

            
          //prev_btn.visible=false;
          }
          
          // next_btn.visible=true;
          // prev_btn.visible=false;
        },
        addProcedure:function(){
          switch(current_scene){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Procedure_1",1);
              voice.play();
                  if(previous_scene==2)
                  {
                    slabDiagram.visible=false;
                  }
                  procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
                  procedure_step_text_2=this.game.add.text(300,370,procedure_step_2,fontStyle);
              break;
            case 2:
              voice.destroy(true);
                  if(previous_scene==1)
                  {
                    procedure_step_text_1.destroy();
                    procedure_step_text_2.destroy();
                  }
            else if(previous_scene==3)
                  {
                    procedure_step_text_3.destroy();
                    procedure_step_text_4.destroy();
                    procedure_step_text_5.destroy();
                    procedure_step_text_6.destroy();

                  }
                  slabDiagram.visible=true;
              break;  
            case 3:
                    if(previous_scene==2)
                    {
                      slabDiagram.visible=false;
                    }
              else if(previous_scene==4)
                    {
                    voice.destroy(true);
              
                    procedure_step_text_7.destroy();
                    procedure_step_text_8.destroy();
                    procedure_step_text_9.destroy();
                    procedure_step_text_10.destroy();
                    procedure_step_text_11.destroy();

                    }    
                    voice=this.game.add.audio("Procedure_2",1);
                    voice.play(); 
                    procedure_step_text_3=this.game.add.text(300,300,procedure_step_3,fontStyle);
                    procedure_step_text_4=this.game.add.text(300,430,procedure_step_4,fontStyle);
                    procedure_step_text_5=this.game.add.text(300,560,procedure_step_5,fontStyle);
                    procedure_step_text_6=this.game.add.text(300,680,procedure_step_6,fontStyle);
              
              break;
            case 4:
              voice.destroy(true);
              voice=this.game.add.audio("Procedure_3",1);
              voice.play();
                    if(previous_scene==3)
                    {
                    procedure_step_text_3.destroy();
                    procedure_step_text_4.destroy();
                    procedure_step_text_5.destroy();
                    procedure_step_text_6.destroy();
                    }
                     procedure_step_text_7=this.game.add.text(300,300,procedure_step_7,fontStyle);
                     procedure_step_text_8=this.game.add.text(300,380,procedure_step_8,fontStyle);
                     procedure_step_text_9=this.game.add.text(300,510,procedure_step_9,fontStyle);
                     procedure_step_text_10=this.game.add.text(300,600,procedure_step_10,fontStyle);
                     procedure_step_text_11=this.game.add.text(300,730,procedure_step_11,fontStyle);
                    
              break;  
          
          }
        },
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
  
    //For to next scene   
   
        toExperiment:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
          //procedure_audio2.destroy();
          this.state.start("Experiment_1", true, false);//Experiment_3
           
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    //procedure_audio1.destroy();
    //procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    //procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //procedure_audio1.destroy();
  //procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  