var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');

///////////////////////////lens////////////////////////////////////
        this.game.load.image('paper','assets/Lens/paper.png');    
        this.game.load.image('dir_arrow','assets/Lens/dir_arrow.png'); 
        this.game.load.image('Lens_1','assets/Lens/Lens_1.png');    
        this.game.load.image('Lens_2','assets/Lens/Lens_2.png'); 
        this.game.load.image('Nail','assets/Lens/Nail.png');    
        this.game.load.image('Nail_top','assets/Lens/Nail_top.png'); 
        this.game.load.image('Object_1','assets/Lens/Object_1.png');    
        this.game.load.image('drawing_board_top','assets/Lens/drawing_board_top.png');   
        this.game.load.image('drawing_board_side','assets/Lens/drawing_board_side.png'); 
        this.game.load.image('scale','assets/Lens/scale.png');
        this.game.load.image('pencil','assets/Lens/pencil.png');
        this.game.load.image('pencil_M','assets/Lens/pencil_M.png');
        this.game.load.image('nails_M','assets/Lens/nails_M.png');

        this.game.load.image('img1','assets/Lens/img1.png');
        this.game.load.image('img2','assets/Lens/img2.png');
        this.game.load.image('img3','assets/Lens/img3.png');
        this.game.load.image('img4','assets/Lens/img4.png');
        this.game.load.image('img5','assets/Lens/img5.png');
        this.game.load.image('img6','assets/Lens/img6.png');
        this.game.load.image('img7','assets/Lens/img7.png');
        


      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');  
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Procedure_4','assets/audio/Procedure_4.mp3');
      this.game.load.audio('Procedure_5','assets/audio/Procedure_5.mp3');  
      this.game.load.audio('Procedure_6','assets/audio/Procedure_6.mp3');
      this.game.load.audio('Procedure_7','assets/audio/Procedure_7.mp3');
      this.game.load.audio('Procedure_8','assets/audio/Procedure_8.mp3');  
      

      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('step_0','assets/audio/step_0.mp3');
      this.game.load.audio('step_1','assets/audio/step_1.mp3');
      this.game.load.audio('step_2','assets/audio/step_2.mp3');
      this.game.load.audio('step_3','assets/audio/step_3.mp3');
      this.game.load.audio('step_4','assets/audio/step_4.mp3');
      this.game.load.audio('step_5','assets/audio/step_5.mp3');
      this.game.load.audio('step_6','assets/audio/step_6.mp3');
      this.game.load.audio('step7_1','assets/audio/step7_1.mp3');
      this.game.load.audio('step7_2','assets/audio/step7_2.mp3');

      this.game.load.audio('step8_1','assets/audio/step8_1.mp3');
      this.game.load.audio('step8_2','assets/audio/step8_2.mp3');
      this.game.load.audio('step8_3','assets/audio/step8_3.mp3');
      this.game.load.audio('step8_4','assets/audio/step8_4.mp3');
      this.game.load.audio('step8_5','assets/audio/step8_5.mp3');
      this.game.load.audio('step8_6','assets/audio/step8_6.mp3');
      this.game.load.audio('step9_1','assets/audio/step9_1.mp3');
      this.game.load.audio('step9_2','assets/audio/step9_2.mp3');
      this.game.load.audio('step10_1','assets/audio/step10_1.mp3');
      this.game.load.audio('step10_2','assets/audio/step10_2.mp3');
      this.game.load.audio('step11_1','assets/audio/step11_1.mp3');
      this.game.load.audio('step11_2','assets/audio/step11_2.mp3');
      this.game.load.audio('step11_3','assets/audio/step11_3.mp3');
      this.game.load.audio('step11_4','assets/audio/step11_4.mp3');
      this.game.load.audio('step11_5','assets/audio/step11_5.mp3');
      this.game.load.audio('step11_6','assets/audio/step11_6.mp3');
      this.game.load.audio('step12_1','assets/audio/step12_1.mp3');
      this.game.load.audio('step12_2','assets/audio/step12_2.mp3');
      this.game.load.audio('step12_3','assets/audio/step12_3.mp3');
      this.game.load.audio('step12_4','assets/audio/step12_4.mp3');
      this.game.load.audio('step12_5','assets/audio/step12_5.mp3');
      this.game.load.audio('step12_6','assets/audio/step12_6.mp3');


      



      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
      //this.game.state.start("Experiment_6");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

