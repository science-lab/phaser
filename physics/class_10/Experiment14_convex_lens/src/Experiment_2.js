var experiment_2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;

var handle1;
var handle2;
var line1;
var handleMove;
var linecount;
var currentx;
var currenty;
}

experiment_2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);

 handleMove=false;
 
 
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 linecount=1;
 currentx=0;
 currenty=0;
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg1');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  
  this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
        bgGroup=this.game.add.group();
        var objBox = this.game.add.graphics(0, 0);
        objBox.lineStyle(8,0xC04000,1);
        objBox.beginFill(0xFFFFC2);
        objBox.drawRoundedRect(0, 0, 300, 620,15);
        objBox.x=30;
        objBox.y=250; 
        drawing_board_top=this.game.add.sprite(1050,600, 'drawing_board_top');
        drawing_board_top.scale.setTo(2.15,1.05);
        drawing_board_top.anchor.set(.5);
        paper1=this.game.add.sprite(1050,600, 'paper');
        paper1.scale.setTo(2.3,1.1);
        paper1.anchor.set(.5);
        paper1.alpha=0;
        paper=this.game.add.sprite(40,260, 'paper');
        paper.scale.setTo(.4,.3);
        paper.xp=40;
        paper.yp=260;

        Nail_top1=this.game.add.sprite(385,200, 'Nail_top');
        Nail_top1.scale.set(1.5);
        Nail_top2=this.game.add.sprite(1695,200, 'Nail_top');
        Nail_top2.scale.set(1.5);
        Nail_top3=this.game.add.sprite(1695,980, 'Nail_top');
        Nail_top3.scale.set(1.5);
        Nail_top4=this.game.add.sprite(385,980, 'Nail_top');
        Nail_top4.scale.set(1.5);
        Nail_top1.visible=false;
        Nail_top2.visible=false;
        Nail_top3.visible=false;
        Nail_top4.visible=false;

        Nail1=this.game.add.sprite(250,580, 'Nail');
        Nail2=this.game.add.sprite(20,0, 'Nail');//270,550,
        Nail3=this.game.add.sprite(0,20, 'Nail');
        Nail4=this.game.add.sprite(20,20, 'Nail');
        Nail5=this.game.add.sprite(0,0, 'collider');
        Nail1.addChild(Nail2);

        Nail1.addChild(Nail3);
        Nail1.addChild(Nail4);
        Nail1.addChild(Nail5);
        Nail5.alpha=0;
        //Nail1.x=1000;
        Nail1.scale.set(1.5);
        Nail1.xp=250;
        Nail1.yp=580;
        // Nail2.scale.set(1.5);
        // Nail3.scale.set(1.5);
        // Nail4.scale.set(1.5);

        Object_1=this.game.add.sprite(100,480, 'Object_1');
        Object_1.anchor.setTo(.5,0);
        Object_1.xp=100;
        Object_1.yp=480;
        Object_2=this.game.add.sprite(1385,723, 'Object_1');
        Object_2.anchor.setTo(.5,0);
        Object_2.scale.y=-.7;
        Object_2.visible=false;
        Lens_1=this.game.add.sprite(180,600, 'Lens_1');
        Lens_1.scale.set(.6);
        Lens_1.anchor.setTo(.5,.5);
        Lens_1.xp=180;
        Lens_1.yp=600;
        Origin_1=this.game.add.text(240,760,"O",lablel_fontStyle);
        Origin_1.xp=240;
        Origin_1.yp=760;
        focus_1=this.game.add.text(80,760,"F\u2081",lablel_fontStyle);
        focus_1.xp=80;
        focus_1.yp=760;
        focus_11=this.game.add.text(1240,605,"F\u2082",lablel_fontStyle);//1040 605
        focus_11.xp=80;
        focus_11.yp=760;
        focus_11.visible=false;
        focus_2=this.game.add.text(160,760,"2F\u2081",lablel_fontStyle);
        focus_2.xp=160;
        focus_2.yp=760;
        focus_21=this.game.add.text(1440,605,"2F\u2082",lablel_fontStyle);
        focus_21.xp=160;
        focus_21.yp=760;
        focus_21.visible=false;

        C1=this.game.add.text(640,550,"C\u2081",lablel_fontStyle);//1040 605
        C1.visible=false;
        C2=this.game.add.text(1440,550,"C\u2082",lablel_fontStyle);//1040 605
        C2.visible=false;

        A=this.game.add.text(540,435,"A",lablel_fontStyle);
        B=this.game.add.text(540,605,"B",lablel_fontStyle);
        A.visible=false;
        B.visible=false;
        A1=this.game.add.text(1375,735,"A'",lablel_fontStyle);
        B1=this.game.add.text(1375,555,"B'",lablel_fontStyle);
        A1.visible=false;
        B1.visible=false;
        X=this.game.add.text(470,575,"X",lablel_fontStyle);//500,600,1600,600
        X1=this.game.add.text(1610,575,"X'",lablel_fontStyle);
        X.visible=false;
        X1.visible=false
        M=this.game.add.text(1030,390,"M",lablel_fontStyle);//500,600,1600,600
        N=this.game.add.text(1035,760,"N",lablel_fontStyle);
        M.visible=false;
        N.visible=false
        C=this.game.add.text(1000,420,"C",lablel_fontStyle);
        C.visible=false;
      //////////////////////////////////////////////////////////////////////////////  
      scales=this.game.add.sprite(400,500, 'scale');
      //scales.scale.setTo(1,.9);
      scales.anchor.set(.5);
      //scales.tint='0x000000';
      //scales.alpha=.5;
      scales.visible=false;
      collider=drawing_board_top;//this.game.add.sprite(1000,500, 'collider');
      this.game.physics.arcade.enable(collider);
      //collider.scale.setTo(1.3,2.5);
      collider.inputEnabled = true;
      collider.enableBody =true;
      //collider.alpha=0;//.5
      bgGroup.add(collider);
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      arrow=this.game.add.sprite(200,200, 'arrow');
      arrow.angle=90;
      arrow_y=200;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  


       handle1 = this.game.add.sprite(500, 600, 'pencil', 0);
       handle1.anchor.setTo(0.5,1);
       handle1.inputEnabled = true;
       //handle1.input.enableDrag(true);
       //handle1.scale.set(3);
       handle1.events.onInputDown.add(this.handleMove,this); 
       handle1.events.onInputUp.add(this.handleStop,this);
       handle1.visible=false;
       pencilGroup=this.game.add.group();
       pencilGroup.add(handle1);
       lineGroup=this.game.add.group();
       pencilGroup=this.game.add.group();
       
       pencilGroup.add(handle1);
       pencilGroup.add(scales);
       //handle2 = this.game.add.sprite(1400, 700, 'bullet', 0);
       //handle2.anchor.set(0.5);
       //handle2.scale.set(3);
       //handle2.alpha=.5;
       //handle2.events.onInputDown.add(this.handleMove,this); 
       //handle2.events.onInputUp.add(this.lineStop,this);
       //handle2.inputEnabled = true;
       //handle2.input.enableDrag(true);

     //line1 = new Phaser.Line(handle1.x, handle1.y, handle2.x, handle2.y);


      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);

        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/
//////////////////////////////////////////////////////////
        bmd1 = this.game.add.bitmapData(1920,1080);
        var color = 'black';

        bmd1.ctx.beginPath();
        bmd1.ctx.lineWidth = "1";
        bmd1.ctx.strokeStyle = color;
        bmd1.ctx.stroke();
        sprite = this.game.add.sprite(0, 0, bmd1);

    },
    drawLine:function(){ //this.drawVLine(550,480,1050,480);
      bmd1.clear();
      bmd1.ctx.beginPath();
      bmd1.ctx.beginPath();
      bmd1.ctx.moveTo(550, 480);
      bmd1.ctx.lineTo(this.game.input.activePointer.x , this.game.input.activePointer.y);
      bmd1.ctx.lineWidth = 2;
      bmd1.ctx.stroke();
      bmd1.ctx.closePath();
      bmd1.render();
      
      
      //bmd.refreshBuffer();
    },
    
    handleMove:function(){
      handleMove=true;
      if(linecount==1){
        arrow.x=1620;
        arrow.y=500;
        arrow_y=arrow.y
      }else if(linecount==2){
        arrow.x=1070;
        arrow.y=650;
        arrow_y=arrow.y
      }else if(linecount==3){
        arrow.x=1050;
        arrow.y+=150;
        arrow_y=arrow.y
      }else if(linecount==4){
        arrow.x=1550;
        arrow.y=650;
        arrow_y=arrow.y
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }else if(linecount==5){
        arrow.x=1550;
        arrow.y=630;
        arrow_y=arrow.y
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
        
      //pointer_currentx=this.game.input.activePointer.x;
    },

    handleStop:function(){
      handleMove=false;
      //pointer_currentx=this.game.input.activePointer.x;
    },
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
       voice.destroy(true);
       voice=this.game.add.audio("step_1",1);
       voice.play();
       dialog.text="Drag the white sheet of paper to the drawing board.";
       paper.events.onDragStart.add(function() {this.gameC(paper)}, this);
       paper.events.onDragStop.add(function() {this.gameC1(paper)}, this);
       this.game.physics.arcade.enable(paper);
       paper.inputEnabled = true;
       paper.input.useHandCursor = true;
       paper.input.enableDrag(true);
       arrow.visible=true;
      //////////////////////////////////////////
      //this.ShowReactions();
    },
    
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    arrow.visible=false;
    //console.log("drag");
     if(obj==Origin_1){
       //1050,600
       arrow.x=1060;
       arrow.y=500;
       arrow_y=arrow.y;
      arrow.visible=true;
     }else if(obj==focus_1){
       //1050,600
       arrow.x=860;
       arrow.y=500;
       arrow_y=arrow.y;
      arrow.visible=true;
     }else if(obj==focus_2){
      //1050,600
      arrow.x=660;
      arrow.y=500;
      arrow_y=arrow.y;
     arrow.visible=true;
    }else if(obj==Object_1){
       //1050,600
       arrow.x=560;
       arrow.y=400;
       arrow_y=arrow.y;
      arrow.visible=true;
     }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    //console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==paper){
      paper.visible=false;
      paper1.alpha=1;

      Nail1.events.onDragStart.add(function() {this.gameC(Nail1)}, this);
      Nail1.events.onDragStop.add(function() {this.gameC1(Nail1)}, this);
       this.game.physics.arcade.enable(Nail1);
       Nail1.inputEnabled = true;
       Nail1.input.useHandCursor = true;
       Nail1.input.enableDrag(true);
       arrow.x=Nail1.x+50;
       arrow.y=Nail1.y-110;
       arrow_y=arrow.y;
       arrow.visible=true;
       voice.destroy(true);
       voice=this.game.add.audio("step_2",1);
       voice.play();
       dialog.text="Fix the sheet with the drawing pins.";
    }else if(currentobj==Nail1){
      
      var xp=Nail1.x;
      var yp=Nail1.y;
      Nail1.visible=false;
      Nail1_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail1_1.scale.set(2);
      tween1=this.game.add.tween(Nail1_1).to( {x:385, y:190}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        Nail_top1.visible=true;
        Nail1_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail2_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail2_1.scale.set(2);
      tween2=this.game.add.tween(Nail2_1).to( {x:1695, y:190}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        Nail_top2.visible=true;
        Nail2_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail3_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail3_1.scale.set(2);
      tween3=this.game.add.tween(Nail3_1).to( {x:1695, y:990}, 600, Phaser.Easing.Out, true);
      tween3.onComplete.add(function () {
        Nail_top3.visible=true;
        Nail3_1.visible=false;
        //this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
      }.bind(this));
      Nail4_1=this.game.add.sprite(xp,yp, 'Nail');
      Nail4_1.scale.set(2);
      tween4=this.game.add.tween(Nail4_1).to( {x:385, y:990}, 600, Phaser.Easing.Out, true);
      tween4.onComplete.add(function () {
        Nail_top4.visible=true;
        Nail4_1.visible=false;
        //
      }.bind(this));
      Lens_1.events.onDragStart.add(function() {this.gameC(Lens_1)}, this);
      Lens_1.events.onDragStop.add(function() {this.gameC1(Lens_1)}, this);
       this.game.physics.arcade.enable(Lens_1);
       Lens_1.inputEnabled = true;
       Lens_1.input.useHandCursor = true;
       Lens_1.input.enableDrag(true);
       arrow.x=Lens_1.x+10;
       arrow.y=Lens_1.y-180;
       arrow_y=arrow.y;
       arrow.visible=true;
       voice.destroy(true);
       voice=this.game.add.audio("step_3",1);
       voice.play();
       dialog.text="Drag the convex lens to the centre of the sheet.";
    }else if(currentobj==Lens_1){
      //Lens_1.visible=false;
      Lens_1.x=paper1.x;
      Lens_1.y=paper1.y;
      Lens_1.scale.set(1.5);
      voice.destroy(true);
        voice=this.game.add.audio("step_4",1);
        voice.play();
        dialog.text="Draw a thin line XX' in the middle of the sheet.";
        X.visible=true;
        X1.visible=true;
      arrow.visible=false;  
      scales.x=1050;
      scales.y=1645;
      tween1=this.game.add.tween(scales).to( {y:650}, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
       handle1.visible=true;
       arrow.x=handle1.x+10;
       arrow.y=handle1.y-250;
       arrow_y=arrow.y;
       arrow.visible=true;
       collider=this.game.add.sprite(1000,530, 'A_Blue');
       collider.alpha=0;
       this.game.physics.arcade.enable(collider);
        collider.scale.setTo(1.2,2);
        collider.inputEnabled = true;
        collider.enableBody =true;
        bgGroup.add(collider);
      }.bind(this));
      scales.visible=true;
    }else if(currentobj==Origin_1){
      arrow.visible=false;
      Origin_1.x=1040;
      Origin_1.y=605;
      this.drawVLine(1050,590,1050,610);//1050,600
      scales.y=1645;
      linecount=2;
      M.visible=true;
      N.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step_6",1);
      voice.play();
      dialog.y=60;
      dialog.text="Draw a perpendicular line 'MN' at the pont 'O'.";
      scales.visible=true;
      scales.x=1000;
      
          
      ////////////////////////////////////////////
      tween1=this.game.add.tween(scales).to( {y:600}, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        handle1.angle=15;
        handle1.visible=true;
         handle1.x=1047;
        handle1.y=450;
        arrow.x=handle1.x+40;
        arrow.y=handle1.y-250;
        arrow_y=arrow.y;
        arrow.visible=true;
        scales.angle=90;
        
      }.bind(this));
      //////////////////////////////////////////
      
      
      ///////////////////////////////////////////////////
      
    }else if(currentobj==focus_1){
      focus_1.x=840;
      focus_1.y=605;
      focus_11.visible=true;
      this.drawVLine(850,590,850,610);//1050,600
      this.drawVLine(1250,590,1250,610);//1050,600
      
      focus_2.events.onDragStart.add(function() {this.gameC(focus_2)}, this);
      focus_2.events.onDragStop.add(function() {this.gameC1(focus_2)}, this);
      this.game.physics.arcade.enable(focus_2);
      focus_2.inputEnabled = true;
      focus_2.input.useHandCursor = true;
      focus_2.input.enableDrag(true);
      arrow.x=focus_2.x+30;
      arrow.y=focus_2.y-100;
      arrow_y=arrow.y;
      arrow.visible=true;
      collider.x=600;
      voice.destroy(true);
        voice=this.game.add.audio("step7_2",1);
        voice.play();
        dialog.text="Mark the points 2F\u2081 and 2F\u2082 on the line at double the distances \nOF\u2081 and OF\u2082 respectively.";
        
        //bmd.destroy(true);
    }else if(currentobj==focus_2){
      focus_2.x=640;
      focus_2.y=605;
      C1.visible=true;
      C2.visible=true;
      focus_21.visible=true;
      this.drawVLine(650,590,650,610);//1050,600
      this.drawVLine(1450,590,1450,610);//1050,600
      dialog.y=60;
      voice.destroy(true);
      voice=this.game.add.audio("step8_2",1);
      voice.play();
      dialog.text="Place the object beyond 2F\u2081";
      Object_1.events.onDragStart.add(function() {this.gameC(Object_1)}, this);
      Object_1.events.onDragStop.add(function() {this.gameC1(Object_1)}, this);
      this.game.physics.arcade.enable(Object_1);
      Object_1.inputEnabled = true;
      Object_1.input.useHandCursor = true;
      Object_1.input.enableDrag(true);
      arrow.x=Object_1.x+20;
      arrow.y=Object_1.y-50;
      arrow_y=arrow.y;
      arrow.visible=true;
      collider.x=500;
        
    }else if(currentobj==Object_1){
      arrow.visible=false;
      Object_1.x=550;
      Object_1.y=423;
      scales.y=1645;
      scales.x=800;
      scales.angle=0;
      A.visible=true;
      B.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step9_2",1);
      voice.play();
      dialog.text="Draw a line from 'A' to 'C' parallel to the principle axis.";
      scales.visible=true;
      ////////////////////////////////////////////
      tween1=this.game.add.tween(scales).to( {y:532}, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
       handle1.visible=true;
        handle1.x=550;
        handle1.y=483;
        linecount=3;
       arrow.x=handle1.x+10;
       arrow.y=handle1.y-250;
       arrow_y=arrow.y;
       arrow.visible=true;
        
      }.bind(this));
      //////////////////////////////////////////
 }
    
  },
  

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
      
    }/**/
    //line1.fromSprite(handle1, handle2, false);
    //this.drawLine();   
    if(handleMove){
      if(linecount==1){
        this.Action_handle1();
      }else if(linecount==2){
        this.Action_handle2();
      }else if(linecount==3){
        this.Action_handle3();
      }else if(linecount==4){
        this.Action_handle4();
      }else if(linecount==5){
        this.Action_handle5();
      }
      
     }
  },
  Action_handle4:function(){
    if(this.game.input.activePointer.x>=550 && this.game.input.activePointer.x<=1550){
      if(currentx<this.game.input.activePointer.x||currenty<this.game.input.activePointer.y){
        handle1.x+=5;//this.game.input.activePointer.x;
        handle1.y+=3;//this.game.input.activePointer.y;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
      if(handle1.x>=1450 && handle1.y>=650){
        //scales.visible=false;
        arrow.visible=false;
        handleMove=false;
        handle1.visible=false;
        scales.y=1645;
        scales.angle=0;
        this.drawVLine(1050,480,1550,780);
        dir1=this.game.add.sprite(1180,560,'dir_arrow');
          dir1.anchor.set(.5);
          dir1.angle=31;
          lineGroup.add(dir1);
          voice.destroy(true);
          voice=this.game.add.audio("step10_2",1);
          voice.play();
          dialog.text="Draw another line through the optical centre 'O'.";
  
            ////////////////////////////////////////////
            tween1=this.game.add.tween(scales).to( {x:1050,y:653}, 1000, Phaser.Easing.Out, true);
            tween1.onComplete.add(function () {
              handle1.x=550;
              handle1.y=483;
              linecount=5;
              arrow.x=handle1.x+10;
              arrow.y=handle1.y-250;
              arrow_y=arrow.y;
              arrow.visible=true;
              scales.angle=13.5;
              //scales.visible=true;
              handle1.visible=true;
            }.bind(this));
            //////////////////////////////////////////  


        
      }
    }
    
  },
  Action_handle5:function(){
    //console.log(arrow.y);
    //this.drawLine();
    if(this.game.input.activePointer.x>=550 && this.game.input.activePointer.x<=1550){
      //console.log(handle1.x+"//"+handle1.y);
      if(currentx<this.game.input.activePointer.x||currenty<this.game.input.activePointer.y){
        handle1.x+=8;//this.game.input.activePointer.x;
        handle1.y+=2;//this.game.input.activePointer.y;
        currentx=this.game.input.activePointer.x;
        currenty=this.game.input.activePointer.y;
      }
      
      if(handle1.x>=1400 && handle1.y>=600){
        scales.visible=false;
        arrow.visible=false;
        handleMove=false;
        handle1.visible=false;
        this.drawVLine(550,480,1550,720);
        scales.visible=false;
        dir1=this.game.add.sprite(700,515,'dir_arrow');
        dir1.anchor.set(.5); 
        dir1.angle=14;
        lineGroup.add(dir1);
        dir2=this.game.add.sprite(1180,630,'dir_arrow');
        dir2.anchor.set(.5); 
        dir2.angle=14;
        lineGroup.add(dir2);
        Object_2.visible=true;
        A1.visible=true;
        B1.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step11_2",1);
        voice.play();
        dialog_box.scale.setTo(.8,.9);
        dialog.y=35;
        dialog.text="After refraction through the convex lens, the rays meet between \nF\u2082 and 2F\u2082. A diminished, real and inverted image of the object is \nformed in between F\u2082 and 2F\u2082.";
      //Click on the next button to see the image formation, if the object is placed at 2F1.
      this.game.time.events.add(Phaser.Timer.SECOND*11,this.nextScene, this); 
      }
    }
  },
  nextScene:function(){
    play.visible=true;
    dialog.y=50;
    dialog_box.scale.setTo(.8,.8);
    voice.destroy(true);
        voice=this.game.add.audio("step12_2",1);
        voice.play();
    dialog.text="Click on the next button to see the image formation, when the \nobject is placed at 2F\u2081.";
  },
  Action_handle3:function(){
   // console.log(this.game.input.activePointer.x);
    if(this.game.input.activePointer.x>=550 && this.game.input.activePointer.x<=1050){
        handle1.y=483;
        handle1.x=this.game.input.activePointer.x;
        //console.log(handle1.x)
        if(handle1.x>=1000){
          
          arrow.visible=false;
          handleMove=false;
          //scales.visible=false;
          //scales.y=1645;
          handle1.visible=false;
          this.drawVLine(550,480,1050,480);
          dir1=this.game.add.sprite(700,480,'dir_arrow');
          dir1.anchor.set(.5);
          lineGroup.add(dir1);
          voice.destroy(true);
      voice=this.game.add.audio("step10_1",1);
     voice.play();
      dialog.text="Draw refracted rays through the focus F\u2082.";
        ////////////////////////////////////////////
        tween1=this.game.add.tween(scales).to( {x:1245}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          scales.y=655;
          handle1.x=1050;
          handle1.y=483;
          linecount=4;
          arrow.x=handle1.x+10;
          arrow.y=handle1.y-250;
          arrow_y=arrow.y;
          arrow.visible=true;
          scales.angle=31;
          
          handle1.visible=true;
        }.bind(this));
        //////////////////////////////////////////
        

        }
      }else{
        if(handle1.x<550){
          handle1.x=550;
        }else if(handle1.x>=1050){
          handle1.x=1050;
        }
      }
  },
  Action_handle2:function(){
    
    if(this.game.input.activePointer.y>=200 && this.game.input.activePointer.y<=950){
      handle1.x=1050;
      handle1.y=this.game.input.activePointer.y;
      if(handle1.y>=700){
        arrow.visible=false;
        handleMove=false;
        handle1.visible=false;
        scales.visible=false;
        handle1.angle=0;
        bmd=this.game.add.bitmapData(1920,1080);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 1;
        bmd.ctx.strokeStyle = 'black';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(1050,450);
        bmd.ctx.lineTo(1050,750);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(0,0,bmd);
        lineGroup.add(l1);
        //////////////////////////////////////////////////
        focus_1.events.onDragStart.add(function() {this.gameC(focus_1)}, this);
        focus_1.events.onDragStop.add(function() {this.gameC1(focus_1)}, this);
        this.game.physics.arcade.enable(focus_1);
        focus_1.inputEnabled = true;
        focus_1.input.useHandCursor = true;
        focus_1.input.enableDrag(true);
        arrow.x=focus_1.x+30;
        arrow.y=focus_1.y-100;
        arrow_y=arrow.y;
        arrow.visible=true;
        collider.x=800;
        dialog.y=40;
        voice.destroy(true);
          voice=this.game.add.audio("step7_1",1);
          voice.play();//Mark the principal foci F1 and F2 on either side of the lens, such that OF1 = OF2
          dialog.text="Mark the principal foci F\u2081 and F\u2082 on either side of the lens, \nsuch that, OF\u2081 = OF\u2082.";
          //Mark the focus points F\u2081 aand F\u2082 on either side of the lens.
        }
    }
  },
  Action_handle1:function(){

    if(this.game.input.activePointer.x>=500 && this.game.input.activePointer.x<=1600){
      handle1.y=605;
        handle1.x=this.game.input.activePointer.x;
        //console.log(handle1.x)
        if(handle1.x>=1550){
          arrow.visible=false;
          handleMove=false;
          handle1.visible=false;
          scales.visible=false;
          this.drawVLine(500,600,1600,600);
          // dir1=this.game.add.sprite(700,600,'dir_arrow');
          // dir1.anchor.set(.5);
          // lineGroup.add(dir1);
          // dir1_1=this.game.add.sprite(1180,600,'dir_arrow');
          // dir1_1.anchor.set(.5);
          // lineGroup.add(dir1_1);
          // this.drawVLine(1050,590,1050,610);//1050,600
          // this.drawVLine(850,590,850,610);//1050,600
          // this.drawVLine(1250,590,1250,610);//1050,600
          // this.drawVLine(650,590,650,610);//1050,600
          // this.drawVLine(1450,590,1450,610);//1050,600
          Origin_1.events.onDragStart.add(function() {this.gameC(Origin_1)}, this);
          Origin_1.events.onDragStop.add(function() {this.gameC1(Origin_1)}, this);
          this.game.physics.arcade.enable(Origin_1);
          Origin_1.inputEnabled = true;
          Origin_1.input.useHandCursor = true;
          Origin_1.input.enableDrag(true);
          arrow.x=Origin_1.x+30;
          arrow.y=Origin_1.y-100;
          arrow_y=arrow.y;
          arrow.visible=true;
          voice.destroy(true);
        voice=this.game.add.audio("step_5",1);
        voice.play();
        dialog.y=40;
        dialog.text="Mark the point 'O' at the centre of the lens. This point is considered \nas the optical centre of the lens.";
        }
      }else{
        if(handle1.x<500){
          handle1.x=500;
        }else if(handle1.x>=1600){
          handle1.x=1600;
        }
      }
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(2,0x000000,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
  },
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==paper || currentobj==Nail1 || currentobj==Lens_1){
        arrow.visible=true;
        
      }else if(currentobj==Origin_1){
        arrow.x=Origin_1.x+30;
          arrow.y=Origin_1.y-100;
          arrow_y=arrow.y;
        arrow.visible=true;
      }else if(currentobj==focus_1){
        arrow.x=focus_1.x+30;
          arrow.y=focus_1.y-100;
          arrow_y=arrow.y;
        arrow.visible=true;
      }else if(currentobj==focus_2){
        arrow.x=focus_2.x+20;
          arrow.y=focus_2.y-100;
          arrow_y=arrow.y;
        arrow.visible=true;
      }else if(currentobj==Object_1){
        arrow.x=Object_1.x+30;
          arrow.y=Object_1.y-50;
          arrow_y=arrow.y;
        arrow.visible=true;
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      voice.destroy();
      
      this.state.start("Experiment_3", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
