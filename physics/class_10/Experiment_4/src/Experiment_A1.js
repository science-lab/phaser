var experiment_a1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var HCl_step;
var colorChangingAnimstart;
var TostartTesttubeB;
var animationCounter;

var isRheostatMoving;
var rHead_minimuval;
var ammeter_needle_initial_angle;
var voltmeter_needle_initial_angle;
var resistor_resistance;

// var voltage_val1;
// var voltage_val2;
// var voltage_val3;

var voltage_val_final1;
var voltage_val_final2;
var voltage_val_final3;

// var current_val1;
// var current_val2;
// var current_val3;

var current_val_final1;
var current_val_final2;
var current_val_final3;


var resistance_val1;
var resistance_val2;
var resistance_val3;

var resistance_val_final1;
var resistance_val_final2;
var resistance_val_final3;

var isValue1_taken;
var isValue2_taken;
var isValue3_taken;
var isValue4_taken;
var isValue5_taken;
var isValue6_taken;

var voltmeter_reading;
var ammeter_reading;
var rheostat_resistance;
var rheostat_head_initial_val;
var pointer_currentx;
var pointer_newx;
var power_State;
var rheostat_Valus;
var ammeter_Values;
var voltmeter_Values;
var ammeter_angles;
var voltmeter_angle;
var Needle_Position_am;
var Needle_Position_vm;
var am_Val;
var vm_Val;
var rs_Val;
}

experiment_a1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
//  voice1=this.game.add.audio("A_exp_procedure1",1);
//  voice2=this.game.add.audio("A_exp_procedure2",1);
//  voice3=this.game.add.audio("A_exp_procedure3",1);
//  voice4=this.game.add.audio("A_exp_procedure4",1);
//  voice5=this.game.add.audio("A_exp_procedure5",1);
//  voice6=this.game.add.audio("A_exp_procedure6",1);
//  voice7=this.game.add.audio("A_exp_procedure7",1);
//  voice8=this.game.add.audio("A_exp_procedure8",1);
//  voice9=this.game.add.audio("A_exp_procedure9",1);
//  voice10=this.game.add.audio("A_exp_procedure10",1);
//  voice11=this.game.add.audio("A_exp_procedure11",1);
//  voice12=this.game.add.audio("A_exp_procedure12",1);
//  voice13=this.game.add.audio("A_exp_procedure13",1);
//  voice14=this.game.add.audio("A_exp_procedure14",1);
 //voice1=this.game.add.audio("obj",1);
 
 this.arrangeScene();
 isRheostatMoving=false;
 rHead_minimuval=1760;
 ammeter_needle_initial_angle=285;
 voltmeter_needle_initial_angle=285;
 //resistor_resistance=6;
 rheostat_resistance=10;
 rheostat_head_initial_val=1460;
 
 
 pointer_currentx=0; 
 pointer_newx=0;

//  pointer_currentx=1460; 
//  pointer_newx=1730;


 isValue1_taken=false;
 isValue2_taken=false;
 isValue3_taken=false;
 isValue4_taken=false;
 isValue5_taken=false;
 isValue6_taken=false;

 am_Val=1.5;
 vm_Val=4.5;
 rs_Val=0;
 
 animationCounter=0;
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, -50,'bg');
 bg.scale.setTo(.241,.253);

  var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.lineStyle(10,0xff704d,1);
  maskBg1.beginFill(0xffffcc);
  maskBg1.drawRect(20, 40, 300, 1010);
  maskBg1.alpha=1;
  // rheostat_Valus=[1,2,3,4,5,6,7,8,9,10];
  // ammeter_angles=[165,145,128,116,105,98,90,85,80,75];
  // ammeter_Values=[1,.86,.75,.67,.6,.55,.5,.46,.43,.4];
  // voltmeter_Values=[5,4.3,3.75,3.35,3,2.75,2.5,2.3,2.15,2];

  rheostat_Valus=[1,2,3,4,5,6,7,8,9,10];
  ammeter_angles=[53,45,40,37,34,32,30,28,27,26];
  ammeter_Values=[1.5,1.2,1,.85,.75,.66,.6,.54,.5,.46];
  voltmeter_angles=[151,123,105,92,83,74,69,64,60,56]
  voltmeter_Values=[4.5,3.6,3,2.55,2.25,1.98,1.8,1.62,1.5,1.38];


  power_State="switch_off";
  ratio=5.5;
 HCl_step=0;
 colorChangingAnimstart=false;
 delay=0;
 TostartTesttubeB=false;
 Needle_Position_am=-1;
 Needle_Position_vm=-1;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  dialog_box=this.game.add.sprite(340,20, 'dialogue_box1');
      dialog_box.scale.setTo(.64,.8);
      dialog_text="Lets perform the experiment";
      
      //procedure_voice1.play();
      dialog=this.game.add.text(360,60,dialog_text,fontStyle);
      

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

voltmeter_value1_text="V1 = ";
voltmeter_value1=this.game.add.text(50,150,voltmeter_value1_text,fontStyle3);
ammeter_value1_text="I1 = ";
ammeter_value1=this.game.add.text(60,80,ammeter_value1_text,fontStyle3);

voltmeter_value1.visible=false;
ammeter_value1.visible=false;

// voltmeter_value1.visible=true;
// ammeter_value1.visible=true;

voltmeter_value2_text="V2 = ";
voltmeter_value2=this.game.add.text(50,300,voltmeter_value2_text,fontStyle3);
ammeter_value2_text="I2 = ";
ammeter_value2=this.game.add.text(60,230,ammeter_value2_text,fontStyle3);

voltmeter_value2.visible=false;
ammeter_value2.visible=false;

// voltmeter_value2.visible=true;
// ammeter_value2.visible=true;

voltmeter_value3_text="V3 = ";
voltmeter_value3=this.game.add.text(50,450,voltmeter_value3_text,fontStyle3);
ammeter_value3_text="I3 = ";
ammeter_value3=this.game.add.text(60,380,ammeter_value3_text,fontStyle3);

voltmeter_value3.visible=false;
ammeter_value3.visible=false;

// voltmeter_value3.visible=true;
// ammeter_value3.visible=true;

voltmeter_value4_text="V4 = ";
voltmeter_value4=this.game.add.text(50,600,voltmeter_value4_text,fontStyle3);
ammeter_value4_text="I4 = ";
ammeter_value4=this.game.add.text(60,530,ammeter_value4_text,fontStyle3);

voltmeter_value4.visible=false;
ammeter_value4.visible=false;

// voltmeter_value4.visible=true;
// ammeter_value4.visible=true;

voltmeter_value5_text="V5 = ";
voltmeter_value5=this.game.add.text(50,750,voltmeter_value5_text,fontStyle3);
ammeter_value5_text="I5 = ";
ammeter_value5=this.game.add.text(60,680,ammeter_value5_text,fontStyle3);

voltmeter_value5.visible=false;
ammeter_value5.visible=false;

// voltmeter_value5.visible=true;
// ammeter_value5.visible=true;

voltmeter_value6_text="V6 = ";
voltmeter_value6=this.game.add.text(50,900,voltmeter_value6_text,fontStyle3);
ammeter_value6_text="I6 = ";
ammeter_value6=this.game.add.text(60,830,ammeter_value6_text,fontStyle3);

voltmeter_value6.visible=false;
ammeter_value6.visible=false;

// voltmeter_value4.visible=true;
// ammeter_value4.visible=true;



//sample_button_text="Take value";
button_sample=this.game.add.sprite(70,940,'take_reading_btn');
button_sample.inputEnabled = true;
//play.input.priorityID = 3;
button_sample.input.useHandCursor = true;
button_sample.events.onInputDown.add(this.takeValue, this);
button_sample.visible=false;
button_sample.scale.setTo(2,2);

////////////////////////////////////////////////////////////////////

 },

 takeValue:function()
 {

  // if(voltage_val1==vm_Val&&current_val1==am_Val||voltage_val2==vm_Val&&current_val2==am_Val||voltage_val3==vm_Val&&current_val3==am_Val)
  // {

  //   dialog.text="Please take different values";
  // }
  arrow.visible=false;
  if(isValue1_taken==false)
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure21",1);
    voice.play();
    voltage_val1=vm_Val;
    current_val1=am_Val;
    rhst_val1=rs_Val;
    console.log(rhst_val1+"rhst_val1");
    //console.log("voltage1="+voltage_val1+"current1="+current_val1);
    //voltmeter_value1.text="V1 = "+voltage_val1+"V";
    voltmeter_val1=this.game.add.text(150,150,voltage_val1+" V",fontStyle4);
    //ammeter_value1.text="I1 = "+current_val1+"A";
    ammeter_val1=this.game.add.text(150,80,current_val1+" A",fontStyle4);
    voltmeter_value2.visible=true;
    ammeter_value2.visible=true;
    dialog.text="Take the second reading";
     isValue1_taken=true;

  }
  else if(isValue2_taken==false)
  {
    if(vm_Val==voltage_val1&&am_Val==current_val1)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure22",1);
      voice.play();
      voltmeter_value3.visible=true;
    ammeter_value3.visible=true;
        voltage_val2=vm_Val;
        current_val2=am_Val;
        rhst_val2=rs_Val;
        console.log(rhst_val2+"rhst_val2");
        //console.log("voltage2="+voltage_val2+"current2="+current_val2);
        //voltmeter_value2.text="V2 = "+voltage_val2+"V";
        voltmeter_val2=this.game.add.text(150,300,voltage_val2+" V",fontStyle4);
        //ammeter_value2.text="I2 = "+current_val2+"A";
        ammeter_val2=this.game.add.text(150,230,current_val2+" A",fontStyle4);
        dialog.text="Take the third reading";

        isValue2_taken=true;

    }
    
  }

  else if(isValue3_taken==false)
  {
    if(vm_Val==voltage_val2&&am_Val==current_val2||vm_Val==voltage_val1&&am_Val==current_val1)
    {
      
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure23",1);
      voice.play();
      
        voltmeter_value4.visible=true;
        ammeter_value4.visible=true;
        voltage_val3=vm_Val;
        current_val3=am_Val;
        rhst_val3=rs_Val;
        console.log(rhst_val3+"rhst_val3");
        //console.log("voltage2="+voltage_val3+"current2="+current_val3);
        //voltmeter_value3.text="V3 = "+voltage_val3+"V";
        voltmeter_value3=this.game.add.text(150,450,+voltage_val3+" V",fontStyle4);
        //ammeter_value3.text="I3 = "+current_val3+"A";
        ammeter_value3=this.game.add.text(150,380,current_val3+" A",fontStyle4);
        dialog.text="Take the fourth reading";

        isValue3_taken=true;

    }
    
  }

  
  else if(isValue4_taken==false)
  {
    if(vm_Val==voltage_val3&&am_Val==current_val3||vm_Val==voltage_val2&&am_Val==current_val2||vm_Val==voltage_val1&&am_Val==current_val1)
    {
      
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure24",1);
      voice.play();
      
        voltmeter_value5.visible=true;
        ammeter_value5.visible=true;
        voltage_val4=vm_Val;
        current_val4=am_Val;
        rhst_val4=rs_Val;
        console.log(rhst_val4+"rhst_val4");
        //console.log("voltage2="+voltage_val4+"current2="+current_val4);
        //voltmeter_value4.text="V4 = "+voltage_val4+"V";
        voltmeter_value4=this.game.add.text(150,600,voltage_val4+" V",fontStyle4);
        //ammeter_value4.text="I4 = "+current_val4+"A";
        ammeter_value4=this.game.add.text(150,530,current_val4+" A",fontStyle4);
        dialog.text="Take the fifth reading";

        isValue4_taken=true;

    }
    
  }

  else if(isValue5_taken==false)
  {
    if(vm_Val==voltage_val4&&am_Val==current_val4||vm_Val==voltage_val3&&am_Val==current_val3||vm_Val==voltage_val2&&am_Val==current_val2||vm_Val==voltage_val1&&am_Val==current_val1)
    {
      
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure15",1);
      voice.play();
      
        voltmeter_value6.visible=true;
        ammeter_value6.visible=true;
        voltage_val5=vm_Val;
        current_val5=am_Val;
        rhst_val5=rs_Val;
        console.log(rhst_val5+"rhst_val5");
        //console.log("voltage2="+voltage_val5+"current2="+current_val5);
        //voltmeter_value5.text="V5 = "+voltage_val5+"V";
        voltmeter_value5=this.game.add.text(150,750,voltage_val5+" V",fontStyle4);
       // ammeter_value5.text="I5 = "+current_val5+"A";
       ammeter_value5=this.game.add.text(150,680,current_val5+" A",fontStyle4);
        dialog.text="Take the sixth reading";

        isValue5_taken=true;

    }
    
  }
  else if(isValue6_taken==false)
  {
    if(vm_Val==voltage_val5&&am_Val==current_val5||vm_Val==voltage_val4&&am_Val==current_val4||vm_Val==voltage_val3&&am_Val==current_val3||vm_Val==voltage_val2&&am_Val==current_val2||vm_Val==voltage_val1&&am_Val==current_val1)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog.text="Please take different values";
    }
    else
    {
      voice.destroy();
        voice=this.game.add.audio("A_exp_procedure16",1);
        voice.play();
         voltage_val6=vm_Val;
         current_val6=am_Val;
         rhst_val6=rs_Val;
         console.log(rhst_val6+"rhst_val6");
         //console.log("voltage4="+voltage_val6+"current4="+current_val6);
         //voltmeter_value6.text="V6 = "+voltage_val6+"V";
         voltmeter_value6=this.game.add.text(150,900,voltage_val6+" V",fontStyle4);
         //ammeter_value6.text="I6 = "+current_val6+"A";
         ammeter_value6=this.game.add.text(150,830,current_val6+" A",fontStyle4);
         isValue6_taken=true;

         dialog.text="Click on the next button to see the observation";
         button_sample.visible=false;
         play.visible=true;
         
    }
    
  }




 },

arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
  // circuit= this.game.add.sprite(320,0,'circuit');
  // circuit.scale.setTo(.4,.5);

//////////colliders//////////
 collider_battery = this.game.add.sprite(500,310,'collider');
 collider_battery.scale.setTo(8,3);
 collider_battery.alpha=1;
 collider_battery.visible=false;
 this.game.physics.arcade.enable(collider_battery);//For colling purpose
 collider_battery.inputEnabled = true;
 collider_battery.enableBody =true;

 collider_rheostat = this.game.add.sprite(1410,230,'collider');
 collider_rheostat.scale.setTo(8,3);
 collider_rheostat.alpha=1;
 collider_rheostat.visible=false;
 this.game.physics.arcade.enable(collider_rheostat);//For colling purpose
 collider_rheostat.inputEnabled = true;
 collider_rheostat.enableBody =true;

 collider_key = this.game.add.sprite(1560,600,'collider');
 collider_key.scale.setTo(5,3);
 collider_key.alpha=1;
 collider_key.visible=false;
 this.game.physics.arcade.enable(collider_key);//For colling purpose
 collider_key.inputEnabled = true;
 collider_key.enableBody =true;

 collider_ammeter = this.game.add.sprite(955,520,'collider');
 collider_ammeter.scale.setTo(8,5);
 collider_ammeter.alpha=1;
 collider_ammeter.visible=false;
 this.game.physics.arcade.enable(collider_ammeter);//For colling purpose
 collider_ammeter.inputEnabled = true;
 collider_ammeter.enableBody =true;

 collider_resistor = this.game.add.sprite(590,550,'collider');
 collider_resistor.scale.setTo(3,3);
 collider_resistor.alpha=1;
 collider_resistor.visible=false;
 this.game.physics.arcade.enable(collider_resistor);//For colling purpose
 collider_resistor.inputEnabled = true;
 collider_resistor.enableBody =true;

 collider_voltmeter = this.game.add.sprite(460,800,'collider');
 collider_voltmeter.scale.setTo(8,5);
 collider_voltmeter.alpha=1;
 collider_voltmeter.visible=false;
 this.game.physics.arcade.enable(collider_voltmeter);//For colling purpose
 collider_voltmeter.inputEnabled = true;
 collider_voltmeter.enableBody =true;

 
 ////////////////////////////////////////equipments//////////////////////////////////
 
 battery = this.game.add.sprite(450,324,'battery');
 battery.scale.setTo(.2,.25);
 battery.alpha=0;

 voltmeter = this.game.add.sprite(500,705,'voltmeter');
 voltmeter.scale.setTo(.25,.25);
 voltmeter.alpha=0;

 voltmeter_needle=this.game.add.sprite(692,871,'needle');
 voltmeter_needle.scale.setTo(.24,.27);
 voltmeter_needle.alpha=0;
 voltmeter_needle.anchor.setTo(.9,.5)
 voltmeter_needle.angle=15;//-75;

 ammeter = this.game.add.sprite(985,415,'ammeter');
 ammeter.scale.setTo(.25,.25);
 ammeter.alpha=0;

//  ammeter_needle=this.game.add.sprite(1176,582,'needle');
//  ammeter_needle.scale.setTo(.24,.27);
//  ammeter_needle.alpha=1;
//  ammeter_needle.anchor.setTo(.5,.9)
//  ammeter_needle.angle=285;
 ammeter_needle=this.game.add.sprite(1176,581,'needle');
 ammeter_needle.scale.setTo(.24,.27);
 ammeter_needle.alpha=0;
 ammeter_needle.anchor.setTo(.9,.5);
 ammeter_needle.angle=15;//15

 keySet = this.game.add.sprite(1559,600,'keySet');
 keySet.scale.setTo(.2,.25);
 keySet.alpha=0;

//  key = this.game.add.sprite(1670,575,'key');//key inserted diamension
//  key.scale.setTo(.2,.25);

 key = this.game.add.sprite(1670,750,'key');
 key.angle=45;
 key.scale.setTo(.2,.25);
 key.alpha=0;
 //key.visible=false;
 

 rheostat = this.game.add.sprite(1360,225,'rheostat');
 rheostat.scale.setTo(.18,.25);
 rheostat.alpha=0;

//  var line1=this.game.add.graphics(0, 0);
//  //line1.lineStyle(7,0xe5e5e5,1);
//  line1.moveTo(710, 520);
//  line1.lineTo(890, 520);
//  this.drawVLine(1460,380,"1");//0
//  this.drawVLine(1490,380,"2");//35
//  this.drawVLine(1520,380,"3");//70
//  this.drawVLine(1550,380,"4");//105
//  this.drawVLine(1580,380,"5");//140
//  this.drawVLine(1610,380,"6");//175
//  this.drawVLine(1640,380,"7");//210
//  this.drawVLine(1670,380,"8");//245
//  this.drawVLine(1700,380,"9");//280
//  this.drawVLine(1730,380,"10");//315
 
 

//1730,295,
 rheostat_head = this.game.add.sprite(1460,295,'rheostat_top');
 rheostat_head.scale.setTo(.20,.25);
 rheostat_head.anchor.setTo(.5,1);
 rheostat_head.alpha=0;
//  rheostat_head.inputEnabled=true;
//  rheostat_head.input.useHandCursor=true;
//  rheostat_head.events.onInputDown.add(this.rheostat_head_move,this);
//  rheostat_head.events.onInputUp.add(this.rheostat_head_stop,this);

 resistor = this.game.add.sprite(574,595,'nichrome_wire');
 resistor.scale.setTo(.20,.25);
 resistor.alpha=0;

 wire1= this.game.add.sprite(368,367,'wire1');
 wire1.scale.setTo(.25,.25);
 wire1.visible=false;
//  wire1.visible=true;

 wire2 = this.game.add.sprite(445,640,'wire2');
 wire2.scale.setTo(.25,.26);
 wire2.visible=false;
//  wire2.visible=true;

 wire3 = this.game.add.sprite(835,640,'wire3');
 wire3.scale.setTo(.24,.26);
 wire3.visible=false;
//  wire3.visible=true;

 wire4 = this.game.add.sprite(741, 636,'wire4');
 wire4.scale.setTo(.21,.25);
 wire4.visible=false;
//  wire4.visible=true;

 wire5 = this.game.add.sprite(1320, 636,'wire5');
 wire5.scale.setTo(.164,.25);
 wire5.visible=false;
//  wire5.visible=true;

 wire6 = this.game.add.sprite(1360, 205,'wire6');
 wire6.scale.setTo(.20,.25);
 wire6.visible=false;
// wire6.visible=true;

 wire7 = this.game.add.sprite(932,368,'wire7');
 wire7.scale.setTo(.20,.25);
 //wire7.alpha=0;
 wire7.visible=false;
//  wire7.visible=true;
//////////////////////////////equipments to drag//////////////////////////////

 temp_battery = this.game.add.sprite(65,220,'battery');
 temp_battery.scale.setTo(.08,.15);
//  temp_battery.events.onDragStart.add(function() {this.onDragStart(temp_battery)}, this);
//  temp_battery.events.onDragStop.add(function() {this.onDragStop(temp_battery)}, this);
//  temp_battery.inputEnabled = true;
//  temp_battery.input.useHandCursor = true;
//  temp_battery.input.enableDrag(true);
//  this.game.physics.arcade.enable(temp_battery);
//  temp_battery.enableBody =true;
 //temp_battery.alpha=.5;
//  temp_battery.visible=false;
 temp_battery.visible=true;

 temp_rheostat = this.game.add.sprite(40,360,'rheostat_full');
 temp_rheostat.scale.setTo(.1,.1);

//temp_rheostat.alpha=.5;
//  temp_rheostat.visible=false;
temp_rheostat.visible=true;

temp_keySet = this.game.add.sprite(90,470,'keySet_full');
temp_keySet.scale.setTo(.15,.15);
//temp_keySet.alpha=.5;
//  temp_keySet.visible=false;
temp_keySet.visible=true;

temp_ammeter = this.game.add.sprite(100,610,'ammeter_full');
temp_ammeter.scale.setTo(.09,.09);
 //temp_ammeter.alpha=1;
  // temp_ammeter.visible=false;
 temp_ammeter.visible=true;

temp_resistor = this.game.add.sprite(110,800,'nichrome_wire');
temp_resistor.scale.setTo(.13,.13);
//temp_resistor.alpha=.5;
//  temp_resistor.visible=false;
temp_resistor.visible=true;

temp_voltmeter = this.game.add.sprite(100,900,'voltmeter_full');
temp_voltmeter.scale.setTo(.09,.09);
 //temp_voltmeter.alpha=1;
//  temp_voltmeter.visible=false;
 

 direction1=this.game.add.sprite(385,495,'direction');
 direction1.angle=90;
 direction1.visible=false;
 direction2=this.game.add.sprite(970,625,'direction');
 direction2.angle=0;
 direction2.visible=false;
 direction3=this.game.add.sprite(1460,625,'direction');
 direction3.angle=0;
 direction3.visible=false;
 direction4=this.game.add.sprite(1842,445,'direction');
 direction4.angle=270;
 direction4.visible=false;
 direction5=this.game.add.sprite(1150,384,'direction');
 direction5.angle=180;
 direction5.visible=false;
//  direction6=this.game.add.sprite(1100,1000,'direction');
//  direction6.angle=90;

//  direction7=this.game.add.sprite(1110,1026,'direction');
//  direction7.angle=270;

//  direction8=this.game.add.sprite(1180,1030,'direction');
//  direction8.angle=180;

 direction9=this.game.add.sprite(1190,1000,'direction');
 direction9.angle=0;

 headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
 direction_text=this.game.add.text(1250,985,"Direction of current",headfontStyle);
 

 current_direction_group=this.game.add.group();
 
//  current_direction_group.add(direction6);
//  current_direction_group.add(direction7);
//  current_direction_group.add(direction8);
 current_direction_group.add(direction9);
 current_direction_group.add(direction_text);

 current_direction_group.visible=false;



//  collider_B = this.game.add.sprite(515, 555,'collider');
//  collider_B.alpha=1;
//  this.game.physics.arcade.enable(collider_B);//For colling purpose
  
arrow=this.game.add.sprite(1475,450, 'arrow');
      arrow.angle=90;
      arrow_y=130;
      arrow.visible=false;

      // arrow.visible=true;
      // arrow.x=180;
      // arrow.y=820;
      // arrow_y=820;

      // circuit= this.game.add.sprite(320,0,'circuit');
      // circuit.scale.setTo(.4,.5);
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
},


loadScene:function()
{
  voice=this.game.add.audio("A_exp_procedure0",1);
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this);
},

startExperiment:function()
{
  voice.destroy();
  voice=this.game.add.audio("A_exp_procedure1",1);
  voice.play();
  dialog.text="Drag the battery-set and place it on the table.";
  temp_battery.events.onDragStart.add(function() {this.onDragStart(temp_battery)}, this);
  temp_battery.events.onDragStop.add(function() {this.onDragStop(temp_battery)}, this);
  temp_battery.inputEnabled = true;
  temp_battery.input.useHandCursor = true;
  temp_battery.input.enableDrag(true);
  this.game.physics.arcade.enable(temp_battery);
  temp_battery.enableBody =true;

},
rheostat_head_move:function()
{
  pointer_currentx=this.game.input.activePointer.x;
  isRheostatMoving=true;
  arrow.visible=false;
  voltmeter_value1.visible=true;
  ammeter_value1.visible=true;
  button_sample.visible=true;
  
  
  if(isValue1_taken==false)
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure25",1);
    voice.play();
    dialog.text="Take the reading by click on the button. Take upto six\nreadings";
    
    arrow.visible=true;
    arrow.x=180;
    arrow.y=820;
    arrow_y=820;
  }
  

},

rheostat_head_stop:function()
{
  isRheostatMoving=false;
  // console.log("voltmeter_meter"+voltmeter_needle.angle);
  // console.log("voltmeter_reading"+voltmeter_reading);
  // console.log("voltmeter_angle"+voltmeter_needle.angle);
  //console.log("inital_pos"+rHead_minimuval+"final_pos"+rheostat_head.x+"voltmeter_angle"+voltmeter_needle.angle);
  //console.log("rheostat_head_pos"+rheostat_head.x);
//rheostat_resistance=rheostat_head.x - rheostat_head_initial_val;
 //console.log("rheostat_resistance"+rheostat_resistance);
//console.log("rheostat_head_pos"+ rheostat_head.x);

 
},


drawVLine:function(xp,yp,val){
  var line1=this.game.add.graphics(0, 0);
      line1.lineStyle(2,0xFFFFFF,1);
      line1.moveTo(xp, yp);
      yp+=20;
      line1.lineTo(xp, yp);
      fontStyle2={ font: "20px Segoe UI", fill: "#ffffff", align: "center" };
  var text1 = this.game.add.text(0,0,val,fontStyle2);
      text1.x=xp-10;
      text1.y=yp+10;

},
onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==(temp_battery))
    {
       currentobj.xp=65;
       currentobj.yp=220;
       battery.alpha=.5;
    }

    if(currentobj==(temp_rheostat))
    {
     currentobj.xp=40;
     currentobj.yp=360;
     rheostat.alpha=.5;

    }

    if(currentobj==(temp_keySet))
    {
     currentobj.xp=90;
     currentobj.yp=470;
    keySet.alpha=.5;

    }
 if(currentobj==(temp_ammeter))
    {
     currentobj.xp=100;
     currentobj.yp=610;
    ammeter.alpha=.5;
    ammeter_needle.alpha=1;
    ammeter_needle.visible=true;

    }
if(currentobj==(temp_resistor))
    {
     currentobj.xp=110;
     currentobj.yp=800;
     resistor.alpha=.5;

    }
    if(currentobj==(temp_voltmeter))
    {
     currentobj.xp=100;
     currentobj.yp=900;
     voltmeter.alpha=.5;
     voltmeter_needle.alpha=1;
     voltmeter_needle.visible=true;

    }

    if(currentobj==(key))
    {
    
      key.angle=0;
     currentobj.xp=1670;
     currentobj.yp=750;
     arrow.visible=false;
     arrow.x=1705;
     arrow.y=500;
     arrow_y=500;
     arrow.visible=true;
     
   

    }
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==temp_battery)
    {
        temp_battery.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure2",1);
        voice.play();
        battery.alpha=1;
        battery.visible=true;
        dialog.text="Drag the rheostat to the table and connect with the\nbattery";
        temp_rheostat.events.onDragStart.add(function() {this.onDragStart(temp_rheostat)}, this);
        temp_rheostat.events.onDragStop.add(function() {this.onDragStop(temp_rheostat)}, this);
        temp_rheostat.inputEnabled = true;
        temp_rheostat.input.useHandCursor = true;
        temp_rheostat.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_rheostat);
        temp_rheostat.enableBody =true;
        collider_battery.visible=false;
        collider_battery.enableBody=false;
        temp_battery.inputEnabled =false;
        temp_battery.input.useHandCursor = false;
        temp_battery.input.enableDrag(false);
        //this.game.physics.arcade.enable(temp_battery);
        temp_battery.enableBody =false;
      }

      if(currentobj==temp_rheostat)
      {
        temp_rheostat.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure3",1);
        voice.play();

        rheostat.alpha=1;
        rheostat.visible=true;
        rheostat_head.alpha=1;
        rheostat_head.visible=true;
        dialog.text="Connect the key to the rheostat in series";
        wire7.visible=true;
        temp_keySet.events.onDragStart.add(function() {this.onDragStart(temp_keySet)}, this);
        temp_keySet.events.onDragStop.add(function() {this.onDragStop(temp_keySet)}, this);
        temp_keySet.inputEnabled = true;
        temp_keySet.input.useHandCursor = true;
        temp_keySet.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_keySet);
        temp_keySet.enableBody =true;
        collider_rheostat.visible=false;
        collider_rheostat.enableBody=false;
        var line1=this.game.add.graphics(0, 0);
        //line1.lineStyle(7,0xe5e5e5,1);
        line1.moveTo(710, 520);
        line1.lineTo(890, 520);
        this.drawVLine(1460,380,"1");//0
        this.drawVLine(1490,380,"2");//35
        this.drawVLine(1520,380,"3");//70
        this.drawVLine(1550,380,"4");//105
        this.drawVLine(1580,380,"5");//140
        this.drawVLine(1610,380,"6");//175
        this.drawVLine(1640,380,"7");//210
        this.drawVLine(1670,380,"8");//245
        this.drawVLine(1700,380,"9");//280
        this.drawVLine(1730,380,"10");//315
      }

      if(currentobj==temp_keySet)
      {
        temp_keySet.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure4",1);
        voice.play();
        keySet.alpha=1;
        key.alpha=1;
        key.visible=true;
        
        dialog.text="Connect the ammeter to the circuit in series";
        wire6.visible=true;
        temp_ammeter.events.onDragStart.add(function() {this.onDragStart(temp_ammeter)}, this);
        temp_ammeter.events.onDragStop.add(function() {this.onDragStop(temp_ammeter)}, this);
        temp_ammeter.inputEnabled = true;
        temp_ammeter.input.useHandCursor = true;
        temp_ammeter.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_ammeter);
        temp_ammeter.enableBody =true;
        collider_key.visible=false;
        collider_key.enableBody=false;
      }
           
      if(currentobj==temp_ammeter)
      {
        temp_ammeter.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure5",1);
        voice.play();
        ammeter.alpha=1;
        ammeter.visible=true;
        dialog.text="Connect the resistor to the circuit in series with ammeter";
        wire5.visible=true;
        //wire1.visible=true;
        temp_resistor.events.onDragStart.add(function() {this.onDragStart(temp_resistor)}, this);
        temp_resistor.events.onDragStop.add(function() {this.onDragStop(temp_resistor)}, this);
        temp_resistor.inputEnabled = true;
        temp_resistor.input.useHandCursor = true;
        temp_resistor.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_resistor);
        temp_resistor.enableBody =true;
        collider_ammeter.visible=false;
        collider_ammeter.enableBody=false;
      }
         if(currentobj==temp_resistor)
      {
        temp_resistor.visible=false;
        //temp_battery.destroy();
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure6",1);
        voice.play();
        resistor.alpha=1;
        resistor.visible=true;
        dialog.text="Connect the voltmeter parallel to the resistor";
        wire1.visible=true;
        wire4.visible=true;
        temp_voltmeter.events.onDragStart.add(function() {this.onDragStart(temp_voltmeter)}, this);
        temp_voltmeter.events.onDragStop.add(function() {this.onDragStop(temp_voltmeter)}, this);
        temp_voltmeter.inputEnabled = true;
        temp_voltmeter.input.useHandCursor = true;
        temp_voltmeter.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_voltmeter);
        temp_voltmeter.enableBody =true;
        collider_resistor.visible=false;
        collider_resistor.enableBody=false;
      }
       if(currentobj==temp_voltmeter)
      {
        temp_voltmeter.visible=false;
        //temp_battery.destroy();
        voltmeter.alpha=1;
        voltmeter.visible=true;
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure7",1);
        voice.play();
        dialog.text="Take the key and insert the key into the key socket";
        wire2.visible=true;
        wire3.visible=true;
       // rheostat_head.inputEnabled=true;
        key.events.onDragStart.add(function() {this.onDragStart(key)}, this);
        key.events.onDragStop.add(function() {this.onDragStop(key)}, this);
        key.inputEnabled = true;
        key.input.useHandCursor = true;
        key.input.enableDrag(true);
        this.game.physics.arcade.enable(key);
        temp_voltmeter.enableBody =true;
        collider_voltmeter.visible=false;
        collider_voltmeter.enableBody=false;
        collider_key.inputEnabled=true;
        collider_key.enableBody=true;
        collider_key.visible=false;
        arrow.visible=true;
        arrow.x=1690;
        arrow.y=650;
        arrow_y=650;
      }

      if(currentobj==key)
      {
        voice.destroy();
        voice=this.game.add.audio("A_exp_procedure8",1);
        voice.play();
        arrow.visible=false;
        key.x=1670;
        key.y=574;
        power_State="switched_on";
        dialog.y=40;
        //dialog.text="Change the rheostat value by moving the head, and take voltmeter and\nammeter reading three times by click on the 'take value' button";
        dialog.text="Change the rheostat value by moving the rheostat head,\nand observe the voltmeter and ammeter. ";
        arrow.visible=true;
        arrow.x=1475;
        arrow.y=125;
        arrow_y=125;
        rheostat_head.inputEnabled=true;
        rheostat_head.input.useHandCursor=true;
        rheostat_head.events.onInputDown.add(this.rheostat_head_move,this);
        rheostat_head.events.onInputUp.add(this.rheostat_head_stop,this);
        current_direction_group.visible=true;
      
      }

  },
  
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }

    if(power_State=="switched_on")
    {
        animationCounter++;
        if(animationCounter==1)
         {

          direction1.visible=true;
          }
          else if(animationCounter==20)
         {
          direction1.visible=false;
          direction2.visible=true;
         }
          else if(animationCounter==40)
           {
          direction2.visible=false;
          direction3.visible=true;
           }
          else if(animationCounter==60)
           {
          direction3.visible=false;
          direction4.visible=true;
          }
          else if(animationCounter==80)
           {
          direction4.visible=false;
          direction5.visible=true;
          
          }
          else if(animationCounter>=100)
          {
            direction5.visible=false;
            animationCounter=0;
          }
    }
    

    if(isRheostatMoving==true)
    {
      //pointer_currentx=this.game.input.activePointer.x;


      pointer_newx=this.game.input.activePointer.x;
      if(pointer_newx>=pointer_currentx+15){
        rheostat_head.x+=30;
        pointer_currentx=rheostat_head.x;
      }else if(pointer_newx<=pointer_currentx-15){
        rheostat_head.x-=30;
        pointer_currentx=rheostat_head.x;
      }

      // pointer_newx=this.game.input.activePointer.x;
      // if(pointer_newx>=pointer_currentx+15){
      //   rheostat_head.x+=30;
      //   pointer_currentx=rheostat_head.x;
      // }else if(pointer_newx<=pointer_currentx-15){
      //   rheostat_head.x-=30;
      //   pointer_currentx=rheostat_head.x;
      // }




      //rheostat_head.x=this.game.input.activePointer.x;
      if(rheostat_head.x<1460)
      {
        rheostat_head.x=1460;
      }
      if(rheostat_head.x>1730)
      {
        rheostat_head.x=1730;
      }
      
      
      /*console.log(rs+"//"+ratio);
       ammeter_needle.angle=ammeter_needle_initial_angle+(rheostat_head_initial_val - rheostat_head.x)/ratio;
       voltmeter_needle.angle=voltmeter_needle_initial_angle+(rheostat_head_initial_val - rheostat_head.x)/ratio;
       //ammeter_needle.angle+=ammeter_needle.angle/10;
       //rheostat_head_initial_val=rheostat_head.x;
      // voltmeter_needle.angle=ammeter_needle_initial_angle+((rHead_minimuval - rheostat_head.x)/2);
       //voltmeter_reading=(((rHead_minimuval - rheostat_head.x)/voltmeter_needle.angle)/-5);
      rheostat_resistance=Number((((rheostat_head.x - rheostat_head_initial_val)/30))+1).toFixed(2);*/
      

      //console.log("voltmeter_meter"+voltmeter_needle.angle);
      //console.log("ammeter_meter"+ammeter_needle.angle);
    }
    var rs=(rheostat_head.x-1460)/30;
      // console.log(rs+"////");
      //power_State="switched_on";
      if(power_State=="switched_on"){
        if(rs!=Needle_Position_am){
          var new_angle_am=ammeter_angles[rs];
          am_Val=ammeter_Values[rs];
          if(ammeter_needle.angle<new_angle_am){
            ammeter_needle.angle++;
         
            //voltmeter_needle.angle++;
            //console.log(ammeter_needle.angle+"//"+new_angle);
            if(ammeter_needle.angle>=new_angle_am){
              // am_Val=ammeter_Values[rs];
               //vm_Val=voltmeter_Values[rs]
              //console.log(am_Val+"//"+vm_Val);
              Needle_Position_am=rs;
              // console.log(rs+"////");
              rs_Val=rs;
            }
          }else if(ammeter_needle.angle>new_angle_am){
            ammeter_needle.angle--;
            //voltmeter_needle.angle--;
            //console.log(ammeter_needle.angle+"//"+new_angle);
            if(ammeter_needle.angle<=new_angle_am){
             // am_Val=ammeter_Values[rs];
               //console.log(rs+"////");
               //vm_Val=voltmeter_Values[rs]
              //console.log(am_Val+"//"+vm_Val);
              rs_Val=rs;
              Needle_Position_am=rs;
            }
          }
        }


        if(rs!=Needle_Position_vm){
          var new_angle_vm=voltmeter_angles[rs];
          vm_Val=voltmeter_Values[rs]
          if(voltmeter_needle.angle<new_angle_vm){
            //ammeter_needle.angle++;
            voltmeter_needle.angle++;
            //console.log(ammeter_needle.angle+"//"+new_angle);
            if(voltmeter_needle.angle>=new_angle_vm){
              // am_Val=ammeter_Values[rs];
              // vm_Val=voltmeter_Values[rs]
              //console.log(am_Val+"//"+vm_Val);
              Needle_Position_vm=rs;
             // rs_Val=rs;
            }
          }else if(voltmeter_needle.angle>new_angle_vm){
            //ammeter_needle.angle--;
            voltmeter_needle.angle--;
            //console.log(ammeter_needle.angle+"//"+new_angle);
            if(voltmeter_needle.angle<=new_angle_vm){
              // am_Val=ammeter_Values[rs];
               //vm_Val=voltmeter_Values[rs]
              //console.log(am_Val+"//"+vm_Val);
              Needle_Position_vm=rs;
            }
          }
        }

      }
  },
 
  detectCollision:function(){
   

     if(collider_battery.enableBody && currentobj!=null)
     {
         this.game.physics.arcade.overlap(currentobj, collider_battery,function() {this.match_Obj()},null,this);
        // collider=collider_battery;
     } 

     if(collider_rheostat.enableBody && currentobj!=null)
     {
      this.game.physics.arcade.overlap(currentobj, collider_rheostat,function() {this.match_Obj()},null,this);
      //collider=collider_battery;
     }

    if(collider_key.enableBody && currentobj!=null)
    {    
    this.game.physics.arcade.overlap(currentobj, collider_key,function() {this.match_Obj()},null,this);
    //collider=collider_battery;
    }
    if(collider_ammeter.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_ammeter,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }
    if(collider_resistor.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_resistor,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }
  if(collider_voltmeter.enableBody && currentobj!=null)
    {  
    this.game.physics.arcade.overlap(currentobj, collider_voltmeter,function() {this.match_Obj()},null,this);
  //collider=collider_battery;
    }
    // if(collider_B.enableBody && currentobj!=null){
    //     this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
    //     collider=collider_B;
    // }
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
       if(currentobj==(temp_battery))
       {
        temp_battery.visible=true;

        battery.alpha=0;
        //battery.visible=false; 
       }
       if(currentobj==(temp_rheostat))
       {
       temp_rheostat.visible=true;
       rheostat.alpha=0;
      // rheostat.visible=false;
       }
       if(currentobj==(temp_keySet))
       {
       temp_keySet.visible=true;
       keySet.alpha=0;
     //keySet.visible=false;
       }   
       if(currentobj==(temp_ammeter))
       {
       temp_ammeter.visible=true;
       ammeter.alpha=0;
       ammeter_needle.alpha=0;
     //keySet.visible=false;
       }
       if(currentobj==(temp_resistor))
       {
       temp_resistor.visible=true;
       resistor.alpha=0;
     //keySet.visible=false;
       }
      if(currentobj==(temp_voltmeter))
       {
       temp_voltmeter.visible=true;
       voltmeter.alpha=0;
       voltmeter_needle.alpha=0;
     //keySet.visible=false;
       }
       
    //     arrow.x=1500;
    //     arrow.y=450;
    //     arrow_y=450;
    //     arrow.visible=true;
        
    //   }

         currentobj=null;
      }
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
