var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  var graphCount;
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  var line1;
  var line2;
  
  var contentstyle;
  var count;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var sceneNo;
  var r1;
  var r2;
  var r3;
  var r4;
  var r5;
  var r6;
  var bullet1;
  var bullet2;
  var bullet3;
  var bullet4;
  var bullet5;
  var bullet6;
  var textObj;
  var textObj1;
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

sceneNo=1;
count=0;
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
 muted = false;
 voice=this.game.add.audio("A_observation",1);
 
 voice.play();
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/
  // ammeter_Values=[1,.86,.75,.67,.6,.55,.5,.46,.43,.4];
  // voltmeter_Values=[5,4.3,3.75,3.35,3,2.75,2.5,2.3,2.15,2];
  // voltage_val1=4.5;
  // voltage_val2=3.6;
  // voltage_val3=3.75;
  // voltage_val4=3.35;
  // voltage_val5=3;
  // voltage_val6=2.75;
  // current_val1=1.5;
  // current_val2=.86;
  // current_val3=.75;
  // current_val4=0.67;
  // current_val5=0.6;
  // current_val6=0.55;
  // rhst_val1=0;
  // rhst_val2=1;
  // rhst_val3=2;
  // rhst_val4=3;
  // rhst_val5=4;
  // rhst_val6=5;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  //////////////////////////////////Graph///////////////////////////


  /////////////////////////////////////////////////////////////////

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle2={ font: "32px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  
  dialogbox1=this.game.add.sprite(20,30,'dialogue_box3');
  dialogbox1.scale.setTo(.63,1.02);

  dialogbox2=this.game.add.sprite(20,900,'dialogue_box1');
  dialogbox2.scale.setTo(.511,.8);
  //dialogbox2.scale.setTo(.8,1);

  base= this.game.add.sprite(20,400,'observation_table');
  base.scale.setTo(.63,1.02);
  //switch(exp_Name){
   // case "Iron with copper sulphate":
   
   graph=this.game.add.sprite(840,120,'obervation_graph');//x=1100
   graph.scale.setTo(1.1,1.3);


   r1=voltage_val1/current_val1;
   r2=voltage_val2/current_val2;
   r3=voltage_val3/current_val3;
   r4=voltage_val4/current_val4;
   r5=voltage_val5/current_val5;
   r6=voltage_val6/current_val6;

    sno_1="1";
    experiment_1=current_val1+"";
    observation_1=voltage_val1+" ";
    inference_1=Number(r1).toFixed(1)+" ";

    sno_2="2";
    experiment_2=current_val2+"";
    observation_2=voltage_val2+"";
    inference_2=Number(r2).toFixed(1)+" ";


    sno_3="3";
    experiment_3=current_val3+"";
    observation_3=voltage_val3+"";
    inference_3=Number(r3).toFixed(1)+" ";

    sno_4="4";
    experiment_4=current_val4+"";
    observation_4=voltage_val4+"";
    inference_4=Number(r4).toFixed(1)+"";

    sno_5="5";
    experiment_5=current_val5+"";
    observation_5=voltage_val5+"";
    inference_5=Number(r5).toFixed(1)+"";

    sno_6="6";
    experiment_6=current_val6+"";
    observation_6=voltage_val6+"";
    inference_6=Number(r6).toFixed(1)+" ";

    //inference_3="Fe(s) + Cu\u00B2\u207A(aq)---->Fe\u00B2\u207A(aq) + Cu(s)";
    //inference_4="Grey          Blue                    Pale green    Reddish brown";/**/
    
    sno_text_1=this.game.add.text(65,525,sno_1,fontStyle2);
    experiment_text_1=this.game.add.text(210,525,experiment_1,fontStyle2);
    observation_text_1=this.game.add.text(460,525,observation_1,fontStyle2);
    inference_text_1=this.game.add.text(700,525,inference_1,fontStyle2);
    
    sno_text_2=this.game.add.text(65,585,sno_2,fontStyle2);
    experiment_text_2=this.game.add.text(210,585,experiment_2,fontStyle2);
    observation_text_2=this.game.add.text(460,585,observation_2,fontStyle2);
    inference_text_2=this.game.add.text(700,585,inference_2,fontStyle2);

    sno_text_3=this.game.add.text(65,650,sno_3,fontStyle2);
    experiment_text_3=this.game.add.text(210,650,experiment_3,fontStyle2);
    observation_text_3=this.game.add.text(460,650,observation_3,fontStyle2);
    inference_text_3=this.game.add.text(700,650,inference_3,fontStyle2);

    sno_text_4=this.game.add.text(65,715,sno_4,fontStyle2);
    experiment_text_4=this.game.add.text(210,715,experiment_4,fontStyle2);
    observation_text_4=this.game.add.text(460,715,observation_4,fontStyle2);
    inference_text_4=this.game.add.text(700,715,inference_4,fontStyle2);

    sno_text_5=this.game.add.text(65,785,sno_5,fontStyle2);
    experiment_text_5=this.game.add.text(210,785,experiment_5,fontStyle2);
    observation_text_5=this.game.add.text(460,785,observation_5,fontStyle2);
    inference_text_5=this.game.add.text(700,785,inference_5,fontStyle2);

    sno_text_6=this.game.add.text(65,850,sno_6,fontStyle2);
    experiment_text_6=this.game.add.text(210,850,experiment_6,fontStyle2);
    observation_text_6=this.game.add.text(460,850,observation_6,fontStyle2);
    inference_text_6=this.game.add.text(700,850,inference_6,fontStyle2);
//////////////////////////////////////////////////////////////////////////////////////////////////
    sno_text_2.visible=false;
    experiment_text_2.visible=false;
    observation_text_2.visible=false;
    inference_text_2.visible=false;

    sno_text_3.visible=false;
    experiment_text_3.visible=false;
    observation_text_3.visible=false;
    inference_text_3.visible=false;

    sno_text_4.visible=false;
    experiment_text_4.visible=false;
    observation_text_4.visible=false;
    inference_text_4.visible=false;

    sno_text_5.visible=false;
    experiment_text_5.visible=false;
    observation_text_5.visible=false;
    inference_text_5.visible=false;

    sno_text_6.visible=false;
    experiment_text_6.visible=false;
    observation_text_6.visible=false;
    inference_text_6.visible=false;




    observatio_top_text_1=this.game.add.text(40,150,"Range of ammeter             = 0 A to 1 A",fontStyle2);
    observatio_top_text_1=this.game.add.text(40,210,"Least count of ammeter     = 0.1 A",fontStyle2);
    observatio_top_text_1=this.game.add.text(40,270,"Range of voltmeter            = 0 V to 5 V",fontStyle2);
    observatio_top_text_1=this.game.add.text(40,330,"Least count of voltmeter    = 0.1 V",fontStyle2);
    observatio_top_text_1=this.game.add.text(40,960,"The mean resistance of the resistor ="+Number((r1+r2+r3)/3).toFixed(1)+ " ohm",fontStyle2);
    /*inference_text_3=this.game.add.text(950,700,inference_3,fontStyle);
    inference_text_4=this.game.add.text(950,750,inference_4,fontStyle_1);*/
    

   // break;
 // }
// next_btn = this.game.add.sprite(1590,860,'components','next_disabled.png');
//  next_btn.scale.setTo(.7,.7);
//  next_btn = this.game.add.sprite(1590,860,'components','next_pressed.png');
//  next_btn.scale.setTo(.7,.7);
//  next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
//  next_btn.input.useHandCursor = true;
//  next_btn.events.onInputDown.add(this.toNext, this);
//  prev_btn = this.game.add.sprite(230,860,'components','next_disabled.png');
//  prev_btn.scale.setTo(-.7,.7);
//  prev_btn = this.game.add.sprite(230,860,'components','next_pressed.png');
//  prev_btn.scale.setTo(-.7,.7);
//  prev_btn.inputEnabled = true;
 //prev_btn.input.priorityID = 3;
//  prev_btn.input.useHandCursor = true;
//  prev_btn.events.onInputDown.add(this.toPrev, this);
//  prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
//////////////////////////////////////For Graph//////////////////////////////////////////////////////////////////
graphCount=0;


// ammeter_Values=[1,.86,.75,.67,.6,.55,.5,.46,.43,.4];
ammeter_Values=[1.5,1.2,1,.85,.75,.66,.6,.54,.5,.46];
arrow_x=this.game.add.sprite(1196,755,"arrow");//1170,742
Ammeter_points=[1860,1669,1541,1446,1382,1325,1285,1248,1222,1196];


line2=this.game.add.graphics(0, 0);
line2.lineStyle(3,0xe5e5e5,1);
line2.moveTo(0,0);
line2.lineTo(0, -35);
line2.x=1816;//+7//1816
line2.y=755;



arrow_x.scale.setTo(.5,.5);
// voltmeter_Values=[5,4.3,3.75,3.35,3,2.75,2.5,2.3,2.15,2];
voltmeter_Values=[4.5,3.6,3,2.55,2.25,1.98,1.8,1.62,1.5,1.38];
Voltage_points=[410,475,520,555,578,597,611,625,635,643];
arrow1=this.game.add.sprite(908,643,"arrow");//1175,736
line1=this.game.add.graphics(0, 0);
line1.lineStyle(3,0xe5e5e5,1);
line1.moveTo(0,0);
line1.lineTo(77, 0);
line1.x=908;//+6//1175
line1.y=422;


arrow1.scale.setTo(.5,.5);
arrow_x.angle=-90;
rs_count=0;
arrow1.visible=false;
arrow_x.visible=false;
arrow1.move=false;
arrow_x.move=false;
//arrow1.visible=false;
//1170-v
//800-A

this.drawGraph();

//textObj=this.game.add.sprite(Ammeter_points[rhst_val1],Voltage_points[rhst_val1],"bullet");
//textObj1=this.game.add.sprite(Ammeter_points[rhst_val1]+10,Voltage_points[rhst_val1]-10,"bullet");

textLine=this.game.add.graphics(0, 0);
textLine.lineStyle(3,0x000fff,1);
textLine.moveTo(0,0);
textLine.lineTo(14,0);
textLine.x=908;//+6
textLine.y=755;
textLine.scale.setTo(1,1);
textLine.visible=false;
textLine.move=false;
// textLine.angle=-26.5;
textLine.angle=-19.5;

//this.game.physics.arcade.angleBetween(textObj, textObj1);
//Voltage_points=[442,492,532,560,583,604,620,634,644,654];

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        drawGraph:function(){
          arrow1.x=1175;
          console.log(rhst_val1);
          console.log(Voltage_points[rhst_val1]);
          if(graphCount==0){
            
            arrow1.y=Voltage_points[rhst_val1];
            line1.scale.setTo(1,1);
            line2.scale.setTo(0,0);
            line1.y=Voltage_points[rhst_val1]+6;

          }else if(graphCount==1){
            sno_text_2.visible=true;
            experiment_text_2.visible=true;
            observation_text_2.visible=true;
            inference_text_2.visible=true;
            arrow1.y=Voltage_points[rhst_val2];
            line1.scale.setTo(1,1);
            line2.scale.setTo(0,0);
            line1.y=Voltage_points[rhst_val2]+6;

          }else if(graphCount==2){
            sno_text_3.visible=true;
            experiment_text_3.visible=true;
            observation_text_3.visible=true;
            inference_text_3.visible=true;
            arrow1.y=Voltage_points[rhst_val3];
            line1.scale.setTo(1,1);
            line2.scale.setTo(0,0);
            line1.y=Voltage_points[rhst_val3]+6;
          }else if(graphCount==3){
            sno_text_4.visible=true;
            experiment_text_4.visible=true;
            observation_text_4.visible=true;
            inference_text_4.visible=true;
            arrow1.y=Voltage_points[rhst_val4];
            line1.scale.setTo(1,1);
            line2.scale.setTo(0,0);
            line1.y=Voltage_points[rhst_val4]+6;
          }else if(graphCount==4){
            sno_text_5.visible=true;
            experiment_text_5.visible=true;
            observation_text_5.visible=true;
            inference_text_5.visible=true;
            arrow1.y=Voltage_points[rhst_val5];
            line1.scale.setTo(1,1);
            line2.scale.setTo(0,0);
            line1.y=Voltage_points[rhst_val5]+6;
          }else if(graphCount==5){
            sno_text_6.visible=true;
            experiment_text_6.visible=true;
            observation_text_6.visible=true;
            inference_text_6.visible=true;
            arrow1.y=Voltage_points[rhst_val6];
            line1.scale.setTo(1,1);
            line2.scale.setTo(0,0);
            line1.y=Voltage_points[rhst_val6]+6;
          }else if(graphCount==6){
            line1.scale.setTo(0,0);
            line2.scale.setTo(0,0);
            textLine.move=true;
            textLine.visible=true;
          }

          arrow1.move=true;
          console.log(arrow1.move+"......");
          //line1.alpha=1;
        },
        drawGraph1:function(){
          arrow_x.x=1175;
          console.log(rhst_val1);
          console.log(Voltage_points[rhst_val1]);
          if(graphCount==0){
            arrow_x.x=Ammeter_points[rhst_val1];
            line2.scale.setTo(1,1);
            line2.x=Ammeter_points[rhst_val1]+8;
          }else if(graphCount==1){
            arrow_x.x=Ammeter_points[rhst_val2];
            line2.scale.setTo(1,1);
            line2.x=Ammeter_points[rhst_val2]+8;
          }else if(graphCount==2){
            arrow_x.x=Ammeter_points[rhst_val3];
            line2.scale.setTo(1,1);
            line2.x=Ammeter_points[rhst_val3]+8;
          }else if(graphCount==3){
            arrow_x.x=Ammeter_points[rhst_val4];
            line2.scale.setTo(1,1);
            line2.x=Ammeter_points[rhst_val4]+8;
          }else if(graphCount==4){
            arrow_x.x=Ammeter_points[rhst_val5];
            line2.scale.setTo(1,1);
            line2.x=Ammeter_points[rhst_val5]+8;
          }else if(graphCount==5){
            arrow_x.x=Ammeter_points[rhst_val6];
            line2.scale.setTo(1,1);
            line2.x=Ammeter_points[rhst_val6]+8;
          }
          arrow_x.move=true;
          //line1.alpha=1;
        },


    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice1.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    //voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  update: function()
  {
    if(sceneNo==1){
      count++;
      //console.log(count);
      if(count==1050){
        // voice.destroy();
        // voice=this.game.add.audio("A_observation2",1);
        // voice.play();
        // play.visible=true;
      }
    }
    //////////////////////////////////For Graph//////////////////////////////////////////
    if(textLine.move==true)
    {
      if(textLine.scale.x<75)
        textLine.scale.x+=.1;
      
        if(textLine.scale.x>=75)
        {
          
          textLine.move=false;
          voice.destroy();
        voice=this.game.add.audio("A_observation2",1);
        voice.play();
        play.visible=true;
        }
        //textLine.y+=.1;

    }

    if(arrow1.move){
      if(line1.scale.x<13){
       line1.scale.x+=.1;
       if(line1.scale.x>=13){

          arrow1.move=false;
          this.drawGraph1();
       }
      }
    }
    if(arrow_x.move){
      if(line2.scale.y<11){
       line2.scale.y+=.1;
       if(line2.scale.y>=11){
        arrow_x.move=false;
        if(graphCount==0){
          bullet1=this.game.add.sprite(Ammeter_points[rhst_val1]+8,Voltage_points[rhst_val1]+7,"bullet");
          bullet1.anchor.setTo(.5,.5);
        }else if(graphCount==1){
          bullet2=this.game.add.sprite(Ammeter_points[rhst_val2]+8,Voltage_points[rhst_val2]+7,"bullet");
          bullet2.anchor.setTo(.5,.5);
        }if(graphCount==2){
          bullet3=this.game.add.sprite(Ammeter_points[rhst_val3]+8,Voltage_points[rhst_val3]+7,"bullet");
          bullet3.anchor.setTo(.5,.5);
        }if(graphCount==3){
          bullet4=this.game.add.sprite(Ammeter_points[rhst_val4]+8,Voltage_points[rhst_val4]+7,"bullet");
          bullet4.anchor.setTo(.5,.5);
        }if(graphCount==4){
          bullet5=this.game.add.sprite(Ammeter_points[rhst_val5]+8,Voltage_points[rhst_val5]+7,"bullet");
          bullet5.anchor.setTo(.5,.5);
        }if(graphCount==5){
          bullet6=this.game.add.sprite(Ammeter_points[rhst_val6]+8,Voltage_points[rhst_val6]+7,"bullet");
          bullet6.anchor.setTo(.5,.5);
        }
        graphCount++;
        this.drawGraph();
       }
      }
    }
  },
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  //voice.destroy();
      //  voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  