var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var count;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var sceneNo;
  var r1;
  var r2;
  var r3;
  var r4;
  var r5;
  var r6;
  var r7;
  var r8;
  var r9;
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

sceneNo=1;
count=0;
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
 muted = false;
 voice=this.game.add.audio("A_observation",1);
 voice2=this.game.add.audio("A_observation2",1);
 
 voice.play();
 setTimeout(this.nextSound,2000);
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  // R1voltage_val1=0;
  // R1voltage_val2=0;
  // R1voltage_val3=0;
  // R2voltage_val1=0;
  // R2voltage_val2=0;
  // R2voltage_val3=0;
  // R3voltage_val1=0;
  // R3voltage_val2=0;
  // R3voltage_val3=0;
  // R1current_val1=0;
  // R1current_val2=0;
  // R1current_val3=0;
  // R2current_val1=0;
  // R2current_val2=0;
  // R2current_val3=0;
  // R3current_val1=0;
  // R3current_val2=0;
  // R3current_val3=0;
  


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  //////////////////////////////////Graph///////////////////////////
//  graph=this.game.add.sprite(1100,200,'obervation_graph');
//  graph.scale.setTo(.15,.15);

  /////////////////////////////////////////////////////////////////

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  headfontStyle2={ font: "28px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  fontStyle2={ font: "32px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  
  dialogbox1=this.game.add.sprite(50,30,'dialogue_box3');
  dialogbox1.scale.setTo(.8,.7);

  dialogbox2=this.game.add.sprite(50,900,'dialogue_box1');
  dialogbox2.scale.setTo(.65,.1);
  //dialogbox2.scale.setTo(.8,1);

  base= this.game.add.sprite(50,270,'observation_table');
  //base.scale.setTo(.8,1.1);
  base.scale.setTo(1,1);
  //switch(exp_Name){
   // case "Iron with copper sulphate":
   
   r1=R1voltage_val1/R1current_val1;
   r2=R1voltage_val2/R1current_val2;
   r3=R1voltage_val3/R1current_val3;
   r4=R2voltage_val1/R2current_val1;
   r5=R2voltage_val2/R2current_val2;
   r6=R2voltage_val3/R2current_val3;
   r7=R3voltage_val1/R3current_val1;
   r8=R3voltage_val2/R3current_val2;
   r9=R3voltage_val3/R3current_val3;
   

    heading1=this.game.add.text(260,310,"S/N",headfontStyle);
    heading2=this.game.add.text(400,300,"Voltmeter\nreading V (volts)",headfontStyle);
    heading3=this.game.add.text(740,300,"Ammeter\nreading I (amperes)",headfontStyle);
    heading4=this.game.add.text(1080,300,"Resistance\nR=V/ I (ohms)",headfontStyle);
    heading5=this.game.add.text(1340,300,"Mean value\nof resistor",headfontStyle);
    R1=this.game.add.text(65,410,"Resistor R1\n(Resistance\n of \nR1=1.5 ohm)",headfontStyle2);
    R2=this.game.add.text(65,650,"Resistor R2\n(Resistance\n of \nR2=2.5 ohm)",headfontStyle2);
    R1_R2=this.game.add.text(68,900,"Resistor R1,\nR2 in\nparallel",headfontStyle2);

    sno_1="1";
    experiment_1=R1voltage_val1+"";
    observation_1=R1current_val1+" ";
    inference_1=Number(r1).toFixed(1)+" ";
    inference1=Number((r1+r2+r3)/3).toFixed(1)+" ohm";

    sno_2="2";
    experiment_2=R1voltage_val2+"";
    observation_2=R1current_val2+"";
    inference_2=Number(r2).toFixed(1)+" ";
   

    sno_3="3";
    experiment_3=R1voltage_val3+"";
    observation_3=R1current_val3+"";
    inference_3=Number(r3).toFixed(1)+" ";

    sno_4="1";
    experiment_4=R2voltage_val1+"";
    observation_4=R2current_val1+"";
    inference_4=Number(r4).toFixed(1)+"";
    inference4=Number((r4+r5+r6)/3).toFixed(1)+" ohm";

    sno_5="2";
    experiment_5=R2voltage_val2+"";
    observation_5=R2current_val2+"";
    inference_5=Number(r5).toFixed(1)+"";

    sno_6="3";
    experiment_6=R2voltage_val3+"";
    observation_6=R2current_val3+"";
    inference_6=Number(r6).toFixed(1)+" ";

    sno_7="1";
    experiment_7=R3voltage_val1+"";
    observation_7=R3current_val1+"";
    inference_7=Number(r7).toFixed(1)+"5";
    inference7=Number((r7+r8+r9)/3).toFixed(1)+"5 ohm";

    sno_8="2";
    experiment_8=R3voltage_val2+"";
    observation_8=R3current_val2+"";
    inference_8=Number(r8).toFixed(1)+"5";

    sno_9="3";
    experiment_9=R3voltage_val3+"";
    observation_9=R3current_val3+"";
    inference_9=Number(r9).toFixed(1)+"5";

   
    
    sno_text_1=this.game.add.text(290,400,sno_1,fontStyle2);
    experiment_text_1=this.game.add.text(500,400,experiment_1,fontStyle2);
    observation_text_1=this.game.add.text(860,400,observation_1,fontStyle2);
    inference_text_1=this.game.add.text(1150,400,inference_1,fontStyle2);
    mean_val_text_1=this.game.add.text(1350,470,inference1,fontStyle2);
    
    sno_text_2=this.game.add.text(290,480,sno_2,fontStyle2);
    experiment_text_2=this.game.add.text(500,480,experiment_2,fontStyle2);
    observation_text_2=this.game.add.text(860,480,observation_2,fontStyle2);
    inference_text_2=this.game.add.text(1150,480,inference_2,fontStyle2);
    //mean_val_text_2=this.game.add.text(1400,480,inference2,fontStyle2);

    sno_text_3=this.game.add.text(290,560,sno_3,fontStyle2);
    experiment_text_3=this.game.add.text(500,560,experiment_3,fontStyle2);
    observation_text_3=this.game.add.text(860,560,observation_3,fontStyle2);
    inference_text_3=this.game.add.text(1150,560,inference_3,fontStyle2);
    //mean_val_text_3=this.game.add.text(1400,560,inference3,fontStyle2);


    sno_text_4=this.game.add.text(290,640,sno_4,fontStyle2);
    experiment_text_4=this.game.add.text(500,640,experiment_4,fontStyle2);
    observation_text_4=this.game.add.text(860,640,observation_4,fontStyle2);
    inference_text_4=this.game.add.text(1150,640,inference_4,fontStyle2);
    mean_val_text_4=this.game.add.text(1350,710,inference4,fontStyle2);

    sno_text_5=this.game.add.text(290,720,sno_5,fontStyle2);
    experiment_text_5=this.game.add.text(500,720,experiment_5,fontStyle2);
    observation_text_5=this.game.add.text(860,720,observation_5,fontStyle2);
    inference_text_5=this.game.add.text(1150,720,inference_5,fontStyle2);
    //mean_val_text_5=this.game.add.text(1400,720,inference5,fontStyle2);

    sno_text_6=this.game.add.text(290,800,sno_6,fontStyle2);
    experiment_text_6=this.game.add.text(500,800,experiment_6,fontStyle2);
    observation_text_6=this.game.add.text(860,800,observation_6,fontStyle2);
    inference_text_6=this.game.add.text(1150,800,inference_6,fontStyle2);
    //mean_val_text_6=this.game.add.text(1400,800,inference6,fontStyle2);

    sno_text_7=this.game.add.text(290,860,sno_7,fontStyle2);
    experiment_text_7=this.game.add.text(500,860,experiment_7,fontStyle2);
    observation_text_7=this.game.add.text(860,860,observation_7,fontStyle2);
    inference_text_7=this.game.add.text(1150,860,inference_7,fontStyle2);
    mean_val_text_7=this.game.add.text(1350,920,inference7,fontStyle2);

    sno_text_8=this.game.add.text(290,930,sno_8,fontStyle2);
    experiment_text_8=this.game.add.text(500,930,experiment_8,fontStyle2);
    observation_text_8=this.game.add.text(860,930,observation_8,fontStyle2);
    inference_text_8=this.game.add.text(1150,930,inference_8,fontStyle2);
    //mean_val_text_8=this.game.add.text(1400,720,inference8,fontStyle2);

    sno_text_9=this.game.add.text(290,1000,sno_9,fontStyle2);
    experiment_text_9=this.game.add.text(500,1000,experiment_9,fontStyle2);
    observation_text_9=this.game.add.text(860,1000,observation_9,fontStyle2);
    inference_text_9=this.game.add.text(1150,1000,inference_9,fontStyle2);
    //mean_val_text_9=this.game.add.text(1400,800,inference9,fontStyle2);


    observatio_top_text_1=this.game.add.text(80,120,"Range of ammeter     = 0 A to 6 A",fontStyle2);
    observatio_top_text_1=this.game.add.text(600,120,"Least count of ammeter   =  0.1 A",fontStyle2);
    observatio_top_text_1=this.game.add.text(80,200,"Range of voltmeter    = 0 V to 5 V",fontStyle2);
    observatio_top_text_1=this.game.add.text(600,200,"Least count of voltmeter  = 0.1 V",fontStyle2);
    observatio_top_text_1=this.game.add.text(600,1500,"The mean resistance of the resistor ="+Number((r1+r2+r3)/3).toFixed(1)+ " ohm",fontStyle2);
    /*inference_text_3=this.game.add.text(950,700,inference_3,fontStyle);
    inference_text_4=this.game.add.text(950,750,inference_4,fontStyle_1);*/
    

   // break;
 // }
// next_btn = this.game.add.sprite(1590,860,'components','next_disabled.png');
//  next_btn.scale.setTo(.7,.7);
//  next_btn = this.game.add.sprite(1590,860,'components','next_pressed.png');
//  next_btn.scale.setTo(.7,.7);
//  next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
//  next_btn.input.useHandCursor = true;
//  next_btn.events.onInputDown.add(this.toNext, this);
//  prev_btn = this.game.add.sprite(230,860,'components','next_disabled.png');
//  prev_btn.scale.setTo(-.7,.7);
//  prev_btn = this.game.add.sprite(230,860,'components','next_pressed.png');
//  prev_btn.scale.setTo(-.7,.7);
//  prev_btn.inputEnabled = true;
 //prev_btn.input.priorityID = 3;
//  prev_btn.input.useHandCursor = true;
//  prev_btn.events.onInputDown.add(this.toPrev, this);
//  prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=true;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

  

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    
        nextSound:function()
        {

            voice.destroy();
            //voice=this.game.add.audio("A_observation2",1);
            voice2.play();

        },
    toNext:function(){
          sceneNo=2;
          voice.destroy();
          voice=this.game.add.audio("A_observation3",1);
          voice.play();
          sno_text_1.text="3";
          experiment_text_1.text="Reaction of dil. HCl \nwith sodium \ncarbonate ";
          observation_text_1.text="A brisk effervescence \nis observed. A colorless \nand odourless gas \nevolves. The gas turns \nlime water milky";
          inference_text_1.text="Acids on reaction with sodium \ncarbonate liberate CO\u2082, gas \nwith brisk effervescence.";
          
          sno_text_2.text="";
          experiment_text_2.text="";
          observation_text_2.text="";
          inference_text_2.text="";
          
          play.visible=true;
          next_btn.visible=false;
          prev_btn.visible=true;
        },
    toPrev:function(){
      sceneNo=1;
      count=0;
          voice.destroy();
          voice=this.game.add.audio("A_observation1",1);
          voice.play();
          sno_text_1.text=sno_1;
          experiment_text_1.text=experiment_1;
          observation_text_1.text=observation_1;
          inference_text_1.text=inference_1;
          
          sno_text_2.text=sno_2;
          experiment_text_2.text=experiment_2;
          observation_text_2.text=observation_2;
          inference_text_2.text=inference_2;
          next_btn.visible=true;
          prev_btn.visible=false;
        },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice1.destroy();
        voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  update: function()
  {
    // if(sceneNo==1){
    //   count++;
    //   //console.log(count);
    //   if(count==1050){
    //     voice.destroy();
    //     voice=this.game.add.audio("A_observation2",1);
    //     voice.play();
    //   }
    // }
  },
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  //voice.destroy();
      //  voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  