var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
 
  //var corrent_val1;
  var resistance_val1;
  
 // var corrent_val2;
  var resistance_val2;
  
  //var corrent_val3;
  var resistance_val3;
  
  //var corrent_val4;
  var resistance_val4;
 
  var R1voltage_val1;
  var R1voltage_val2;
  var R1voltage_val3;
  var R2voltage_val1;
  var R2voltage_val2;
  var R2voltage_val3;
  var R3voltage_val1;
  var R3voltage_val2;
  var R3voltage_val3;

  var R1current_val1;
  var R1current_val2;
  var R1current_val3;
  var R2current_val1;
  var R2current_val2;
  var R2current_val3;
  var R3current_val1;
  var R3current_val2;
  var R3current_val3;

}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
       
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         //this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        this.game.load.image('dialogue_box3','assets/Ohms_law/dialogbox2.png');
        
         this.game.load.image('observation_table','assets/Ohms_law/observationTable.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
//////////////////////////////Experiment images//////////////////////////////////
        this.game.load.image('bg','assets/Ohms_law/Bg.png');
        this.game.load.image('wire1','assets/Ohms_law/Wire_1.png');
        this.game.load.image('wire2','assets/Ohms_law/Wire_2.png');
        this.game.load.image('wire3','assets/Ohms_law/Wire_3.png');
        this.game.load.image('wire4','assets/Ohms_law/Wire_4.png');
        this.game.load.image('wire5','assets/Ohms_law/Wire_5.png');
        this.game.load.image('wire6','assets/Ohms_law/Wire_6.png');
        this.game.load.image('wire7','assets/Ohms_law/Wire_7.png');
        this.game.load.image('wire8','assets/Ohms_law/Wire_8.png');
        this.game.load.image('wire9','assets/Ohms_law/Wire_9.png');
        this.game.load.image('wire10','assets/Ohms_law/Wire_10.png');
        this.game.load.image('wire11','assets/Ohms_law/Wire_11.png');
        this.game.load.image('wire12','assets/Ohms_law/Wire_12.png');
 this.game.load.image('direction','assets/Direction.png');


        this.game.load.image('wireparallel1','assets/Parallel/Wire_1.png');
        this.game.load.image('wireparallel2','assets/Parallel/Wire_2.png');
        this.game.load.image('wireparallel3','assets/Parallel/Wire_3.png');
        this.game.load.image('wireparallel4','assets/Parallel/Wire_4.png');
        this.game.load.image('wireparallel5','assets/Parallel/Wire_5.png');
        this.game.load.image('wireparallel6','assets/Parallel/Wire_6.png');
        this.game.load.image('wireparallel21','assets/Parallel/Wire_2_1.png');
        this.game.load.image('wireparallel51','assets/Parallel/Wire_5_1.png');


        this.game.load.image('voltmeter','assets/Ohms_law/Voltmeter.png');
        this.game.load.image('voltmeter_full','assets/Ohms_law/Voltmeter_full.png');
        this.game.load.image('ammeter','assets/Ohms_law/Ammeter.png');
        this.game.load.image('ammeter_full','assets/Ohms_law/Ammeter_full.png');
        this.game.load.image('needle','assets/Ohms_law/Ammeter_needle.png');
        this.game.load.image('rheostat','assets/Ohms_law/Rheostat.png');
        this.game.load.image('rheostat_top','assets/Ohms_law/Rheostat_top.png');
        this.game.load.image('rheostat_full','assets/Ohms_law/Rheostat_full.png');
        this.game.load.image('keySet','assets/Ohms_law/Key_set.png');
        this.game.load.image('key','assets/Ohms_law/Key.png');
        this.game.load.image('keySet_full','assets/Ohms_law/Key_set_full.png');
        this.game.load.image('battery','assets/Ohms_law/Battery.png');
        this.game.load.image('nichrome_wire','assets/Ohms_law/R1.png');
        this.game.load.image('nichrome_wire2','assets/Ohms_law/R2.png');
         this.game.load.image('circuit','assets/Ohms_law/series.png');
      this.game.load.image('take_reading_btn','assets/Ohms_law/take_reading_button.png');


         this.game.load.image('result_text1','assets/Ohms_law/result1.png');
         this.game.load.image('result_text2','assets/Ohms_law/result2.png');
         this.game.load.image('result_text3','assets/Ohms_law/result3.png');
         this.game.load.image('result_graph','assets/Ohms_law/obs_graph.png');
         this.game.load.image('obervation_graph','assets/Ohms_law/Graph.png');
         this.game.load.image('circuit_diagram','assets/Ohms_law/Circuit_Diagram.png');
         this.game.load.image('circuit_diagram2','assets/Ohms_law/Circuit_Diagrama.png');


         ////////////////////////////material required//////////////////////////
        this.game.load.image('m_drycell','assets/Ohms_law/Materials/Battery.png');
this.game.load.image('m_voltmeter','assets/Ohms_law/Materials/Voltmeter.png');
this.game.load.image('m_ammeter','assets/Ohms_law/Materials/Ammeter.png');
this.game.load.image('m_rheostat','assets/Ohms_law/Materials/Rheostat.png');
this.game.load.image('m_key','assets/Ohms_law/Materials/Key.png');
this.game.load.image('m_resistor','assets/Ohms_law/Materials/Resistor.png');
this.game.load.image('m_resistor2','assets/Ohms_law/Materials/R2.png');
this.game.load.image('m_wire','assets/Ohms_law/Materials/Wire.png');
this.game.load.image('m_ammeter_screen','assets/Ohms_law/Materials/Ameter_screen.png');
this.game.load.image('m_voltmeter_screen','assets/Ohms_law/Materials/Voltmeter_screen.png');
this.game.load.image('circuit_paralleldiagram','assets/Parallel/Circuit Diagram Parallel-01.png');
         //////////////////////////////////////////////////////////////////////
      //   this.game.load.image('wire9','assets/Ohms_law/Bg.png');
      //   this.game.load.image('wire9','assets/Ohms_law/Bg.png');
      //   this.game.load.image('wire9','assets/Ohms_law/Bg.png');
      //   this.game.load.image('wire9','assets/Ohms_law/Bg.png');
      //   this.game.load.image('wire9','assets/Ohms_law/Bg.png');
  /////////////////////Experiment_A1_sounds////////////////////////////////////
        this.game.load.audio('A_aim','assets/audio/Experiment_A1/Aim.mp3');
        this.game.load.audio('A_precaution1','assets/audio/Experiment_A1/precaution_1.mp3');
        this.game.load.audio('A_precaution2','assets/audio/Experiment_A1/precaution_2.mp3');
        this.game.load.audio('A_precaution3','assets/audio/Experiment_A1/precaution_3.mp3');
        this.game.load.audio('A_precaution4','assets/audio/Experiment_A1/precaution_4.mp3');

        this.game.load.audio('A_procedure1','assets/audio/Experiment_A1/procedure_1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Experiment_A1/procedure_2.mp3');
        this.game.load.audio('A_procedure3','assets/audio/Experiment_A1/procedure_3.mp3');
        this.game.load.audio('A_procedure4','assets/audio/Experiment_A1/procedure_4.mp3');
        this.game.load.audio('A_procedure5','assets/audio/Experiment_A1/procedure_5.mp3');
        this.game.load.audio('A_procedure6','assets/audio/Experiment_A1/procedure_6.mp3');
        this.game.load.audio('A_exp_procedure1','assets/audio/Experiment_A1/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/Experiment_A1/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/Experiment_A1/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/Experiment_A1/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/Experiment_A1/A_exp_procedure5.mp3');
        this.game.load.audio('A_exp_procedure5b','assets/audio/Experiment_A1/A_exp_procedure5b.mp3');
        this.game.load.audio('A_exp_procedure6','assets/audio/Experiment_A1/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/Experiment_A1/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/Experiment_A1/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/Experiment_A1/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/Experiment_A1/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/Experiment_A1/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/Experiment_A1/A_exp_procedure12.mp3');
        this.game.load.audio('A_exp_procedure13','assets/audio/Experiment_A1/A_exp_procedure13.mp3');
        this.game.load.audio('A_exp_procedure14','assets/audio/Experiment_A1/A_exp_procedure14.mp3');
      this.game.load.audio('A_exp_procedure15','assets/audio/Experiment_A1/A_exp_procedure15.mp3');
      this.game.load.audio('A_exp_procedure16','assets/audio/Experiment_A1/A_exp_procedure16.mp3');
      this.game.load.audio('A_exp_procedure17','assets/audio/Experiment_A1/A_exp_procedure26.mp3');
      this.game.load.audio('A_exp_procedure18','assets/audio/Experiment_A1/A_exp_procedure18.mp3');
      this.game.load.audio('A_exp_procedure19','assets/audio/Experiment_A1/A_exp_procedure19.mp3');
      this.game.load.audio('A_exp_procedure20','assets/audio/Experiment_A1/A_exp_procedure20.mp3');
       this.game.load.audio('A_exp_procedure21','assets/audio/Experiment_A1/A_exp_procedure21.mp3');
       this.game.load.audio('A_exp_procedure22','assets/audio/Experiment_A1/A_exp_procedure22.mp3');
     this.game.load.audio('A_exp_procedure23','assets/audio/Experiment_A1/A_exp_procedure23.mp3');
       this.game.load.audio('A_exp_procedure24','assets/audio/Experiment_A1/A_exp_procedure24.mp3');
       this.game.load.audio('A_exp_procedure27','assets/audio/Experiment_A1/A_exp_procedure27.mp3');
       this.game.load.audio('A_exp_procedure28','assets/audio/Experiment_A1/A_exp_procedure28.mp3');
      this.game.load.audio('lets_perform_Exp','assets/audio/Experiment_A1/lets_perform_Exp.mp3');

       this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
        this.game.load.audio('A_observation','assets/audio/Experiment_A1/A_observation.mp3');
        this.game.load.audio('A_observation2','assets/audio/Experiment_A1/A_observation2.mp3');
        this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
      
      },
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
     loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
     //loc="https://scienceapp.in/swadhyaya/science_lab_V/lab_main.php?parent_id=30";
      //this.game.state.start("Exp_Selection");//Starting the gametitle state
 
    //this.game.state.start("Experiment_A1");//Starting the gametitle state

    //this.game.state.start("Experiment_A2");


      this.game.state.start("Aim");//Simulation_hot1
      //this.game.state.start("Theory");//Simulation_hot1
      //this.game.state.start("Lab_Precautions");
    //this.game.state.start("Procedure");
      //this.game.state.start("Observations");//hot
    //this.game.state.start("Result");//Starting the gametitle state
     //// this.game.state.start("Materials");//Starting the gametitle state
    //  this.game.state.start("Viva");//Starting the gametitle state
	}
}

