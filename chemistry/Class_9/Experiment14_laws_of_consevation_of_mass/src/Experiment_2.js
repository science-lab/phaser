var experiment_2 = function(game){

var inst;



var delay;
var incr;
var currentobj;
var arrow_y;
var mix_count;
var spoon3;
var bowl;
var interval;
var heatFlag;
var heatFlag2;
var magnetFlag;
var glass_rod1;
var content1;
var magnet1;
var magnet2;
var spoon4;
var spoon5;
var shakeFlag1;
var shakeFlag2;
var shakingFlag1;
var shakingFlag2;
var shakingCount1;
var shakingCount2;
var mixFlag;
var stickFlag;
var stick;
var texttube3;
var texttube4;
}


experiment_2.prototype = {
  	create: function(){
      fontstyle_1={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_2={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_3={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_4={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_5={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_6={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_7={ font: "40px Segoe UI", fill: "#FFA500", align: "left" };
hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');
interval=0;
heatFlag=false;
heatFlag2=false;
magnetFlag=false;
currentobj=null;
glass_rod1=null;
magnet1=null;
magnet2=null;
spoon4=null;
spoon5=null;
shakeFlag1=false;
shakeFlag2=false;
shakingFlag1=false;
shakingFlag2=false;
shakingCount1=0;
shakingCount2=0;
mixFlag=false;
hclFlag=false;
compHeatFlag=false;
mixHeatFlag=false;
stickFlag=false;
stick=null;
texttube3=null;
texttube4=null;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/


spoon3=null;
bowl=null;
mix_count=0;

voice=this.game.add.audio("step0",1);
//voice.play();

this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Procedure_3", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  nextSound3:function(){
    //voice3.play();
  },
  addItems:function(){
    
      
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
    dia_box.scale.setTo(.8,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(60,50,dia_text,fontstyle_2);
    collider=this.game.add.sprite(540,770, 'collider');//y-320
    dialog.y=60;
    this.game.physics.arcade.enable(collider);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.0;

    
    //plate=this.game.add.sprite(800,720, 'compounds', 'plate.png');//y-320
    plate1=this.game.add.sprite(500,720, 'compounds', 'plate.png');//y-320
    plate1.frameName="black_mass.png";
    plate1.y=850;//450
    compound_lable=this.game.add.sprite(530,910,'compounds', 'compound_lable.png');//500
    mixture=this.game.add.sprite(790,840, 'compounds', 'mixture.png');//y-440
    //mixture.visible=false;
    
    mix=this.game.add.sprite(780,570, 'mixing', 'mix0001.png');//y-170
    mix.scale.setTo(.61,.61);
    mix.visible=false;
    mix.animations.add('anim',[
            'mix0002.png',
            'mix0003.png',
            'mix0004.png',
            'mix0005.png',
            'mix0006.png',
            'mix0007.png',
            'mix0008.png',
            
             ], 10, true, false);

    //ironFillings=this.game.add.sprite(1320,850, 'compounds', 'iron.png');//y-450, x-1370
    mixture_label=this.game.add.sprite(825,910, 'compounds', 'mixture_label.png');//y-510
    /**/
    
    
    magnet2=this.game.add.sprite(1150,890, 'compounds', 'magnet.png');//y-490
    magnet2.scale.setTo(.8,1);
    magnet2.anchor.setTo(.5,.5);
    magnet2.xp=1150;
    magnet2.yp=890;
    magnet1=this.game.add.sprite(1170,880, 'compounds', 'magnet.png');//y-480
    magnet1.scale.setTo(.8,1);
    magnet1.anchor.setTo(.5,.5);
    magnet1.xp=1170;
    magnet1.yp=880;
    magnet1.events.onDragStart.add(function() {this.gameC(magnet1)}, this);
    magnet1.events.onDragStop.add(function() {this.gameC1(magnet1)}, this);
    
    /////////////////////////////////////////////////
    
    
    // burner=this.game.add.sprite(30,600, 'compounds', 'burner0001.png');

    // burner.animations.add('anim',['burner0001.png',
    //         'burner0001.png',
    //          ], 10, true, false);
   
    
    arrow=this.game.add.sprite(1200,750, 'arrow');//y-350
    arrow.angle=90;
    arrow_y=750;//y-350
    arrow.visible=false;
    //mixture_label.scale.setTo(1.5,1.5);
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_1",1);
    voice.play();
    dialog.text="Drag a magnet into the compound.";
    magnet1.inputEnabled = true;
    magnet1.input.useHandCursor = true;
    //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
    magnet1.input.enableDrag(true);
    this.game.physics.arcade.enable(magnet1);
    arrow.x=1170;
    arrow.y=740;//340
    arrow_y=740;
    arrow.visible=true;

  },
  ClickOnSpoon:function(){

  },
  gameC:function(obj)
  {
    obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    arrow.visible=false;
    if(obj==magnet1){
      arrow.x=600;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }else if(obj==magnet2){
      arrow.x=900;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    
   
    if(currentobj==magnet1){
      magnet1.x=600;
      magnet1.y=800;//400
      arrow.x=1170;
      arrow.y=740;//340
      arrow_y=740;
      arrow.visible=false;
      /*arrow.visible=true;
      magnet2.events.onDragStart.add(function() {this.gameC(magnet2)}, this);
      magnet2.events.onDragStop.add(function() {this.gameC1(magnet2)}, this);
      magnet2.inputEnabled = true;
      magnet2.input.useHandCursor = true;
      //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
      magnet2.input.enableDrag(true);
      this.game.physics.arcade.enable(magnet2);*/
      collider.reset(840,720);//320
      //popup = this.game.add.sprite(420,550,'popup');
      //popup.scale.setTo(.4,.3);
      //popup.visible=false;
      voice.destroy(true);
      voice=this.game.add.audio("step2_2",1);
      voice.play();
      dialog.y=40;
      dialog.text="Observe that no particles cling to the magnet, because iron loses its \nmagnetic properties on forming to compound.";
      //dialog.text="Iron loses its magnetic properties on forming to compound. \nSo no particles cling into the magnet."
      //content1=this.game.add.text(470,700,dia_text,fontstyle_2);
      //play = this.game.add.sprite(1230,920,'components','play_pressed.png');
      //play.scale.setTo(.7,.7);
      //play.inputEnabled = true;
      //play.input.priorityID = 3;
      //play.input.useHandCursor = true;
      //play.events.onInputDown.add(this.toContinue01, this);
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.useSecondMagnet, this);
    }else if(currentobj==magnet2){
      mixture.visible=false;
      mix.visible=true;
      mix.frameName="mixture_magnet0002.png";
      mix.scale.setTo(1.02,1.02);
      mix.x=800;
      mix.y=705;//305
      magnet2.visible=false;
      magnetFlag=true;
      interval=0;
      arrow.visible=false;

    }
    
  },
  useSecondMagnet:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_3",1);
    voice.play();
    dialog.y=70;
    dialog.text="Drag the magnet into the mixture.";
    arrow.visible=true;
    magnet2.events.onDragStart.add(function() {this.gameC(magnet2)}, this);
    magnet2.events.onDragStop.add(function() {this.gameC1(magnet2)}, this);
    magnet2.inputEnabled = true;
    magnet2.input.useHandCursor = true;
    //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
    magnet2.input.enableDrag(true);
    this.game.physics.arcade.enable(magnet2);
    
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	addList:function(){
    

  },
  ClickOnPlay:function(){

    
  },
  
  ClickOnSlow2:function(){

  },
  


  hotsec:function()
  {

    //voice41.destroy();
    //voice5.play();

  },
  
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(magnetFlag){
      interval++;
      console.log(interval);
      if(interval==20){
        mix.frameName="mixture_magnet0002.png";
        mix.scale.setTo(1.02,1.02);
        mix.x=800;
        mix.y=705;//305
      }else if(interval==40){
        mix.frameName="mixture_magnet0003.png";
      }else if(interval==60){
        mix.frameName="mixture_magnet0004.png";
      }else if(interval==90){
        /////////////////////////
        

        voice.destroy(true);
        voice=this.game.add.audio("step2_4",1);
        voice.play();
        dialog.y=40;
        dialog.text="Observe that iron clings to the magnet, because iron retains its \nproperties.";
        //dialog.text="Iron clings into the magnet, because iron retains its properties. \nNow check with carbon disulphide."
        //content1=this.game.add.text(470,700,dia_text,fontstyle_2);
        //play.visible=true;
        //arrow.x=620;
        // arrow.visible=false;
        // collider.scale.setTo(1,3);
        // collider.x=1235;
        // collider.y=100;
        this.game.time.events.add(Phaser.Timer.SECOND*6,this.NextExperiment, this);
        magnetFlag=false;
      }
    }
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    /////////////////////////////
         
	},
  NextExperiment:function(){
    dialog.y=40;
    voice.destroy(true);
    voice=this.game.add.audio("step2_5",1);
    voice.play();
    dialog.text="Click on the next button to see the behaviour towards carbon \ndisulphide.";
    play.visible=true;
  },
  
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==magnet1||currentobj==magnet2){
        arrow.x=1170;
        arrow.y=740;//340
        arrow_y=740;
      }
      
      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_2",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

Hot:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Simulation_hot1",true,false,ip);

 },
toSimulation:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);

 },
graph:function(){

 },

LC:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);


 },


 nextsim:function()
 {


 },
 buttonState:function(state1,state2){

 state1.visible = false;
 state2.visible = true;
 },

buttonState2:function(state2,state1){


 state1.visible = true;
 state2.visible = false;
 }


}
