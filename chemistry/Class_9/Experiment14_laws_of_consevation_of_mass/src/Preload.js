var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Conservation//////////////////////////////////////////////////////////
this.game.load.image('plate','assets/Conservation/Plate.png');
this.game.load.image('digital_weight','assets/Conservation/Digital_weight.png');
this.game.load.image('beaker_back','assets/Conservation/Beaker_back.png');
this.game.load.image('beaker_with_water','assets/Conservation/Beaker_with_Water.png');

this.game.load.image('conical_flask_back','assets/Conservation/Conical_Flask_Back.png');
this.game.load.image('testube','assets/Conservation/Testube.png');
this.game.load.image('testube_back','assets/Conservation/Testube_back.png');
this.game.load.image('testube_with_tie','assets/Conservation/Testube_with_tie.png');
this.game.load.image('testube_with_tie_2','assets/Conservation/Testube_with_tie_2.png');
this.game.load.image('testube_with_tie_3','assets/Conservation/Testube_with_tie_3.png');
this.game.load.image('testube_with_tie_3_1','assets/Conservation/Testube_with_tie_3_1.png');
this.game.load.image('testube_with_tie_4','assets/Conservation/Testube_with_tie_4.png');
this.game.load.image('testube_with_tie_4_1','assets/Conservation/Testube_with_tie_4_1.png');
this.game.load.image('testube_rack_back','assets/Conservation/Testube_stand_Back.png');
this.game.load.image('testube_rack_front','assets/Conservation/Testube_stand_front.png');
this.game.load.image('tie','assets/Conservation/Tie.png');
this.game.load.image('cork','assets/Conservation/Cork.png');
this.game.load.image('barium_label','assets/Conservation/Barium.png');
this.game.load.image('sodium_label','assets/Conservation/Sodium.png');
this.game.load.image('water_label','assets/Conservation/Water.png');
this.game.load.image('Spoon_M','assets/Conservation/Spoon_M.png');


this.game.load.atlasJSONHash('arrow', 'assets/Conservation/arrow.png', 'assets/Conservation/arrow.json');
this.game.load.atlasJSONHash('drop_to_testube', 'assets/Conservation/Drop_to_testube.png', 'assets/Conservation/Drop_to_testube.json');
this.game.load.atlasJSONHash('testube_tilt', 'assets/Conservation/Testube_tilt.png', 'assets/Conservation/Testube_tilt.json');
this.game.load.atlasJSONHash('spoon', 'assets/Conservation/Spoon_4.png', 'assets/Conservation/Spoon_4.json');
this.game.load.atlasJSONHash('barium_testube', 'assets/Conservation/Barium_Testube.png', 'assets/Conservation/Barium_Testube.json');
this.game.load.atlasJSONHash('conical_flask','assets/Conservation/Conical_Flask.png', 'assets/Conservation/Conical_Flask.json');
this.game.load.atlasJSONHash('conical_flask_shake','assets/Conservation/Conical_flask_Shake.png', 'assets/Conservation/Conical_flask_Shake.json');
this.game.load.atlasJSONHash('testube_to_flask','assets/Conservation/Testube_to_flask.png', 'assets/Conservation/Testube_to_flask.json');
this.game.load.atlasJSONHash('Water_to_flask','assets/Conservation/Water_to_Flask.png', 'assets/Conservation/Water_to_Flask.json');


      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');  
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Procedure_4','assets/audio/Procedure_4.mp3');
      this.game.load.audio('Procedure_5','assets/audio/Procedure_5.mp3');  
      this.game.load.audio('Procedure_6','assets/audio/Procedure_6.mp3');
      

      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('step0','assets/audio/step0.mp3');
      this.game.load.audio('step1','assets/audio/step1.mp3');
      this.game.load.audio('step2','assets/audio/step2.mp3');
      this.game.load.audio('step3','assets/audio/step3.mp3');
      this.game.load.audio('step4','assets/audio/step4.mp3');
      this.game.load.audio('step5','assets/audio/step5.mp3');
      this.game.load.audio('step6','assets/audio/step6.mp3');
      this.game.load.audio('step7','assets/audio/step7.mp3');
      this.game.load.audio('step8','assets/audio/step8.mp3');
      this.game.load.audio('step9','assets/audio/step9.mp3');
      this.game.load.audio('step10','assets/audio/step10.mp3');
      this.game.load.audio('step11','assets/audio/step11.mp3');
      this.game.load.audio('step12','assets/audio/step12.mp3');
      this.game.load.audio('step13','assets/audio/step13.mp3');
      this.game.load.audio('step14','assets/audio/step14.mp3');
      this.game.load.audio('step15','assets/audio/step15.mp3');
      this.game.load.audio('step16','assets/audio/step16.mp3');
      this.game.load.audio('step17','assets/audio/step17.mp3');
      this.game.load.audio('step18','assets/audio/step18.mp3');
      this.game.load.audio('step19','assets/audio/step19.mp3');
      this.game.load.audio('step20','assets/audio/step20.mp3');

      

      
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

