var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;
var interval;
var content1;
var dropperCount;
var initial_weight;
var weightFlag;
}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');

interval=0;
initial_weight=0;
weightFlag=false;
weight=238.25;
currentobj=null;
dropperCount=0;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/

voice=this.game.add.audio("step0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(840,770, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.0;

    barium_plate=this.game.add.sprite(1350,900, 'plate');
    barium_plate.anchor.set(.5);
    barium_plate.scale.set(.65);
    barium_label=this.game.add.sprite(1350,950, 'barium_label');
    barium_label.anchor.set(.5);
    //barium_label.scale.set(.65);
    

    sodium_plate=this.game.add.sprite(1650,900, 'plate');
    sodium_plate.anchor.set(.5);
    sodium_plate.scale.set(.65);
    sodium_label=this.game.add.sprite(1650,950, 'sodium_label');
    sodium_label.anchor.set(.5);
    //sodium_label.scale.set(.65);
    // maskflask = this.game.add.graphics(0, 0);
    // maskflask.lineStyle(5,0xE67E22,.6);
    // maskflask.beginFill(0x000000,.2);//9B59B6
    // maskflask.drawRect(0, 0, 300, 400);
    // maskflask.x=600;
    // maskflask.y=570;
    // maskflask.visible=false;
    
     digital_weight=this.game.add.sprite(2900,900, 'digital_weight');
     digital_weight.anchor.set(.5);
     //digital_weight.scale.set(1.5);
    conical_flask=this.game.add.sprite(760,760, 'conical_flask_back');
    conical_flask.anchor.set(.5);
    conical_flask.scale.set(1.4);
    conical_flask.state="flask";
    conical_flask.xp=760;
    conical_flask.yp=760;

    conical_flask.events.onDragStart.add(function() {this.gameC(conical_flask)}, this);
    conical_flask.events.onDragStop.add(function() {this.gameC1(conical_flask)}, this);
    this.game.physics.arcade.enable(conical_flask);

    cork=this.game.add.sprite(-3,-130, 'cork');
    cork.scale.set(.6);
    cork.anchor.set(.5);
    cork.visible=false;
    conical_flask.addChild(cork);

    thread_testube_4=this.game.add.sprite(-40,-3, 'testube_with_tie_4');//758
     thread_testube_4.anchor.set(.5);
     thread_testube_4.scale.set(.6);
    thread_testube_4.visible=false;
    conical_flask.addChild(thread_testube_4);
    thread_testube_4_1=this.game.add.sprite(-40,-3, 'testube_with_tie_4_1');//758
     thread_testube_4_1.anchor.set(.5);
     thread_testube_4_1.scale.set(.6);
    thread_testube_4_1.visible=false;
    conical_flask.addChild(thread_testube_4_1);


    thread_testube_3=this.game.add.sprite(0,-350, 'testube_with_tie_3');//758
     thread_testube_3.anchor.set(.5);
     thread_testube_3.scale.set(.6);
    thread_testube_3.visible=false;
    conical_flask.addChild(thread_testube_3);
    thread_testube_3_1=this.game.add.sprite(0,-350, 'testube_with_tie_3_1');//758
     thread_testube_3_1.anchor.set(.5);
     thread_testube_3_1.scale.set(.6);
    thread_testube_3_1.visible=false;
    conical_flask.addChild(thread_testube_3_1);
    
    conical_flask_f=this.game.add.sprite(0,0, 'conical_flask','Conical_Flask.png');
    conical_flask_f.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],10,false,true);
    conical_flask_f.anchor.set(.5);
    //conical_flask_f.scale.set(1.4);
    //conical_flask_f.visible=false;
    conical_flask.addChild(conical_flask_f);
     conical_flask_water=this.game.add.sprite(-3.5,-6, 'Water_to_flask','Water_to_Flask0001.png');
    conical_flask_water.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12],10,false,true);
    conical_flask_water.animations.add('anim2',[13,14,15,16,17,18,19,20,21,22,23],10,false,true);
    conical_flask_water.anchor.set(.5);
    //conical_flask_water.scale.set(1.4);
    conical_flask_water.visible=false;
    conical_flask.addChild(conical_flask_water);

    
    

    testube_to_flask=this.game.add.sprite(-3.5,-6, 'testube_to_flask','Testube_to_flask0000.png');
    testube_to_flask.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21],10,false,true);
    testube_to_flask.anchor.set(.5);
    //testube_to_flask.scale.set(1.4);
    testube_to_flask.visible=false;
    conical_flask.addChild(testube_to_flask);
    //conical_flask.mask=maskflask;

    conical_flask_shake=this.game.add.sprite(750,760, 'conical_flask_shake','Conical_flask_Shake0001.png');
    conical_flask_shake.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29],10,false,true);
    conical_flask_shake.anchor.set(.5);
    conical_flask_shake.scale.set(1.4);
    conical_flask_shake.visible=false;



    conical_flask2=this.game.add.sprite(760,760, 'conical_flask_back');
    conical_flask2.anchor.set(.5);
    conical_flask2.scale.set(1.4);
    conical_flask2.state="flask";
    conical_flask2.visible=false;

    testube_tilt=this.game.add.sprite(955,562, 'testube_tilt','Testube0001.png');
    testube_tilt.animations.add('anim',[6,7,8,9,10,11,12,13,14],10,false,true);//0,1,2,3,4,5,
    testube_tilt.anchor.set(.5);
    testube_tilt.scale.set(.85);
    testube_tilt.visible=false;

    rackGroup=this.game.add.group();
    testube_rack_back=this.game.add.sprite(300,800, 'testube_rack_back');
    testube_rack_back.anchor.set(.5);
    testube_rack_back.scale.set(0.6);
    rackGroup.add(testube_rack_back);
     testube=this.game.add.sprite(432,715, 'testube_back');
     testube.anchor.set(.5);
     testube.scale.set(.85);
     rackGroup.add(testube);
    //  testube_f=this.game.add.sprite(0,0, 'testube');
    //  testube_f.anchor.set(.5);
     barium_testube=this.game.add.sprite(432,715, 'barium_testube','Testube.png');
     barium_testube.anchor.set(.5);
     barium_testube.scale.set(.85);

     barium_testube.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10,false,true);
     
     barium_testube.animations.add('water',[31,32,33,34,35,36,37,38,39,40,41,42,43,44],10,false,true);
     rackGroup.add(barium_testube);
     barium_testube_shake=this.game.add.sprite(432,715, 'barium_testube','Testube_shake0001.png');
     barium_testube_shake.animations.add('shake',[17,18,19,20,21,22,23,24,25,26,27,28,29,30],10,false,true);
     barium_testube_shake.anchor.set(.5);
     barium_testube_shake.scale.set(.85);
     barium_testube_shake.visible=false;
     rackGroup.add(barium_testube_shake) ;
     
     thread_testube=this.game.add.sprite(385,715, 'testube_with_tie');
     thread_testube.anchor.set(.5);
     thread_testube.scale.set(.85);
      thread_testube.visible=false;
      rackGroup.add(thread_testube);

    testube_rack_front=this.game.add.sprite(300,800, 'testube_rack_front');
    testube_rack_front.anchor.set(.5);
    testube_rack_front.scale.set(0.6);
    rackGroup.add(testube_rack_front);
    // flaskGroup=this.game.add.group();
    // flaskGroup.add(conical_flask);
    // flaskGroup.add(cork);
    // flaskGroup.add(thread_testube_4);
    // flaskGroup.add(thread_testube_3);
    // flaskGroup.add(conical_flask_f);
    // flaskGroup.add(conical_flask_water);
    // flaskGroup.add(conical_flask_shake);
    
    tie=this.game.add.sprite(250,970, 'tie');
    tie.anchor.set(.5);
    tie.events.onDragStart.add(function() {this.gameC(tie)}, this);
    tie.events.onDragStop.add(function() {this.gameC1(tie)}, this);
    this.game.physics.arcade.enable(tie);
    tie.xp=tie.x;
    tie.yp=tie.y;
    rackGroup.add(tie);
    
    plateGroup=this.game.add.group();
    beaker=this.game.add.sprite(1080,830, 'beaker_back');
    beaker.anchor.set(.5);
    beaker.scale.setTo(.7,.5);
    water_label=this.game.add.sprite(1080,950, 'water_label');
    water_label.anchor.set(.5);
    plateGroup.add(beaker);
    dropper=this.game.add.sprite(1080,800, 'drop_to_testube','Aluminium_Sulphate_Dropper0001.png');
    dropper.anchor.set(.5);
    dropper.scale.y=1.2;
    dropper.xp=1080;
    dropper.yp=800;
    dropper.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10,false,true);
    //dropper.scale.setTo(1.8,2.25);
    dropper.events.onDragStart.add(function() {this.gameC(dropper)}, this);
    dropper.events.onDragStop.add(function() {this.gameC1(dropper)}, this);
    this.game.physics.arcade.enable(dropper);
    plateGroup.add(dropper);
    
    beaker_f=this.game.add.sprite(1080,830, 'beaker_with_water');
    beaker_f.anchor.set(.5);
    beaker_f.scale.setTo(.7,.5);
    plateGroup.add(beaker_f);
    plateGroup.add(water_label);
    
    thread_testube_2=this.game.add.sprite(432,500, 'testube_with_tie_2');
     thread_testube_2.anchor.set(.5);
     thread_testube_2.scale.set(.85);
     thread_testube_2.visible=false;
     thread_testube_2.events.onDragStart.add(function() {this.gameC(thread_testube_2)}, this);
     thread_testube_2.events.onDragStop.add(function() {this.gameC1(thread_testube_2)}, this);
    this.game.physics.arcade.enable(thread_testube_2);
    

    cork1=this.game.add.sprite(600,885, 'cork');
    cork1.scale.set(.8);
    cork1.anchor.set(.5);
    cork1.state="toFlask";
    cork1.events.onDragStart.add(function() {this.gameC(cork1)}, this);
    cork1.events.onDragStop.add(function() {this.gameC1(cork1)}, this);
    this.game.physics.arcade.enable(cork1);

    plateGroup.add(barium_plate);
    plateGroup.add(barium_label);
    plateGroup.add(sodium_plate);
    plateGroup.add(sodium_label);

    spoon_barium=this.game.add.sprite(1350,890, 'spoon','Spoon0008.png');
    spoon_barium.animations.add('anim',[0,1,2,3,4,5,6,7],10,false,true);
    spoon_barium.scale.set(.9);
    spoon_barium.angle=30;
    spoon_barium.anchor.set(.5);
    spoon_barium.xp=spoon_barium.x;
    spoon_barium.yp=spoon_barium.y;
    spoon_barium.events.onDragStart.add(function() {this.gameC(spoon_barium)}, this);
    spoon_barium.events.onDragStop.add(function() {this.gameC1(spoon_barium)}, this);
    this.game.physics.arcade.enable(spoon_barium);
    plateGroup.add(spoon_barium);


    spoon_sodium=this.game.add.sprite(1650,890, 'spoon','Spoon0008.png');
    spoon_sodium.animations.add('anim',[0,1,2,3,4,5,6,7],10,false,true);
    spoon_sodium.scale.set(.9);
    spoon_sodium.anchor.set(.5);
    spoon_sodium.angle=30;
    spoon_sodium.xp=spoon_sodium.x;
    spoon_sodium.yp=spoon_sodium.y;
    spoon_sodium.events.onDragStart.add(function() {this.gameC(spoon_sodium)}, this);
    spoon_sodium.events.onDragStop.add(function() {this.gameC1(spoon_sodium)}, this);
    this.game.physics.arcade.enable(spoon_sodium);
    plateGroup.add(spoon_sodium);

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;

    blackBox = this.game.add.graphics(0, 0);
    blackBox.lineStyle(5,0xE67E22,.6);
    blackBox.beginFill(0x000000,.2);//9B59B6
    blackBox.drawRect(0, 0, 580, 300);
    blackBox.x=30;
    blackBox.y=460;
    blackBox.scale.y=0;
    weight_first=this.game.add.text(60,480,"Total mass of apparatus and",fontStyle);
    weight_first1=this.game.add.text(60,530,"       reactants = 233.2 g",fontStyle);
    //weight_first
    maskBox1 = this.game.add.graphics(0, 0);
    //maskBox1.lineStyle(5,0xE67E22,.6);
    maskBox1.beginFill(0x000000);//9B59B6
    maskBox1.drawRect(0, 0, 500, 60);
    maskBox1.scale.x=0.01;
    maskBox1.alpha=0;
    maskBox1.x=50;
    maskBox1.y=478;
    weight_first.mask=maskBox1;
    //tween1=this.game.add.tween(maskBox1.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
    maskBox2 = this.game.add.graphics(0, 0);
    //maskBox2.lineStyle(5,0xE67E22,.6);
    maskBox2.beginFill(0x000000);//9B59B6
    maskBox2.drawRect(0, 0, 340, 60);
    maskBox2.scale.x=0.01;
    maskBox2.alpha=0;
    maskBox2.x=135;
    maskBox2.y=530;
    weight_first1.mask=maskBox2;
    //tween2=this.game.add.tween(maskBox2.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
    weight_second=this.game.add.text(60,630,"Total mass of apparatus and ",fontStyle);
    weight_second1=this.game.add.text(60,680,"       products = 233.2 g",fontStyle);
    maskBox3 = this.game.add.graphics(0, 0);
    //maskBox1.lineStyle(5,0xE67E22,.6);
    maskBox3.beginFill(0x000000);//9B59B6
    maskBox3.drawRect(0, 0, 500, 60);
    maskBox3.scale.x=0.01;
    maskBox3.alpha=0;
    maskBox3.x=50;
    maskBox3.y=630;
    weight_second.mask=maskBox3;
    //tween1=this.game.add.tween(maskBox3.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
    maskBox4 = this.game.add.graphics(0, 0);
    //maskBox2.lineStyle(5,0xE67E22,.6);
    maskBox4.beginFill(0x000000);//9B59B6
    maskBox4.drawRect(0, 0, 340, 60);
    maskBox4.scale.x=0.01;
    maskBox4.alpha=0;
    maskBox4.x=135;
    maskBox4.y=680;
    weight_second1.mask=maskBox4;
    //tween2=this.game.add.tween(maskBox4.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);


    weight_text=this.game.add.text(1150,915,"00.00",fontStyle_b); 
    weight_text.alpha=.6;
    weight_text.visible=false;
    dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
    dia_box.scale.setTo(.8,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(60,50,dia_text,fontStyle);
    
    dialog.y=60;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1",1);
    voice.play();
    dialog.text="Now prepare some barium chloride solution.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.prepareBariumChloride, this); 
    
    ///////////////////////shortcut////////////////////
    // arrow.visible=false;
    //   cork1.visible=false;
    //   cork.visible=true;
    //   voice.destroy(true);
    //     voice=this.game.add.audio("step1_1",1);
    //     //voice.play();
    //     dialog.text="Take a weighting machine.";
    //     tween1=this.game.add.tween(plateGroup).to( { x:2400}, 800, Phaser.Easing.Out, true);
    //     tween1.onComplete.add(function () {
    //       tween2=this.game.add.tween(digital_weight).to( { x:1200}, 800, Phaser.Easing.Out, true);
    //       tween2.onComplete.add(function () {
    //         voice.destroy(true);
    //         voice=this.game.add.audio("step1_1",1);
    //         //voice.play();
    //         dialog.text="Drag the conical flask to the weighing machine.";
    //         conical_flask.state="toWeighing";
    //         conical_flask.inputEnabled = true;
    //         conical_flask.useHandCursor = true;
    //         conical_flask.input.enableDrag(true);
    //         collider.x=digital_weight.x;
    //         collider.y=digital_weight.y-50;
    //         arrow.visible=true;
    //       }.bind(this));  
    //     }.bind(this)); 
  },
  prepareBariumChloride:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2",1);
    voice.play();
    dialog.text="Add a pinch of barium chloride to the test tube.";
    arrow.x=spoon_barium.x;
    arrow.y=spoon_barium.y-60;
    spoon_barium.inputEnabled = true;
    spoon_barium.input.useHandCursor = true;
    spoon_barium.input.enableDrag(true);
    collider.x=testube.x;
    collider.y=testube.y-150;
    arrow.visible=true;
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    arrow.visible=false;
    if(obj==spoon_barium){
      obj.frame=0;
      obj.angle=0;
      //spoon1_1.visible=false;
      arrow.x=collider.x;
      arrow.y=collider.y-30;//370
      
      arrow.visible=true;
    }else if(obj==spoon_sodium){
      obj.frame=0;
      obj.angle=0;
      //spoon1_1.visible=false;
      arrow.x=collider.x;
      arrow.y=collider.y-30;//370
      
      arrow.visible=true;
    }else if(obj==dropper||obj==thread_testube || obj==tie || obj==cork1 || obj==conical_flask){
      arrow.x=collider.x;
      arrow.y=collider.y-30;//370
      arrow.visible=true;
      if(obj==cork1 && cork1.state=="fromFlask"){
        cork1.alpha=1;
        cork.visible=false;
      }
      if(obj==conical_flask && conical_flask.state=="fromWeighing"){
        weight_text.text="00.00";
      }
    }else if(obj==thread_testube_2){
      thread_testube.visible=false;
      testube.visible=false;
      barium_testube.visible=false;
      thread_testube_2.alpha=1;
      arrow.x=collider.x;
      arrow.y=collider.y-30;//370
      arrow.visible=true;
      
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
    
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==spoon_barium){
      spoon_barium.x=520;
      spoon_barium.y=487;
      spoon_barium.animations.play('anim');
      barium_testube.animations.play('anim');
      barium_testube.animations.currentAnim.onComplete.add(function(){
          spoon_barium.reset(spoon_barium.xp,spoon_barium.yp);
          spoon_barium.frameName='Spoon0008.png';
          spoon_barium.angle=30;
          voice.destroy(true);
          voice=this.game.add.audio("step3",1);
          voice.play();
          dialog.text="Add 5 ml of water to the test tube.";
          dropperCount++;
          arrow.x=dropper.x;
          arrow.y=dropper.y-dropper.height/2;
          dropper.inputEnabled = true;
          dropper.input.useHandCursor = true;
          dropper.input.enableDrag(true);
          arrow.visible=true;
      }.bind(this));
      arrow.visible=false;
    }else if(currentobj==spoon_sodium){
      spoon_sodium.x=840;
      spoon_sodium.y=515;
      spoon_sodium.animations.play('anim');
      conical_flask_f.animations.play('anim');
      conical_flask_f.animations.currentAnim.onComplete.add(function(){
        spoon_sodium.reset(spoon_sodium.xp,spoon_sodium.yp);
        spoon_sodium.frameName='Spoon0008.png';
        spoon_sodium.angle=30;
          voice.destroy(true);
          voice=this.game.add.audio("step7",1);
          voice.play();
          dialog.text="Pour 10 ml of water into the conical flask.";
          arrow.x=dropper.x;
          dropperCount++;
          arrow.y=dropper.y-dropper.height/2;
          dropper.inputEnabled = true;
          dropper.input.useHandCursor = true;
          dropper.input.enableDrag(true);
          arrow.visible=true;
      }.bind(this));
      arrow.visible=false;
    }else if(currentobj==dropper){
      if(dropperCount==1){
        dropper.x=collider.x;
        dropper.y=collider.y-100;
        arrow.visible=false;
        dropper.animations.play('anim');
        barium_testube.animations.play('water');
        barium_testube.animations.currentAnim.onComplete.add(function(){
          voice.destroy(true);
          voice=this.game.add.audio("step4",1);
          voice.play();
          dialog.text="Click on the test tube to shake it.";
          arrow.visible=true;
          dropper.reset(dropper.xp,dropper.yp);
          barium_testube.inputEnabled = true;
          barium_testube.input.useHandCursor = true;
          barium_testube.events.onInputDown.add(this.shakeTestTube, this);
        }.bind(this));
      }else if(dropperCount==2){
        dropper.x=collider.x;
        dropper.y=collider.y-100;
        arrow.visible=false;
        dropper.animations.play('anim');
        conical_flask_water.visible=true;
        conical_flask_f.visible=false;
        conical_flask_water.animations.play('anim');
        conical_flask_water.animations.currentAnim.onComplete.add(function(){
          // voice.destroy(true);
          // voice=this.game.add.audio("step1_1",1);
          // voice.play();
          // dialog.text="Shake the test tube.";
          dropper.reset(dropper.xp,dropper.yp);
          arrow.x=dropper.x;
          dropperCount++;
          arrow.y=dropper.y-dropper.height/2;
          arrow.visible=true;
          dropper.inputEnabled = true;
          dropper.input.useHandCursor = true;
          dropper.input.enableDrag(true);
          
        }.bind(this));
      }else if(dropperCount==3){
        dropper.x=collider.x;
        dropper.y=collider.y-100;
        arrow.visible=false;
        dropper.animations.play('anim');
        conical_flask_water.animations.play('anim2');
        conical_flask_water.animations.currentAnim.onComplete.add(function(){
          voice.destroy(true);
          voice=this.game.add.audio("step8",1);
          voice.play();
          dialog.text="Click on the conical flask to shake it.";
          dropper.reset(dropper.xp,dropper.yp);
          conical_flask.state="toShake";
          arrow.visible=true;
          conical_flask.inputEnabled = true;
          conical_flask.input.useHandCursor = true;
          conical_flask.events.onInputDown.add(this.shakeConicalFlask, this);
          
        }.bind(this));
      }
    }else if(currentobj==tie){
      tie.reset(tie.xp,tie.yp);
      //arrow.visible=false;
      testube.visible=false;
      barium_testube_shake.visible=false;
      thread_testube.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step10",1);
      voice.play();
      dialog.text="Carefully hang the test tube into the flask.";
      thread_testube_2.inputEnabled = true;
      thread_testube_2.input.useHandCursor = true;
      thread_testube_2.input.enableDrag(true);
      thread_testube_2.alpha=0;
      thread_testube_2.visible=true;
      collider.x=conical_flask.x;
      collider.y=conical_flask.y-200;
    }else if(currentobj==thread_testube_2){
      arrow.visible=false;
      thread_testube_2.visible=false;
      thread_testube_3.visible=true;
      tween1=this.game.add.tween(thread_testube_3).to( { y:-120}, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        thread_testube_3.visible=false;
        thread_testube_4.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step11",1);
        voice.play();
        dialog.text="Cover the conical flask with a cork.";
        cork1.inputEnabled = true;
        cork1.input.useHandCursor = true;
        cork1.input.enableDrag(true);
        arrow.x=cork1.x;
        arrow.y=cork1.y-cork1.height/2;
        arrow.visible=true;
      }.bind(this));
    }else if(currentobj==cork1){
      arrow.visible=false;
      if(cork1.state=="toFlask"){
        cork1.visible=false;
        cork.visible=true;
        voice.destroy(true);
          voice=this.game.add.audio("step12",1);
          voice.play();
          dialog.text="Take a weighing machine.";
          tween0=this.game.add.tween(rackGroup).to( { x:-1400}, 800, Phaser.Easing.Out, true);
          tween1=this.game.add.tween(plateGroup).to( { x:2400}, 800, Phaser.Easing.Out, true);
          tween1.onComplete.add(function () {
            tween2=this.game.add.tween(digital_weight).to( { x:1200}, 800, Phaser.Easing.Out, true);
            tween2.onComplete.add(function () {
              weight_text.visible=true;
              voice.destroy(true);
              voice=this.game.add.audio("step13",1);
              voice.play();
              dialog.text="Drag the conical flask to the weighing machine.";
              conical_flask.state="toWeighing";
              conical_flask.inputEnabled = true;
              conical_flask.useHandCursor = true;
              conical_flask.input.enableDrag(true);
              collider.x=digital_weight.x;
              collider.y=digital_weight.y-50;
              arrow.visible=true;
              
            }.bind(this));  
          }.bind(this));  
      }else{
          cork1.x=600;
          cork1.y=885;
          voice.destroy(true);
          voice=this.game.add.audio("step17",1);
          voice.play();
          dialog.y=40;
          dialog.text="Now mix the barium chloride solution with the sodium sulphate \nsolution.";
          conical_flask2.state="tilt";
          conical_flask2.inputEnabled = true;
          conical_flask2.input.useHandCursor = true;
          conical_flask2.events.onInputDown.add(this.shakeConicalFlask2, this);
          conical_flask2.alpha=0;
          conical_flask2.visible=true;
          arrow.x=conical_flask.x;
          arrow.y=conical_flask.y-conical_flask.height/2;
          arrow.visible=true;
          
        }
    }else if(currentobj==conical_flask){
      arrow.visible=false;
      console.log(conical_flask.state);
      if(conical_flask.state=="toWeighing" || conical_flask.state=="toWeighing2"){
        conical_flask.x=digital_weight.x;
        conical_flask.y=digital_weight.y-digital_weight.height;
        weight_text.text=103.55;
        initial_weight=103.55;
        delay=0;
        incr=150.15;
        weightFlag=true;
        if(conical_flask.state=="toWeighing"){
          tween1=this.game.add.tween(blackBox.scale).to( { y:1}, 800, Phaser.Easing.Out, true);
          tween1.onComplete.add(function () {
            tween2=this.game.add.tween(maskBox1.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
            tween2.onComplete.add(function () {
              tween3=this.game.add.tween(maskBox2.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
            }.bind(this));  
          }.bind(this));  
        }else {
            tween1=this.game.add.tween(maskBox3.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
            tween1.onComplete.add(function () {
              tween2=this.game.add.tween(maskBox4.scale).to( { x:1}, 2000, Phaser.Easing.Out, true);
            }.bind(this));  
          
        }
      }else{
        conical_flask.x=760;
        conical_flask.y=760;
        voice.destroy(true);
        voice=this.game.add.audio("step16",1);
        voice.play();
        dialog.text="Remove the cork.";
        cork1.state="fromFlask";
        //cork1.alpha=0;
        cork1.x=conical_flask.x-10;
        cork1.alpha=0;
        cork1.y=(conical_flask.y-conical_flask.height/2)+60;
        cork1.xp=cork1.x;
        cork1.yp=cork1.y;
        cork1.visible=true;
        cork1.inputEnabled = true;
        cork1.input.useHandCursor = true;
        cork1.input.enableDrag(true);
        arrow.x=cork1.x;
        arrow.y=cork1.y-cork1.height/2;
        arrow.visible=true;
        collider.x=600;
        collider.y=885;
      }
    }
    
  },
  shakeConicalFlask2:function(){
    arrow.visible=false;
    console.log("/////"+conical_flask.state);
    conical_flask2.visible=false;
    thread_testube_3.y=-120;
    thread_testube_3.visible=true;
    thread_testube_4.visible=false;
    tween1=this.game.add.tween(thread_testube_3).to( { y:-350}, 400, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        thread_testube_3.visible=false;
        testube_tilt.visible=true;
        testube_tilt.animations.play('anim');
        testube_to_flask.visible=true;
        testube_to_flask.animations.play('anim');
        testube_to_flask.animations.currentAnim.onComplete.add(function(){
          thread_testube_3_1.visible=true;
          testube_tilt.visible=false;
          tween3=this.game.add.tween(thread_testube_3_1).to( { y:-120}, 400, Phaser.Easing.Out, true);
          tween3.onComplete.add(function () {
            thread_testube_3_1.visible=false;
            thread_testube_4_1.visible=true;
            cork.visible=true;
            cork1.visible=false;
            voice.destroy(true);
                voice=this.game.add.audio("step18",1);
                voice.play();
                dialog.y=60;
                dialog.text="Weigh the contents of the flask again.";
                conical_flask.state="toWeighing2";
                conical_flask.inputEnabled = true;
                conical_flask.useHandCursor = true;
                conical_flask.input.enableDrag(true);
                conical_flask.xp=760;
                conical_flask.yp=760;
                collider.x=digital_weight.x;
                collider.y=digital_weight.y-50;
                arrow.visible=true;
          }.bind(this));  
        }.bind(this));
      }.bind(this));
  },
  shakeConicalFlask:function(){//9745127997
    
    
    if(conical_flask.state=="toShake"){
      arrow.visible=false;
      conical_flask.state="flask";
      conical_flask_water.visible=false;
      conical_flask_shake.visible=true;
      conical_flask.visible=false;
      tween1=this.game.add.tween(conical_flask_shake).to( { y:400}, 400, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          conical_flask_shake.animations.play('anim');
          //;
          conical_flask_shake.animations.currentAnim.onComplete.add(function(){
            tween2=this.game.add.tween(conical_flask_shake).to( { y:760}, 400, Phaser.Easing.Out, true);
              tween2.onComplete.add(function () {
                conical_flask.visible=true;
                conical_flask_shake.visible=false;
                testube_to_flask.visible=true;
                // conical_flask_water.frameName="Water_to_Flask0025.png";
                // conical_flask_water.x=-10;
                // conical_flask_water.y=5;
                // conical_flask_water.visible=true;
                voice.destroy(true);
                voice=this.game.add.audio("step9",1);
                voice.play();
                dialog.text="Tie the test tube with a thread.";
                
                arrow.x=tie.x;
                arrow.y=tie.y-tie.height/2;
                arrow.visible=true;
                tie.inputEnabled = true;
                tie.input.useHandCursor = true;
                tie.input.enableDrag(true);
                collider.x=testube.x;
                collider.y=testube.y-150;
              }.bind(this));
          }.bind(this));  
        }.bind(this));
      }
  },
  shakeTestTube:function(){
    arrow.visible=false;
    testube.visible=false;
    barium_testube.visible=false;
    barium_testube_shake.visible=true;
    tween1=this.game.add.tween(barium_testube_shake).to( { y:400}, 400, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        barium_testube_shake.animations.play('shake');
        barium_testube_shake.animations.currentAnim.onComplete.add(function(){
          tween2=this.game.add.tween(barium_testube_shake).to( { y:710}, 400, Phaser.Easing.Out, true);
            tween2.onComplete.add(function () {
              voice.destroy(true);
              voice=this.game.add.audio("step5",1);
              voice.play();
              dialog.text="Good job...Now, lets prepare some sodium sulphate solution.";
              this.game.time.events.add(Phaser.Timer.SECOND*4,this.prepareSodiumSulphate, this); 
              
            }.bind(this));
        }.bind(this));  
      }.bind(this));
  },
  prepareSodiumSulphate:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step6",1);
    voice.play();
    dialog.text="Add a pinch of sodium sulphate to the conical flask.";
    arrow.x=spoon_sodium.x;
    arrow.y=spoon_sodium.y-60;
    arrow.visible=true;
    spoon_sodium.inputEnabled = true;
    spoon_sodium.input.useHandCursor = true;
    spoon_sodium.input.enableDrag(true);
    collider.x=conical_flask.x;
    collider.y=conical_flask.y-200;
  },
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    if(weightFlag){
      delay++;
      if(delay>=15){
        if(incr>100){
          initial_weight+=incr;
          weight_text.text=initial_weight;
          incr=10.25;
        }else{
          if(initial_weight>240){
            initial_weight-=incr;
            weight_text.text=initial_weight;
          }else{
            weightFlag=false;
            voice.destroy(true);
            if(conical_flask.state=="toWeighing2"){
              voice=this.game.add.audio("step19",1);
              dialog.text="Observe that the weight of the content is the same as the first.";
              this.game.time.events.add(Phaser.Timer.SECOND*4,this.NextExperiment, this); 
            }else{
              voice=this.game.add.audio("step14",1);
              dialog.text="Carefully weigh the flask and all the contents of the flask.";
              printFlag1=true;
              //weight_first=this.game.add.text(60,480,"Total mass of apparatus and reactants = 233.2 g",fontStyle);
              this.game.time.events.add(Phaser.Timer.SECOND*5,this.startMixing, this); 
            }
            voice.play();
          }
        }
        delay=0;
      }
      
     }
	},
  startMixing:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step15",1);
    voice.play();
    dialog.text="Remove the flask from the weighing balance.";
    collider.x=760;
    collider.y=760;
    arrow.x=digital_weight.x;
    arrow.y=digital_weight.y-digital_weight.height/2;
    arrow.visible=true;
    conical_flask.inputEnabled = true;
    conical_flask.useHandCursor = true;
    conical_flask.input.enableDrag(true);
    conical_flask.xp=conical_flask.x;
    conical_flask.yp=conical_flask.y;
    conical_flask.state="fromWeighing";
  },
  
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==spoon_barium){
        spoon_barium.angle=30;
        arrow.x=spoon_barium.x;
        arrow.y=spoon_barium.y-60;//325
        
      }else if(currentobj==spoon_sodium){
        spoon_sodium.angle=30;
        arrow.x=spoon_sodium.x;
        arrow.y=spoon_sodium.y-60;//325
        
      }else if(currentobj==dropper){
        arrow.x=dropper.x;
        arrow.y=dropper.y-dropper.height/2;//325
        
      }else if(currentobj==thread_testube){
        arrow.x=testube.x;
        arrow.y=testube.y-testube.height/2;//325
        
      }else if(currentobj==tie){
        arrow.x=tie.x;
        arrow.y=tie.y-tie.height/2;//325
        
      }else if(currentobj==thread_testube_2){
        thread_testube.visible=true;
        thread_testube_2.alpha=0;
        arrow.x=testube.x;
        arrow.y=testube.y-testube.height/2;//325
      }else if(currentobj==cork1){
        arrow.x=cork1.x;
        arrow.y=cork1.y-cork1.height/2;//325
        if(cork1.state=="fromFlask"){
          cork1.alpha=0;
          cork.visible=true;
        }
      }else if(currentobj==conical_flask){
        arrow.x=conical_flask.x;
        arrow.y=conical_flask.y-conical_flask.height/2;//325
        if(conical_flask.state=="fromWeighing"){
          weight_text.text="233.2";
        }
      }
      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
	NextExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step20",1);
    voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
