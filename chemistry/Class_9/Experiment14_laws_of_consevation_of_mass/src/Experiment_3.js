var experiment_3 = function(game){

var inst;



var delay;
var incr;
var currentobj;
var arrow_y;
var mix_count;
var spoon3;
var bowl;
var interval;
var heatFlag;
var heatFlag2;
var magnetFlag;
var glass_rod1;
var content1;
var magnet1;
var magnet2;
var spoon4;
var spoon5;
var shakeFlag1;
var shakeFlag2;
var shakingFlag1;
var shakingFlag2;
var shakingCount1;
var shakingCount2;
var mixFlag;
var stickFlag;
var stick;
var texttube3;
var texttube4;
}


experiment_3.prototype = {
  	create: function(){
      fontstyle_1={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_2={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_3={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_4={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_5={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_6={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_7={ font: "40px Segoe UI", fill: "#FFA500", align: "left" };
hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');
interval=0;
heatFlag=false;
heatFlag2=false;
magnetFlag=false;
currentobj=null;
glass_rod1=null;
magnet1=null;
magnet2=null;
spoon4=null;
spoon5=null;
shakeFlag1=false;
shakeFlag2=false;
shakingFlag1=false;
shakingFlag2=false;
shakingCount1=0;
shakingCount2=0;
mixFlag=false;
hclFlag=false;
compHeatFlag=false;
mixHeatFlag=false;
stickFlag=false;
stick=null;
texttube3=null;
texttube4=null;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/


spoon3=null;
bowl=null;
mix_count=0;

voice=this.game.add.audio("step0",1);
//voice.play();

this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Procedure_4", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  nextSound3:function(){
    voice3.play();
  },
  addItems:function(){
    
      
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
    dia_box.scale.setTo(.8,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(60,50,dia_text,fontstyle_2);
    collider=this.game.add.sprite(540,770, 'collider');//y-320
    dialog.y=60;
    this.game.physics.arcade.enable(collider);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.0;

    


    disulphide=this.game.add.sprite(250,750, 'compounds', 'disulphide.png');//y-600
    droper1=this.game.add.sprite(290,720, 'compounds', 'droper1.png');//y-570
    droper1.xp=290;
    droper1.yp=725;//y-570
    droper2=this.game.add.sprite(290,725, 'compounds', 'droper1.png');//y-575
    droper2.xp=290;
    droper2.yp=725;//y-575
    droper2.visible=false;
    





    //plate=this.game.add.sprite(800,720, 'compounds', 'plate.png');//y-320
    plate1=this.game.add.sprite(500,720, 'compounds', 'plate.png');//y-320
    plate1.frameName="black_mass.png";
    plate1.y=850;//450
    compound_lable=this.game.add.sprite(530,910,'compounds', 'compound_lable.png');//500
    mixture=this.game.add.sprite(790,840, 'compounds', 'mixture.png');//y-440
    //mixture.visible=false;
    
    mix=this.game.add.sprite(780,570, 'mixing', 'mix0001.png');//y-170
    mix.scale.setTo(.61,.61);
    mix.visible=false;
    mix.animations.add('anim',[
            'mix0002.png',
            'mix0003.png',
            'mix0004.png',
            'mix0005.png',
            'mix0006.png',
            'mix0007.png',
            'mix0008.png',
            
             ], 10, true, false);




    //ironFillings=this.game.add.sprite(1320,850, 'compounds', 'iron.png');//y-450, x-1370
    mixture_label=this.game.add.sprite(825,910, 'compounds', 'mixture_label.png');//y-510
    /**/
    ////////////////////////////////////////////////////////////


    ///////////////////////////////////////////////////////////
    stand1=this.game.add.sprite(1150,700,'carbonMix','hold1.png');//y300
        texttube1=this.game.add.sprite(1255,625,'carbonMix','mix_compound0001.png');//y225
        texttube2=this.game.add.sprite(1380,617,'carbonMix','mix_mixture0001.png');//y217
        texttube1.anchor.setTo(.5,0);
        texttube2.anchor.setTo(.5,0);
        stand2=this.game.add.sprite(1150,700,'carbonMix','hold2.png');//y300
        
        mix.visible=false;
        mixture.visible=true;

        spoon4_1=this.game.add.sprite(650,870, 'Spoon_M');//y-470
        spoon4_1.angle=-20;   
        spoon4_1.anchor.setTo(.5,.5);        
        spoon4_1.scale.setTo(.5,.8);
        spoon5_1=this.game.add.sprite(950,870, 'Spoon_M');//y-470
        spoon5_1.angle=-20;   
        spoon5_1.anchor.setTo(.5,.5);        
        spoon5_1.scale.setTo(.5,.8); 

        
        spoon4=this.game.add.sprite(650,870, 'spoon', 'spoon_compound.png');//y-470
        spoon4.angle=-20;
        spoon4.xp=650;
        spoon4.nam="spoon";
        spoon4.yp=870;//470
        spoon4.events.onDragStart.add(function() {this.gameC(spoon4)}, this);
        spoon4.events.onDragStop.add(function() {this.gameC1(spoon4)}, this);
        spoon4.scale.setTo(.5,.8);
        spoon4.anchor.setTo(.5,.5);
        spoon4.alpha=0;
        
        spoon5=this.game.add.sprite(950,870, 'spoon', 'spoon_compound.png');//y-470
        spoon5.angle=-20;
        spoon5.xp=950;
        spoon5.yp=870;//470
        spoon5.nam="spoon";
        spoon5.scale.setTo(.5,.8);
        spoon5.anchor.setTo(.5,.5);
        this.game.physics.arcade.enable(spoon5);
        spoon5.enableBody =true;
        spoon5.alpha=0;
        collider.x=1235;
        collider.y=500;//100
        collider.scale.setTo(1,3);
        //collider.alpha=0;
        
    
    
    
    
    // burner=this.game.add.sprite(30,600, 'compounds', 'burner0001.png');

    // burner.animations.add('anim',['burner0001.png',
    //         'burner0001.png',
    //          ], 10, true, false);
   
    
    arrow=this.game.add.sprite(1200,750, 'arrow');//y-350
    arrow.angle=90;
    
        arrow.y=750;//350
        arrow_y=750;
        
    arrow.visible=false;
    //mixture_label.scale.setTo(1.5,1.5);
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_1",1);
    voice.play();
    dialog.text="Add a small amount of compound into a test tube.";
    //dialog.text="Take two test tubes and add small amount of compound into one test \ntube and add small amount of mixture into another test tube.";
    spoon4.inputEnabled = true;
        spoon4.input.useHandCursor = true;
        spoon4.events.onInputDown.add(this.ClickOnSpoon, this);
        spoon4.input.enableDrag(true);
        this.game.physics.arcade.enable(spoon4);
        spoon4.enableBody =true;
        arrow.x=630;
        arrow.y=770;//370
        arrow_y=770;
        arrow.visible=true;
  },
  ClickOnSpoon:function(){

  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    arrow.visible=false;
    if(spoon4!=null && obj==spoon4){
      obj.frame=1;
      obj.alpha=1;
      spoon4_1.visible=false;
      arrow.x=1270;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
    }else if(spoon5!=null && obj==spoon5){
      arrow.x=1390;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
      obj.frame=3;
      obj.alpha=1;
      spoon5_1.visible=false;
    }else if(obj==droper1){
      arrow.x=1270;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
    }else if(obj==droper2){
      arrow.x=1390;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj.nam=="spoon"){
      currentobj.angle=-20;
      currentobj.reset(currentobj.xp,currentobj.yp);//
      currentobj.frame=0;
      //currentobj.scale.setTo(1,1);
    }
   
    
    if(currentobj==spoon4){
      if(hclFlag){
        texttube3.frameName="testtubec0001.png";
      }else{
        texttube1.frameName="mix_compound0002.png";
      }
      spoon4.visible=false;
      spoon4_1.visible=true;
      arrow.x=880;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
      spoon5.events.onDragStart.add(function() {this.gameC(spoon5)}, this);
      spoon5.events.onDragStop.add(function() {this.gameC1(spoon5)}, this);
      spoon5.inputEnabled = true;
      spoon5.input.useHandCursor = true;
      spoon5.events.onInputDown.add(this.ClickOnSpoon, this);
      spoon5.input.enableDrag(true);
      this.game.physics.arcade.enable(spoon5);
      spoon5.enableBody=true;
      collider.x=1360;
      voice.destroy(true);
      voice=this.game.add.audio("step3_2",1);
      voice.play();
      dialog.text="Add a small amount of mixture into another test tube.";
    
    }else if(currentobj==spoon5){
      spoon5.visible=false;
      spoon5_1.visible=true;
      collider.x=1235;
      
      arrow.x=330;
      arrow.y=600;
      arrow_y=600;
      voice.destroy(true);
      voice=this.game.add.audio("step3_3",1);
      voice.play();
        dialog.text="Add a few drops of carbon disulphide into the test tubes.";
        texttube2.frameName="mix_mixture0002.png";
        droper1.events.onDragStart.add(function() {this.gameC(droper1)}, this);
        droper1.events.onDragStop.add(function() {this.gameC1(droper1)}, this);
        droper1.inputEnabled = true;
        droper1.input.useHandCursor = true;
        droper1.events.onInputDown.add(this.ClickOnSpoon, this);
        droper1.input.enableDrag(true);
        this.game.physics.arcade.enable(droper1);
        droper1.enableBody=true;
      
      arrow.visible=true;
      
      
    }else if(currentobj==droper1){
      texttube1.frameName="mix_compound0003.png";
      currentobj.reset(currentobj.xp,currentobj.yp);
      droper1.visible=false;
      droper2.visible=true;
      arrow.x=330;
      arrow.y=600;
      arrow_y=600;
      arrow.visible=true;
      droper2.events.onDragStart.add(function() {this.gameC(droper2)}, this);
      droper2.events.onDragStop.add(function() {this.gameC1(droper2)}, this);
      droper2.inputEnabled = true;
      droper2.input.useHandCursor = true;
      droper2.events.onInputDown.add(this.ClickOnSpoon, this);
      droper2.input.enableDrag(true);
      this.game.physics.arcade.enable(droper2);
      droper2.enableBody=true;
      collider.x=1360;
    }else if(currentobj==droper2){
      texttube2.frameName="mix_mixture0003.png";
      currentobj.reset(currentobj.xp,currentobj.yp);
      arrow.x=1270;
      arrow.y=540;
      arrow_y=540;
      arrow.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_4",1);
      voice.play();
      dialog.text="Stir well the test tube of compound.";      
      texttube1.inputEnabled = true;
      texttube1.input.useHandCursor = true;
      texttube1.events.onInputDown.add(this.ClickOntexttube1, this);
    }
  },
  
  ClickOntexttube1:function(){
    shakeFlag1=true;
    arrow.visible=false;
    /*arrow.x=1390;
    arrow.y=140;
    arrow_y=140;
    arrow.visible=true;
    texttube2.inputEnabled = true;
    texttube2.input.useHandCursor = true;
    texttube2.events.onInputDown.add(this.ClickOntexttube2, this);
    texttube1.inputEnabled = false;*/
  },
  ClickOntexttube2:function(){
    shakeFlag2=true;
    arrow.visible=false;
    texttube2.inputEnabled = false;
  },
  
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	addList:function(){
    

  },
  ClickOnPlay:function(){

    
  },
  
  ClickOnSlow2:function(){

  },
  


  hotsec:function()
  {

    //voice41.destroy();
    //voice5.play();

  },
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(shakeFlag1){
      texttube1.y=420;
      shakingFlag1=true;
      shakeFlag1=false;
      texttube1.animations.add('anim',[
            'mix_compound0003.png',
            'mix_compound0004.png',
            'mix_compound0005.png',
            'mix_compound0006.png',
            'mix_compound0007.png',
            'mix_compound0008.png',
            
             ], 3, true, false);
      texttube1.play("anim",false);
    }
    if(shakingFlag1){
      shakingCount1++;
      texttube1.angle-=1;
      if(texttube1.angle<-5)
        texttube1.angle=5;
      if(shakingCount1>160){
        shakingFlag1=false;
        texttube1.animations.stop(null, true);
        texttube1.frameName="mix_compound0008.png";
        texttube1.angle=0;
        texttube1.y=625;
        // = this.game.add.sprite(420,550,'popup');
        //popup.scale.setTo(.4,.3);
        //popup.visible=false;
        voice.destroy(true);
        voice=this.game.add.audio("step3_5",1);
        voice.play();
        dialog.y=40;
        dialog.text="Observe that there are no changes, because in compound state \nsulphur loses its original properties."
        // content1=this.game.add.text(470,700,dia_text,fontstyle_2);
        // play = this.game.add.sprite(1230,920,'components','play_pressed.png');
        // play.scale.setTo(.7,.7);
        // play.inputEnabled = true;
        // //play.input.priorityID = 3;
        // play.input.useHandCursor = true;
        // play.events.onInputDown.add(this.toContinue02, this);
        this.game.time.events.add(Phaser.Timer.SECOND*7,this.toContinue02, this);
      }
    }
    if(shakeFlag2){
      texttube2.y=420;
      shakingFlag2=true;
      shakeFlag2=false;
      texttube2.animations.add('anim',[
            'mix_mixture0003.png',
            'mix_mixture0004.png',
            'mix_mixture0005.png',
            'mix_mixture0006.png',
            'mix_mixture0007.png',
            'mix_mixture0008.png',
            'mix_mixture0009.png',
            'mix_mixture0010.png',
            'mix_mixture0011.png',
            'mix_mixture0012.png',
            'mix_mixture0013.png',
            ], 3, true, false);
      texttube2.play("anim",false);
    }
    if(shakingFlag2){
      shakingCount2++;
      texttube2.angle-=1;
      if(texttube2.angle<-5)
        texttube2.angle=5;
      if(shakingCount2>160){
        shakingFlag2=false;
        texttube2.animations.stop(null, true);
        texttube2.frameName="mix_mixture0013.png";
        texttube2.angle=0;
        texttube2.y=615;
        ///////////////////////////////
        //ironFillings.visible=false;
        //sulphur.visible=false;
        //spoon1.visible=false;
        //spoon2.visible=false;
        //mixture.visible=true;
        
        arrow.visible=false;
        plate1.frameName="black_mass.png";
        plate1.x=500;
        ////////////////////////////plate1.y=450;
        //////////////////////////
        arrow.visible=false;
        //popup = this.game.add.sprite(420,550,'popup');
        //popup.scale.setTo(.4,.3);
        //popup.visible=false;
        voice.destroy(true);
        voice=this.game.add.audio("step3_7",1);
        voice.play();
        //dialog.y=40;
        //dialog.text="Observe that sulphur first dissolves then after some time it reappears. "
        dialog.text="Observe that sulphur dissolves while black iron particles settle down. ";
        // content1=this.game.add.text(470,700,dia_text,fontstyle_2);
        // play = this.game.add.sprite(1230,920,'components','play_pressed.png');
        // play.scale.setTo(.7,.7);
        // play.inputEnabled = true;
        // //play.input.priorityID = 3;
        // play.input.useHandCursor = true;
        // play.events.onInputDown.add(this.toContinue2, this);
        // hclFlag=true;
        this.game.time.events.add(Phaser.Timer.SECOND*5,this.NextExperiment, this);
      }
    }
    
    
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    /////////////////////////////
    
    
	},
  NextExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_8",1);
    voice.play();
    dialog.text="Click on the next button to see the effect of heat.";
    play.visible=true;
  },
  toContinue02:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_6",1);
    voice.play();
    dialog.y=70;
    dialog.text="Stir well the test tube of mixture."; 
    arrow.x=1390;
    arrow.y=540;
    arrow_y=540;
    arrow.visible=true;
    texttube2.inputEnabled = true;
    texttube2.input.useHandCursor = true;
    texttube2.events.onInputDown.add(this.ClickOntexttube2, this);
    texttube1.inputEnabled = false;
    //dialog.text="Now show the effect of heat"; 
    //play.visible=true;
    
  },
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj.nam=="spoon"){
        currentobj.angle=-30;
        //currentobj.frame=0;
        //currentobj.scale.setTo(1,1);
      }

      if(currentobj==spoon4){
        arrow.x=630;
        arrow.y=770;//370
        arrow_y=770;
        arrow.visible=true;
        currentobj.alpha=0;
        spoon4_1.visible=true;
      }
      
      if(currentobj==spoon5){
        arrow.x=880;
        arrow.y=770;//370
        arrow_y=770;
        arrow.visible=true;
        currentobj.alpha=0;
        spoon5_1.visible=true;
      }
      if(currentobj==droper1 || currentobj==droper2){
        arrow.x=330;
        arrow.y=600;
        arrow_y=600;
        arrow.visible=true;
      }
      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_3",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

Hot:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Simulation_hot1",true,false,ip);

 },
toSimulation:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);

 },
graph:function(){

 },

LC:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);


 },


 nextsim:function()
 {


 },
 buttonState:function(state1,state2){

 state1.visible = false;
 state2.visible = true;
 },

buttonState2:function(state2,state1){


 state1.visible = true;
 state2.visible = false;
 }


}
