var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
 
  //var corrent_val1;
  var resistance_val1;
  
 // var corrent_val2;
  var resistance_val2;
  
  //var corrent_val3;
  var resistance_val3;
  
  //var corrent_val4;
  var resistance_val4;
 
  var R1voltage_val1;
  var R1voltage_val2;
  var R1voltage_val3;
  var R2voltage_val1;
  var R2voltage_val2;
  var R2voltage_val3;
  var R3voltage_val1;
  var R3voltage_val2;
  var R3voltage_val3;

  var R1current_val1;
  var R1current_val2;
  var R1current_val3;
  var R2current_val1;
  var R2current_val2;
  var R2current_val3;
  var R3current_val1;
  var R3current_val2;
  var R3current_val3;

}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
       
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         //this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
       // this.game.load.image('dialogue_box3','assets/Ohms_law/dialogbox2.png');
       this.game.load.image('observation_1','assets/observation_box.png');
// this.game.load.image('observation_table','assets/Ohms_law/observationTable.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
//////////////////////////////Experiment images//////////////////////////////////
        this.game.load.image('bg','assets/Bg2.png');
        this.game.load.image('bg1','assets/Bg_1.png');
      this.game.load.image('direction','assets/Direction.png');
      this.load.atlasJSONHash('arrow', 'assets/arrow.png', 'assets/arrow.json')
      //////////////////////////Experiment 1////////////////////

      this.game.load.image('Beaker_Back','assets/Experiment1/Beaker_Back.png');
        this.game.load.image('water','assets/Experiment1/Sand_to_Beaker0001.png');
      this.game.load.image('inplate1','assets/Experiment1/Salt_in_Plate.png');
      this.game.load.image('inplate2','assets/Experiment1/Sugar_in_Plate.png');
      this.game.load.image('inplate3','assets/Experiment1/Alum_in_Plate.png');
      this.game.load.image('inplate5','assets/Experiment1/Chalkpower_in_Plate.png');
      this.game.load.image('inplate8','assets/Experiment1/Egg_in_Plate.png');
      this.game.load.image('inplate6','assets/Experiment1/Sand_in_Plate.png');
      this.game.load.image('inplate4','assets/Experiment1/Soil_in_Plate.png');
      this.game.load.image('inplate7','assets/Experiment1/Starch_in_Plate.png');
      this.game.load.image('Spoon','assets/Experiment1/Spoon.png');
      this.game.load.image('Rod','assets/Experiment1/Rod.png');
      this.game.load.image('Stand','assets/Experiment1/Stand.png');
      this.game.load.image('Funnel','assets/Experiment1/Funnel.png');
      this.game.load.image('col1','assets/Experiment1/col.png');
      this.game.load.image('Clock','assets/Experiment1/Clock.png');
      this.game.load.image('ClockNeedleBig','assets/Experiment1/Clock_Needle_Big.png');
      this.game.load.image('ClockNeedleSmall','assets/Experiment1/Clock_Needle_Small.png');
      this.game.load.image('observation_box1','assets/reactivity_of_metals_obs_table-01.png');
  this.game.load.image('mainBeaker','assets/Experiment1/Beaker.png');
  this.game.load.image('Filterpaper','assets/Experiment1/Filter_paper.png');
  this.game.load.image('Filterpaper1','assets/Experiment1/Filter_paper_1.png');
 this.game.load.image('funnel_back','assets/Experiment1/funnel_back.png');
      this.game.load.image('Beaker','assets/Experiment1/mainbeakerwithfunnel.png');
       this.game.load.image('Beaker_1','assets/Experiment1/mainbeakerwithfunnel_1.png');
       this.game.load.image('Beaker_1','assets/Experiment1/mainbeakerwithfunnel_1.png');
    this.game.load.image('result3','assets/Experiment1/Alum_in_Beaker_result.png');
    this.game.load.image('result5','assets/Experiment1/ChalkPowder_in_Beaker_result.png');
      this.game.load.image('result1','assets/Experiment1/CommonSalt_in_Beaker_result.png');
    this.game.load.image('result8','assets/Experiment1/Egg_in_Beaker_reaction.png');
      this.game.load.image('result6','assets/Experiment1/Sand _in_Beaker_result.png');
      this.game.load.image('result4','assets/Experiment1/Soil_in_ Beaker_result.png');
    this.game.load.image('result7','assets/Experiment1/Starch_in_Beaker_result.png');
    this.game.load.image('result2','assets/Experiment1/Sugar_in_Beaker_result.png');
     this.game.load.image('Paper','assets/Experiment1/Paper.png');
     this.game.load.image('Paper_side_view','assets/Experiment1/Paper_side_view.png');

     this.game.load.image('result41','assets/Experiment1/allanimtions/soil/Soil_in_ Beaker_result.png');
     this.game.load.image('result51','assets/Experiment1/allanimtions/chalk/ChalkPowder_in_Beaker_result.png');
     this.game.load.image('result61','assets/Experiment1/allanimtions/sand/Sand _in_Beaker_result.png');
      this.load.atlasJSONHash('tofunnel', 'assets/Experiment1/allanimtions/alum/alumbeakertofunnel.png', 'assets/Experiment1/allanimtions/alum/alumbeakertofunnel.json')
      this.load.atlasJSONHash('infunnel', 'assets/Experiment1/allanimtions/alum/aluminfunnel.png', 'assets/Experiment1/allanimtions/alum/aluminfunnel.json')
      this.load.atlasJSONHash('alumtobeakers', 'assets/Experiment1/allanimtions/alum/alumtobeakers.png', 'assets/Experiment1/allanimtions/alum/alumtobeakers.json')
      this.load.atlasJSONHash('alumwithrod', 'assets/Experiment1/allanimtions/alum/alumwithrod.png', 'assets/Experiment1/allanimtions/alum/alumwithrod.json')
      this.load.atlasJSONHash('spoonalum', 'assets/Experiment1/allanimtions/alum/spoonalum.png', 'assets/Experiment1/allanimtions/alum/spoonalum.json')
      this.game.load.image('alum_beaker', 'assets/Experiment1/allanimtions/alum/salt_Suga_Alum_Beaker.png')
     
      this.game.load.image('filterpaper_chalk', 'assets/Experiment1/allanimtions/chalk/Filter_papper_Chalk_powder.png')
      this.game.load.image('settle_chalk', 'assets/Experiment1/allanimtions/chalk/Chalk_Powder_ Beaker_settle.png')
      this.load.atlasJSONHash('chalkbeakertofunnel', 'assets/Experiment1/allanimtions/chalk/chalkbeakertofunnel.png', 'assets/Experiment1/allanimtions/chalk/chalkbeakertofunnel.json')
      this.load.atlasJSONHash('chalkinfunnel', 'assets/Experiment1/allanimtions/chalk/chalkinfunnel.png', 'assets/Experiment1/allanimtions/chalk/chalkinfunnel.json')
      this.load.atlasJSONHash('chalktobeaker', 'assets/Experiment1/allanimtions/chalk/chalktobeaker.png', 'assets/Experiment1/allanimtions/chalk/chalktobeaker.json')
      this.load.atlasJSONHash('chalkwithrod', 'assets/Experiment1/allanimtions/chalk/chalkwithrod.png', 'assets/Experiment1/allanimtions/chalk/chalkwithrod.json')
      this.load.atlasJSONHash('spoonchalk', 'assets/Experiment1/allanimtions/chalk/spoonchalk.png', 'assets/Experiment1/allanimtions/chalk/spoonchalk.json')
      this.game.load.image('chalkpowder_beaker', 'assets/Experiment1/allanimtions/chalk/Chalkpowder_Beaker.png')

      this.load.atlasJSONHash('eggbeakertofunnel', 'assets/Experiment1/allanimtions/egg/eggbeakertofunnel.png', 'assets/Experiment1/allanimtions/egg/eggbeakertofunnel.json')
      this.load.atlasJSONHash('egginfunnel', 'assets/Experiment1/allanimtions/egg/egginfunnel.png', 'assets/Experiment1/allanimtions/egg/egginfunnel.json')
      this.load.atlasJSONHash('eggtobeakers', 'assets/Experiment1/allanimtions/egg/eggtobeakers.png', 'assets/Experiment1/allanimtions/egg/eggtobeakers.json')
      this.load.atlasJSONHash('eggwithrod', 'assets/Experiment1/allanimtions/egg/eggwithrod.png', 'assets/Experiment1/allanimtions/egg/eggwithrod.json')
      this.load.atlasJSONHash('spooneg', 'assets/Experiment1/allanimtions/egg/spooneg.png', 'assets/Experiment1/allanimtions/egg/spooneg.json')
      this.game.load.image('egg_beaker', 'assets/Experiment1/allanimtions/egg/Egg_Beaker.png')

      this.load.atlasJSONHash('salttobeaker', 'assets/Experiment1/allanimtions/salt/salttobeaker.png', 'assets/Experiment1/allanimtions/salt/salttobeaker.json')
      this.load.atlasJSONHash('saltwithrod', 'assets/Experiment1/allanimtions/salt/saltwithrod.png', 'assets/Experiment1/allanimtions/salt/saltwithrod.json')
      this.load.atlasJSONHash('spoonsalt', 'assets/Experiment1/allanimtions/salt/spoonsalt.png', 'assets/Experiment1/allanimtions/salt/spoonsalt.json')

      this.game.load.image('settle_sand', 'assets/Experiment1/allanimtions/sand/Sand_Beaker_Settle.png')
      this.game.load.image('filterpaper_sand', 'assets/Experiment1/allanimtions/sand/Filter_papper_sand.png')
      this.load.atlasJSONHash('sandbeakertofunnel', 'assets/Experiment1/allanimtions/sand/sandbeakertofunnel.png', 'assets/Experiment1/allanimtions/sand/sandbeakertofunnel.json')
      this.load.atlasJSONHash('sandinfunnel', 'assets/Experiment1/allanimtions/sand/sandinfunnel.png', 'assets/Experiment1/allanimtions/sand/sandinfunnel.json')
      this.load.atlasJSONHash('sandtobeakers', 'assets/Experiment1/allanimtions/sand/sandtobeakers.png', 'assets/Experiment1/allanimtions/sand/sandtobeakers.json')
      this.load.atlasJSONHash('sandwithrod', 'assets/Experiment1/allanimtions/sand/sandwithrod.png', 'assets/Experiment1/allanimtions/sand/sandwithrod.json')
      this.load.atlasJSONHash('spoonsand', 'assets/Experiment1/allanimtions/sand/spoonsand.png', 'assets/Experiment1/allanimtions/sand/spoonsand.json')
      this.game.load.image('sand_beaker', 'assets/Experiment1/allanimtions/sand/Sand_Beaker.png')

    this.game.load.image('settle_soil', 'assets/Experiment1/allanimtions/soil/Soil_Beaker_Settle.png')
      this.game.load.image('filterpaper_soil', 'assets/Experiment1/allanimtions/soil/Filter_papper_soil.png')
      this.load.atlasJSONHash('soilbeakertofunnel', 'assets/Experiment1/allanimtions/soil/soilbeakertofunnel.png', 'assets/Experiment1/allanimtions/soil/soilbeakertofunnel.json')
      this.load.atlasJSONHash('soilinfunnel', 'assets/Experiment1/allanimtions/soil/soilinfunnel.png', 'assets/Experiment1/allanimtions/soil/soilinfunnel.json')
      this.load.atlasJSONHash('soiltobeakers', 'assets/Experiment1/allanimtions/soil/soiltobeakers.png', 'assets/Experiment1/allanimtions/soil/soiltobeakers.json')
      this.load.atlasJSONHash('soilwithrod', 'assets/Experiment1/allanimtions/soil/soilwithrod.png', 'assets/Experiment1/allanimtions/soil/soilwithrod.json')
      this.load.atlasJSONHash('spoonsoil', 'assets/Experiment1/allanimtions/soil/spoonsoil.png', 'assets/Experiment1/allanimtions/soil/spoonsoil.json')
      this.game.load.image('soil_beaker', 'assets/Experiment1/allanimtions/soil/Soil_Beaker.png')

      this.load.atlasJSONHash('starchbeakertofunnel', 'assets/Experiment1/allanimtions/starch/starchbeakertofunnel.png', 'assets/Experiment1/allanimtions/starch/starchbeakertofunnel.json')
      this.load.atlasJSONHash('starchbolied', 'assets/Experiment1/allanimtions/starch/starchbolied.png', 'assets/Experiment1/allanimtions/starch/starchbolied.json')
      this.load.atlasJSONHash('starchboliedwithrod', 'assets/Experiment1/allanimtions/starch/starchboliedwithrod.png', 'assets/Experiment1/allanimtions/starch/starchboliedwithrod.json')
      this.load.atlasJSONHash('starchinfunnel', 'assets/Experiment1/allanimtions/starch/starchinfunnel.png', 'assets/Experiment1/allanimtions/starch/starchinfunnel.json')
      this.load.atlasJSONHash('starchtobeakers', 'assets/Experiment1/allanimtions/starch/starchtobeakers.png', 'assets/Experiment1/allanimtions/starch/starchtobeakers.json')
      this.load.atlasJSONHash('starchwaterboiling', 'assets/Experiment1/allanimtions/starch/starchwaterboiling.png', 'assets/Experiment1/allanimtions/starch/starchwaterboiling.json')
      this.load.atlasJSONHash('starchwithrod', 'assets/Experiment1/allanimtions/starch/starchwithrod.png', 'assets/Experiment1/allanimtions/starch/starchwithrod.json')
      this.load.atlasJSONHash('spoonstarch', 'assets/Experiment1/allanimtions/starch/spoonstarch.png', 'assets/Experiment1/allanimtions/starch/spoonstarch.json')
      this.game.load.image('starch_beaker', 'assets/Experiment1/allanimtions/starch/Starch_beaker.png')

      this.load.atlasJSONHash('flame', 'assets/Experiment1/allanimtions/starch/Flame.png', 'assets/Experiment1/allanimtions/starch/Flame.json')
      this.game.load.image('tripod','assets/Experiment1/allanimtions/starch/Tripod-01.png');
      this.game.load.image('burner','assets/Experiment1/allanimtions/starch/Burner.png');

      this.load.atlasJSONHash('sugartobeaker', 'assets/Experiment1/allanimtions/sugar/sugartobeaker.png', 'assets/Experiment1/allanimtions/sugar/sugartobeaker.json')
      this.load.atlasJSONHash('sugarwithrod', 'assets/Experiment1/allanimtions/sugar/sugarwithrod.png', 'assets/Experiment1/allanimtions/sugar/sugarwithrod.json')
      this.load.atlasJSONHash('spoonsugar', 'assets/Experiment1/allanimtions/sugar/spoonsugar.png', 'assets/Experiment1/allanimtions/sugar/spoonsugar.json')
      this.load.atlasJSONHash('flip', 'assets/Experiment1/flip.png', 'assets/Experiment1/flip.json')
  /////////////////////Experiment_A1_sounds////////////////////////////////////
       this.game.load.audio('A_aim','assets/audio/Experiment_A1/Aim.mp3');
        this.game.load.audio('A_precaution1','assets/audio/Experiment_A1/precaution_1.mp3');
        this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
        this.game.load.audio('lets_perform_Exp','assets/audio/Experiment_A1/lets_perform_Exp.mp3');

        this.game.load.audio('A_observation1','assets/audio/Experiment_A1/A_observation1.mp3');
        this.game.load.audio('A_observation2','assets/audio/Experiment_A1/A_observation2.mp3');
        this.game.load.audio('A_observation3','assets/audio/Experiment_A1/A_observation3.mp3');
       this.game.load.audio('A_procedure1','assets/audio/Experiment_A1/procedure_1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Experiment_A1/procedure_2.mp3');
        this.game.load.audio('A_procedure3','assets/audio/Experiment_A1/procedure_3.mp3');
        this.game.load.audio('A_procedure4','assets/audio/Experiment_A1/procedure_4.mp3');
        this.game.load.audio('A_procedure5','assets/audio/Experiment_A1/procedure_5.mp3');
        this.game.load.audio('A_procedure6','assets/audio/Experiment_A1/procedure_6.mp3');
        this.game.load.audio('A_procedure7','assets/audio/Experiment_A1/procedure_7.mp3');
     
 
        this.game.load.audio('A_exp_procedure1','assets/audio/Experiment_A1/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/Experiment_A1/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/Experiment_A1/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/Experiment_A1/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/Experiment_A1/A_exp_procedure5.mp3');
        this.game.load.audio('A_exp_procedure6','assets/audio/Experiment_A1/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/Experiment_A1/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/Experiment_A1/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/Experiment_A1/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/Experiment_A1/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/Experiment_A1/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/Experiment_A1/A_exp_procedure12.mp3');
        this.game.load.audio('A_exp_procedure13','assets/audio/Experiment_A1/A_exp_procedure13.mp3');
        this.game.load.audio('A_exp_procedure14','assets/audio/Experiment_A1/A_exp_procedure14.mp3');
      this.game.load.audio('A_exp_procedure15','assets/audio/Experiment_A1/A_exp_procedure15.mp3');
      this.game.load.audio('A_exp_procedure16','assets/audio/Experiment_A1/A_exp_procedure16.mp3');
      this.game.load.audio('A_exp_procedure17','assets/audio/Experiment_A1/A_exp_procedure17.mp3');
      this.game.load.audio('A_exp_procedure18','assets/audio/Experiment_A1/A_exp_procedure18.mp3');
      this.game.load.audio('A_exp_procedure19','assets/audio/Experiment_A1/A_exp_procedure19.mp3');
      this.game.load.audio('A_exp_procedure20','assets/audio/Experiment_A1/A_exp_procedure20.mp3');
  

    this.game.load.audio('A2_exp_procedure1','assets/audio/Experiment_A1/A2_exp_procedure1.mp3');
    this.game.load.audio('A2_exp_procedure2','assets/audio/Experiment_A1/A2_exp_procedure2.mp3');
    this.game.load.audio('A2_exp_procedure3','assets/audio/Experiment_A1/A2_exp_procedure3.mp3');
    this.game.load.audio('A2_exp_procedure4','assets/audio/Experiment_A1/A2_exp_procedure4.mp3');
    this.game.load.audio('A2_exp_procedure5','assets/audio/Experiment_A1/A2_exp_procedure5.mp3');
    this.game.load.audio('A2_exp_procedure6','assets/audio/Experiment_A1/A2_exp_procedure6.mp3');
    this.game.load.audio('A2_exp_procedure7','assets/audio/Experiment_A1/A2_exp_procedure7.mp3');
    this.game.load.audio('A2_exp_procedure8','assets/audio/Experiment_A1/A2_exp_procedure8.mp3');

    this.game.load.audio('A4_exp_procedure1','assets/audio/Experiment_A1/A4_exp_procedure1.mp3');
    this.game.load.audio('A4_exp_procedure2','assets/audio/Experiment_A1/A4_exp_procedure2.mp3');
    this.game.load.audio('A4_exp_procedure3','assets/audio/Experiment_A1/A4_exp_procedure3.mp3');
    this.game.load.audio('A4_exp_procedure4','assets/audio/Experiment_A1/A4_exp_procedure4.mp3');

    this.game.load.audio('A5_exp_procedure1','assets/audio/Experiment_A1/A5_exp_procedure1.mp3');
    this.game.load.audio('A5_exp_procedure2','assets/audio/Experiment_A1/A5_exp_procedure2.mp3');
    this.game.load.audio('A5_exp_procedure3','assets/audio/Experiment_A1/A5_exp_procedure3.mp3');
    this.game.load.audio('A5_exp_procedure4','assets/audio/Experiment_A1/A5_exp_procedure4.mp3');
    this.game.load.audio('A5_exp_procedure5','assets/audio/Experiment_A1/A5_exp_procedure5.mp3');
    this.game.load.audio('A5_exp_procedure6','assets/audio/Experiment_A1/A5_exp_procedure6.mp3');
    this.game.load.audio('A5_exp_procedure7','assets/audio/Experiment_A1/A5_exp_procedure7.mp3');
    this.game.load.audio('A5_exp_procedure8','assets/audio/Experiment_A1/A5_exp_procedure8.mp3');
    this.game.load.audio('A5_exp_procedure9','assets/audio/Experiment_A1/A5_exp_procedure9.mp3');
    this.game.load.audio('A5_exp_procedure10','assets/audio/Experiment_A1/A5_exp_procedure10.mp3');

    this.game.load.audio('A3_exp_procedure1','assets/audio/Experiment_A1/A3_exp_procedure1.mp3');
    this.game.load.audio('A3_exp_procedure2','assets/audio/Experiment_A1/A3_exp_procedure2.mp3');
    this.game.load.audio('A3_exp_procedure3','assets/audio/Experiment_A1/A3_exp_procedure3.mp3');
    this.game.load.audio('A3_exp_procedure4','assets/audio/Experiment_A1/A3_exp_procedure4.mp3');
    this.game.load.audio('A3_exp_procedure5','assets/audio/Experiment_A1/A3_exp_procedure5.mp3');
    this.game.load.audio('A3_exp_procedure6','assets/audio/Experiment_A1/A3_exp_procedure6.mp3');
    this.game.load.audio('A3_exp_procedure7','assets/audio/Experiment_A1/A3_exp_procedure7.mp3');
    this.game.load.audio('A3_exp_procedure8','assets/audio/Experiment_A1/A3_exp_procedure8.mp3');
    this.game.load.audio('A3_exp_procedure9','assets/audio/Experiment_A1/A3_exp_procedure9.mp3');
    this.game.load.audio('A3_exp_procedure10','assets/audio/Experiment_A1/A3_exp_procedure10.mp3');
    this.game.load.audio('A3_exp_procedure11','assets/audio/Experiment_A1/A3_exp_procedure11.mp3');
    this.game.load.audio('A3_exp_procedure12','assets/audio/Experiment_A1/A3_exp_procedure12.mp3');
    this.game.load.audio('A3_exp_procedure13','assets/audio/Experiment_A1/A3_exp_procedure13.mp3');
    this.game.load.audio('A3_exp_procedure14','assets/audio/Experiment_A1/A3_exp_procedure14.mp3');
    this.game.load.audio('A3_exp_procedure15','assets/audio/Experiment_A1/A3_exp_procedure15.mp3');
    this.game.load.audio('A3_exp_procedure16','assets/audio/Experiment_A1/A3_exp_procedure16.mp3');
    this.game.load.audio('A3_exp_procedure31','assets/audio/Experiment_A1/A3_exp_procedure31.mp3');
    this.game.load.audio('A3_exp_procedure32','assets/audio/Experiment_A1/A3_exp_procedure32.mp3');
    this.game.load.audio('A3_exp_procedure33','assets/audio/Experiment_A1/A3_exp_procedure33.mp3');
    this.game.load.audio('A3_exp_procedure34','assets/audio/Experiment_A1/A3_exp_procedure34.mp3');
      },
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
    

     this.game.state.start("Aim");//Simulation_hot1
  // this.game.state.start("Materials");//Starting the gametitle state
    // this.game.state.start("Theory");//Simulation_hot1
   //// this.game.state.start("Lab_Precautions");
    // this.game.state.start("Procedure")
     // this.game.state.start("Procedure1")
       //this.game.state.start("Procedure2")
  // this.game.state.start("Procedure3");
 //this.game.state.start("Experiment_A5");//Starting the gametitle state
 // this.game.state.start("Experiment_A2")
// this.game.state.start("Experiment_A3")

//this.game.state.start("Experiment_A4")
 //this.game.state.start("Experiment_A1")


//this.game.state.start("Observations");//hot
  //  this.game.state.start("Result");//Starting the gametitle state
    //this.game.state.start("Viva");//Starting the gametitle state
	}
}

