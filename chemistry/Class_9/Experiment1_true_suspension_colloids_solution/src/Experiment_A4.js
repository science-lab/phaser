var experiment_a4 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;

var arrow;


var DeltaTime;




}

experiment_a4.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  this.arrowArr=new Array();
  this.arrowArr=[,];
  this.arrowpos=0;
  currentobj=null;
  this.dropped=false;

  dialogX=20;
  dialogY=80;
  
  this.DeltaTime=null;
  
 muted = false;
 voice=this.game.add.audio("obj",1);

 //this.arrangeScene();

 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(.241,.253);




  
 


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "38px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "35px Segoe UI", fill: "#FFFFFF", align: "center" };
  //labelfontStyle1={ font: "32px Segoe UI", fill: "#ffffff", wordWrap: { width: 10 }, align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

////////////////////////////////////////////////////////////////////
this.labelText=["A","B","C","D","E","F","G","H"];

this.clockStart=false;
this.maingroupback=this.game.add.group();
this.maingroupResult=this.game.add.group();
this.arrangeScene();
this.startExperiment();
 
      

 },
 // For Full screen checking.
  
 gofull: function()
 {

 if (this.game.scale.isFullScreen)
   {
    this.game.scale.stopFullScreen();
   }
 else
   {
   this.game.scale.startFullScreen(false);
   }  
 },
 
///Start
 startExperiment:function(){
  this.procedure=0;
    voice.stop();
    voice=this.game.add.audio("A4_exp_procedure1",1);
    voice.play();
    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
    dialog_text="Allow the solutions to stand undisturbed for about half an hour";
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
   
    
  
 },
 ///Arrange scene
 arrangeScene:function(){
 //  var itemOnarr=new Array();

 this.resultarr1=["result1","result2","result3","result4","result5","result6","result7","result8"]
 this.resultarr2=["result1","result2","result3","settle_soil","settle_chalk","settle_sand","result7","result8"]
 var resultarrName=["Common\nSalt","Sugar","Alum","Soil","Chalk\npowder","Fine\nsand","Starch","Egg\nalbumin"]

   for(var i=1;i<9;i++){
      this.beakerback=this.maingroupback.create(-80+(i*220),770, 'Beaker_Back');
      this.beakerback.anchor.set(.5);
      this.beakerback.scale.set(.7);

      this.beaker=this.maingroupResult.create(-80+(i*220),770, this.resultarr1[i-1]);
      this.beaker.anchor.set(.5);
      this.beaker.scale.set(.7);
    /*this.cols=this.game.add.sprite(0,-35,"col1");
      this.cols.anchor.set(.5);
      this.cols.scale.setTo(4,3)
      this.cols.alpha=.2
      this.beaker.addChild(this.cols);*/
      if(i==1){
       var labeltext1=this.game.add.text(-70,-70,resultarrName[i-1],labelfontStyle1);
      }else if(i==8||i==5){
        var labeltext1=this.game.add.text(-60,-70,resultarrName[i-1],labelfontStyle1);
       }else {
      var labeltext1=this.game.add.text(-40,-70,resultarrName[i-1],labelfontStyle1);
     }
      this["labeltext1"+i]=this.game.add.text(-10,220,this.labelText[i-1],headfontStyle)
      this.beaker.addChild(this["labeltext1"+i]);
      this.beaker.addChild(labeltext1);
  }
  this.ClockPart();
  this.angleTimer= this.game.time.events.loop(Phaser.Timer.SECOND,this.MoveAngle,this);
   
  
 },
//clock part
 ClockPart:function(){
 
  this.clocks=this.game.add.sprite(550,290,"Clock");
  this.clocks.anchor.set(.5);
  this.clocks.scale.set(.7);
  this.clocksNeedleBig=this.game.add.sprite(550,290,"ClockNeedleBig");
  this.clocksNeedleBig.anchor.set(.5,.85);
  this.clocksNeedleBig.scale.set(.7);
  this.clocksNeedleSmall=this.game.add.sprite(550,290,"ClockNeedleSmall");
  this.clocksNeedleSmall.anchor.set(.5,.8);
  this.clocksNeedleSmall.scale.set(.7);
  //this.clocksNeedleSmall.visible=false;
  this.clockNum=0;
  this.clockNumForangle=0;
  this.clockStart=false;
 
 },
 angleMovement:function(){
  if(this.clockNum>=36){
    this.game.time.events.remove(this.angleTimer);
  
    this.clocksNeedleBig.angle=180;
    for(var i=1;i<9;i++){
    this.maingroupResult.children[i-1].loadTexture(this.resultarr2[i-1]);
    if(i>=4&&i<7){
      this.maingroupResult.children[i-1].y=765
    }
    this.ShowStable();
    this.clockStart=true;
    }
  }else{
   
       
      if(this.clocksNeedleBig.angle>=179||this.clocksNeedleBig.angle<-160){
        this.clockNum=36;
      }
   
    this.clocksNeedleBig.angle += 7* DeltaTime;
  
  
    
  }
  
},
MoveAngle:function(){
  
  this.clockNum+= 1;
  
},
///Stable Solutions
ShowStable:function(){
  voice.stop();
  voice=this.game.add.audio("A4_exp_procedure2",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="The solute particles in solutions A, B, C, G and H do not settle down when left\nundisturbed. So these solutions are therefore stable";
  dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
  for(var i=1;i<4;i++){
  var tweens=this.game.add.tween(this.maingroupback.children[i-1].scale).to({x:.9,y:.9}, 400, Phaser.Easing.Linear.Out, true);
  var tweens=this.game.add.tween(this.maingroupResult.children[i-1].scale).to({x:.9,y:.9}, 400, Phaser.Easing.Linear.Out, true);
  }
  for(var i=7;i<9;i++){
    var tweens=this.game.add.tween(this.maingroupback.children[i-1].scale).to({x:.9,y:.9}, 400, Phaser.Easing.Linear.Out, true);
    var tweens=this.game.add.tween(this.maingroupResult.children[i-1].scale).to({x:.9,y:.9}, 400, Phaser.Easing.Linear.Out, true);
    }
    this.game.time.events.add(Phaser.Timer.SECOND*11,this.goBackScaleOption,this)
},
goBackScaleOption:function(){
  for(var i=1;i<4;i++){
    var tweens=this.game.add.tween(this.maingroupback.children[i-1].scale).to({x:.7,y:.7}, 400, Phaser.Easing.Linear.Out, true);
    var tweens=this.game.add.tween(this.maingroupResult.children[i-1].scale).to({x:.7,y:.7}, 400, Phaser.Easing.Linear.Out, true);
    }
    for(var i=7;i<9;i++){
      var tweens=this.game.add.tween(this.maingroupback.children[i-1].scale).to({x:.7,y:.7}, 400, Phaser.Easing.Linear.Out, true);
      var tweens=this.game.add.tween(this.maingroupResult.children[i-1].scale).to({x:.7,y:.7}, 400, Phaser.Easing.Linear.Out, true);
      }

      this.game.time.events.add(Phaser.Timer.SECOND,this.goBackScaleOptionForNextbeaker,this)  
},
//Unstable Solution
goBackScaleOptionForNextbeaker:function(){
  voice.stop();
  voice=this.game.add.audio("A4_exp_procedure3",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="The solute particles in solutions D, E and F settle down when left undisturbed.\nSuspensions are therefore unstable";
  dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
  for(var i=4;i<7;i++){
  var tweens=this.game.add.tween(this.maingroupback.children[i-1].scale).to({x:.9,y:.9}, 400, Phaser.Easing.Linear.Out, true);
  var tweens=this.game.add.tween(this.maingroupResult.children[i-1].scale).to({x:.9,y:.9}, 400, Phaser.Easing.Linear.Out, true);
  }
  this.game.time.events.add(Phaser.Timer.SECOND*10,this.goBackNext,this)  
},
goBackNext:function(){
  for(var i=4;i<7;i++){
    var tweens=this.game.add.tween(this.maingroupback.children[i-1].scale).to({x:.7,y:.7}, 400, Phaser.Easing.Linear.Out, true);
    var tweens=this.game.add.tween(this.maingroupResult.children[i-1].scale).to({x:.7,y:.7}, 400, Phaser.Easing.Linear.Out, true);
    }
  voice.stop();
  voice=this.game.add.audio("A4_exp_procedure4",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="Click on the next button to find the conclusions";
  dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
  play.visible=true
},
update:function(){
  DeltaTime=this.game.time.elapsed/1000;
  if( this.clockStart==false){
  this.angleMovement();
  }
},
      toNextScene:function()
      {
      voice.destroy();
      
     this.state.start("Experiment_A5", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A4",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
