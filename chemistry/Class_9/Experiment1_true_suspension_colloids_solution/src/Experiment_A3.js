var experiment_a3 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  /////////////////////////////////////////////////////////
  var TestTube_1;
  var TestTube_2;
  var collider;
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var ratio;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  var delay;
  var incr;
  var currentobj;
  
  var arrow;
  var arrow1;
  var arrow2;
  
  
  var DeltaTime;
  
  
  
  
  }
  
  experiment_a3.prototype ={
  
  init: function( ipadrs) {
  
       ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
   {
  
    if(this.game.sound.mute)
    {
      this.game.sound.mute = false;
    }
  
    this.dropped=false;
    currentobj=null;
    this.arrowpos=0;
    this.exp=1;
    dialogX=20;
    dialogY=80;
    this.paperscale=1.5;//to change the scaling of the filter paper
    this.DeltaTime=null;
    
   muted = false;
   voice=this.game.add.audio("obj",1);
  
   bg= this.game.add.sprite(0, 0,'bg');
  
  
  
  
  
    
   
  
  
  ////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////
  
  // Button panel backgroud-balck
  
    righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
    righttop_bg.scale.setTo(.5,.5);
    righttop_bg.angle=-90;
    
  // Button panel -Quit button
    
    quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
    quitButton.scale.setTo(.7,.7);
    quitButton.inputEnabled = true;
    quitButton.input.useHandCursor = true;
    quitButton.events.onInputDown.add(this.closeTheGame, this);
  
  // Button panel -Home button
    
    homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
    homeButton.scale.setTo(.7,.7);
    homeButton.inputEnabled = true;
    homeButton.input.useHandCursor = true;
    homeButton.events.onInputDown.add(this.gotoHome, this);
  
  // Button panel -Mute button
    
    muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
    muteButton.scale.setTo(.7,.7);
    muteButton.inputEnabled = true;
    muteButton.input.useHandCursor = true;
    muteButton.events.onInputDown.add(this.muteTheGame, this);
  
  // Button panel - Volume button
    
    volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
    volumeButton.scale.setTo(.7,.7);
    volumeButton.inputEnabled = true;
    volumeButton.input.useHandCursor = true;
    volumeButton.events.onInputDown.add(this.volume, this);
    volumeButton.visible=false;
  
    resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
    resetButton.scale.setTo(.7,.7);
    resetButton.inputEnabled = true;
    resetButton.input.useHandCursor = true;
    resetButton.events.onInputDown.add(this.resetTheGame, this);
  
  ///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////
  
  // Button panel backgroud-balck
    
    //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
    //leftbottom_bg.scale.setTo(2,2);
  
  ////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////
  
    // if (!this.game.device.desktop)
    // {
    this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
    
    fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
    fullScreen1.scale.setTo(2.5,2.5);
    fullScreen1.inputEnabled = true;
    fullScreen1.input.useHandCursor = true;
    fullScreen1.events.onInputUp.add(this.gofull,this);    
    //}
  
    ////////////////////////////////////////Font///////////////////////
    fontStyle={ font: "38px Segoe UI", fill: "#ffffff", align: "left" };
    headfontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
    labelfontStyle={ font: "55px Segoe UI", fill: "#ffffff", align: "left" };
    labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
    /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
    
    //this.addItems();
  
    
  
    /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
   
   play = this.game.add.sprite(1800,800,'components','play_pressed.png');
   play.scale.setTo(.7,.7);
   play.inputEnabled = true;
   //play.input.priorityID = 3;
   play.input.useHandCursor = true;
   play.events.onInputDown.add(this.toNextScene, this);
   play.visible=false;
  
    // Button panel group
    
    buttonGroup=this.game.add.group();
    buttonGroup.add(righttop_bg);
    buttonGroup.add(muteButton);
    buttonGroup.add(homeButton);
    buttonGroup.add(quitButton);
    buttonGroup.add(volumeButton);
    buttonGroup.add(resetButton);
    collider=null;
  //////////////////////////////Values///////////////////////////////
  fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
  fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};
  
  ////////////////////////////////////////////////////////////////////
  
  
  this.maingroup=this.game.add.group();
  
  this.exp=1///////////////////to change experiment
  this.ChangeText();
  this.arrangeScene();
  this.startExperiment();
  this.ArrowPart()
        
  
   },
   // For Full screen checking.
    
   gofull: function()
   {
  
   if (this.game.scale.isFullScreen)
     {
      this.game.scale.stopFullScreen();
     }
   else
     {
     this.game.scale.startFullScreen(false);
     }  
   },
   
  
   startExperiment:function(){
    this.procedure=0
    this.allenable=false;
    this.dropped=false;
    currentobj=null;
    this.arrowpos=0;
      
    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
  
    voice=this.game.add.audio(this.firstAudio,1);
    voice.play();
    dialog_text=this.firstText;
  
    dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
     
   },
   //ArrangeScene
   arrangeScene:function(){
    
    this.arr1=new Array();
    this.arr2=new Array();
    this.arr3=new Array();
    this.maingroup.alpha=0;
    var tweens=this.game.add.tween(this.maingroup).to({alpha:1}, 1500, Phaser.Easing.Linear.Out, true);
  
      this.alphaArrangeScene();
     
   
   
   
   },
   alphaArrangeScene:function(){
    
    
    for(var i=1;i<this.maxCount;i++){
      if(this.exp==3){
        this.stand=this.maingroup.create((i*800)-300,700,"Stand");
      }else{
      this.stand=this.maingroup.create((i*600)-300,700,"Stand");
      }
      this.stand.anchor.set(.5);
      this.stand.scale.set(.7);
      this.beakerback=this.game.add.sprite(198,120,"Beaker_Back");
      this.beakerback.anchor.set(.5);
      this.beakerback.scale.set(.8);
      this.stand.addChild(this.beakerback);
       this.funnelback=this.game.add.sprite(80,-280,"funnel_back");
      //this.funnelback.anchor.set(.5);
      this.stand.addChild(this.funnelback);
      this.beaker=this.game.add.sprite(200,20,this.labelfunnelItem[i-1]);
     this.beaker.anchor.set(.5);
     
     this.stand.addChild(this.beaker);
    
      this.beaker1=this.game.add.sprite(200,20,"Beaker");
      this.beaker1.anchor.set(.5);
    
      this.stand.addChild(this.beaker1);
      this.colliderA=this.game.add.sprite(0,-250, 'collider');
      this.game.physics.arcade.enable(this.colliderA);
      this.beaker.addChild(this.colliderA);
      this.colliderA.anchor.set(.5);
      this.colliderA.scale.setTo(2,2);
      this.colliderA.enableBody =false;
     this.colliderA.visible=false;
       this.colliderA.alpha=.0001;
      this.beaker.colliderOb=this.colliderA;
    
      this.beaker2=this.game.add.sprite(200,20,"Beaker_1");
      this.beaker2.anchor.set(.5);
    
      this.stand.addChild(this.beaker2);
      /*this.resultmainfunnel=this.game.add.sprite(200,-90,"Funnel");
      this.resultmainfunnel.anchor.set(.5);
      this.resultmainfunnel.scale.set(.7);
      this.stand.addChild(this.resultmainfunnel);*/
      this.resultbeaker=this.game.add.sprite(200,120,this.resultText[i-1]);
      this.resultbeaker.anchor.set(.5);
      this.resultbeaker.scale.set(.8);
      this.resultbeaker.scale.x*=-1;
      this.stand.addChild(this.resultbeaker);
      this.funnelback.visible=false;
     this.beaker.visible=false;
     this.beaker2.visible=false;
     this.resultbeaker.visible=false;
  
     this.filterpaper1=this.game.add.sprite(200,400,"Filterpaper1");
     this.filterpaper1.anchor.set(.5);
     this.filterpaper1.scale.set(.8);
   
     this.stand.addChild(this.filterpaper1);
     this.filterpaper1.xposed=200;
     this.filterpaper1.yposed=-200;
   /*this.beaker.visible=false;
   this.resultmainfunnel.visible=false;
   this.resultbeaker.visible=false;
  */
  
      var labeltext1=this.game.add.text(0,50,this.labelText[i-1],labelfontStyle);
      this.stand.addChild(labeltext1);
      this["arr"+i]=[this.filterpaper1,this.beaker1,this.beaker,this.funnelback,this.resultbeaker,this.beaker2]
    }
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.ShowFilterpaperAnimText,this);
  // this.game.time.events.add(Phaser.Timer.SECOND*6,this.loadFilterPaper,this);
   },
   ShowFilterpaperAnimText:function(){
     
      voice.stop();
      voice=this.game.add.audio("A3_exp_procedure16",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="To do the filtration test we use filter papers.\n Let us see how to fold the filter paper before placing it in the funnel.";
      dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
      
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.ShowFilterpaperAnimPage,this);
   },
   ShowFilterpaperAnimPage:function(){
    this.popupBase= this.game.add.sprite(280,250,'dialogue_box');
    this.popupBase.scale.setTo(.8);
    this.popupBase.alpha=0;
    this.filterPaperAnims=this.game.add.sprite(900,580, 'flip');
    this.filterPaperAnims.anchor.set(.5);
    this.filterPaperAnims.scale.set(1.2);
    this.filterPaperAnims.alpha=0;
    this.filterPaperAnims.animations.add('allframes');
   
     var tweens1=this.game.add.tween(this.popupBase).to({alpha:1}, 800, Phaser.Easing.Linear.Out, true);
      var tweens2=this.game.add.tween(this.filterPaperAnims).to({alpha:1}, 1000, Phaser.Easing.Linear.Out, true);
      tweens2.onComplete.add(function () {
        this.filterPaperAnims.animations.play('allframes', 10,false);
      this.game.time.events.add(Phaser.Timer.SECOND*9,this.removefilterPaperanim,this);
     }.bind(this));
   },
   removefilterPaperanim:function(){
    var tweens1=this.game.add.tween(this.popupBase).to({alpha:0}, 800, Phaser.Easing.Linear.Out, true);
    var tweens2=this.game.add.tween(this.filterPaperAnims).to({alpha:0}, 800, Phaser.Easing.Linear.Out, true);
    tweens1.onComplete.add(function () {
      this.popupBase.destroy();
    this.filterPaperAnims.destroy();
    voice.stop();
      voice=this.game.add.audio("A3_exp_procedure34",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="First need to fit the filter paper in all funnels.";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    this.loadFilterPaper();
   }.bind(this));
   
   },
   //Filter paper animation
   loadFilterPaper:function(){
   
    for(var i=1;i<this.maxCount;i++){
     
      //var tweens=this.game.add.tween(this["arr"+i][0]).to({x:this["arr"+i][0].xposed,y: this["arr"+i][0].yposed}, 400, Phaser.Easing.Linear.Out, true);
      var tweens=this.game.add.tween(this["arr"+i][0]).to({y: this["arr"+i][0].yposed-130}, 400, Phaser.Easing.Linear.Out, true);
   
      tweens.onComplete.add(function () {
        this.game.time.events.add(Phaser.Timer.SECOND*.2,this.FilterPapertoRound,this);
        
      }.bind(this));
  }
   },
   FilterPapertoRound:function(){
    for(var i=1;i<this.maxCount;i++){
    this["arr"+i][0].loadTexture("Filterpaper");
    var tweens1=this.game.add.tween(this["arr"+i][0]).to({y: this["arr"+i][0].yposed}, 400, Phaser.Easing.Linear.Out, true);
     
      tweens1.onComplete.add(function () {
         this.allenable=true;
        
      }.bind(this));
    }
   },
   //Drag beaker process
   VisibleBeaker:function(){
    this.ProcedureText();
    this.dragbeaker=this.game.add.sprite(-100,this.maingroup.children[this.labeli-1].y+130,this.labelfunnelToItem[this.labeli-1]);
    this.dragbeaker.anchor.setTo(.8,.3);
    this.dragbeaker.scale.set(.6);
    /*this.cols=this.game.add.sprite(-25,120,"col1");
    this.cols.anchor.set(.5);
    this.cols.scale.setTo(3,2)
    this.cols.alpha=.2
    this.dragbeaker.addChild(this.cols);*/
    console.log(this.labeli);
    if(this.exp==3&&this.labeli==2){
      this.dragbeakerlabeltext1=this.game.add.text(-90,60, this.labelNameText[this.labeli-1],headfontStyle);
    }else{
    this.dragbeakerlabeltext1=this.game.add.text(-80,100, this.labelNameText[this.labeli-1],headfontStyle);
    }
    this.dragbeaker.addChild(this.dragbeakerlabeltext1);
  
    this.game.physics.arcade.enable(this.dragbeaker);
    this.colliderA=this.game.add.sprite(60,0, 'collider');
    this.game.physics.arcade.enable(this.colliderA);
    this.dragbeaker.addChild(this.colliderA);
    this.colliderA.anchor.setTo(.8,.3);
    this.colliderA.scale.setTo(4,3);
    this.colliderA.enableBody =true;
    //this.colliderA.visible=false;
    this.colliderA.alpha=.0001;
    this.dragbeaker.colliderOb=this.colliderA;
    this.dragbeaker.events.onDragStart.add(function() {this.DragOn(this.dragbeaker)}, this);
    this.dragbeaker.events.onDragStop.add(function() {this.DropOut(this.dragbeaker)}, this);
  
    this.dragbeaker.body.enable =true;
    if(this.exp==3){
      this.dragbeaker.xpos=this.maingroup.children[1].x-300
      this.dragbeaker.ypos=this.maingroup.children[1].y+130
    }else{
    this.dragbeaker.xpos=this.maingroup.children[1].x+50
    this.dragbeaker.ypos=this.maingroup.children[1].y+130
    }
  
  
    var t=this.game.add.tween( this.dragbeaker).to({x:  this.dragbeaker.xpos}, 500, Phaser.Easing.Linear.In, true);
    t.onComplete.add(function () {
      this.dragbeaker.inputEnabled=true;
      this.dragbeaker.input.useHandCursor = true;
      this.dragbeaker.input.enableDrag(true);
      this["arr"+this.labeli][2].colliderOb.visible=true;
      this["arr"+this.labeli][2].colliderOb.enableBody=true;;
      this.arrowpos+=1;
      this. ArrowVisibility();
    }.bind(this));
   },
   beakerGone:function(){
    var t=this.game.add.tween( this.dragbeaker).to({x:  -100}, 500, Phaser.Easing.In, true);
    t.onComplete.add(function () {
      this.dragbeaker.colliderOb.destroy()
      this.dragbeaker.destroy()
      this.labeli+=1;
      if(this.labeli==this.maxCount){
        this.ProcedureText();
      }else{
        this.game.time.events.add(Phaser.Timer.SECOND*2,this.VisibleBeaker,this);
      }
   
    }.bind(this));
   },
    DragOn(obj){
      
    obj.body.enable =false;
      currentobj=obj;
    
      this.dropped=false;
      this.arrowpos+=1;
      this.ArrowVisibility();
      if(currentobj==this.dragbeaker)
      {
      //  this.dragbeaker.anchor.setTo(.8,.3);
      // this.dragbeaker.colliderOb.anchor.setTo(.8,.3);
      }
    },
    DropOut(obj){
      obj.body.enable =true;
      this.dropped=true;
      console.log( this.dropped);
    },
    detectCollision:function(){
      if(this["arr"+this.labeli][2].colliderOb!=null&&this["arr"+this.labeli][2].colliderOb.enableBody && currentobj!=null&& currentobj==this.dragbeaker)
          {
          
          
              this.game.physics.arcade.overlap(this.dragbeaker.colliderOb, this["arr"+this.labeli][2].colliderOb,function() {this.match_Obj()},null,this);
            
            
          } 
      if(currentobj!=null && currentobj.body.enable){
        this.arrowpos-=1;
        this.dragbeakerlabeltext1.visible=true
      ////////// this.cols.visible=true
        this.ArrowVisibility();
        currentobj.reset(currentobj.xpos,currentobj.ypos);//
        this.dropped=false;
      }
    },
    match_Obj:function(){
    
      currentobj.inputEnabled=false;
      currentobj.input.enableDrag(false);
      currentobj.body.enable=false;
    arrow.visible=false
    this.dragbeakerlabeltext1.visible=false
    //////this.cols.visible=false
      if(currentobj==this.dragbeaker)
        {
          this["arr"+this.labeli][2].colliderOb.enableBody=false;
          this["arr"+this.labeli][2].colliderOb.visible=false
          this.dragbeaker.x= this.maingroup.children[this.labeli-1].x+245
          this.dragbeaker.y= this.maingroup.children[this.labeli-1].y-228
          this.dragbeaker.anchor.setTo(.4,.3);
          var anim=this.dragbeaker.animations.add('frameall');
          this.dragbeaker.animations.play('frameall',10,false)
          this.game.time.events.add(Phaser.Timer.SECOND*1,this.playAnimtofunnelbeaker,this);
        
          anim.onComplete.add(function () {
            this.dragbeaker.reset(this.dragbeaker.xpos,this.dragbeaker.ypos);//
          // this.dragbeaker.animations.add('framel1',[0]);
          // this.dragbeaker.animations.play('framel1',10,false)
          this.dragbeaker.loadTexture("alumtobeakers");
          this.dragbeaker.anchor.set(.5);
          this.dragbeaker.x=this.dragbeaker.xpos-20;
          this.dragbeaker.y=this.dragbeaker.ypos+80;
          var beakerback1=this.game.add.sprite(-150,-243, 'Beaker_Back');
          this.dragbeaker.addChild(beakerback1)
          this.dragbeaker.scale.set(.5)
        
          this.beakerGone();
  
          }.bind(this));
        }
  
          currentobj=null;
          this.dropped=false;
    },
    playAnimtofunnelbeaker:function(){
      this["arr"+this.labeli][2].animations.add('frameall');
          this["arr"+this.labeli][2].animations.play('frameall',10,false)
    },
  
    update:function(){
      if( this.allenable){
        for(var i=1;i<this.maxCount;i++){
        
          this["arr"+i][0].visible=false
          this["arr"+i][1].visible=false
          this["arr"+i][2].visible=true
          this["arr"+i][3].visible=true
  
        }
        this.procedure=0;
        
      
        
        this.labeli=1;
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.VisibleBeaker,this);
        this.allenable=false;
      }
      if(this.dropped){
      this.detectCollision();
      }
    },
    ArrowPart:function(){
    
      arrow=this.game.add.sprite(0,0, 'arrow');
      arrow.anchor.set(.5);
      arrow.animations.add('allframes');
      arrow_y=arrow.y;
      arrow.visible=false;
  
      arrow1=this.game.add.sprite(0,0, 'arrow');
      arrow1.anchor.set(.5);
      arrow1.animations.add('allframes');
      
      arrow1.visible=false;
  
      arrow2=this.game.add.sprite(0,0, 'arrow');
      arrow2.anchor.set(.5);
      arrow2.animations.add('allframes');
      
      arrow2.visible=false;
  
    },
    ArrowVisibility:function(){
      arrow.destroy();
      arrow1.destroy();
      arrow2.destroy();
      this.ArrowPart()
      switch(this.arrowpos){
        case 1:
          case 3:
            case 5:
            arrow.visible=true
            arrow.animations.play('allframes', 25,true);
            arrow.x=this.dragbeaker.xpos-20;
            arrow.y=this.dragbeaker.ypos;
            break;
            case 2:
      
              arrow.visible=true
              arrow.animations.play('allframes', 25,true);
              arrow.x=this.maingroup.children[0].x+125;
              arrow.y=this.maingroup.children[0].y-200
              break;
              case 4:
      
                arrow.visible=true
                arrow.animations.play('allframes', 25,true);
                arrow.x=this.maingroup.children[1].x+125;
                arrow.y=this.maingroup.children[1].y-200
                break;
                case 6:
      
                  arrow.visible=true
                  arrow.animations.play('allframes', 25,true);
                  arrow.x=this.maingroup.children[2].x+125;
                  arrow.y=this.maingroup.children[2].y-200
                  break;
      }
    },
    ProcedureText:function(){
      this.procedure+=1;
      if(this.exp==1){
      this.ProcedureTextExp1();
      }else if(this.exp==2){
        this.ProcedureTextExp2();
        }else if(this.exp==3){
          this.ProcedureTextExp3();
          }
    },
    ////////////////To change for all 3 process/////////////////////////////
  /////////////////////////////////////////////////////
  ChangeText:function(){
    if(this.exp==1){
    this.labelText=["A","B","C"];
    this.labelNameText=["Salt","Sugar","Alum"];
    this.resultText=["result1","result2","result3"]
    this.labelfunnelItem=["infunnel","infunnel","infunnel"];
    this.labelfunnelToItem=["tofunnel","tofunnel","tofunnel"];
    this.maxCount=4
    this.firstText="Check whether the contents of beakers A, B and C can be separated by\nfiltration or not.";
    this.firstAudio="A3_exp_procedure31"
    } if(this.exp==2){
      this.labelNameText=["Soil","Chalk","Sand"];
      this.resultText=["result41","result51","result61"]
      this.labelText=["D","E","F"];
      this.labelfunnelItem=["soilinfunnel","chalkinfunnel","sandinfunnel"];
      this.labelfunnelToItem=["soilbeakertofunnel","chalkbeakertofunnel","sandbeakertofunnel"];
      this.maxCount=4
      this.firstText="Check whether the contents of beakers D, E and F can be separated by\nfiltration or not.";
      this.firstAudio="A3_exp_procedure32"
      }
      if(this.exp==3){
        this.labelNameText=["Starch","Egg\nAlbumin"];
        this.resultText=["result7","result8"]
        this.labelText=["G","H"];
      this.labelfunnelItem=["starchinfunnel","egginfunnel"];
      this.labelfunnelToItem=["starchbeakertofunnel","eggbeakertofunnel"];
        this.maxCount=3
        this.firstText="Check whether the contents of beakers G and H can be separated by\nfiltration or not.";
        this.firstAudio="A3_exp_procedure33"
        }
  },
  ProcedureTextExp1:function(){
    switch(this.procedure){
      
      case 1:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure2",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker A";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 2:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure3",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker B";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 3:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure4",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker C";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 4:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure5",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Now observe that the solutions A, B and C easily pass through the filter paper\nand no residue is left on the filter paper.";
        dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
        for(var i=1;i<this.maxCount;i++){
       
          this["arr"+i][0].visible=true
          this["arr"+i][1].visible=false
          this["arr"+i][2].visible=false
          this["arr"+i][3].visible=false
          this["arr"+i][4].visible=true
          this["arr"+i][5].visible=true
         
            this["arr"+i][0].loadTexture("Filterpaper1");
            this["arr"+i][0].y-=35
            var tweens=this.game.add.tween(this["arr"+i][0].scale).to({x:this.paperscale,y:this.paperscale}, 1500, Phaser.Easing.Linear.Out, true);
  
        }
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.visiblearrowLast1,this);
        arrow.animations.play('allframes', 25,true);
        arrow.x=this.maingroup.children[0].x+125;
        arrow.y=this.maingroup.children[0].y-210
        arrow1.animations.play('allframes', 25,true);
        arrow1.x=this.maingroup.children[1].x+125;
        arrow1.y=this.maingroup.children[1].y-210
        arrow2.animations.play('allframes', 25,true);
        arrow2.x=this.maingroup.children[2].x+125;
        arrow2.y=this.maingroup.children[2].y-210
       this.game.time.events.add(Phaser.Timer.SECOND*10,this.LoadOtherPage,this);
      break;
    }
  
  },
  visiblearrowLast1:function(){
    arrow.visible=true;
    arrow1.visible=true;
    arrow2.visible=true;
   
  },
  ProcedureTextExp2:function(){
    switch(this.procedure){
      
      case 1:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure7",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker D";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 2:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure8",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker E";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 3:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure9",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker F";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 4:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure10",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
  
        dialog_text="Now observe that the solution D, E and F do not pass through filter paper\nand are left on the filter paper as residue.";
        dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
        for(var i=1;i<this.maxCount;i++){
       
          this["arr"+i][0].visible=true
          this["arr"+i][1].visible=false
          this["arr"+i][2].visible=false
          this["arr"+i][3].visible=false
          this["arr"+i][4].visible=true
          this["arr"+i][5].visible=true
        
          if(i==1){
            this["arr"+i][0].loadTexture("filterpaper_soil");
          }
          if(i==2){
            this["arr"+i][0].loadTexture("filterpaper_chalk");
          }
          if(i==3){
            this["arr"+i][0].loadTexture("filterpaper_sand");
          }
            this["arr"+i][0].y-=35
            var tweens=this.game.add.tween(this["arr"+i][0].scale).to({x:this.paperscale,y:this.paperscale}, 1500, Phaser.Easing.Linear.Out, true);
        }
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.visiblearrowLast2,this);
       
       arrow.animations.play('allframes', 25,true);
       arrow.x=this.maingroup.children[0].x+125;
       arrow.y=this.maingroup.children[0].y-210
       arrow1.animations.play('allframes', 25,true);
       arrow1.x=this.maingroup.children[1].x+125;
       arrow1.y=this.maingroup.children[1].y-210
       arrow2.animations.play('allframes', 25,true);
       arrow2.x=this.maingroup.children[2].x+125;
       arrow2.y=this.maingroup.children[2].y-210
       this.game.time.events.add(Phaser.Timer.SECOND*10,this.LoadOtherPage,this);
      break;
    }
  
  },
  visiblearrowLast2:function(){
    arrow.visible=true;
    arrow1.visible=true;
    arrow2.visible=true;
  },
  ProcedureTextExp3:function(){
    switch(this.procedure){
      
      case 1:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure12",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker G";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
      case 2:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure13",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Drag and pour the solution to the beaker H";
        dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    
      break;
    
      case 3:
        voice.stop();
        voice=this.game.add.audio("A3_exp_procedure14",1);
        voice.play();
        dialog_text="";
        dialog.destroy();
        dialog_text="Now observe that the solution G and H pass through the filter paper and no\nresidue is left on the filter paper";
        dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
        for(var i=1;i<this.maxCount;i++){
       
          this["arr"+i][0].visible=true
          this["arr"+i][1].visible=false
          this["arr"+i][2].visible=false
          this["arr"+i][3].visible=false
          this["arr"+i][4].visible=true
          this["arr"+i][5].visible=true
       
         
            this["arr"+i][0].loadTexture("Filterpaper1");
            this["arr"+i][0].y-=35
            var tweens=this.game.add.tween(this["arr"+i][0].scale).to({x:this.paperscale,y:this.paperscale}, 1500, Phaser.Easing.Linear.Out, true);
        }
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.visiblearrowLast3,this);
     
      
       arrow.animations.play('allframes', 25,true);
       arrow.x=this.maingroup.children[0].x+125;
       arrow.y=this.maingroup.children[0].y-210
       arrow1.animations.play('allframes', 25,true);
       arrow1.x=this.maingroup.children[1].x+125;
       arrow1.y=this.maingroup.children[1].y-210
     
       this.game.time.events.add(Phaser.Timer.SECOND*10,this.LoadOtherPage,this);
      break;
   
      }
  },
  visiblearrowLast3:function(){
    arrow.visible=true;
    arrow1.visible=true;
  },
  LoadOtherPage:function(){
    
    this.exp+=1;
    arrow.visible=false;
    arrow1.visible=false;
    arrow2.visible=false;
    if(this.exp==4){
      voice.stop();
      voice.stop();
      voice=this.game.add.audio("A3_exp_procedure15",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text=dialog_text="Click on the next button to see the stability of solutions";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
      play.visible=true;
    }else{
      voice.stop();
   
    dialog_text="";
    dialog.destroy();
    this.maingroup.removeAll(true);
    this.maingroup.destroy();
    arrow.visible=false;
    arrow1.visible=false;
    arrow2.visible=false;
    arrow.destroy();
    arrow1.destroy();
    arrow2.destroy();
      this.maingroup=this.game.add.group();
      this.ChangeText();
      this.arrangeScene();
      this.startExperiment();
      this.ArrowPart()
    }
  },
  /////////////////////////////////////////////////////////
  //For to next scene
   
        toNextScene:function()
        {
        voice.destroy();
        
        this.state.start("Procedure3", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
        volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
        gotoHome:function()
        {
          voice.destroy();
          
          this.state.start("Aim", true, false, ip);
        },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  //To reset the game
  resetTheGame:function(){
    voice.destroy();
    
      this.state.start("Experiment_A3",true,false);
   },
   // To quit the experiment
  
  closeTheGame:function()
  {
    voice.destroy();
    
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  