var experiment_a5 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;

var arrow;


var DeltaTime;




}

experiment_a5.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  this.arrowArr=new Array();
  this.arrowArr=[,];
  this.arrowpos=0;
  currentobj=null;
  this.dropped=false;

  dialogX=20;
  dialogY=80;
  
  this.DeltaTime=null;
  
 muted = false;
 voice=this.game.add.audio("obj",1);

 //this.arrangeScene();

 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(.241,.253);




  
 


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "38px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "35px Segoe UI", fill: "#FFFFFF", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

////////////////////////////////////////////////////////////////////
this.labelText=["A","B","C","D","E","F","G","H"];


this.maingroupback=this.game.add.group();
this.maingroupResult=this.game.add.group();
this.arrangeScene();
this.startExperiment();
 
      

 },
 // For Full screen checking.
  
 gofull: function()
 {

 if (this.game.scale.isFullScreen)
   {
    this.game.scale.stopFullScreen();
   }
 else
   {
   this.game.scale.startFullScreen(false);
   }  
 },
 
//start
 startExperiment:function(){
  this.procedure=0;
    voice.stop();
    voice=this.game.add.audio("A5_exp_procedure1",1);
    voice.play();
    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
    dialog_text="The conclusions are ";
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    this.gotoAddItems();
    
  
 },
 //arrange scenes
 arrangeScene:function(){
 //  var itemOnarr=new Array();

 var resultarr=["result1","result2","result3","settle_soil","settle_chalk","settle_sand","result7","result8"]
 var resultarrName=["Common\n  Salt","Sugar","Alum","Soil","Chalk\npowder","Fine\nsand","Starch","Egg\nalbumin"]

   for(var i=1;i<9;i++){
      this.beakerback=this.maingroupback.create(-80+(i*220),770, 'Beaker_Back');
      this.beakerback.anchor.set(.5);
      this.beakerback.scale.set(.8);

      this.beaker=this.maingroupResult.create(-80+(i*220),770, resultarr[i-1]);
      this.beaker.anchor.set(.5);
      this.beaker.scale.set(.8);
     /* this.cols=this.game.add.sprite(0,-35,"col1");
      this.cols.anchor.set(.5);
      this.cols.scale.setTo(4,3)
      this.cols.alpha=.2
      this.beaker.addChild(this.cols);*/
      if(i==1){
        var labeltext1=this.game.add.text(-70,-70,resultarrName[i-1],labelfontStyle1);
       }else if(i==8||i==5){
         var labeltext1=this.game.add.text(-60,-70,resultarrName[i-1],labelfontStyle1);
        }else {
       var labeltext1=this.game.add.text(-40,-70,resultarrName[i-1],labelfontStyle1);
      }
      this.beaker.addChild(labeltext1);
  }


   
  
 },

 //Enable each Item
 EnableItem:function(audiostr,textstr,indexstr){
  voice.stop();
  voice=this.game.add.audio(audiostr,1);
  voice.play();
  dialog_text=""
  dialog.destroy();
  dialog_text=textstr;
  dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
  console.log(audiostr+" kkk ");
  var tweens=this.game.add.tween(this.maingroupback.children[indexstr].scale).to({x:1,y:1}, 400, Phaser.Easing.Linear.Out, true);
  var tweens=this.game.add.tween(this.maingroupResult.children[indexstr].scale).to({x:1,y:1}, 400, Phaser.Easing.Linear.Out, true);
  
  this.procedure+=1;
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.goBackScaleOption,this)
 // this.gotoAddItems();
 },
 goBackScaleOption:function(){
  var tweens=this.game.add.tween(this.maingroupback.children[this.procedure-1].scale).to({x:.8,y:.8}, 400, Phaser.Easing.Linear.Out, true);
  var tweens=this.game.add.tween(this.maingroupResult.children[this.procedure-1].scale).to({x:.8,y:.8}, 400, Phaser.Easing.Linear.Out, true);
  this.gotoAddItems();
 },
 //Add items with text & audio
 gotoAddItems:function(){
  var audiotext;
  var dialogtext;
  var indexstr;
   switch(this.procedure){

      case 0:
          audiotext="A5_exp_procedure2";
          dialogtext="Common salt forms a true solution in water";
          indexstr=0
      break;
      case 1:
          audiotext="A5_exp_procedure3";
          dialogtext="Sugar forms a true solution in water";
          indexstr=1
      break;
      case 2:
          audiotext="A5_exp_procedure4";
          dialogtext="Alum forms a true solution in water";
          indexstr=2
      break;
      case 3:
          audiotext="A5_exp_procedure5";
          dialogtext="Soil forms a suspension in water";
          indexstr=3
      break;
      case 4:
          audiotext="A5_exp_procedure6";
          dialogtext="Chalk powder forms a suspension in water";
          indexstr=4
      break;

      case 5:
          audiotext="A5_exp_procedure7";
          dialogtext="Fine sand forms a suspension in water";
          indexstr=5
      break;

      case 6:
          audiotext="A5_exp_procedure8";
          dialogtext="Starch forms a colloidal solution in water";
          indexstr=6
      break;

      case 7:
          audiotext="A5_exp_procedure9";
          dialogtext="Egg albumin forms a colloidal solution in water";
          indexstr=7
      break;  
      case 8:
          audiotext="A5_exp_procedure10";
          dialogtext="Click on the next button to see the Observation table";
          voice.stop();
          voice=this.game.add.audio(audiotext,1);
          voice.play();
          dialog_text=""
          dialog.destroy();
          dialog_text=dialogtext;
          dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
          indexstr=8
          play.visible=true
      break;   
   }
  if(this.procedure<8){
   this.game.time.events.add(Phaser.Timer.SECOND*1.5,this.EnableItem,this,audiotext,dialogtext,indexstr);
  }
 },

      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A5",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
