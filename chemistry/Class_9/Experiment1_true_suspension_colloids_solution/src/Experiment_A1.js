var experiment_a1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;

var arrow;


var DeltaTime;




}

experiment_a1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  this.arrowArr=new Array();
  this.arrowArr=[,];
  this.arrowpos=0;
  currentobj=null;
  this.dropped=false;

  dialogX=20;
  dialogY=80;
  
  this.DeltaTime=null;
  
 muted = false;
 voice=this.game.add.audio("obj",1);

 //this.arrangeScene();

 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(.241,.253);




  
 


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "38px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

////////////////////////////////////////////////////////////////////
this.labelText=["A","B","C","D","E","F","G","H"];
//this.inPlate=["Salt_in_Plate","Sugar_in_Plate","Alum_in_Plate","Soil_in_Plate","Sand_in_Plate","Chalkpower_in_Plate","Starch_in_Plate","Egg_in_Plate"];
this.inSpoon=["spoonsalt","spoonsugar","spoonalum","spoonsoil","spoonchalk","spoonsand","spoonstarch","spooneg"];
this.maingroupback=this.game.add.group();
this.maingroup=this.game.add.group();
this.maingroupRod=this.game.add.group();
this.maingroupResult=this.game.add.group();
this.arrangeScene();
this.intDailog();
 
      

 },
 // For Full screen checking.
  
 gofull: function()
 {

 if (this.game.scale.isFullScreen)
   {
    this.game.scale.stopFullScreen();
   }
 else
   {
   this.game.scale.startFullScreen(false);
   }  
 },
 //start exp
 intDailog:function(){

    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
    dialog_text="Lets perform the experiment";
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    voice=this.game.add.audio("lets_perform_Exp",1);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadExp,this);
   
 },
 loadExp:function()
 {  
     voice.play();
     this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment,this);
 },
 startExperiment:function(){
  this.procedure=0;
    voice.stop();
    voice=this.game.add.audio("A_exp_procedure1",1);
    voice.play();
    dialog_text="";
    dialog.destroy();
    dialog_text="Take 8 beakers with water and label them as A, B, C, D, E, F, G and H";
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.ShowLabels,this);


    
  
 },
 ArrowPart:function(){
  
    
    arrow=this.game.add.sprite(0,0, 'arrow');
    arrow.anchor.set(.5);
    arrow.animations.add('allframes');
    arrow_y=arrow.y;
    arrow.visible=false;
  
 },
 /// All Text used
 ProcedureText:function(){
  this.procedure+=1;
  switch(this.procedure){
    
    case 1:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure2",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Now add 1 gram of common salt into the beaker A";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 2:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure3",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker A";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 3:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure4",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Add 1 gram of sugar into the beaker B";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 4:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure5",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker B";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 5:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure6",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Add 1 gram of alum into the beaker C";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 6:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure7",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker C";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 7:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure8",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Add 1 gram of soil into the beaker D";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 8:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure9",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker D";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 9:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure10",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Add 1 gram of chalk powder into the beaker E";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 10:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure11",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker E";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 11:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure12",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Add 1 gram of sand into the beaker F";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 12:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure13",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in baker F";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 13:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure14",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Click the Beaker G to boil the water";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 14:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure15",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Now add starch into the beaker G";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 15:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure16",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker G";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 16:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure17",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Add egg albumin into the beaker H";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
    case 17:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure18",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Drag the glass rod to stir the contents in beaker H";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    break;
     case 18:
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure19",1);
      voice.play();
      dialog_text="";
      dialog.destroy();
      dialog_text="Click on the next button to see the transparency for each beakers";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
      play.visible=true;
    //  this.game.time.events.add(Phaser.Timer.SECOND*3,this.GotoExperiment2,this);
    break;
  }

 },
///Arrows position
 ArrowVisibility:function(labeli){
  arrow.destroy();
  this.ArrowPart();
  
  switch(this.arrowpos){
     case 1:
     case 5:
     case 9:  
     case 13:  
     case 17:  
     case 21:  
     case 26:
     case 30:    
        arrow.visible=true
        arrow.animations.play('allframes', 25,true);
        arrow.x=this.maingroup.children[3].x+135;
        arrow.y=this.maingroup.children[3].y+190;
      break;
      case 2:
      case 6:
      case 10:  
      case 14:  
      case 18:  
      case 22:  
      case 27: 
      case 31:  
        arrow.visible=true
        arrow.animations.play('allframes', 25,true);
        arrow.x=this.maingroup.children[labeli].x;
        arrow.y=this.maingroup.children[labeli].y-190;
      break;
      case 3:
      case 7:
      case 11:  
      case 15:  
      case 19: 
      case 23: 
      case 28:
      case 32:      
        arrow.visible=true
        arrow.animations.play('allframes', 25,true);
       /* if(labeli==7){
        arrow.x=this.maingroup.children[3].x+50;
        }else{
          arrow.x=this.maingroup.children[labeli].x+150;
          }*/
          arrow.x=this.maingroup.children[3].x+200;
        arrow.y=this.maingroup.children[labeli].y+220;
      break;
      case 4:
      case 8:
      case 12:  
      case 16:  
      case 20: 
      case 24:
      case 29: 
      case 33:    
        arrow.visible=true
        arrow.animations.play('allframes', 25,true);
        arrow.x=this.maingroup.children[labeli].x;
        arrow.y=this.maingroup.children[labeli].y-200;
      break;
      case 25:  
        arrow.visible=true
        arrow.animations.play('allframes', 25,true);
        arrow.x=this.maingroup.children[labeli].x;
        arrow.y=this.maingroup.children[labeli].y;
      break;

    
  }
},
///Scene Arrangement
 arrangeScene:function(){
 //  var itemOnarr=new Array();

 var itemOnarr=["salttobeaker","sugartobeaker","alumtobeakers","soiltobeakers","chalktobeaker","sandtobeakers","chalktobeaker","eggtobeakers"]
 var itemOnarrwithrod=["saltwithrod","sugarwithrod","alumwithrod","soilwithrod","chalkwithrod","sandwithrod","starchwithrod","eggwithrod"]
 //var rody=[687,675,690,677,677,677,677,677]
 var rody=[727,715,730,717,717,717,717,717]
   for(var i=1;i<9;i++){
      this.beakerback=this.maingroupback.create(-80+(i*220),740, 'Beaker_Back');
      this.beakerback.anchor.set(.5);
      this.beakerback.scale.set(.8);

      this.beaker=this.maingroup.create(-80+(i*220),740, itemOnarr[i-1]);
      this.beaker.anchor.set(.5);
      this.beaker.scale.set(.8);
      this.beaker.animations.add('frame1',[0]);
      this.beaker.animations.add('frameall');
      this.beaker.animations.play('frame1',10,false);

      this.colliderA=this.game.add.sprite(0,-150, 'collider');
      this.game.physics.arcade.enable(this.colliderA);
      this.beaker.addChild(this.colliderA);
      this.colliderA.anchor.set(.5);
      this.colliderA.scale.setTo(4,.5);
      this.colliderA.enableBody =false;
      this.colliderA.visible=false;
       this.colliderA.alpha=.0001;
      this.beaker.colliderOb=this.colliderA;


      var spoons=this.game.add.sprite(-50,-365, this.inSpoon[i-1]);
      this.beaker.addChild(spoons);

   
      this.beaker.spoonOb=spoons;
      spoons.visible=false


      this.beaker1=this.maingroupRod.create(-80+(i*220),rody[i-1], itemOnarrwithrod[i-1]);
      this.beaker1.anchor.set(.5);
      this.beaker1.scale.set(.8);
     
      
   
      this.beaker1.visible=false;

     this.mainbeaker=this.maingroupResult.create(-80+(i*220),740, "result"+i);
      this.mainbeaker.anchor.set(.5);
      this.mainbeaker.scale.set(.8);
      this.mainbeaker.visible=false;
     // this.beaker1.animations.add('frameall');
     // this.beaker1.animations.play('frame1',10,false);
    

   

  }
  
   this.labeli=0;
   
  
 },
 ShowLabels:function(){
    var labeltext1=this.game.add.text(this.maingroup.children[this.labeli].x,this.maingroup.children[this.labeli].y+170,this.labelText[this.labeli],headfontStyle);
   // this.maingroup.children[this.labeli].addChild(labeltext1);
    this.labeli++;
    if(this.labeli<this.labelText.length){
    this.game.time.events.add(Phaser.Timer.SECOND*.8,this.ShowLabels,this);
    }else{
      this.labeli=0;
      this.ArrowPart();
      this.game.time.events.add(Phaser.Timer.SECOND*.8,this.Gotoprcodeure1,this);
    
    }
   
},

Gotoprcodeure1:function(){
 

  this.ProcedureText();
  this.ItemsOntable();
 
},
//Drag items visibility
ItemsOntable:function(){
  
  this.item1=this.game.add.sprite( -100,1000, "inplate"+(this.labeli+1));
  this.item1.anchor.set(.5);
  this.item1.scale.set(.6);
  /*if(this.labeli==7){
    this.item1.xpos=this.maingroup.children[this.labeli].x-100
  }else{
  this.item1.xpos=this.maingroup.children[this.labeli].x;
  }*/
  this.item1.xpos=this.maingroup.children[3].x+80;
  this.item1.ypos=980;
 
  this.item11=this.game.add.sprite(-100,950, "Spoon");
  this.item11.anchor.set(.5);
  this.item11.angle=10
 
/*  if(this.labeli==7){
    this.item11.xpos=this.maingroup.children[this.labeli].x-50
  }else{
  this.item11.xpos=this.maingroup.children[this.labeli].x+50;
  }*/
  this.item11.xpos=this.maingroup.children[3].x+195;
  this.item11.ypos=this.item11.y;
  this.game.physics.arcade.enable(this.item11)
  this.item11.scale.set(.8);
  this.item11.body.enable=true;
  this.item11.events.onDragStart.add(function() {this.DragOn(this.item11)}, this);
  this.item11.events.onDragStop.add(function() {this.DropOut(this.item11)}, this);


  this.item12=this.game.add.sprite(-100,1010, "Rod");
  this.item12.anchor.set(.5);
  
 
  /*if(this.labeli==7){
    this.item12.xpos=this.maingroup.children[this.labeli].x+50
  }else{
  this.item12.xpos=this.maingroup.children[this.labeli].x+200;
  }*/
  this.item12.xpos=this.maingroup.children[3].x+110;
  this.item12.ypos=this.item12.y;
  this.item12.angle=90
  this.game.physics.arcade.enable(this.item12)
  this.item12.scale.set(.8);
  //  this.item12.body.enable=true;
    this.item12.events.onDragStart.add(function() {this.DragOn(this.item12)}, this);
    this.item12.events.onDragStop.add(function() {this.DropOut(this.item12)}, this);

    this.item12collider=this.game.add.sprite(-10,0, 'collider');
    this.game.physics.arcade.enable(this.item12collider);
    this.item12.addChild(this.item12collider);
    this.item12collider.anchor.set(.5);
    this.item12collider.scale.setTo(1,5);
    this.item12collider.enableBody =false;
    this.item12collider.alpha=.0001
    this.item12collider.visible=false;
    this.item12.collider=this.item12collider;
  this.game.add.tween( this.item1).to({x:  this.item1.xpos}, 500, Phaser.Easing.In, true);
  this.game.add.tween( this.item11).to({x:  this.item11.xpos}, 500, Phaser.Easing.In, true);
  var t= this.game.add.tween( this.item12).to({x:  this.item12.xpos}, 500, Phaser.Easing.In, true);
  t.onComplete.add(function () {
    this.item11.inputEnabled=true;
    this.item11.input.useHandCursor = true;
    this.item11.input.enableDrag(true);
    this.arrowpos+=1;
    this.ArrowVisibility(this.labeli)
    this.maingroup.children[this.labeli].colliderOb.enableBody=true;
    this.maingroup.children[this.labeli].colliderOb.visible=true;

  }.bind(this));
},
ItemsBack:function(){
  this.game.add.tween( this.item1).to({x:  -100}, 200, Phaser.Easing.In, true);
  this.game.add.tween( this.item11).to({x:  -100}, 200, Phaser.Easing.In, true);
  var t= this.game.add.tween( this.item12).to({x:  -100}, 200, Phaser.Easing.In, true);
  t.onComplete.add(function () {
    this.item1.destroy();
    this.item11.destroy();
    this.item12collider.destroy();
    
    this.item12.destroy();

    this.labeli+=1;
  /////this.labeli=6;
    if(this.labeli==6){
      this.arrowpos+=1;
    this.ArrowVisibility(this.labeli)
      this.maingroup.children[this.labeli].events.onInputDown.add(this.ClickBeaker, this);
      this.maingroup.children[this.labeli].inputEnabled=true;
      this.maingroup.children[this.labeli].input.useHandCursor=true;
      
    }else{
    if(this.labeli==8){
this.ProcedureText();
    }else{
      this.game.time.events.add(Phaser.Timer.SECOND,this.ItemsOntable,this);
      
    }
  }
  }.bind(this));
},
////G beaker clickabale
ClickBeaker:function(){
  arrow.visible=false;
  this.maingroup.children[this.labeli].inputEnabled=false;
  this.maingroup.children[this.labeli].input.useHandCursor=false;
  this.maingroup.children[this.labeli].visible=false;
  this.maingroupback.children[this.labeli].visible=false;
  this.fullBase=this.game.add.sprite(0,0, 'collider');
  this.fullBase.anchor.set(.5);
  this.fullBase.scale.setTo(1922,1081);
  this.fullBase.alpha=.4
  this.popupgroup=this.game.add.group();
  this.popupBase= this.popupgroup.create(280,250,'dialogue_box');
  this.popupBase.scale.setTo(.8);
  this.tripod= this.game.add.sprite(550,350,'tripod');
  this.tripod.scale.set(.9);
  this.popupBase.addChild(this.tripod)
  this.burner= this.game.add.sprite(-130,348,'burner');
   this.burner.anchor.set(.5);
   this.tripod.addChild( this.burner);
   this.flame= this.game.add.sprite(210,138,'flame');
   this.flame.animations.add("frameall");
   this.flame.animations.play("frameall",10,true);
   this.tripod.addChild( this.flame);
   this.onBeakerBack= this.game.add.sprite(109,-280,'Beaker_Back');
   this.tripod.addChild( this.onBeakerBack);
   this.onBeakerBack.scale.set(.9);
   this.onBeaker= this.game.add.sprite(109,-282,'starchwaterboiling');
   this.tripod.addChild( this.onBeaker);
   this.onBeaker.animations.add("frame1",[0,1]);
   this.onBeaker.animations.add("frame2",[2,3,4,5,6]);
   this.onBeaker.animations.add("frameall",[7,8,9]);
   this.onBeaker.animations.play("frame1",5,true);
   this.game.time.events.add(Phaser.Timer.SECOND*2,this.gotoMiddleBoiling,this)
  this.onBeaker.scale.set(.9);
 
},
gotoMiddleBoiling:function(){
  voice.stop();
  voice=this.game.add.audio("A_exp_procedure20",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="Water is boiling now";
  dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
  this.onBeaker.animations.play("frame2",5,true);
  this.game.time.events.add(Phaser.Timer.SECOND*2,this.gotoLastBoiling,this)
},
gotoLastBoiling:function(){

  this.onBeaker.animations.play("frameall",5,true);
  this.game.time.events.add(Phaser.Timer.SECOND*2,this.gotoBoiledBeaker,this)
},
gotoBoiledBeaker:function(){
  this.flame.destroy()
  this.burner.destroy()
  this.onBeakerBack.destroy()
  this.onBeaker.destroy()
  this.fullBase.destroy();
  this.tripod.destroy()
  this.popupBase.destroy();
  this.popupgroup.destroy();
  this.maingroup.children[this.labeli].visible=true;
  this.maingroupback.children[this.labeli].visible=true;
  this.maingroup.children[this.labeli].loadTexture("starchwaterboiling");
  this.maingroup.children[this.labeli].anchor.set(.5);
  this.maingroup.children[this.labeli].scale.set(.8);
 // this.maingroup.children[this.labeli].y=677
  this.maingroup.children[this.labeli].animations.add('frame1',[7,8,9]);

  this.maingroup.children[this.labeli].animations.play('frame1',5,true);
  this.ProcedureText();
  this.ItemsOntable();
  

},
//////////Drag & Drop ////
DragOn(obj){
   
 obj.body.enable =false;
  currentobj=obj;
  this.arrowpos+=1;
  this.ArrowVisibility(this.labeli)
   if(currentobj==this.item11)
  {
    this.item11.loadTexture(this.inSpoon[this.labeli]);
  this.item11.x=this.item11.xpos+20
  this.item11.y=this.item11.ypos-20
  this.item11.animations.add('frame1',[0]);
  this.item11.animations.play('frame1',10,false);
  }
  if(currentobj==this.item12)
  {
    this.item12collider.enableBody=true;
    this.item12collider.visible=true;
    this.item12.angle=0
  }
  this.dropped=false;
},
DropOut(obj){
  
  obj.body.enable =true;
  this.dropped=true;

},
detectCollision:function(){
  if(this.maingroup.visible){
      if(this.maingroup.children[this.labeli].colliderOb!=null&&this.maingroup.children[this.labeli].colliderOb.enableBody && currentobj!=null&& currentobj==this.item11)
      {
          this.game.physics.arcade.overlap(this.item11, this.maingroup.children[this.labeli].colliderOb,function() {this.match_Obj()},null,this);
        
      } 
      if(this.maingroup.children[this.labeli].colliderOb!=null&&this.maingroup.children[this.labeli].colliderOb.enableBody && currentobj!=null&& currentobj==this.item12)
      {
          this.game.physics.arcade.overlap(this.item12collider, this.maingroup.children[this.labeli].colliderOb,function() {this.match_Obj()},null,this);
        
      } 
    }
  if(currentobj!=null && currentobj.body.enable){
    currentobj.reset(currentobj.xpos,currentobj.ypos);//
    this.dropped=false;
    this.arrowpos-=1;
    this.ArrowVisibility(this.labeli)
     if(currentobj==this.item11)
    {
      this.item11.loadTexture("Spoon");
    }
    if(currentobj==this.item12)
    {
      this.item12.angle=90
    }
  }
},
match_Obj:function(){

  currentobj.inputEnabled=false;
  currentobj.input.enableDrag(false);
  currentobj.body.enable=false;
  arrow.visible=false;
  if(this.maingroup.visible){
    if(currentobj==this.item11)
    {
      
        this.item11.visible=false;
        if(this.labeli==6){
            
            this.maingroup.children[this.labeli].loadTexture("starchbolied");
            this.maingroup.children[this.labeli].anchor.set(.5);
            this.maingroup.children[this.labeli].scale.set(.8);
            this.maingroup.children[this.labeli].y=722
            this.maingroup.children[this.labeli].spoonOb.visible=true;
              var anim=this.maingroup.children[this.labeli].spoonOb.animations.add('frame1');
              this.maingroup.children[this.labeli].spoonOb.animations.play("frame1",10,false);
            this.maingroup.children[this.labeli].animations.add('frame1');
            this.maingroup.children[this.labeli].animations.play('frame1',10,false);
            anim.onComplete.add(function () {
              this.visibilityOfrod()
            }.bind(this));
        }else{
              this.maingroup.children[this.labeli].animations.play("frameall",10,false);
              this.maingroup.children[this.labeli].spoonOb.visible=true;
              var anim=this.maingroup.children[this.labeli].spoonOb.animations.add('frame1');
              this.maingroup.children[this.labeli].spoonOb.animations.play("frame1",10,false);
              anim.onComplete.add(function () {
               
                this.visibilityOfrod()
              }.bind(this));
      }
    }
   else if(currentobj==this.item12)
    {
        this.item12.visible=false;
        if(this.labeli==6){
          this.item12collider.enableBody=false;
          this.item12collider.visible=false;
          this.maingroup.children[this.labeli].loadTexture("starchboliedwithrod");
          this.maingroup.children[this.labeli].anchor.set(.5);
          this.maingroup.children[this.labeli].scale.set(.8);
          var anim= this.maingroup.children[this.labeli].animations.add('frame1');
          this.maingroup.children[this.labeli].animations.play('frame1',10,false);
          anim.onComplete.add(function () {
            this.maingroup.children[this.labeli].visible=false;
            this.DisablevisibilityOfrod();
          
          }.bind(this));
        }else{
            this.item12collider.enableBody=false;
            this.item12collider.visible=false;
            this.maingroup.children[this.labeli].visible=false;
            this.maingroupRod.children[this.labeli].visible=true;
            var anim= this.maingroupRod.children[this.labeli].animations.add('frame1');
            this.maingroupRod.children[this.labeli].animations.play('frame1',10,false)
            anim.onComplete.add(function () {
              this.maingroupRod.children[this.labeli].visible=false;
              this.DisablevisibilityOfrod();
            
            }.bind(this));
        }
      
    }
  }
  currentobj=null;
  this.dropped=false;

},
///Visibility of rod parts
visibilityOfrod:function(){
  this.maingroup.children[this.labeli].spoonOb.visible=false;
  this.item11.visible=true;
  this.item11.reset(  this.item11.xpos,  this.item11.ypos)
  this.item11.inputEnabled=false;
  this.item11.input.useHandCursor = false;
  this.item11.input.enableDrag(false);
  this.item12.inputEnabled=true;
  this.item12.input.useHandCursor = true;
  this.item12.input.enableDrag(true);
  this.arrowpos+=1;
  this.item11.loadTexture("Spoon");
 // this.item11.x=this.item11.xpos-20
 // this.item11.y=this.item11.ypos+20
  this.ArrowVisibility(this.labeli)
  this.ProcedureText();
},
//after stirring action
DisablevisibilityOfrod:function(){
    this.item12.visible=true;
    this.item12.angle=90
    this.item12.reset(  this.item12.xpos,  this.item12.ypos);//
    this.item12.inputEnabled=false;
    this.item12.input.useHandCursor = false;
    this.item12.input.enableDrag(false);
    this.maingroupResult.children[this.labeli].visible=true;
   
    this.ItemsBack();
    this.ProcedureText();
},
update:function(){
  if(this.dropped){
  this.detectCollision();
  }
},
//For to next scene
      toNextScene:function()
      {
      voice.destroy();
      
     this.state.start("Procedure1", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
