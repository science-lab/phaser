var experiment_a2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;

var arrow;
var arrow1;
var arrow2;

var DeltaTime;




}

experiment_a2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

  this.dropped=false;
  currentobj=null;
 

  dialogX=20;
  dialogY=80;
  
  this.DeltaTime=null;
  
 muted = false;
 voice=this.game.add.audio("obj",1);

 bg= this.game.add.sprite(0, 0,'bg1');





  
 


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "38px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "65px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

////////////////////////////////////////////////////////////////////

this.exp=1;

this.ChangeText();
this.startExperiment();
  
      

 },
 // For Full screen checking.
  
 gofull: function()
 {

 if (this.game.scale.isFullScreen)
   {
    this.game.scale.stopFullScreen();
   }
 else
   {
   this.game.scale.startFullScreen(false);
   }  
 },
 
//Start exp
 startExperiment:function(){
  this.papergroupforExp1=this.game.add.group();


    this.groupforExp1=this.game.add.group();
   
  
    this.groupforExp1.alpha=0;
   
    this.papergroupforExp1.alpha=0;
   
  this.allenable=false;
    voice=this.game.add.audio(this.firstAudio,1);
    voice.play();
    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
    dialog_text=this.firstText
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
  
    
    this.arrangeForExp();

    this.arrangeScene();
    this.ArrowPart()
 },

ArrowPart:function(){
 
   
   arrow=this.game.add.sprite(0,0, 'arrow');
   arrow.anchor.set(.5);
   arrow.animations.add('allframes');
 
   arrow.visible=false;

   arrow1=this.game.add.sprite(0,0, 'arrow');
   arrow1.anchor.set(.5);
   arrow1.animations.add('allframes');
   
   arrow1.visible=false;

   arrow2=this.game.add.sprite(0,0, 'arrow');
   arrow2.anchor.set(.5);
   arrow2.animations.add('allframes');
   
   arrow2.visible=false;
 
},
 arrangeScene:function(){
 
  var tweens=this.game.add.tween(this.papergroupforExp1).to({alpha:1}, 2500, Phaser.Easing.Linear.Out, true);
  var tweens=this.game.add.tween(this.groupforExp1).to({alpha:1}, 2500, Phaser.Easing.Linear.Out, true);
  tweens.onComplete.add(function () {
  this.game.time.events.add(Phaser.Timer.SECOND*2,this.beakerExp1,this);
    
  
  }.bind(this))
 },
 beakerExp1:function(){
  voice=this.game.add.audio("A2_exp_procedure2",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="First place the paper under each beaker";
  dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
 this.ShowExp1();
 
 },
 ///Arrangements 
 arrangeForExp:function(){
 
  
  for(var i=1;i<this.maxCount;i++){
   //900
   
    this.paper1=this.papergroupforExp1.create((i*this.xwidth)-500,650,"Paper");
    this.paper1.anchor.set(.5);
    this.paper1.scale.setTo(.7,.7);
   // this.paper1.xpos=(i*600)-200;

    this.beaker= this.groupforExp1.create((i*this.xwidth)-250,650,this.labelfunnelItem[i-1]);
    this.beaker.anchor.set(.5);
    this.beaker.scale.set(.7);
    
    this["labeltext"+i]=this.game.add.text(0,-350,this.labelText[i-1],headfontStyle)
    this.beaker.addChild(this["labeltext"+i]);
    
  }

 },
 //Moving actions
 ShowExp1:function(){
  for(var i=1;i<this.maxCount;i++){
    var tweens2=this.game.add.tween(this.groupforExp1.children[i-1].scale).to({x:.8,y:.8}, 500, Phaser.Easing.Linear.Out, true);
  
  }
  this.game.time.events.add(Phaser.Timer.SECOND,this.MovePaper,this);
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.BeakerOriginalPosition,this);
    
 },
 MovePaper:function(){
  for(var i=1;i<this.maxCount;i++){
    var x1=this.papergroupforExp1.children[i-1].x+260;
  var tweens1=this.game.add.tween(this.papergroupforExp1.children[i-1]).to({x:x1}, 500, Phaser.Easing.Linear.Out, true);
 

  }
  if(this.exp==1){
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.CheckTheTransparency1,this);
  }else if(this.exp==2){
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.CheckTheTransparency2,this);
    }else if(this.exp==3){
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.CheckTheTransparency3,this);
      }
 },
 BeakerOriginalPosition:function(){
  for(var i=1;i<this.maxCount;i++){
    
    var tweens2=this.game.add.tween(this.groupforExp1.children[i-1].scale).to({x:.7,y:.7}, 500, Phaser.Easing.Linear.Out, true);
   
   }
  
  },
  

////////////////To change for all 3 process/////////////////////////////
 /////////////////////////////////////////////////
 ChangeText:function(){
  if(this.exp==1){
    this.labelText=["A","B","C"];
    this.firstText="Find the transparency of the beaker A, B and C"
    this.xwidth=600;
    this.firstAudio="A2_exp_procedure1"
    this.maxCount=4
    this.labelfunnelItem=["alum_beaker","alum_beaker","alum_beaker"];
  } if(this.exp==2){
    this.labelText=["D","E","F"];
    this.maxCount=4
    this.xwidth=600;
    this.firstAudio="A2_exp_procedure4"
    this.firstText="Find the transparency of the beaker D, E and F"
    this.labelfunnelItem=["soil_beaker","chalkpowder_beaker","sand_beaker"];
    }
    if(this.exp==3){
      this.labelText=["G","H"];
      this.maxCount=3
      this.xwidth=800;
      this.firstAudio="A2_exp_procedure6"
      this.firstText="Find the transparency of the beaker G and H"
      this.labelfunnelItem=["starch_beaker","egg_beaker"];
      }
},
CheckTheTransparency1:function(){
  voice=this.game.add.audio("A2_exp_procedure3",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="Now we can see that the mark is clearly visible through solutions A, B and C.\nHence these solutions are transparent.";
  dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
  arrow1.visible=true;
  arrow2.visible=true;
  arrow.visible=true;
  
  arrow.x=this.groupforExp1.children[0].x+10
  arrow.y=this.groupforExp1.children[0].y-50
  arrow.animations.play("allframes",20,true);
  arrow1.x=this.groupforExp1.children[1].x+10
  arrow1.y=this.groupforExp1.children[1].y-50
  arrow1.animations.play("allframes",20,true);
  arrow2.x=this.groupforExp1.children[2].x+10
  arrow2.y=this.groupforExp1.children[2].y-50
  arrow2.animations.play("allframes",20,true);
  this.game.time.events.add(Phaser.Timer.SECOND*9,this.LoadOtherPage,this);
},
CheckTheTransparency2:function(){
  voice=this.game.add.audio("A2_exp_procedure5",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="We can see that the mark is not visible through solutions D, E and F.\nHence these solutions are not transparent but are opaque. ";
  dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
  arrow1.visible=true;
  arrow2.visible=true;
  arrow.visible=true;
  
  arrow.x=this.groupforExp1.children[0].x+10
  arrow.y=this.groupforExp1.children[0].y-50
  arrow.animations.play("allframes",20,true);
  arrow1.x=this.groupforExp1.children[1].x+10
  arrow1.y=this.groupforExp1.children[1].y-50
  arrow1.animations.play("allframes",20,true);
  arrow2.x=this.groupforExp1.children[2].x+10
  arrow2.y=this.groupforExp1.children[2].y-50
  arrow2.animations.play("allframes",20,true);
  this.game.time.events.add(Phaser.Timer.SECOND*10,this.LoadOtherPage,this);
},
CheckTheTransparency3:function(){
  voice=this.game.add.audio("A2_exp_procedure7",1);
  voice.play();
  dialog_text="";
  dialog.destroy();
  dialog_text="We can see that the mark is not clearly visible through solutions G and H.\nHence these solutions are not transparent but are translucent. ";
  dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
  arrow1.visible=true;
 
  arrow.visible=true;
  
  arrow.x=this.groupforExp1.children[0].x+10
  arrow.y=this.groupforExp1.children[0].y-50
  arrow.animations.play("allframes",20,true);
  arrow1.x=this.groupforExp1.children[1].x+10
  arrow1.y=this.groupforExp1.children[1].y-50
  arrow1.animations.play("allframes",20,true);
   this.game.time.events.add(Phaser.Timer.SECOND*10,this.LoadOtherPage,this);
},
LoadOtherPage:function(){
  
  this.exp+=1;
  arrow.visible=false;
  arrow1.visible=false;
  arrow2.visible=false;
  if(this.exp==4){
    voice.stop();
    voice=this.game.add.audio("A2_exp_procedure8",1);
    voice.play();
    dialog_text="";
    dialog.destroy();
    dialog_text=dialog_text="Click on the next button to see whether the solute particles can be\nseparated by filtration";
    dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
    play.visible=true;
  }else{
    voice.stop();
 
  dialog_text="";
  dialog.destroy();
  this.groupforExp1.removeAll(true);
  this.groupforExp1.destroy();
   this.papergroupforExp1.removeAll(true);
  this.papergroupforExp1.destroy();
 
  arrow.destroy();
  arrow1.destroy();
  arrow2.destroy();
    this.ChangeText();
    this.startExperiment();
  }
},
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Procedure2", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A2",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
