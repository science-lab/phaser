var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var exp_Name;
  var currentScene;
  var previousScene;
  
  }
  
  materials.prototype ={
  
  init: function(exp_nam) 
  {
    //ip = ipadrs;
    //exp_Name=exp_nam;

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1610,880,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,880,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(310,880,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,880,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScenes, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
        //if(exp_Name=="Iron with copper sulphate"){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                mat_img_2.destroy(true);
                mat_text_2.destroy(true);
              }
              mat_img_1 = this.game.add.sprite(260,380,'Beaker_Back');
              mat_img_1.scale.set(.8);
              mat_text_1=this.game.add.text(550,470,"",fontStyle);
              mat_img_2 = this.game.add.sprite(260,380,'mainBeaker');
              mat_img_2.scale.set(.8);
              mat_text_2=this.game.add.text(550,470,"",fontStyle);

             
              mat_name_1=this.game.add.text(550,370,"Beaker:",headfontStyle);
              mat_content_1="Beakers are useful as a reaction container or to hold \nliquid or solid samples. They are also used to catch \nliquids from titrations and filtrates from filtering \noperations."
              mat_text_1=this.game.add.text(550,490,mat_content_1,fontStyle);
              
              break;
          case 2:
              mat_text_2.destroy(true);
          
              mat_img_2.destroy(true);
             mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(260,380,'Beaker_Back');
              mat_img_1.scale.setTo(.8);
              mat_img_2 = this.game.add.sprite(260,380,'water');
              mat_img_2.scale.set(.8);
              mat_name_1=this.game.add.text(550,370,"Distilled water:",headfontStyle);
             
              mat_content_1="Distilled water is a type of purified water that has \nhad both contaminants and minerals removed."
              mat_text_1=this.game.add.text(550,490,mat_content_1,fontStyle);
              
          break;
          case 3:
              mat_text_2.destroy(true);
              mat_img_1.destroy(true);
              mat_img_2.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(260,380,'Stand');
              mat_img_1.scale.setTo(.5,.5);
            
              mat_name_1=this.game.add.text(550,370,"Clamp stand:",headfontStyle);
              mat_content_1="In chemistry, a retort stand, also called a clamp stand, a ring \nstand, or a support stand, is a piece of scientific equipment \nintended to support other pieces of equipment and glassware \nfor instance, burettes, test tubes and flasks.";
              mat_text_1=this.game.add.text(550,490,mat_content_1,fontStyle);
          break;
       
          case 4:
              mat_text_2.destroy(true);
              mat_img_2.destroy(true);
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_text_2.destroy(true);
              mat_img_1 = this.game.add.sprite(250,500,'Spoon');
            //  mat_img_1.angle=90
              mat_img_1.scale.set(.8);
              mat_name_1=this.game.add.text(550,370,"Spatula:",headfontStyle);
              mat_content_1="Spatula is a small stainless steel utensil, used for scraping,\ntransferring, or applying powders and paste-like chemicals or\ntreatments.";
              mat_text_1=this.game.add.text(550,490,mat_content_1,fontStyle);
          break;
          case 5:
              mat_img_2.destroy(true);
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_text_2.destroy(true);
              mat_img_1 = this.game.add.sprite(260,400,'Funnel');
              mat_img_1.scale.setTo(.8,.8);
              
              mat_name_1=this.game.add.text(550,370,"Funnel:",headfontStyle);
              mat_content_1="A funnel is a tube or pipe that is wide at the top and narrow\nat the bottom, used for guiding liquid or powder into a\nsmall opening.";
              mat_text_1=this.game.add.text(550,500,mat_content_1,fontStyle);
          break;
         case 6:
              mat_img_2.destroy(true);
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_text_2.destroy(true);
              mat_img_1 = this.game.add.sprite(240,470,'tripod');
              mat_img_1.scale.setTo(.5,.5);
              mat_name_1=this.game.add.text(550,370,"Tripod:",headfontStyle);
              mat_content_1="A laboratory tripod is a piece of three-legged equipment\ncommonly used to conduct experiments in laboratories.\nIt is used as a platform to hold and support glassware, such as\nbeakers and flasks, during experiments and when the\nglassware is not in use";
              mat_text_1=this.game.add.text(550,500,mat_content_1,fontStyle);
          break;
          case 7:
            mat_img_2.destroy(true);
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_text_2.destroy(true);
            mat_img_1 = this.game.add.sprite(260,470,'Rod');
            mat_img_1.scale.setTo(.5,.5);
            mat_name_1=this.game.add.text(550,370,"Glass Rod:",headfontStyle);
            mat_content_1="A Glass rod, stirring rod or stir rod is a piece of laboratory\nequipment used to mix chemicals and liquids for laboratory\npurposes.";
            mat_text_1=this.game.add.text(550,500,mat_content_1,fontStyle);
        break;
        case 8:
          mat_img_2.destroy(true);
          mat_img_1.destroy(true);
          mat_name_1.destroy(true);
          console.log(previousScene);
          if(previousScene==9){
          mat_img_3.destroy(true);
          mat_img_4.destroy(true);
          mat_img_5.destroy(true);
          mat_img_6.destroy(true);
          mat_img_7.destroy(true);
          mat_img_8.destroy(true);
     
          mat_name_11.destroy(true);
          mat_name_21.destroy(true);
          mat_name_31.destroy(true);
          mat_name_41.destroy(true);
          mat_name_51.destroy(true);
          mat_name_61.destroy(true);
          mat_name_71.destroy(true);
          mat_name_81.destroy(true);
          }
          mat_text_1.destroy(true);
          mat_text_2.destroy(true);
         
         

          mat_img_1 = this.game.add.sprite(260,470,'Filterpaper1');
          mat_img_1.scale.setTo(.8,.8);
          mat_name_1=this.game.add.text(550,370,"Filter paper:",headfontStyle);
          mat_content_1="Filter paper is a porous paper used especially for filtering."
          mat_text_1=this.game.add.text(550,500,mat_content_1,fontStyle);
      break;
      case 9:
        mat_img_2.destroy(true);
        mat_img_1.destroy(true);
        mat_name_1.destroy(true);
        mat_text_1.destroy(true);
        mat_text_2.destroy(true);
        mat_img_1 = this.game.add.sprite(400,470,'inplate1');
        mat_img_1.scale.setTo(.5,.5);
        mat_name_11=this.game.add.text(370,570,"Common-Salt",fontStyle);

        mat_img_2 = this.game.add.sprite(700,470,'inplate2');
        mat_img_2.scale.setTo(.5,.5);
        mat_name_21=this.game.add.text(770,570,"Sugar",fontStyle);

        mat_img_3 = this.game.add.sprite(1000,470,'inplate3');
        mat_img_3.scale.setTo(.5,.5);
        mat_name_31=this.game.add.text(1080,570,"Alum",fontStyle);

        mat_img_4 = this.game.add.sprite(1300,470,'inplate4');
        mat_img_4.scale.setTo(.5,.5);
        mat_name_41=this.game.add.text(1400,570,"Soil",fontStyle);

        mat_img_5 = this.game.add.sprite(400,700,'inplate5');
        mat_img_5.scale.setTo(.5,.5);
        mat_name_51=this.game.add.text(370,800,"Chalk powder",fontStyle);

        mat_img_6 = this.game.add.sprite(700,700,'inplate6');
        mat_img_6.scale.setTo(.5,.5);
        mat_name_61=this.game.add.text(760,800,"Fine Sand",fontStyle);

        mat_img_7 = this.game.add.sprite(1000,700,'inplate7');
        mat_img_7.scale.setTo(.5,.5);
        mat_name_71=this.game.add.text(1080,800,"Starch",fontStyle);

        mat_img_8 = this.game.add.sprite(1300,700,'inplate8');
        mat_img_8.scale.setTo(.5,.5);
        mat_name_81=this.game.add.text(1300,800,"Egg albumin",fontStyle);

        mat_name_1=this.game.add.text(550,370,"Materials required to prepare solutions:",headfontStyle);
        mat_content_1="";
        mat_text_1=this.game.add.text(550,500,mat_content_1,fontStyle);
    break;
          // case 8:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(230,350,'hydrochloric_acid');
          //     mat_img_1.scale.setTo(.8,.8);
          //     mat_name_1=this.game.add.text(700,370,"A piece of sandpaper:",headfontStyle);
          //     mat_content_1="Dilute hydrochloric acid is often used in the \nextraction of basic substances from mixtures or \nin the removal of basic impurities. The dilute \nacid converts the base such as ammonia or an \norganic amine into water-soluble chloride salt.";
          //     mat_text_1=this.game.add.text(700,470,mat_content_1,fontStyle);
          // break;
          // case 9:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(300,400,'Red_Litmus');
          //     mat_img_1.scale.setTo(.8,.8);
          //     mat_name_1=this.game.add.text(700,370,"Litmus solution(blue/red):",headfontStyle);
          //     mat_content_1="Red and blue litmus solution Indicators are used \nto determine whether substances are acids, bases, \nor neutral.";
          //     mat_text_1=this.game.add.text(700,470,mat_content_1,fontStyle);
          // break;
          // case 10:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(230,350,'Zinc');
          //     mat_img_1.scale.setTo(.8,.8);
          //     mat_name_1=this.game.add.text(700,370,"Zinc metal granules:",headfontStyle);
          //     mat_content_1="Zinc granules are the solid crystals of zinc. ";
          //     mat_text_1=this.game.add.text(700,470,mat_content_1,fontStyle);
          // break;
          // case 11:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(230,350,'sodium_bicarbonate');
          //     mat_img_1.scale.setTo(.25,.2);
          //     mat_name_1=this.game.add.text(650,370,"Sodium bicarbonate:",headfontStyle);
          //     mat_content_1="Sodium bicarbonate, commonly known as \nbaking soda or bicarbonate of soda, is a \nchemical compound with the formula NaHCO₃.";
          //     mat_text_1=this.game.add.text(650,470,mat_content_1,fontStyle);
          // break;
          // case 12:
          //     mat_img_1.destroy(true);
          //     mat_name_1.destroy(true);
          //     mat_text_1.destroy(true);
          //     mat_img_1 = this.game.add.sprite(270,350,'lemonjuice');
          //     mat_img_1.scale.setTo(1.2,1.2);
          //     mat_name_1=this.game.add.text(650,370,"Lemon juice:",headfontStyle);
          //     mat_content_1="The major acid in lemons is citric acid, which \nconstitutes around 5 to 6% of the lemon's juice. \nOther acids are also present, although in much \nlower concentrations than citric acid. Malic acid is \none of these, present at around 5% of the \nconcentration of citric acid.";
          //     mat_text_1=this.game.add.text(650,470,mat_content_1,fontStyle);
          // break;

          
          
          }
          

        //}
      },
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==9){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
        toNextScenes:function()
        {
      ///////  voice.destroy();
        this.state.start("Theory", true, false, ip);
        
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  