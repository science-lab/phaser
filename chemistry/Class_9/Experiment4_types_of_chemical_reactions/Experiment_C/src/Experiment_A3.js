var experiment_a3 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var HCl_step;
var colorChangingAnimstart;
var TostartTesttubeB;
}

experiment_a3.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 voice2=this.game.add.audio('sim_2',1);
  procedure_voice1=this.game.add.audio('A3_procedure1',1);
  procedure_voice2=this.game.add.audio('A3_procedure2',1);
  procedure_voice3=this.game.add.audio('A3_procedure3',1);
  procedure_voice4=this.game.add.audio('A3_procedure4',1);
  procedure_voice5=this.game.add.audio('A3_procedure5',1);
  procedure_voice6=this.game.add.audio('A3_procedure6',1);
  procedure_voice7=this.game.add.audio('A3_procedure7',1);
  procedure_voice8=this.game.add.audio('A3_procedure8',1);
  procedure_voice9=this.game.add.audio('A3_procedure9',1);
//  procedure_voice10=this.game.add.audio('A_procedure10',1);
//  procedure_voice11=this.game.add.audio('A_procedure11',1);
 
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 HCl_step=0;
 colorChangingAnimstart=false;
 delay=0;
 TostartTesttubeB=false;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  /*base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_step_1="1. Take 2 grams of ferrous sulphate crystals in a boiling tube. ";
  procedure_step_2="2. Heat the boiling tube on the bunsen burner. ";
  procedure_step_3="3. Observe the colour of the residue.";
  procedure_text=this.game.add.text(250,250,"Procedure:",headfontStyle);
  procedure_step_text_1=this.game.add.text(300,350,procedure_step_1,fontStyle);
  procedure_step_text_2=this.game.add.text(300,430,procedure_step_2,fontStyle);
  procedure_step_text_3=this.game.add.text(300,510,procedure_step_3,fontStyle);*/



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toObservations, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

  this.arrangeScene();

  dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.82,.8);
      dialog_text="Take the test tube to the clamp stand";
      procedure_voice1.play();
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
////////////////////////Add Items on the scene///////////////////////////
arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
 //holder1.visible=false;
 //holder2.visible=false;

 A3_TestTube_A_Back = this.game.add.sprite(305, 525,'A3_testtube_back');
 A3_TestTube_A_Back.scale.setTo(.7,1);
 A3_TestTube_A_Back.visible=false;
 
 A3_testTube_NaCO3=this.game.add.sprite(305, 530,'NaCO3_testtube_sprite','Sodium_to_testube0001.png');
 A3_testTube_NaCO3.scale.setTo(.7,.9);
 A3_testTube_NaCO3.visible=false;
 A3_testTube_NaCO3.animations.add('anim',[1,2,3,4,5,6,7,8,9],3,false,true);
  //TestTube_B_Back = this.game.add.sprite(505, 560,'TestTube_Nill_Back');
  //TestTube_B_Back.scale.setTo(.9,1);
  //TestTube_B_Back.visible=false;
  A3_cork_tube_sprite=this.game.add.sprite(285,450,'Glass_tube_sprite','Rod0001.png');
  A3_cork_tube_sprite.scale.setTo(.75,.7);
  A3_cork_tube_sprite.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],8,true,true);
  A3_cork_tube_sprite.visible=false;

  A3_cork_tube=this.game.add.sprite(1125,850,'A3_Cork_tube');
  A3_cork_tube.anchor.setTo(.5,.5);
  A3_cork_tube.scale.setTo(.75,.7);
  A3_cork_tube.angle=45;
  A3_cork_tube.visible=false;
  A3_cork_tube.events.onDragStart.add(function() {this.onDragStart(A3_cork_tube)}, this);
  A3_cork_tube.events.onDragStop.add(function() {this.onDragStop(A3_cork_tube)}, this);
  A3_cork_tube.inputEnabled = true;
  A3_cork_tube.input.useHandCursor = true;
  //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
  A3_cork_tube.input.enableDrag(true);
  this.game.physics.arcade.enable(A3_cork_tube);
  A3_cork_tube.enableBody =true;



  A3_testTube_HCl=this.game.add.sprite(305, 530,'HCl_testtube_sprite','HCL_to_Na0001.png');
  A3_testTube_HCl.scale.setTo(.7,.9);
  A3_testTube_HCl.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,
    16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,
  41,42,443,44,45,46,47,48,49,50],30,false,true);
  A3_testTube_HCl.visible=false;
  A3_testTube_HCl.animations.add('anim2',[45,46,47,48,49,50],30,true,true);

  A3_testTube_smoke=this.game.add.sprite(295, 600,'A3_white_smoke','White smoke0002.png');
  A3_testTube_smoke.scale.setTo(.4,.4);
  A3_testTube_smoke.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,
    16,17,18,19,20,21,22,23,24,25,26,27,28,29,30],24,true,true);
  A3_testTube_smoke.visible=false;

  A3_NaCO3=this.game.add.sprite(1120,750,'A3_NaCO3');
  A3_NaCO3.scale.setTo(.5,.8);
  A3_spoon=this.game.add.sprite(1200,680,'A3_Spoon');
  A3_spoon.scale.setTo(.8,.8);
  A3_spoon.angle=35;
 
  
  
  A3_spoon_NaCO3_sprite=this.game.add.sprite(290,420,'Spoon_sprite','Spoon0001.png');
  A3_spoon_NaCO3_sprite.scale.setTo(.8,.8);
  A3_spoon_NaCO3_sprite.animations.add('anim',[1,2,3,4],3, false, true);
  A3_spoon_NaCO3_sprite.visible=false;

 

  //A3_HCl=this.game.add.sprite(1120,750,'A3_NaCO3')
  
  
  
  
 // RedJar_Back = this.game.add.sprite(1325, 598,'Jar_Back');
  //RedJar_Back.scale.setTo(.3,.4);
 // Dropper_Red = this.game.add.sprite(1380, 550,'Dropper_Red_OUT','Dropper_Red_OUT_0016.png');
 // Dropper_Red.scale.setTo(.8,1);
 // Red_Litmus = this.game.add.sprite(1330, 600,'Red_Litmus_front');
//Red_Litmus.scale.setTo(.8,1);
 // Red_Litmus_text = this.game.add.text(1360,790,'Red\nlitmus',labelfontStyle1)

 // BlueJar_Back = this.game.add.sprite(1145, 598,'Jar_Back');
 // BlueJar_Back.scale.setTo(.3,.4);
 // Dropper_Blue = this.game.add.sprite(1200, 550,'Dropper_Blue_OUT','Dropper_Blue_OUT_0016.png');
 // Dropper_Blue.scale.setTo(.8,1);

  
  //Dropper_Blue.animations.add('anim',[,7,8,9,10,11,12,13,14,15,], 30, false, true);
//0,1,2,3,4,5,6
 // Dropper_Red.animations.add('anim',[7,8,9,10,11,12,13,14,15,], 30, false, true);
//0,1,2,3,4,5,6,
 // Dropper_Blue.events.onDragStart.add(function() {this.onDragStart(Dropper_Blue)}, this);
 // Dropper_Blue.events.onDragStop.add(function() {this.onDragStop(Dropper_Blue)}, this);
 // Dropper_Blue.inputEnabled = true;
 // Dropper_Blue.input.useHandCursor = true;
 // Dropper_Blue.input.enableDrag(true);
 // this.game.physics.arcade.enable(Dropper_Blue);
 // Dropper_Blue.enableBody =true;

  //Blue_Litmus = this.game.add.sprite(1150, 600,'Blue_Litmus_front');
 // Blue_Litmus.scale.setTo(.8,1);
 // Blue_Litmus_text = this.game.add.text(1170,790,'Blue\nlitmus',labelfontStyle1)


  HCl_Back = this.game.add.sprite(1505, 598,'Jar_Back');
  HCl_Back.scale.setTo(.3,.4);
 // BlueJar_Back.scale.setTo(.3,.4);
  A3_HCl = this.game.add.sprite(1510, 600,'HCl');
  A3_HCl.scale.setTo(.8,1);
  
  Dropper_HCl_1 = this.game.add.sprite(1535,300,'A3_Dropper_HCl_OUT','Filler out0001.png');
  Dropper_HCl_1.scale.setTo(.8,1);
  Dropper_HCl_1.xp=1535;
  Dropper_HCl_1.yp=300;
  Dropper_HCl_1.visible=false;
  Dropper_HCl_1.events.onDragStart.add(function() {this.onDragStart(Dropper_HCl_1)}, this);
  Dropper_HCl_1.events.onDragStop.add(function() {this.onDragStop(Dropper_HCl_1)}, this);
  Dropper_HCl_1.inputEnabled = true;
  Dropper_HCl_1.input.useHandCursor = true;
  Dropper_HCl_1.input.enableDrag(true);
  this.game.physics.arcade.enable(Dropper_HCl_1);
  Dropper_HCl_1.enableBody =true;
  

Dropper_HCl_1.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,], 30, false, true);
//0,1,2,3,4,5,6,

  HCl_front = this.game.add.sprite(1510, 600,'HCl_front');
  HCl_front.scale.setTo(.8,1);
  HCl_front.visible=false;
  corck = this.game.add.sprite(1300, 850,'cork');
  corck.scale.setTo(.8,1);
  corck.visible=false;
  HCl_text = this.game.add.text(1550,810,'HCl',labelfontStyle1);
  A3_NaCO3_text=this.game.add.text(1110,730,'Sodium carbonate',labelfontStyle1)

  Dropper_HCl = this.game.add.sprite(1700, 900,'A3_Dropper_HCl_OUT','Filler out0015.png');
  Dropper_HCl.scale.setTo(.8,1);
  Dropper_HCl.angle=90;

  litmusGroup=this.game.add.group();
 // litmusGroup.add(RedJar_Back);
  //litmusGroup.add(Dropper_Red);
  //Dropper_Red.visible=false;

  //litmusGroup.add(Red_Litmus);
  //litmusGroup.add(Red_Litmus_text);
 // litmusGroup.add(BlueJar_Back);
 // litmusGroup.add(Dropper_Blue);
  //Dropper_Blue.visible=false;
 // litmusGroup.add(Blue_Litmus);
 // litmusGroup.add(Blue_Litmus_text);
  litmusGroup.add(HCl_Back);
  litmusGroup.add(Dropper_HCl_1);
  litmusGroup.add(A3_HCl);
  litmusGroup.add(Dropper_HCl);
  litmusGroup.add(HCl_front);
  litmusGroup.add(HCl_text);
  litmusGroup.add(corck);
  litmusGroup.add(A3_NaCO3_text);
  
  litmusGroup.add(A3_NaCO3);
  litmusGroup.add(A3_spoon);
 



  litmusGroup.visible=false;
  litmusGroup.x=2500;
 /////////////////////////////////////////////////////////////////////
  rackGroup=this.game.add.group();

  holder1 = this.game.add.sprite(1350, 570,'holder1');
  //TestTube_Nill_Back_1 = this.game.add.sprite(1605, 360,'TestTube_Nill_Back');
  //TestTube_Nill_Back_1.scale.setTo(.8,.8);

  A3_spoon_NaCO3=this.game.add.sprite(1160,680,'Spoon_sprite','Spoon0001.png');
  A3_spoon_NaCO3.scale.setTo(.8,.8);
  A3_spoon_NaCO3.events.onDragStart.add(function() {this.onDragStart(A3_spoon_NaCO3)}, this);
  A3_spoon_NaCO3.events.onDragStop.add(function() {this.onDragStop(A3_spoon_NaCO3)}, this);
  A3_spoon_NaCO3.inputEnabled = true;
  A3_spoon_NaCO3.input.useHandCursor = true;
  A3_spoon_NaCO3.events.onInputDown.add(this.ClickOnSpoon, this);
  A3_spoon_NaCO3.input.enableDrag(true);
  this.game.physics.arcade.enable(A3_spoon_NaCO3);
  A3_spoon_NaCO3.enableBody =true;
  A3_spoon_NaCO3.alpha=0;



  A3_TestTube_1 = this.game.add.sprite(1455, 580,'A3_testtube1');
  A3_TestTube_1.scale.setTo(.7,.9);
  A3_TestTube_1.xp=1455;
  A3_TestTube_1.yp=580;

  A3_TestTube_1.events.onDragStart.add(function() {this.onDragStart(A3_TestTube_1)}, this);
  A3_TestTube_1.events.onDragStop.add(function() {this.onDragStop(A3_TestTube_1)}, this);
  A3_TestTube_1.inputEnabled = true;
  A3_TestTube_1.input.useHandCursor = true;
  //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
  A3_TestTube_1.input.enableDrag(true);
  this.game.physics.arcade.enable(A3_TestTube_1);
  A3_TestTube_1.enableBody =true;

  A3_TestTube_2 = this.game.add.sprite(1455,550,'A3_Lime_testtube_full');
  A3_TestTube_2.scale.setTo(.7,.9);
  A3_TestTube_2.events.onDragStart.add(function() {this.onDragStart( A3_TestTube_2)}, this);
  A3_TestTube_2.events.onDragStop.add(function() {this.onDragStop( A3_TestTube_2)}, this);
  A3_TestTube_2.inputEnabled = true;
  A3_TestTube_2.input.useHandCursor = true;
  //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
  A3_TestTube_2.input.enableDrag(true);
  this.game.physics.arcade.enable(A3_TestTube_2);
  A3_TestTube_2.enableBody =true;




  A3_TestTube_2_Back = this.game.add.sprite(430, 525,'A3_testtube_back');
  A3_TestTube_2_Back.scale.setTo(.7,1);

  A3_testTube_lime_sprite=this.game.add.sprite(430,530,'Lime_sprite','Lime_water_colourchange0001.png');
  A3_testTube_lime_sprite.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,
    16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35], 8, false, true);

    A3_testTube_lime_sprite.animations.currentAnim.onComplete.add(this.EndofProcedure, this);

  A3_testTube_lime_sprite.scale.setTo(.7,.9);
  A3_testTube_lime_sprite.visible=false;

A3_TestTube_2.visible=false;
A3_TestTube_2_Back.visible=false;



  /*TestTube_2.events.onDragStart.add(function() {this.onDragStart(TestTube_2)}, this);
  TestTube_2.events.onDragStop.add(function() {this.onDragStop(TestTube_2)}, this);
  TestTube_2.inputEnabled = true;
  TestTube_2.input.useHandCursor = true;
  TestTube_2.input.enableDrag(true);
  this.game.physics.arcade.enable(TestTube_2);
  TestTube_2.enableBody =true;*/

  holder2 = this.game.add.sprite(1350, 570,'holder2');
  holder1.scale.setTo(1.25,1.25);
  holder2.scale.setTo(1.25,1.25);
  rackGroup.add(holder1);
  rackGroup.add(A3_TestTube_1);
  rackGroup.add(A3_TestTube_2);
  rackGroup.add(holder2);
//rackGroup.add( A3_TestTube_2);
//rackGroup.add(A3_TestTube_2_Back);
  rackGroup.add(A3_cork_tube);



  TestTubeStand_Emty = this.game.add.sprite(50, 420,'TestTubeStand_Emty');
  TestTubeStand_Emty.scale.setTo(.9,1);

  //TestTubeStand_Hanger1_B = this.game.add.sprite(450, 385,'TestTubeStand_Hanger_B');
  //
  
  //TestTubeStand_Hanger2_B = this.game.add.sprite(650, 385,'TestTubeStand_Hanger_B');
  
  A3_TestTube_front =  this.game.add.sprite(305, 530,'A3_testtube_front');
  A3_TestTube_front.scale.setTo(.7,.9);
  // TestTube_A.animations.add('anim',[16,17,18,19,20,21,22,23,24,25,26,27,], 10, false, true);
  // TestTube_A.animations.add('anim_1',[28,29,30,31,32,33,34,35,36,37,38,39,], 10, false, true);
  // TestTube_A.animations.add('anim_2',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,], 30, false, true);
  

  //TestTube_A.animations.add('anim_1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,], 30, false, true);

  //anim = TestTube_A.animations.add('anim');
  //TestTube_A.scale.setTo(.9,1);
  //TestTube_A = this.game.add.sprite(455, 360,'TestTube_Nill');
  //TestTube_A.scale.setTo(.8,1);
  A3_TestTube_front.visible=false;

  // TestTube_A_Label=this.game.add.text(328,610,"A",labelfontStyle);
  // TestTube_A_Label.visible=false;
  

  
  
  TestTube_B =  this.game.add.sprite(505, 560,'TestTube_Red_in','00_TestTube_Red_Output_010001.png');
  
  TestTube_B.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,], 30, false, true);
  TestTube_B.animations.add('anim_1',[12,13,14,15,16,17,18,19,20,21,22,23,], 30, false, true);

  //TestTube_B = this.game.add.sprite(655, 360,'TestTube_Nill');
  TestTube_B.scale.setTo(.9,1);
  TestTube_B.visible=false;
  TestTube_B_Label=this.game.add.text(530,610,"B",labelfontStyle);
  TestTube_B_Label.visible=false;



  TestTubeStand_Hanger_A = this.game.add.sprite(300, 585,'TestTubeStand_Hanger_A');
  TestTubeStand_Hanger_A.scale.setTo(.85,1);
  
  TestTubeStand_Hanger_B = this.game.add.sprite(425, 585,'TestTubeStand_Hanger_A');
  TestTubeStand_Hanger_B.scale.setTo(.85,1);
  
      /*TestTubeStand_Hanger_B.inputEnabled = true;
      TestTubeStand_Hanger_B.enableBody =true;*/
 // collider=[TestTubeStand_Hanger_A,TestTubeStand_Hanger_B]; 


 collider_A = this.game.add.sprite(315, 555,'collider');
 collider_A.alpha=0;
 this.game.physics.arcade.enable(collider_A);//For colling purpose
      collider_A.inputEnabled = true;
      collider_A.enableBody =true;
 collider_B = this.game.add.sprite(440, 555,'collider');
 collider_B.alpha=0;
 collider_B.inputEnabled = true;
      collider_B.enableBody =true;
 this.game.physics.arcade.enable(collider_B);//For colling purpose
  
arrow=this.game.add.sprite(1500,450, 'arrow');
      arrow.angle=90;
      arrow_y=480;

/*arrow.visible=false;
        tween1=this.game.add.tween(rackGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
        tween1.onComplete.add(function(){this.tween_rackExit()}, this);*/

},

onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    if(currentobj==A3_TestTube_1){
      currentobj.xp=1455;
      currentobj.yp=550;
      arrow.x=355;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
      //collider=TestTubeStand_Hanger_A;
    }
    else if(currentobj==A3_cork_tube){
      //1125,650
      1125,850
      currentobj.xp=1125;
      currentobj.yp=850;
      arrow.x=355;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
      A3_cork_tube.angle=0;
      //collider=TestTubeStand_Hanger_B;
    }   
    else if(currentobj==A3_TestTube_2){
      //1125,650
      //1455,555
      console.log("sdbkjasjdhasbjdasgdagdjaghd");
      currentobj.xp=1455;
      currentobj.yp=550;
      arrow.x=470;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
      //A3_cork_tube.angle=0;
      //collider=TestTubeStand_Hanger_B;
    }
    else if(currentobj==A3_spoon_NaCO3){
      //Dropper_Blue.frameName = "Dropper_Blue_OUT_0006.png";
      currentobj.xp=1160;
      currentobj.yp=680;
      arrow.x=355;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
    }
    // else if(currentobj==Dropper_Red){
    //   Dropper_Red.frameName = "Dropper_Red_OUT_0006.png";
    //   currentobj.xp=1380;
    //   currentobj.yp=550;
    //   arrow.x=570;
    //   arrow.y=450;
    //   arrow_y=450;
    //   arrow.visible=true;
    // }
    else if(currentobj==Dropper_HCl_1){
      Dropper_HCl_1.frameName = "Dropper_HCl_OUT_0006.png";
      if(HCl_step==1){
        arrow.x=355;
      }else{
        arrow.x=570;
      }
      //1535,300
      currentobj.xp=1535;
      currentobj.yp=550;
      //1560,550
      
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
    } 
    
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
      ClickOnSpoon:function()
      {
        A3_spoon.visible=false;
        A3_spoon_NaCO3.alpha=1;

      },  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    
    if(currentobj==A3_TestTube_1){//
        currentobj.visible=false;
        A3_TestTube_A_Back.visible=true;
        A3_TestTube_front.visible=true;
        A3_TestTube_A_Back.visible=true;
        //TestTube_A_Label.visible=true;
        //TestTube_2.events.onDragStart.add(function() {this.onDragStart(TestTube_2)}, this);
        //TestTube_2.events.onDragStop.add(function() {this.onDragStop(TestTube_2)}, this);
        //TestTube_2.inputEnabled = true;
        //TestTube_2.input.useHandCursor = true;
        //TestTube_2.input.enableDrag(true);
       // this.game.physics.arcade.enable(TestTube_2);
       // TestTube_2.enableBody =true;
        A3_TestTube_1.enableBody =false;
        collider_A.enableBody=false;

        collider_B.inputEnabled = true;
        collider_B.enableBody =true;
        tween1=this.game.add.tween(rackGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
        tween1.onComplete.add(function(){this.tween_rackExit()}, this);
        // arrow.x=1580;
        // arrow.y=550;
        // arrow_y=550;
      }

      
    if(currentobj==A3_cork_tube){//
      currentobj.visible=false;
      A3_TestTube_2.visible=false;
      A3_cork_tube_sprite.visible=true;
      A3_testTube_lime_sprite.visible=true;
      A3_testTube_lime_sprite.animations.play('anim');
      A3_TestTube_2_Back.visible=true;

      arrow.visible=false;
      A3_cork_tube_sprite.animations.play('anim');
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.toLimewaterChange, this);
      
      /*dialog.text="Take the lime water to the other end of the delivery tube.";
      procedure_voice6.stop();
      procedure_voice7.play();
      arrow.visible=true;
      arrow.x=1500;
      arrow.y=500;
      arrow_y=500;*/
      //TestTube_A_Label.visible=true;
      //TestTube_2.events.onDragStart.add(function() {this.onDragStart(TestTube_2)}, this);
      //TestTube_2.events.onDragStop.add(function() {this.onDragStop(TestTube_2)}, this);
      //TestTube_2.inputEnabled = true;
      //TestTube_2.input.useHandCursor = true;
      //TestTube_2.input.enableDrag(true);
     // this.game.physics.arcade.enable(TestTube_2);
     // TestTube_2.enableBody =true;
     // A3_TestTube_1.enableBody =false;
      collider_A.enableBody=false;

      collider_B.inputEnabled = true;
      collider_B.enableBody =true;
      //tween1=this.game.add.tween(rackGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
     // tween1.onComplete.add(function(){this.tween_rackExit()}, this);
      // arrow.x=1580;
      // arrow.y=550;
      // arrow_y=550;
    }
      // else if(currentobj==TestTube_2){//
      //   //currentobj.visible=false;
      //   //TestTube_B_Back.visible=true;
      //   //TestTube_B.visible=true;
      //   //TestTube_B_Label.visible=true;
      //  // arrow.visible=false;
       
        
        

      // }
      else if(currentobj==A3_spoon_NaCO3){
        
        arrow.visible=false;
        //A3_spoon_NaCO3.x=320;
       // A3_spoon_NaCO3.y=450;
       A3_spoon_NaCO3.visible=false;
       A3_TestTube_front.visible=false;
       A3_spoon_NaCO3_sprite.visible=true;
       A3_spoon_NaCO3_sprite.animations.play('anim');
       A3_spoon_NaCO3_sprite.animations.visible=true;
       A3_testTube_NaCO3.visible=true;
       A3_testTube_NaCO3.animations.play('anim');

       A3_spoon_NaCO3_sprite.animations.currentAnim.onComplete.add(this.EndofSpoon_animation, this);
       A3_testTube_NaCO3.animations.currentAnim.onComplete.add(this.EndofTesttube_animation1, this);
       //Dropper_Blue.x=325;
        //Dropper_Blue.y=300;
        //Dropper_Blue.animations.play("anim");
        //TestTube_A.animations.play("anim");
        //anim.onComplete.add(animationStopped, this);
        //TestTube_A.animations.currentAnim.onComplete.add(this.animationStoppedA, this);
        collider_A.enableBody=false;

        collider_B.inputEnabled = true;
        collider_B.enableBody =true;
      }

      else if(currentobj==A3_TestTube_2){
        
      //  arrow.visible=false;
       // A3_testTube_lime_sprite.visible=true;
       // A3_TestTube_2_Back.visible=true;
       // A3_testTube_lime_sprite.animations.play('anim');
       // A3_TestTube_2.visible=false;
        A3_TestTube_2.x=A3_testTube_lime_sprite.x;
        A3_TestTube_2.y=A3_testTube_lime_sprite.y;
        arrow.visible=true;
        arrow.x=1125;
        arrow.y=610;
        arrow_y=610;
        A3_TestTube_1.visible=false;
        dialog.text="Take the cork with delivery tube and place it on the mouth of the \ntest tube.";
        procedure_voice5.stop();
        procedure_voice6.play();
        //A3_spoon_NaCO3.x=320;
       // A3_spoon_NaCO3.y=450;
      //  A3_spoon_NaCO3.visible=false;
      //  A3_TestTube_front.visible=false;
      //  A3_spoon_NaCO3_sprite.visible=true;
      //  A3_spoon_NaCO3_sprite.animations.play('anim');
      //  A3_spoon_NaCO3_sprite.animations.visible=true;
      //  A3_testTube_NaCO3.visible=true;
      //  A3_testTube_NaCO3.animations.play('anim');

      //  A3_spoon_NaCO3_sprite.animations.currentAnim.onComplete.add(this.EndofSpoon_animation, this);
      //  A3_testTube_NaCO3.animations.currentAnim.onComplete.add(this.EndofTesttube_animation1, this);
       //Dropper_Blue.x=325;
        //Dropper_Blue.y=300;
        //Dropper_Blue.animations.play("anim");
        //TestTube_A.animations.play("anim");
        //anim.onComplete.add(animationStopped, this);
        //TestTube_A.animations.currentAnim.onComplete.add(this.animationStoppedA, this);
        collider_A.enableBody=false;

        collider_B.inputEnabled = true;
        collider_B.enableBody =true;
      }
      // else if(currentobj==Dropper_Red){
      //   arrow.visible=false;
      //   Dropper_Red.x=525;
      //   Dropper_Red.y=300;
      //   Dropper_Red.animations.play("anim");
      //   TestTube_B.animations.play("anim");
      //   //anim.onComplete.add(animationStopped, this);
      //   TestTube_B.animations.currentAnim.onComplete.add(this.animationStoppedB, this);
      //   collider_B.enableBody=false;

      // }
      else if(currentobj==Dropper_HCl_1){
        arrow.visible=false;
        //if(HCl_step==1){
          Dropper_HCl_1.x=300;
          Dropper_HCl_1.y=300;
          Dropper_HCl_1.animations.play("anim");
          A3_testTube_HCl.visible=true;
          A3_testTube_HCl.animations.play('anim');
          A3_testTube_smoke.visible=true;
          A3_testTube_smoke.animations.play('anim');
          A3_testTube_NaCO3.visible=false;
          //TestTube_A.animations.play("anim_1");
          //Dropper_HCl_1.animations.play("anim");
          //anim.onComplete.add(animationStopped, this);
         // TestTube_A.animations.currentAnim.onComplete.add(this.animationStopped_HClA, this);
          A3_testTube_HCl.animations.currentAnim.onComplete.add(this.toStartHClanimation, this);
          Dropper_HCl_1.animations.currentAnim.onComplete.add(this.animationStopped_HClA, this);
         
         collider_A.enableBody=false;

          collider_B.inputEnabled = true;
          collider_B.enableBody =true;
        // }else{
        //   Dropper_HCl_1.x=525;
        //   Dropper_HCl_1.y=300;
        //   Dropper_HCl_1.animations.play("anim");
        //   TestTube_B.animations.play("anim_1");
        //   //Dropper_HCl_1.animations.play("anim");
        //   //anim.onComplete.add(animationStopped, this);
        //   TestTube_B.animations.currentAnim.onComplete.add(this.animationStopped_HClA, this);
        //   collider_B.enableBody=false;

        // }
        
        
      }
    /*if(currentobj==spoon){
      spoon.reset(spoon.xp,spoon.yp);
      spoon.frameName="spoon.png";
      test_tube1.frameName="test_tube0002.png";
      dialog.text="Drag the boiling tube for heating.";
      procedure_voice2.play();
      procedure_voice2.onStop.add(this.enableProcedure3,this);
      arrow.visible=false;
    }else if(currentobj==test_tube1){
      test_tube1.visible=false;
      testtube_h.visible=true; 
      //burner_on.visible=true;
      arrow.visible=false;
      dialog.text="Observe the colour of the residue.";
      procedure_voice3.play();

      fNames=['test_tube0002.png','test_tube0003.png','test_tube0004.png',
            'test_tube0005.png',
            'test_tube0006.png',
            'test_tube0007.png',
            'test_tube0008.png',  
            'test_tube0009.png',
            'test_tube0010.png',
            'test_tube0011.png',
            'test_tube0012.png'];
      
      interval=0;
      incr=0;
      heatFlag=true;
      burner.visible=false;
      burner_on=this.game.add.sprite(248,923,'burner_sprites','Burner_sprite_0.png');//
      burner_on.anchor.setTo(0.8,1);
      burner_on.animations.add('burner',['Burner_sprite_0.png',
            'Burner_sprite_1.png','Burner_sprite_2.png','Burner_sprite_3.png',
             ], 4, true, false);

      burner_on.scale.setTo(.8,.8);
      burner_on.animations.play('burner');
      //testtube_h.play('anim', false);
    }*/
    
  },
  toLimewaterChange:function(){
    A3_testTube_lime_sprite.visible=true;
    A3_TestTube_2_Back.visible=true;
    A3_testTube_lime_sprite.animations.play('anim');
    A3_TestTube_2.visible=false;
  },

  toStartHClanimation:function()
  {
A3_testTube_HCl.animations.play('anim2');

  },

  EndofSpoon_animation:function()
  {
    A3_spoon.visible=true;
    A3_spoon_NaCO3_sprite.visible=false;


  },
  EndofTesttube_animation1:function()
  {
   // Dropper_Red.x=1380;
   // Dropper_Red.y=550;
    dialog.text="Take a dropper";
    procedure_voice2.stop();
    procedure_voice3.play();
    arrow.x=1500;
    arrow.y=800;
    arrow_y=800;
    arrow.visible=true;
    Dropper_HCl.inputEnabled = true;
    Dropper_HCl.input.useHandCursor = true;
    Dropper_HCl.events.onInputDown.add(this.ClickOnDropperHCl, this);

  },

  
  animationStoppedA:function(){
    //tween1=this.game.add.tween(Dropper_Blue).to( {x:1200, y:550}, 600, Phaser.Easing.Out, true);
    //tween1.onComplete.add(function(){this.tween_testTubeA_Return()}, this);
    Dropper_Blue.x=1200;
    Dropper_Blue.y=550;
    Dropper_Red.events.onDragStart.add(function() {this.onDragStart(Dropper_Red)}, this);
    Dropper_Red.events.onDragStop.add(function() {this.onDragStop(Dropper_Red)}, this);
    Dropper_Red.inputEnabled = true;
    Dropper_Red.input.useHandCursor = true;
    Dropper_Red.input.enableDrag(true);
    this.game.physics.arcade.enable(Dropper_Red);
    Dropper_Red.enableBody =true;
    dialog.text="Add about 2 ml of Red litmus solution to test tube B";
    //procedure_voice2.stop();
    //procedure_voice3.play();
    arrow.x=1410;
    arrow.y=450;
    arrow_y=450;
    arrow.visible=true;
  },
  animationStoppedB:function(){
    //tween1=this.game.add.tween(Dropper_Red).to( {x:1380, y:550}, 600, Phaser.Easing.Out, true);
    //tween1.onComplete.add(function(){this.tween_testTubeB_Return()}, this);
    // Dropper_Red.x=1380;
    // Dropper_Red.y=550;
    // dialog.text="Take a dropper";
    // procedure_voice3.stop();
    // procedure_voice4.play();
    // arrow.x=1500;
    // arrow.y=800;
    // arrow_y=800;
    // arrow.visible=true;
    // Dropper_HCl.inputEnabled = true;
    // Dropper_HCl.input.useHandCursor = true;
    // Dropper_HCl.events.onInputDown.add(this.ClickOnDropperHCl, this);
  },
  animationStopped_HClA:function(){
    //if(HCl_step==1){

    Dropper_HCl_1.visible=false;
      corck.visible=false;
      A3_HCl.visible=true;
      HCl_front.visible=false;

      tween1=this.game.add.tween(Dropper_HCl_1).to( {x:1550}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.tween_testTube_HClA_Return()}, this);
      dialog.text="Observe the brisk effervescence.";
      procedure_voice4.stop();
      procedure_voice5.play();
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.toNextProcedure, this);
      
     


     // procedure_voice5.stop();
      //procedure_voice6.play();
    //}
    // else{
    //   dialog.text="Observe what happened in test tube B";
    //   procedure_voice8.stop();
    //   procedure_voice9.play();
    //   Dropper_HCl_1.visible=false;
    //   corck.visible=false;
    //   A3_HCl.visible=true;
    //   HCl_front.visible=false;
    //   TostartTesttubeB=true;

    // }
  },


  toNextProcedure:function()
  {
    tween2=this.game.add.tween(litmusGroup).to( {x:2500}, 600, Phaser.Easing.Out, true);
    A3_TestTube_2.visible=true;
    
    A3_cork_tube.visible=true;
    tween2.onComplete.add(function(){this.tween_solutions_out()}, this);


  },
  animationStopped_TestTubeA:function(){
    dialog.text="Observe, blue colour changed to red colour";
   // procedure_voice6.stop();
   // procedure_voice7.play();
    TostartTesttubeB=true;
    delay=0;
  },
  tween_rackExit:function(){
    rackGroup.visible=false;
    litmusGroup.visible=true;
    tween2=this.game.add.tween(litmusGroup).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween2.onComplete.add(function(){this.tween_solutions_in()}, this);//////tween_testTubeB_Return
  },

  tween_solutions_out:function()
  {
    rackGroup.visible=true;
    console.log("sadbjhasdgfad");
    tween2=this.game.add.tween(rackGroup).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween2.onComplete.add(function(){this.tween_rackend()}, this);
    //1125,650
   
  },

  
  tween_solutions_in:function(){
    dialog.text="Add 1 gms of sodium carbonate to the test tube";
     procedure_voice1.stop();
     procedure_voice2.play();
    arrow.x=1230;
    arrow.y=640;
    arrow_y=640;
    arrow.visible=true;
    collider_A.enableBody=true;
  },
  tween_rackend:function(){
    currentobj.visible=false;
      //A3_cork_tube_sprite.visible=true;
      arrow.visible=false;
      //A3_cork_tube_sprite.animations.play('anim');
      dialog.text="Take the limewater to the clamp stand.";
      procedure_voice6.stop();
      procedure_voice7.play();
      arrow.visible=true;
      arrow.x=1500;
      arrow.y=500;
      arrow_y=500;
    /*arrow.visible=true;
    arrow.x=1125;
    arrow.y=610;
    arrow_y=610;
    A3_TestTube_1.visible=false;
    dialog.text="Take the cork with delivery tube and place it on the mouth of the \ntest tube.";
    procedure_voice5.stop();
      procedure_voice6.play();*/
  },
  tween_testTubeB_Return:function(){
    dialog.text="Take a dropper";
    procedure_voice2.stop();
     procedure_voice3.play();
    arrow.x=1500;
    arrow.y=800;
    arrow_y=800;
    arrow.visible=true;
    Dropper_HCl.inputEnabled = true;
    Dropper_HCl.input.useHandCursor = true;
    Dropper_HCl.events.onInputDown.add(this.ClickOnDropperHCl, this);
    
  },
  ClickOnDropperHCl:function(){
    arrow.visible=false;
    Dropper_HCl.angle=0;
    Dropper_HCl.x=1470;
    Dropper_HCl.y=650;
    tween1=this.game.add.tween(Dropper_HCl).to( {x:1560, y:300}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.tween_DropperHCl()}, this);
  },
  tween_DropperHCl:function(){
    arrow.visible=false;
    Dropper_HCl.visible=false;
    Dropper_HCl_1.visible=true;
    corck.visible=true;
    A3_HCl.visible=false;
    HCl_front.visible=true;/**/
    tween1=this.game.add.tween(Dropper_HCl_1).to( {y:550}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.tween_DropperHCl_1(1)}, this);
  },
  tween_testTube_HClA_Return:function(){
    
      tween1=this.game.add.tween(Dropper_HCl_1).to( {y:550}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.tween_DropperHCl_1(2)}, this);
    
  },
  tween_DropperHCl_1:function(count){
    HCl_step=count;
    if(count==1){
      dialog.text="Add 2ml of diluted hydrochloric acid solution to the test tube and\nobserve";
      procedure_voice3.stop();
      procedure_voice4.play();
      collider_A.enableBody=true;
      arrow.x=1590;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
     }else if(count==2){
      /*Dropper_HCl_1.visible=false;
      corck.visible=false;
      HCl.visible=true;
      HCl_front.visible=false;*/
      arrow.visible=false;
      //dialog.text="Observe what happened in test tube A";
      colorChangingAnimstart=true;
      delay=0;
      /*dialog.text="Add 4 to 5 drops of diluted hydrochloric acid solution to the test tube B";
      Dropper_HCl_1.inputEnabled=true;
      Dropper_HCl_1.input.enableDrag(true);
      Dropper_HCl_1.body.enable=true;
      TestTubeStand_Hanger_B.enableBody=true;*/
    }else if(count==3){
      dialog.text="";
    }
    
    
  },
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    // if(colorChangingAnimstart==true){
    //   delay++;
    //   if(delay>50){
    //     colorChangingAnimstart=false;
    //    // TestTube_A.animations.play("anim_2");
    //     this.game.time.events.add(Phaser.Timer.SECOND*5,this.animationStopped_TestTubeA, this);
    //     //TestTube_A.animations.currentAnim.onComplete.add(this.animationStopped_TestTubeA, this);
    //   }
    // }
    // if(TostartTesttubeB==true){
    //   delay++;
    //   if(delay==200){
    //     dialog.text="Add 4 to 5 drops of diluted hydrochloric acid solution to the test tube B";
    //     procedure_voice7.stop();
    //     procedure_voice8.play();
    //     Dropper_HCl_1.inputEnabled=true;
    //     Dropper_HCl_1.input.enableDrag(true);
    //     Dropper_HCl_1.body.enable=true;
    //     collider_B.enableBody=true;
    //     arrow.x=1590;
    //     arrow.y=450;
    //     arrow_y=450;
    //     arrow.visible=true;
    //     TostartTesttubeB=false;
    //   }else if(delay==500){
    //     dialog.text="Observe, there is no change in test tube B";
    //     procedure_voice9.stop();
    //     procedure_voice10.play();
    //     TostartTesttubeB=false;
    //     this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextMsgSound, this);
    //     //
    //   }
    // }   
  },

  EndofProcedure:function()
  {

dialog.text="Observe that the quick lime truns milky.";
procedure_voice7.stop();
    procedure_voice8.play();
this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextMsgSound, this);

  },
  nextMsgSound:function(){
    dialog.text="Click on the next button to see the observation.";
    play.visible=true;
     procedure_voice8.stop();
     procedure_voice9.play();
     play.visible=true;
  },
  detectCollision:function(){
   

    if(collider_A.enableBody && currentobj!=null){
      
        this.game.physics.arcade.overlap(currentobj, collider_A,function() {this.match_Obj()},null,this);
        collider=collider_A;
     
    } 
    if(collider_B.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
        collider=collider_B;
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
      
      
      
      if(currentobj==A3_TestTube_1){
      
        arrow.x=1500;
        arrow.y=450;
        arrow_y=450;
        arrow.visible=true;
       // A3_TestTube_1.reset(A3_TestTube_1.xp,A3_TestTube_1.yp);
        
      }

      else if(currentobj==A3_cork_tube)
      {
        arrow.x=1125;
        arrow.y=610;
        arrow_y=610;
        arrow.visible=true;
        A3_cork_tube.angle=45;
      }

     else if(currentobj==A3_TestTube_2)
      {
        arrow.x=1500;
        arrow.y=450;
        arrow_y=450;
        arrow.visible=true;
        //A3_cork_tube.angle=45;
      }
      // else if(currentobj==TestTube_2){
      //   arrow.x=1580;
      //   arrow.y=450;
      //   arrow_y=450;
      // }
      else if(currentobj==A3_spoon_NaCO3){
        arrow.x=1230;
        arrow.y=640;
        arrow_y=640;
        A3_spoon_NaCO3.alpha=0;
        A3_spoon.visible=true;
      }  
      else if(currentobj==A3_cork_tube){
       
        A3_spoon_NaCO3.alpha=0;
        A3_spoon.visible=true;
      }
      else if(currentobj==Dropper_HCl_1){
       
        A3_spoon_NaCO3.alpha=0;
        A3_spoon.visible=true;
        arrow.visible=false;
        arrow.x=1590;
        arrow.y=450;
        arrow_y=450;
        arrow.visible=true;
      }
      // else if(currentobj==Dropper_Red){
      //   arrow.x=1410;
      //   arrow.y=450;
      //   arrow_y=450;
      // }










        currentobj=null;
      }
    
  },
//For to next scene
 
      toObservations:function()
      {
      voice.destroy();
      voice2.destroy();
      // procedure_voice1.destroy();
      // procedure_voice2.destroy();
      // procedure_voice3.destroy();
      // procedure_voice4.destroy();
      // procedure_voice5.destroy();
      // procedure_voice6.destroy();
      // procedure_voice7.destroy();
      // procedure_voice8.destroy();
       procedure_voice9.destroy();
      // procedure_voice10.destroy();
      // procedure_voice11.destroy();
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        voice2.destroy();
        procedure_voice1.destroy();
        procedure_voice2.destroy();
        procedure_voice3.destroy();
        procedure_voice4.destroy();
        procedure_voice5.destroy();
        procedure_voice6.destroy();
        procedure_voice7.destroy();
        procedure_voice8.destroy();
        procedure_voice9.destroy();
        // procedure_voice10.destroy();
        // procedure_voice11.destroy();
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  voice2.destroy();
  procedure_voice1.destroy();
  procedure_voice2.destroy();
  procedure_voice3.destroy();
  procedure_voice4.destroy();
  procedure_voice5.destroy();
  procedure_voice6.destroy();
  procedure_voice7.destroy();
  procedure_voice8.destroy();
  procedure_voice9.destroy();
  // procedure_voice10.destroy();
  // procedure_voice11.destroy();
    this.state.start("Experiment_A3",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  voice2.destroy();
  procedure_voice1.destroy();
  procedure_voice2.destroy();
  procedure_voice3.destroy();
  procedure_voice4.destroy();
  procedure_voice5.destroy();
  procedure_voice6.destroy();
  procedure_voice7.destroy();
  procedure_voice8.destroy();
  procedure_voice9.destroy();
  // procedure_voice10.destroy();
  // procedure_voice11.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
