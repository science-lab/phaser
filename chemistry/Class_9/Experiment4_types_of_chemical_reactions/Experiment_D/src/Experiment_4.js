var experiment_4 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;

}

experiment_4.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 voice2=this.game.add.audio('sim_2',1);
 procedure_voice0=this.game.add.audio('audio0',1);
 procedure_voice1=this.game.add.audio('audio1',1);
 procedure_voice2=this.game.add.audio('audio2',1);
 procedure_voice3=this.game.add.audio('audio3',1);
 procedure_voice4=this.game.add.audio('audio4',1);
 procedure_voice5=this.game.add.audio('audio5',1);
 procedure_voice6=this.game.add.audio('audio6',1);
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  /*base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_step_1="1. Take 2 grams of ferrous sulphate crystals in a boiling tube. ";
  procedure_step_2="2. Heat the boiling tube on the bunsen burner. ";
  procedure_step_3="3. Observe the colour of the residue.";
  procedure_text=this.game.add.text(250,250,"Procedure:",headfontStyle);
  procedure_step_text_1=this.game.add.text(300,350,procedure_step_1,fontStyle);
  procedure_step_text_2=this.game.add.text(300,430,procedure_step_2,fontStyle);
  procedure_step_text_3=this.game.add.text(300,510,procedure_step_3,fontStyle);*/



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toObservations, this);
 //play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      addItems:function(){
      collider=this.game.add.sprite(1030,240, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(2,3);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;
      dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dia_box.scale.setTo(.8,.7);
      dia_text="Take a dry boiling tube.";
      procedure_voice0.play();
      dialog=this.game.add.text(70,60,dia_text,fontStyle);
      plate=this.game.add.sprite(1300,400,'ferrous_sulphate','ferrous_sulphate.png');
      plate.scale.setTo(.6,.6);
      
      stand=this.game.add.sprite(250,200,'ferrous_sulphate','testtube_holder.png');
      stand.scale.setTo(1.2,1.2);
      burner=this.game.add.sprite(0,560,'ferrous_sulphate','burner1.png');
      burner.scale.setTo(.8,.8);
      /*burner_on=this.game.add.sprite(0,560,'ferrous_sulphate','burner12.png');
      burner_on.scale.setTo(.8,.8);
      burner_on.visible=false;*/
      test_tube2=this.game.add.sprite(675,750,'ferrous_sulphate','test_tube0001.png');
      test_tube2.scale.setTo(.8,.8);
      test_tube2.anchor.setTo(.5,.5);
      test_tube3=this.game.add.sprite(751,750,'ferrous_sulphate','test_tube0001.png');
      test_tube3.scale.setTo(.8,.8);
      test_tube3.anchor.setTo(.5,.5);
      test_tube1=this.game.add.sprite(598,750,'ferrous_sulphate','test_tube0001.png');//560,570
      test_tube1.scale.setTo(.8,.8);
      test_tube1.anchor.setTo(.5,.5);
      standtop=this.game.add.sprite(250,200,'ferrous_sulphate','testtube_holdertop.png');
      standtop.scale.setTo(1.2,1.2);
      test_tube=this.game.add.sprite(565,570, 'collider');
      test_tube.scale.setTo(4.5,7.2);
      test_tube.alpha=0;
      test_tube.inputEnabled = true;
      test_tube.input.useHandCursor = true;
      test_tube.events.onInputDown.add(this.clickOnTesttube, this);
      
      testtube_h=this.game.add.sprite(500,350,'ferrous_sulphate','test_tube0002.png');
      testtube_h.angle=59;
      testtube_h.scale.setTo(.8,.8);
      testtube_h.visible=false;
      hand=this.game.add.sprite(330,420,'ferrous_sulphate','testtube_handl.png');
      hand.scale.setTo(1.2,1.2);

      spoon=this.game.add.sprite(1450,450, 'ferrous_sulphate', 'spoon.png');
      spoon.scale.setTo(.6,.6);
      spoon.anchor.setTo(.5,.5);
      spoon.xp=1450;
      spoon.yp=450;

      arrow=this.game.add.sprite(600,450, 'arrow');
      arrow.angle=90;
      arrow_y=450;

      
      //mixture_label.scale.setTo(1.5,1.5);
    },
    clickOnTesttube:function(){
    arrow.visible=false;
    tween1=this.game.add.tween(test_tube1).to( {x:960, y:500, angle:45}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.tweenComplete1()}, this);
    test_tube.visible=false;
    

  },
  ClickOnSpoon:function(){

  },
  tweenComplete1:function(){
    test_tube1.xp=960;
    test_tube1.yp=500;
    dialog.text="Take 2gm ferrous sulphate crystals in the tube.";
    procedure_voice1.play();
    procedure_voice1.onStop.add(this.enableProcedure2,this);
    
  },
  enableProcedure2:function(){
    arrow.x=1400;
    arrow.y=320;
    arrow_y=320;
    arrow.visible=true;
    spoon.events.onDragStart.add(function() {this.gameC(spoon)}, this);
    spoon.events.onDragStop.add(function() {this.gameC1(spoon)}, this);
    spoon.inputEnabled = true;
    spoon.input.useHandCursor = true;
    spoon.events.onInputDown.add(this.ClickOnSpoon, this);
    spoon.input.enableDrag(true);
    this.game.physics.arcade.enable(spoon);
    spoon.enableBody =true;
  },
  tweenComplete2:function(){
    eq_name=this.game.add.sprite(550, 600,'equations','Ferrous_sulphate.png');
    eq_name.anchor.setTo(.5,.5);
    eq_name.scale.setTo(0.2,0.2);
    eq_arrow=this.game.add.sprite(670, 500,'equations','arrow.png');
    eq_arrow.anchor.setTo(0,.5);
    eq_arrow.scale.setTo(0,0);
    //eq_arrow.alpha=0;
    //this.game.add.tween(eq_arrow).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
    tween3=this.game.add.tween(eq_arrow.scale).to( {x:.2,y:.4}, 1000, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.tweenComplete3()}, this);
  },
  tweenComplete3:function(){
    eq_heat=this.game.add.sprite(750, 700,'equations','heat.png');
    eq_heat.anchor.setTo(0.5,0.5);
    eq_heat.scale.setTo(.3,.3);
    //eq_arrow.alpha=0;
    //this.game.add.tween(eq_arrow).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
    tween4=this.game.add.tween(eq_heat).to( {y:480}, 800, Phaser.Easing.Out, true);
    tween4.onComplete.add(function(){this.tweenComplete4()}, this);
  },
  tweenComplete4:function(){
    eq2=this.game.add.sprite(930, 500,'equations','eq2.png');
    eq2.anchor.setTo(0.5,0.5);
    eq2.scale.setTo(0,0);
    eq2.alpha=0;
    this.game.add.tween(eq2).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
    tween5=this.game.add.tween(eq2.scale).to( {x:.2,y:.20}, 1000, Phaser.Easing.Out, true);
    tween5.onComplete.add(function(){this.tweenComplete5()}, this);
  },
  tweenComplete5:function(){
    eq_name1=this.game.add.sprite(930, 600,'equations','Ferric.png');
    eq_name1.anchor.setTo(.5,.5);
    eq_name1.scale.setTo(0.2,0.2);
    eq_plus=this.game.add.sprite(1040, 500,'equations','plus.png');
    eq_plus.anchor.setTo(0.5,0.5);
    eq_plus.scale.setTo(0,0);
    //eq_arrow.alpha=0;
    //this.game.add.tween(eq2).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
    tween6=this.game.add.tween(eq_plus.scale).to( {x:.2,y:.20}, 1000, Phaser.Easing.Out, true);
    tween6.onComplete.add(function(){this.tweenComplete6()}, this);
  },
  tweenComplete6:function(){
    eq3=this.game.add.sprite(1150, 500,'equations','eq3.png');
    eq3.anchor.setTo(0.5,0.5);
    eq3.scale.setTo(0,0);
    eq3.alpha=0;
    this.game.add.tween(eq3).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
    tween7=this.game.add.tween(eq3.scale).to( {x:.2,y:.20}, 1000, Phaser.Easing.Out, true);
    tween7.onComplete.add(function(){this.tweenComplete7()}, this);
  },
  tweenComplete7:function(){
    eq_plus=this.game.add.sprite(1260, 500,'equations','plus.png');
    eq_plus.anchor.setTo(0.5,0.5);
    eq_plus.scale.setTo(0,0);
    //eq_arrow.alpha=0;
    //this.game.add.tween(eq2).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
    tween8=this.game.add.tween(eq_plus.scale).to( {x:.2,y:.20}, 1000, Phaser.Easing.Out, true);
    tween8.onComplete.add(function(){this.tweenComplete8()}, this);
  },
  tweenComplete8:function(){
    eq4=this.game.add.sprite(1370, 500,'equations','eq4.png');
    eq4.anchor.setTo(0.5,0.5);
    eq4.scale.setTo(0,0);
    eq4alpha=0;
    this.game.add.tween(eq4).to( {alpha:1}, 1300, Phaser.Easing.Out, true);
    tween9=this.game.add.tween(eq4.scale).to( {x:.2,y:.20}, 1000, Phaser.Easing.Out, true);
    tween9.onComplete.add(function(){this.tweenComplete9()}, this);
  },
  tweenComplete9:function(){
     /*next = this.game.add.sprite(1400,860,'components','next_pressed.png');
     next.scale.setTo(.7,.7);
     next.inputEnabled = true;
     next.input.useHandCursor = true;
     next.events.onInputDown.add(this.toSimulation, this);*/
     hotButton.animations.play('hotty');
        this.buttontween=this.game.add.tween(buttonGroup).to( { x:-130 }, 400, Phaser.Easing.Out, true);
        this.buttontween.onComplete.add(this.complete1,this);
  },
  complete1:function(obj)
  {
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    //arrow.visible=false;
    if(obj==spoon){
      spoon.frameName="spoon2.png";
      arrow.x=1080;
      arrow.y=250;
      arrow_y=250;
      arrow.visible=true;
    }else if(obj==test_tube1){
      arrow.x=360;
      arrow.y=300;
      arrow_y=300;
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==spoon){
      spoon.reset(spoon.xp,spoon.yp);
      spoon.frameName="spoon.png";
      test_tube1.frameName="test_tube0002.png";
      dialog.text="Drag the boiling tube for heating.";
      procedure_voice2.play();
      procedure_voice2.onStop.add(this.enableProcedure3,this);
      arrow.visible=false;
    }else if(currentobj==test_tube1){
      test_tube1.visible=false;
      testtube_h.visible=true; 
      //burner_on.visible=true;
      arrow.visible=false;
      dialog.text="Observe the colour of the residue.";
      procedure_voice3.play();

      fNames=['test_tube0002.png','test_tube0003.png','test_tube0004.png',
            'test_tube0005.png',
            'test_tube0006.png',
            'test_tube0007.png',
            'test_tube0008.png',  
            'test_tube0009.png',
            'test_tube0010.png',
            'test_tube0011.png',
            'test_tube0012.png'];
      
      interval=0;
      incr=0;
      heatFlag=true;
      burner.visible=false;
      burner_on=this.game.add.sprite(248,923,'burner_sprites','Burner_sprite_0.png');//
      burner_on.anchor.setTo(0.8,1);
      burner_on.animations.add('burner',['Burner_sprite_0.png',
            'Burner_sprite_1.png','Burner_sprite_2.png','Burner_sprite_3.png',
             ], 4, true, false);

      burner_on.scale.setTo(.8,.8);
      burner_on.animations.play('burner');
      //testtube_h.play('anim', false);
    }
    
  },
  enableProcedure3:function(){
    arrow.x=980;
      arrow.y=350;
      arrow_y=350;
      arrow.visible=true;
      test_tube1.events.onDragStart.add(function() {this.gameC(test_tube1)}, this);
      test_tube1.events.onDragStop.add(function() {this.gameC1(test_tube1)}, this);
      test_tube1.inputEnabled = true;
      test_tube1.input.useHandCursor = true;
      test_tube1.events.onInputDown.add(this.ClickOnSpoon, this);
      test_tube1.input.enableDrag(true);
      this.game.physics.arcade.enable(test_tube1);
      test_tube1.enableBody =true;
      collider.reset(300,350);
      collider.scale.setTo(3.5,5);
  },
  update: function()
  {
    /*DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    ///////////////////////////////
    if(heatFlag){
      //console.log(interval);
      interval++;
      if(interval>=250){
        interval=0;
        testtube_h.frameName=fNames[incr];
        console.log(incr+"...........");
        if(incr==3){
          dialog.y=40;
          dialog.text="Observe that on heating green colour crystals turns to \nwhite colour crystals";
          procedure_voice5.play();
          interval=-250;
        }
        if(incr==8){
          dialog.y=40;
          dialog.text="Observe that on further heating white colour crystals turns to \ndark brown crystals";
          procedure_voice6.play();
          interval=-250;
        }
        incr++;
        if(incr==10){
          play.visible=true;
          procedure_voice4.play();
          
        }
      }
    }*/
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==spoon){
        currentobj.frameName="spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=1400;
        arrow.y=320;
        arrow_y=320;
      }else if(currentobj==test_tube1){
        arrow.x=980;
        arrow.y=350;
        arrow_y=350;
        arrow.visible=true;
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
      toObservations:function()
      {
      voice.destroy();
      voice2.destroy();
      procedure_voice0.destroy();
      procedure_voice1.destroy();
      procedure_voice2.destroy();
      procedure_voice3.destroy();
      procedure_voice4.destroy();
      procedure_voice5.destroy();
      procedure_voice6.destroy();
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        voice2.destroy();
        procedure_voice0.destroy();
        procedure_voice1.destroy();
        procedure_voice2.destroy();
        procedure_voice3.destroy();
        procedure_voice4.destroy();
        procedure_voice5.destroy();
      procedure_voice6.destroy();
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  voice2.destroy();
  procedure_voice0.destroy();
  procedure_voice1.destroy();
  procedure_voice2.destroy();
  procedure_voice3.destroy();
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  voice2.destroy();
  procedure_voice0.destroy();
  procedure_voice1.destroy();
  procedure_voice2.destroy();
  procedure_voice3.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
