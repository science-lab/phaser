var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        //this.game.load.atlasJSONHash('ferrous_sulphate', 'assets/ferrous_sulphate.png', 'assets/ferrous_sulphate.json');
        //this.game.load.atlasJSONHash('burner_sprites', 'assets/burner_sprites.png', 'assets/burner_sprites.json');
        this.game.load.atlasJSONHash('equations', 'assets/equations.png', 'assets/equations.json');

        //new
        this.game.load.atlasJSONHash('flame_sprite', 'assets/Decomposition_reaction/Flame_sprite/Flame.png','assets/Decomposition_reaction/Flame_sprite/Flame.json');
        this.game.load.atlasJSONHash('smoke_sprite','assets/Decomposition_reaction/Smoke_sprite/Smoke.png','assets/Decomposition_reaction/Smoke_sprite/Smoke.json');
       // this.game.load.atlasJSONHash('spoon_pour_sprite','assets/Decomposition_reaction/Spoon_pour_sprite/Spoon.png','assets/Decomposition_reaction/Spoon_pour_sprite/Spoon.json');
        this.game.load.atlasJSONHash('testTube_fill_sprite','assets/Decomposition_reaction/FS_to_testtube_sprite/FS_Testube.png','assets/Decomposition_reaction/FS_to_testtube_sprite/FS_Testube.json');
        this.game.load.atlasJSONHash('testTube_heat_sprite','assets/Decomposition_reaction/FS_heating_sprite/FS_Heating.png','assets/Decomposition_reaction/FS_heating_sprite/FS_Heating.json');
        this.game.load.atlasJSONHash('Spoon_sprite','assets/Decomposition_reaction/Spoon_sprite/Spoon.png','assets/Decomposition_reaction/Spoon_sprite/Spoon.json');
        this.game.load.atlasJSONHash('Spoon_anim_sprite','assets/Decomposition_reaction/Spoon_anim_sprite/Spoon.png','assets/Decomposition_reaction/Spoon_anim_sprite/Spoon.json');
        this.game.load.atlasJSONHash('litmus_color_change_sprite','assets/Decomposition_reaction/litmus_paper_sprite/litmus_color_change_sprite.png','assets/Decomposition_reaction/litmus_paper_sprite/litmus_color_change_sprite.json');
        this.game.load.atlasJSONHash('water_dropper_sprite','assets/Decomposition_reaction/water_dropper_sprite.png','assets/Decomposition_reaction/water_dropper_sprite.json');
        this.game.load.atlasJSONHash('White_to_blue','assets/Decomposition_reaction/White_to_blue.png','assets/Decomposition_reaction/White_to_blue.json');
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
        this.game.load.image("bg1","assets/bg1.png");
        this .game.load.image("bg2","assets/Decomposition_reaction/Bg3.jpg");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        this.game.load.image('combination_btn','assets/combination_btn.png');
        this.game.load.image('decomposition_btn','assets/decomposition_btn.png');
        this.game.load.image('displacement_btn','assets/displacement_btn.png');
        this.game.load.image('doubledisplacement_btn','assets/doubledisplacement_btn.png');

        //this.game.load.image('burner','assets/burner.png');
        //new
        this.game.load.image('burner','assets/Decomposition_reaction/Burner.png');
        this.game.load.image('burner_knobe_front','assets/Decomposition_reaction/Flame_Knobe_front.png');
        this.game.load.image('burner_knobe_back','assets/Decomposition_reaction/Flame_Knobe_back.png');
        //this.game.load.image('burner','assets/Decomposition_reaction/Burner.png');

        this.game.load.image('testtube_empty','assets/Decomposition_reaction/Testube.png');
        this.game.load.image('spoon','assets/Decomposition_reaction/Spoon.png');
        this.game.load.image('testtube_holder','assets/Decomposition_reaction/Testube_Holder.png');
        this.game.load.image('FS','assets/Decomposition_reaction/FS.png');
        this.game.load.image('FS_spoon','assets/Decomposition_reaction/FS_Spoon.png');
        this.game.load.image('FS_testtube','assets/Decomposition_reaction/FS_Test_Tube.png');
        this.game.load.image('testtube_stand','assets/Decomposition_reaction/Testube_Stand.png');
        this.game.load.image('testtube_stand_holder','assets/Decomposition_reaction/Testtube_Holder.png');
        this.game.load.image('testtube','assets/Decomposition_reaction/Testube.png');
        this.game.load.image('woodstand_back','assets/Decomposition_reaction/Wooden_stand_back.png');
        this.game.load.image('woodstand_front','assets/Decomposition_reaction/Wooden_stand_front.png');
        this.game.load.image('flameKnobe_front','assets/Decomposition_reaction/Flame_Knobe_front.png');
        this.game.load.image('flameKnobe_back','assets/Decomposition_reaction/Flame_Knobe_back.png');
        this.game.load.image('testtube_holder_testtube','assets/Decomposition_reaction/Testube_holder_testtube.png');
        this.game.load.image('testtube_black','assets/Decomposition_reaction/Testube_black.png');
        this.game.load.image('testube_white','assets/Decomposition_reaction/Testube_White.png');
        this.game.load.image('watch_glass','assets/Decomposition_reaction/Plate.png');
        this.game.load.image('litmus_paper','assets/Decomposition_reaction/litmas_paper.png');
        this.game.load.image('tongs','assets/Decomposition_reaction/Plucker.png');
        this.game.load.image('testube_black_holder','assets/Decomposition_reaction/Testube_black_with_holder.png');
        this.game.load.image('testube_white_holder','assets/Decomposition_reaction/Testube_With_holder.png');
        this.game.load.image('Beaker_with_Water','assets/Decomposition_reaction/Beaker_with_Water.png');
        this.game.load.image('Beaker_back','assets/Decomposition_reaction/Beaker_back.png');
        this.game.load.image('Dropper','assets/Decomposition_reaction/Dropper.png');
        
        /////////////////////////////for materials////////////////
        this.game.load.image('m_burner','assets/Decomposition_reaction/Materials/m_burner.png');
        this.game.load.image('m_fs','assets/Decomposition_reaction/Materials/m_fs.png');
        this.game.load.image('m_litmus','assets/Decomposition_reaction/Materials/m_litmus.png');
        this.game.load.image('m_spoon','assets/Decomposition_reaction/Materials/m_spoon.png');
        this.game.load.image('m_stand','assets/Decomposition_reaction/Materials/m_stand.png');
        this.game.load.image('m_testube','assets/Decomposition_reaction/Materials/m_testube.png');
        this.game.load.image('m_tongs','assets/Decomposition_reaction/Materials/m_tongs.png');
        this.game.load.image('m_testtube_holder','assets/Decomposition_reaction/Materials/m_testtube_holder.png');
        this.game.load.image('ferrous_sulphate1','assets/ferrous_sulphate_1.png');

        //this.game.load.image('spoon','assets/spoon.png');
        //new
       
        ///////

        //this.game.load.image('stand','assets/stand.png');
        this.game.load.image('test_tube','assets/test_tube.png');
        //this.game.load.image('testube_stand','assets/testube_stand.png');
        this.game.load.image('observation_table_decomposition','assets/observation_table_decomposition.png');
        this.game.load.image('observation_img_1','assets/observation_img_1.png');
        this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        
        this.game.load.audio('title_a','assets/audio/ttl.mp3');
        this.game.load.audio('next_snd','assets/audio/next_snd.mp3');
        this.game.load.audio('sim_1','assets/audio/inst1.mp3');
        this.game.load.audio('sim_2','assets/audio/inst2.mp3');

//////Audio for decomposition reaction/////////////////////////
         this.game.load.audio('Aim_1','assets/audio/Aim_1.mp3');
         this.game.load.audio('Aim_2','assets/audio/Aim_2.mp3');
         this.game.load.audio('audio_start','assets/audio/Decomposition_reaction/lets_perform_Exp.mp3');
        this.game.load.audio('audio_procedure1','assets/audio/Decomposition_reaction/A_procedure1.mp3');
        this.game.load.audio('audio_procedure2','assets/audio/Decomposition_reaction/A_procedure2.mp3');
        this.game.load.audio('audio_procedure3','assets/audio/Decomposition_reaction/A_procedure3.mp3');
        this.game.load.audio('audio_procedure4','assets/audio/Decomposition_reaction/A_procedure4.mp3');
        this.game.load.audio('audio_procedure5','assets/audio/Decomposition_reaction/A_procedure5.mp3');
        this.game.load.audio('audio_procedure6','assets/audio/Decomposition_reaction/A_procedure6.mp3');
        this.game.load.audio('audio_procedure7','assets/audio/Decomposition_reaction/A_procedure7.mp3');
        this.game.load.audio('audio_procedure8','assets/audio/Decomposition_reaction/A_procedure8.mp3');
        this.game.load.audio('audio_procedure9','assets/audio/Decomposition_reaction/A_procedure9.mp3');
        this.game.load.audio('audio_procedure10','assets/audio/Decomposition_reaction/A_procedure10.mp3');
        this.game.load.audio('audio_procedure11','assets/audio/Decomposition_reaction/A_procedure11.mp3');
        this.game.load.audio('audio_procedure12','assets/audio/Decomposition_reaction/A_procedure12.mp3');
        this.game.load.audio('audio_procedure13','assets/audio/Decomposition_reaction/A_procedure13.mp3');
        this.game.load.audio('audio_procedure14','assets/audio/Decomposition_reaction/A_procedure14.mp3');
        this.game.load.audio('audio_procedure15','assets/audio/Decomposition_reaction/A_procedure15.mp3');
        this.game.load.audio('audio_procedure16','assets/audio/Decomposition_reaction/A_procedure16.mp3');
        this.game.load.audio('audio_procedure17','assets/audio/Decomposition_reaction/A_procedure17.mp3');


        
        this.game.load.audio('precautions_1','assets/audio/Decomposition_reaction/A_precaution1.mp3');
        this.game.load.audio('procedure_voice1','assets/audio/Decomposition_reaction/A_procedure_voice1.mp3');
        this.game.load.audio('procedure_voice2','assets/audio/Decomposition_reaction/A_procedure_voice2.mp3');

        this.game.load.audio('observation1_snd','assets/audio/Decomposition_reaction/A_observation1.mp3');
        this.game.load.audio('observation2_snd','assets/audio/Decomposition_reaction/A_observation2.mp3');
        this.game.load.audio('result1','assets/audio/Result_1.mp3');
        this.game.load.audio('result2','assets/audio/Result_2.mp3');
        this.game.load.audio('result3','assets/audio/Result_3.mp3');


        this.game.load.audio('obj','assets/audio/K_objective.mp3');
        this.game.load.audio('sum','assets/audio/K_summary.mp3');
       
/////////////////////////////Decomposition reaction materials/////////////////////////
        this.game.load.image('wood_rack','assets/Decomposition_reaction/Materials/Wooden_stand_full.png');
        this.game.load.image('testtube','assets/Decomposition_reaction/Materials/Testube.png');
        this.game.load.image('FS','assets/Decomposition_reaction/Materials/FS.png');
        this.game.load.image('spoon','assets/Decomposition_reaction/Materials/Spoon.png');
        this.game.load.image('stand','assets/Decomposition_reaction/Materials/Testube_Stand_full.png');
        this.game.load.image('burner','assets/Decomposition_reaction/Materials/Burner_full.png');
        

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      this.game.state.start("Materials");//Simulation_hot1
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");
      //this.game.state.start("Exp_Selection");

	}
}

