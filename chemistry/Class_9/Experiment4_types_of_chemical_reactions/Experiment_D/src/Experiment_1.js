var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var isBurnerOn;
var Is_end_FS_heat_anim2;

}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }


  
 muted = false;
 voice=this.game.add.audio("audio_start",1);
 
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 //bg= this.game.add.sprite(0, 0,'bg');

 //new
 bg= this.game.add.sprite(0, 0,'bg2');



 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  droperFlag=false;
 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  isBurnerOn=false;
  Is_end_FS_heat_anim2=false;
  this.addItems();

  /*base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_step_1="1. Take 2 grams of ferrous sulphate crystals in a boiling tube. ";
  procedure_step_2="2. Heat the boiling tube on the bunsen burner. ";
  procedure_step_3="3. Observe the colour of the residue.";
  procedure_text=this.game.add.text(250,250,"Procedure:",headfontStyle);
  procedure_step_text_1=this.game.add.text(300,350,procedure_step_1,fontStyle);
  procedure_step_text_2=this.game.add.text(300,430,procedure_step_2,fontStyle);
  procedure_step_text_3=this.game.add.text(300,510,procedure_step_3,fontStyle);*/

 

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toObservations, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      addItems:function(){
      collider=this.game.add.sprite(840,500, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(2,3);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0.0;

      collider2=this.game.add.sprite(840,460, 'collider');
      this.game.physics.arcade.enable(collider2);
      collider2.scale.setTo(2,4);
      collider2.inputEnabled = true;
      collider2.enableBody =true;
      collider2.alpha=0.0;


      dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dia_box.scale.setTo(.8,.7);
      // dia_text="Add 2gms of ferrous sulphate crystals to the boiling tube";
      // procedure_voice1.play();
      // procedure_voice1.onStop.add(this.enableProcedure2,this);
      dialog=this.game.add.text(70,60,"",fontStyle);

    


//new
      testube_rack_back=this.game.add.sprite(550,650,'woodstand_back');
      
      //testtube_stand=this.game.add.sprite(250,250,'testtube_stand');
      
      burner=this.game.add.sprite(-50,650,'burner');
      
      burner_knobe_back=this.game.add.sprite(390,855,'burner_knobe_back');
      burner_knobe_front=this.game.add.sprite(440,905,'burner_knobe_front');
      burner_knobe_front.anchor.setTo(.5,.5);
      //burner_knobe_front.inputEnabled = true;
      //burner_knobe_front.input.useHandCursor = true;
      burner_knobe_front.events.onInputDown.add(this.burnerKnobeClick, this);

      burner_flame_anim=this.game.add.sprite(405,585,'flame_sprite','Flame0001.png');
      burner_flame_anim.visible=false;
      
     
      White_to_blue=this.game.add.sprite(835,570,'White_to_blue','White_to_blue0001.png');
      White_to_blue.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26],10, false, true);
      White_to_blue.visible=false;
      testtube_empty1=this.game.add.sprite(837,600,'testtube_empty');
      testtube_empty1.inputEnabled = true;
      testtube_empty1.input.useHandCursor = true;
      testtube_empty1.events.onInputDown.add(this.clickOnTesttube, this);
      testtube_empty2=this.game.add.sprite(750,600,'testtube_empty');
      testtube_empty3=this.game.add.sprite(665,600,'testtube_empty');
      

      testtube_FS=this.game.add.sprite(836,560,'FS_testtube');
      // testtube_FS.inputEnabled = true;
      // testtube_FS.input.useHandCursor = true;
     // testtube_FS.input.enableDrag(true);
      this.game.physics.arcade.enable(testtube_FS);
      testtube_FS.enableBody =true;
      testtube_FS.visible=false;
      testtube_FS.xp=870;
      testtube_FS.yp=357;

      testtube_FS_anim=this.game.add.sprite(836 ,561,'testTube_fill_sprite','FS_to_Testtube0001.png');
      testtube_FS_anim.visible=false;

      testtube_FS_heat_anim1=this.game.add.sprite(393,300,'testTube_heat_sprite','FS_colour_change0001.png');
      testtube_FS_heat_anim1.visible=false;
      testtube_FS_heat_anim1.animations.add('testTube_heat_sprite',[1,2,,3,4,5,6,7,8,9,10,11,12,13,14,15,16],1, false, true);

      testtube_FS_heat_anim2=this.game.add.sprite(393,300,'testTube_heat_sprite','FS_colour_change0016.png');
      testtube_FS_heat_anim2.visible=false;
      testtube_FS_heat_anim2.animations.add('testTube_heat_sprite',[21,22,23,24,25,26,27,28,29,30,31,32],1, false, true);

      smoke_anim=this.game.add.sprite(410,10,'smoke_sprite','Smoke0002.png');
      smoke_anim.animations.add('smoke_sprite',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
        17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,
        44,45,46,47,48,49,50,51,52,53,54],4, true, true);
        smoke_anim.visible=true;

        smoke2_anim=this.game.add.sprite(848,280,'smoke_sprite','Smoke0008.png');
        smoke2_anim.scale.setTo(1,1);
        smoke2_anim.animations.add('smoke_sprite',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,
          17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,
          44,45,46,47,48,49,50,51,52,53,54],4, true, true);
          smoke2_anim.visible=false;

      spoon_anim_sprite=this.game.add.sprite(835,430,'Spoon_anim_sprite','Spoon0001.png');
      spoon_anim_sprite.animations.add('anim',[1,2,3,4,5,6,7],24,false,true);
      spoon_anim_sprite.visible=false;



      testtube_holder_testtube=this.game.add.sprite(835,560,'testtube_holder_testtube');
      testtube_holder_testtube.xp=835;
      testtube_holder_testtube.yp=560;
      testtube_holder_testtube.visible=false;
      //testtube_holder_testtube.anchor.setTo(.5,.5);
      testtube_holder_testtube.events.onDragStart.add(function() {this.gameC(testtube_holder_testtube)}, this);
      testtube_holder_testtube.events.onDragStop.add(function() {this.gameC1(testtube_holder_testtube)}, this);
      testtube_holder_testtube.inputEnabled = true;
      testtube_holder_testtube.input.useHandCursor = true;
      testtube_holder_testtube.events.onInputDown.add(this.ClickOnSpoon, this);
      testtube_holder_testtube.input.enableDrag(true);
    this.game.physics.arcade.enable(testtube_holder_testtube);
    testtube_holder_testtube.enableBody =true;

      //testtube_stand_holder=this.game.add.sprite(340,420,'testtube_stand_holder');
      fs=testube_rack_front=this.game.add.sprite(1200,830,'FS');
      // fs.visible=false;// should be true
      //testtube_empty.alpha=0;

/////////////////
      //spoon=this.game.add.sprite(1450,450, 'ferrous_sulphate', 'spoon.png');
//new
      spoon=this.game.add.sprite(1355,900,'Spoon_sprite','Spoon.png');
      spoon.scale.setTo(1,1);
      spoon.anchor.setTo(.5,.5);
      spoon.angle=10;
      spoon.xp=1355;
      spoon.yp=900;
      // spoon.visible=false;//should be true

      copperGroup=this.game.add.group();
      copperGroup.add(fs);
      copperGroup.add(spoon);

      
      
      testtube_holder=this.game.add.sprite(1500,855,'testtube_holder');
      testtube_holder.xp=1500;
      testtube_holder.yp=855;

      testtube_holder.visible=true;
      
      
     
    
    testtube_holder2=this.game.add.sprite(379,345,'testtube_holder');
    testtube_holder2.visible=false;
    //testtube_holder_testtube2=this.game.add.sprite(393,309,'testtube_holder_testtube');
/////////////
      // spoon.scale.setTo(.6,.6);
      // spoon.anchor.setTo(.5,.5);
      // spoon.xp=1450;
      // spoon.yp=450;

      //testtubeBlack=this.game.add.sprite(835,570,'testtube_black');
      //testtubeBlack.visible=false;
      testTubeWhite=this.game.add.sprite(835,570,'testube_white');
      testTubeWhite.visible=false;
      
      beaker_back=this.game.add.sprite(1200,615, 'Beaker_back');
      beaker_back.scale.set(.8);
      water_dropper=this.game.add.sprite(1325,600,'water_dropper_sprite','Aluminium_Sulphate_Dropper0001.png');
      water_dropper.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
      water_dropper.visible=false;
      water_dropper.anchor.set(.5);
      water_dropper.scale.setTo(1,1.5);
      water_dropper.events.onDragStart.add(function() {this.gameC(water_dropper)}, this);
      water_dropper.events.onDragStop.add(function() {this.gameC1(water_dropper)}, this);
      this.game.physics.arcade.enable(water_dropper);
      Beaker_with_Water=this.game.add.sprite(1200,615, 'Beaker_with_Water');
      Beaker_with_Water.scale.set(.8);
      Dropper=this.game.add.sprite(1600,900, 'Dropper');
      Dropper.scale.setTo(1,1.5);
      Dropper.angle=60;
      Dropper.anchor.set(.5);
      beakerGroup=this.game.add.group();
      beakerGroup.add(beaker_back);
       beakerGroup.add(water_dropper);
       beakerGroup.add(Beaker_with_Water);
      beakerGroup.add(Dropper);
      beakerGroup.x=2000;

      
      litmus_sprite=this.game.add.sprite(1325,800,'litmus_color_change_sprite','Litmaspaprt_Blue to_Red0001.png');
      litmus_sprite.animations.add('anim',[0,1,2,3,4,5,6,7,8,9],2,false,true);
      litmus_sprite.alpha=0;

      litmus_sprite.xp=1325;
      litmus_sprite.yp=800;

      watchGlass=this.game.add.sprite(1000,850,'watch_glass');
      litmusPaper=this.game.add.sprite(980,820,'litmus_paper');
      tongs=this.game.add.sprite(1750,850,'tongs');
      tongs.scale.setTo(-1,.7);

      // testTubeBlackHolder=this.game.add.sprite(393,300,'testube_black_holder');
      // testTubeBlackHolder.visible=false;
      // testTubeBlackHolder.xp=393;
      // testTubeBlackHolder.yp=300;

      testTubeWhiteHolder=this.game.add.sprite(380,310,'testube_white_holder');
      testTubeWhiteHolder.visible=false;
      testTubeWhiteHolder.xp=393;
      testTubeWhiteHolder.yp=300;
      

      litmusGroup=this.game.add.group();
      litmusGroup.add(watchGlass);
      litmusGroup.add(litmusPaper);
      litmusGroup.add(tongs);
      litmusGroup.x=2500;
      



      arrow=this.game.add.sprite(1350,750, 'arrow');
      arrow.angle=90;
      arrow_y=750;
      arrow.visible=false;

      // arrow.x=450;
      // arrow.y=250;
      // arrow_y=250;
      // arrow.visible=true;

    
      testube_rack_front=this.game.add.sprite(550,650,'woodstand_front');
      voice=this.game.add.audio("audio_start",1);
      dialog.text="Lets perform the experiment.";
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
 
      //mixture_label.scale.setTo(1.5,1.5);
    },
    loadScene:function()
{
 
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this);
},

startExperiment:function()
{
  voice.destroy();
  voice=this.game.add.audio("audio_procedure1",1);
  voice.play();
  dialog.text="Add 2 gms of copper sulphate crystals to the boiling tube.";
  arrow.x=1350;
     arrow.y=750;
     arrow_y=750;
     arrow.visible=true;

    spoon.events.onDragStart.add(function() {this.gameC(spoon)}, this);
    spoon.events.onDragStop.add(function() {this.gameC1(spoon)}, this);
    spoon.inputEnabled = true;
    spoon.input.useHandCursor = true;
    spoon.events.onInputDown.add(this.ClickOnSpoon, this);
    spoon.input.enableDrag(true);
    this.game.physics.arcade.enable(spoon);
    spoon.enableBody =true;

  

},
    clickOnTesttube:function(){
     arrow.visible=false;
    
    // testtube_rack.animations.add('testTube_move_sprite',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29
    // ,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70],24, false, true);
    
    //testtube_rack.animations.play('testTube_move_sprite');
   // testtube_empty1_tween1=this.game.add.tween(testtube_empty1).to( {x:1080, y:350, angle:51}, 600, Phaser.Easing.Out, true);
    

   // testtube_empty1_tween1.onComplete.add(function(){this.tweenComplete1()}, this);
    // testtube_rack.animations.currentAnim.onComplete.add(this.tweenComplete1, this)
    // test_tube.visible=false;
    

  },
  ClickOnSpoon:function(){

  },

  burnerKnobeClick:function()
  {
    if(isBurnerOn==false)
    {
      arrow.visible=false;
      burner_knobe_front.angle=90;

      burner_flame_anim.visible=true;
      burner_flame_anim.animations.add('flame_sprite',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24, true, true);
      burner_flame_anim.animations.play('flame_sprite');
      testtube_FS_heat_anim1.animations.play('testTube_heat_sprite');
      testtube_FS_heat_anim1.animations.currentAnim.onComplete.add(this.Endof_FS_heat_anim1, this)
      smoke_anim.visible=true;
      //smoke_anim.animations.play('smoke_sprite');
      setTimeout(this.initiate_smoke_anim,5000);
      isBurnerOn=true;
      burner_knobe_front.inputEnabled = false;
      burner_knobe_front.input.useHandCursor = false;
      isBurnerOn=true;
    }
    else if(isBurnerOn==true)
    {
      voice.destroy();
      voice=this.game.add.audio("audio_procedure9",1);
      voice.play();
      burner_knobe_front.angle=0;
      burner_flame_anim.visible=false;
      testtube_FS_heat_anim1.visible=false;
    testtube_holder2.visible=false;

      burner_knobe_front.inputEnabled = false;
      burner_knobe_front.input.useHandCursor = false;
      isBurnerOn=false;
      dialog.text="Take the test tube back to the test tube stand.";
    //  testTubeBlackHolder.events.onDragStart.add(function() {this.gameC(testTubeBlackHolder)}, this);
    //   testTubeBlackHolder.events.onDragStop.add(function() {this.gameC1(testTubeBlackHolder)}, this);
    //   testTubeBlackHolder.inputEnabled = true;
    //   testTubeBlackHolder.input.useHandCursor = true;
    //   testTubeBlackHolder.input.enableDrag(true);
    //   this.game.physics.arcade.enable(testTubeBlackHolder);
    //   testTubeBlackHolder.enableBody =true;
    testTubeWhiteHolder.visible=true;
    testTubeWhiteHolder.events.onDragStart.add(function() {this.gameC(testTubeWhiteHolder)}, this);
    testTubeWhiteHolder.events.onDragStop.add(function() {this.gameC1(testTubeWhiteHolder)}, this);
    testTubeWhiteHolder.inputEnabled = true;
    testTubeWhiteHolder.input.useHandCursor = true;
    testTubeWhiteHolder.input.enableDrag(true);
      this.game.physics.arcade.enable(testTubeWhiteHolder);
      testTubeWhiteHolder.enableBody =true;
      arrow.angle=90;
      arrow.x=550;
      arrow.y=250;
      arrow_y=250;
      arrow.visible=true;
    }


  },
  tweenComplete1:function(){
    
    // testtube_empty1.xp=1200;
    // testtube_empty1.yp=350;
    // dialog.text="";
    //  procedure_voice1.play();
    //  procedure_voice1.onStop.add(this.enableProcedure2,this);
    //  testtube_empty1.inputEnabled = false;
    //   testtube_empty1.input.useHandCursor = false;
      //testtube_empty1.events.onInputDown.add(this.clickOnTesttube, this);
    
  },
  enableProcedure2:function(){
    //  arrow.x=1350;
    //  arrow.y=750;
    //  arrow_y=750;
    //  arrow.visible=true;

    // spoon.events.onDragStart.add(function() {this.gameC(spoon)}, this);
    // spoon.events.onDragStop.add(function() {this.gameC1(spoon)}, this);
    // spoon.inputEnabled = true;
    // spoon.input.useHandCursor = true;
    // spoon.events.onInputDown.add(this.ClickOnSpoon, this);
    // spoon.input.enableDrag(true);
    // this.game.physics.arcade.enable(spoon);
    // spoon.enableBody =true;
  },

  complete1:function(obj)
  {
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    //arrow.visible=false;
    if(obj==spoon){
      spoon.frameName="FS_Spoon.png";
      arrow.x=900;
      arrow.y=500;
      arrow_y=500;
      arrow.visible=true;
    }else if(obj==testtube_holder){
      arrow.x=900;
      arrow.y=500;
      arrow_y=500;
      arrow.visible=true;
    }

    else if(obj==testtube_holder_testtube){
      arrow.x=450;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
    }

    else if(obj==testTubeWhiteHolder)
    {
      arrow.visible=false;
      collider2.visible=true;
      arrow.x=900;
      arrow.y=350;
      arrow_y=350;
      arrow.visible=true;
      smoke_anim.visible=false;

    }else if(obj==water_dropper){
      collider2.visible=true;
      arrow.x=900;
      arrow.y=550;
      arrow_y=550;
      arrow.visible=true;
    }
    else if(obj==litmus_sprite)
    {
      voice.destroy();
      voice=this.game.add.audio("audio_procedure11",1);
      voice.play();

      arrow.visible=false;
      arrow.x=900;
      arrow.y=350;
      arrow_y=350;
      arrow.visible=true;
      litmusPaper.visible=false;
      tongs.visible=false;
      litmus_sprite.alpha=1;

      dialog.text="Place the litmus paper on the mouth of the test tube and observe.";

    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    
    if(currentobj==spoon){
      spoon.reset(spoon.xp,spoon.yp);
      spoon.frameName="Spoon.png";
      spoon.visible=false;
      //test_tube1.frameName="test_tube0002.png";
      testtube_empty1.visible=false;
      testtube_FS_anim.visible=true;
      testtube_FS_anim.animations.add('FS_testtube',[1,2,3,4,5,6,7,8,9,10,11,12],24, false, true);
     //testtube_FS_anim.animations.play('FS_testtube');
      spoon_anim_sprite.visible=true;
      spoon_anim_sprite.animations.play('anim');
      spoon_anim_sprite.animations.currentAnim.onComplete.add(this.spoon_anim_end,this);
      //testtube_FS_anim.animations.currentAnim.onComplete.add(this.Endof_FS_pour_animation, this)

      //dialog.text="Take boiling tube with the test tube holder";
      //procedure_voice2.play();
      // procedure_voice2.onStop.add(this.enableProcedure3,this);
      arrow.visible=false;
    }
    
    else if(currentobj==testtube_holder){
   
      voice.destroy();
  voice=this.game.add.audio("audio_procedure3",1);
  voice.play();
      testtube_holder.visible=false;
      testtube_FS.visible=false;
      
      testtube_holder_testtube.visible=true;
     
      arrow.visible=false;
      burner_knobe_front.inputEnabled = true;
      burner_knobe_front.input.useHandCursor = true;
      dialog.text="Drag the boiling tube to the burner by using the test tube holder.";
      // procedure_voice3.play();
      arrow.x=900;
      arrow.angle=90;
      arrow.y=510;
      arrow_y=510;
      arrow.visible=true;
      collider.reset(370,450);
      collider.scale.setTo(3,5);

      

      // fNames=['test_tube0002.png','test_tube0003.png','test_tube0004.png',
      //       'test_tube0005.png',
      //       'test_tube0006.png',
      //       'test_tube0007.png',
      //       'test_tube0008.png',  
      //       'test_tube0009.png',
      //       'test_tube0010.png',
      //       'test_tube0011.png',
      //       'test_tube0012.png'];
      
      interval=0;
      incr=0;
      heatFlag=true;
      // burner.visible=false;
      // burner_on=this.game.add.sprite(248,923,'burner_sprites','Burner_sprite_0.png');//
      // burner_on.anchor.setTo(0.8,1);
      // burner_on.animations.add('burner',['Burner_sprite_0.png',
      //       'Burner_sprite_1.png','Burner_sprite_2.png','Burner_sprite_3.png',
      //        ], 4, true, false);

      // burner_on.scale.setTo(.8,.8);
      // burner_on.animations.play('burner');
      //testtube_h.play('anim', false);
    }
    else if(currentobj==testtube_holder_testtube)
    {
      voice.destroy();
  voice=this.game.add.audio("audio_procedure4",1);
  voice.play();
    testtube_holder_testtube.visible=false;
    testtube_FS_heat_anim1.visible=true;
    testtube_holder2.visible=true;
    arrow.visible=false;
    
    dialog.text="Turn on the burner and obeserve what happens."; 
     
    arrow.x=300;
    arrow.angle=0;
    arrow.y=890;
    arrow_y=300;
    arrow.visible=true;
    ///////////////////collider.visible=false;
    }
    else if(currentobj==testTubeWhiteHolder)
    {
      arrow.visible=false;
      testTubeWhiteHolder.visible=false;
      testTubeWhite.visible=true;
      smoke2_anim.visible=true;
      smoke2_anim.animations.play('smoke_sprite');
      
      voice.destroy();
  voice=this.game.add.audio("audio_procedure14",1);
  voice.play();
    smoke2_anim.visible=false;
    dialog.text="Now check what happens when you drop some water into it.";
      copperGrouptween=this.game.add.tween(copperGroup).to( {x:2500 }, 1000, Phaser.Easing.Out, true);
      copperGrouptween.onComplete.add(function () {
        tween1=this.game.add.tween(beakerGroup).to( {x:0 }, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          this.game.time.events.add(Phaser.Timer.SECOND*2,this.takeDropper, this);
        }.bind(this));
      }.bind(this));

    }else if(currentobj==water_dropper){
      arrow.visible=false;
      water_dropper.x=collider2.x+50;
      water_dropper.y=collider2.y;//-50;
      water_dropper.animations.play('anim');
      //Testube_Front1.visible=false;
      testTubeWhite.visible=false;
      White_to_blue.visible=true;
      White_to_blue.animations.play('anim');
      White_to_blue.animations.currentAnim.onComplete.add(function () {
        water_dropper.visible=false;
        voice.destroy();
  voice=this.game.add.audio("audio_procedure17",1);
  voice.play();
    smoke2_anim.visible=false;
    dialog.text="Observe that when adding water, white copper sulphate turns blue.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.enablePlayButton, this);
      }.bind(this)); 
    }
    
    else if(currentobj==litmus_sprite)
    {

      arrow.visible=false;
      litmus_sprite.x=825;
      litmus_sprite.y=380;
      litmus_sprite.animations.play('anim');

      litmus_sprite.animations.currentAnim.onComplete.add(this.expObservation,this);

    }
    // else if(currentobj=="null") {
    //   //testtube_FS.reset(testtube_FS.xp,testtube_FS.yp);
    // }
    
  },

  expObservation:function()
  {
    voice.destroy();
  voice=this.game.add.audio("audio_procedure12",1);
  voice.play();
    smoke2_anim.visible=false;
    dialog.text="Observe that the blue litmus turns to red color. It means the gas,\nwhich is evolved in this experiment is acidic in nature. ";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.enablePlayButton, this);
  },

  enablePlayButton:function()
  {
    voice.destroy();
    voice=this.game.add.audio("audio_procedure13",1);
    voice.play();
  dialog.text="Click on the next button to do the experiment.";
  play.visible=true;

  },

  takeDropper:function(){
    arrow.x=Dropper.x;
      arrow.y=Dropper.y-130;
      arrow_y=arrow.y;
      arrow.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("audio_procedure15",1);
      voice.play();
      dialog.text="Take a Dropper.";
      Dropper.inputEnabled = true;
      Dropper.input.useHandCursor = true;
      Dropper.events.onInputDown.add(this.clickOnDropper, this);
      droperFlag=true;
  },
  clickOnDropper:function(){
    if(droperFlag){
      arrow.visible=false;
      droperFlag=false;
      Dropper.angle=0;
        tween1=this.game.add.tween(Dropper).to( {x:water_dropper.x, y:water_dropper.y}, 400, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Dropper.visible=false;
          //cap1.visible=false;
          //cap1_1.visible=true;
          water_dropper.visible=true;
          tween2=this.game.add.tween(water_dropper).to( {y:810}, 400, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            voice.destroy(true);
            voice=this.game.add.audio("audio_procedure16",1);
            voice.play();
            dialog.text="Add 2-3 drops of water to the white copper sulphate.";
            arrow.x=water_dropper.x+15;
            arrow.y=water_dropper.y-230;
            arrow_y=arrow.y;
            arrow.visible=true;
            water_dropper.xp=water_dropper.x;
            water_dropper.yp=water_dropper.y;
            //console.log(Testube_Front1.x+"//"+Testube_Front2.x+"//"+Testube_Front3.x+"//"+Testube_Front4.x);
            //collider.x=testtube_empty1.x+30;
            //collider.y=testtube_empty1.y-20;
            water_dropper.inputEnabled = true;
            water_dropper.input.useHandCursor = true;
            water_dropper.input.enableDrag(true);
          }.bind(this));
        }.bind(this));
    }
  },
  copperTweenEnd:function()
  {
    litmusGrouptween=this.game.add.tween(litmusGroup).to( {x:0 }, 1000, Phaser.Easing.Out, true);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.nextStage, this);

  },

  nextStage:function()
  {
    voice.destroy();
  voice=this.game.add.audio("audio_procedure10",1);
  voice.play();
    dialog.text="Hold the moist blue litmus paper with help of the tongs.";
    arrow.x=1550;
    arrow.y=780;
    arrow_y=780;
    arrow.visible=true;
    litmus_sprite.events.onDragStart.add(function() {this.gameC(litmus_sprite)}, this);
    litmus_sprite.events.onDragStop.add(function() {this.gameC1(litmus_sprite)}, this);
    litmus_sprite.inputEnabled = true;
    litmus_sprite.input.useHandCursor = true;
    
    litmus_sprite.input.enableDrag(true);
    this.game.physics.arcade.enable(litmus_sprite);
    litmus_sprite.enableBody =true;

  },

  spoon_anim_end:function()
  {
    voice.destroy();
  voice=this.game.add.audio("audio_procedure2",1);
  voice.play();
  arrow.x=1700;
  arrow.y=780;
  arrow_y=780;
  arrow.visible=true;
  testtube_holder.events.onDragStart.add(function() {this.gameC(testtube_holder)}, this);
  testtube_holder.events.onDragStop.add(function() {this.gameC1(testtube_holder)}, this);
  testtube_holder.inputEnabled = true;
  testtube_holder.input.useHandCursor = true;
  testtube_holder.events.onInputDown.add(this.ClickOnSpoon, this);
  testtube_holder.input.enableDrag(true);
  this.game.physics.arcade.enable(testtube_holder);
  testtube_holder.enableBody =true;
  collider.reset(840,500);
testtube_FS_anim.animations.play('FS_testtube');
testtube_FS_anim.animations.currentAnim.onComplete.add(this.Endof_FS_pour_animation, this)
dialog.text="Hold the test tube with the test tube holder.";
  },
  enableProcedure3:function(){
      // arrow.x=1700;
      // arrow.y=780;
      // arrow_y=780;
      // arrow.visible=true;
      // testtube_holder.events.onDragStart.add(function() {this.gameC(testtube_holder)}, this);
      // testtube_holder.events.onDragStop.add(function() {this.gameC1(testtube_holder)}, this);
      // testtube_holder.inputEnabled = true;
      // testtube_holder.input.useHandCursor = true;
      // testtube_holder.events.onInputDown.add(this.ClickOnSpoon, this);
      // testtube_holder.input.enableDrag(true);
      // this.game.physics.arcade.enable(testtube_holder);
      // testtube_holder.enableBody =true;
      // collider.reset(840,500);
      //collider.visible=true;
      //collider.scale.setTo(1,5);
  },
  Endof_FS_pour_animation:function()
  {
  // procedure_voice2.play();
  spoon.reset(spoon.xp,spoon.yp);
  spoon.frameName="Spoon.png";
  testtube_FS_anim.visible=false;
  spoon_anim_sprite.visible=false;
  testtube_FS.visible=true;
  
  spoon.visible=true;
  },

  Endof_FS_heat_anim1:function()
  {
    voice.destroy();
    voice=this.game.add.audio("audio_procedure5",1);
    voice.play();
    dialog.text="See the color of the crystal turns to white color crystals and some\nvapour formed inside the test tube with smoke.";
    dialog.y=45;
    //this.game.time.events.add(Phaser.Timer.SECOND*7,this.Onsecond_stage_heating, this);
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.EndOf_reaction, this);

  },

  Onsecond_stage_heating:function()
  {
    voice.destroy();
  voice=this.game.add.audio("audio_procedure6",1);
  voice.play();
    dialog.text="Observe what is going to happen on further heating?";
    dialog.y=45;
   
    testtube_FS_heat_anim1.visible=false;
    testtube_FS_heat_anim2.visible=true;
    testtube_FS_heat_anim2.animations.play('testTube_heat_sprite');
    testtube_FS_heat_anim2.animations.currentAnim.onComplete.add(this.Endof_FS_heat_anim2,this);
  },

  Endof_FS_heat_anim2:function(){
    dia_box.scale.setTo(.8,1);
    voice.destroy();
    voice=this.game.add.audio("audio_procedure7",1);
    voice.play();
    dialog.text="See the vapour inside the test tube completely vapourized and\nthe color of the  residue becomes black. \nThe experiment also produces smell of burning sulphur.";
    dialog.y=45;
    testtube_FS_heat_anim2.visible=false;
    testtube_holder2.visible=false;
    testTubeWhiteHolder.visible=true;
    ////////////////////////collider.visible=false;
    this.game.time.events.add(Phaser.Timer.SECOND*9,this.EndOf_reaction, this);
    // procedure_voice7.onStop.add(this.EndOf_reaction,this);
    //procedure_voice4.onStop.add(this.Onsecond_stage_heating,this);
  },

  EndOf_reaction:function()
  {
    voice.destroy();
  voice=this.game.add.audio("audio_procedure8",1);
  voice.play();
    dia_box.scale.setTo(.8,.7);
    dialog.text="Now turn off the burner.";
    arrow.x=300;
    arrow.angle=0;
    arrow.y=890;
    arrow_y=300;
    arrow.visible=true;
    burner_knobe_front.inputEnabled = true;
    burner_knobe_front.input.useHandCursor = true;
    //setTimeout(this.ForNext_screen, 2000);
  },

  initiate_smoke_anim:function()
  {

    smoke_anim.animations.play('smoke_sprite');
  },


  ForNext_screen:function()
  {
    // dia_box.scale.setTo(.8,.7);
    // procedure_voice7.play();
    // dialog.text="Click on the next button to see the observations.";
    // burner_flame_anim.visible=false;
    // smoke_anim.visible=false;
    // burner_knobe_front.angle=0;
    // play.visible=true;
    
  },
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible&&arrow.angle==90)
    {
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }

    if(arrow.visible&&arrow.angle==0)
    {
      arrow.x+=(3+DeltaTime);
      if(arrow.x>=(arrow_y+50))
      {
        arrow.x=arrow_y;
      }
    }
    ///////////////////////////////

    // if(Is_end_FS_heat_anim2==true)
    // {
    //   console.log("sucesssssssssssssssssssss upadte");
    //   interval++;
    //   console.log(interval);
    //   if(interval==35)
    //   {
    //     console.log("sucesssssssssssssssssssss");
    //     dia_box.scale.setTo(.8,.7);
    //     //procedure_voice8.play();
    //      dialog.text="Now take the test tube back to the test tube stand";
    //     burner_flame_anim.visible=false;
    //     smoke_anim.visible=false;
    //     burner_knobe_front.angle=0;
    //     // play.visible=true;
        
    //   }
        
    // }

    // if(heatFlag){
    //   //console.log(interval);
    //   interval++;
    //   if(interval>=250){
    //     interval=0;
    //     testtube_h.frameName=fNames[incr];
    //     console.log(incr+"...........");
    //     if(incr==3){
    //       dialog.y=40;
    //       dialog.text="Observe that on heating green colour crystals turns to \nwhite colour crystals";
    //       procedure_voice5.play();
    //       interval=-250;
    //     }
    //     if(incr==8){
    //       dialog.y=40;
    //       dialog.text="Observe that on further heating white colour crystals turns to \ndark brown crystals";
    //       procedure_voice6.play();
    //       interval=-250;
    //     }
    //     incr++;
    //     if(incr==10){
    //       play.visible=true;
    //       procedure_voice4.play();
          /*heatFlag=false;
          burner_on.visible=false;
          dia_box.visible=false;
          dialog.visible=false;
          popup = this.game.add.sprite(370,160,'popup');
          popup.scale.setTo(.5,.5);
          headingstyle={ font: "54px Segoe UI", fill: "#000000", align: "center" };
          headingtext1=this.game.add.text(650,200,"Decomposition Reaction",headingstyle);
          content1="";
          voice2.play();
          //contenttext1=this.game.add.text(420,270,content1,contentstyle);
          equations1=this.game.add.sprite(550, 500,'equations','eq1.png');
          equations1.anchor.setTo(.5,.5);
          equations1.scale.setTo(0,0);
          equations1.alpha=0;
          this.game.add.tween(equations1).to( {alpha:1}, 1000, Phaser.Easing.Out, true);
          tween2=this.game.add.tween(equations1.scale).to( {x:.2,y:.2}, 1000, Phaser.Easing.Out, true);
          tween2.onComplete.add(function(){this.tweenComplete2()}, this);*/
    //     }
    //   }
    // }
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(collider2.enableBody && currentobj!=null){
      this.game.physics.arcade.overlap(currentobj, collider2,function() {this.match_Obj()},null,this);
  }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==spoon){
        currentobj.frameName="Spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=1350;
        arrow.y=750;
        arrow_y=750;
      }else if(currentobj==testtube_holder){
        arrow.x=1700;
      arrow.y=780;
      arrow_y=780;
      arrow.visible=true;
      }
      else if(currentobj==testtube_holder_testtube){
        arrow.x=900;
      arrow.angle=90;
      arrow.y=510;
      arrow_y=510;
      arrow.visible=true;
      }

      else if(currentobj==testTubeWhiteHolder)
      {
        arrow.x=450;
        arrow.y=250;
        arrow_y=250;
        arrow.visible=true;

      }
      else if(currentobj==water_dropper)
      {
        arrow.x=water_dropper.x+15;
            arrow.y=water_dropper.y-230;
            arrow_y=arrow.y;
        arrow.visible=true;

      }
      else if(currentobj==litmus_sprite)
      {

        arrow.x=1550;
        arrow.y=780;
        arrow_y=780;
        arrow.visible=true;
        litmus_sprite.alpha=0;
        tongs.visible=true;
        litmusPaper.visible=true;
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
      toObservations:function()
      {
      voice.destroy();
    //   voice2.destroy();
    //  // procedure_voice0.destroy();
    //   procedure_voice1.destroy();
    //   procedure_voice2.destroy();
    //   procedure_voice3.destroy();
    //   procedure_voice4.destroy();
    //   procedure_voice5.destroy();
    //   procedure_voice6.destroy();
    //   procedure_voice7.destroy();
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
      //   voice2.destroy();
      //  // procedure_voice0.destroy();
      //   procedure_voice1.destroy();
      //   procedure_voice2.destroy();
      //   procedure_voice3.destroy();
      //   procedure_voice4.destroy();
      //   procedure_voice5.destroy();
      // procedure_voice6.destroy();
      // procedure_voice7.destroy();
        //this.state.start("Aim", true, false, ip);
        window.location.href="../index.php";
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  // voice2.destroy();
 // procedure_voice0.destroy();
      // procedure_voice1.destroy();
      // procedure_voice2.destroy();
      // procedure_voice3.destroy();
      // procedure_voice4.destroy();
      // procedure_voice5.destroy();
      // procedure_voice6.destroy();
      // procedure_voice7.destroy();
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // voice2.destroy();
  // //procedure_voice0.destroy();
  // procedure_voice1.destroy();
  // procedure_voice2.destroy();
  // procedure_voice3.destroy();
  // procedure_voice4.destroy();
  //     procedure_voice5.destroy();
  //     procedure_voice6.destroy();
  //     procedure_voice7.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
