var exp_selection = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;


}

exp_selection.prototype ={

init: function( ipadrs) 
{
  ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  //window.location.href="Experiment_D/index.php";
 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
 // leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiments /////////////////////////////////////
  button_Base= this.game.add.sprite(160,140,'dialogue_box')
  combination_btn = this.game.add.sprite(900,260,'title_a_button');
  combination_btn.anchor.setTo(.5,.5);
  //combination_btn.scale.setTo(.8,.8);
  combination_btn.inputEnabled = true;
  combination_btn.input.useHandCursor = true;
  combination_btn.events.onInputOver.add(this.toMouseOver, this);
  combination_btn.events.onInputOut.add(this.toMouseOut, this);
  combination_btn.events.onInputDown.add(function() {this.toExperiment_A()}, this);
  // combination_btn.events.onInputDown.add(function() {this.toCombination_Reaction()}, this);
  ///////////////////////////////////////////////////
  decomposition_btn = this.game.add.sprite(900,420,'title_b_button');
  decomposition_btn.inputEnabled = true;
  decomposition_btn.anchor.setTo(.5,.5);
  decomposition_btn.inputEnabled = true;
  decomposition_btn.input.useHandCursor = true;
  decomposition_btn.events.onInputOver.add(this.toMouseOver, this);
  decomposition_btn.events.onInputOut.add(this.toMouseOut, this);
  decomposition_btn.events.onInputDown.add(function() {this.toExperiment_B()}, this);
  //////////////////////////////////////////////////////////////
  displacement_btn = this.game.add.sprite(900,590,'title_c_button');
  displacement_btn.anchor.setTo(.5,.5);
  displacement_btn.inputEnabled = true;
  displacement_btn.inputEnabled = true;
  displacement_btn.input.useHandCursor = true;
  displacement_btn.events.onInputOver.add(this.toMouseOver, this);
  displacement_btn.events.onInputOut.add(this.toMouseOut, this);
  displacement_btn.events.onInputDown.add(function() {this.toExperiment_C()}, this);
  
  //////////////////////////////////////////////////////////////
  doubledisplacement_btn = this.game.add.sprite(900,750,'title_d_button');
  doubledisplacement_btn.anchor.setTo(.5,.5);
  doubledisplacement_btn.inputEnabled = true;
  doubledisplacement_btn.inputEnabled = true;
  doubledisplacement_btn.input.useHandCursor = true;
  doubledisplacement_btn.events.onInputOver.add(this.toMouseOver, this);
  doubledisplacement_btn.events.onInputOut.add(this.toMouseOut, this);
  doubledisplacement_btn.events.onInputDown.add(function() {this.toExperiment_D()}, this);
///////////////////////////////////////////////////////////////////
  doubledisplacement_btn = this.game.add.sprite(900,920,'title_e_button');
  doubledisplacement_btn.anchor.setTo(.5,.5);
  doubledisplacement_btn.inputEnabled = true;
  doubledisplacement_btn.inputEnabled = true;
  doubledisplacement_btn.input.useHandCursor = true;
  doubledisplacement_btn.events.onInputOver.add(this.toMouseOver, this);
  doubledisplacement_btn.events.onInputOut.add(this.toMouseOut, this);
  doubledisplacement_btn.events.onInputDown.add(function() {this.toExperiment_E()}, this);

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 /*play = this.game.add.sprite(1600,870,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toMaterials, this);*/


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },

  ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

  // For Full screen checking.
  
      gofull: function()
      {
        if (this.game.scale.isFullScreen)
            {
            this.game.scale.stopFullScreen();
            }
        else
            {
             this.game.scale.startFullScreen(false);
            }  
      },

  //For to next scene   
 
  toExperiment_A:function()
      {
        console.log("oooooooooooooooookkk");
        window.location.href="Experiment_A/index.php";

      },
    toExperiment_B:function(){
    window.location.href="Experiment_B/index.php";
  },  
  toExperiment_C:function(){
    window.location.href="Experiment_C/index.php";
  },  
  toExperiment_D:function(){
    window.location.href="Experiment_D/index.php";
  },  

  toExperiment_E:function(){
    window.location.href="Experiment_E/index.php";
  },  
// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

gotoHome:function()
{
  voice.destroy();
  this.state.start("Aim", true, false);
},

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/

// To quit the experiment
closeTheGame:function()
{
voice.destroy();
window.open(loc,"_self");
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link
//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

toMouseOver:function(e){
  e.scale.x=1.02;
  e.scale.y=1.02;
  /*decomposition_btn.scale.x=1.1;
  decomposition_btn.scale.y=1.1;
  displacement_btn.scale.x=1.1;
  displacement_btn.scale.y=1.1;
  doubledisplacement_btn.scale.x=1.1;
  doubledisplacement_btn.scale.y=1.1;*/
},
toMouseOut:function(e){
  e.scale.x=1;
  e.scale.y=1;
  /*decomposition_btn.scale.x=1;
  decomposition_btn.scale.y=1;
  displacement_btn.scale.x=1;
  displacement_btn.scale.y=1;
  doubledisplacement_btn.scale.x=1;
  doubledisplacement_btn.scale.y=1;*/
},
}
