var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        
        
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
        this.game.load.image("bg1","assets/bg1.png");
        this .game.load.image("bg2","assets/Decomposition_reaction/Bg3.jpg");
        
        this.game.load.image('transBackground','assets/transBackground.png');
      this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        this.game.load.image('combination_btn','assets/combination_btn.png');
        this.game.load.image('decomposition_btn','assets/decomposition_btn.png');
        this.game.load.image('displacement_btn','assets/displacement_btn.png');
        this.game.load.image('doubledisplacement_btn','assets/doubledisplacement_btn.png');

        this.game.load.image('title_a_button','assets/title_a.png');
        this.game.load.image('title_b_button','assets/title_b.png');
        this.game.load.image('title_c_button','assets/title_c.png');
        this.game.load.image('title_d_button','assets/title_d.png');
        this.game.load.image('title_e_button','assets/title_e.png');



       

//////Audio for decomposition reaction/////////////////////////
         this.game.load.audio('Aim_1','assets/audio/Aim_1.mp3');
         this.game.load.audio('Aim_2','assets/audio/Aim_2.mp3');
        

        

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      this.game.state.start("Aim");//Simulation_hot1
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");
      //this.game.state.start("Exp_Selection");

	}
}

