var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
 
  //var corrent_val1;
  var resistance_val1;
  
 // var corrent_val2;
  var resistance_val2;
  
  //var corrent_val3;
  var resistance_val3;
  
  //var corrent_val4;
  var resistance_val4;
 
  var R1voltage_val1;
  var R1voltage_val2;
  var R1voltage_val3;
  var R2voltage_val1;
  var R2voltage_val2;
  var R2voltage_val3;
  var R3voltage_val1;
  var R3voltage_val2;
  var R3voltage_val3;

  var R1current_val1;
  var R1current_val2;
  var R1current_val3;
  var R2current_val1;
  var R2current_val2;
  var R2current_val3;
  var R3current_val1;
  var R3current_val2;
  var R3current_val3;

}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
       
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         //this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
       // this.game.load.image('dialogue_box3','assets/Ohms_law/dialogbox2.png');
       this.game.load.image('observation_1','assets/observation_box.png');
// this.game.load.image('observation_table','assets/Ohms_law/observationTable.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
//////////////////////////////Experiment images//////////////////////////////////
        this.game.load.image('bg','assets/Bg3.jpg');
       
      this.game.load.image('direction','assets/Direction.png');
////////////////Displacement//////////////
      this.game.load.image('beaker_front','assets/displacement/CuSO4_Beaker.png');
      this.game.load.image('beaker_back','assets/displacement/Beaker_back.png');
      this.game.load.image('droper','assets/displacement/CuSO4_To_droper/CuSO4_To_droper0016.png');
      this.game.load.image('rackback','assets/displacement/Testube_Rack_Back.png');
      this.game.load.image('rackfront','assets/displacement/Testube_Rack_Front.png');
      this.game.load.image('testtubefront','assets/displacement/Testube.png');
      this.game.load.image('testtubeback','assets/displacement/Testube_back.png');
      this.game.load.image('nailtile','assets/displacement/Tile.png');
      this.game.load.image('nailontable1','assets/displacement/Nail_v1.png');
      this.game.load.image('nailontable2','assets/displacement/Nail_v2.png');
      this.game.load.image('nail','assets/displacement/nail.png');
      this.game.load.image('stand','assets/displacement/Stand.png');
      this.game.load.image('holder1','assets/displacement/holder1.png');
      this.game.load.image('standportion1','assets/displacement/Testune_Stand_portion.png');
      this.game.load.image('standportion2','assets/displacement/Testune_Stand_portion_1.png');
      this.game.load.image('Clock','assets/displacement/Clock.png');
      this.game.load.image('ClockNeedleBig','assets/displacement/Clock_Needle_Big.png');
      this.game.load.image('ClockNeedleSmall','assets/displacement/Clock_Needle_Small.png');
      this.game.load.image('testtubeOnRacklast','assets/displacement/CuSO4_Colour_Change0015.png');
 this.game.load.image('testubewithsolution','assets/displacement/CuSO4_to_Testube0015.png');
      this.load.atlasJSONHash('colourchange', 'assets/displacement/colourchange.png', 'assets/displacement/colourchange.json')
       this.load.atlasJSONHash('pouron_testube', 'assets/displacement/pouron_Testube.png', 'assets/displacement/pouron_Testube.json')
       this.load.atlasJSONHash('droper', 'assets/displacement/droper.png', 'assets/displacement/droper.json')
         this.load.atlasJSONHash('nailcolourchange', 'assets/displacement/nailcolourchange.png', 'assets/displacement/nailcolourchange.json')
         this.load.atlasJSONHash('dropertotesttube', 'assets/displacement/dropertotesttube.png', 'assets/displacement/dropertotesttube.json')
         this.load.atlasJSONHash('nailthread', 'assets/displacement/nailthread.png', 'assets/displacement/nailthread.json')
         this.load.atlasJSONHash('holder', 'assets/displacement/holder.png', 'assets/displacement/holder.json')
 this.load.atlasJSONHash('arrow', 'assets/displacement/arrow.png', 'assets/displacement/arrow.json')



  /////////////////////Experiment_A1_sounds////////////////////////////////////
        //this.game.load.audio('A_aim','assets/audio/Experiment_A1/Aim.mp3');
        this.game.load.audio('A_precaution1','assets/audio/Experiment_A1/precaution_1.mp3');
        this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
        this.game.load.audio('lets_perform_Exp','assets/audio/Experiment_A1/lets_perform_Exp.mp3');

        this.game.load.audio('A_observation','assets/audio/Experiment_A1/A_observation.mp3');
       this.game.load.audio('A_procedure1','assets/audio/Experiment_A1/procedure_1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Experiment_A1/procedure_2.mp3');
 
        this.game.load.audio('A_exp_procedure1','assets/audio/Experiment_A1/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/Experiment_A1/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/Experiment_A1/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/Experiment_A1/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/Experiment_A1/A_exp_procedure5.mp3');
        this.game.load.audio('A_exp_procedure6','assets/audio/Experiment_A1/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/Experiment_A1/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/Experiment_A1/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/Experiment_A1/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/Experiment_A1/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/Experiment_A1/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/Experiment_A1/A_exp_procedure12.mp3');
        this.game.load.audio('A_exp_procedure13','assets/audio/Experiment_A1/A_exp_procedure13.mp3');
        this.game.load.audio('A_exp_procedure14','assets/audio/Experiment_A1/A_exp_procedure14.mp3');
      this.game.load.audio('A_exp_procedure15','assets/audio/Experiment_A1/A_exp_procedure15.mp3');
      this.game.load.audio('A_exp_procedure16','assets/audio/Experiment_A1/A_exp_procedure16.mp3');
      this.game.load.audio('A_exp_procedure17','assets/audio/Experiment_A1/A_exp_procedure17.mp3');
      this.game.load.audio('A_exp_procedure18','assets/audio/Experiment_A1/A_exp_procedure18.mp3');
      this.game.load.audio('A_exp_procedure19','assets/audio/Experiment_A1/A_exp_procedure19.mp3');
      this.game.load.audio('A_exp_procedure20','assets/audio/Experiment_A1/A_exp_procedure20.mp3');
    this.game.load.audio('A_exp_procedure21','assets/audio/Experiment_A1/A_exp_procedure21.mp3');

      },
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
    

     // this.game.state.start("Aim");//Simulation_hot1
  this.game.state.start("Materials");//Starting the gametitle state
    //this.game.state.start("Theory");//Simulation_hot1
   // this.game.state.start("Lab_Precautions");
   //this.game.state.start("Procedure");
  //this.game.state.start("Experiment_A1");//Starting the gametitle state
 //this.game.state.start("Observations");//hot
   //  this.game.state.start("Result");//Starting the gametitle state
    //this.game.state.start("Viva");//Starting the gametitle state
	}
}

