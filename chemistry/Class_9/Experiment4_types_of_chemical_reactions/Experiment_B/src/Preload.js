var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        //this.game.load.atlasJSONHash('ferrous_sulphate', 'assets/ferrous_sulphate.png', 'assets/ferrous_sulphate.json');
        //this.game.load.atlasJSONHash('burner_sprites', 'assets/burner_sprites.png', 'assets/burner_sprites.json');
      
        this.game.load.json('questions','data/questions1.json');
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
        this.game.load.image("bg","assets/Burning_Mg/Bg3.jpg");
        this.game.load.image("bg1","assets/bg1.png");
        //this .game.load.image("bg2","assets/Decomposition_reaction/Bg3.jpg");
        
        this.game.load.image('transBackground','assets/transBackground.png');
        this.game.load.image("arrow","assets/arrow.png");
        this.game.load.image("collider","assets/col.png");
        this.game.load.image("Button_Bg","assets/Black_panel.png");
           
        this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');

        this.game.load.image('observation_table_decomposition','assets/observation1.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        
        this.game.load.audio('title_a','assets/audio/ttl.mp3');
        this.game.load.audio('next_snd','assets/audio/next_snd.mp3');
        this.game.load.audio('sim_1','assets/audio/inst1.mp3');
        this.game.load.audio('sim_2','assets/audio/inst2.mp3');


        
     

///////////////////////////////////graphics contents////////////////////////////////////////////////
this.game.load.image('tong','assets/Burning_Mg/Plucker.png');
this.game.load.image('MgRibbon','assets/Burning_Mg/Ribbon.png');
this.game.load.image('glass_plate','assets/Burning_Mg/Plate.png');
this.game.load.image('burner','assets/Burning_Mg/Burner.png');
this.game.load.image('burner_knobe_back','assets/Burning_Mg/Flame_Knobe_back.png');
this.game.load.image('burner_knobe_front','assets/Burning_Mg/Flame_Knobe_front.png');
this.game.load.image('burner_wire','assets/Burning_Mg/Burner_wire.png');
this.game.load.image('water_beaker','assets/Burning_Mg/Beaker_with_Water.png');
this.game.load.image('beaker_back','assets/Burning_Mg/Beaker_back.png');
this.game.load.atlasJSONHash('burner_flame_sprite', 'assets/Burning_Mg/animations/Flame.png', 'assets/Burning_Mg/animations/Flame.json');
this.game.load.atlasJSONHash('flame_sprite', 'assets/Burning_Mg/animations/Flame.png', 'assets/Burning_Mg/animations/Flame.json');
this.game.load.atlasJSONHash('Mg_burning_sprite', 'assets/Burning_Mg/animations/Mg_burning_sprite.png', 'assets/Burning_Mg/animations/Mg_burning_sprite.json');
this.game.load.atlasJSONHash('ash_to_plate_sprite', 'assets/Burning_Mg/animations/ash_to_plate_sprite.png', 'assets/Burning_Mg/animations/ash_to_plate_sprite.json');
this.game.load.atlasJSONHash('water_dropper_sprite', 'assets/Burning_Mg/animations/water_dropper_sprite.png', 'assets/Burning_Mg/animations/water_dropper_sprite.json');
this.game.load.atlasJSONHash('drop_sprite', 'assets/Burning_Mg/animations/water_drop_sprite.png', 'assets/Burning_Mg/animations/water_drop_sprite.json');
this.game.load.atlasJSONHash('smoke_sprite', 'assets/Burning_Mg/animations/Fog.png', 'assets/Burning_Mg/animations/Fog.json');
this.game.load.atlasJSONHash('litmus_sprite', 'assets/Burning_Mg/animations/litmus_color_change_sprite.png', 'assets/Burning_Mg/animations/litmus_color_change_sprite.json');
this.game.load.atlasJSONHash('water_ash_sprite', 'assets/Burning_Mg/animations/water_to_ash_sprite.png', 'assets/Burning_Mg/animations/water_to_ash_sprite.json');
//////////////////////////////for materials/////////////////////////////////////////////////////
this.game.load.image('m_tong','assets/Burning_Mg/materials/m_tongs.png');
this.game.load.image('m_ribbon','assets/Burning_Mg/materials/m_ribbon.png');
this.game.load.image('m_glassPlate','assets/Burning_Mg/materials/m_watchGlass.png');
this.game.load.image('m_burner','assets/Burning_Mg/materials/m_burner.png');
this.game.load.image('m_distilledWater','assets/Burning_Mg/materials/m_water.png');
this.game.load.image('m_litmus','assets/Burning_Mg/materials/m_redLitmus.png');
this.game.load.image('m_dropper','assets/Burning_Mg/materials/m_dropper.png');


/////////////////////////////////////new Audios///////////////////////////////////////////
this.game.load.audio('start_audio','assets/audio/Double_displacement/start_audio.mp3');

this.game.load.audio('precautions_1','assets/audio/Double_displacement/A_precaution1.mp3');
this.game.load.audio('procedure_1','assets/audio/Double_displacement/A_procedure_voice1.mp3');
this.game.load.audio('observation1','assets/audio/Double_displacement/A_observation1.mp3');
this.game.load.audio('result1','assets/audio/Double_displacement/Result_1.mp3');

this.game.load.audio('A_step1','assets/audio/Double_displacement/A_step1.mp3');
this.game.load.audio('A_step2','assets/audio/Double_displacement/A_step2.mp3');
this.game.load.audio('A_step3','assets/audio/Double_displacement/A_step3.mp3');
this.game.load.audio('A_step4','assets/audio/Double_displacement/A_step4.mp3');
this.game.load.audio('A_step5','assets/audio/Double_displacement/A_step5.mp3');
this.game.load.audio('A_step6','assets/audio/Double_displacement/A_step6.mp3');
this.game.load.audio('A_step7','assets/audio/Double_displacement/A_step7.mp3');
this.game.load.audio('A_step8','assets/audio/Double_displacement/A_step8.mp3');
this.game.load.audio('A_step9','assets/audio/Double_displacement/A_step9.mp3');
this.game.load.audio('A_step10','assets/audio/Double_displacement/A_step10.mp3');
this.game.load.audio('A_step11','assets/audio/Double_displacement/A_step11.mp3');
this.game.load.audio('A_step12','assets/audio/Double_displacement/A_step12.mp3');
this.game.load.audio('A_step13','assets/audio/Double_displacement/A_step13.mp3');

/////////////////////////////////////////////////////////////////////////////////////////////////////

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
     this.game.state.start("Materials");//Simulation_hot1
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
   //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
     //this.game.state.start("Procedure");
      //this.game.state.start("Exp_Selection");

	}
}

