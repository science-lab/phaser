var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////

var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;

// var isNaSO4taken;
// var isBaCltaken;
// var isClockRunning;
// var ispptTaken;
// var ishacltaken;
// var isbaso4taken;
 var isBurnerOn;

}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("start_audio",1);

 
//  this.arrangeScene();

 



 //voice.play();
 bg= this.game.add.sprite(0,0,'bg');
//  bg.scale.setTo(1,1.1);

///////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////

  currentobj=null;
   fNames=[];
  isBurnerOn=false;


  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  // dialog_box=this.game.add.sprite(60,20, 'dialogue_box1');
  //     dialog_box.scale.setTo(.80,.8);
  //     dialog_text="Lets perform the experiment";
  //     this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      

  //     dialog=this.game.add.text(75,60,dialog_text,fontStyle);
      

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};


 },

 



arrangeScene:function(){

  
  burner=this.game.add.sprite(60,600,'burner');
  burnerWire=this.game.add.sprite(-75,904,'burner_wire');
  burnerWire.scale.setTo(1,1);

  burnerKnobeBack=this.game.add.sprite(548,850,'burner_knobe_back');
  burnerKnobeBack.anchor.setTo(.5,.5);
  burnerKnobeFront=this.game.add.sprite(548,850,'burner_knobe_front');
  burnerKnobeFront.anchor.setTo(.5,.5);

  flame=this.game.add.sprite(514,535,'flame_sprite','Flame0001.png');
  flame.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],24,true,true);
  flame.visible=false;

  glassPlate=this.game.add.sprite(750,870,'glass_plate');
  glassPlate.scale.set(1,1);



  ashToPlate=this.game.add.sprite(1200,410,'ash_to_plate_sprite','Mg_Powder0001.png');
  ashToPlate.scale.setTo(-1,1);
  ashToPlate.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10],24,false,true);
  ashToPlate.visible=false;

  waterToAshPlate=this.game.add.sprite(750,845,'water_ash_sprite','water_Mg0001.png');
  waterToAshPlate.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],18,false,true);
  waterToAshPlate.visible=false;

  waterDrop=this.game.add.sprite(950,830,'drop_sprite','Water_Drop0001.png');
  waterDrop.animations.add('anim',[0,1,2,3,0,1,2,3],18,false,true);
  waterDrop.visible=false;

  litmusPaper=this.game.add.sprite(2500,938,'litmus_sprite');
  litmusPaper.scale.set(1.1);
  litmusPaper.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12],4,false,true);
  // litmusPaper.visible=false;

  MgBurningSprite=this.game.add.sprite(700,480,'Mg_burning_sprite','Mg_ribbon0000.png');
  MgBurningSprite.scale.setTo(-1,1);
  MgBurningSprite.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],3,false,true);
  MgBurningSprite.anchor.setTo(.5,.5);
  MgBurningSprite.visible=false;

  burningSmoke=this.game.add.sprite(495,220,'smoke_sprite','Fog0001.png');
  burningSmoke.scale.setTo(1,.5);
  burningSmoke.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49],8,false,true);
  burningSmoke.visible=false;

  MgWithTong=this.game.add.sprite(1540,950,'Mg_burning_sprite','Mg_ribbon0000.png');
  MgWithTong.scale.setTo(-1,1);

  MgWithTong.alpha=0;
  MgWithTong.anchor.setTo(.5,.5);
  // MgWithTong.visible=false;
  // MgWithTong.visible=true;

  tong=this.game.add.sprite(1755,850,'tong');
  tong.scale.setTo(-1,.6);
  // tong.visible=false;
   tong.visible=true;

  Mg=this.game.add.sprite(1400,930,'MgRibbon');
  Mg.scale.setTo(-1,1);
  Mg.angle=90;
  // Mg.visible=false;
  Mg.visible=true;
  beakerBack=this.game.add.sprite(1600,700,'beaker_back');
  beakerBack.scale.set(.6);

  dropper=this.game.add.sprite(1660,830,'water_dropper_sprite','Aluminium_Sulphate_Dropper0001.png');
  dropper.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13],18,false,true);
  dropper.scale.set(1,1.3);
  dropper.anchor.setTo(.5,.5);
  dropper.angle=-15;
    // dropper.angle=90;
  
  waterBeaker=this.game.add.sprite(1600,700,'water_beaker');
  waterBeaker.scale.set(.6);

  waterGroup=this.game.add.group();
  waterGroup.add(beakerBack);
  waterGroup.add(dropper);
  waterGroup.add(waterBeaker);
  //waterGroup.add(litmusPaper);
  waterGroup.x=2500;


arrow=this.game.add.sprite(1475,450, 'arrow');
      arrow.angle=90;
      arrow_y=130;
      arrow.visible=false;

      // arrow.visible=true;
      // arrow.x=1450;
      // arrow.y=820;
      // arrow_y=820;
  
      
collider_A=this.game.add.sprite(450,400,'collider');
collider_A.scale.setTo(4,4);
collider_A.alpha=0;
this.game.physics.arcade.enable(collider_A);
collider_A.visible=false;

 collider_B = this.game.add.sprite(870,470,'collider');
 collider_B.scale.setTo(4,8);
 collider_B.alpha=0;
 this.game.physics.arcade.enable(collider_B);
 collider_B.visible=false;

 collider_C = this.game.add.sprite(900,820,'collider');
 collider_C.scale.setTo(3,3);
 collider_C.alpha=0;
 this.game.physics.arcade.enable(collider_C);
 collider_C.visible=false;

//  collider_D = this.game.add.sprite(375,480,'collider');
//  collider_D.scale.setTo(2,4);
//  collider_D.alpha=0;
//  this.game.physics.arcade.enable(collider_D);
//  collider_D.visible=false;
 
 

 
 dialog_box=this.game.add.sprite(60,20, 'dialogue_box1');
 dialog_box.scale.setTo(.80,.8);
 dialog_text="Lets perform the experiment.";
 this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
 

 dialog=this.game.add.text(75,60,dialog_text,fontStyle);

 //For colling purpose

},

loadScene:function()
{
 
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this);
},

startExperiment:function()
{
   voice.destroy();
   voice=this.game.add.audio("A_step1",1);
   voice.play();

  arrow.visible=true;
  arrow.x=565;
  arrow.y=720;
  arrow_y=720;

  dialog.text="Turn on the bunsen burner.";
  burnerKnobeFront.inputEnabled = true;
  burnerKnobeFront.input.useHandCursor = true;
  burnerKnobeFront.events.onInputDown.add(this.clickOnBurner, this);
  // tween1=this.game.add.tween(testtube_stand_group).to( {x:0, }, 1000, Phaser.Easing.Out, true);
  // tween1.onComplete.add(function(){this.holdertweenIn()}, this);

},
clickOnBurner:function()
{
if(isBurnerOn==false)
{
  voice.destroy();
  voice=this.game.add.audio("A_step2",1);
   voice.play();

  arrow.visible=false;
  burnerKnobeFront.angle=90;
  flame.visible=true;
  flame.animations.play('anim');
  isBurnerOn=true;
  burnerKnobeFront.inputEnabled = false;
  burnerKnobeFront.input.useHandCursor = false;
  dialog.text="Hold the Magnesium ribbon with the tongs.";
  
  arrow.x=1550;
  arrow.y=800;
  arrow_y=800;
  arrow.visible=true;
  MgWithTong.events.onDragStart.add(function() {this.onDragStart(MgWithTong)}, this);
  MgWithTong.events.onDragStop.add(function() {this.onDragStop(MgWithTong)}, this);
  MgWithTong.inputEnabled = true;
  MgWithTong.input.useHandCursor = true;
  this.game.physics.arcade.enable(MgWithTong);
  MgWithTong.input.enableDrag(true);

  
}
else if(isBurnerOn==true)
{
  voice.destroy();
  voice=this.game.add.audio("A_step7",1);
   voice.play();
  flame.visible=false;
  burnerKnobeFront.angle=0;
  isBurnerOn=false;
  burnerKnobeFront.inputEnabled = false;
  burnerKnobeFront.input.useHandCursor = false;

  dialog.text="Collect the white ash into the watch-glass.";
  arrow.x=570;
  arrow.y=290;
  arrow_y=290;
  arrow.visible=true;

  MgBurningSprite.events.onDragStart.add(function() {this.onDragStart(MgBurningSprite)}, this);
  MgBurningSprite.events.onDragStop.add(function() {this.onDragStop(MgBurningSprite)}, this);
  MgBurningSprite.inputEnabled = true;
  MgBurningSprite.input.useHandCursor = true;
  this.game.physics.arcade.enable(MgBurningSprite);
  MgBurningSprite.input.enableDrag(true);
}

},

// holdertweenIn:function()
// {
//   tween1=this.game.add.tween(testtube_holder).to( {x:1300, }, 800, Phaser.Easing.Out, true);
//   this.game.time.events.add(Phaser.Timer.SECOND*3,this.holdertweenInEnd, this); 
// },

// holdertweenInEnd:function()
// {
//   voice.destroy();
//   voice=this.game.add.audio("A_step2",1);
//   voice.play();
// dialog.text="Hold the Na\u2082SO\u2084 test tube with the test tube holder.";
// arrow.visible=true;
// arrow.x=1480;
// arrow.y=720;
// arrow_y=720;
// testtube_holder.events.onDragStart.add(function() {this.onDragStart(testtube_holder)}, this);
// testtube_holder.events.onDragStop.add(function() {this.onDragStop(testtube_holder)}, this);
// testtube_holder.inputEnabled = true;
// testtube_holder.input.useHandCursor = true;
// this.game.physics.arcade.enable(testtube_holder);
// testtube_holder.input.enableDrag(true);

// },

onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==MgWithTong)
    {
      voice.destroy();
      voice=this.game.add.audio("A_step3",1);
      voice.play();
      arrow.visible=false;
      Mg.visible=false;
      tong.visible=false;
      MgWithTong.alpha=1;
      
      collider_A.visible=true;
      collider_A.inputEnabled=true;
      collider_A.enableBody=true;

      arrow.x=570;
      arrow.y=290;
      arrow_y=290;
      arrow.visible=true;
      dialog.text="Burn the magnesium ribbon carefully with the tongs.";
      currentobj.xp=1540;
      currentobj.yp=950;
    }
    else if(currentobj==MgBurningSprite)
    {
      arrow.visible=false;
      collider_B.visible=true;
      collider_B.inputEnabled=true;
      collider_B.enableBody=true;

      arrow.x=970;
      arrow.y=360;
      arrow_y=360;
      arrow.visible=true;

      currentobj.xp=700;
      currentobj.yp=480;
      
    }
    else if(currentobj==dropper)
    {
      dropper.angle=0;
      arrow.visible=false;
      arrow.x=990;
      arrow.y=820;
      arrow_y=830;
      arrow.visible=true;
      collider_B.visible=true;
      collider_B.inputEnabled=true;
      collider_B.enableBody=true;

      currentobj.xp=1660;
      currentobj.yp=830;


    }
    else if(currentobj==litmusPaper)
  {
    arrow.visible=false;
    arrow.x=990;
    arrow.y=820;
    arrow_y=820;
    arrow.visible=true;
    currentobj.xp=1200;
    currentobj.yp=835;
    collider_C.visible=true;
      collider_C.inputEnabled=true;
      collider_C.enableBody=true;




  }
      // if(isNaSO4taken==false)
      // {
      //   arrow.visible=false;
      //   currentobj.xp=1300;
      //   currentobj.yp=830;
      //   arrow.x=1015;
      //   arrow.y=520;
      //   arrow_y=520;
      //   arrow.visible=true;
      //   //collider_A.visible=true;
      //   collider_A.inputEnabled = true;
      //   collider_A.enableBody =true;

      // }
      // else if(isBaCltaken==false)
      // {
      //   arrow.visible=false;
      //   currentobj.xp=1300;
      //   currentobj.yp=830;
      //   arrow.x=935;
      //   arrow.y=520;
      //   arrow_y=520;
      //   arrow.visible=true;
      //   //collider_B.visible=true;
      //   collider_B.inputEnabled = true;
      //   collider_B.enableBody =true;

      // }
    

  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
  
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
 
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==MgWithTong)
    {
      voice.destroy();
      voice=this.game.add.audio("A_step4",1);
      voice.play();
      arrow.visible=false;
      collider_A.visible=false;
      collider_A.inputEnabled = false;
      collider_A.enableBody =false;

      MgWithTong.visible=false;
      MgBurningSprite.visible=true;
      MgBurningSprite.animations.play('anim');
      burningSmoke.visible=true;
      burningSmoke.animations.play('anim');

      dialog.text="Please wait until the ribbon completely burns.";

      MgBurningSprite.animations.currentAnim.onComplete.add(this.burningMgCompleted, this);
    }
    else if(currentobj==MgBurningSprite)
    {
      arrow.visible=false;
      collider_B.visible=false;
      collider_B.inputEnabled = false;
      collider_B.enableBody =false;

      MgBurningSprite.visible=false;
      ashToPlate.visible=true;
      ashToPlate.animations.play('anim');
      ashToPlate.animations.currentAnim.onComplete.add(this.ashToPlateCompleted, this);

    }
    else if(currentobj==dropper)
    {
      arrow.visible=false;
      ashToPlate.visible=false;
      collider_B.visible=false;
      dropper.x=963;
      dropper.y=720;
      dropper.animations.play('anim');
    
      waterDrop.visible=true;
      waterDrop.animations.play('anim');

      waterToAshPlate.visible=true;
      waterToAshPlate.animations.play('anim');

       dropper.animations.currentAnim.onComplete.add(this.waterToPlateCompleted, this);

    }
    else if(currentobj==litmusPaper)
    {
      collider_C.visible=false;
      arrow.visible=false;
      litmusPaper.x=925;
      litmusPaper.y=935;
      litmusPaper.animations.play('anim');

      litmusPaper.animations.currentAnim.onComplete.add(this.expObservation2, this);
    }

    

      // if(isNaSO4taken==false)
      // {
      //   //collider_A.visible=false;
      //   // collider_A.inputEnabled = false;
      //   // collider_A.enableBody =false;
      //   // arrow.visible=false;
      //   // testtube_holder.visible=false;
      //   // NaSO4_testtube_holder.visible=true;
      //   // NaSO4_testtube.visible=false;
  
      //   // NaSO4_testtube_holder.events.onDragStart.add(function() {this.onDragStart( NaSO4_testtube_holder)}, this);
      //   // NaSO4_testtube_holder.events.onDragStop.add(function() {this.onDragStop( NaSO4_testtube_holder)}, this);
      //   // NaSO4_testtube_holder.inputEnabled = true;
      //   // NaSO4_testtube_holder.input.useHandCursor = true;
      //   // this.game.physics.arcade.enable( NaSO4_testtube_holder);
      //   // NaSO4_testtube_holder.input.enableDrag(true);
  
      //   // voice.destroy();
      //   // voice=this.game.add.audio("A_step3",1);
      //   // voice.play();
      //   // dialog.text="Drag the Na\u2082SO\u2084 test tube to the clamp stand using the test tube\nholder."
      //   // arrow.x=1015;
      //   // arrow.y=520;
      //   // arrow_y=520;
      //   // arrow.visible=true;
  
      //   // //collider_A.visible=false;
      //   // isNaSO4taken=true;
        

      // }
      // else if(isBaCltaken==false)
      // {
      //   //collider_B.visible=false;
      //   collider_B.inputEnabled = false;
      //   collider_B.enableBody =false;
      //   arrow.visible=false;
      //   testtube_holder.visible=false;
      //   BaCl_testtube_holder.visible=true;
      //   BaCl_testtube.visible=false;
      //   BaCl_testtube_holder.events.onDragStart.add(function() {this.onDragStart(BaCl_testtube_holder)}, this);
      //   BaCl_testtube_holder.events.onDragStop.add(function() {this.onDragStop(BaCl_testtube_holder)}, this);
      //   BaCl_testtube_holder.inputEnabled = true;
      //   BaCl_testtube_holder.input.useHandCursor = true;
      //   this.game.physics.arcade.enable(BaCl_testtube_holder);
      //   BaCl_testtube_holder.input.enableDrag(true);
      //   voice.destroy();
      //   voice=this.game.add.audio("A_step5",1);
      //   voice.play();
      //   dialog.text="Now add BaCl\u2082 solution to the Na\u2082SO\u2084 solution and observe.";
      //   arrow.x=935;
      //   arrow.y=520;
      //   arrow_y=520;
      //   arrow.visible=true;
      //   //collider_B.visible=false;
      //   isBaCltaken=true;
       

      // }

  },

  expObservation2:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step11",1);
      voice.play();
    dialog.text="Observe that the red litmus turns into blue color. It means\nmagnesium oxide is basic in nature.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.endOfExperiment, this);

  },

  endOfExperiment:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step12",1);
      voice.play();
    dialog.text="You have successfully completed the experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.toObservation, this);

  },

  toObservation:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step13",1);
      voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
  waterToPlateCompleted:function()
  {
    waterDrop.visible=false;
    dropper.inputEnabled = false;
    dropper.input.useHandCursor = false;
    dropperTween1=this.game.add.tween(dropper).to( {x:1660,y:740}, 1000, Phaser.Easing.Out, true);
    dropperTween1.onComplete.add(function(){this.dropperTween1Complete()}, this);

  },
  dropperTween1Complete:function()
  {
    dropperTween2=this.game.add.tween(dropper).to( {y:830}, 10, Phaser.Easing.Out, true);
    dropperTween2.onComplete.add(function(){this.dropperTween2Complete()}, this);

  },

  dropperTween2Complete:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step10",1);
      voice.play();
    dropper.angle=-15;
    dialog.text="Put red litmus paper on to the watch glass and observe what\nhappens.";
    arrow.visible=false;
    arrow.x=1450;
    arrow.y=820;
    arrow_y=820;
    arrow.visible=true;

    litmusPaper.events.onDragStart.add(function() {this.onDragStart(litmusPaper)}, this);
    litmusPaper.events.onDragStop.add(function() {this.onDragStop(litmusPaper)}, this);
    litmusPaper.inputEnabled = true;
    litmusPaper.input.useHandCursor = true;
    this.game.physics.arcade.enable(litmusPaper);
    litmusPaper.input.enableDrag(true);

  },


  ashToPlateCompleted:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step8",1);
      voice.play();
    dialog.text="Good job.";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.nextStage, this);
  },
  
  nextStage:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step9",1);
      voice.play();
    dialog.text="Now add few drops of water to the watch-glass containing ash.";
    tween1=this.game.add.tween(waterGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
    litmusPaperTween1=this.game.add.tween(litmusPaper).to( {x:1385,y:938}, 1000, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.addWaterToAsh()}, this);
  },

  addWaterToAsh:function()
  {
    arrow.x=1700;
    arrow.y=620;
    arrow_y=620;
    arrow.visible=true;

    dropper.events.onDragStart.add(function() {this.onDragStart(dropper)}, this);
    dropper.events.onDragStop.add(function() {this.onDragStop(dropper)}, this);
    dropper.inputEnabled = true;
    dropper.input.useHandCursor = true;
    this.game.physics.arcade.enable(dropper);
    dropper.input.enableDrag(true);

  },


  burningMgCompleted:function()
  {
    // voice.destroy();
    // voice=this.game.add.audio("A_step5",1);
    //   voice.play();
    burningSmoke.visible=false;
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.expObservation1, this);
  },

  expObservation1:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step5",1);
      voice.play();
    burningSmoke.visible=false;
    dialog.text="Observe that the Magnesium burns with brilliant white light and\na white ash is formed.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextStep, this);

  },


  nextStep:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_step6",1);
      voice.play();
    dialog.text="Now turn of the burner.";
    burnerKnobeFront.inputEnabled = true;
    burnerKnobeFront.input.useHandCursor = true;
    burnerKnobeFront.events.onInputDown.add(this.clickOnBurner, this);
  },

  
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }

    // if(isClockRunning==true)
    // {
    //   clock_long_needle.rotation+=DeltaTime*0.5;
    //   clock_short_needle.rotation+=DeltaTime*0.04;
    //  // clock_short_needle.rotation+=.025+DeltaTime;
    //   console.log("angle="+clock_long_needle.rotation);
    //   console.log("angle="+clock_long_needle.angle);
    //   if(clock_long_needle.rotation>=6.28)
    //   {
    //     isClockRunning=false;
        
    //   }
    // }
    
   
  },
 
  detectCollision:function(){
   

     
    if(collider_A.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj,collider_A,function() {this.match_Obj()},null,this);
        collider=collider_A;
        
    }
    if(collider_B.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
        collider=collider_B;
    }
    if(collider_C.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_C,function() {this.match_Obj()},null,this);
        collider=collider_C;
    }
    // if(collider_D.enableBody && currentobj!=null)
    // {
    //     this.game.physics.arcade.overlap(currentobj, collider_D,function() {this.match_Obj()},null,this);
    //     collider=collider_D;
    // }
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
        if(currentobj==MgWithTong)
        {
        arrow.visible=false;
        arrow.x=1550;
        arrow.y=800;
        arrow_y=800;
        arrow.visible=true;
        MgWithTong.alpha=0;
        Mg.visible=true;
        tong.visible=true;

        }
        else if(currentobj==MgBurningSprite)
        {
          arrow.visible=false;
          arrow.x=570;
          arrow.y=290;
          arrow_y=290;
          arrow.visible=true;

          

        }
        else if(currentobj==dropper)
        {

          arrow.visible=false;
          arrow.x=1700;
          arrow.y=620;
          arrow_y=620;
          arrow.visible=true;
          dropper.angle=-15;
        }
        else if(currentobj==litmusPaper)
        {

          arrow.visible=false;
          arrow.x=1450;
          arrow.y=820;
          arrow_y=820;
          arrow.visible=true;
          litmusPaper.x=1385;
          litmusPaper.y=938;
        }
        // else if(currentobj==NaSO4_testtube_holder)
        // {
        //   arrow.visible=false;
        //   arrow.x=1015;
        //   arrow.y=520;
        //   arrow_y=520;
        //   arrow.visible=true;
        // }
     
      
       
    //     arrow.x=1500;
    //     arrow.y=450;
    //     arrow_y=450;
    //     arrow.visible=true;
        
    //   }
    currentobj=null;
      
      }

         
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        window.location.href="../index.php";
        //this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
