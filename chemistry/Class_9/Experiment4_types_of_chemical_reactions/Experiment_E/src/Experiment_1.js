var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////

var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;

var isNaSO4taken;
var isBaCltaken;
var isClockRunning;
var ispptTaken;
var ishacltaken;
var isbaso4taken;


}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("start_audio",1);

 
//  this.arrangeScene();

 



 //voice.play();
 bg= this.game.add.sprite(0,0,'bg');
//  bg.scale.setTo(1,1.1);

///////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
 
  fNames=[];
  //this.addItems();
isNaSO4taken=false;
isBaCltaken=false;
isClockRunning=false;
ispptTaken=false;
ishacltaken=false;
isbaso4taken=false;
  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  // dialog_box=this.game.add.sprite(60,20, 'dialogue_box1');
  //     dialog_box.scale.setTo(.80,.8);
  //     dialog_text="Lets perform the experiment";
  //     this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      

  //     dialog=this.game.add.text(75,60,dialog_text,fontStyle);
      

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};


 },

 



arrangeScene:function(){

  clock=this.game.add.sprite(500,200,'clock');
  clock.scale.setTo(.6,.6);

  clock_short_needle=this.game.add.sprite(583,285,'clock_short_needle');
  clock_short_needle.anchor.setTo(.35,.5);
  clock_short_needle.alpha=1;
  clock_short_needle.angle=60;

  clock_long_needle=this.game.add.sprite(583,285,'clock_long_needle');
  clock_long_needle.anchor.setTo(.5,.75);
  clock_long_needle.alpha=1;
  clock_long_needle.angle=0;
  clock.visible=false;
  clock_short_needle.visible=false;
  clock_long_needle.visible=false;

 
testtube_back1=this.game.add.sprite(985,621,'testtube_back');
testtube_back1.scale.setTo(.5,.5);
testtube_back2=this.game.add.sprite(905,621,'testtube_back');
testtube_back2.scale.setTo(.5,.5);
testtube_back3=this.game.add.sprite(830,622,'testtube_back');
testtube_back3.scale.setTo(.5,.5);
testtube_back4=this.game.add.sprite(408,590,'testtube_back');
testtube_back4.scale.setTo(.5,.5);
testtube_back4.visible=false;

testtube_stand_back=this.game.add.sprite(700,650,'testtube_stand_back');
testtube_stand_back.scale.setTo(.45,.45);

NaSO4_testtube=this.game.add.sprite(965,540,'bacl_to_na2so4_sprite','NaSo4_to_Bariun_Cl0001.png');
NaSO4_testtube.scale.setTo(.5,.55);
NaSO4_testtube_holder=this.game.add.sprite(840,396,'NaSO4_holder');
NaSO4_testtube_holder.scale.setTo(.5,.55);
NaSO4_testtube_holder.visible=false;
fontStyle4={ font: "28px Segoe UI", fill: "#000000", align: "left"};
NaSO4_testtube_text=this.game.add.text(980,890,"Na\u2082SO\u2084",fontStyle4);


BaCl_testtube=this.game.add.sprite(885,540,'bacl_to_na2so4_sprite','NaSo4_to_Bariun_Cl0001.png');
BaCl_testtube.scale.setTo(.5,.55);
BaCl_testtube_text=this.game.add.text(895,890,"BaCl\u2082",fontStyle4);
BaCl_testtube_holder=this.game.add.sprite(760,396,'NaSO4_holder');
BaCl_testtube_holder.scale.setTo(.5,.55);
BaCl_testtube_holder.visible=false;

BaCl_empty_testtube=this.game.add.sprite(885,540,'solution_to_empty_testtube_sprite','Solution_removing_to another_testube0001.png');
BaCl_empty_testtube.scale.setTo(.5,.55);
BaCl_empty_testtube.visible=false;

empty_testtube1=this.game.add.sprite(810,544,'solution_to_empty_testtube_sprite','Solution_removing_to another_testube0001.png');
empty_testtube1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39],24,false,true);
empty_testtube1.scale.setTo(.5,.55);

NaSO4_testtube_solution_pour_sprite=this.game.add.sprite(815,250,'solution_pour_sprite','Solution_removing_to another_testube _holder0005.png');
NaSO4_testtube_solution_pour_sprite.scale.setTo(.5,.55);
NaSO4_testtube_solution_pour_sprite.animations.add('anim1',[5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39],24,false,true);
NaSO4_testtube_solution_pour_sprite.visible=false;

ppt_holder=this.game.add.sprite(815,150,'ppt_holder');
ppt_holder.scale.setTo(.5,.55);
ppt_holder.visible=false;




testtube_stand_front=this.game.add.sprite(700,650,'testtube_stand_front');
testtube_stand_front.scale.setTo(.45,.45);



testtube_stand_group=this.game.add.group();
testtube_stand_group.add(testtube_back1);
testtube_stand_group.add(testtube_back2);
testtube_stand_group.add(testtube_back3);
testtube_stand_group.add(testtube_back4);
testtube_stand_group.add(testtube_stand_back);
testtube_stand_group.add(NaSO4_testtube);
testtube_stand_group.add(NaSO4_testtube_holder);
testtube_stand_group.add(NaSO4_testtube_text);
testtube_stand_group.add(BaCl_testtube);
testtube_stand_group.add(BaCl_testtube_holder);
testtube_stand_group.add(BaCl_testtube_text);
testtube_stand_group.add(empty_testtube1);
testtube_stand_group.add(testtube_stand_front);
testtube_stand_group.x=2500;




clamp_stand=this.game.add.sprite(200,450,'clamp_stand');
clamp_stand.scale.setTo(.7,.7);
clamp_stand_extender=this.game.add.sprite(380,612,'clamp_stand_extender');
clamp_stand_extender.scale.setTo(.8,.75);

BaCl_testtube_pour_sprite=this.game.add.sprite(398,222,'bacl_pour_sprite','Testube_with_holder_and_Naso40001.png');
BaCl_testtube_pour_sprite.scale.setTo(.5,.55);
BaCl_testtube_pour_sprite.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34],10,false,true);
BaCl_testtube_pour_sprite.visible=false;

NaSO4_testtube_on_stand_sprite=this.game.add.sprite(388,510,'bacl_to_na2so4_sprite','NaSo4_to_Bariun_Cl0001.png');
NaSO4_testtube_on_stand_sprite.scale.setTo(.5,.55);
NaSO4_testtube_on_stand_sprite.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34],10,false,true);
NaSO4_testtube_on_stand_sprite.visible=false;

NaSO4_testtube_pptset_sprite=this.game.add.sprite(388,510,'ppt_settle_sprite','NaSo4_to_Bariun_Cl0001.png');
NaSO4_testtube_pptset_sprite.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59],5,false,true);
NaSO4_testtube_pptset_sprite.scale.setTo(.5,.55);
NaSO4_testtube_pptset_sprite.visible=false;

ppt_hcl_testtube_sprite=this.game.add.sprite(388,565,'hcl_to_ppt_sprite','Hcl_to Precipitate0001.png');
ppt_hcl_testtube_sprite.scale.setTo(.5,.55);
ppt_hcl_testtube_sprite.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],24,false,true);
ppt_hcl_testtube_sprite.visible=false;


ppt_testtube_holder=this.game.add.sprite(259,355,'solution_pour_sprite','Solution_removing_to another_testube _holder0001.png');
ppt_testtube_holder.scale.setTo(.5,.55);
ppt_testtube_holder.visible=false;

baso4_testtube_holder=this.game.add.sprite(348,250,'baso4_hcl_shake_sprite','Barium_Chloride_Shake_HCL_With_Holder0001.png');
baso4_testtube_holder.scale.setTo(.5,.55);
baso4_testtube_holder.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,],24,false,true);
baso4_testtube_holder.visible=false;


clamp_stand_holder=this.game.add.sprite(380,613,'clamp_stand_holder');
clamp_stand_holder.scale.setTo(.6,.75);


hcl_bottle_back=this.game.add.sprite(1665,755,'bottle_back');
hcl_bottle_back.scale.setTo(.25,.25);


cork=this.game.add.sprite(1699,750,'cork');
cork.scale.setTo(.7,.7);

hcl_dropper=this.game.add.sprite(1600,910,'hcl_drooper_sprite','Dill_HCL_dropper_to_testube0016.png');
hcl_dropper.scale.setTo(.7,1);
hcl_dropper.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],20,false,true);
hcl_dropper.angle=90;

hcl_bottle=this.game.add.sprite(1650,750,'hcl_bottle');

hcl_group=this.game.add.group();
hcl_group.add(hcl_bottle_back);
hcl_group.add(cork);
hcl_group.add(hcl_dropper);
hcl_group.add(hcl_bottle);
hcl_group.x=2500;

testtube_holder=this.game.add.sprite(2500,830,'testtube_holder');
testtube_holder.scale.setTo(.8,.88);

  

arrow=this.game.add.sprite(1475,450, 'arrow');
      arrow.angle=90;
      arrow_y=130;
      arrow.visible=false;

      // arrow.visible=true;
      // arrow.x=1015;
      // arrow.y=250;
      // arrow_y=250;
  
      
collider_A=this.game.add.sprite(955,555,'collider');
collider_A.scale.setTo(2,3);
collider_A.alpha=0;
this.game.physics.arcade.enable(collider_A);
collider_A.visible=false;

 collider_B = this.game.add.sprite(875, 555,'collider');
 collider_B.scale.setTo(2,3);
 collider_B.alpha=0;
 this.game.physics.arcade.enable(collider_B);
 collider_B.visible=false;

 collider_C = this.game.add.sprite(795, 555,'collider');
 collider_C.scale.setTo(2,3);
 collider_C.alpha=0;
 this.game.physics.arcade.enable(collider_C);
 collider_C.visible=false;

 collider_D = this.game.add.sprite(375,480,'collider');
 collider_D.scale.setTo(2,4);
 collider_D.alpha=0;
 this.game.physics.arcade.enable(collider_D);
 collider_D.visible=false;
 
 

 
 dialog_box=this.game.add.sprite(60,20, 'dialogue_box1');
 dialog_box.scale.setTo(.80,.8);
 dialog_text="Lets perform the experiment.";
 this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
 

 dialog=this.game.add.text(75,60,dialog_text,fontStyle);

 //For colling purpose

},

loadScene:function()
{
  // voice=this.game.add.audio("A_exp_procedure0",1);
  // voice.play();
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this);
},

startExperiment:function()
{
   voice.destroy();
   voice=this.game.add.audio("A_step1",1);
   voice.play();
  dialog.text="Take Na\u2082SO\u2084 and BaCl\u2082 solutions in the test tubes.";
  tween1=this.game.add.tween(testtube_stand_group).to( {x:0, }, 1000, Phaser.Easing.Out, true);
  tween1.onComplete.add(function(){this.holdertweenIn()}, this);

},

holdertweenIn:function()
{
  tween1=this.game.add.tween(testtube_holder).to( {x:1300, }, 800, Phaser.Easing.Out, true);
  this.game.time.events.add(Phaser.Timer.SECOND*3,this.holdertweenInEnd, this); 
},

holdertweenInEnd:function()
{
  voice.destroy();
  voice=this.game.add.audio("A_step2",1);
  voice.play();
dialog.text="Hold the Na\u2082SO\u2084 test tube with the test tube holder.";
arrow.visible=true;
arrow.x=1480;
arrow.y=720;
arrow_y=720;
testtube_holder.events.onDragStart.add(function() {this.onDragStart(testtube_holder)}, this);
testtube_holder.events.onDragStop.add(function() {this.onDragStop(testtube_holder)}, this);
testtube_holder.inputEnabled = true;
testtube_holder.input.useHandCursor = true;
this.game.physics.arcade.enable(testtube_holder);
testtube_holder.input.enableDrag(true);

},

onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==(testtube_holder))
    {
      if(isNaSO4taken==false)
      {
        arrow.visible=false;
        currentobj.xp=1300;
        currentobj.yp=830;
        arrow.x=1015;
        arrow.y=520;
        arrow_y=520;
        arrow.visible=true;
        //collider_A.visible=true;
        collider_A.inputEnabled = true;
        collider_A.enableBody =true;

      }
      else if(isBaCltaken==false)
      {
        arrow.visible=false;
        currentobj.xp=1300;
        currentobj.yp=830;
        arrow.x=935;
        arrow.y=520;
        arrow_y=520;
        arrow.visible=true;
        //collider_B.visible=true;
        collider_B.inputEnabled = true;
        collider_B.enableBody =true;

      }
      else if(ispptTaken==false)
      {
        arrow.visible=false;
        currentobj.xp=1300;
        currentobj.yp=830;
        arrow.x=440;
        arrow.y=490;
        arrow_y=490;
        arrow.visible=true;
        //collider_D.visible=true;
        collider_D.inputEnabled = true;
        collider_D.enableBody =true;
        
      }
      else if(isbaso4taken==false)
      {
        arrow.visible=false;
        currentobj.xp=1300;
        currentobj.yp=830;
        arrow.x=440;
        arrow.y=490;
        arrow_y=490;
        arrow.visible=true;
        collider_D.inputEnabled = true;
        collider_D.enableBody =true;
        
      }
 
    }
    else if(currentobj==NaSO4_testtube_holder)
    {
      arrow.visible=false;
      currentobj.xp=840;
      currentobj.yp=396;
      arrow.x=440;
      arrow.y=490;
      arrow_y=490;
      arrow.visible=true;
      //collider_D.visible=true;
      collider_D.inputEnabled=true;
      collider_D.enableBody =true;
      testtube_back1.visible=false;

    }

    else if(currentobj==BaCl_testtube_holder)
    {
      arrow.visible=false;
      currentobj.xp=760;
      currentobj.yp=396;
      arrow.x=440;
      arrow.y=490;
      arrow_y=490;
      arrow.visible=true;
      collider_D.visible=true;
      collider_D.inputEnabled=true;
      collider_D.enableBody =true;
      testtube_back2.visible=false;

    }
    else if(currentobj==ppt_testtube_holder)
    {
      arrow.visible=false;
      currentobj.xp=259;
      currentobj.yp=355;
      arrow.x=860;
      arrow.y=520;
      arrow_y=520;
      arrow.visible=true;
      //collider_C.visible=true;
      collider_C.inputEnabled=true;
      collider_C.enableBody =true;
      testtube_back4.visible=false;

    }
    else if(currentobj==ppt_holder)
    {

      arrow.visible=false;
      currentobj.xp=815;
      currentobj.yp=150;
      arrow.x=440;
      arrow.y=490;
      arrow_y=490;
      arrow.visible=true;
      //collider_D.visible=true;
      collider_D.inputEnabled=true;
      collider_D.enableBody =true;
    }

    else if(currentobj==hcl_dropper)
    {

      arrow.visible=false;
      currentobj.xp=1710;
      currentobj.yp=720;
      arrow.x=440;
      arrow.y=490;
      arrow_y=490;
      arrow.visible=true;
      //_D.visible=true;
      collider_D.inputEnabled=true;
      collider_D.enableBody =true;
    }

  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==testtube_holder)
    {
      if(isNaSO4taken==false)
      {
        //collider_A.visible=false;
        collider_A.inputEnabled = false;
        collider_A.enableBody =false;
        arrow.visible=false;
        testtube_holder.visible=false;
        NaSO4_testtube_holder.visible=true;
        NaSO4_testtube.visible=false;
  
        NaSO4_testtube_holder.events.onDragStart.add(function() {this.onDragStart( NaSO4_testtube_holder)}, this);
        NaSO4_testtube_holder.events.onDragStop.add(function() {this.onDragStop( NaSO4_testtube_holder)}, this);
        NaSO4_testtube_holder.inputEnabled = true;
        NaSO4_testtube_holder.input.useHandCursor = true;
        this.game.physics.arcade.enable( NaSO4_testtube_holder);
        NaSO4_testtube_holder.input.enableDrag(true);
  
        voice.destroy();
        voice=this.game.add.audio("A_step3",1);
        voice.play();
        dialog.text="Drag the Na\u2082SO\u2084 test tube to the clamp stand using the test tube\nholder."
        arrow.x=1015;
        arrow.y=520;
        arrow_y=520;
        arrow.visible=true;
  
        //collider_A.visible=false;
        isNaSO4taken=true;
        

      }
      else if(isBaCltaken==false)
      {
        //collider_B.visible=false;
        collider_B.inputEnabled = false;
        collider_B.enableBody =false;
        arrow.visible=false;
        testtube_holder.visible=false;
        BaCl_testtube_holder.visible=true;
        BaCl_testtube.visible=false;
        BaCl_testtube_holder.events.onDragStart.add(function() {this.onDragStart(BaCl_testtube_holder)}, this);
        BaCl_testtube_holder.events.onDragStop.add(function() {this.onDragStop(BaCl_testtube_holder)}, this);
        BaCl_testtube_holder.inputEnabled = true;
        BaCl_testtube_holder.input.useHandCursor = true;
        this.game.physics.arcade.enable(BaCl_testtube_holder);
        BaCl_testtube_holder.input.enableDrag(true);
        voice.destroy();
        voice=this.game.add.audio("A_step5",1);
        voice.play();
        dialog.text="Now add BaCl\u2082 solution to the Na\u2082SO\u2084 solution and observe.";
        arrow.x=935;
        arrow.y=520;
        arrow_y=520;
        arrow.visible=true;
        //collider_B.visible=false;
        isBaCltaken=true;
       

      }
      else if(ispptTaken==false)
      {
        arrow.visible=false;
        NaSO4_testtube_pptset_sprite.visible=false;
        ppt_testtube_holder.visible=true;
        arrow.x=440;
        arrow.y=490;
        arrow_y=490;
        arrow.visible=true;
        testtube_holder.visible=false;
        ppt_testtube_holder.events.onDragStart.add(function() {this.onDragStart(ppt_testtube_holder)}, this);
        ppt_testtube_holder.events.onDragStop.add(function() {this.onDragStop(ppt_testtube_holder)}, this);
        ppt_testtube_holder.inputEnabled = true;
        ppt_testtube_holder.input.useHandCursor = true;
        this.game.physics.arcade.enable(ppt_testtube_holder);
        ppt_testtube_holder.input.enableDrag(true);
        voice.destroy();
      voice=this.game.add.audio("A_step10",1);
      voice.play();
        dialog.text="Now gently pour the upper portion of the solution to the empty\ntest tube."
        //collider_D.visible=false;
      collider_D.inputEnabled = false;
        collider_D.enableBody =false;
        ispptTaken=true;
      }

      else if(isbaso4taken==false)
      {
        collider_D.inputEnabled = false;
        collider_D.enableBody =false;
        arrow.visible=false;
        ppt_hcl_testtube_sprite.visible=false;
        testtube_holder.visible=false;
        testtube_back4.visible=false;
        baso4_testtube_holder.visible=true;
        tween1=this.game.add.tween(baso4_testtube_holder).to( {y:250}, 800, Phaser.Easing.Out, true);
        // tween1.onComplete.add(function(){this.baso4_testtube_holder_tweenup_end()}, this);
        tween1.onComplete.add(this.baso4_testtube_holder_tweenup_end, this);
        isbaso4taken=true;
        


      }
    
        
    }
    else if(currentobj==NaSO4_testtube_holder)
    {
      arrow.visible=false;
      NaSO4_testtube_holder.visible=false;
      NaSO4_testtube_on_stand_sprite.visible=true;
      testtube_back4.visible=true;

      testtube_holder.x=1300;
      testtube_holder.y=830;
      testtube_holder.visible=true;
      testtube_holder.events.onDragStart.add(function() {this.onDragStart(testtube_holder)}, this);
      testtube_holder.events.onDragStop.add(function() {this.onDragStop(testtube_holder)}, this);
      testtube_holder.inputEnabled = true;
      testtube_holder.input.useHandCursor = true;
      this.game.physics.arcade.enable(testtube_holder);
      testtube_holder.input.enableDrag(true);
      arrow.x=1480;
      arrow.y=720;
      arrow_y=720;
      arrow.visible=true;
      voice.destroy();
        voice=this.game.add.audio("A_step4",1);
        voice.play();
      dialog.text="Hold the BaCl\u2082 test tube with test tube holder.";
      //collider_D.visible=false;
      collider_D.inputEnabled = false;
        collider_D.enableBody =false;
        NaSO4_testtube_text.visible=false;
    }
    else if(currentobj==BaCl_testtube_holder)
    {
      arrow.visible=false;
      BaCl_testtube_holder.visible=false;
      NaSO4_testtube_on_stand_sprite.visible=true;
      NaSO4_testtube_on_stand_sprite.animations.play('anim1');
      testtube_back4.visible=true;
      // testtube_holder.x=1300;
      // testtube_holder.y=830;
      // testtube_holder.visible=true;

      BaCl_testtube_pour_sprite.visible=true;
      BaCl_testtube_pour_sprite.animations.play('anim1');
      BaCl_testtube_pour_sprite.animations.currentAnim.onComplete.add(this.BaCl_animation_end, this);
      collider_D.visible=false;
      collider_D.inputEnabled = false;
        collider_D.enableBody =false;
        BaCl_testtube_text.visible=false;

    }
    else if(currentobj==ppt_testtube_holder)
    {
      arrow.visible=false;
      ppt_testtube_holder.visible=false;
      NaSO4_testtube_solution_pour_sprite.visible=true;
      NaSO4_testtube_solution_pour_sprite.animations.play('anim1');
      empty_testtube1.visible=true;
      empty_testtube1.animations.play('anim1');
      //collider_C.visible=false;
      collider_C.inputEnabled = false;
      collider_C.enableBody =false;
      NaSO4_testtube_solution_pour_sprite.animations.currentAnim.onComplete.add(this.solution_pour_animation_end, this);
      //collider_C.visible=false;
      collider_C.inputEnabled = false;
        collider_C.enableBody =false;

    }

    else if(currentobj==ppt_holder)
    {
      arrow.visible=false;
      ppt_holder.visible=false;
      ppt_hcl_testtube_sprite.visible=true;
      testtube_back4.visible=true;
      testtube_holder.x=1300;
      testtube_holder.y=830;
      testtube_holder.visible=true;

      //collider_D.visible=false;
      collider_D.inputEnabled = false;
      collider_D.enableBody =false;
      tween1=this.game.add.tween(hcl_group).to( {x:0, }, 800, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.hcl_group_tween_end()}, this);
  
    }

    else if(currentobj==hcl_dropper)
    {
      //collider_D.visible=false;
      collider_D.inputEnabled = false;
      collider_D.enableBody =false;
      arrow.visible=false;
  hcl_dropper.x=407;
  hcl_dropper.y=400;
  hcl_dropper.animations.play('anim1');
  ppt_hcl_testtube_sprite.animations.play('anim1');
  hcl_dropper.animations.currentAnim.onComplete.add(this.dropper_animation_end, this);

      
     

  
    }
  },


  baso4_testtube_holder_tweenup_end:function()
  {
    baso4_testtube_holder.animations.play('anim1');
    baso4_testtube_holder.animations.currentAnim.onComplete.add(this.baso4_testtube_holder_tweendown, this);
  },

  baso4_testtube_holder_tweendown:function()
  {
    tween1=this.game.add.tween(baso4_testtube_holder).to( {y:521}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.baso4_testtube_holder_tweendown_end()}, this);
  },
  baso4_testtube_holder_tweendown_end:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_step15",1);
      voice.play();
    baso4_testtube_holder.visible=false;
    ppt_hcl_testtube_sprite.visible=true;
    dialog.text="Observe that the precipitate is insoluble in dil. HCl.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.endOfExperiment, this);


  },
  dropper_animation_end:function()
  {
    tween1=this.game.add.tween(hcl_dropper).to( {x:1710,y:720}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.hcl_group_tween_out()}, this);

  },

  hcl_group_tween_out:function()
  {
    tween1=this.game.add.tween(hcl_group).to( {x:2500}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.hcl_group_tween_out_end()}, this);
  

  },
  hcl_group_tween_out_end:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_step18",1);
      voice.play();
    arrow.visible=false;
    arrow.x=1480;
    arrow.y=720;
    arrow_y=720;
    arrow.visible=true;
    testtube_holder.events.onDragStart.add(function() {this.onDragStart(testtube_holder)}, this);
    testtube_holder.events.onDragStop.add(function() {this.onDragStop(testtube_holder)}, this);
    testtube_holder.inputEnabled = true;
    testtube_holder.input.useHandCursor = true;
    this.game.physics.arcade.enable(testtube_holder);
    testtube_holder.input.enableDrag(true);

    dialog.text="Now hold the test tube with the test tube holder and shake it, \nthen observe. ";
      //this.game.time.events.add(Phaser.Timer.SECOND*5,this.endOfExperiment, this);

  },



  endOfExperiment:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_step16",1);
      voice.play();
      dialog_box.scale.setTo(.80,.9);
    dialog.y=30;
dialog.text="In this reaction, the sulphate ions are displaced by chloride ions and \nchloride ions are displaced by sulphate ions. So the reaction is double \ndisplacement reaction.";
this.game.time.events.add(Phaser.Timer.SECOND*12,this.goNextScene, this);

  },
  goNextScene:function(){
    voice.destroy();
      voice=this.game.add.audio("A_step17",1);
      voice.play();
      dialog_box.scale.setTo(.80,.8);
    dialog.y=60;
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
  hcl_group_tween_end:function()
  {
    
    clock.visible=false;
  clock_short_needle.visible=false;
  clock_long_needle.visible=false;
    voice.destroy();
        voice=this.game.add.audio("A_step13",1);
        voice.play();
        dialog.y=60;
    dialog.text="Take the dropper.";
    arrow.x=1500;
    arrow.y=815;
    arrow_y=815;
    arrow.visible=true;
    hcl_dropper.inputEnabled=true;
    hcl_dropper.input.useHandCursor=true;
    hcl_dropper.events.onInputDown.add(this.clickOnDropper, this);
  },
  clickOnDropper:function()
  {
    if(ishacltaken==false)
    {
      hcl_dropper.inputEnabled=false;
      hcl_dropper.input.useHandCursor=false;
      hcl_dropper.angle=0;
      tween1=this.game.add.tween(hcl_dropper).to( {x:1710, y:520}, 300, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.dropperTween2()}, this);
      ishacltaken=true;
      arrow.visible=false;
      cork.x=1600;
      cork.y=900;
    }
   
   
  },
  dropperTween2:function()
  {
    tween1=this.game.add.tween(hcl_dropper).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.dropperTween2_end()}, this);

  },
  dropperTween2_end:function()
  {
    voice.destroy();
        voice=this.game.add.audio("A_step14",1);
        voice.play();
    dialog.text="Add few drops of dil. HCl to the test tube.";
    arrow.x=1740;
    arrow.y=620;
    arrow_y=620;
    arrow.visible=true;
    hcl_dropper.events.onDragStart.add(function() {this.onDragStart(hcl_dropper)}, this);
    hcl_dropper.events.onDragStop.add(function() {this.onDragStop(hcl_dropper)}, this);
    hcl_dropper.inputEnabled = true;
    hcl_dropper.input.useHandCursor = true;
    this.game.physics.arcade.enable(hcl_dropper);
    hcl_dropper.input.enableDrag(true);
  },

  solution_pour_animation_end:function()
  {
    NaSO4_testtube_solution_pour_sprite.visible=false;
    ppt_holder.visible=true;
    voice.destroy();
        voice=this.game.add.audio("A_step11",1);
        voice.play();
    dialog.text="Well done, you have successfully seperated the solution from the\ntest tube.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.pptBacktoClamp, this);
  },

  pptBacktoClamp:function()
  {
    ppt_holder.events.onDragStart.add(function() {this.onDragStart( ppt_holder)}, this);
    ppt_holder.events.onDragStop.add(function() {this.onDragStop( ppt_holder)}, this);
    ppt_holder.inputEnabled = true;
    ppt_holder.input.useHandCursor = true;
    this.game.physics.arcade.enable( ppt_holder);
    ppt_holder.input.enableDrag(true);
    arrow.x=1015;
    arrow.y=250;
    arrow_y=250;
    arrow.visible=true;
    voice.destroy();
        voice=this.game.add.audio("A_step12",1);
        voice.play();
    dialog.text="Now take the test tube back to the clamp stand.";
  },

  BaCl_animation_end:function()
  {
    BaCl_testtube_pour_sprite.visible=false;
      testtube_holder.x=1300;
      testtube_holder.y=830;
      testtube_holder.visible=true;
      voice.destroy();
      voice=this.game.add.audio("A_step6",1);
      voice.play();
      
      dialog.text="Observe that a white precipitate is formed in the test tube.";
      //this.game.time.events.add(Phaser.Timer.SECOND*5,this.waitForanHour, this);
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.goNextScene, this);
  },

  waitForanHour:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_step7",1);
      voice.play();
    dialog.text="Come on, lets observe the solution for an hour and see what happens?";
    clock.visible=true;
    clock_short_needle.visible=true;
    clock_long_needle.visible=true;
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.timeRunningStart, this);
  },
  timeRunningStart:function()
  {
    isClockRunning=true;
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.playnextSound, this);
    NaSO4_testtube_on_stand_sprite.visible=false;
    NaSO4_testtube_pptset_sprite.visible=true;
    NaSO4_testtube_pptset_sprite.animations.play('anim1');
    NaSO4_testtube_pptset_sprite.animations.currentAnim.onComplete.add(this.pptSettleAnimationEnd, this);
  },
  playnextSound:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_step8",1);
      voice.play();
      dialog.y=40;
    dialog.text="See that the white precipitate settle down in the bottom of the \ntest tube.";
    //this.game.time.events.add(Phaser.Timer.SECOND*5,this.experimentStage2, this);
  },
  pptSettleAnimationEnd:function()
  {
    // voice.destroy();
    //   voice=this.game.add.audio("A_step8",1);
    //   voice.play();
    // dialog.text="See that the white precipitate settle down in the bottom of the test tube.";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.experimentStage2, this);
  },
  experimentStage2:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_step9",1);
      voice.play();
      dialog.y=40;
    dialog.text="Come on lets see what happens if added dil. HCl to the test tube.\nFor that hold the test tube with test tube holder.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.nextAction, this);
    /**/

  },
  nextAction: function(){
    arrow.x=1480;
    arrow.y=740;
    arrow_y=740;
    arrow.visible=true;
      testtube_holder.events.onDragStart.add(function() {this.onDragStart(testtube_holder)}, this);
      testtube_holder.events.onDragStop.add(function() {this.onDragStop(testtube_holder)}, this);
      testtube_holder.inputEnabled = true;
      testtube_holder.input.useHandCursor = true;
      this.game.physics.arcade.enable(testtube_holder);
      testtube_holder.input.enableDrag(true);
  },
  
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }

    if(isClockRunning==true)
    {
      clock_long_needle.rotation+=DeltaTime*0.5;
      clock_short_needle.rotation+=DeltaTime*0.04;
     // clock_short_needle.rotation+=.025+DeltaTime;
      console.log("angle="+clock_long_needle.rotation);
      console.log("angle="+clock_long_needle.angle);
      if(clock_long_needle.rotation>=6.28)
      {
        isClockRunning=false;
        
      }
    }
    
   
  },
 
  detectCollision:function(){
   

     
    if(collider_A.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_A,function() {this.match_Obj()},null,this);
        collider=collider_A;
    }
    if(collider_B.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
        collider=collider_B;
    }
    if(collider_C.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_C,function() {this.match_Obj()},null,this);
        collider=collider_C;
    }
    if(collider_D.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_D,function() {this.match_Obj()},null,this);
        collider=collider_D;
    }
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
        if(currentobj==testtube_holder)
        {
          arrow.visible=false;
          arrow.x=1480;
          arrow.y=720;
          arrow_y=720;
          arrow.visible=true;
        }
        else if(currentobj==NaSO4_testtube_holder)
        {
          arrow.visible=false;
          arrow.x=1015;
          arrow.y=520;
          arrow_y=520;
          arrow.visible=true;
        }
        else if(currentobj==NaSO4_testtube_holder)
        {
          arrow.visible=false;
          arrow.x=1015;
          arrow.y=520;
          arrow_y=520;
          arrow.visible=true;
        }

        else if(currentobj==BaCl_testtube_holder)
        {
          arrow.visible=false;
          arrow.x=935;
          arrow.y=520;
          arrow_y=520;
          arrow.visible=true;
        }

        else if(currentobj==ppt_testtube_holder)
        {
          arrow.visible=false;
          arrow.x=440;
          arrow.y=490;
          arrow_y=490;
          arrow.visible=true;
        }

        else if(currentobj==ppt_holder)
        {
          arrow.visible=false;
          arrow.x=1015;
          arrow.y=250;
          arrow_y=250;
          arrow.visible=true;
        }

        else if(currentobj==hcl_dropper)
        {
          arrow.visible=false;
          arrow.x=1740;
          arrow.y=620;
          arrow_y=620;
          arrow.visible=true;
        }
      
       
    //     arrow.x=1500;
    //     arrow.y=450;
    //     arrow_y=450;
    //     arrow.visible=true;
        
    //   }

         currentobj=null;
      }
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        window.location.href="../index.php";
        //this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
