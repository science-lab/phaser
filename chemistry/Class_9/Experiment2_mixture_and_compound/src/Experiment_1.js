var experiment_1 = function(game){

var inst;


var Fog;
var delay;
var incr;
var currentobj;
var arrow_y;
var mix_count;
var spoon3;
var bowl;
var interval;
var heatFlag;
var heatFlag2;
var magnetFlag;
var glass_rod1;
var content1;
var magnet1;
var magnet2;
var spoon4;
var spoon5;
var shakeFlag1;
var shakeFlag2;
var shakingFlag1;
var shakingFlag2;
var shakingCount1;
var shakingCount2;
var mixFlag;
var stickFlag;
var stick;
var texttube3;
var texttube4;
var grindFlag;
var grineMove;
}


experiment_1.prototype = {
  	create: function(){
      fontstyle_1={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_2={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_3={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_4={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_5={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_6={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_7={ font: "40px Segoe UI", fill: "#FFA500", align: "left" };
hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');
grindFlag=false;
grinderMove=false;
Fog=null;
interval=0;
heatFlag=false;
heatFlag2=false;
heatFlag3=false;
magnetFlag=false;
currentobj=null;
glass_rod1=null;
magnet1=null;
magnet2=null;
spoon4=null;
spoon5=null;
shakeFlag1=false;
shakeFlag2=false;
shakingFlag1=false;
shakingFlag2=false;
shakingCount1=0;
shakingCount2=0;
mixFlag=false;
hclFlag=false;
compHeatFlag=false;
mixHeatFlag=false;
stickFlag=false;
stick=null;
texttube3=null;
texttube4=null;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/


spoon3=null;
bowl=null;
mix_count=0;

voice=this.game.add.audio("step0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Procedure_2", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
      
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
    dia_box.scale.setTo(.8,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(60,50,dia_text,fontstyle_2);
    collider=this.game.add.sprite(840,770, 'collider');//y-320
    dialog.y=60;
    this.game.physics.arcade.enable(collider);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.0;

    burner=this.game.add.sprite(0,575, 'compounds', 'burner0001.png');
    burner.animations.add('anim',['burner0001.png',
            'burner0002.png',
             ], 10, true, false);
    burner.scale.set(1.25);

    burner_Switch=this.game.add.sprite(130,820, 'collider');
    burner_Switch.alpha=0;
    burner_Switch.state="switch_deactivate";
    burner_Switch.inputEnabled = true;
    burner_Switch.input.useHandCursor = true;
    burner_Switch.events.onInputDown.add(this.burnerOperate, this);

    plate=this.game.add.sprite(800,720, 'compounds', 'plate.png');//y-320
    plate1=this.game.add.sprite(-250,720, 'compounds', 'plate.png');//y-320
    mixture=this.game.add.sprite(790,840, 'compounds', 'mixture.png');//y-440
    mixture.visible=false;
    mix=this.game.add.sprite(780,570, 'mixing', 'mix0001.png');//y-170
    mix.scale.setTo(.61,.61);
    mix.visible=false;
    mix.animations.add('anim',[
            'mix0002.png',
            'mix0003.png',
            'mix0004.png',
            'mix0005.png',
            'mix0006.png',
            'mix0007.png',
            'mix0008.png',
            
             ], 10, true, false);

    ironFillings=this.game.add.sprite(1320,850, 'compounds', 'iron.png');//y-450, x-1370
    sulphur=this.game.add.sprite(1070,850, 'compounds', 'sulphur.png');//-450, x-1120

    spoon1_1=this.game.add.sprite(1470,870, 'Spoon_M');//y-470
    spoon1_1.angle=-20;   
    spoon1_1.anchor.setTo(.5,.5);        
    spoon1_1.scale.setTo(.5,.8);
    spoon2_1=this.game.add.sprite(1220,870, 'Spoon_M');//y-470
    spoon2_1.angle=-20;   
    spoon2_1.anchor.setTo(.5,.5);        
    spoon2_1.scale.setTo(.5,.8);         
    spoon3_1=this.game.add.sprite(930,870, 'Spoon_M');//y-470
    spoon3_1.angle=-20;   
    spoon3_1.anchor.setTo(.5,.5);        
    spoon3_1.scale.setTo(.5,.8);        
    spoon3_1.visible=false;

    spoon2=this.game.add.sprite(1220,870, 'spoon', 'spoon_compound.png');//y-470, x-1270
    spoon2.angle=-20;
    spoon2.xp=1220;//1270
    spoon2.yp=850;//450
    spoon2.scale.setTo(.5,.8);
    spoon2.anchor.setTo(.5,.5);
    spoon2.nam="spoon";
    spoon2.events.onDragStart.add(function() {this.gameC(spoon2)}, this);
    spoon2.events.onDragStop.add(function() {this.gameC1(spoon2)}, this);
    spoon2.inputEnabled = true;
    spoon2.input.useHandCursor = true;
    spoon2.events.onInputDown.add(this.ClickOnSpoon, this);
    spoon2.alpha=0;
    
    spoon1=this.game.add.sprite(1470,870, 'spoon', 'spoon_compound.png');//y-470, x-1470
    spoon1.angle=-20;
    spoon1.scale.setTo(.5,.8);
    spoon1.xp=1470;//1470
    spoon1.yp=850;//450
    spoon1.nam="spoon";
    spoon1.anchor.setTo(.5,.5);
    this.game.physics.arcade.enable(spoon1);
    spoon1.enableBody =true;
    spoon1.alpha=0;
    
    
    

    Grander_back=this.game.add.sprite(3350,750, 'Grander_back');
    Grander_back.scale.set(.5);       
    Bowl_to_grander=this.game.add.sprite(1170,550,'Bowl_to_grander', 'Bowl_to_grander0001.png');//500
        Bowl_to_grander.animations.add('anim',[
          'Bowl_to_grander0002.png',
          'Bowl_to_grander0003.png',
          'Bowl_to_grander0004.png',
          'Bowl_to_grander0005.png',
          'Bowl_to_grander0006.png',
          'Bowl_to_grander0007.png',
          'Bowl_to_grander0008.png',
          'Bowl_to_grander00091.png',
          'Bowl_to_grander00010.png',
          'Bowl_to_grander00011.png',
           ], 10, false, false);  
           Bowl_to_grander.visible=false;     
    Grander_animation=this.game.add.sprite(3350,750, 'Grander_animation', 'Grander_animation0001.png');
    Grander_animation.scale.set(.5);  
    Grander_animation.animations.add('anim',[
          'Grander_animation0002.png',
          'Grander_animation0003.png',
          'Grander_animationr0004.png',
          'Grander_animationr0005.png',
          'Grander_animation0006.png',
          'Grander_animation0007.png',
          'Grander_animation0008.png',
          'Grander_animation00091.png',
          'Grander_animation00010.png',
          'Grander_animation00011.png',
           ], 10, true, false);      
           Grander_Front=this.game.add.sprite(3350,750, 'Grander_Front');
           Grander_Front.xp=1200;
           Grander_Front.yp=750;  
           Grander_Front.scale.set(.5);     
    glass_rod=this.game.add.sprite(1300,1000, 'compounds', 'glass_rod.png');
    glass_rod.anchor.setTo(.5,.5);
    glass_rod.scale.set(1.3);
    glass_rod.xp=1300;
    glass_rod.yp=1000;
    glass_rod.angle=90;
    arrow=this.game.add.sprite(1200,750, 'arrow');//y-350
    arrow.angle=90;
    arrow_y=750;//y-350
    arrow.visible=false;
    //mixture_label.scale.setTo(1.5,1.5);
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_1",1);
    voice.play();
    dialog.text="Add 4 g of Sulphur into the watch-glass.";
    spoon2.input.enableDrag(true);
    this.game.physics.arcade.enable(spoon2);
    spoon2.enableBody =true;
    
    arrow.visible=true;

  },
  burnerOperate:function(){
    if(burner_Switch.state=="switch_off"){
      burner_Switch.state="switch_deactivate";
      burner.frameName = "burner0002.png";
        heatFlag=true;
        Fog.visible=true;
        Fog.animations.play('anim',true);
        arrow.visible=false;
    }else if(burner_Switch.state=="switch_on"){

      burner.frameName = "burner0001.png";
      burner_Switch.state="switch_deactivate2";
      arrow.x=140;
      arrow.y=485;
      arrow_y=485;
      arrow.visible=true;
      console.log(bowl.x+"//"+bowl.y);
      bowl.input.enableDrag(true);
      collider.reset(1200,750);//y-350
      collider.scale.setTo(3,3);
      voice.destroy(true);
          voice=this.game.add.audio("step1_16",1);
          voice.play();
          //dialog.text="Pour the compound into the empty plate";
          dialog.text="Pour the black mass into the mortar.";//
          
    }
    
  },
  ClickOnSpoon:function(){

  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    arrow.visible=false;
    if(obj==spoon1){
      obj.frame=2;
      obj.alpha=1;
      spoon1_1.visible=false;
      arrow.x=920;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
    }else if(obj==spoon2){
      obj.frame=4;
      obj.alpha=1;
      spoon2_1.visible=false;
      arrow.x=920;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
    }else if(obj==glass_rod){
      obj.angle=0;
      arrow.x=920;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
    }else if(obj==spoon3){
      obj.frame=3;
      obj.alpha=1;
      spoon3_1.visible=false;
      arrow.x=1300;
      arrow.y=720;//320
      arrow_y=720;
      arrow.visible=true;
    }else if(obj==bowl && bowl.frameName != "bowl0022.png"){
      arrow.x=170;
      arrow.y=525;
      arrow_y=525;
      arrow.visible=true;
    }else if(obj==bowl && bowl.frameName == "bowl0022.png"){
      //burner.frameName = "burner0001.png";
      arrow.x=1270;
      arrow.y=750;//350
      arrow_y=750;
      arrow.visible=true;
    }else if(obj==glass_rod1){
      obj.angle=0;
      arrow.x=150;
      arrow.y=480;//480
      arrow_y=480;
      arrow.visible=true;
    }else if(Grander_Front){
      arrow.x=630;
      arrow.y=750;//350
      arrow_y=750;
      grinderMove=true;
      arrow.visible=true;
      Grander_animation.visible=false;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
    
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==glass_rod){
      glass_rod.visible=false;
      mix.play("anim",true);
      mixFlag=true;
      arrow.visible=false;
    }else if(currentobj==glass_rod1){
      console.log("pppppppppppppppppppppp");
      glass_rod1.visible=false;
      mix_count=0;
      heatFlag2=true;
      Fog.visible=true;
      bowl.play("anims",true);
      arrow.visible=false;
    }else if(currentobj.nam=="spoon"){
      currentobj.angle=-20;
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //currentobj.frame=0;
      //currentobj.scale.setTo(1,1);
    }
   
    if(currentobj==spoon2){
      spoon2.visible=false;
      spoon2_1.visible=true;
      mix.visible=true;
      spoon1.events.onDragStart.add(function() {this.gameC(spoon1)}, this);
      spoon1.events.onDragStop.add(function() {this.gameC1(spoon1)}, this);
      spoon1.inputEnabled = true;
      spoon1.input.useHandCursor = true;
      spoon1.events.onInputDown.add(this.ClickOnSpoon, this);
      spoon1.input.enableDrag(true);
      voice.destroy(true);
      voice=this.game.add.audio("step1_2",1);
      voice.play();
      dialog.text="Add 7 g of Iron Filings.";
      arrow.x=1450;
      arrow.visible=true;
    }else if(currentobj==spoon1){
      spoon1.visible=false;
      spoon1_1.visible=true;
      mix.frame=1;
      glass_rod.events.onDragStart.add(function() {this.gameC(glass_rod)}, this);
      glass_rod.events.onDragStop.add(function() {this.gameC1(glass_rod)}, this);
      glass_rod.inputEnabled = true;
      glass_rod.input.useHandCursor = true;
      glass_rod.events.onInputDown.add(this.ClickOnSpoon, this);
      glass_rod.input.enableDrag(true);
      this.game.physics.arcade.enable(glass_rod);
      glass_rod.enableBody=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_3",1);
      voice.play();
      dialog.text="Mix well with a glass rod.";
      arrow.x=1320;//1520
      arrow.y=880;//570
      arrow_y=880;
      arrow.visible=true;

    }else if(currentobj==spoon3){
      spoon3.visible=false;
      spoon3_1.visible=true;
      bowl.frame=2;
      collider.x=130;
      collider.y=500;
      collider.scale.setTo(2,3);
      bowl.events.onDragStart.add(function() {this.gameC(bowl)}, this);
      bowl.events.onDragStop.add(function() {this.gameC1(bowl)}, this);
      bowl.inputEnabled = true;
      bowl.input.useHandCursor = true;
      bowl.events.onInputDown.add(this.ClickOnSpoon, this);
      bowl.input.enableDrag(true);
      this.game.physics.arcade.enable(bowl);
      voice.destroy(true);
      voice=this.game.add.audio("step1_7",1);
      voice.play();
      dialog.text="Heat the mixture.";
      arrow.x=1300;
      arrow.y=725;//325
      arrow_y=725;
      arrow.visible=true;
    }else if(currentobj==bowl){
      if(bowl.frameName == "bowl0022.png"){
        //plate1.frameName="black_mass.png";
        //plate1.y=850;//450
        
        //bowl.frame=0;
        bowl.visible=false;
        spoon3.visible=false;
        Fog.visible=false;
        Bowl_to_grander.visible=true;
           Bowl_to_grander.animations.play('anim',false);
           Bowl_to_grander.animations.currentAnim.onComplete.add(function () {
             Bowl_to_grander.visible=false;
            Grander_animation.animations.play('anim',false);
            voice.destroy(true);
            voice=this.game.add.audio("step1_10",1);
            voice.play();
            dialog.text="Grind the black mass formed into a powder.";
            grindFlag=true;
          }.bind(this));
            
        //this.game.add.tween(bowl).to( {x:1300, y:950}, 600, Phaser.Easing.Out, true);
        //compound_lable=this.game.add.sprite(-200,910,'compounds', 'compound_lable.png');//500
        //tween5=this.game.add.tween(compound_lable).to( {x:530}, 600, Phaser.Easing.Out, true);
        //tween5.onComplete.add(function(){
          ////////////popup = this.game.add.sprite(420,550,'popup');
          //popup.visible=false;
          //////////////popup.scale.setTo(.4,.3);
          ////////////////voice2.play();
          ////////////////voice2.onStop.add(this.nextSound3,this);
          //dialog.text="You have successfully made a mixture and a compound using sulphur \nand iron fillings. Now Check the magnetism in the materials."
          
          //content1=this.game.add.text(470,700,dia_text,fontstyle_2);
          //play.visible=true;
        //}.bind(this));
        arrow.visible=false;
      }else{
        arrow.x=burner_Switch.x+40;
        arrow.y=burner_Switch.y-80;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_15",1);
        voice.play();
        dialog.text="Switch on the burner.";
        burner_Switch.state="switch_off";
      }
      bowl.x=170;
      bowl.y=710;
      bowl.xp=170;
      bowl.yp=710;
      
      
    }else if(currentobj==Grander_Front){
      Grander_Front.visible=false;
      Grander_back.visible=false;
      arrow.visible=false;
      Grander_to_Plate=this.game.add.sprite(600,550, 'Grander_to_Plate', 'Grander_to_Plate0001.png');
      Grander_to_Plate.scale.set(.5);  
      Grander_to_Plate.animations.add('anim',[
            'Grander_to_Plate0002.png',
            'Grander_to_Plate0003.png',
            'Grander_to_Plate0004.png',
            'Grander_to_Plate0005.png',
            'Grander_to_Plate0006.png',
            'Grander_to_Plate0007.png',
            'Grander_to_Plate0008.png',
            'Grander_to_Platen00091.png',
            'Grander_to_Plate00010.png',
            
            ], 10, false, false);  
      Grander_to_Plate.animations.play('anim',false);
      //Grander_to_Plate.animations.currentAnim.onComplete.add(function () {
        Plate=this.game.add.sprite(455,650, 'Plate', 'To_plate0001.png');
        //Plate.scale.set(.5);  
        Plate.animations.add('anim',[
              'To_plate0002.png',
              'To_plate0003.png',
              'To_plate0004.png',
              'To_plate0005.png',
              'To_plate0006.png',
              'To_plate0007.png',
              'To_plate0008.png',
              'To_plate0009.png',
              'To_plate00010.png',
              
              ], 10, false, false);  
              Plate.animations.play('anim',false);
              Plate.animations.currentAnim.onComplete.add(function () {   
                Plate.visible=false;
                plate1.frameName="black_mass.png";
                plate1.y=850;//450
                Grander_to_Plate.visible=false;
                voice.destroy(true);
                voice=this.game.add.audio("step_12",1);
                voice.play();
                dialog.text="Label it compound.";
                compound_lable=this.game.add.sprite(-200,910,'compounds', 'compound_lable.png');//500
                tween5=this.game.add.tween(compound_lable).to( {x:530}, 600, Phaser.Easing.Out, true);
                tween5.onComplete.add(function(){
                  voice.destroy(true);
                  voice=this.game.add.audio("step1_13",1);
                  voice.play();
                  dialog.text="Observe the appearance of mixture and compound with naked eyes.";
                  this.game.time.events.add(Phaser.Timer.SECOND*4,this.NextExperiment, this);
                  ////////////popup = this.game.add.sprite(420,550,'popup');
                  //popup.visible=false;
                  //////////////popup.scale.setTo(.4,.3);
                  ////////////////voice2.play();
                  ////////////////voice2.onStop.add(this.nextSound3,this);
                  //dialog.text="You have successfully made a mixture and a compound using sulphur \nand iron fillings. Now Check the magnetism in the materials."
                  
                  //content1=this.game.add.text(470,700,dia_text,fontstyle_2);
                  play.visible=true;
                }.bind(this));
               }.bind(this));  
     //}.bind(this));      
            
    }
    
    
  },
  NextExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_14",1);
    voice.play();
    dialog.text="Click on the next button to see the behaviour towards a magnet.";
    play.visible=true;
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	addList:function(){
    

  },
  ClickOnPlay:function(){

    
  },
  
  ClickOnSlow2:function(){

  },
  


  hotsec:function()
  {

    //voice41.destroy();
    //voice5.play();

  },
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(grindFlag){
      incr++;
      if(incr>200){
        grindFlag=false;
        
        Grander_animation.animations.stop('anim');
        tween4=this.game.add.tween(plate1).to( {x:500}, 600, Phaser.Easing.Out, true);
        tween4.onComplete.add(function(){
          arrow.visible=true;
          Grander_Front.events.onDragStart.add(function() {this.gameC(Grander_Front)}, this);
          Grander_Front.events.onDragStop.add(function() {this.gameC1(Grander_Front)}, this);
          Grander_Front.inputEnabled = true;
          Grander_Front.input.useHandCursor = true;
          Grander_Front.xp=1200;
          Grander_Front.yp=750;
          //Grander_Front.events.onInputDown.add(this.ClickOnSpoon, this);
          Grander_Front.input.enableDrag(true);
          this.game.physics.arcade.enable(Grander_Front);
           
          collider.x=500;
          voice.destroy(true);
          voice=this.game.add.audio("step1_11",1);
          voice.play();
          dialog.text="Now, we got compound and pour the compound into the watch-glass.";
        }.bind(this)); 
        
      }
    }
    if(grinderMove){
      Grander_back.x=Grander_Front.x;
      Grander_back.y=Grander_Front.y;
    }
    if(Fog!=null && Fog.visible){
      //bowl.x=170;
     // bowl.y=710;
      Fog.x=bowl.x-70;
      Fog.y=bowl.y-360;
    }
    
    
    if(mixFlag){
      mix_count++;
      if(mix_count>=60){
        mixFlag=false;
        mix.animations.stop(null, true);
        mix.visible=false;
        currentobj.reset(1300,1000);
        currentobj.angle=90;
        plate.visible=false;
        mixture.visible=true;
        glass_rod.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_4",1);
        voice.play();
        dialog.text="Now, we got a Mixture, lable it 'Mixture'.";
        this.game.add.tween(ironFillings).to( {x:2500}, 600, Phaser.Easing.Out, true);
        tween1=this.game.add.tween(spoon1_1).to( {x:2500}, 600, Phaser.Easing.Out, true);
        tween1=this.game.add.tween(spoon1).to( {x:2500}, 600, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          mixFlag=false;
          this.game.add.tween(sulphur).to( {x:2500}, 600, Phaser.Easing.Out, true);
          tween2=this.game.add.tween(spoon2_1).to( {x:2500}, 600, Phaser.Easing.Out, true);
          tween2=this.game.add.tween(spoon2).to( {x:2500}, 600, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            // voice.destroy(true);
            // voice=this.game.add.audio("step1_5",1);
            // voice.play();
            // dialog.text="Label it 'Mixture'";
            mixture_label=this.game.add.sprite(-55,910, 'compounds', 'mixture_label.png');//y-510
            tween3=this.game.add.tween(mixture_label).to( {x:825}, 800, Phaser.Easing.Out, true);
            tween3.onComplete.add(function(){
              collider.x=1230;
              collider.y=700;//300
              collider.scale.setTo(2,3);
              
              //collider.alpha=0;
              bowl=this.game.add.sprite(1290,950, 'heating', 'bowl0001.png');//y-550
              bowl.anchor.setTo(.5,1);  
              bowl.xp=1290;
              bowl.yp=950;//55
              bowl.animations.add('anims',['bowl0005.png',
                      'bowl0006.png',
                      'bowl0007.png',
                      'bowl0008.png',
                      'bowl0009.png',
                      'bowl0010.png',
                      'bowl0011.png',
                      'bowl0012.png',
                      'bowl0013.png',
                      'bowl0014.png',
                      
                      
                      ], 20, true, false);
            bowl.animations.add('anims2',['bowl0015.png',
                'bowl0016.png',
                'bowl0017.png',
                'bowl0018.png',
                'bowl0019.png',
                'bowl0020.png',
                'bowl0021.png',
                'bowl0022.png',
                
                ], 20, true, false);

                      Fog=this.game.add.sprite(100,350, 'Fog', 'Fog0001.png');//y-170
                      Fog.scale.setTo(1,.6);
                      Fog.visible=false;
                      Fog.animations.add('anim',['Fog0001.png','Fog0002.png','Fog0003.png','Fog0004.png','Fog0005.png','Fog0006.png','Fog0007.png','Fog0008.png','Fog0009.png','Fog0010.png','Fog0011.png','Fog0012.png','Fog0013.png','Fog0014.png','Fog0015.png','Fog0016.png','Fog0017.png','Fog0018.png','Fog0019.png','Fog0020.png'], 5, true, false);
              
              this.game.time.events.add(Phaser.Timer.SECOND*2,this.addMixture, this);                 
              
            }.bind(this));    
          }.bind(this));  
        }.bind(this));
        
      }
    }
    
    if(heatFlag2){// && bowl.frame==21
      mix_count++;
      if(mix_count>=160){
        //Fog.visible=true;
        heatFlag2=false;
        bowl.animations.stop('anims');
        bowl.animations.play('anims2');
        currentobj.reset(1300,1000);
        currentobj.angle=90;
        glass_rod1.visible=true;
        heatFlag3=true;
        mix_count=0;
        
      }
      
    }
    if(heatFlag3){// && bowl.frame==21
      mix_count++;
      if(mix_count>=160){
        //Fog.visible=true;
        heatFlag3=false;
        bowl.animations.stop('anims2');
         bowl.frameName = "bowl0022.png";
        //dialog.text="Now we got a Compound";
        tween4=this.game.add.tween(Grander_back).to( {x:1200}, 1000, Phaser.Easing.Out, true);
        tween4=this.game.add.tween(Grander_animation).to( {x:1200}, 1000, Phaser.Easing.Out, true);
        tween4.onComplete.add(function(){
          Grander_Front.x=1200;
          bowl.inputEnabled=true;
            bowl.input.useHandCursor = true;
            
            bowl.body.enable=true;
            bowl.x=170;
            bowl.y=710;
          voice.destroy(true);
          voice=this.game.add.audio("step1_9",1);
          voice.play();
          //dialog.text="Pour the compound into the empty plate";
          dialog.text="Now, we got a black mass. Switch off the burner.";//Pour the black mass into the mortar
          burner_Switch.state="switch_on";
          arrow.x=burner_Switch.x+40;
          arrow.y=burner_Switch.y-80;
          arrow_y=arrow.y;
          arrow.visible=true;
        }.bind(this));
        
      }
      
    }
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    /////////////////////////////
    if(heatFlag){
      interval++;
      if(interval>=60){
        interval=0;
        bowl.frame++;
        if(bowl.frame==4){
          heatFlag=false;
          voice.destroy(true);
          voice=this.game.add.audio("step1_8",1);
          voice.play();
          dialog.text="Stir well with the glass rod.";
          arrow.x=1320;//1520
          arrow.y=880;//570
          arrow_y=880;
          arrow.visible=true;
          glass_rod.visible=false;
          glass_rod1=this.game.add.sprite(1300,1000, 'compounds', 'glass_rod.png');
          glass_rod1.anchor.setTo(.5,.5);
          glass_rod1.scale.set(1.3);
          glass_rod1.xp=1300;
          glass_rod1.yp=1000;
          glass_rod1.angle=90;
          glass_rod1.events.onDragStart.add(function() {this.gameC(glass_rod1)}, this);
          glass_rod1.events.onDragStop.add(function() {this.gameC1(glass_rod1)}, this);
          glass_rod1.inputEnabled = true;
          glass_rod1.input.useHandCursor = true;
          glass_rod1.events.onInputDown.add(this.ClickOnSpoon, this);
          glass_rod1.input.enableDrag(true);
          this.game.physics.arcade.enable(glass_rod1);
        }
      }
    }
    
    
    
	},
  addMixture:function(){
    spoon3_1.visible=true;
    spoon3=this.game.add.sprite(930,870, 'spoon', 'spoon_compound.png');//y-470
    console.log("/////////");
    spoon3.angle=-20;
    spoon3.xp=930;
    spoon3.yp=870;//470
    spoon3.scale.setTo(.5,.8);
    spoon3.alpha=0;
    spoon3.nam="spoon";
    spoon3.events.onDragStart.add(function() {this.gameC(spoon3)}, this);
    spoon3.events.onDragStop.add(function() {this.gameC1(spoon3)}, this);
    spoon3.inputEnabled = true;
    spoon3.input.useHandCursor = true;
    //spoon3.events.onInputDown.add(this.ClickOnSpoon, this);
    spoon3.input.enableDrag(true);
    this.game.physics.arcade.enable(spoon3);
    spoon3.enableBody =true;
    spoon3.anchor.setTo(.5,.5);
    voice.destroy(true);
    voice=this.game.add.audio("step1_6",1);
    voice.play();
    dialog.text="Add a small amount of the mixture to the china dish.";
    arrow.x=880;
    arrow.y=770;//370
    arrow_y=770;
    arrow.visible=true;
  },
  
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj.nam=="spoon"){
        currentobj.angle=-20;
        //currentobj.frame=0;
        //currentobj.scale.setTo(1,1);
      }

      if(currentobj==bowl && bowl.frameName != "bowl0022.png"){
        console.log(bowl.frameName);
        arrow.x=1300;
        arrow.y=725;//325
        arrow_y=725;
      }
      if(currentobj==bowl && bowl.frameName == "bowl0022.png"){
        console.log(bowl.frameName+"///");
        arrow.x=140;
        arrow.y=485;
        arrow_y=485;
        
      }
      
      if(currentobj==spoon1){
        arrow.x=1470;
        arrow.y=750;//350
        arrow_y=750;
        currentobj.alpha=0;
        spoon1_1.visible=true;
      }
      if(currentobj==spoon2){
        arrow.x=1200;
        arrow.y=750;//350
        arrow_y=750;
        currentobj.alpha=0;
        spoon2_1.visible=true;
      }
      if(currentobj==spoon3){
        arrow.x=920;
        arrow.y=770;//370
        arrow_y=770;
        currentobj.alpha=0;
        spoon3_1.visible=true;
        spoon3.angle-20;
      }

      if(currentobj==Grander_Front){
        arrow.x=1300;
        arrow.y=700;//370
        arrow_y=700;
        Grander_back.x=1200;
        Grander_back.y=750;
        grinderMove=false;
        Grander_animation.visible=true;
      }
      
      if(currentobj==glass_rod || currentobj==glass_rod1){
        arrow.x=1320;//1520
        arrow.y=880;//570
        arrow_y=880;
        currentobj.angle=90;
        
      }
      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

Hot:function(){

  voice.destroy();
// voice2.destroy();
//   voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Simulation_hot1",true,false,ip);

 },
toSimulation:function(){

  voice.destroy();
// voice2.destroy();
//   voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);

 },
graph:function(){

 },

LC:function(){

  voice.destroy();
// voice2.destroy();
//   voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);


 },


 nextsim:function()
 {


 },
 buttonState:function(state1,state2){

 state1.visible = false;
 state2.visible = true;
 },

buttonState2:function(state2,state1){


 state1.visible = true;
 state2.visible = false;
 }


}
