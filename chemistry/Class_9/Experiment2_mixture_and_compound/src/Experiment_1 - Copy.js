var experiment_1 = function(game){

var inst;



var delay;
var incr;
var currentobj;
var arrow_y;
var mix_count;
var spoon3;
var bowl;
var interval;
var heatFlag;
var heatFlag2;
var magnetFlag;
var glass_rod1;
var content1;
var magnet1;
var magnet2;
var spoon4;
var spoon5;
var shakeFlag1;
var shakeFlag2;
var shakingFlag1;
var shakingFlag2;
var shakingCount1;
var shakingCount2;
var mixFlag;
var stickFlag;
var stick;
var texttube3;
var texttube4;
}


experiment_1.prototype = {
  	create: function(){
      fontstyle_1={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_2={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_3={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_4={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_5={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_6={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_7={ font: "40px Segoe UI", fill: "#FFA500", align: "left" };
hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');
interval=0;
heatFlag=false;
heatFlag2=false;
magnetFlag=false;
currentobj=null;
glass_rod1=null;
magnet1=null;
magnet2=null;
spoon4=null;
spoon5=null;
shakeFlag1=false;
shakeFlag2=false;
shakingFlag1=false;
shakingFlag2=false;
shakingCount1=0;
shakingCount2=0;
mixFlag=false;
hclFlag=false;
compHeatFlag=false;
mixHeatFlag=false;
stickFlag=false;
stick=null;
texttube3=null;
texttube4=null;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/


spoon3=null;
bowl=null;
mix_count=0;

voice=this.game.add.audio('sim_1',1);
voice2=this.game.add.audio('sim_2',1);
voice3=this.game.add.audio('sim_3',1);
voice.play();

this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Experiment_3", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  nextSound3:function(){
    voice3.play();
  },
  addItems:function(){
    
      
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
    dia_box.scale.setTo(.8,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(60,50,dia_text,fontstyle_2);
    collider=this.game.add.sprite(840,770, 'collider');//y-320
    dialog.y=60;
    this.game.physics.arcade.enable(collider);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.4;


    disulphide=this.game.add.sprite(1750,700, 'compounds', 'disulphide.png');//y-600
    hcl=this.game.add.sprite(1550,700, 'compounds', 'hcl.png');//y-600
    droper1=this.game.add.sprite(1790,670, 'compounds', 'droper1.png');//y-570
    droper1.xp=1790;
    droper1.yp=670;//y-570
    droper2=this.game.add.sprite(1790,675, 'compounds', 'droper1.png');//y-575
    droper2.xp=1790;
    droper2.yp=675;//y-575
    droper2.visible=false;
    droper3=this.game.add.sprite(1610,675, 'compounds', 'droper1.png');//y-575
    droper3.xp=1610;
    droper3.yp=675;//y-575
    droper4=this.game.add.sprite(1610,675, 'compounds', 'droper1.png');//y-575
    droper4.xp=1610;
    droper4.yp=675;//y-575
    droper4.visible=false;





    plate=this.game.add.sprite(800,720, 'compounds', 'plate.png');//y-320
    plate1=this.game.add.sprite(-250,720, 'compounds', 'plate.png');//y-320
    mixture=this.game.add.sprite(790,840, 'compounds', 'mixture.png');//y-440
    mixture.visible=false;
    mix=this.game.add.sprite(780,570, 'mixing', 'mix0001.png');//y-170
    mix.scale.setTo(.61,.61);
    mix.visible=false;
    mix.animations.add('anim',[
            'mix0002.png',
            'mix0003.png',
            'mix0004.png',
            'mix0005.png',
            'mix0006.png',
            'mix0007.png',
            'mix0008.png',
            
             ], 10, true, false);

    ironFillings=this.game.add.sprite(1320,850, 'compounds', 'iron.png');//y-450, x-1370
    
    /**/
    
    
    sulphur=this.game.add.sprite(1070,850, 'compounds', 'sulphur.png');//-450, x-1120
    spoon2=this.game.add.sprite(1220,870, 'spoon', 'spoon.png');//y-470, x-1270
    spoon2.angle=-20;
    spoon2.xp=1220;//1270
    spoon2.yp=850;//450
    spoon2.scale.set(.7);
    spoon2.anchor.setTo(.5,.5);
    spoon2.nam="spoon";
    spoon2.events.onDragStart.add(function() {this.gameC(spoon2)}, this);
    spoon2.events.onDragStop.add(function() {this.gameC1(spoon2)}, this);
    spoon2.inputEnabled = true;
    spoon2.input.useHandCursor = true;
    spoon2.events.onInputDown.add(this.ClickOnSpoon, this);
    
    
    spoon1=this.game.add.sprite(1470,870, 'spoon', 'spoon.png');//y-470, x-1470
    spoon1.angle=-20;
    spoon1.scale.set(.7);
    spoon1.xp=1470;//1470
    spoon1.yp=850;//450
    spoon1.nam="spoon";
    spoon1.anchor.setTo(.5,.5);
    this.game.physics.arcade.enable(spoon1);
    spoon1.enableBody =true;
    
    
    
    burner=this.game.add.sprite(30,600, 'compounds', 'burner0001.png');

    burner.animations.add('anim',['burner0001.png',
            'burner0001.png',
             ], 10, true, false);
   
    glass_rod=this.game.add.sprite(1650,920, 'compounds', 'glass_rod.png');
    glass_rod.anchor.setTo(.5,.5);
    glass_rod.xp=1650;
    glass_rod.yp=920;
    glass_rod.angle=90;
    arrow=this.game.add.sprite(1200,750, 'arrow');//y-350
    arrow.angle=90;
    arrow_y=750;//y-350
    arrow.visible=false;
    //mixture_label.scale.setTo(1.5,1.5);
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step_1",1);
    //voice.play();
    dialog.text="Take a small amount of Sulphur into the empty plate using a spoon.";
    spoon2.input.enableDrag(true);
    this.game.physics.arcade.enable(spoon2);
    spoon2.enableBody =true;
    
    arrow.visible=true;

  },
  ClickOnSpoon:function(){

  },
  gameC:function(obj)
  {
    obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    arrow.visible=false;
    if(obj==spoon1){
      obj.frame=2;
      arrow.x=920;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
    }else if(obj==spoon2){
      obj.frame=4;
      arrow.x=920;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
    }else if(obj==glass_rod){
      arrow.x=920;
      arrow.y=770;//370
      arrow_y=770;
      arrow.visible=true;
    }else if(obj==spoon3){
      obj.frame=3;
      arrow.x=1300;
      arrow.y=720;//320
      arrow_y=720;
      arrow.visible=true;
    }else if(spoon4!=null && obj==spoon4){
      obj.frame=1;
      obj.scale.setTo(.5,.5);
      arrow.x=1270;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
    }else if(spoon5!=null && obj==spoon5){
      arrow.x=1390;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
      obj.frame=3;
      obj.scale.setTo(.5,.5);
    }else if(obj==bowl && bowl.frameName != "bowl0022.png"){
      arrow.x=170;
      arrow.y=525;
      arrow_y=525;
      arrow.visible=true;
    }else if(obj==bowl && bowl.frameName == "bowl0022.png"){
      arrow.x=630;
      arrow.y=750;//350
      arrow_y=750;
      arrow.visible=true;
    }else if(obj==glass_rod1){
      arrow.x=150;
      arrow.y=480;//480
      arrow_y=480;
      arrow.visible=true;
    }else if(obj==magnet1){
      arrow.x=600;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }else if(obj==magnet2){
      arrow.x=900;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }else if(obj==droper1 || obj==droper3){
      arrow.x=1270;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
    }else if(obj==droper2 || obj==droper4){
      arrow.x=1390;
      arrow.y=540;//140
      arrow_y=540;
      arrow.visible=true;
    }else if(obj==texttube3 || obj==texttube4){
      //arrow.angle=30;
      arrow.x=200;
      arrow.y=750;//350
      arrow_y=750;
      arrow.visible=true;
    }else if(obj==stick){
      arrow.x=470;
      arrow.y=500;
      arrow_y=500;
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==glass_rod){
      glass_rod.visible=false;
      mix.play("anim",true);
      mixFlag=true;
      arrow.visible=false;
    }else if(currentobj==glass_rod1){
      glass_rod1.visible=false;
      mix_count=0;
      heatFlag2=true;
      bowl.play("anims",true);
      arrow.visible=false;
    }else if(currentobj.nam=="spoon"){
      currentobj.angle=-30;
      currentobj.reset(currentobj.xp,currentobj.yp);//
      currentobj.frame=0;
      //currentobj.scale.setTo(1,1);
    }
   
    if(currentobj==spoon2){
      mix.visible=true;
      spoon1.events.onDragStart.add(function() {this.gameC(spoon1)}, this);
      spoon1.events.onDragStop.add(function() {this.gameC1(spoon1)}, this);
      spoon1.inputEnabled = true;
      spoon1.input.useHandCursor = true;
      spoon1.events.onInputDown.add(this.ClickOnSpoon, this);
      spoon1.input.enableDrag(true);
      dialog.text="Add same amount of Iron Filling";
      arrow.x=1450;
      arrow.visible=true;
    }else if(currentobj==spoon1){
      mix.frame=1;
      glass_rod.events.onDragStart.add(function() {this.gameC(glass_rod)}, this);
      glass_rod.events.onDragStop.add(function() {this.gameC1(glass_rod)}, this);
      glass_rod.inputEnabled = true;
      glass_rod.input.useHandCursor = true;
      glass_rod.events.onInputDown.add(this.ClickOnSpoon, this);
      glass_rod.input.enableDrag(true);
      this.game.physics.arcade.enable(glass_rod);
      glass_rod.enableBody=true;
      dialog.text="Mix well using a glass rod";
      arrow.x=1670;//1520
      arrow.y=800;//570
      arrow_y=800;
      arrow.visible=true;

    }else if(currentobj==spoon3){
      bowl.frame=2;
      collider.x=130;
      collider.y=500;
      collider.scale.setTo(2,3);
      bowl.events.onDragStart.add(function() {this.gameC(bowl)}, this);
      bowl.events.onDragStop.add(function() {this.gameC1(bowl)}, this);
      bowl.inputEnabled = true;
      bowl.input.useHandCursor = true;
      bowl.events.onInputDown.add(this.ClickOnSpoon, this);
      bowl.input.enableDrag(true);
      this.game.physics.arcade.enable(bowl);
      dialog.text="Heat the mixture";
      arrow.x=1300;
      arrow.y=725;//325
      arrow_y=725;
      arrow.visible=true;
    }else if(currentobj==spoon4){
      if(hclFlag){
        texttube3.frameName="testtubec0001.png";
      }else{
        texttube1.frameName="mix_compound0002.png";
      }
      
      arrow.x=980;
      arrow.y=370;
      arrow_y=370;
      arrow.visible=true;
      spoon5.events.onDragStart.add(function() {this.gameC(spoon5)}, this);
      spoon5.events.onDragStop.add(function() {this.gameC1(spoon5)}, this);
      spoon5.inputEnabled = true;
      spoon5.input.useHandCursor = true;
      spoon5.events.onInputDown.add(this.ClickOnSpoon, this);
      spoon5.input.enableDrag(true);
      this.game.physics.arcade.enable(spoon5);
      spoon5.enableBody=true;
      collider.x=1360;
    }else if(currentobj==spoon5){
      
      collider.x=1235;
      
      arrow.x=1830;
      arrow.y=470;
      arrow_y=470;
      if(hclFlag){
        texttube4.frameName="testtube0002.png";
        arrow.x=1650;
        droper3.events.onDragStart.add(function() {this.gameC(droper3)}, this);
        droper3.events.onDragStop.add(function() {this.gameC1(droper3)}, this);
        droper3.inputEnabled = true;
        droper3.input.useHandCursor = true;
        droper3.events.onInputDown.add(this.ClickOnSpoon, this);
        droper3.input.enableDrag(true);
        this.game.physics.arcade.enable(droper3);
        droper3.enableBody=true;
        dialog.text="Add a few drops of dil.HCl \ninto the test tubes";
      }else{
        dialog.text="Add a few drops of carbon \ndisulphide into the test tubes";
        texttube2.frameName="mix_mixture0002.png";
        droper1.events.onDragStart.add(function() {this.gameC(droper1)}, this);
        droper1.events.onDragStop.add(function() {this.gameC1(droper1)}, this);
        droper1.inputEnabled = true;
        droper1.input.useHandCursor = true;
        droper1.events.onInputDown.add(this.ClickOnSpoon, this);
        droper1.input.enableDrag(true);
        this.game.physics.arcade.enable(droper1);
        droper1.enableBody=true;
      }
      arrow.visible=true;
      
      
    }else if(currentobj==bowl){
      if(bowl.frameName == "bowl0022.png"){
        plate1.frameName="black_mass.png";
        plate1.y=850;//450
        bowl.y=850;//450
        bowl.frame=0;
        spoon3.visible=false;
        this.game.add.tween(bowl).to( {x:2000}, 600, Phaser.Easing.Out, true);
        compound_lable=this.game.add.sprite(-200,910,'compounds', 'compound_lable.png');//500
        tween5=this.game.add.tween(compound_lable).to( {x:530}, 600, Phaser.Easing.Out, true);
        tween5.onComplete.add(function(){
          ////////////popup = this.game.add.sprite(420,550,'popup');
          //popup.visible=false;
          //////////////popup.scale.setTo(.4,.3);
          ////////////////voice2.play();
          ////////////////voice2.onStop.add(this.nextSound3,this);
          dialog.text="You have successfully made a mixture and a compound using sulphur \nand iron fillings. Now Check the magnetism in the materials."
          
          //content1=this.game.add.text(470,700,dia_text,fontstyle_2);
          play = this.game.add.sprite(1230,920,'components','play_pressed.png');
          play.scale.setTo(.7,.7);
          play.inputEnabled = true;
          //play.input.priorityID = 3;
          play.input.useHandCursor = true;
          play.events.onInputDown.add(this.toContinue, this);
        }.bind(this));
      }else{
        burner.frameName = "burner0002.png";
        heatFlag=true;
      }
      bowl.x=170;
      bowl.y=710;
      bowl.xp=170;
      bowl.yp=710;
      arrow.visible=false;
      
    }else if(currentobj==magnet1){
      magnet1.x=600;
      magnet1.y=400;
      arrow.x=1170;
      arrow.y=340;
      arrow_y=340;
      arrow.visible=false;
      /*arrow.visible=true;
      magnet2.events.onDragStart.add(function() {this.gameC(magnet2)}, this);
      magnet2.events.onDragStop.add(function() {this.gameC1(magnet2)}, this);
      magnet2.inputEnabled = true;
      magnet2.input.useHandCursor = true;
      //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
      magnet2.input.enableDrag(true);
      this.game.physics.arcade.enable(magnet2);*/
      collider.reset(840,320);
      popup = this.game.add.sprite(420,550,'popup');
      popup.scale.setTo(.4,.3);
      //popup.visible=false;
      dia_text="Iron loses its magnetic properties on forming to \ncompound. So no particles cling into the magnet."
      content1=this.game.add.text(470,700,dia_text,fontstyle_2);
      play = this.game.add.sprite(1230,920,'components','play_pressed.png');
      play.scale.setTo(.7,.7);
      play.inputEnabled = true;
      //play.input.priorityID = 3;
      play.input.useHandCursor = true;
      play.events.onInputDown.add(this.toContinue01, this);
    }else if(currentobj==magnet2){
      mixture.visible=false;
      mix.visible=true;
      mix.frameName="mixture_magnet0002.png";
      mix.scale.setTo(1.02,1.02);
      mix.x=800;
      mix.y=305;
      magnet2.visible=false;
      magnetFlag=true;
      interval=0;
      arrow.visible=false;

    }else if(currentobj==droper1){
      texttube1.frameName="mix_compound0003.png";
      currentobj.reset(currentobj.xp,currentobj.yp);
      droper1.visible=false;
      droper2.visible=true;
      arrow.x=1830;
      arrow.y=470;
      arrow_y=470;
      arrow.visible=true;
      droper2.events.onDragStart.add(function() {this.gameC(droper2)}, this);
      droper2.events.onDragStop.add(function() {this.gameC1(droper2)}, this);
      droper2.inputEnabled = true;
      droper2.input.useHandCursor = true;
      droper2.events.onInputDown.add(this.ClickOnSpoon, this);
      droper2.input.enableDrag(true);
      this.game.physics.arcade.enable(droper2);
      droper2.enableBody=true;
      collider.x=1360;
    }else if(currentobj==droper2){
      texttube2.frameName="mix_mixture0003.png";
      currentobj.reset(currentobj.xp,currentobj.yp);
      arrow.x=1270;
      arrow.y=140;
      arrow_y=140;
      arrow.visible=true;
      dialog.text="Stir well the test tube of \ncompound";      
      texttube1.inputEnabled = true;
      texttube1.input.useHandCursor = true;
      texttube1.events.onInputDown.add(this.ClickOntexttube1, this);
    }else if(currentobj==droper3){
      texttube3.frameName="testtubec0002.png";
      currentobj.reset(currentobj.xp,currentobj.yp);
      droper3.visible=false;
      droper4.visible=true;
      arrow.x=1650;
      arrow.y=470;
      arrow_y=470;
      arrow.visible=true;
      droper4.events.onDragStart.add(function() {this.gameC(droper4)}, this);
      droper4.events.onDragStop.add(function() {this.gameC1(droper4)}, this);
      droper4.inputEnabled = true;
      droper4.input.useHandCursor = true;
      droper4.events.onInputDown.add(this.ClickOnSpoon, this);
      droper4.input.enableDrag(true);
      this.game.physics.arcade.enable(droper4);
      droper4.enableBody=true;
      collider.x=1360;
    }else if(currentobj==droper4){
      texttube4.frameName="testtube0003.png";
      currentobj.reset(currentobj.xp,currentobj.yp);
      arrow.x=1270;
      arrow.y=140;
      arrow_y=140;
      arrow.visible=true;
      dialog.text="Drag the test tube of compound \nto the heater"
      texttube3.events.onDragStart.add(function() {this.gameC(texttube3)}, this);
      texttube3.events.onDragStop.add(function() {this.gameC1(texttube3)}, this);
      texttube3.inputEnabled = true;
      texttube3.input.useHandCursor = true;
      texttube3.events.onInputDown.add(this.ClickOnSpoon, this);
      texttube3.input.enableDrag(true);
      this.game.physics.arcade.enable(texttube3);
      texttube3.enableBody=true;
      collider.reset(140,380);
      collider.scale.setTo(3,3);
      /*texttube1.inputEnabled = true;
      texttube1.input.useHandCursor = true;
      texttube1.events.onInputDown.add(this.ClickOntexttube1, this);*/
    }else if(currentobj==texttube3){
      dialog.text="Observe the change of copper \nsulphate in the test tube"
      texttube.frameName="testtubec0004.png";
      arrow.visible=false;
      texttube3.visible=false;
      texttube.visible=true;
      tube.visible=true;
      burner2.visible=true;
      texttube.animations.add('anim',['testtubec0004.png',
            'testtubec0005.png',
            'testtubec0006.png',
            'testtubec0007.png',
            'testtubec0008.png',  
            'testtubec0009.png',
            'testtubec0010.png',
            'testtubec0011.png',
            'testtubec0012.png',  
            'testtubec0013.png',
            'testtubec0014.png',
            'testtubec0015.png',
            'testtubec0016.png',  
             ], 10, true, false);
      texttube.play('anim', false);
      compHeatFlag=true;
      interval=0;
    }else if(currentobj==texttube4){
      dialog.text="heat the contents of the test \ntube. A colourless gas evolves"
      texttube.frameName="testtubec0004.png";
      arrow.visible=false;
      texttube4.visible=false;
      texttube.visible=true;
      tube.visible=true;
      burner2.visible=true;
      texttube.animations.add('anim',[
            'testtube0005.png',
            'testtube0006.png',
            'testtube0007.png',
            'testtube0008.png',  
            'testtube0009.png',
            'testtube0010.png',
            'testtube0011.png',
            'testtube0012.png',  
            'testtube0013.png',
            'testtube0014.png',
            'testtube0015.png',
            'testtube0016.png',
            'testtube0017.png',  
             ], 10, true, false);
      texttube.play('anim', false);
      mixHeatFlag=true;
      interval=0;
    }else if(currentobj==stick){
      stick.x=490;
      stick.y=550;
      arrow.visible =false;
      stick.frameName="match_burst0001.png";
      stick.animations.add('anim',[
            'match_burst0001.png',
            'match_burst0002.png',
            'match_burst0003.png',
            'match_burst0004.png',
            'match_burst0005.png',
            'match_burst0006.png',
            'match_burst0007.png',
            'match_burst0008.png',  
            'match_burst0009.png',
            'match_burst0010.png',
            'match_burst0011.png',
            'match_burst0012.png',  
            'match_burst0013.png',
            'match_burst0014.png',
            
             ], 20, true, true);
      stick.play('anim', true);
      stickFlag=true;
    }
    
  },
  ClickOntexttube1:function(){
    shakeFlag1=true;
    arrow.visible=false;
    /*arrow.x=1390;
    arrow.y=140;
    arrow_y=140;
    arrow.visible=true;
    texttube2.inputEnabled = true;
    texttube2.input.useHandCursor = true;
    texttube2.events.onInputDown.add(this.ClickOntexttube2, this);
    texttube1.inputEnabled = false;*/
  },
  ClickOntexttube2:function(){
    shakeFlag2=true;
    arrow.visible=false;
    texttube2.inputEnabled = false;
  },
  drawVLine:function(xp,yp,val){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(2,0xFFFFFF,1);
        line1.moveTo(xp, yp);
        yp+=20;
        line1.lineTo(xp, yp);
    var text1 = this.game.add.text(0,0,val, fontstyle_6);
        text1.x=xp-10;
        text1.y=yp+10;
  },
  drawVLine1:function(xp,yp,val){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(2,0xFFFFFF,1);
        line1.moveTo(xp, yp);
        xp+=20;
        line1.lineTo(xp, yp);
    var text1 = this.game.add.text(0,0,val, fontstyle_6);
        text1.x=xp-50;
        text1.y=yp-10
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	addList:function(){
    

  },
  ClickOnPlay:function(){

    
  },
  
  ClickOnSlow2:function(){

  },
  


  hotsec:function()
  {

    //voice41.destroy();
    //voice5.play();

  },
  hclFunction:function(){
    ///////////////////////////////////////////////
    /*ironFillings.visible=false;
    sulphur.visible=false;
    spoon1.visible=false;
    spoon2.visible=false;
    mixture.visible=true;
    
    arrow.visible=false;
    plate1.frameName="black_mass.png";
    plate1.x=500;
    plate1.y=450;
    popup = this.game.add.sprite(420,550,'popup');
    popup.scale.setTo(.4,.3);
    //popup.visible=false;
    dia_text="You successfully Completed the testing \nwith carbon disulphide. Now check with dil. HCl."
    content1=this.game.add.text(470,700,dia_text,fontstyle_2);
    play = this.game.add.sprite(1230,920,'components','play_pressed.png');
    play.scale.setTo(.7,.7);
    play.inputEnabled = true;
    //play.input.priorityID = 3;
    play.input.useHandCursor = true;
    play.events.onInputDown.add(this.toContinue2, this);
    ////////////////////////////////////////
    hclFlag=true;*/
  },
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(shakeFlag1){
      texttube1.y=20;
      shakingFlag1=true;
      shakeFlag1=false;
      texttube1.animations.add('anim',[
            'mix_compound0003.png',
            'mix_compound0004.png',
            'mix_compound0005.png',
            'mix_compound0006.png',
            'mix_compound0007.png',
            'mix_compound0008.png',
            
             ], 3, true, false);
      texttube1.play("anim",false);
    }
    if(shakingFlag1){
      shakingCount1++;
      texttube1.angle-=1;
      if(texttube1.angle<-5)
        texttube1.angle=5;
      if(shakingCount1>60){
        shakingFlag1=false;
        texttube1.animations.stop(null, true);
        texttube1.frameName="mix_compound0008.png";
        texttube1.angle=0;
        texttube1.y=225;
        popup = this.game.add.sprite(420,550,'popup');
        popup.scale.setTo(.4,.3);
        //popup.visible=false;
        dia_text="No changes, because in compound state sulphur \nloses its original properties."
        content1=this.game.add.text(470,700,dia_text,fontstyle_2);
        play = this.game.add.sprite(1230,920,'components','play_pressed.png');
        play.scale.setTo(.7,.7);
        play.inputEnabled = true;
        //play.input.priorityID = 3;
        play.input.useHandCursor = true;
        play.events.onInputDown.add(this.toContinue02, this);
      }
    }
    if(shakeFlag2){
      texttube2.y=20;
      shakingFlag2=true;
      shakeFlag2=false;
      texttube2.animations.add('anim',[
            'mix_mixture0003.png',
            'mix_mixture0004.png',
            'mix_mixture0005.png',
            'mix_mixture0006.png',
            'mix_mixture0007.png',
            'mix_mixture0008.png',
            'mix_mixture0009.png',
            'mix_mixture0010.png',
            'mix_mixture0011.png',
            'mix_mixture0012.png',
            'mix_mixture0013.png',
            ], 3, true, false);
      texttube2.play("anim",false);
    }
    if(shakingFlag2){
      shakingCount2++;
      texttube2.angle-=1;
      if(texttube2.angle<-5)
        texttube2.angle=5;
      if(shakingCount2>60){
        shakingFlag2=false;
        texttube2.animations.stop(null, true);
        texttube2.frameName="mix_mixture0013.png";
        texttube2.angle=0;
        texttube2.y=215;
        ///////////////////////////////
        ironFillings.visible=false;
        sulphur.visible=false;
        spoon1.visible=false;
        spoon2.visible=false;
        mixture.visible=true;
        
        arrow.visible=false;
        plate1.frameName="black_mass.png";
        plate1.x=500;
        ////////////////////////////plate1.y=450;
        //////////////////////////
        arrow.visible=false;
        popup = this.game.add.sprite(420,550,'popup');
        popup.scale.setTo(.4,.3);
        //popup.visible=false;
        dia_text="Sulphur first dissolves then after some time it reappears. \nNow check with dil. HCl."
        content1=this.game.add.text(470,700,dia_text,fontstyle_2);
        play = this.game.add.sprite(1230,920,'components','play_pressed.png');
        play.scale.setTo(.7,.7);
        play.inputEnabled = true;
        //play.input.priorityID = 3;
        play.input.useHandCursor = true;
        play.events.onInputDown.add(this.toContinue2, this);
        hclFlag=true;
      }
    }
    
    if(mixFlag){
      mix_count++;
      if(mix_count>=60){
        mixFlag=false;
        mix.animations.stop(null, true);
        mix.visible=false;
        currentobj.reset(1650,920);
        currentobj.angle=90;
        plate.visible=false;
        mixture.visible=true;
        glass_rod.visible=true;
        dialog.text="Now we got a Mixture";
        this.game.add.tween(ironFillings).to( {x:2500}, 600, Phaser.Easing.Out, true);
        tween1=this.game.add.tween(spoon1).to( {x:2500}, 600, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          mixFlag=false;
          this.game.add.tween(sulphur).to( {x:2500}, 600, Phaser.Easing.Out, true);
          tween2=this.game.add.tween(spoon2).to( {x:2500}, 600, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            mixture_label=this.game.add.sprite(-55,910, 'compounds', 'mixture_label.png');//y-510
            tween3=this.game.add.tween(mixture_label).to( {x:825}, 800, Phaser.Easing.Out, true);
            tween3.onComplete.add(function(){
              collider.x=1230;
              collider.y=700;//300
              collider.scale.setTo(2,3);
              
              //collider.alpha=0;
              bowl=this.game.add.sprite(1290,950, 'heating', 'bowl0001.png');//y-550
              bowl.anchor.setTo(.5,1);  
              bowl.xp=1290;
              bowl.yp=950;//55
              bowl.animations.add('anims',['bowl0005.png',
                      'bowl0006.png',
                      'bowl0007.png',
                      'bowl0008.png',
                      'bowl0009.png',
                      'bowl0010.png',
                      'bowl0011.png',
                      'bowl0012.png',
                      'bowl0013.png',
                      'bowl0014.png',
                      'bowl0015.png',
                      'bowl0016.png',
                      'bowl0017.png',
                      'bowl0018.png',
                      'bowl0019.png',
                      'bowl0020.png',
                      'bowl0021.png',
                      'bowl0022.png',
                      
                      ], 20, true, false);
              spoon3=this.game.add.sprite(930,870, 'spoon', 'spoon.png');//y-470
              console.log("/////////");
              spoon3.angle=-20;
              spoon3.xp=930;
              spoon3.yp=870;//470
              spoon3.scale.set(.7);
              spoon3.nam="spoon";
              spoon3.events.onDragStart.add(function() {this.gameC(spoon3)}, this);
              spoon3.events.onDragStop.add(function() {this.gameC1(spoon3)}, this);
              spoon3.inputEnabled = true;
              spoon3.input.useHandCursor = true;
              //spoon3.events.onInputDown.add(this.ClickOnSpoon, this);
              spoon3.input.enableDrag(true);
              this.game.physics.arcade.enable(spoon3);
              spoon3.enableBody =true;
              spoon3.anchor.setTo(.5,.5);
              dialog.text="Take a small amount of mixture into the bowl";
              arrow.x=880;
              arrow.y=770;//370
              arrow_y=770;
              arrow.visible=true;
            }.bind(this));    
          }.bind(this));  
        }.bind(this));
        
      }
    }
    if(magnetFlag){
      interval++;
      if(interval==20){
        mix.frameName="mixture_magnet0002.png";
        mix.scale.setTo(1.02,1.02);
        mix.x=800;
        mix.y=305;
      }else if(interval==40){
        mix.frameName="mixture_magnet0003.png";
      }else if(interval==60){
        mix.frameName="mixture_magnet0004.png";
      }else if(interval==90){
        /////////////////////////
        

        popup = this.game.add.sprite(420,550,'popup');
        popup.scale.setTo(.4,.3);
        //popup.visible=false;
        dia_text="Iron clings into the magnet, \nbecause iron retains its properties. \n\nNow check with carbon disulphide."
        content1=this.game.add.text(470,700,dia_text,fontstyle_2);
        play = this.game.add.sprite(1230,920,'components','play_pressed.png');
        play.scale.setTo(.7,.7);
        play.inputEnabled = true;
        //play.input.priorityID = 3;
        play.input.useHandCursor = true;
        play.events.onInputDown.add(this.toContinue1, this);
        arrow.x=620;
        //arrow.visible=true;
        collider.scale.setTo(1,3);
        collider.x=1235;
        collider.y=100;
        magnetFlag=false;
      }
    }
    if(heatFlag2 && bowl.frame==21){
      mix_count++;
      if(mix_count>=5){
        heatFlag2=false;
        bowl.animations.stop(null, true);
        bowl.frameName = "bowl0022.png";
        currentobj.reset(1650,920);
        currentobj.angle=90;
        glass_rod1.visible=true;
        dialog.text="Now we got a Compound";
        tween4=this.game.add.tween(plate1).to( {x:500}, 600, Phaser.Easing.Out, true);
        tween4.onComplete.add(function(){
          dialog.text="Pour the compound \ninto the empty plate";
          arrow.x=140;
          arrow.y=485;
          arrow_y=485;
          arrow.visible=true;
          bowl.inputEnabled=true;
          bowl.input.enableDrag(true);
          bowl.body.enable=true;
          collider.reset(530,750);//y-350
          collider.scale.setTo(3,3);
        }.bind(this));
        
      }
      
    }
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    /////////////////////////////
    if(heatFlag){
      interval++;
      if(interval>=60){
        interval=0;
        bowl.frame++;
        if(bowl.frame==4){
          heatFlag=false;
          dialog.text="Stir well using the glass rod";
          arrow.x=1670;//1520
          arrow.y=800;//570
          arrow_y=800;
          arrow.visible=true;
          glass_rod.visible=false;
          glass_rod1=this.game.add.sprite(1650,920, 'compounds', 'glass_rod.png');
          glass_rod1.anchor.setTo(.5,.5);
          glass_rod1.xp=1650;
          glass_rod1.yp=920;
          glass_rod1.angle=90;
          glass_rod1.events.onDragStart.add(function() {this.gameC(glass_rod1)}, this);
          glass_rod1.events.onDragStop.add(function() {this.gameC1(glass_rod1)}, this);
          glass_rod1.inputEnabled = true;
          glass_rod1.input.useHandCursor = true;
          glass_rod1.events.onInputDown.add(this.ClickOnSpoon, this);
          glass_rod1.input.enableDrag(true);
          this.game.physics.arcade.enable(glass_rod1);
        }
      }
    }
    if(compHeatFlag){
      interval++;
      if(interval>=60){
        interval=0;
        console.log(hcl_cmp.frame)
        if(hcl_cmp.frame<9){
          hcl_cmp.frame++;
        }else{
          compHeatFlag=false;
          //dialog.text=" ";
          popup = this.game.add.sprite(520,550,'popup');
          //popup.visible=false;
          popup.scale.setTo(.4,.3);
          dia_text="A colourless gas evolves that reacts with copper \nsulphate to form a black coloured precipitate."
          content1=this.game.add.text(570,700,dia_text,fontstyle_2);
          play = this.game.add.sprite(1330,920,'components','play_pressed.png');
          play.scale.setTo(.7,.7);
          play.inputEnabled = true;
          //play.input.priorityID = 3;
          play.input.useHandCursor = true;
          play.events.onInputDown.add(this.toContinue3, this);
        }
      }
    }
    if(mixHeatFlag){
          interval++;
          if(interval>=500){
              dialog.text="Drag the match stick near the \nmouth of the test tube"
              mixHeatFlag=false;
              tube.visible=false;
              stick=this.game.add.sprite(800,600,'hclActions','match_burst0016.png');
              stick.anchor.setTo(.5,.5);
              stick.xp=800;
              stick.yp=600;
              stick.animations.add('anim',[
                    'match_burst0016.png',
                    'match_burst0017.png',
                    'match_burst0018.png',
                    'match_burst0019.png',
                    'match_burst0020.png',
                    ], 15, true, false);
              stick.play("anim",false);
              stick.events.onDragStart.add(function() {this.gameC(stick)}, this);
              stick.events.onDragStop.add(function() {this.gameC1(stick)}, this);
              stick.inputEnabled = true;
              stick.input.useHandCursor = true;
              stick.events.onInputDown.add(this.ClickOnSpoon, this);
              stick.input.enableDrag(true);
              this.game.physics.arcade.enable(stick);
              stick.enableBody=true;
              arrow.x=800;
              arrow.y=500;
              arrow_y=500;
              arrow.visible=true;
              collider.reset(430,500);
              collider.scale.setTo(1,3);
            
          }
        }
    if(stickFlag) {
      if(stick.frameName == "match_burst0014.png"){
        stickFlag=false;
        stick.animations.stop(null,true);
        stick.frameName = "match_burst0014.png";
        popup = this.game.add.sprite(520,550,'popup');
          //popup.visible=false;
          popup.scale.setTo(.4,.3);
          dia_text="The flame dips out producing a popping sound"
          content1=this.game.add.text(570,700,dia_text,fontstyle_2);
          /*play = this.game.add.sprite(1330,920,'components','play_pressed.png');
          play.scale.setTo(.7,.7);
          play.inputEnabled = true;
          //play.input.priorityID = 3;
          play.input.useHandCursor = true;
          play.events.onInputDown.add(this.toSimulation, this);//*/
          if(!hotFlag){
            hotFlag=true;
            hotButton.animations.play('hotty');
            this.buttontween=this.game.add.tween(buttonGroup).to( { x:-130 }, 400, Phaser.Easing.Out, true);
            //this.buttontween.onComplete.add(this.complete1,this);
          }
      }
    }     
	},
  complete1:function(){
    
  },
  tweenComplete:function(){
    
  },
  tweenComplete2:function(){
    
    

  },
  tweenComplete3:function(){
    

  },
  tweenComplete4:function(){
    
  },
  tweenComplete5:function(){
    /////////////////////////
    /*collider.reset(530,350);
    collider.scale.setTo(3,3);
    collider.alpha=1;
    plate1.x=500;
    plate1.frameName="black_mass.png";
    plate1.y=450;
    mixture.visible=true;
    //mix.visible=true;
    mix.frameName="mixture_magnet0002.png";
    mix.scale.setTo(1.02,1.02);
    mix.x=800;
    mix.y=305;*/
    /////////////////////////
    
    
  },
  toContinue:function(){
    //popup.destroy();
    //content1.destroy();
    play.destroy();
    dialog.text="Drag the magnet into the \ncompound";
    magnet2=this.game.add.sprite(1150,490, 'compounds', 'magnet.png');
    magnet2.scale.setTo(.8,1);
    magnet2.anchor.setTo(.5,.5);
    magnet2.xp=1150;
    magnet2.yp=490;
    magnet1=this.game.add.sprite(1170,480, 'compounds', 'magnet.png');
    magnet1.scale.setTo(.8,1);
    magnet1.anchor.setTo(.5,.5);
    magnet1.xp=1170;
    magnet1.yp=480;
    magnet1.events.onDragStart.add(function() {this.gameC(magnet1)}, this);
    magnet1.events.onDragStop.add(function() {this.gameC1(magnet1)}, this);
    magnet1.inputEnabled = true;
    magnet1.input.useHandCursor = true;
    //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
    magnet1.input.enableDrag(true);
    this.game.physics.arcade.enable(magnet1);
    arrow.x=1170;
    arrow.y=340;
    arrow_y=340;
    arrow.visible=true;
  },
  toContinue01:function(){
    popup.destroy();
    content1.destroy();
    play.destroy();
    dialog.text="Drag the magnet into the \nmixture";
    arrow.visible=true;
    magnet2.events.onDragStart.add(function() {this.gameC(magnet2)}, this);
    magnet2.events.onDragStop.add(function() {this.gameC1(magnet2)}, this);
    magnet2.inputEnabled = true;
    magnet2.input.useHandCursor = true;
    //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
    magnet2.input.enableDrag(true);
    this.game.physics.arcade.enable(magnet2);
  },
  toContinue02:function(){
    popup.destroy();
    content1.destroy();
    play.destroy();
    dialog.text="Stir well the test tube of \nmixture"; 
    arrow.x=1390;
    arrow.y=140;
    arrow_y=140;
    arrow.visible=true;
    texttube2.inputEnabled = true;
    texttube2.input.useHandCursor = true;
    texttube2.events.onInputDown.add(this.ClickOntexttube2, this);
    texttube1.inputEnabled = false;
  },
  toContinue1:function(){
    popup.destroy();
    content1.destroy();
    play.destroy();
    dialog.text="Take two test tubes and add small \namount of compound into one \ntest tube and add small amount \nof mixture into another test tube.";
    //collider.reset(530,350);
       // collider.scale.setTo(3,3);
       // collider.alpha=1;
        plate1.x=500;
        plate1.frameName="black_mass.png";
        plate1.y=450;
        mix.frameName="mixture_magnet0004.png";
        
        /////////////////////////
        stand1=this.game.add.sprite(1150,300,'carbonMix','hold1.png');
        texttube1=this.game.add.sprite(1255,225,'carbonMix','mix_compound0001.png');
        texttube2=this.game.add.sprite(1380,217,'carbonMix','mix_mixture0001.png');
        texttube1.anchor.setTo(.5,0);
        texttube2.anchor.setTo(.5,0);
        stand2=this.game.add.sprite(1150,300,'carbonMix','hold2.png');
        if(magnet1!=null){
          magnet1.visible=false;
          magnet2.visible=false;
        }
        mix.visible=false;
        mixture.visible=true;
        
        spoon4=this.game.add.sprite(650,470, 'spoon', 'spoon.png');
        spoon4.angle=-20;
        spoon4.xp=650;
        spoon4.nam="spoon";
        spoon4.yp=470;
        spoon4.events.onDragStart.add(function() {this.gameC(spoon4)}, this);
        spoon4.events.onDragStop.add(function() {this.gameC1(spoon4)}, this);
        spoon4.inputEnabled = true;
        spoon4.input.useHandCursor = true;
        spoon4.events.onInputDown.add(this.ClickOnSpoon, this);
        spoon4.input.enableDrag(true);
        this.game.physics.arcade.enable(spoon4);
        spoon4.enableBody =true;
        spoon4.anchor.setTo(.5,.5);
        
        spoon5=this.game.add.sprite(950,470, 'spoon', 'spoon.png');
        spoon5.angle=-20;
        spoon5.xp=950;
        spoon5.yp=470;
        spoon5.nam="spoon";
        spoon5.anchor.setTo(.5,.5);
        this.game.physics.arcade.enable(spoon5);
        spoon5.enableBody =true;
        collider.x=1235;
        collider.y=100;
        collider.scale.setTo(1,3);
        collider.alpha=0;
        arrow.x=620;
        arrow.y=350;
        arrow_y=350;
        arrow.visible=true;
  },
  toContinue2:function(){
    popup.destroy();
    content1.destroy();
    play.destroy();
    dialog.text="Take two test tubes and add small \namount of compound into one \ntest tube and add small amount \nof mixture into another test tube.";
    //collider.reset(530,350);
       // collider.scale.setTo(3,3);
       // collider.alpha=1;
        //plate1.x=500;
        //plate1.frameName="black_mass.png";
        //plate1.y=450;
        //mix.frameName="mixture_magnet0004.png";
        
        /////////////////////////
        burner.visible=false;
        spoon1.visible=false;
        spoon2.visible=false;
        spoon3.visible=false;
        spoon4.visible=false;
        spoon5.visible=false;
        texttube1.visible=false;
        texttube2.visible=false;
        burner1=this.game.add.sprite(-65,585,'compounds','testtube_lamp_off.png');
        burner2=this.game.add.sprite(-90,550,'compounds','testtube_lamp.png');
        burner2.visible=false;
        stand=this.game.add.sprite(95,265,'compounds','testtube_holder.png');
        texttube=this.game.add.sprite(290,385,'hclActions','testtube0005.png');
        texttube.angle=59;
        texttube.visible=false;
        tube=this.game.add.sprite(185,350,'compounds','glass_tube.png');
        tube.visible=false;
        hand=this.game.add.sprite(165,440,'compounds','testtube_handl.png');
        hcl_cmp=this.game.add.sprite(425,600,'hclActions','coppersulphide0001.png');
        hcl_cmp.frame=0;
        standtop=this.game.add.sprite(95,265,'compounds','testtube_holdertop.png');
        cuso4=this.game.add.sprite(300,880,'compounds','cuso4.png');
        cuso4.scale.setTo(1.25,1.25);

        stand1=this.game.add.sprite(1150,300,'carbonMix','hold1.png');
        texttube3=this.game.add.sprite(1255,225,'hclActions','testtube0001.png');
        texttube4=this.game.add.sprite(1380,217,'hclActions','testtube0001.png');
        texttube3.xp=1255;
        texttube3.yp=225;
        texttube4.xp=1380;
        texttube4.yp=217;
        texttube3.anchor.setTo(.5,0);
        texttube4.anchor.setTo(.5,0);
        stand2=this.game.add.sprite(1150,300,'carbonMix','hold2.png');
        if(magnet1!=null){
          magnet1.visible=false;
          magnet2.visible=false;
        }
        mix.visible=false;
        mixture.visible=true;
        
        spoon4=this.game.add.sprite(650,470, 'spoon', 'spoon.png');
        spoon4.angle=-20;
        spoon4.xp=650;
        spoon4.nam="spoon";
        spoon4.yp=470;
        spoon4.events.onDragStart.add(function() {this.gameC(spoon4)}, this);
        spoon4.events.onDragStop.add(function() {this.gameC1(spoon4)}, this);
        spoon4.inputEnabled = true;
        spoon4.input.useHandCursor = true;
        spoon4.events.onInputDown.add(this.ClickOnSpoon, this);
        spoon4.input.enableDrag(true);
        this.game.physics.arcade.enable(spoon4);
        spoon4.enableBody =true;
        spoon4.anchor.setTo(.5,.5);
        
        spoon5=this.game.add.sprite(950,470, 'spoon', 'spoon.png');
        spoon5.angle=-20;
        spoon5.xp=950;
        spoon5.yp=470;
        spoon5.nam="spoon";
        spoon5.anchor.setTo(.5,.5);
        this.game.physics.arcade.enable(spoon5);
        spoon5.enableBody =true;
        collider.x=1235;
        collider.y=100;
        collider.scale.setTo(1,3);
        collider.alpha=0;
        arrow.x=620;
        arrow.y=350;
        arrow_y=350;
        arrow.visible=true;
        hclFlag=true;
  },
  toContinue3:function(){
    dialog.text="Drag the test tube of mixture \nfor heating"
    popup.destroy();
    content1.destroy();
    play.destroy();
    texttube.visible=false;
    //texttube.frameName="testtube_empty.png";
    tube.visible=false;
    cuso4.visible=false;
    hcl_cmp.frameName="testtube_empty.png";
    burner2.visible=false;
    arrow.x=1390;
    arrow.y=140;
    arrow_y=140;
    arrow.visible=true;
    texttube4.events.onDragStart.add(function() {this.gameC(texttube4)}, this);
    texttube4.events.onDragStop.add(function() {this.gameC1(texttube4)}, this);
    texttube4.inputEnabled = true;
    texttube4.input.useHandCursor = true;
    texttube4.events.onInputDown.add(this.ClickOnSpoon, this);
    texttube4.input.enableDrag(true);
    this.game.physics.arcade.enable(texttube4);
    texttube4.enableBody=true;
  },
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj.nam=="spoon"){
        currentobj.angle=-30;
        currentobj.frame=0;
        //currentobj.scale.setTo(1,1);
      }

      if(currentobj==bowl && bowl.frameName != "bowl0022.png"){
        console.log(bowl.frameName);
        arrow.x=1300;
        arrow.y=725;//325
        arrow_y=725;
      }
      if(currentobj==bowl && bowl.frameName == "bowl0022.png"){
        console.log(bowl.frameName+"///");
        arrow.x=140;
        arrow.y=485;
        arrow_y=485;
        arrow.visible=true;
      }
      if(currentobj==magnet1||currentobj==magnet2){
        arrow.x=1170;
        arrow.y=740;//340
        arrow_y=740;
      }
      if(currentobj==spoon1){
        arrow.x=1470;
        arrow.y=750;//350
        arrow_y=750;
        arrow.visible=true;
      }
      if(currentobj==spoon2){
        arrow.x=1200;
        arrow.y=750;//350
        arrow_y=750;
        arrow.visible=true;
      }
      if(currentobj==spoon3){
        arrow.x=920;
        arrow.y=770;//370
        arrow_y=770;
        arrow.visible=true;
        spoon3.angle-20;
      }
      if(currentobj==spoon4){
        arrow.x=630;
        arrow.y=770;//370
        arrow_y=770;
        arrow.visible=true;
      }
      if(currentobj==glass_rod || currentobj==glass_rod1){
        arrow.x=1670;//1520
        arrow.y=800;//570
        arrow_y=800;
        currentobj.angle=90;
        arrow.visible=true;
      }
      if(currentobj==spoon5){
        arrow.x=980;
        arrow.y=770;//370
        arrow_y=770;
        arrow.visible=true;
      }
      if(currentobj==droper1 || currentobj==droper2){
        arrow.x=1830;
        arrow.y=470;
        arrow_y=470;
        arrow.visible=true;
      }
      if(currentobj==droper3 || currentobj==droper4){
        arrow.x=1650;
        arrow.y=470;
        arrow_y=470;
        arrow.visible=true;
      }
      if(currentobj==texttube3){
        arrow.x=1270;
        arrow.y=140;
        arrow_y=140;
        arrow.visible=true;
      }
      if(currentobj==texttube4){
        arrow.x=1390;
        arrow.y=140;
        arrow_y=140;
        arrow.visible=true;
      }else if(stick!=null && currentobj==stick){
        arrow.x=800;
        arrow.y=500;
        arrow_y=500;
        arrow.visible=true;
      }
      arrow.visible=true;
      currentobj=null;
    }
    
  },
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Simulation_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
    this.state.start("Title",true,false,ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
   voice2.destroy();
  voice3.destroy();
this.postData();

  //voice5.destroy();
  //window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

Hot:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Simulation_hot1",true,false,ip);

 },
toSimulation:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);

 },
graph:function(){

 },

LC:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);


 },


 nextsim:function()
 {


 },
 buttonState:function(state1,state2){

 state1.visible = false;
 state2.visible = true;
 },

buttonState2:function(state2,state1){


 state1.visible = true;
 state2.visible = false;
 }


}
