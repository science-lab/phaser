var experiment_4 = function(game){

var inst;



var delay;
var incr;
var currentobj;
var arrow_y;
var mix_count;
var spoon3;
var bowl;
var interval;
var heatFlag;
var heatFlag2;
var magnetFlag;
var glass_rod1;
var content1;
var magnet1;
var magnet2;
var spoon4;
var spoon5;
var shakeFlag1;
var shakeFlag2;
var shakingFlag1;
var shakingFlag2;
var shakingCount1;
var shakingCount2;
var mixFlag;
var stickFlag;
var stick;
var texttube3;
var texttube4;
}


experiment_4.prototype = {
  	create: function(){
      fontstyle_1={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_2={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_3={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_4={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_5={ font: "40px Segoe UI", fill: "#000000", align: "left" };
fontstyle_6={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
fontstyle_7={ font: "40px Segoe UI", fill: "#FFA500", align: "left" };
hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');
interval=0;
heatFlag=false;
heatFlag2=false;
magnetFlag=false;
magnetFlag2=false;
currentobj=null;
glass_rod1=null;
magnet1=null;
magnet2=null;
spoon4=null;
spoon5=null;
shakeFlag1=false;
shakeFlag2=false;
shakingFlag1=false;
shakingFlag2=false;
shakingCount1=0;
shakingCount2=0;
mixFlag=false;
hclFlag=false;
compHeatFlag=false;
compHeatFlag2=false;
mixHeatFlag=false;
stickFlag=false;
stick=null;
texttube3=null;
texttube4=null;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/


spoon3=null;
bowl=null;
mix_count=0;

voice=this.game.add.audio("step0",1);
//voice.play();

this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  nextSound3:function(){
    //voice3.play();
  },
  addItems:function(){
    
      
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    dia_box=this.game.add.sprite(40,20, 'dialogue_box1');
    dia_box.scale.setTo(.8,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(60,50,dia_text,fontstyle_2);
    collider=this.game.add.sprite(1150,770, 'collider');//y-320
    dialog.y=60;
    this.game.physics.arcade.enable(collider);
    collider.scale.setTo(2,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.0;

    burner=this.game.add.sprite(0,575, 'compounds', 'burner0001.png');
    burner.animations.add('anim',['burner0001.png',
            'burner0002.png',
             ], 10, true, false);
    burner.scale.set(1.25);

    burner_Switch=this.game.add.sprite(130,820, 'collider');
    burner_Switch.alpha=0;
    burner_Switch.state="switch_deactivate";
    burner_Switch.inputEnabled = true;
    burner_Switch.input.useHandCursor = true;
    burner_Switch.events.onInputDown.add(this.burnerOperate, this);         
    
    //plate=this.game.add.sprite(800,720, 'compounds', 'plate.png');//y-320
    plate1=this.game.add.sprite(500,720, 'compounds', 'plate.png');//y-320
    plate1.frameName="black_mass.png";
    plate1.y=850;//450
    compound_lable=this.game.add.sprite(530,910,'compounds', 'compound_lable.png');//500
    plate=this.game.add.sprite(800,720, 'compounds', 'plate.png');//y-320
    mixture=this.game.add.sprite(790,840, 'compounds', 'mixture.png');//y-440
    //mixture.visible=false;
    
    
    mix=this.game.add.sprite(780,570, 'mixing', 'mix0001.png');//y-170
    mix.scale.setTo(.61,.61);
    mix.visible=false;
    mix.animations.add('anim',[
            'mix0002.png',
            'mix0003.png',
            'mix0004.png',
            'mix0005.png',
            'mix0006.png',
            'mix0007.png',
            'mix0008.png',
            
             ], 10, true, false);

    //ironFillings=this.game.add.sprite(1320,850, 'compounds', 'iron.png');//y-450, x-1370
    mixture_label=this.game.add.sprite(825,910, 'compounds', 'mixture_label.png');//y-510
    /**/

    spoon1_1=this.game.add.sprite(950,870, 'Spoon_M');//y-470
    spoon1_1.angle=-20;   
    spoon1_1.anchor.setTo(.5,.5);        
    spoon1_1.scale.setTo(.5,.8);
    spoon2_1=this.game.add.sprite(650,870, 'Spoon_M');//y-470
    spoon2_1.angle=-20;   
    spoon2_1.anchor.setTo(.5,.5);        
    spoon2_1.scale.setTo(.5,.8);
    

    bowl1=this.game.add.sprite(1200,950, 'heating', 'bowl0001.png');//y-550
    bowl1.anchor.setTo(.5,1);  
    bowl1.xp=1200;
    bowl1.yp=950;
    bowl1.state1="beforeHeat";
    bowl1.events.onDragStart.add(function() {this.gameC(bowl1)}, this);
    bowl1.events.onDragStop.add(function() {this.gameC1(bowl1)}, this);
    bowl1.animations.add('anim',[
      'bowl0003.png',
      'bowl0003.png',
      
      
       ], .1, true, false);
    bowl1.animations.add('anim2',[
      'bowl0015.png',
      'bowl0016.png',
      'bowl0017.png',
      'bowl0018.png',
      'bowl0019.png',
      'bowl0020.png',
      'bowl0021.png',
      'bowl00226.png',
      
       ], 10, true, false);  

      bowl4=this.game.add.sprite(1195,930, 'Bowl_Compound');//y-550
      bowl4.anchor.setTo(.5,1);  
      bowl4.visible=false;
       


    bowl2=this.game.add.sprite(1450,950, 'heating', 'bowl0001.png');//y-550
    bowl2.events.onDragStart.add(function() {this.gameC(bowl2)}, this);
    bowl2.events.onDragStop.add(function() {this.gameC1(bowl2)}, this);
    bowl2.anchor.setTo(.5,1);  
    bowl2.state1="beforeHeat"; 
    bowl2.animations.add('anim2',[
      'bowl0015.png',
      'bowl0016.png',
      'bowl0017.png',
      'bowl0018.png',
      'bowl0019.png',
      'bowl0020.png',
      'bowl0021.png',
      'bowl00226.png',
      
       ], 10, true, false);   

           
    bowl3=this.game.add.sprite(1445,930, 'Bowl_Compound');//y-550
    bowl3.anchor.setTo(.5,1);  
    bowl3.visible=false;
    bowl3.xp=1445;
    bowl3.yp=950; 
    bowl3.state1="beforeHeat";   
    bowl3.events.onDragStart.add(function() {this.gameC(bowl3)}, this);
    bowl3.events.onDragStop.add(function() {this.gameC1(bowl3)}, this);

    
    spoon1=this.game.add.sprite(950,870, 'spoon', 'spoon_compound.png');//y-470
    spoon1.angle=-20;
    spoon1.scale.setTo(.5,.8);
    spoon1.xp=950;
    spoon1.nam="spoon";
    spoon1.yp=870;//470
    spoon1.events.onDragStart.add(function() {this.gameC(spoon1)}, this);
    spoon1.events.onDragStop.add(function() {this.gameC1(spoon1)}, this);
    spoon1.anchor.setTo(.5,.5);
    spoon1.alpha=0;
       
    
    spoon2=this.game.add.sprite(650,870, 'spoon', 'spoon_compound.png');//y-470
    spoon2.angle=-20;
    spoon2.xp=650;
    spoon2.yp=870;//470
    spoon2.nam="spoon";
    spoon2.scale.setTo(1,2);
    spoon2.anchor.setTo(.5,.5);
    spoon2.events.onDragStart.add(function() {this.gameC(spoon2)}, this);
    spoon2.events.onDragStop.add(function() {this.gameC1(spoon2)}, this);
    spoon2.scale.setTo(.5,.8);
    spoon2.anchor.setTo(.5,.5);
    spoon2.alpha=0;
    
    Fog=this.game.add.sprite(100,350, 'Fog', 'Fog0001.png');//y-170
    Fog.scale.setTo(1,.6);
    Fog.visible=false;
    Fog.animations.add('anim',['Fog0001.png','Fog0002.png','Fog0003.png','Fog0004.png','Fog0005.png','Fog0006.png','Fog0007.png','Fog0008.png','Fog0009.png','Fog0010.png','Fog0011.png','Fog0012.png','Fog0013.png','Fog0014.png','Fog0015.png','Fog0016.png','Fog0017.png','Fog0018.png','Fog0019.png','Fog0020.png'], 5, true, false);
      
    
    /////////////////////////////////////////////////
    
    
    //burner=this.game.add.sprite(30,600, 'compounds', 'burner0001.png');

    
    
    arrow=this.game.add.sprite(1200,750, 'arrow');//y-350
    arrow.angle=90;
    arrow_y=750;//y-350
    arrow.visible=false;
    //mixture_label.scale.setTo(1.5,1.5);
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_1",1);
    voice.play();
    dialog.text="Add a small amount of mixture into a china dish.";
    spoon1.inputEnabled = true;
    spoon1.input.useHandCursor = true;
    //magnet1.events.onInputDown.add(this.ClickOnSpoon, this);
    spoon1.input.enableDrag(true);
    this.game.physics.arcade.enable(spoon1);
    arrow.x=950;
    arrow.y=740;//340
    arrow_y=740;
    arrow.visible=true;

  },
  burnerOperate:function(){
    if(burner_Switch.state=="switch_off"){
      burner_Switch.state="switch_deactivate";
      ////////////////////////////////////////
      bowl1.animations.play('anim');
        Fog.visible=true;
        Fog.animations.play('anim');
        heatFlag=true;
        incr=0;
        burner.frameName = "burner0002.png";
      ////////////////////////////////////////
      
        arrow.visible=false;
    }else if(burner_Switch.state=="switch_off1"){
      burner_Switch.state="switch_deactivate";
      ////////////////////////////////////////
      arrow.visible=false;
      compHeatFlag=true;
      incr=0;
      burner.frameName = "burner0002.png";
      ////////////////////////////////////////
      
    }else if(burner_Switch.state=="switch_on"){

      collider.x=1150;
        collider.y=750;
        burner.frameName = "burner0001.png";
        bowl1.state1="afterHeat";
        arrow.visible=true;
        arrow.x=180;
          arrow.y=500;//300
          arrow_y=500;
        bowl1.inputEnabled = true;
        bowl1.input.useHandCursor = true;
        bowl1.input.enableDrag(true);
        this.game.physics.arcade.enable(bowl1);
        voice.destroy(true);
        voice=this.game.add.audio("step4_5",1);
        voice.play();
        dialog.text="Replace the china dish from the Bunsen burner.";
          
    }else if(burner_Switch.state=="switch_on1"){

      burner.frameName = "burner0001.png";
        bowl3.state1="afterHeat";
        arrow.visible=true;
        arrow.x=180;
          arrow.y=500;//300
          arrow_y=500;
        bowl3.inputEnabled = true;//bowl2
        bowl3.input.useHandCursor = true;
        bowl3.input.enableDrag(true);
        this.game.physics.arcade.enable(bowl2);
        voice.destroy(true);
        voice=this.game.add.audio("step4_5",1);
        voice.play();
        dialog.text="Replace the china dish from the Bunsen burner.";
          
    }
    
  },
  ClickOnSpoon:function(){

  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    arrow.visible=false;
    if(obj==spoon1){
      obj.frame=3;
      obj.alpha=1;
      spoon1_1.visible=false;
      arrow.x=1200;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }else if(obj==spoon2){
      obj.frame=1;
      obj.alpha=1;
      spoon2_1.visible=false;
      arrow.x=1450;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }else if(obj==bowl1){
      if(bowl1.state1=="beforeHeat"){
        arrow.x=180;
        arrow.y=500;//300
        arrow_y=500;
      }else{
        arrow.x=1200;
        arrow.y=700;//300
        arrow_y=700;
      }
      arrow.visible=true;
    }else if(obj==bowl3){
      if(bowl3.state1=="beforeHeat"){
        arrow.x=180;
        arrow.y=500;//300
        arrow_y=500;
        arrow.visible=true;
      }else{
        arrow.x=1450;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
      }
      
    }else if(obj==bowl3 || obj==bowl2){
      console.log('.............'+bowl3.state1);
      arrow.x=1450;
      arrow.y=700;//300
      arrow_y=700;
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    
   
    if(currentobj==spoon1){
      bowl1.frameName = "bowl0003.png";
      arrow.x=1170;
      arrow.y=740;//340
      arrow_y=740;
      arrow.visible=false;
      spoon1.visible=false;
      spoon1_1.visible=true;
      collider.reset(1400,720);//320
      
      voice.destroy(true);
      voice=this.game.add.audio("step4_2",1);
      voice.play();
      dialog.text="Add a small amount of compound into another china dish.";
      spoon2.inputEnabled = true;
      spoon2.input.useHandCursor = true;
      spoon2.input.enableDrag(true);
      this.game.physics.arcade.enable(spoon2);
      arrow.x=650;
      arrow.y=740;//340
      arrow_y=740;
      arrow.visible=true;
    }else if(currentobj==spoon2){
      currentobj.visible=false;
      spoon2_1.visible=true;
      bowl2.visible=false;
      bowl3.visible=true;
      interval=0;
      arrow.visible=false;
      collider.x=100;
      collider.y=500;
      bowl1.inputEnabled = true;
      bowl1.input.useHandCursor = true;
      bowl1.input.enableDrag(true);
      this.game.physics.arcade.enable(bowl1);
      arrow.x=1200;
      arrow.y=740;//340
      arrow_y=740;
      arrow.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_3",1);
      voice.play();
      dialog.text="Heat the mixture in the china dish.";
    }else if(currentobj==bowl1){
      arrow.visible=false;
      if(bowl1.state1=="beforeHeat"){
        bowl1.x=170;
        bowl1.y=700;
        bowl1.xp=170;
        bowl1.yp=700;
        ///////////////////////////////
        arrow.x=burner_Switch.x+40;
        arrow.y=burner_Switch.y-80;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_15",1);
        voice.play();
        dialog.text="Switch on the burner.";
        burner_Switch.state="switch_off";
        ////////////////////////////////
        
        // voice.destroy(true);
        // voice=this.game.add.audio("step_1",1);
        // //voice.play();
        // dialog.text="Observe the changes in the china dish";
      }else{
        bowl1.x=1200;
        bowl1.y=950;
        bowl1.visible=false;
        bowl4.visible=true;
        arrow.x=1450;
        arrow.y=740;//340
        arrow_y=740;
        arrow.visible=true;
        collider.x=100;
        collider.y=500;
        bowl3.inputEnabled = true;
        bowl3.input.useHandCursor = true;
        bowl3.input.enableDrag(true);
        this.game.physics.arcade.enable(bowl3);
        voice.destroy(true);
        voice=this.game.add.audio("step4_6",1);
        voice.play();
        dialog.text="Heat the compound in the china dish.";
      }
    }else if(currentobj==bowl3){
      //currentobj.visible=false;
      console.log(bowl3.state1);
      if(bowl3.state1=="beforeHeat"){
        bowl3.x=170;
        bowl3.y=700;
        bowl3.x=170;
        bowl3.y=680;
        //Fog.animations.play('anim');
        //Fog.visible=true;
        ///////////////////////////////
        arrow.x=burner_Switch.x+40;
        arrow.y=burner_Switch.y-80;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_15",1);
        voice.play();
        dialog.text="Switch on the burner.";
        burner_Switch.state="switch_off1";
        ////////////////////////////////
        
      }else{
        bowl3.x=1445;
        bowl3.y=930;
        bowl3.xp=1445;
        bowl3.yp=930;
        bowl3.visible=true;
        arrow.visible=false;
        voice.destroy(true);
        voice=this.game.add.audio("step4_8",1);
        voice.play();
        dialog.text="Click on the next button to see the observations.";
        play.visible=true;
      }
      // voice.destroy(true);
      // voice=this.game.add.audio("step_1",1);
      // //voice.play();
       //dialog.text="Observe the changes in the china dish";
    }else if(currentobj==bowl2){
      bowl3.x=1445;
      bowl3.y=930;
      bowl2.visible=false;
      bowl3.visible=true;
      arrow.visible=false;
      voice.destroy(true);
      voice=this.game.add.audio("step4_8",1);
      voice.play();
      dialog.text="Click on the next button to see the observations.";
      play.visible=true;
    }
    
  },
 
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	addList:function(){
    

  },
  ClickOnPlay:function(){

    
  },
  
  ClickOnSlow2:function(){

  },
  


  hotsec:function()
  {

    //voice41.destroy();
    //voice5.play();

  },
  
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(heatFlag){
      incr++;
      if(incr>=160){
        heatFlag=false;
        bowl1.animations.stop('anim');
        bowl1.animations.play('anim2');
        heatFlag2=true;
        incr=0;
        voice.destroy(true);
        voice=this.game.add.audio("step4_4",1);
        voice.play();
        dialog.text="Observe that a black mass is formed.";
      }
    }else if(heatFlag2){
      incr++;
      if(incr>=260){
        heatFlag2=false;
        bowl1.animations.stop('anim2');
        Fog.animations.stop('anim');
        Fog.visible=false;
        //bowl1.animations.play('anim2');
        //////////////////////////////////////
        voice.destroy(true);
          voice=this.game.add.audio("step2_6",1);
          voice.play();
          //dialog.text="Pour the compound into the empty plate";
          dialog.text="Switch off the burner.";//Pour the black mass into the mortar
          burner_Switch.state="switch_on";
          arrow.x=burner_Switch.x+40;
          arrow.y=burner_Switch.y-80;
          arrow_y=arrow.y;
          arrow.visible=true;
        /////////////////////////////////

        
      }
    }
    if(compHeatFlag){
      incr++;
      if(incr>=160){
        compHeatFlag=false;
        //bowl3.visible=false;
        bowl2.x=170;
        bowl2.y=700;
        bowl2.xp=170;
        bowl2.yp=700;
        //bowl2.animations.play('anim2');
        //bowl2.visible=true;
        incr=0;
        compHeatFlag2=true;
        voice.destroy(true);
        voice=this.game.add.audio("step4_7",1);
        voice.play();
        dialog.text="Observe that there are no changes in the compound.";
      }
    }
    if(compHeatFlag2){
      incr++;
      if(incr>=200){
        compHeatFlag2=false;
        //bowl2.animations.stop('anim2');
        //Fog.animations.stop('anim');
        //Fog.visible=false;
        //bowl1.animations.play('anim2');
        collider.x=1400;
        collider.y=750;
        //////////////////////////////////////
        voice.destroy(true);
          voice=this.game.add.audio("step2_6",1);
          voice.play();
          //dialog.text="Pour the compound into the empty plate";
          dialog.text="Switch off the burner.";//Pour the black mass into the mortar
          burner_Switch.state="switch_on1";
          arrow.x=burner_Switch.x+40;
          arrow.y=burner_Switch.y-80;
          arrow_y=arrow.y;
          arrow.visible=true;
        /////////////////////////////////
        

      }
    }
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    /////////////////////////////
         
	},
  
  // 
  
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==spoon1){
        //currentobj.frame=0;
        currentobj.alpha=0;
        spoon1_1.visible=true;
        arrow.x=950;
        arrow.y=740;//340
        arrow_y=740;
      }
      if(currentobj==spoon2){
        //currentobj.frame=0;
        //currentobj.frame=0;
        currentobj.alpha=0;
        spoon2_1.visible=true;
        arrow.x=650;
        arrow.y=740;//340
        arrow_y=740;
      }
      if(currentobj==bowl1){
        if(bowl1.state1=="beforeHeat"){
          arrow.x=1200;
          arrow.y=740;//340
          arrow_y=740;
        }else{
          arrow.x=180;
          arrow.y=500;//300
          arrow_y=500;
        }
      }
      if(currentobj==bowl3 ){
        if(bowl3.state1=="beforeHeat"){
          arrow.x=1450;
          arrow.y=740;//340
          arrow_y=740;
        }else{
          arrow.x=180;
          arrow.y=500;//340
          arrow_y=500;
        }
      }if(currentobj==bowl2 ){
        arrow.x=180;
        arrow.y=500;//340
        arrow_y=500;
      }

      arrow.visible=true;
      currentobj=null;
    }
    
  },
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_4",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
    this.state.start("Aim",true,false,ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

Hot:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Simulation_hot1",true,false,ip);

 },
toSimulation:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);

 },
graph:function(){

 },

LC:function(){

  voice.destroy();
voice2.destroy();
  voice3.destroy();
  //voice5.destroy();
  this.game.state.start("Summary",true,false,ip);


 },


 nextsim:function()
 {


 },
 buttonState:function(state1,state2){

 state1.visible = false;
 state2.visible = true;
 },

buttonState2:function(state2,state1){


 state1.visible = true;
 state2.visible = false;
 }


}
