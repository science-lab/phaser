var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  var currentScene;
  var previousScene;
  var total_scene;
  
  }
  
  materials.prototype ={
  
  init: function(ipadrs) 
  {
    ip = ipadrs;
    

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1600,870,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1600,870,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(300,870,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(300,870,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toTheory, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
       
        total_scene=13;
        this.show_combination_materials();
       

        
      },
      
      show_combination_materials:function(){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_img_0.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                
              }
              mat_img_1 = this.game.add.sprite(370,550,'carbonMix','mix_compound0001.png');
              mat_img_1.anchor.setTo(.5,.5);
              //mat_img_1.scale.setTo(.8,.8);
              mat_name_1=this.game.add.text(550,370,"Test tube:",headfontStyle);
              mat_content_1="Test tube is a common piece of laboratory glassware used to, \nhold, mix, or heat small quantities of solid or liquid chemicals.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
              
              break;
          case 2:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'carbonMix','hold1.png');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_0=this.game.add.sprite(370,550,'carbonMix','hold2.png');
              mat_img_0.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(550,370,"Test tube stand:",headfontStyle);
              mat_content_1="Test tube racks are laboratory equipment used to hold upright\nmultiple test tubes at the same time. They are most commonly\nused when various different solutions are needed to work with\nsimultaneously, for safety reasons, for safe storage of test tubes,\nand to ease the transport of multiple tubes.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
              
          break;
          case 3:
            if(previousScene==2){
              mat_img_0.destroy(true);
              
            }
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550, 'Spoon_M');
            mat_img_1.scale.setTo(.8,.8);
            mat_img_1.anchor.setTo(.5,.5);
            mat_name_1=this.game.add.text(550,370,"Spatula:",headfontStyle);
            mat_content_1="Spatula is a small stainless steel utensil, used for scraping,\ntransferring, or applying powders and paste-like chemicals or\ntreatments.";
            mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 4:
                
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
            

              mat_img_1 = this.game.add.sprite(370,550, 'compounds', 'droper1.png');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(1.2,1);
              //mat_img_1.angle=-15;
              mat_name_1=this.game.add.text(550,370,"Dropper:",headfontStyle);
              mat_content_1="A dropper, also known as a Pasteur pipette, or dropper, is a \ndevice used to transfer small quantities of liquids. They are used \nin the laboratory.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          
          break;
          case 5:
             
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
            
                mat_img_1=this.game.add.sprite(370,450, 'heating', 'bowl0001.png');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(1.2,1);
              //mat_img_1.angle=-15;
              mat_name_1=this.game.add.text(550,370,"China dish:",headfontStyle);
              mat_content_1="The China dish is a porcelain plate used for experiments in the \nscience laboratory.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          
          break;
          case 6:
            
                
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
            
                mat_img_1=this.game.add.sprite(370,500, 'compounds', 'plate.png');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(1.2,1);
              //mat_img_1.angle=-15;
              mat_name_1=this.game.add.text(550,370,"Watch-glass:",headfontStyle);
              mat_content_1="Watch glasses are circular, slightly-concave pieces of glass that \nare used by chemists to evaporate liquids and cover beakers \nduring sample preparation and also used to hold solids during \nweighing.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          
          break;
          case 7:
            
            mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,450, 'compounds', 'glass_rod.png');
            //mat_img_1.scale.setTo(1.2,1.2);
            mat_name_1=this.game.add.text(550,370,"Glass Rod:",headfontStyle);
            mat_content_1="A Glass rod, stirring rod or stir rod is a piece of laboratory\nequipment used to mix chemicals and liquids for laboratory\npurposes.";
            mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          
          break;  
          case 8:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(250,400, 'compounds', 'testtube_lamp_off.png');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Bunsen burner:",headfontStyle);
              mat_content_1="A Bunsen burner, named after Robert Bunsen, is a kind of gas \nburner used as laboratory equipment; it produces a single open \ngas flame and is used for heating, sterilization, and combustion.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);

              
          break;
          case 9:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(270,450,'tripod');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Tripod stand:",headfontStyle);
              mat_content_1="A laboratory tripod is a piece of three-legged equipment\ncommonly used to conduct experiments in laboratories.It is used \nas a platform to hold and support glassware, such as beakers and \nflasks, during experiments and when the glassware is not in use.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);

              
          break;
          case 10:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(270,450, 'compounds', 'magnet.png');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Magnet:",headfontStyle);
              mat_content_1="A magnet is a material or object that produces a magnetic field.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);

              
          break;
          
          case 11:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(270,450, 'sulphur');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Sulphur powder:",headfontStyle);
              mat_content_1="Sulphur is a non-metallic chemical element. Pure sulphur is a \ntasteless, odourless, brittle solid and it is pale yellow in colour.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);

              
          break;
          case 12:
            //mat_img_0.destroy(true);
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1=this.game.add.sprite(270,450, 'iron_filings');
            //mat_img_1.scale.set(.8);
            mat_name_1=this.game.add.text(550,370,"Iron filings:",headfontStyle);
            mat_content_1="Iron filings are very small pieces of iron that look like a light \npowder.";
            mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 13:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(270,450, 'compounds', 'disulphide.png');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Carbon disulphide:",headfontStyle);
              mat_content_1="Carbon disulphide, is a colorless volatile liquid with the \nformula CS\u2082";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);

              
          break;

        }
        //Aluminium sulfate is a chemical compound with the formula Al2(SO4)3. It is soluble in water and is mainly used as a coagulating agent in the purification of drinking water and wastewater treatment plants, and also in paper manufacturing.
      //mat_content_1="A dropper, also known as a Pasteur pipette, or \ndropper, is a device used to transfer small quantities \nof liquids. They are used in the laboratory.";
      //Zinc granules are the solid crystals of zinc.
      //Iron filings are very small pieces of iron that look like a light powder.
      //Copper is a chemical element with the symbol Cu. It is a soft, malleable, and ductile metal with very high thermal and electrical conductivity.
     //Aluminium is a chemical element with the symbol Al. It is silvery-white and lightweight metal
    },
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==total_scene){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
      toTheory:function()
        {
        voice.destroy();
        this.state.start("Theory", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  