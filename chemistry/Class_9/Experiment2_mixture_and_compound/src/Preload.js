var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        this.game.load.atlasJSONHash('compounds', 'assets/compounds/compounds.png', 'assets/compounds/compounds.json');
        this.game.load.atlasJSONHash('spoon', 'assets/compounds/spoon.png', 'assets/compounds/spoon.json');
        this.game.load.atlasJSONHash('mixing', 'assets/compounds/mixing.png', 'assets/compounds/mixing.json');
        this.game.load.atlasJSONHash('heating', 'assets/compounds/heating.png', 'assets/compounds/heating.json');
        this.game.load.atlasJSONHash('carbonMix', 'assets/compounds/carbon.png', 'assets/compounds/carbon.json');
        this.game.load.atlasJSONHash('Fog', 'assets/compounds/Fog.png', 'assets/compounds/Fog.json');
        //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
///////////////////////////////////////////////////////////////////////////////////////////////Grander_Front
        this.game.load.image('Grander_Front', 'assets/compounds/Grander_Front.png');
        this.game.load.image('Grander_back', 'assets/compounds/Grander_back.png');
        this.game.load.image('Grabder_rod', 'assets/compounds/Grabder_rod.png');
        this.game.load.image('Spoon_M', 'assets/compounds/Spoon_M.png');
        this.game.load.image('tripod', 'assets/compounds/tripod.png');
        this.game.load.image('iron_filings', 'assets/compounds/iron_filings.png');
        this.game.load.image('sulphur', 'assets/compounds/sulphur.png');
        this.game.load.image('Bowl_Compound', 'assets/compounds/Bowl_Compound.png');
        this.game.load.atlasJSONHash('Bowl_to_grander', 'assets/compounds/Bowl_to_grander.png', 'assets/compounds/Bowl_to_grander.json');
        this.game.load.atlasJSONHash('Grander_animation', 'assets/compounds/Grander_animation.png', 'assets/compounds/Grander_animation.json');
        this.game.load.atlasJSONHash('Grander_to_Plate', 'assets/compounds/Grander_to_Plate.png', 'assets/compounds/Grander_to_Plate.json');
        this.game.load.atlasJSONHash('Plate', 'assets/compounds/Plate.png', 'assets/compounds/Plate.json');
        
      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');  
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Procedure_4','assets/audio/Procedure_4.mp3');
      this.game.load.audio('Procedure_5','assets/audio/Procedure_5.mp3');  
      this.game.load.audio('Procedure_6','assets/audio/Procedure_6.mp3');
      

      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('step0','assets/audio/step0.mp3');
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');
      this.game.load.audio('step1_7','assets/audio/step1_7.mp3');
      this.game.load.audio('step1_8','assets/audio/step1_8.mp3');
      this.game.load.audio('step1_9','assets/audio/step1_9.mp3');
      this.game.load.audio('step1_10','assets/audio/step1_10.mp3');
      this.game.load.audio('step1_11','assets/audio/step1_11.mp3');
      this.game.load.audio('step1_12','assets/audio/step1_12.mp3');
      this.game.load.audio('step1_13','assets/audio/step1_13.mp3');
      this.game.load.audio('step1_14','assets/audio/step1_14.mp3');
      this.game.load.audio('step1_15','assets/audio/step1_15.mp3');
      this.game.load.audio('step1_16','assets/audio/step1_16.mp3');

      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step2_5','assets/audio/step2_5.mp3');
      this.game.load.audio('step2_6','assets/audio/step2_6.mp3');


      this.game.load.audio('step3_1','assets/audio/step3_1.mp3');
      this.game.load.audio('step3_2','assets/audio/step3_2.mp3');
      this.game.load.audio('step3_3','assets/audio/step3_3.mp3');
      this.game.load.audio('step3_4','assets/audio/step3_4.mp3');
      this.game.load.audio('step3_5','assets/audio/step3_5.mp3');
      this.game.load.audio('step3_6','assets/audio/step3_6.mp3');
      this.game.load.audio('step3_7','assets/audio/step3_7.mp3');
      this.game.load.audio('step3_8','assets/audio/step3_8.mp3');
      
      
      
      this.game.load.audio('step4_1','assets/audio/step4_1.mp3');
      this.game.load.audio('step4_2','assets/audio/step4_2.mp3');
      this.game.load.audio('step4_3','assets/audio/step4_3.mp3');
      this.game.load.audio('step4_4','assets/audio/step4_4.mp3');
      this.game.load.audio('step4_5','assets/audio/step4_5.mp3');
      this.game.load.audio('step4_6','assets/audio/step4_6.mp3');
      this.game.load.audio('step4_7','assets/audio/step4_7.mp3');
      this.game.load.audio('step4_8','assets/audio/step4_8.mp3');
      
      



      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_4");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

