var experiment_3 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;
// var voice1;
// var voice2;
// var voice3;
// var voice4;
//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var temperatureFlag;
var dropStep;
var dropperClickCount;
var droperFlag;
var circle1;
var circle2;
var circle3;
var circle4;
var Aluminium_Sulphate_Text;
var Copper_Sulphate_Text
var Ferrous_Sulphate_Text;
var Zing_Sulphate_Text;
}

experiment_3.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);
//  voice1=this.game.add.audio("step_0",1);
// voice2=this.game.add.audio("step_0",1);
// voice3=this.game.add.audio("step_0",1);
// voice4=this.game.add.audio("step_0",1);
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 dropStep=1;
 dropperClickCount=0;
 droperFlag=false;
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  this.addItems();

  /*base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_step_1="1. Take 2 grams of ferrous sulphate crystals in a boiling tube. ";
  procedure_step_2="2. Heat the boiling tube on the bunsen burner. ";
  procedure_step_3="3. Observe the colour of the residue.";
  procedure_text=this.game.add.text(250,250,"Procedure:",headfontStyle);
  procedure_step_text_1=this.game.add.text(300,350,procedure_step_1,fontStyle);
  procedure_step_text_2=this.game.add.text(300,430,procedure_step_2,fontStyle);
  procedure_step_text_3=this.game.add.text(300,510,procedure_step_3,fontStyle);*/



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toObservations, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
        rack_Back=this.game.add.sprite(200,600, 'rack_Back');
        rack_Back.scale.set(.7);
        Testube_Back1=this.game.add.sprite(327,495, 'Testube_Back');
        Testube_Back1.scale.setTo(.85,.85);
        Testube_Front1=this.game.add.sprite(327,495, 'Testube_Front');
        Testube_Front1.scale.setTo(.85,.85);
        Aluminium_Sulphate_Solution_in_Testube=this.game.add.sprite(327,495, 'Aluminium_Sulphate_Solution_in_Testube',"Aluminium_Sulphate_Solution_in_Testube0001.png");
        Aluminium_Sulphate_Solution_in_Testube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,16,17,18,19,20,21,22,23,24,25],10, false, true);
        Aluminium_Sulphate_Solution_in_Testube.scale.setTo(.85,.85);
        Aluminium_Sulphate_Solution_in_Testube.visible=false;
        /////
        Copper_in_Aluminiumr_Sulphate=this.game.add.sprite(327,495, 'Copper_in_Aluminiumr_Sulphate',"Copper_in_Aluminiumr_Sulphate0001.png");
        Copper_in_Aluminiumr_Sulphate.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
        Copper_in_Aluminiumr_Sulphate.scale.setTo(.85,.85);
        Copper_in_Aluminiumr_Sulphate.visible=false;


        A_Blue=this.game.add.sprite(365,560, 'A_Blue');
        A_Blue.scale.setTo(.6,.6);


        
        ////////////////////////////////////////////////////////////////
        Testube_Back2=this.game.add.sprite(432,495, 'Testube_Back');
        Testube_Back2.scale.setTo(.85,.85);
        Testube_Front2=this.game.add.sprite(432,495, 'Testube_Front');
        Testube_Front2.scale.setTo(.85,.85);
        Copper_Sulphate_Solution_in_Testube=this.game.add.sprite(432,495, 'Copper_Sulphate_Solution_in_Testube',"Copper_Sulphate_Solution_in_Testube0001.png");
        Copper_Sulphate_Solution_in_Testube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,16,17,18,19,20,21,22,23,24,25],10, false, true);
        Copper_Sulphate_Solution_in_Testube.scale.setTo(.85,.85);
        Copper_Sulphate_Solution_in_Testube.visible=false;
        /////////
        Copper_in_Copper_Sulphate=this.game.add.sprite(432,495, 'Copper_in_Copper_Sulphate',"Copper_in_Copper_Sulphate0001.png");
        Copper_in_Copper_Sulphate.animations.add('anim',[3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
        //Copper_in_Copper_Sulphate.animations.add('anim2',[16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45],2, false, true);
        Copper_in_Copper_Sulphate.scale.setTo(.85,.85);
        Copper_in_Copper_Sulphate.visible=false;
        
        B_Blue=this.game.add.sprite(470,560, 'B_Blue');
        B_Blue.scale.setTo(.6,.6);
        /////////////////////////////////////////////////////////////////////////////////////////
        Testube_Back3=this.game.add.sprite(537,495, 'Testube_Back');
        Testube_Back3.scale.setTo(.85,.85);
        Testube_Front3=this.game.add.sprite(537,495, 'Testube_Front');
        Testube_Front3.scale.setTo(.85,.85);
        Ferrous_Sulphate_Solution_in_Testube=this.game.add.sprite(537,495, 'Ferrous_Sulphate_Solution_in_Testube',"Ferrous_Sulphate_Solution_in_Testube00010001.png");
        Ferrous_Sulphate_Solution_in_Testube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,16,17,18,19,20,21,22,23,24,25],10, false, true);
        Ferrous_Sulphate_Solution_in_Testube.scale.setTo(.85,.85);
        Ferrous_Sulphate_Solution_in_Testube.visible=false;

        Copper_in_Ferrousr_Sulphate=this.game.add.sprite(537,495, 'Copper_in_Ferrousr_Sulphate',"Copper_in_Ferrousr_Sulphate0001.png");
        Copper_in_Ferrousr_Sulphate.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
        //Copper_in_Ferrousr_Sulphate.animations.add('anim2',[16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45],2, false, true);
        Copper_in_Ferrousr_Sulphate.scale.setTo(.85,.85);
        Copper_in_Ferrousr_Sulphate.visible=false;
        C_Blue=this.game.add.sprite(575,560, 'C_Blue');
        C_Blue.scale.setTo(.6,.6);
        //////////////////////////////////////////////////////////////////////////////////
        Testube_Back4=this.game.add.sprite(642,495, 'Testube_Back');
        Testube_Back4.scale.setTo(.85,.85);
        Testube_Front4=this.game.add.sprite(642,495, 'Testube_Front');
        Testube_Front4.scale.setTo(.85,.85);
        Zinc_Sulphate_Solution_in_Testube=this.game.add.sprite(642,495, 'Zinc_Sulphate_Solution_in_Testube',"Zinc_Sulphate_Solution_in_Testube0001.png");
        Zinc_Sulphate_Solution_in_Testube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,16,17,18,19,20,21,22,23,24,25],10, false, true);
        Zinc_Sulphate_Solution_in_Testube.scale.setTo(.85,.85);
        Zinc_Sulphate_Solution_in_Testube.visible=false;
        Copper_in_Zinc_Sulphate=this.game.add.sprite(642,495, 'Copper_in_Zinc_Sulphate',"Copper_in_Zinc_Sulphate0001.png");
        Copper_in_Zinc_Sulphate.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
        //Copper_in_Zinc_Sulphate.animations.add('anim2',[16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35],2, false, true);
        Copper_in_Zinc_Sulphate.scale.setTo(.85,.85);
        Copper_in_Zinc_Sulphate.visible=false;

        D_Blue=this.game.add.sprite(680,560, 'D_Blue');
        D_Blue.scale.setTo(.6,.6);
        ///////////////////////////////////////////////////////////////////////////
        rack_front=this.game.add.sprite(200,600, 'rack_front');
        rack_front.scale.set(.7);
        rackGroup=this.game.add.group();
        rackGroup.add(rack_Back);
        rackGroup.add(Testube_Back1);
        rackGroup.add(Testube_Front1);
        rackGroup.add(Aluminium_Sulphate_Solution_in_Testube);
        rackGroup.add(Copper_in_Aluminiumr_Sulphate);
        
        rackGroup.add(A_Blue);
        ///////////////////////////////////
        rackGroup.add(Testube_Back2);
        rackGroup.add(Testube_Front2);
        rackGroup.add(Copper_Sulphate_Solution_in_Testube);
        rackGroup.add(Copper_in_Copper_Sulphate);

        rackGroup.add(B_Blue);
        ////////////////////////////////////
        rackGroup.add(Testube_Back3);
        rackGroup.add(Testube_Front3);
        rackGroup.add(Ferrous_Sulphate_Solution_in_Testube);
        rackGroup.add(Copper_in_Ferrousr_Sulphate);

        rackGroup.add(C_Blue);
        ////////////////////////////////////////
        rackGroup.add(Testube_Back4);
        rackGroup.add(Testube_Front4);
        rackGroup.add(Zinc_Sulphate_Solution_in_Testube);
        rackGroup.add(Copper_in_Zinc_Sulphate);

        rackGroup.add(D_Blue);
        ///////////////////////////////////////
        rackGroup.add(rack_front);
        rackGroup.x=-3000;
/////////////////////////////////////////////////////////
        Aluminium_Sulphate_Text=this.game.add.sprite(345,900, 'Aluminium_Sulphate_Text');
        Aluminium_Sulphate_Text.scale.set(.7);
        Aluminium_Sulphate_Text.visible=false;
        Copper_Sulphate_Text=this.game.add.sprite(453,900, 'Copper_Sulphate_Text');
        Copper_Sulphate_Text.scale.set(.7);
        Copper_Sulphate_Text.visible=false;
        Ferrous_Sulphate_Text=this.game.add.sprite(562,900, 'Ferrous_Sulphate_text');
        Ferrous_Sulphate_Text.scale.set(.7);
        Ferrous_Sulphate_Text.visible=false;
        Zing_Sulphate_Text=this.game.add.sprite(670,900, 'Zing_Sulphate_Text');
        Zing_Sulphate_Text.scale.set(.7);
        Zing_Sulphate_Text.visible=false;
/////////////////////////////////////////////////////////
        beaker1_back=this.game.add.sprite(900,515, 'Beaker_back');
        beaker1_back.scale.set(.8);
        //Bottle_back1=this.game.add.sprite(900,560, 'Bottle_back');
        Aluminium_Sulphate_Dropper=this.game.add.sprite(1025,500, 'Aluminium_Dropper',"Aluminium_Sulphate_Dropper0016.png");
        Aluminium_Sulphate_Dropper.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        Aluminium_Sulphate_Dropper.visible=false;
        Aluminium_Sulphate_Dropper.anchor.set(.5);
        Aluminium_Sulphate_Dropper.scale.setTo(1,1.5);
        Aluminium_Sulphate_Dropper.events.onDragStart.add(function() {this.gameC(Aluminium_Sulphate_Dropper)}, this);
        Aluminium_Sulphate_Dropper.events.onDragStop.add(function() {this.gameC1(Aluminium_Sulphate_Dropper)}, this);
        this.game.physics.arcade.enable(Aluminium_Sulphate_Dropper);
        //Aluminium_Sulphate_Dropper.frameName="Aluminium_Sulphate_Dropper0016";
        //cap1=this.game.add.sprite(987,600, 'cap');
        Aluminium_Sulphate=this.game.add.sprite(900,480, 'Aluminium_Sulphate');
        Aluminium_Sulphate.scale.set(.8);
        Aluminium_label=this.game.add.sprite(960,700, 'Aluminium_label');
        Aluminium_label.scale.set(.8);
        //cap1_1=this.game.add.sprite(950,820, 'cap');
        //cap1_1.visible=false;
        beaker2_back=this.game.add.sprite(1100,515, 'Beaker_back');
        beaker2_back.scale.set(.8);
        Copper_Sulphate_Dropper=this.game.add.sprite(1225,500, 'Copper_Sulphate_Dropper','Copper_Sulphate_Dropper0016.png');
        for(var i=1;i<16;i++){//i=1
          Copper_Sulphate_Dropper.animations.add('frame'+i,[i-1]);
        }
        Copper_Sulphate_Dropper.events.onDragStart.add(function() {this.gameC(Copper_Sulphate_Dropper)}, this);
        Copper_Sulphate_Dropper.events.onDragStop.add(function() {this.gameC1(Copper_Sulphate_Dropper)}, this);
        this.game.physics.arcade.enable(Copper_Sulphate_Dropper);
        Copper_Sulphate_Dropper.visible=false;
        Copper_Sulphate_Dropper.anchor.set(.5);
        Copper_Sulphate_Dropper.scale.setTo(1,1.5);
        //cap2=this.game.add.sprite(1187,600, 'cap');
        Copper_Sulphate=this.game.add.sprite(1100,480, 'Copper_Sulphate');
        Copper_Sulphate.scale.set(.8);
        Copper_label=this.game.add.sprite(1160,700, 'Copper_label');
        Copper_label.scale.set(.8);
        //cap2_1=this.game.add.sprite(1150,820, 'cap');
        //cap2_1.visible=false;
        beaker3_back=this.game.add.sprite(1300,515, 'Beaker_back');
        beaker3_back.scale.set(.8);
        //Bottle_back3=this.game.add.sprite(1300,560, 'Bottle_back');
        Ferrous_Sulphate_Dropper=this.game.add.sprite(1425,500, 'Ferrous_Sulphate_Dropper','Ferrous_Sulphate_Dropper0016.png');
        for(var i=1;i<16;i++){//i=1
          Ferrous_Sulphate_Dropper.animations.add('frame'+i,[i-1]);
        }
        Ferrous_Sulphate_Dropper.events.onDragStart.add(function() {this.gameC(Ferrous_Sulphate_Dropper)}, this);
        Ferrous_Sulphate_Dropper.events.onDragStop.add(function() {this.gameC1(Ferrous_Sulphate_Dropper)}, this);
        this.game.physics.arcade.enable(Ferrous_Sulphate_Dropper);
        Ferrous_Sulphate_Dropper.visible=false;
        Ferrous_Sulphate_Dropper.anchor.set(.5);
        Ferrous_Sulphate_Dropper.scale.setTo(1,1.5);
        //cap3=this.game.add.sprite(1387,600, 'cap');
        Ferrous_Sulphate=this.game.add.sprite(1300,480, 'Ferrous_Sulphate');
        Ferrous_Sulphate.scale.set(.8);
        Ferrous_label=this.game.add.sprite(1360,700, 'Ferrous_label');
        Ferrous_label.scale.set(.8);
        //cap3_1=this.game.add.sprite(1350,820, 'cap');
        //cap3_1.visible=false;
        beaker4_back=this.game.add.sprite(1500,515, 'Beaker_back');
        beaker4_back.scale.set(.8);
        //Bottle_back4=this.game.add.sprite(1500,560, 'Bottle_back');
        Zinc_Sulphate_Dropper=this.game.add.sprite(1625,500, 'Zinc_Sulphate_Dropper','Zinc_Sulphate_Dropper0016.png');
        for(var i=1;i<16;i++){//i=1
          Zinc_Sulphate_Dropper.animations.add('frame'+i,[i-1]);
        }
        Zinc_Sulphate_Dropper.events.onDragStart.add(function() {this.gameC(Zinc_Sulphate_Dropper)}, this);
        Zinc_Sulphate_Dropper.events.onDragStop.add(function() {this.gameC1(Zinc_Sulphate_Dropper)}, this);
        this.game.physics.arcade.enable(Zinc_Sulphate_Dropper);
        Zinc_Sulphate_Dropper.visible=false;
        Zinc_Sulphate_Dropper.anchor.set(.5);
        Zinc_Sulphate_Dropper.scale.setTo(1,1.5);
        //cap4=this.game.add.sprite(1587,600, 'cap');
        Zinc_Sulphate=this.game.add.sprite(1500,480, 'Zing_Sulphate');
        Zinc_Sulphate.scale.set(.8);
        Zinc_label=this.game.add.sprite(1560,700, 'Zinc_label');
        Zinc_label.scale.set(.8);
        //cap4_1=this.game.add.sprite(1550,820, 'cap');
        //cap4_1.visible=false;
        ///////////////////////////////////////////////////////////////////////
        bottleGroup=this.game.add.group();
        bottleGroup.add(beaker1_back);
        bottleGroup.add(Aluminium_Sulphate_Dropper);
        //bottleGroup.add(Aluminium_label);
        bottleGroup.add(Aluminium_Sulphate);
        bottleGroup.add(Aluminium_label);

        bottleGroup.add(beaker2_back);
        bottleGroup.add(Copper_Sulphate_Dropper);
        bottleGroup.add(Copper_Sulphate);
        bottleGroup.add(Copper_label);
        //bottleGroup.add(cap2_1);
        
        bottleGroup.add(beaker3_back);
        bottleGroup.add(Ferrous_Sulphate_Dropper);
        //bottleGroup.add(cap3);
        bottleGroup.add(Ferrous_Sulphate);
        bottleGroup.add(Ferrous_label);

        bottleGroup.add(beaker4_back);
        bottleGroup.add(Zinc_Sulphate_Dropper);
        //bottleGroup.add(cap4);
        bottleGroup.add(Zinc_Sulphate);
        bottleGroup.add(Zinc_label);

        Aluminium_in_Plate=this.game.add.sprite(1050,840, 'Copper_in_Plate');
        Aluminium_in_Plate.scale.set(.8);
        Spoon_M=this.game.add.sprite(1200,790, 'Spoon_M');
        Spoon_M.angle=20;
        //Spoon_M.scale.set(1.1);
        spoon=this.game.add.sprite(1255,880, 'Copper_Spoon',"Copper_Spoon0001.png");
        spoon.angle=20;
        spoon.xp=1255;
        spoon.yp=880;
        spoon.alpha=0;
        spoon.nam="Copper_to_Aluminium";
        spoon.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],10, false, true);
        //Aluminium_Sulphate_Dropper.visible=false;
        spoon.anchor.set(.5);
        //Aluminium_Spoon.scale.setTo(1,1.2);
        spoon.events.onDragStart.add(function() {this.gameC(spoon)}, this);
        spoon.events.onDragStop.add(function() {this.gameC1(spoon)}, this);
        this.game.physics.arcade.enable(spoon);
        plateGroup=this.game.add.group();
        plateGroup.add(Aluminium_in_Plate);
        plateGroup.add(Spoon_M);
        plateGroup.add(spoon);
        plateGroup.x=3000;
/////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        Dropper1=this.game.add.sprite(1150,970, 'Dropper');
        Dropper1.scale.setTo(1,1.2);
        Dropper1.angle=60;
        Dropper1.anchor.set(.5);
        Dropper2=this.game.add.sprite(1250,970, 'Dropper');
        Dropper2.scale.setTo(1,1.2);
        Dropper2.angle=60;
        Dropper2.anchor.set(.5);
        Dropper3=this.game.add.sprite(1350,970, 'Dropper');
        Dropper3.scale.setTo(1,1.2);
        Dropper3.angle=60;
        Dropper3.anchor.set(.5);
        Dropper4=this.game.add.sprite(1450,970, 'Dropper');
        Dropper4.scale.setTo(1,1.2);
        Dropper4.angle=60;
        Dropper4.anchor.set(.5);
      //////////////////////////////////////////////////////////////////////////  
      clocks=this.game.add.sprite(550,260,"Clock");
      clocks.anchor.set(.5);
      clocks.scale.set(.7);
      clocksNeedleBig=this.game.add.sprite(550,265,"ClockClock_Needle_Big");
      clocksNeedleBig.anchor.set(.5,.85);
      clocksNeedleBig.scale.set(.7);
      clocksNeedleSmall=this.game.add.sprite(550,265,"Clock_Needle_Small");
      clocksNeedleSmall.anchor.set(.5,.8);
      clocksNeedleSmall.scale.set(.7);
      clocks.visible=false;
      clocksNeedleBig.visible=false;
      clocksNeedleSmall.visible=false;
      clockFlag=false;
      clockNum=0;
      clockDelay=4;
      clockAngle=0;
      //////////////////////////////////////////////////////////////////////////////  
      collider=this.game.add.sprite(680,500, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(1.3,2.5);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;//.5
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      arrow=this.game.add.sprite(1220,780, 'arrow');
      arrow.angle=90;
      arrow_y=830;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);

        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/

    },
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
      voice.destroy(true);
      voice=this.game.add.audio("step_1",1);
      voice.play();
      dialog.text="Take four test tube labeled as A, B, C, D";
      tween1=this.game.add.tween(rackGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
        
      }.bind(this));
       //////////////////////////////////////////
       //this.ShowReactions();
    },
    takeDropper:function(){
      arrow.x=Dropper1.x;
        arrow.y=Dropper1.y-130;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step_2",1);
        voice.play();
        dialog.text="Take a Dropper";
        Dropper1.inputEnabled = true;
        Dropper1.input.useHandCursor = true;
        Dropper1.events.onInputDown.add(this.clickOnDropper, this);
        droperFlag=true;
    },
  clickOnDropper:function(){
    if(droperFlag){
      arrow.visible=false;
      droperFlag=false;
      dropperClickCount++;
      
      if(dropperClickCount==1){
        Dropper1.angle=0;
        tween1=this.game.add.tween(Dropper1).to( {x:Aluminium_Sulphate_Dropper.x, y:Aluminium_Sulphate_Dropper.y}, 400, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Dropper1.visible=false;
         // cap1.visible=false;
         // cap1_1.visible=true;
          Aluminium_Sulphate_Dropper.visible=true;
          tween2=this.game.add.tween(Aluminium_Sulphate_Dropper).to( {y:710}, 400, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            voice.destroy(true);
            voice=this.game.add.audio("step_3",1);
            voice.play();
            dialog.text="Add 5 ml of Aluminium sulphate solution to the test tube A";
            arrow.x=Aluminium_Sulphate_Dropper.x+15;
            arrow.y=Aluminium_Sulphate_Dropper.y-230;
            arrow_y=arrow.y;
            arrow.visible=true;
            Aluminium_Sulphate_Dropper.xp=Aluminium_Sulphate_Dropper.x;
            Aluminium_Sulphate_Dropper.yp=Aluminium_Sulphate_Dropper.y;
            //console.log(Testube_Front1.x+"//"+Testube_Front2.x+"//"+Testube_Front3.x+"//"+Testube_Front4.x);
            collider.x=Testube_Front1.x+30;
            collider.y=Testube_Front1.y-20;
            Aluminium_Sulphate_Dropper.inputEnabled = true;
            Aluminium_Sulphate_Dropper.input.useHandCursor = true;
            Aluminium_Sulphate_Dropper.input.enableDrag(true);
          }.bind(this));
        }.bind(this));
      }else if(dropperClickCount==2){
        Dropper2.angle=2;
        tween1=this.game.add.tween(Dropper2).to( {x:Copper_Sulphate_Dropper.x, y:Copper_Sulphate_Dropper.y}, 400, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Dropper2.visible=false;
         // cap2.visible=false;
         // cap2_1.visible=true;
          Copper_Sulphate_Dropper.visible=true;
          tween2=this.game.add.tween(Copper_Sulphate_Dropper).to( {y:710}, 400, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            //dialog.text="Add 5 ml of Aluminium sulphate solution to the test tube A";
            arrow.x=Copper_Sulphate_Dropper.x+15;
            arrow.y=Copper_Sulphate_Dropper.y-230;
            arrow_y=arrow.y;
            arrow.visible=true;
            Copper_Sulphate_Dropper.xp=Copper_Sulphate_Dropper.x;
            Copper_Sulphate_Dropper.yp=Copper_Sulphate_Dropper.y;
            //console.log(Testube_Front1.x+"//"+Testube_Front2.x+"//"+Testube_Front3.x+"//"+Testube_Front4.x);
            collider.x=Testube_Front2.x+30;
            collider.y=Testube_Front2.y-20;
            Copper_Sulphate_Dropper.inputEnabled = true;
            Copper_Sulphate_Dropper.input.useHandCursor = true;
            Copper_Sulphate_Dropper.input.enableDrag(true);
          }.bind(this));
        }.bind(this));
      }else if(dropperClickCount==3){
        Dropper3.angle=0;
        tween1=this.game.add.tween(Dropper3).to( {x:Ferrous_Sulphate_Dropper.x, y:Ferrous_Sulphate_Dropper.y}, 400, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Dropper3.visible=false;
         // cap3.visible=false;
         // cap3_1.visible=true;
          Ferrous_Sulphate_Dropper.visible=true;
          tween2=this.game.add.tween(Ferrous_Sulphate_Dropper).to( {y:710}, 400, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            //dialog.text="Add 5 ml of Aluminium sulphate solution to the test tube A";
            arrow.x=Ferrous_Sulphate_Dropper.x+15;
            arrow.y=Ferrous_Sulphate_Dropper.y-230;
            arrow_y=arrow.y;
            arrow.visible=true;
            Ferrous_Sulphate_Dropper.xp=Ferrous_Sulphate_Dropper.x;
            Ferrous_Sulphate_Dropper.yp=Ferrous_Sulphate_Dropper.y;
            //console.log(Testube_Front1.x+"//"+Testube_Front2.x+"//"+Testube_Front3.x+"//"+Testube_Front4.x);
            collider.x=Testube_Front3.x+30;
            collider.y=Testube_Front3.y-20;
            Ferrous_Sulphate_Dropper.inputEnabled = true;
            Ferrous_Sulphate_Dropper.input.useHandCursor = true;
            Ferrous_Sulphate_Dropper.input.enableDrag(true);
          }.bind(this));
        }.bind(this));
      }else if(dropperClickCount==4){
        Dropper4.angle=0;
        tween1=this.game.add.tween(Dropper4).to( {x:Zinc_Sulphate_Dropper.x, y:Zinc_Sulphate_Dropper.y}, 400, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Dropper4.visible=false;
         // cap4.visible=false;
          //cap4_1.visible=true;
          Zinc_Sulphate_Dropper.visible=true;
          tween2=this.game.add.tween(Zinc_Sulphate_Dropper).to( {y:710}, 400, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            //dialog.text="Add 5 ml of Aluminium sulphate solution to the test tube A";
            arrow.x=Zinc_Sulphate_Dropper.x+15;
            arrow.y=Zinc_Sulphate_Dropper.y-230;
            arrow_y=arrow.y;
            arrow.visible=true;
            Zinc_Sulphate_Dropper.xp=Zinc_Sulphate_Dropper.x;
            Zinc_Sulphate_Dropper.yp=Zinc_Sulphate_Dropper.y;
            //console.log(Testube_Front1.x+"//"+Testube_Front2.x+"//"+Testube_Front3.x+"//"+Testube_Front4.x);
            collider.x=Testube_Front4.x+30;
            collider.y=Testube_Front4.y-20;
            Zinc_Sulphate_Dropper.inputEnabled = true;
            Zinc_Sulphate_Dropper.input.useHandCursor = true;
            Zinc_Sulphate_Dropper.input.enableDrag(true);
          }.bind(this));
        }.bind(this));
      }
    }
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    //arrow.visible=false;
    console.log("drag");
    if(obj==Aluminium_Sulphate_Dropper){
      Aluminium_Sulphate_Dropper.frameName="Aluminium_Sulphate_Dropper0001.png";
      arrow.x=collider.x+45;
      arrow.y=collider.y-55;
      arrow_y=arrow.y;
    }else if(obj==Copper_Sulphate_Dropper){
      Copper_Sulphate_Dropper.frameName="Copper_Sulphate_Dropper0001.png";
      arrow.x=collider.x+45;
      arrow.y=collider.y-55;
      arrow_y=arrow.y;
    }else if(obj==Ferrous_Sulphate_Dropper){
      Ferrous_Sulphate_Dropper.frameName="Ferrous_Sulphate_Dropper0001.png";
      arrow.x=collider.x+45;
      arrow.y=collider.y-55;
      arrow_y=arrow.y;
    }else if(obj==Zinc_Sulphate_Dropper){
      Zinc_Sulphate_Dropper.frameName="Zinc_Sulphate_Dropper0001.png";
      arrow.x=collider.x+45;
      arrow.y=collider.y-55;
      arrow_y=arrow.y;
    }else if(obj==spoon){
      arrow.x=collider.x+45;
      arrow.y=collider.y-55;
      arrow_y=arrow.y;
      Spoon_M.visible=false;
      spoon.alpha=1;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==Aluminium_Sulphate_Dropper){
      arrow.visible=false;
     // cap1_1.visible=false;
     // cap1.visible=true;
      Aluminium_Sulphate_Dropper.x=collider.x+30;
      Aluminium_Sulphate_Dropper.y=collider.y-50;
      Aluminium_Sulphate_Dropper.animations.play('anim');
      Testube_Front1.visible=false;
      Aluminium_Sulphate_Solution_in_Testube.visible=true;
      Aluminium_Sulphate_Solution_in_Testube.animations.play('anim');
      Aluminium_Sulphate_Solution_in_Testube.animations.currentAnim.onComplete.add(function () {
        Aluminium_Sulphate_Dropper.visible=false;
        Aluminium_Sulphate_Text.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step_4",1);
        voice.play();
        dialog.text="Repeat the same steps for each liquid with corresponding test tubes.";
        arrow.x=Dropper2.x;
        arrow.y=Dropper2.y-130;
        arrow_y=arrow.y;
        arrow.visible=true;
        Dropper2.inputEnabled = true;
        Dropper2.input.useHandCursor = true;
        Dropper2.events.onInputDown.add(this.clickOnDropper, this);
        droperFlag=true;
      }.bind(this)); 
    }else if(currentobj==Copper_Sulphate_Dropper){
      arrow.visible=false;
    //  cap2_1.visible=false;
     // cap2.visible=true;
      Copper_Sulphate_Dropper.x=collider.x+30;
      Copper_Sulphate_Dropper.y=collider.y-50;
      Copper_Sulphate_Dropper.animations.play('anim');
      Testube_Front2.visible=false;
      Copper_Sulphate_Solution_in_Testube.visible=true;
      Copper_Sulphate_Solution_in_Testube.animations.play('anim');
      Copper_Sulphate_Solution_in_Testube.animations.currentAnim.onComplete.add(function () {
        Copper_Sulphate_Dropper.visible=false;
        Copper_Sulphate_Text.visible=true;
        //dialog.text="Repeat the same steps for each liquid with corresponding test tubes.";
        arrow.x=Dropper3.x;
        arrow.y=Dropper3.y-130;
        arrow_y=arrow.y;
        arrow.visible=true;
        Dropper3.inputEnabled = true;
        Dropper3.input.useHandCursor = true;
        Dropper3.events.onInputDown.add(this.clickOnDropper, this);
        droperFlag=true;
      }.bind(this)); 
    }else if(currentobj==Ferrous_Sulphate_Dropper){
      arrow.visible=false;
    //  cap3_1.visible=false;
     // cap3.visible=true;
      Ferrous_Sulphate_Dropper.x=collider.x+30;
      Ferrous_Sulphate_Dropper.y=collider.y-50;
      Ferrous_Sulphate_Dropper.animations.play('anim');
      Testube_Front3.visible=false;
      Ferrous_Sulphate_Solution_in_Testube.visible=true;
      Ferrous_Sulphate_Solution_in_Testube.animations.play('anim');
      Ferrous_Sulphate_Solution_in_Testube.animations.currentAnim.onComplete.add(function () {
        Ferrous_Sulphate_Dropper.visible=false;
        Ferrous_Sulphate_Text.visible=true;
        //dialog.text="Repeat the same steps for each liquid with corresponding test tubes.";
        arrow.x=Dropper4.x;
        arrow.y=Dropper4.y-130;
        arrow_y=arrow.y;
        arrow.visible=true;
        Dropper4.inputEnabled = true;
        Dropper4.input.useHandCursor = true;
        Dropper4.events.onInputDown.add(this.clickOnDropper, this);
        droperFlag=true;
      }.bind(this)); 
    }else if(currentobj==Zinc_Sulphate_Dropper){
      arrow.visible=false;
    //  cap4_1.visible=false;
     // cap4.visible=true;
      Zinc_Sulphate_Dropper.x=collider.x+30;
      Zinc_Sulphate_Dropper.y=collider.y-50;
      Zinc_Sulphate_Dropper.animations.play('anim');
      Testube_Front4.visible=false;
      Zinc_Sulphate_Solution_in_Testube.visible=true;
      Zinc_Sulphate_Solution_in_Testube.animations.play('anim');
      Zinc_Sulphate_Solution_in_Testube.animations.currentAnim.onComplete.add(function () {
        Zinc_Sulphate_Dropper.visible=false;
        Zing_Sulphate_Text.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step_C5",1);
        voice.play();
        dialog.text="Add few small pieces of copper turnings to each of the test tubes.";
        tween1=this.game.add.tween(bottleGroup).to( {x:3000}, 1000, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          tween2=this.game.add.tween(plateGroup).to( {x:0}, 1000, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            arrow.x=spoon.x+20;
            arrow.y=spoon.y-120;
            arrow_y=arrow.y;
            arrow.visible=true;
            spoon.inputEnabled = true;
            spoon.input.useHandCursor = true;
            spoon.input.enableDrag(true);
            collider.x=Testube_Front1.x+30;
            collider.y=Testube_Front1.y-20;
          }.bind(this));  
        }.bind(this));
      }.bind(this)); 
    }else if(currentobj==spoon){
      
      arrow.visible=false;
      spoon.x=collider.x+150;
      spoon.y=collider.y+10;
      spoon.animations.play('anim');
      spoon.animations.currentAnim.onComplete.add(function(){
        
        if(spoon.nam=="Copper_to_Aluminium"){
          Aluminium_Sulphate_Solution_in_Testube.visible=false;
          Copper_in_Aluminiumr_Sulphate.visible=true;
          Copper_in_Aluminiumr_Sulphate.animations.play('anim');
          Copper_in_Aluminiumr_Sulphate.animations.currentAnim.onComplete.add(function(){
            spoon.alpha=0;
            spoon.angle=20;
            spoon.frameName="Copper_Spoon0001.png";
            spoon.reset(spoon.xp,spoon.yp);
            Spoon_M.visible=true;
            arrow.x=spoon.x+20;
            arrow.y=spoon.y-120;
            arrow_y=arrow.y;
            arrow.visible=true;
            spoon.nam="Copper_to_Copper"
            spoon.inputEnabled = true;
            spoon.input.useHandCursor = true;
            spoon.input.enableDrag(true);
            collider.x=Testube_Front2.x+30;
            collider.y=Testube_Front2.y-20;
          }.bind(this)); 
        }else if(spoon.nam=="Copper_to_Copper"){
          Copper_Sulphate_Solution_in_Testube.visible=false;
          Copper_in_Copper_Sulphate.visible=true;
          Copper_in_Copper_Sulphate.animations.play('anim');
          Copper_in_Copper_Sulphate.animations.currentAnim.onComplete.add(function(){
            spoon.alpha=0;
            spoon.angle=20;
            spoon.frameName="Copper_Spoon0001.png";
            spoon.reset(spoon.xp,spoon.yp);
            Spoon_M.visible=true;
            arrow.x=spoon.x+20;
            arrow.y=spoon.y-120;
            arrow_y=arrow.y;
            arrow.visible=true;
            spoon.nam="Copper_to_Ferrous"
            spoon.inputEnabled = true;
            spoon.input.useHandCursor = true;
            spoon.input.enableDrag(true);
            collider.x=Testube_Front3.x+30;
            collider.y=Testube_Front3.y-20;
          }.bind(this));  
        }else if(spoon.nam=="Copper_to_Ferrous"){
          Ferrous_Sulphate_Solution_in_Testube.visible=false;
          Copper_in_Ferrousr_Sulphate.visible=true;
          Copper_in_Ferrousr_Sulphate.animations.play('anim');
          Copper_in_Ferrousr_Sulphate.animations.currentAnim.onComplete.add(function(){
            spoon.alpha=0;
            spoon.angle=20;
            spoon.frameName="Copper_Spoon0001.png";
            spoon.reset(spoon.xp,spoon.yp);
            Spoon_M.visible=true;
            arrow.x=spoon.x+20;
            arrow.y=spoon.y-120;
            arrow_y=arrow.y;
            arrow.visible=true;
            spoon.nam="Aluminium_to_Zinc"
            spoon.inputEnabled = true;
            spoon.input.useHandCursor = true;
            spoon.input.enableDrag(true);
            collider.x=Testube_Front4.x+30;
            collider.y=Testube_Front4.y-20;
          }.bind(this));  
        }else if(spoon.nam=="Aluminium_to_Zinc"){
          Zinc_Sulphate_Solution_in_Testube.visible=false;
          Copper_in_Zinc_Sulphate.visible=true;
          Copper_in_Zinc_Sulphate.animations.play('anim');
          Copper_in_Zinc_Sulphate.animations.currentAnim.onComplete.add(function(){
            spoon.alpha=0;
            spoon.angle=20;
            spoon.frameName="Copper_Spoon0001.png";
            spoon.reset(spoon.xp,spoon.yp);
            Spoon_M.visible=true;
            voice.destroy(true);
            voice=this.game.add.audio("step_6",1);
            voice.play();
            dialog.text="Wait for 30 minutes, and observe the changes in 4 solutions.";
            clocks.visible=true;
            clocksNeedleBig.visible=true;
            clocksNeedleSmall.visible=true;
            clockFlag=true;
            ///heve no changes 
            // Copper_in_Zinc_Sulphate.animations.play('anim2');
            // Copper_in_Ferrousr_Sulphate.animations.play('anim2');
            // Copper_in_Copper_Sulphate.animations.play('anim2');
            //Copper_in_Copper_Sulphate.animations.currentAnim.onComplete.add(function(){
            //  console.log("completed");
            //}.bind(this)); 
          }.bind(this));  
        }
          
      }.bind(this));
      
    }
    
  },
  ShowReactions:function(){

    circle1 = this.game.add.graphics(0, 0);
    circle1.lineStyle(8, 0xffd900, 1);
    //circle1.beginFill(0xFF0000, 1);
    circle1.drawCircle(330, 748, 410);
    circle1.visible=false;

    circle2 = this.game.add.graphics(0, 0);
    circle2.lineStyle(8, 0xffd900, 1);
    //circle2.beginFill(0xFF0000, 1);
    circle2.drawCircle(760, 748, 410);
    circle2.visible=false;

    circle3 = this.game.add.graphics(0, 0);
    circle3.lineStyle(8, 0xffd900, 1);
    //circle3.beginFill(0xFF0000, 1);
    circle3.drawCircle(1190, 748, 410);
    circle3.visible=false;
    
    circle4 = this.game.add.graphics(0, 0);
    circle4.lineStyle(8, 0xffd900, 1);
    //circle4.beginFill(0xFF0000, 1);
    circle4.drawCircle(1620, 748, 410);
    circle4.visible=false;

  Zoom_testtube1=this.game.add.sprite(-3000,550, 'Zoom_testtube',"Copper_in_Zinc_sulphate.png");
    Zoom_testtube1.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
    tween1=this.game.add.tween(Zoom_testtube1).to( {x:130}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      Zing_Sulphate_Text.visible=true;
        Zing_Sulphate_Text.scale.set(1.2);
        Zing_Sulphate_Text.x=260;
        Zing_Sulphate_Text.y=960;
      Zoom_testtube2=this.game.add.sprite(-3000,550, 'Zoom_testtube',"Copper_in_Ferrous_sulphate.png");
      Zoom_testtube2.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
      tween2=this.game.add.tween(Zoom_testtube2).to( {x:560}, 800, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        Ferrous_Sulphate_Text.visible=true;
          Ferrous_Sulphate_Text.scale.set(1.2);
          Ferrous_Sulphate_Text.x=690;
          Ferrous_Sulphate_Text.y=960;
        Zoom_testtube3=this.game.add.sprite(-3000,550, 'Zoom_testtube',"Copper_in_Copper_sulphate.png");
        Zoom_testtube3.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        tween3=this.game.add.tween(Zoom_testtube3).to( {x:990}, 800, Phaser.Easing.Out, true);
        tween3.onComplete.add(function () {
          Copper_Sulphate_Text.visible=true;
            Copper_Sulphate_Text.scale.set(1.2);
            Copper_Sulphate_Text.x=1120;
            Copper_Sulphate_Text.y=960;
          Zoom_testtube4=this.game.add.sprite(-3000,550, 'Zoom_testtube',"Copper_in_Aluminium_sulphate.png");
          Zoom_testtube4.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
          tween4=this.game.add.tween(Zoom_testtube4).to( {x:1420}, 800, Phaser.Easing.Out, true);
          tween4.onComplete.add(function () {
            Aluminium_Sulphate_Text.visible=true;
              Aluminium_Sulphate_Text.scale.set(1.2);
              Aluminium_Sulphate_Text.x=1550;
              Aluminium_Sulphate_Text.y=960;
            circle1.visible=true;
            circle2.visible=true;
            circle3.visible=true;
            circle4.visible=true;
            dialog.y=30;
            dialog_box.scale.setTo(.8,.9);
            voice.destroy(true);
            voice=this.game.add.audio("step_C7",1);
            voice.play();
            this.game.time.events.add(Phaser.Timer.SECOND*14,this.nextSoundofC7, this);
            //voice.onStop.add(this.nextSoundofC7,this);
            dialog.text="There is no change in the colour of the four salt solutions as well as \nthe appearance of the metal pieces. This reaction indicates that Cu is \nless reactive than other three metals Al, Zn and Fe.";
            //step_Z7
          }.bind(this));
        }.bind(this));
      }.bind(this));  
    }.bind(this));  

    
  //spoon.frameName="Aluminium_Spoon0001.png";
},
nextSoundofC7:function(){
  circle1.visible=false;
  circle2.visible=false;
  circle3.visible=false;
  circle4.visible=false;
  dialog.y=60;
    dialog_box.scale.setTo(.8,.7);
  play.visible=true;
  voice.destroy(true);
  voice=this.game.add.audio("step_C11",1);
  voice.play();
  //voice4.onStop.add(this.nextSoundofA10,this);//Al\u2082(SO\u2084)\u2083
  dialog.text="Click on the play button to see the reaction of Aluminium.";
  //voice.destroy(true);
  //voice1=this.game.add.audio("step_A8",1);
  //voice1.play();
  //voice1.onStop.add(this.nextSoundofA8,this);
  //dialog.text="In the case of FeSO\u2084 solution, the green colour of FeSO\u2084 disappears \nand iron settle down at the bottom of the test tube. This indicates \nthat Al is more reactive than Fe.";
},

nextSoundofA8:function(){
  circle2.visible=false;
  circle3.visible=true;
  //voice.destroy(true);
  voice2=this.game.add.audio("step_A9",1);
  voice2.play();
  voice2.onStop.add(this.nextSoundofA9,this);
  dialog.text="In the case of CuSO\u2084 solution, the blue colour of CuSO\u2084 disappears \nand red-brown particles of copper settle down at the bottom of the \ntest tube. This indicates that Al is more reactive than Cu.";
},
nextSoundofA9:function(){
  circle3.visible=false;
  circle4.visible=true;
  //voice.destroy(true);
  voice3=this.game.add.audio("step_A10",1);
  voice3.play();
  voice3.onStop.add(this.nextSoundofA10,this);
  dialog.text="In the case of Al\u2082(SO\u2084)\u2083 solution, no change is observed. \nBecause metal cannot displace itself from a solution of its own salt.";
},
nextSoundofA10:function(){
  circle4.visible=false;
  dialog.y=60;
    dialog_box.scale.setTo(.8,.7);
  play.visible=true;
  voice4=this.game.add.audio("step_C11",1);
  voice4.play();
  //voice4.onStop.add(this.nextSoundofA10,this);//Al\u2082(SO\u2084)\u2083
  dialog.text="Click on the play button to see the reaction of Aluminium.";
},
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }/**/
    if(clockFlag){
      clockNum++;
      if(clockNum>=clockDelay){
        clockNum=0;
        clocksNeedleBig.angle++;
        clockAngle++;
        if(clockAngle>=180){
          clockFlag=false;
          tween1=this.game.add.tween(plateGroup).to( {x:3000}, 800, Phaser.Easing.In, true);
          tween1.onComplete.add(function () {
            clocks.visible=false;
            clocksNeedleBig.visible=false;
            clocksNeedleSmall.visible=false;
            Aluminium_Sulphate_Text.visible=false;
            Copper_Sulphate_Text.visible=false;
            Ferrous_Sulphate_Text.visible=false;
            Zing_Sulphate_Text.visible=false;
            tween2=this.game.add.tween(rackGroup).to( {x:-3000}, 800, Phaser.Easing.In, true);
            tween2.onComplete.add(function () {
              this.ShowReactions();
            }.bind(this));   
          }.bind(this)); 
        }
      }
    }
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==Aluminium_Sulphate_Dropper){
        //currentobj.frameName="spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=Aluminium_Sulphate_Dropper.x+15;
        arrow.y=Aluminium_Sulphate_Dropper.y-230;
        arrow_y=arrow.y;
      }else if(currentobj==Copper_Sulphate_Dropper){
        //currentobj.frameName="spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=Copper_Sulphate_Dropper.x+15;
        arrow.y=Copper_Sulphate_Dropper.y-230;
        arrow_y=arrow.y;
      }else if(currentobj==Ferrous_Sulphate_Dropper){
        //currentobj.frameName="spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=Ferrous_Sulphate_Dropper.x+15;
        arrow.y=Ferrous_Sulphate_Dropper.y-230;
        arrow_y=arrow.y;
      }else if(currentobj==Zinc_Sulphate_Dropper){
        //currentobj.frameName="spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=Zinc_Sulphate_Dropper.x+15;
        arrow.y=Zinc_Sulphate_Dropper.y-230;
        arrow_y=arrow.y;
      }else if(currentobj==spoon){
        arrow.x=spoon.x+20;
        arrow.y=spoon.y-120;
        arrow_y=arrow.y;
        spoon.alpha=0;
        Spoon_M.visible=true;
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
      toObservations:function()
      {
      voice.destroy();
      
      this.state.start("Experiment_4", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_3",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
