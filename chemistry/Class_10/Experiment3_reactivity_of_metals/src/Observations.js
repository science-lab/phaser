var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation1",1);
    //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "36px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(50,50,'observation_table');
  base.scale.setTo(1.14,1.2);
  head1_text=this.game.add.text(83,180,"Metal",headfontStyle);
  head2_text=this.game.add.text(255,180,"Solution",headfontStyle);
  head3_text=this.game.add.text(600,180,"Observation",headfontStyle); 
  head4_text=this.game.add.text(1300,180,"Inference",headfontStyle); 

  //////////////////////////////////////////////////////////////////////////
  metal1_name=this.game.add.text(70,260,"",fontStyle);
  solution1_name=this.game.add.text(285,260,"",fontStyle);  
  observation1_name=this.game.add.text(525,260,"",fontStyle);
  inference1_name=this.game.add.text(1055,260,"",fontStyle);
  solution2_name=this.game.add.text(285,370,"",fontStyle);  
  observation2_name=this.game.add.text(525,370,"",fontStyle);
  inference2_name=this.game.add.text(1055,370,"",fontStyle);
  solution3_name=this.game.add.text(285,650,"",fontStyle);  
  observation3_name=this.game.add.text(525,650,"",fontStyle);
  inference3_name=this.game.add.text(1055,650,"",fontStyle);
  solution4_name=this.game.add.text(285,650,"",fontStyle);  
  observation4_name=this.game.add.text(525,650,"",fontStyle);
  inference4_name=this.game.add.text(1055,650,"",fontStyle);


  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
  next_btn = this.game.add.sprite(1635,880,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1635,880,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(180,880,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(180,880,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;
 play = this.game.add.sprite(1800,840,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observation1",1);
                voice.play();

              metal1_name.destroy(true);
              solution1_name.destroy(true);
              observation1_name.destroy(true);
              inference1_name.destroy(true);
              solution2_name.destroy(true); 
              observation2_name.destroy(true);
              inference2_name.destroy(true);
              solution3_name.destroy(true);  
              observation3_name.destroy(true);
              inference3_name.destroy(true);
              solution4_name.destroy(true);  
              observation4_name.destroy(true);
              inference4_name.destroy(true);

              metal1_name=this.game.add.text(65,260,"",fontStyle);
              solution1_name=this.game.add.text(230,260,"",fontStyle);  
              observation1_name=this.game.add.text(480,260,"",fontStyle);
              inference1_name=this.game.add.text(1027,260,"",fontStyle);
              solution2_name=this.game.add.text(230,380,"",fontStyle);  
              observation2_name=this.game.add.text(480,380,"",fontStyle);
              inference2_name=this.game.add.text(1027,380,"",fontStyle);
              solution3_name=this.game.add.text(230,610,"",fontStyle);  
              observation3_name=this.game.add.text(480,610,"",fontStyle);
              inference3_name=this.game.add.text(1027,610,"",fontStyle);
              solution4_name=this.game.add.text(230,840,"",fontStyle);  
              observation4_name=this.game.add.text(480,840,"",fontStyle);
              inference4_name=this.game.add.text(1027,820,"",fontStyle);

              metal1_name.text=" Zinc\n (Zn)";
              solution1_name.text="ZnSO\u2084 (aq)";  
              observation1_name.text="No change";
              inference1_name.text="Metal cannot displace itself from a solution \nof its own salt.";
              solution2_name.text="FeSO\u2084 (aq)";  
              observation2_name.text="The light green color of the \nFeSO\u2084 solution fades and \nbecomes almost colorless. Zinc \ngranules acquire a grey deposit.";
              inference2_name.text="Zinc displaces iron from FeSO\u2084 solution. \nZn (s) + FeSO\u2084 (aq) ---> ZnSO\u2084 (aq) + Fe (s). \nZinc is more reactive than iron.";
              solution3_name.text="CuSO\u2084 (aq)";  
              observation3_name.text="The blue color of the CuSO\u2084 \nsolution fades and becomes \nalmost colorless. Zinc granules \nget a reddish-brown deposit.";
              inference3_name.text="Zinc displaces Cu from CuSO\u2084 solution. \nZn (s) + CuSO\u2084(aq) ---> ZnSO\u2084 (aq) + Cu (s). \nZinc is more reactive than copper.";
              solution4_name.text="Al\u2082(SO\u2084)\u2083 (aq)";  
              observation4_name.text="No change";
              inference4_name.text="Zinc cannot displace Al from its salt \nsolution, therefore, zinc is less \nreactive than aluminium.";
              
              ///////////////////////////////////////////
              
              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observation2",1);
                voice.play();
              metal1_name.destroy(true);
              solution1_name.destroy(true);
              observation1_name.destroy(true);
              inference1_name.destroy(true);
              solution2_name.destroy(true); 
              observation2_name.destroy(true);
              inference2_name.destroy(true);
              solution3_name.destroy(true);  
              observation3_name.destroy(true);
              inference3_name.destroy(true);
              solution4_name.destroy(true);  
              observation4_name.destroy(true);
              inference4_name.destroy(true);

              metal1_name=this.game.add.text(65,260,"",fontStyle);
              solution1_name=this.game.add.text(230,260,"",fontStyle);  
              observation1_name=this.game.add.text(480,260,"",fontStyle);
              inference1_name=this.game.add.text(1027,260,"",fontStyle);
              solution2_name=this.game.add.text(230,380,"",fontStyle);  
              observation2_name=this.game.add.text(480,380,"",fontStyle);
              inference2_name=this.game.add.text(1027,380,"",fontStyle);
              solution3_name=this.game.add.text(230,510,"",fontStyle);  
              observation3_name=this.game.add.text(480,510,"",fontStyle);
              inference3_name=this.game.add.text(1027,510,"",fontStyle);
              solution4_name=this.game.add.text(230,740,"",fontStyle);  
              observation4_name=this.game.add.text(480,740,"",fontStyle);
              inference4_name=this.game.add.text(1027,740,"",fontStyle);


              
               metal1_name.text=" Iron\n (Fe)";
               solution1_name.text="ZnSO\u2084 (aq)";  
               observation1_name.text="No reaction";
               inference1_name.text="Iron cannot displace zinc from ZnSO\u2084 \nsolution Iron is less reactive than zinc.";
               solution2_name.text="FeSO\u2084 (aq)";  
               observation2_name.text="No reaction";
               inference2_name.text="Metal cannot displace itself from its salt \nsolution.";
               solution3_name.text="CuSO\u2084 (aq)";  
               observation3_name.text="The blue color of CuSO\u2084 \nsolution changes to pale green. \nIron filings get a reddish-brown \ndeposit.";
               inference3_name.text="Iron can displace copper from CuSO\u2084 \nsolution, and therefore, iron is \nmore reactive than copper.\nFe (s) + CuSO\u2084 (aq) ---> FeSO\u2084 (aq) + Cu (s) ";
              
               solution4_name.text="Al\u2082(SO\u2084)\u2083 (aq)";  
               observation4_name.text="No reaction";
               inference4_name.text="Iron cannot displace aluminium from \nAl\u2082(SO\u2084)\u2083 solution and hence, iron is less \nreactive than aluminium.";
               
              

            break;
            case 3:
              voice.destroy(true);
              voice=this.game.add.audio("Observation3",1);
                voice.play();
              metal1_name.destroy(true);
              solution1_name.destroy(true);
              observation1_name.destroy(true);
              inference1_name.destroy(true);
              solution2_name.destroy(true); 
              observation2_name.destroy(true);
              inference2_name.destroy(true);
              solution3_name.destroy(true);  
              observation3_name.destroy(true);
              inference3_name.destroy(true);
              solution4_name.destroy(true);  
              observation4_name.destroy(true);
              inference4_name.destroy(true);

              metal1_name=this.game.add.text(65,260,"",fontStyle);
              solution1_name=this.game.add.text(230,260,"",fontStyle);  
              observation1_name=this.game.add.text(480,260,"",fontStyle);
              inference1_name=this.game.add.text(1027,260,"",fontStyle);
              solution2_name=this.game.add.text(230,430,"",fontStyle);  
              observation2_name=this.game.add.text(480,430,"",fontStyle);
              inference2_name=this.game.add.text(1027,430,"",fontStyle);
              solution3_name=this.game.add.text(230,600,"",fontStyle);  
              observation3_name=this.game.add.text(480,600,"",fontStyle);
              inference3_name=this.game.add.text(1027,600,"",fontStyle);
              solution4_name=this.game.add.text(230,720,"",fontStyle);  
              observation4_name=this.game.add.text(480,720,"",fontStyle);
              inference4_name=this.game.add.text(1027,720,"",fontStyle);


              
              ///////////////////////////////////////////
              metal1_name.text="Copper\n(Cu)";
              solution1_name.text="ZnSO\u2084 (aq)";  
              observation1_name.text="No change";
              inference1_name.text="Copper cannot displace zinc from ZnSO\u2084 \nsolution, therefore, copper is less reactive \nthan zinc.";
              solution2_name.text="FeSO\u2084 (aq)";  
              observation2_name.text="No change";
              inference2_name.text="Copper cannot displace iron from its salt \nsolution, therefore, it is less reactive than \niron.";
              
              solution3_name.text="CuSO\u2084 (aq)";  
              observation3_name.text="No change";
              inference3_name.text="A metal cannot displace itself from its salt \nsolution.";
              solution4_name.text="Al\u2082(SO\u2084)\u2083 (aq)";  
              observation4_name.text="No change";
              inference4_name.text="Copper cannot displace aluminium from \nits salt solution, therefore, it is less reactive \nthan aluminium.";
              
              
             
            break;
            case 4:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4",1);
                voice.play();
              metal1_name.destroy(true);
              solution1_name.destroy(true);
              observation1_name.destroy(true);
              inference1_name.destroy(true);
              solution2_name.destroy(true); 
              observation2_name.destroy(true);
              inference2_name.destroy(true);
              solution3_name.destroy(true);  
              observation3_name.destroy(true);
              inference3_name.destroy(true);
              solution4_name.destroy(true);  
              observation4_name.destroy(true);
              inference4_name.destroy(true);

              metal1_name=this.game.add.text(65,260,"",fontStyle);
              solution1_name=this.game.add.text(230,260,"",fontStyle);  
              observation1_name=this.game.add.text(480,260,"",fontStyle);
              inference1_name=this.game.add.text(1027,260,"",fontStyle);
              solution2_name=this.game.add.text(230,440,"",fontStyle);  
              observation2_name=this.game.add.text(480,440,"",fontStyle);
              inference2_name=this.game.add.text(1027,440,"",fontStyle);
              solution3_name=this.game.add.text(230,620,"",fontStyle);  
              observation3_name=this.game.add.text(480,620,"",fontStyle);
              inference3_name=this.game.add.text(1027,620,"",fontStyle);
              solution4_name=this.game.add.text(230,850,"",fontStyle);  
              observation4_name=this.game.add.text(480,850,"",fontStyle);
              inference4_name=this.game.add.text(1027,850,"",fontStyle);

              
               metal1_name.text="Alumi-\nnium\n(Al)";
               solution1_name.text="ZnSO\u2084 (aq)";  
               observation1_name.text="No change in color of the \nsolution but Al wire gets a grey \ndeposit.";
               inference1_name.text="Aluminium displaces zinc from ZnSO\u2084 solut-\nion and hence is more reactive than Zn metal. \n2Al(s)+3ZnSO\u2084(aq)--->Al\u2082(SO\u2084)\u2083(aq)+3Zn(s)";
              
               solution2_name.text="FeSO\u2084 (aq)";  
               observation2_name.text="The pale green color of the solut-\nion becomes almost colorless. \nAl wire gets a grey deposit.";
               inference2_name.text="Aluminium displaces Fe from FeSO\u2084 solution \ntherefore, it is more reactive than iron. \n2Al(s)+3FeSO\u2084(aq)--->Al\u2082(SO\u2084)\u2083(aq)+3Fe(s)";
               
               solution3_name.text="CuSO\u2084 (aq)";  
               observation3_name.text="The blue color of the solution \nand fades eventually becomes \nalmost colorless. Al wire gets a \nreddish-brown deposit.";
               inference3_name.text="Aluminium displaces Cu from CuSO\u2084 \nsolution, and therefore, is more reactive \nthan Copper metal. \n2Al(s)+3CuSO\u2084(aq)--->Al\u2082(SO\u2084)\u2083(aq)+3Cu(s)";
               solution4_name.text="Al\u2082(SO\u2084)\u2083 (aq)";  
               observation4_name.text="No change";
               inference4_name.text="A metal cannot displace itself \nfrom its salt solution.";

            break;
            case 5:
              voice.destroy(true);
              voice=this.game.add.audio("Observation5",1);
                //////////////////////////voice.play();
              metal1_name.destroy(true);
              solution1_name.destroy(true);
              observation1_name.destroy(true);
              inference1_name.destroy(true);
              solution2_name.destroy(true); 
              observation2_name.destroy(true);
              inference2_name.destroy(true);
              solution3_name.destroy(true);  
              observation3_name.destroy(true);
              inference3_name.destroy(true);
              solution4_name.destroy(true);  
              observation4_name.destroy(true);
              inference4_name.destroy(true);

              metal1_name=this.game.add.text(65,260,"",fontStyle);
              solution1_name=this.game.add.text(230,260,"",fontStyle);  
              observation1_name=this.game.add.text(480,260,"",fontStyle);
              inference1_name=this.game.add.text(1027,260,"",fontStyle);
              solution2_name=this.game.add.text(230,500,"",fontStyle);  
              observation2_name=this.game.add.text(480,500,"",fontStyle);
              inference2_name=this.game.add.text(1027,500,"",fontStyle);
              solution3_name=this.game.add.text(230,840,"",fontStyle);  
              observation3_name=this.game.add.text(480,840,"",fontStyle);
              inference3_name=this.game.add.text(1027,840,"",fontStyle);


              metal1_name.text="";
              solution1_name.text="FeSO\u2084 (aq)";  
              observation1_name.text="The pale green color of the \nsolution becomes almost \ncolorless. Aluminium wire \ngets a grey deposit.";
              inference1_name.text="Aluminium displaces iron from FeSO4 \nsolution therefore, it is more reactive \nthan iron. 2Al (s) + 3FeSO\u2084 (aq) --->\nAl\u2082(SO\u2084)\u2083 (aq) + 3Fe (s)";
              
              solution2_name.text="CuSO\u2084 (aq)";  
              observation2_name.text="The blue color of the \nsolution and fades \neventually becomes almost \ncolorless. Aluminium wire \ngets a reddish-brown \ndeposit.";
              inference2_name.text="Aluminium displaces copper from \nCuSO\u2084 solution, and therefore, is more \nreactive than copper metal. \n2Al (s) + 3CuSO\u2084 (aq) ----> \nAl\u2082(SO\u2084)\u2083 (aq) + 3Cu (s)";
              solution3_name.text="Al\u2082(SO\u2084)\u2083 \n(aq)";  
              observation3_name.text="No change";
              inference3_name.text="A metal cannot displace itself \nfrom its salt solution.";
              solution4_name.text="";  
              observation4_name.text="";
              inference4_name.text="";
            break;
          }
        },
        toNext:function(){
          if(pageCount<4){
            prev_btn.visible=true;
            pageCount++;
              this.addObservation();
              if(pageCount>=4){
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      toPrev:function(){
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  