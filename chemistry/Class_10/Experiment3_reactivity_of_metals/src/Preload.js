var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        //this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
              
        this.game.load.image('observation_1','assets/observation_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        this.game.load.image('Aluminium_Sulphate','assets/Aluminium_Sulphate.png');
        this.game.load.image('Copper_Sulphate','assets/Copper_Sulphate.png');
        this.game.load.image('Ferrous_Sulphate','assets/Ferrous_Sulphate.png');
        this.game.load.image('Zing_Sulphate','assets/Zing_Sulphate.png');
        this.game.load.image('Bottle_back','assets/Bottle_back.png');
        this.game.load.image('Testube_Front','assets/Testube_Front.png');
        this.game.load.image('Testube_Back','assets/Testube_Back.png');
        this.game.load.image('rack_front','assets/Testube_stand_front.png');
        this.game.load.image('rack_Back','assets/Testube_stand_Back.png');
        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        this.game.load.image('Dropper','assets/Dropper.png');
        this.game.load.image('cap','assets/cap.png');
        this.game.load.image('Spoon_M','assets/Spoon_M.png');
        this.game.load.image('Aluminium_Sulphate_Text','assets/Aluminium_Sulphate_Text.png');
        this.game.load.image('Copper_Sulphate_Text','assets/Copper_Sulphate_Text.png');
        this.game.load.image('Ferrous_Sulphate_text','assets/Ferrous_Sulphate_text.png');
        this.game.load.image('Zing_Sulphate_Text','assets/Zing_Sulphate_Text.png');
        this.game.load.image('Aluminium_in_Plate','assets/Aluminium_in_Plate.png');
        this.game.load.image('Copper_in_Plate','assets/Copper_in_Plate.png');
        this.game.load.image('Iron_in_Plate','assets/Iron_in_Plate.png');
        this.game.load.image('Zinc_in_Plate','assets/Zinc_in_Plate.png');
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');

        this.game.load.image('test_tube','assets/test_tube.png');
        this.game.load.image('rack_front_M','assets/rack_front.png');

      this.game.load.image('Aluminium_label','assets/Aluminium_Sulphate_label.png');
      this.game.load.image('Copper_label','assets/Copper_Sulphate_label.png');
      this.game.load.image('Ferrous_label','assets/Ferrous_Sulphate_label.png');
      this.game.load.image('Zinc_label','assets/Zing_Sulphate_label.png');
      
        ////////////////Metal_Preperation////////////////////////////////////////////
        this.game.load.image('Copper_powder','assets/Metal_Reactions/Copper_powder.png');
        this.game.load.image('Ferrous_powder','assets/Metal_Reactions/Ferrous_powder.png');
        this.game.load.image('Zinc_powder','assets/Metal_Reactions/Zinc_powder.png');
        this.game.load.image('Aluminium_powder','assets/Metal_Reactions/Zinc_powder.png');
        this.game.load.image('Rod','assets/Metal_Reactions/Rod.png');

        this.game.load.image('Zinc_powder','assets/Metal_Reactions/Zinc_powder.png');
            this.game.load.image('Ferrous_powder','assets/Metal_Reactions/Ferrous_powder.png');
            this.game.load.image('Copper_powder','assets/Metal_Reactions/Copper_powder.png');
        
        this.game.load.image('Beaker_back', 'assets/Metal_Preperation/Beaker_back.png'); 


        this.game.load.atlasJSONHash('Spoon_Zinc', 'assets/Metal_Preperation/Spoon_Zinc.png', 'assets/Metal_Preperation/Spoon_Zinc.json');    
        this.game.load.atlasJSONHash('Zinc_to_beaker', 'assets/Metal_Preperation/Zinc_to_beaker.png', 'assets/Metal_Preperation/Zinc_to_beaker.json');    
        this.game.load.atlasJSONHash('Zink_with_rod', 'assets/Metal_Preperation/Zink_with_rod.png', 'assets/Metal_Preperation/Zink_with_rod.json');
        this.game.load.atlasJSONHash('Spoon_Ferrous', 'assets/Metal_Preperation/Spoon.png', 'assets/Metal_Preperation/Spoon.json');    
        this.game.load.atlasJSONHash('Ferrous_to_beaker', 'assets/Metal_Preperation/Ferrous_to_beaker.png', 'assets/Metal_Preperation/Ferrous_to_beaker.json');    
        this.game.load.atlasJSONHash('Ferrous_with_rod', 'assets/Metal_Preperation/Ferrous_with_rod.png', 'assets/Metal_Preperation/Ferrous_with_rod.json');
        this.game.load.atlasJSONHash('Spoon_Copper', 'assets/Metal_Preperation/Spoon_Copper.png', 'assets/Metal_Preperation/Spoon_Copper.json');    
        this.game.load.atlasJSONHash('Copper_to_beaker', 'assets/Metal_Preperation/Copper_to_beaker.png', 'assets/Metal_Preperation/Copper_to_beaker.json');    
        this.game.load.atlasJSONHash('Copper_with_rod', 'assets/Metal_Preperation/Copper_with_rod.png', 'assets/Metal_Preperation/Copper_with_rod.json');
        
        


  //////////////////Metal_Reactions/////////////////////////////////
        this.game.load.atlasJSONHash('Aluminium_Dropper', 'assets/Metal_Reactions/Aluminium_Dropper.png', 'assets/Metal_Reactions/Aluminium_Dropper.json');
        this.game.load.atlasJSONHash('Aluminium_Spoon', 'assets/Metal_Reactions/Aluminium_Spoon.png', 'assets/Metal_Reactions/Aluminium_Spoon.json');        
        this.game.load.atlasJSONHash('Aluminium_Sulphate_Solution_in_Testube', 'assets/Metal_Reactions/Aluminium_Sulphate_Solution_in_Testube.png', 'assets/Metal_Reactions/Aluminium_Sulphate_Solution_in_Testube.json');       
        this.game.load.atlasJSONHash('Aluminium_to_Aluminium_Sulphate', 'assets/Metal_Reactions/Aluminium_to_Aluminium_Sulphate.png', 'assets/Metal_Reactions/Aluminium_to_Aluminium_Sulphate.json');        
        this.game.load.atlasJSONHash('Aluminium_to_Copper_Sulphate', 'assets/Metal_Reactions/Aluminium_to_Copper_Sulphate.png', 'assets/Metal_Reactions/Aluminium_to_Copper_Sulphate.json');         
        this.game.load.atlasJSONHash('Aluminium_to_Ferrous_Sulphate', 'assets/Metal_Reactions/Aluminium_to_Ferrous_Sulphate.png', 'assets/Metal_Reactions/Aluminium_to_Ferrous_Sulphate.json');         
        this.game.load.atlasJSONHash('Aluminium_to_Zinc_Sulphate', 'assets/Metal_Reactions/Aluminium_to_Zinc_Sulphate.png', 'assets/Metal_Reactions/Aluminium_to_Zinc_Sulphate.json');        

        this.game.load.atlasJSONHash('Copper_Sulphate_Dropper', 'assets/Metal_Reactions/Copper_Sulphate_Dropper.png', 'assets/Metal_Reactions/Copper_Sulphate_Dropper.json');
        this.game.load.atlasJSONHash('Copper_Spoon', 'assets/Metal_Reactions/Copper_Spoon.png', 'assets/Metal_Reactions/Copper_Spoon.json');        
        this.game.load.atlasJSONHash('Copper_Sulphate_Solution_in_Testube', 'assets/Metal_Reactions/Copper_Sulphate_Solution_in_Testube.png', 'assets/Metal_Reactions/Copper_Sulphate_Solution_in_Testube.json');       
        this.game.load.atlasJSONHash('Copper_in_Aluminiumr_Sulphate', 'assets/Metal_Reactions/Copper_in_Aluminiumr_Sulphate.png', 'assets/Metal_Reactions/Copper_in_Aluminiumr_Sulphate.json');        
        this.game.load.atlasJSONHash('Copper_in_Copper_Sulphate', 'assets/Metal_Reactions/Copper_in_Copper_Sulphate.png', 'assets/Metal_Reactions/Copper_in_Copper_Sulphate.json');         
        this.game.load.atlasJSONHash('Copper_in_Ferrousr_Sulphate', 'assets/Metal_Reactions/Copper_in_Ferrousr_Sulphate.png', 'assets/Metal_Reactions/Copper_in_Ferrousr_Sulphate.json');         
        this.game.load.atlasJSONHash('Copper_in_Zinc_Sulphate', 'assets/Metal_Reactions/Copper_in_Zinc_Sulphate.png', 'assets/Metal_Reactions/Copper_in_Zinc_Sulphate.json');        

        this.game.load.atlasJSONHash('Ferrous_Sulphate_Dropper', 'assets/Metal_Reactions/Ferrous_Sulphate_Dropper.png', 'assets/Metal_Reactions/Ferrous_Sulphate_Dropper.json');
        this.game.load.atlasJSONHash('Iron_Spoon', 'assets/Metal_Reactions/Iron_Spoon.png', 'assets/Metal_Reactions/Iron_Spoon.json');        
        this.game.load.atlasJSONHash('Ferrous_Sulphate_Solution_in_Testube', 'assets/Metal_Reactions/Ferrous_Sulphate_Solution_in_Testube.png', 'assets/Metal_Reactions/Ferrous_Sulphate_Solution_in_Testube.json');       
        this.game.load.atlasJSONHash('Iron_in_Aluminium_sulphate', 'assets/Metal_Reactions/Iron_in_Aluminium_sulphate.png', 'assets/Metal_Reactions/Iron_in_Aluminium_sulphate.json');        
        this.game.load.atlasJSONHash('Iorn_in_Copper_Sulphate', 'assets/Metal_Reactions/Iorn_in_Copper_Sulphate.png', 'assets/Metal_Reactions/Iorn_in_Copper_Sulphate.json');         
        this.game.load.atlasJSONHash('Iron_in_Ferrous_sulphate', 'assets/Metal_Reactions/Iron_in_Ferrous_sulphate.png', 'assets/Metal_Reactions/Iron_in_Ferrous_sulphate.json');         
        this.game.load.atlasJSONHash('Iron_in_Zinc_sulphate', 'assets/Metal_Reactions/Iron_in_Zinc_sulphate.png', 'assets/Metal_Reactions/Iron_in_Zinc_sulphate.json');

        this.game.load.atlasJSONHash('Zinc_Sulphate_Dropper', 'assets/Metal_Reactions/Zinc_Sulphate_Dropper.png', 'assets/Metal_Reactions/Zinc_Sulphate_Dropper.json');
        this.game.load.atlasJSONHash('Zinc_Spoon', 'assets/Metal_Reactions/Zinc_Spoon.png', 'assets/Metal_Reactions/Zinc_Spoon.json');        
        this.game.load.atlasJSONHash('Zinc_Sulphate_Solution_in_Testube', 'assets/Metal_Reactions/Zinc_Sulphate_Solution_in_Testube.png', 'assets/Metal_Reactions/Zinc_Sulphate_Solution_in_Testube.json');       
        this.game.load.atlasJSONHash('Zinc_to_Aluminium_Sulphate', 'assets/Metal_Reactions/Zinc_to_Aluminium_Sulphate.png', 'assets/Metal_Reactions/Zinc_to_Aluminium_Sulphate.json');        
        this.game.load.atlasJSONHash('Zinc_to_Copper_Sulphate', 'assets/Metal_Reactions/Zinc_to_Copper_Sulphate.png', 'assets/Metal_Reactions/Zinc_to_Copper_Sulphate.json');         
        this.game.load.atlasJSONHash('Zinc_to_Ferrous_sulphate', 'assets/Metal_Reactions/Zinc_to_Ferrous_sulphate.png', 'assets/Metal_Reactions/Zinc_to_Ferrous_sulphate.json');         
        this.game.load.atlasJSONHash('Zinc_to_Zinc_Sulphate', 'assets/Metal_Reactions/Zinc_to_Zinc_Sulphate.png', 'assets/Metal_Reactions/Zinc_to_Zinc_Sulphate.json');

        this.game.load.atlasJSONHash('Zoom_testtube', 'assets/Metal_Reactions/Zoom_testtube.png', 'assets/Metal_Reactions/Zoom_testtube.json');

        //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precautions','assets/audio/Precautions.mp3');
      this.game.load.audio('Procedure1','assets/audio/Procedure1.mp3');
      this.game.load.audio('Procedure2','assets/audio/Procedure2.mp3');
      this.game.load.audio('Observation1','assets/audio/Observation1.mp3');
      this.game.load.audio('Observation2','assets/audio/Observation2.mp3');
      this.game.load.audio('Observation3','assets/audio/Observation3.mp3');
      this.game.load.audio('Observation4','assets/audio/Observation4.mp3');
      this.game.load.audio('Observation5','assets/audio/Observation5.mp3');
      this.game.load.audio('Result1','assets/audio/Result1.mp3');
      this.game.load.audio('Result2','assets/audio/Result2.mp3');
        
    
        this.game.load.audio('step_0','assets/audio/step_0.mp3');
        this.game.load.audio('step_1','assets/audio/step_1.mp3');
        this.game.load.audio('step_2','assets/audio/step_2.mp3');
        this.game.load.audio('step_3','assets/audio/step_3.mp3');
        this.game.load.audio('step_4','assets/audio/step_4.mp3');
        this.game.load.audio('step_A5','assets/audio/step_A5.mp3');
        this.game.load.audio('step_6','assets/audio/step_6.mp3');
        this.game.load.audio('step_A7','assets/audio/step_A7.mp3');
        this.game.load.audio('step_A8','assets/audio/step_A8.mp3');
        this.game.load.audio('step_A9','assets/audio/step_A9.mp3');
        this.game.load.audio('step_A10','assets/audio/step_A10.mp3');
        this.game.load.audio('step_A11','assets/audio/step_A11.mp3');
        this.game.load.audio('step_C5','assets/audio/step_C5.mp3');
        this.game.load.audio('step_C7','assets/audio/step_C7.mp3');
        this.game.load.audio('step_C11','assets/audio/step_C11.mp3');
        this.game.load.audio('step_F5','assets/audio/step_F5.mp3');
        this.game.load.audio('step_F7','assets/audio/step_F7.mp3');
        this.game.load.audio('step_F8','assets/audio/step_F8.mp3');
        this.game.load.audio('step_F9','assets/audio/step_F9.mp3');
        this.game.load.audio('step_F10','assets/audio/step_F10.mp3');
        this.game.load.audio('step_F11','assets/audio/step_F11.mp3');
        this.game.load.audio('step_Z5','assets/audio/step_Z5.mp3');
        this.game.load.audio('step_Z7','assets/audio/step_Z7.mp3');
        this.game.load.audio('step_Z8','assets/audio/step_Z8.mp3');
        this.game.load.audio('step_Z9','assets/audio/step_Z9.mp3');
        this.game.load.audio('step_Z10','assets/audio/step_Z10.mp3');    
        this.game.load.audio('step_Z11','assets/audio/step_Z11.mp3');
        /////////////////////////////////////////////////
        this.game.load.audio('step0_1','assets/audio/step0_1.mp3');
        this.game.load.audio('step0_2','assets/audio/step0_2.mp3');
        this.game.load.audio('step0_3','assets/audio/step0_3.mp3');
        this.game.load.audio('step0_4','assets/audio/step0_4.mp3');
        this.game.load.audio('step0_5','assets/audio/step0_5.mp3');
        this.game.load.audio('step0_6','assets/audio/step0_6.mp3');
        this.game.load.audio('step0_7','assets/audio/step0_7.mp3');
        this.game.load.audio('step0_8','assets/audio/step0_8.mp3');
        this.game.load.audio('step0_9','assets/audio/step0_9.mp3');
        this.game.load.audio('step0_10','assets/audio/step0_10.mp3');
        this.game.load.audio('step0_11','assets/audio/step0_11.mp3');
        this.game.load.audio('step0_12','assets/audio/step0_12.mp3');
        this.game.load.audio('step0_13','assets/audio/step0_13.mp3');
        this.game.load.audio('step0_14','assets/audio/step0_14.mp3');
        this.game.load.audio('step0_15','assets/audio/step0_15.mp3');

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
      //this.game.state.start("Experiment_0");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");

	}
}

