var experiment_0 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;
// var voice1;
// var voice2;
// var voice3;
// var voice4;
//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var StepCount;




}

experiment_0.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);
// voice1=this.game.add.audio("step_0",1);
// voice2=this.game.add.audio("step_0",1);
// voice3=this.game.add.audio("step_0",1);
// voice4=this.game.add.audio("step_0",1);

 
 
 // voice=this.game.add.audio("fobj",1);
 StepCount=1;
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  
  currentobj=null;
  
  this.addItems();

  /*base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_step_1="1. Take 2 grams of ferrous sulphate crystals in a boiling tube. ";
  procedure_step_2="2. Heat the boiling tube on the bunsen burner. ";
  procedure_step_3="3. Observe the colour of the residue.";
  procedure_text=this.game.add.text(250,250,"Procedure:",headfontStyle);
  procedure_step_text_1=this.game.add.text(300,350,procedure_step_1,fontStyle);
  procedure_step_text_2=this.game.add.text(300,430,procedure_step_2,fontStyle);
  procedure_step_text_3=this.game.add.text(300,510,procedure_step_3,fontStyle);*/



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
        // Zinc_powder=this.game.add.sprite(750,800, 'Zinc_powder');
        // Zinc_powder.scale.set(.8);
        // Ferrous_powder=this.game.add.sprite(1100,800, 'Ferrous_powder');
        // Ferrous_powder.scale.set(.8);
        // Copper_powder=this.game.add.sprite(1450,800, 'Copper_powder');
        // Copper_powder.scale.set(.8);
        // Aluminium_powder=this.game.add.sprite(1800,800, 'Aluminium_powder');
        // Aluminium_powder.scale.set(.8);

        beakerGroup=this.game.add.group();
        beaker1_back=this.game.add.sprite(250,600, 'Beaker_back');
        beaker1_back.scale.set(.8);
        Zinc_to_beaker=this.game.add.sprite(250,562, 'Zinc_to_beaker','Zinc_to_beaker0001.png');
        Zinc_to_beaker.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],10, false, true);
        Zinc_to_beaker.scale.set(.8);
        Zink_with_rod=this.game.add.sprite(250,562, 'Zink_with_rod','Zink_with_rod0002.png');
        Zink_with_rod.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34],10, false, true);
        Zink_with_rod.scale.set(.8);
        Zink_with_rod.visible=false;
        A_Blue=this.game.add.sprite(335,750, 'A_Blue');
        A_Blue.scale.setTo(.8,.8);
        beakerGroup.add(beaker1_back);
        beakerGroup.add(Zinc_to_beaker);
        beakerGroup.add(Zink_with_rod);
        beakerGroup.add(A_Blue);

        beaker2_back=this.game.add.sprite(500,600, 'Beaker_back');
        beaker2_back.scale.set(.8);
        Ferrous_to_beaker=this.game.add.sprite(500,562, 'Ferrous_to_beaker','Ferrous_to_beaker0001.png');
        Ferrous_to_beaker.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],10, false, true);
        Ferrous_to_beaker.scale.set(.8);
        Ferrous_with_rod=this.game.add.sprite(500,562, 'Ferrous_with_rod','Ferrous_with_rod0001.png');
        Ferrous_with_rod.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34],10, false, true);
        Ferrous_with_rod.scale.set(.8);
        Ferrous_with_rod.visible=false;
        B_Blue=this.game.add.sprite(585,750, 'B_Blue');
        B_Blue.scale.setTo(.8,.8);
        beakerGroup.add(beaker2_back);
        beakerGroup.add(Ferrous_to_beaker);
        beakerGroup.add(Ferrous_with_rod);
        beakerGroup.add(B_Blue);

        beaker3_back=this.game.add.sprite(750,600, 'Beaker_back');
        beaker3_back.scale.set(.8);
        Copper_to_beaker=this.game.add.sprite(750,562, 'Copper_to_beaker','Copper_to_beaker0001.png');
        Copper_to_beaker.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],10, false, true);
        Copper_to_beaker.scale.set(.8);
        Copper_with_rod=this.game.add.sprite(750,562, 'Copper_with_rod','Copper_with_rod0001.png');
        Copper_with_rod.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34],10, false, true);
        Copper_with_rod.scale.set(.8);
        Copper_with_rod.visible=false;
        C_Blue=this.game.add.sprite(835,750, 'C_Blue');
        C_Blue.scale.setTo(.8,.8);
        beakerGroup.add(beaker3_back);
        beakerGroup.add(Copper_to_beaker);
        beakerGroup.add(Copper_with_rod);
        beakerGroup.add(C_Blue);

        beaker4_back=this.game.add.sprite(1000,600, 'Beaker_back');
        beaker4_back.scale.set(.8);
        Aluminium_to_beaker=this.game.add.sprite(1000,562, 'Zinc_to_beaker','Zinc_to_beaker0001.png');
        Aluminium_to_beaker.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],10, false, true);
        Aluminium_to_beaker.scale.set(.8);
        Aluminium_with_rod=this.game.add.sprite(1000,562, 'Zink_with_rod','Zink_with_rod0001.png');
        Aluminium_with_rod.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34],10, false, true);
        Aluminium_with_rod.scale.set(.8);
        Aluminium_with_rod.visible=false;
        D_Blue=this.game.add.sprite(1085,750, 'D_Blue');
        D_Blue.scale.setTo(.8,.8);
        beakerGroup.add(beaker4_back);
        beakerGroup.add(Aluminium_to_beaker);
        beakerGroup.add(Aluminium_with_rod);
        beakerGroup.add(D_Blue);
        beakerGroup.x=-3000;
///////////////////////////////////////////////////////////////////
        Aluminium_Sulphate_Text=this.game.add.sprite(1070,980, 'Aluminium_Sulphate_Text');
        //Aluminium_Sulphate_Text.scale.set(.8);
        Aluminium_Sulphate_Text.visible=false;
        Copper_Sulphate_Text=this.game.add.sprite(820,980, 'Copper_Sulphate_Text');
        //Copper_Sulphate_Text.scale.set(.7);
        Copper_Sulphate_Text.visible=false;
        Ferrous_Sulphate_Text=this.game.add.sprite(570,980, 'Ferrous_Sulphate_text');
        //Ferrous_Sulphate_Text.scale.set(.7);
        Ferrous_Sulphate_Text.visible=false;
        Zing_Sulphate_Text=this.game.add.sprite(320,980, 'Zing_Sulphate_Text');
        //Zing_Sulphate_Text.scale.set(.9);
        Zing_Sulphate_Text.visible=false;
/////////////////////////////////////////////////////////////////





        Zinc_powder=this.game.add.sprite(1300,800, 'Zinc_powder');
        Zinc_powder.scale.set(.8);
        Spoon_Zinc=this.game.add.sprite(1350,730, 'Spoon_Zinc','Spoon_Zinc0001.png');
        Spoon_Zinc.animations.add('anim',[1,2,3,4,5,6,7,8],10, false, true);
        Spoon_Zinc.scale.set(.8);
        Spoon_Zinc.xp=1350;
        Spoon_Zinc.yp=730;
        Spoon_Zinc.events.onDragStart.add(function() {this.gameC(Spoon_Zinc)}, this);
        Spoon_Zinc.events.onDragStop.add(function() {this.gameC1(Spoon_Zinc)}, this);
        this.game.physics.arcade.enable(Spoon_Zinc);
        ///////////////////////////////////////////////////////
        Ferrous_powder=this.game.add.sprite(1300,800, 'Ferrous_powder');
        Ferrous_powder.scale.set(.8);
        Spoon_Ferrous=this.game.add.sprite(1350,730, 'Spoon_Ferrous','Spoon0001.png');
        Spoon_Ferrous.animations.add('anim',[1,2,3,4,5,6,7,8],10, false, true);
        Spoon_Ferrous.scale.set(.8);
        Spoon_Ferrous.xp=1350;
        Spoon_Ferrous.yp=730;
        Spoon_Ferrous.events.onDragStart.add(function() {this.gameC(Spoon_Ferrous)}, this);
        Spoon_Ferrous.events.onDragStop.add(function() {this.gameC1(Spoon_Ferrous)}, this);
        this.game.physics.arcade.enable(Spoon_Ferrous);
        Ferrous_powder.visible=false;
        Spoon_Ferrous.visible=false;

/////////////////////////////////////////////////////////////////////////////////////////////
        Copper_powder=this.game.add.sprite(1300,800, 'Copper_powder');
        Copper_powder.scale.set(.8);
        Spoon_Copper=this.game.add.sprite(1350,730, 'Spoon_Copper','Spoon_Copper0001.png');
        Spoon_Copper.animations.add('anim',[1,2,3,4,5,6,7,8],10, false, true);
        Spoon_Copper.scale.set(.8);
        Spoon_Copper.xp=1350;
        Spoon_Copper.yp=730;
        Spoon_Copper.events.onDragStart.add(function() {this.gameC(Spoon_Copper)}, this);
        Spoon_Copper.events.onDragStop.add(function() {this.gameC1(Spoon_Copper)}, this);
        this.game.physics.arcade.enable(Spoon_Copper);
        Copper_powder.visible=false;
        Spoon_Copper.visible=false;
        //////////////////////////////////////////////////////////////////////////////////////////
        Aluminium_powder=this.game.add.sprite(1300,800, 'Aluminium_powder');
        Aluminium_powder.scale.set(.8);
        Spoon_Aluminium=this.game.add.sprite(1350,730, 'Spoon_Zinc','Spoon_Zinc0001.png');
        Spoon_Aluminium.animations.add('anim',[1,2,3,4,5,6,7,8],10, false, true);
        Spoon_Aluminium.scale.set(.8);
        Spoon_Aluminium.xp=1350;
        Spoon_Aluminium.yp=730;
        Spoon_Aluminium.events.onDragStart.add(function() {this.gameC(Spoon_Aluminium)}, this);
        Spoon_Aluminium.events.onDragStop.add(function() {this.gameC1(Spoon_Aluminium)}, this);
        this.game.physics.arcade.enable(Spoon_Aluminium);
        Aluminium_powder.visible=false;
        Spoon_Aluminium.visible=false;
        Spoon_M=this.game.add.sprite(1370,760, 'Spoon_M');
        Spoon_M.visible=false;
        Rod=this.game.add.sprite(1400,1000, 'Rod');
        Rod.anchor.setTo(0.35,0.5);
        Rod.angle=100;
        Rod.scale.y=.8;
        Rod.xp=1400;
        Rod.yp=1000;
        Rod.events.onDragStart.add(function() {this.gameC(Rod)}, this);
        Rod.events.onDragStop.add(function() {this.gameC1(Rod)}, this);
        this.game.physics.arcade.enable(Rod);


        powderGroup=this.game.add.group();
        powderGroup.add(Zinc_powder);
        powderGroup.add(Spoon_Zinc);
        powderGroup.add(Ferrous_powder);
        powderGroup.add(Spoon_Ferrous);
        powderGroup.add(Copper_powder);
        powderGroup.add(Spoon_Copper);
        powderGroup.add(Aluminium_powder);
        powderGroup.add(Spoon_Aluminium);
        powderGroup.add( Spoon_M);
        powderGroup.add(Rod);
        powderGroup.x=3000;
      //////////////////////////////////////////////////////////////////////////////  
      collider=this.game.add.sprite(300,620, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(3,2);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;//.5
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      arrow=this.game.add.sprite(1220,780, 'arrow');
      arrow.angle=90;
      arrow_y=830;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);

        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/

    },
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();

      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
      voice.destroy(true);
      voice=this.game.add.audio("step0_1",1);
      voice.play();
      dialog.text="Take four beakers with water and labelled as A, B, C, D.";
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.startExperiment1, this); 
      
    },
startExperiment1:function(){
     
       tween1=this.game.add.tween(beakerGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
       tween1.onComplete.add(function () {
         this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeZincSulphate, this);
        
       }.bind(this));
      //////////////////////////////////////////
      //this.ShowReactions();
    },
    
    takeZincSulphate:function(){
      voice.destroy(true);
      voice=this.game.add.audio("step0_2",1);
      voice.play();
      dialog.text="Take zinc sulphate.";
      tween1=this.game.add.tween(powderGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
       tween1.onComplete.add(function () {
         this.game.time.events.add(Phaser.Timer.SECOND*2,this.addZincSulphate, this);
        
       }.bind(this));
    } ,
    addZincSulphate:function(){
      voice.destroy(true);
      voice=this.game.add.audio("step0_3",1);
      voice.play();
      dialog.text="Add some zinc sulphate to beaker A.";
      Spoon_Zinc.inputEnabled = true;
      Spoon_Zinc.input.useHandCursor = true;
      Spoon_Zinc.input.enableDrag(true);
      arrow.x=Zinc_powder.x+150;
      arrow.y=Zinc_powder.y-70;
      arrow_y=arrow.y;
      arrow.visible=true;
    },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    //arrow.visible=false;
    console.log("drag");
    if(obj==Spoon_Zinc){
      arrow.x=collider.x+100;
      arrow.y=collider.y-100;
      arrow_y=arrow.y;
      //Spoon_M.visible=false;
      //spoon.alpha=1;
    }else if(obj==Rod){
      Rod.angle=0;
      arrow.x=collider.x+100;
      arrow.y=collider.y-100;
      arrow_y=arrow.y;
      //Spoon_M.visible=false;
      //spoon.alpha=1;
    }else if(obj==Spoon_Ferrous){
      arrow.x=collider.x+100;
      arrow.y=collider.y-100;
      arrow_y=arrow.y;
      //Spoon_M.visible=false;
      //spoon.alpha=1;
    }else if(obj==Spoon_Copper){
      arrow.x=collider.x+100;
      arrow.y=collider.y-100;
      arrow_y=arrow.y;
      //Spoon_M.visible=false;
      //spoon.alpha=1;
    }else if(obj==Spoon_Aluminium){
      arrow.x=collider.x+100;
      arrow.y=collider.y-100;
      arrow_y=arrow.y;
      //Spoon_M.visible=false;
      //spoon.alpha=1;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==Spoon_Zinc){
      
      arrow.visible=false;
      Spoon_Zinc.x=collider.x+35;
      Spoon_Zinc.y=collider.y-130;
      Spoon_Zinc.animations.play('anim');
      Zinc_to_beaker.animations.play('anim');
      Zinc_to_beaker.animations.currentAnim.onComplete.add(function(){
        Spoon_Zinc.frameName="Spoon_Zinc0001.png";
        Spoon_Zinc.reset(Spoon_Zinc.xp,Spoon_Zinc.yp);
        Spoon_Zinc.visible=false;
        Spoon_M.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step0_4",1);
        voice.play();
        dialog.text="Mix well with a glass rod.";
        Rod.inputEnabled = true;
        Rod.input.useHandCursor = true;
        Rod.input.enableDrag(true);
        arrow.x=Rod.x+30;
        arrow.y=Rod.y-140;
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));
      
    }else if(currentobj==Spoon_Ferrous){
      arrow.visible=false;
      Spoon_Ferrous.x=collider.x+35;
      Spoon_Ferrous.y=collider.y-130;
      Spoon_Ferrous.animations.play('anim');
      Ferrous_to_beaker.animations.play('anim');
      Ferrous_to_beaker.animations.currentAnim.onComplete.add(function(){
        Spoon_Ferrous.frameName="Spoon0001.png";
        Spoon_Ferrous.reset(Spoon_Ferrous.xp,Spoon_Ferrous.yp);
        Spoon_Ferrous.visible=false;
        Spoon_M.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step0_4",1);
        voice.play();
        dialog.text="Mix well with a glass rod.";
        Rod.inputEnabled = true;
        Rod.input.useHandCursor = true;
        Rod.input.enableDrag(true);
        arrow.x=Rod.x+30;
        arrow.y=Rod.y-140;
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));
      
    }else if(currentobj==Spoon_Copper){
      arrow.visible=false;
      Spoon_Copper.x=collider.x+35;
      Spoon_Copper.y=collider.y-130;
      Spoon_Copper.animations.play('anim');
      Copper_to_beaker.animations.play('anim');
      Copper_to_beaker.animations.currentAnim.onComplete.add(function(){
        Spoon_Copper.frameName="Spoon_Copper0001.png";
        Spoon_Copper.reset(Spoon_Copper.xp,Spoon_Copper.yp);
        Spoon_Copper.visible=false;
        Spoon_M.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step0_4",1);
        voice.play();
        dialog.text="Mix well with a glass rod.";
        Rod.inputEnabled = true;
        Rod.input.useHandCursor = true;
        Rod.input.enableDrag(true);
        arrow.x=Rod.x+30;
        arrow.y=Rod.y-140;
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));
      
    }else if(currentobj==Spoon_Aluminium){
      arrow.visible=false;
      Spoon_Aluminium.x=collider.x+35;
      Spoon_Aluminium.y=collider.y-130;
      Spoon_Aluminium.animations.play('anim');
      Aluminium_to_beaker.animations.play('anim');
      Aluminium_to_beaker.animations.currentAnim.onComplete.add(function(){
        Spoon_Aluminium.frameName="Spoon_Zinc0001.png";
        Spoon_Aluminium.reset(Spoon_Aluminium.xp,Spoon_Aluminium.yp);
        Spoon_Aluminium.visible=false;
        Spoon_M.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step0_4",1);
        voice.play();
        dialog.text="Mix well with a glass rod.";
        Rod.inputEnabled = true;
        Rod.input.useHandCursor = true;
        Rod.input.enableDrag(true);
        arrow.x=Rod.x+30;
        arrow.y=Rod.y-140;
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));
      
    }else if(currentobj==Rod){
      arrow.visible=false;
      if(StepCount==1){
        Zinc_to_beaker.visible=false;
        Zink_with_rod.visible=true;
        Rod.visible=false;
        Rod.angle=100;
        Rod.x=Rod.xp;
        Rod.y=Rod.yp;
        Zink_with_rod.animations.play('anim');
        Zink_with_rod.animations.currentAnim.onComplete.add(function(){
          Zing_Sulphate_Text.visible=true;
          Rod.visible=true;
          voice.destroy(true);
          voice=this.game.add.audio("step0_5",1);
          voice.play();
          dialog.text="Good job. Now we made an aqueous solution of zinc sulphate.";
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.TakeFerrous, this);  
        }.bind(this));  
      }else if(StepCount==2){
        Ferrous_to_beaker.visible=false;
        Ferrous_with_rod.visible=true;
        Rod.visible=false;
        Rod.angle=100;
        Rod.x=Rod.xp;
        Rod.y=Rod.yp;
        Ferrous_with_rod.animations.play('anim');
        Ferrous_with_rod.animations.currentAnim.onComplete.add(function(){
          Ferrous_Sulphate_Text.visible=true;
          Rod.visible=true;
          voice.destroy(true);
          voice=this.game.add.audio("step0_8",1);
          voice.play();
          dialog.text="Good job. Now we made an aqueous solution of ferrous sulphate.";
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.TakeCopper, this);  
        }.bind(this));
      }else if(StepCount==3){
        Copper_to_beaker.visible=false;
        Copper_with_rod.visible=true;
        Rod.visible=false;
        Rod.angle=100;
        Rod.x=Rod.xp;
        Rod.y=Rod.yp;
        Copper_with_rod.animations.play('anim');
        Copper_with_rod.animations.currentAnim.onComplete.add(function(){
          Copper_Sulphate_Text.visible=true;
          Rod.visible=true;
          voice.destroy(true);
          voice=this.game.add.audio("step0_11",1);
          voice.play();
          dialog.text="Good job. Now we made an aqueous solution of copper sulphate.";
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.TakeAluminium, this);  
        }.bind(this));
      }else if(StepCount==4){
        Aluminium_to_beaker.visible=false;
        Aluminium_with_rod.visible=true;
        Rod.visible=false;
        Rod.angle=100;
        Rod.x=Rod.xp;
        Rod.y=Rod.yp;
        Aluminium_with_rod.animations.play('anim');
        Aluminium_with_rod.animations.currentAnim.onComplete.add(function(){
          Aluminium_Sulphate_Text.visible=true;
          Rod.visible=true;
          voice.destroy(true);
          voice=this.game.add.audio("step0_14",1);
          voice.play();
          dialog.text="Good job. Now we made an aqueous solution of aluminium sulphate.";
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.conclusion, this);  
        }.bind(this));
      }
    }
    
  },
  TakeFerrous:function(){
    voice.destroy(true);
    collider.x=550;
    voice=this.game.add.audio("step0_6",1);
          voice.play();
    dialog.text="Take ferrous sulphate.";
    StepCount=2;
    tween1=this.game.add.tween(powderGroup).to( {x:3000, }, 1000, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      Spoon_M.visible=false;
      Zinc_powder.visible=false;
      Spoon_Zinc.visible=false;
      Ferrous_powder.visible=true;
      Spoon_Ferrous.visible=true;
      Rod.angle=100;
      //Rod.visible=true;
      tween2=this.game.add.tween(powderGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step0_7",1);
          voice.play();
        dialog.text="Add some Ferrous sulphate to beaker B.";
        Spoon_Ferrous.inputEnabled = true;
        Spoon_Ferrous.input.useHandCursor = true;
        Spoon_Ferrous.input.enableDrag(true);
        arrow.x=Ferrous_powder.x+150;
        arrow.y=Ferrous_powder.y-70;
        console.log(Ferrous_powder.y+"///");
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));  
    }.bind(this));   
  },
  TakeCopper:function(){
    voice.destroy(true);
    collider.x=800;
    voice=this.game.add.audio("step0_9",1);
          voice.play();
    dialog.text="Take copper sulphate.";
    StepCount=3;
    tween1=this.game.add.tween(powderGroup).to( {x:3000, }, 1000, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      Spoon_M.visible=false;
      Ferrous_powder.visible=false;
      Spoon_Ferrous.visible=false;
      Copper_powder.visible=true;
      Spoon_Copper.visible=true;
      Rod.angle=100;
      //Rod.visible=true;
      tween2=this.game.add.tween(powderGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step0_10",1);
          voice.play();
        dialog.text="Add some copper sulphate to beaker C.";
        Spoon_Copper.inputEnabled = true;
        Spoon_Copper.input.useHandCursor = true;
        Spoon_Copper.input.enableDrag(true);
        arrow.x=Copper_powder.x+150;
        arrow.y=Copper_powder.y-70;
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));  
    }.bind(this));   
  },
  TakeAluminium:function(){
    voice.destroy(true);
    collider.x=1050;
    voice=this.game.add.audio("step0_12",1);
          voice.play();
    dialog.text="Take aluminium sulphate.";
    StepCount=4;
    tween1=this.game.add.tween(powderGroup).to( {x:3000, }, 1000, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      Spoon_M.visible=false;
      Copper_powder.visible=false;
      Spoon_Copper.visible=false;
      Aluminium_powder.visible=true;
      Spoon_Aluminium.visible=true;
      Rod.angle=100;
      //Rod.visible=true;
      tween2=this.game.add.tween(powderGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step0_13",1);
          voice.play();
        dialog.text="Add some aluminium sulphate to beaker D.";
        Spoon_Aluminium.inputEnabled = true;
        Spoon_Aluminium.input.useHandCursor = true;
        Spoon_Aluminium.input.enableDrag(true);
        arrow.x=Aluminium_powder.x+150;
        arrow.y=Aluminium_powder.y-70;
        arrow_y=arrow.y;
        arrow.visible=true;
      }.bind(this));  
    }.bind(this));   
  },
  conclusion:function(){
    play.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step0_15",1);
    voice.play();
    dialog.text="Now click on the next button to study the metal reaction of zinc.";
  },
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }/**/
    
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==Spoon_Zinc){
        arrow.x=Zinc_powder.x+150;
        arrow.y=Zinc_powder.y-70;
        arrow_y=arrow.y;
        
      }else if(currentobj==Spoon_Copper){
        arrow.x=Copper_powder.x+150;
        arrow.y=Copper_powder.y-70;
        arrow_y=arrow.y;
        
      }else if(currentobj==Spoon_Ferrous){
        arrow.x=Ferrous_powder.x+150;
        arrow.y=Ferrous_powder.y-70;
        console.log(Ferrous_powder.y+"///......");
        arrow_y=arrow.y;
        
      }else if(currentobj==Spoon_Aluminium){
        arrow.x=Aluminium_powder.x+150;
        arrow.y=Aluminium_powder.y-70;
        arrow_y=arrow.y;
        
      }else if(Rod){

        Rod.angle=100;
        console.log(Rod.x+"//"+Rod.y);
        arrow.x=Rod.x+30;
        arrow.y=Rod.y-140;
        arrow_y=arrow.y;
        arrow.visible=true;
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      voice.destroy();
      
      this.state.start("Experiment_1", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_0",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
