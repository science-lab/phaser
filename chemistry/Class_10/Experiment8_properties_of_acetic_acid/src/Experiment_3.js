var experiment_3 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var dropCount;


}

experiment_3.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);


 
 
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  dropCount=1;
  this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
        //Take some dilute acetic acid in a test tube. Then take a strip of blue and red litmus paper.
        tileGroup=this.game.add.group();
        // rack_Back=this.game.add.sprite(100,600, 'rack_Back');
        // rack_Back.scale.set(.7);  
        // Testube_Back=this.game.add.sprite(435,455, 'Testube_Back');
        // Testube_Back.scale.setTo(.85,.85);
        Jar_Back=this.game.add.sprite(400,645, 'Jar_Back');//1120
        Jar_Back.scale.set(.35);
        Drop_to_testube=this.game.add.sprite(455,400, 'Drop_to_testube','Aluminium_Sulphate_Dropper0001.png');
        Drop_to_testube.scale.set(1.3);
        Drop_to_testube.animations.add('anim',[1,2,3,4],2, false, true);
        Drop_to_testube.visible=false;
        Drop_to_testube.xp=455;
        Drop_to_testube.yp=630;
        Drop_to_testube.events.onDragStart.add(function() {this.gameC(Drop_to_testube)}, this);
        Drop_to_testube.events.onDragStop.add(function() {this.gameC1(Drop_to_testube)}, this);
        this.game.physics.arcade.enable(Drop_to_testube);


        //Testube_wth_AceticAcid=this.game.add.sprite(435,455, 'Testube_wth_AceticAcid');
        //Testube_wth_AceticAcid.scale.setTo(.85,.85);
        //TestubeGroup=this.game.add.group();
        //TestubeGroup.add(Testube_Back);
        //TestubeGroup.add(Testube_wth_AceticAcid);
        cap=this.game.add.sprite(446,635, 'cap');
        cap.xp=446;
        cap.yp=635;
        aceticAcid=this.game.add.sprite(370,630, 'aceticAcid');
        aceticAcid.scale.set(1.5);
        //rack_front=this.game.add.sprite(100,600, 'rack_front');
        //rack_front.scale.set(.7);
        Dropper=this.game.add.sprite(950,800, 'Dropper');
        Dropper.scale.set(1.3);
        Dropper.angle=60;
        

        Tile=this.game.add.sprite(970,740, 'Tile');
        Tile.scale.setTo(.5,.9);
        Tile.visible=false;
        Red_Ls=this.game.add.sprite(1370,820, 'Red_Ls');
        //Red_Ls.scale.set(.9);
        Blue_Ls=this.game.add.sprite(960,820, 'Blue_Ls','Blue_Ls0001.png');
        Blue_Ls.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],6, false, true);
        Acid_drop=this.game.add.sprite(1155,780, 'Acid_drop','Acid_drop0001.png');
        Acid_drop.animations.add('anim',[1,2,3,4],3, false, true);
        Acid_drop.visible=false;
        tileGroup.add(Tile);
        tileGroup.add(Red_Ls);
        tileGroup.add(Blue_Ls);
        tileGroup.x=3000;
      //////////////////////////////////////////////////////////////////////////////  
      collider=this.game.add.sprite(1065,780, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(4,3);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;//.1
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      arrow=this.game.add.sprite(1220,780, 'arrow');
      arrow.angle=90;
      arrow_y=830;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);

        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/

    },
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
      voice.destroy(true);
      voice=this.game.add.audio("step3_1",1);
      voice.play();
      dialog.text="Take each strip of blue and red litmus paper.";
      tween1=this.game.add.tween(tileGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
        
      }.bind(this));
      //////////////////////////////////////////
      //this.ShowReactions();
    },
    takeDropper:function(){
      arrow.x=Dropper.x-100;
        arrow.y=Dropper.y-70;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_2",1);
        voice.play();
        dialog.text="Take the dropper";
        Dropper.inputEnabled = true;
        Dropper.input.useHandCursor = true;
        Dropper.events.onInputDown.add(this.clickOnDropper, this);
    },
    clickOnDropper:function(){
       Dropper.angle=0;
       arrow.visible=false;
       tween1=this.game.add.tween(Dropper).to( {x:Drop_to_testube.x, y:Drop_to_testube.y}, 400, Phaser.Easing.Out, true);
       tween1.onComplete.add(function () {
        cap.reset(300,830);
         Dropper.visible=false;
         Drop_to_testube.visible=true;
         tween2=this.game.add.tween(Drop_to_testube).to( {y:630}, 400, Phaser.Easing.Out, true);
         tween2.onComplete.add(function () {
           voice.destroy(true);
           voice=this.game.add.audio("step3_2",1);
           voice.play();
           dialog.text="Add one drop of diluted acetic acid into the blue litmus paper.";
           arrow.x=Dropper.x+40;
           arrow.y=Dropper.y+120;
           arrow_y=arrow.y;
           arrow.visible=true;
           Drop_to_testube.nam="Dt";
           Drop_to_testube.inputEnabled = true;
           Drop_to_testube.input.useHandCursor = true;
           Drop_to_testube.input.enableDrag(true);
           
         }.bind(this));
       }.bind(this));
    },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    //arrow.visible=false;
    console.log("drag");
    if(obj==Drop_to_testube){
      arrow.x=collider.x+110;
      arrow.y=collider.y-40;
      arrow_y=arrow.y;
      
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;

    if(currentobj==Drop_to_testube){
      
      arrow.visible=false;
      Drop_to_testube.x=collider.x+70;
      Drop_to_testube.y=collider.y-260;
      Drop_to_testube.animations.play('anim');
      Acid_drop.animations.play('anim');
      Acid_drop.visible=true;
      console.log(dropCount);
      Acid_drop.animations.currentAnim.onComplete.add(function(){
        Acid_drop.frameName='Acid_drop0001.png'
        //Acid_drop.visible=false;
        Drop_to_testube.frameName='Aluminium_Sulphate_Dropper0001.png'
        //Drop_to_testube.visible=false;
        collider.x=Red_Ls.x+105;
        Acid_drop.x=1560;
        Drop_to_testube.reset(Drop_to_testube.xp,Drop_to_testube.yp);
        
        
        console.log(dropCount);
        if(dropCount==1){
          Acid_drop.visible=false;
          Blue_Ls.animations.play('anim');  
          Blue_Ls.animations.currentAnim.onComplete.add(function(){
            voice.destroy(true);
            voice=this.game.add.audio("step3_3",1);
            voice.play();
            dialog.text="Now Observe the colour changes of the blue litmus.";
            
            this.game.time.events.add(Phaser.Timer.SECOND*3,this.droptoRedLs, this);
          }.bind(this));  
        }else if(dropCount==2){
          Acid_drop.visible=false;
            voice.destroy(true);
            voice=this.game.add.audio("step3_5",1);
            voice.play();
            dialog.text="Now Observe that the colour of the red litmus is not changed.";
            cap.reset(cap.xp,cap.yp);
            Drop_to_testube.visible=false;
            this.game.time.events.add(Phaser.Timer.SECOND*3,this.nextExp, this);
        }
      }.bind(this));  
     
      
    }
    
  },
  droptoRedLs:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_4",1);
    voice.play();
    dialog.text="Add another drop of diluted acetic acid into the red litmus paper.";
    Drop_to_testube.inputEnabled = true;
    Drop_to_testube.input.useHandCursor = true;
    Drop_to_testube.input.enableDrag(true);
    arrow.x=Dropper.x+40;
    arrow.y=Dropper.y+120;
    arrow_y=arrow.y;
    arrow.visible=true;
    dropCount=2;
  },
  nextExp:function(){
    play.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step3_6",1);
    voice.play();
    dialog.y=40;
    dialog.text="Click on the next button to do the test of reaction with sodium \nhydrogen carbonate.";
  },
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }/**/
    
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==Drop_to_testube){
        arrow.x=Drop_to_testube.x+40;
        arrow.y=Drop_to_testube.y-120;
        arrow_y=arrow.y;
        
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      voice.destroy();
      
      this.state.start("Procedure_4", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
