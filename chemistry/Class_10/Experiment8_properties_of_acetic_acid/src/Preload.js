var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        

this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
              
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        
        this.game.load.image('Testube_Front','assets/Testube_Front.png');
        this.game.load.image('Testube_Back','assets/Testube_Back.png');
        this.game.load.image('rack_front','assets/Testube_stand_front.png');
        this.game.load.image('rack_Back','assets/Testube_stand_Back.png');
        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        this.game.load.image('Dropper','assets/Dropper.png');
        this.game.load.image('cap','assets/cap.png');
        this.game.load.image('Spoon_M','assets/Spoon_M.png');
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');

        this.game.load.image('test_tube','assets/test_tube.png');
        this.game.load.image('rack_front_M','assets/rack.png');
        this.game.load.image('Stand','assets/Stand.png');
        this.game.load.image('Clamp','assets/Testune_Stand_portion.png');
        this.game.load.image('Stand1','assets/Testune_Stand_portion_1.png');
        this.game.load.image('Testube','assets/Testube.png');
        this.game.load.image('NaHCO3','assets/NaHCO3_in_Plate.png');

        
  //////////////////aceticAcid/////////////////////////////////
       this.game.load.image('Jar_Back','assets/aceticAcid/Jar_Back.png');
       this.game.load.image('aceticAcid','assets/aceticAcid/aceticAcid.png');
      this.game.load.image('Beaker_back','assets/aceticAcid/Beaker_back.png');
      this.game.load.image('Beaker_with_Water','assets/aceticAcid/Beaker_with_Water.png');
      this.game.load.image('Testtube_holder','assets/aceticAcid/Testtube_holder.png');
      this.game.load.image('Testube_wth_AceticAcid','assets/aceticAcid/Testube_wth_AceticAcid.png');
      this.game.load.image('Lemon0001','assets/aceticAcid/Lemon0001.png');
      this.game.load.image('Tile','assets/aceticAcid/Tile.png');
      this.game.load.image('Red_Ls','assets/aceticAcid/Red_Ls.png');
      //this.game.load.image('Rod','assets/Rod.png');

      

      this.game.load.atlasJSONHash('Acid_to_dropper', 'assets/aceticAcid/Acid_to_dropper.png', 'assets/aceticAcid/Acid_to_dropper.json'); 
      this.game.load.atlasJSONHash('Drop_to_testube', 'assets/aceticAcid/Drop_to_testube.png', 'assets/aceticAcid/Drop_to_testube.json'); 
      this.game.load.atlasJSONHash('Acetic_Acid_to_testube', 'assets/aceticAcid/Acetic_Acid_to_testube.png', 'assets/aceticAcid/Acetic_Acid_to_testube.json');
      this.game.load.atlasJSONHash('Nose', 'assets/aceticAcid/Nose.png', 'assets/aceticAcid/Nose.json');
      this.game.load.atlasJSONHash('AceticAcid_to_testTube', 'assets/aceticAcid/AceticAcid_to_testTube.png', 'assets/aceticAcid/AceticAcid_to_testTube.json');
      this.game.load.atlasJSONHash('Wter_to_aceticAcid', 'assets/aceticAcid/Wter_to_aceticAcid.png', 'assets/aceticAcid/Wter_to_aceticAcid.json');
      this.game.load.atlasJSONHash('Acid_with_water_shake', 'assets/aceticAcid/Acid_with_water_shake.png', 'assets/aceticAcid/Acid_with_water_shake.json');
      this.game.load.atlasJSONHash('Blue_Ls', 'assets/aceticAcid/Blue_Ls.png', 'assets/aceticAcid/Blue_Ls.json');
      this.game.load.atlasJSONHash('Acid_drop', 'assets/aceticAcid/Acid_drop.png', 'assets/aceticAcid/Acid_drop.json');
      
      this.game.load.atlasJSONHash('AceticAcid_to_Testtube_4', 'assets/aceticAcid/AceticAcid_to_Testtube_4.png', 'assets/aceticAcid/AceticAcid_to_Testtube_4.json');      
      this.game.load.atlasJSONHash('Brisk_Effervesence', 'assets/aceticAcid/Brisk_Effervesence.png', 'assets/aceticAcid/Brisk_Effervesence.json');
      this.game.load.atlasJSONHash('Fog', 'assets/aceticAcid/Fog.png', 'assets/aceticAcid/Fog.json');
      this.game.load.atlasJSONHash('NaHCO3_to_Acid', 'assets/aceticAcid/NaHCO3_to_Acid.png', 'assets/aceticAcid/NaHCO3_to_Acid.json');
      this.game.load.atlasJSONHash('Spoon_4', 'assets/aceticAcid/Spoon_4.png', 'assets/aceticAcid/Spoon_4.json');
      this.game.load.atlasJSONHash('NaHCO3_and_Acid_with_holder', 'assets/aceticAcid/NaHCO3_and_Acid_with_holder.png', 'assets/aceticAcid/NaHCO3_and_Acid_with_holder.json');
      this.game.load.atlasJSONHash('Lemon_reaction', 'assets/aceticAcid/Lemon_reaction.png', 'assets/aceticAcid/Lemon_reaction.json');
      this.game.load.atlasJSONHash('Rod', 'assets/aceticAcid/Rod.png', 'assets/aceticAcid/Rod.json');
      
      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');  
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Procedure_4','assets/audio/Procedure_4.mp3'); 
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');  
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('step_0','assets/audio/step_0.mp3');
      //////////////////////////////////////
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');

      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step2_5','assets/audio/step2_5.mp3');
      this.game.load.audio('step2_6','assets/audio/step2_6.mp3');

      this.game.load.audio('step3_1','assets/audio/step3_1.mp3');
      this.game.load.audio('step3_2','assets/audio/step3_2.mp3');
      this.game.load.audio('step3_3','assets/audio/step3_3.mp3');
      this.game.load.audio('step3_4','assets/audio/step3_4.mp3');
      this.game.load.audio('step3_5','assets/audio/step3_5.mp3');
      this.game.load.audio('step3_6','assets/audio/step3_6.mp3');
      
      this.game.load.audio('step4_1','assets/audio/step4_1.mp3');
      this.game.load.audio('step4_2','assets/audio/step4_2.mp3');
      this.game.load.audio('step4_3','assets/audio/step4_3.mp3');
      this.game.load.audio('step4_4','assets/audio/step4_4.mp3');
      this.game.load.audio('step4_5','assets/audio/step4_5.mp3');
      this.game.load.audio('step4_6','assets/audio/step4_6.mp3');
      this.game.load.audio('step4_7','assets/audio/step4_7.mp3');
      this.game.load.audio('step4_8','assets/audio/step4_8.mp3');
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
      //this.game.state.start("Experiment_2");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_4");

	}
}

