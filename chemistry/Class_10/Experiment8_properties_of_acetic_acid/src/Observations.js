var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation_1",1);
    //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(80,50,'observation_table');
  base.scale.setTo(1.13,1.17);
  base_line1= this.game.add.sprite(180,173,'observation_table_line');
  base_line2= this.game.add.sprite(430,173,'observation_table_line');
  base_line3= this.game.add.sprite(1150,173,'observation_table_line');
  base_line4= this.game.add.sprite(1760,220,'observation_table_line');
  base_line4.angle=90;
  base_line4.scale.setTo(1.1,2.08);
  //base_line.scale.setTo(1,1.15);
   head1_text=this.game.add.text(115,190,"S/N",headfontStyle);
   head2_text=this.game.add.text(285,190,"TEST",headfontStyle);
   head3_text=this.game.add.text(685,190,"OBSERVATION",headfontStyle); 
   head4_text=this.game.add.text(1380,180,"INFERENCE",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
  testNo1=this.game.add.text(120,250,"",fontStyle);
  test1_name=this.game.add.text(120,250,"",fontStyle);
  observation1_name=this.game.add.text(515,250,"",fontStyle);
  inference1_name=this.game.add.text(1030,250,"",fontStyle);
  testNo2=this.game.add.text(130,250,"",fontStyle);
  test2_name=this.game.add.text(270,370,"",fontStyle);  
  observation2_name=this.game.add.text(515,370,"",fontStyle);
  inference2_name=this.game.add.text(1030,370,"",fontStyle);
  testNo3=this.game.add.text(130,250,"",fontStyle);
  test3_name=this.game.add.text(280,650,"",fontStyle);  
  observation3_name=this.game.add.text(515,650,"",fontStyle);
  inference3_name=this.game.add.text(1030,650,"",fontStyle);
  testNo4=this.game.add.text(130,800,"",fontStyle);
  test4_name=this.game.add.text(280,800,"",fontStyle);  
  observation4_name=this.game.add.text(515,800,"",fontStyle);
  inference4_name=this.game.add.text(1030,800,"",fontStyle);


  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
  /*next_btn = this.game.add.sprite(1645,850,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1645,850,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(210,850,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(210,850,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;*/
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
//play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_1",1);
                voice.play();
              testNo1.destroy(true);
              test1_name.destroy(true);
              observation1_name.destroy(true);
              inference1_name.destroy(true);
              testNo2.destroy(true);
              test2_name.destroy(true); 
              observation2_name.destroy(true);
              inference2_name.destroy(true);
              testNo3.destroy(true);
              test3_name.destroy(true);  
              observation3_name.destroy(true);
              inference3_name.destroy(true);
              
              testNo1=this.game.add.text(135,260,"",fontStyle);
              test1_name=this.game.add.text(220,260,"",fontStyle);
              observation1_name=this.game.add.text(470,260,"",fontStyle);
              inference1_name=this.game.add.text(1190,260,"",fontStyle);
              testNo2=this.game.add.text(135,390,"",fontStyle);
              test2_name=this.game.add.text(220,390,"",fontStyle);  
              observation2_name=this.game.add.text(470,390,"",fontStyle);
              inference2_name=this.game.add.text(1190,390,"",fontStyle);
              testNo3=this.game.add.text(135,560,"",fontStyle);
              test3_name=this.game.add.text(220,560,"",fontStyle);  
              observation3_name=this.game.add.text(470,560,"",fontStyle);
              inference3_name=this.game.add.text(1190,560,"",fontStyle);
              testNo4=this.game.add.text(135,690,"",fontStyle);
              test4_name=this.game.add.text(220,690,"",fontStyle);  
              observation4_name=this.game.add.text(470,690,"",fontStyle);
              inference4_name=this.game.add.text(1190,690,"",fontStyle);

              
              testNo1.text="1";
              test1_name.text="Odour";  
              observation1_name.text="The acetic acid solution has a pungent, \nsour smell like that of vinegar.";
              inference1_name.text="Acetic acid has a vinegar-like \nsmell.";
              testNo2.text="2";
              test2_name.text="Solubility in \nwater";  
              observation2_name.text="The given acetic acid solution mixes \nwith water uniformly and forms a \nhomogeneous solution.";
              inference2_name.text="Acetic acid is highly soluble in \nwater.";
              testNo3.text="3";
              test3_name.text="Effect on \nlitmus";  
              observation3_name.text="Acetic acid turns blue litmus red and \nhas no effect on red litmus.";
              inference3_name.text="Acetic  acid behaves like an \nacid.";
              testNo4.text="4";
                test4_name.text="Reaction \nwith sodium \nhydrogen \ncarbonate";  
                observation4_name.text="A colorless and odourless gas evolves \nwith brisk effervescence. The gas, when \npassed through freshly prepared lime \nwater, turns it milky.";
                inference4_name.text="Acetic acid reaction with \nsodium hydrogen carbonate \nliberates carbon dioxide with \nbrisk effervescence.";
              ///////////////////////////////////////////
              
              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_2",1);
                voice.play();
                testNo1.destroy(true);
                test1_name.destroy(true);
                observation1_name.destroy(true);
                inference1_name.destroy(true);
                testNo2.destroy(true);
                test2_name.destroy(true); 
                observation2_name.destroy(true);
                inference2_name.destroy(true);
                testNo3.destroy(true);
                test3_name.destroy(true);  
                observation3_name.destroy(true);
                inference3_name.destroy(true);
                
                testNo1=this.game.add.text(170,280,"",fontStyle);
                test1_name=this.game.add.text(270,280,"",fontStyle);
                observation1_name=this.game.add.text(670,280,"",fontStyle);
                inference1_name=this.game.add.text(1120,280,"",fontStyle);
                
  
                
                testNo1.text="4";
                test1_name.text="Reaction with sodium \nhydrogen carbonate";  
                observation1_name.text="A colorless and \nodourless gas evolves \nwith brisk effervescence. \nThe gas, when passed \nthrough freshly prepared \nlime water, turns it milky. \nThus, the gas evolved is \ncarbon dioxide.";
                inference1_name.text="Acetic acid reaction with sodium \nhydrogen carbonate liberates \ncarbon dioxide with brisk \neffervescence.";
               
            break;
            
          }
        },
        toNext:function(){
          if(pageCount<2){
            prev_btn.visible=true;
            pageCount++;
              this.addObservation();
              if(pageCount>=2){
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      toPrev:function(){
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  