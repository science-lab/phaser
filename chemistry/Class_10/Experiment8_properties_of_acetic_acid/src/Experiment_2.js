var experiment_2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;



}

experiment_2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);


 
 
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  
  this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
        rack_Back=this.game.add.sprite(200,600, 'rack_Back');
        rack_Back.scale.set(.7);
        Testube_Back=this.game.add.sprite(535,495, 'Testube_Back');
        Testube_Back.scale.setTo(.85,.85);
        Testube_Front=this.game.add.sprite(535,495, 'Testube_Front');
        Testube_Front.scale.setTo(.85,.85);
        AceticAcid_to_testTube=this.game.add.sprite(535,495, 'AceticAcid_to_testTube','AceticAcid_to_testTube0001.png');
        AceticAcid_to_testTube.scale.setTo(.85,.85);
        AceticAcid_to_testTube.visible=false;
        AceticAcid_to_testTube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12],10, false, true);

        Wter_to_aceticAcid=this.game.add.sprite(535,495, 'Wter_to_aceticAcid','Wter_to_aceticAcid0001.png');
        Wter_to_aceticAcid.scale.setTo(.85,.85);
        Wter_to_aceticAcid.visible=false;
        Wter_to_aceticAcid.animations.add('anim',[1,2,3,4,5,6,7,8],6, false, true);
        Acid_with_water_shake=this.game.add.sprite(516,453, 'Acid_with_water_shake','Acid_with_water_shake0001.png');
        Acid_with_water_shake.scale.setTo(1.15,1.15);
        Acid_with_water_shake.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11],10, false, true);
        Acid_with_water_shake.visible=false;
        rack_front=this.game.add.sprite(200,600, 'rack_front');
        rack_front.scale.set(.7);



        
        /////////////////////////////////////////////////////
        Jar_Back=this.game.add.sprite(1020,645, 'Jar_Back');
        Jar_Back.scale.set(.35);
        /*Acid_to_dropper=this.game.add.sprite(1175,400, 'Acid_to_dropper','Acid_to_dropper0016.png');
        Acid_to_dropper.scale.set(1.25);
        Acid_to_dropper.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        Acid_to_dropper.visible=false;
        Acid_to_dropper.xp=1175;
        Acid_to_dropper.yp=630;
        Acid_to_dropper.events.onDragStart.add(function() {this.gameC(Acid_to_dropper)}, this);
        Acid_to_dropper.events.onDragStop.add(function() {this.gameC1(Acid_to_dropper)}, this);
        this.game.physics.arcade.enable(Acid_to_dropper);*/


        Drop_to_testube=this.game.add.sprite(1075,400, 'Drop_to_testube','Aluminium_Sulphate_Dropper0001.png');
        Drop_to_testube.scale.set(1.25);
        Drop_to_testube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        Drop_to_testube.visible=false;
        Drop_to_testube.xp=1075;
        Drop_to_testube.yp=630;
        Drop_to_testube.events.onDragStart.add(function() {this.gameC(Drop_to_testube)}, this);
        Drop_to_testube.events.onDragStop.add(function() {this.gameC1(Drop_to_testube)}, this);
        this.game.physics.arcade.enable(Drop_to_testube);

        cap=this.game.add.sprite(1068,635, 'cap');
        cap.xp=1068;
        cap.yp=635;
        aceticAcid=this.game.add.sprite(1000,630, 'aceticAcid');
        aceticAcid.scale.set(1.4);

        
        Beaker_back=this.game.add.sprite(1300,600, 'Beaker_back');
        Beaker_back.scale.set(.7);
        Drop_to_testube1=this.game.add.sprite(1375,400, 'Drop_to_testube','Aluminium_Sulphate_Dropper0001.png');
        Drop_to_testube1.scale.set(1.25);
        Drop_to_testube1.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        Drop_to_testube1.visible=false;
        Drop_to_testube1.xp=1375;
        Drop_to_testube1.yp=630;
        Drop_to_testube1.events.onDragStart.add(function() {this.gameC(Drop_to_testube1)}, this);
        Drop_to_testube1.events.onDragStop.add(function() {this.gameC1(Drop_to_testube1)}, this);
        this.game.physics.arcade.enable(Drop_to_testube1);
        Beaker_with_Water=this.game.add.sprite(1300,600, 'Beaker_with_Water');
        Beaker_with_Water.scale.set(.7);

        Testtube_holder=this.game.add.sprite(1250,950, 'Testtube_holder');
        Testtube_holder.scale.set(1.12);
        Testtube_holder.events.onDragStart.add(function() {this.gameC(Testtube_holder)}, this);
        Testtube_holder.events.onDragStop.add(function() {this.gameC1(Testtube_holder)}, this);
        this.game.physics.arcade.enable(Testtube_holder);
        Testtube_holder.xp=1300;
        Testtube_holder.yp=950;

        Dropper=this.game.add.sprite(1250,930, 'Dropper');
        Dropper.scale.set(1.3);
        Dropper.angle=80;
        Dropper1=this.game.add.sprite(1250,930, 'Dropper');
        Dropper1.scale.set(1.25);
        Dropper1.angle=80;
        Dropper1.visible=false;
        bottleGroup=this.game.add.group();
        bottleGroup.add(Jar_Back);
        bottleGroup.add(Drop_to_testube);
        bottleGroup.add(cap)
        bottleGroup.add(aceticAcid);
        bottleGroup.add(Beaker_back);
        bottleGroup.add(Drop_to_testube1);
        bottleGroup.add(Beaker_with_Water);
        bottleGroup.add(Testtube_holder);
        bottleGroup.add(Dropper);
        bottleGroup.add(Dropper1);
        bottleGroup.x=3000;
      
      //////////////////////////////////////////////////////////////////////////////  
      collider=this.game.add.sprite(680,500, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(1.3,2.5);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;//.5
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      arrow=this.game.add.sprite(1220,780, 'arrow');
      arrow.angle=90;
      arrow_y=830;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);

        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/

    },
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
       voice.destroy(true);
       voice=this.game.add.audio("step2_1",1);
       voice.play();
      dialog.text="Take some dilute solution of acetic acid and some water.";
       tween1=this.game.add.tween(bottleGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
       tween1.onComplete.add(function () {
         this.game.time.events.add(Phaser.Timer.SECOND*3,this.takeDropper, this);
        
       }.bind(this));
      //////////////////////////////////////////
      //this.ShowReactions();
    },
    takeDropper:function(){
      arrow.x=Dropper.x-100;
        arrow.y=Dropper.y-70;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_2",1);
        voice.play();
        dialog.text="Take the dropper.";
        Dropper.inputEnabled = true;
        Dropper.input.useHandCursor = true;
        Dropper.events.onInputDown.add(this.clickOnDropper, this);
    },
  
    clickOnDropper:function(){
      Dropper.angle=0;
      arrow.visible=false;
      tween1=this.game.add.tween(Dropper).to( {x:Drop_to_testube.x, y:Drop_to_testube.y}, 400, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        cap.reset(920,830);
        Dropper.visible=false;
        Drop_to_testube.visible=true;
        tween2=this.game.add.tween(Drop_to_testube).to( {y:630}, 400, Phaser.Easing.Out, true);
        tween2.onComplete.add(function () {
          voice.destroy(true);
          voice=this.game.add.audio("step2_2",1);
          voice.play();
          dialog.text="Add 5 ml of diluted acetic acid to the test tube.";
          //dialog.text="Take 5 ml of diluted acetic acid in the dropper and drop it \ninto the test tube.";
          arrow.x=Dropper.x+40;
          arrow.y=Dropper.y+120;
          arrow_y=arrow.y;
          arrow.visible=true;
          Drop_to_testube.inputEnabled = true;
          Drop_to_testube.input.useHandCursor = true;
          Drop_to_testube.input.enableDrag(true);
          collider.x=Testube_Front.x+30;
          collider.y=Testube_Front.y-20;
          //collider.alpha=.04;
        }.bind(this));
      }.bind(this));
    },
  clickOnDropper1:function(){
      Dropper1.angle=0;
      arrow.visible=false;
      tween1=this.game.add.tween(Dropper1).to( {x:Drop_to_testube1.x, y:Drop_to_testube1.y}, 400, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        Dropper1.visible=false;
        Drop_to_testube1.visible=true;
        tween2=this.game.add.tween(Drop_to_testube1).to( {y:630}, 400, Phaser.Easing.Out, true);
        tween2.onComplete.add(function () {
          arrow.x=Dropper1.x+40;
          arrow.y=Dropper1.y+120;
          arrow_y=arrow.y;
          arrow.visible=true;
          Drop_to_testube1.inputEnabled = true;
          Drop_to_testube1.input.useHandCursor = true;
          Drop_to_testube1.input.enableDrag(true);
          //collider.x=Testube_Front.x+30;
          //collider.y=Testube_Front.y-20;
          //collider.alpha=.04;
        }.bind(this));
      }.bind(this));
    },  
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    //arrow.visible=false;
    console.log("drag");
    if(obj==Drop_to_testube||obj==Drop_to_testube1||obj==Testtube_holder){
      arrow.x=collider.x+45;
      arrow.y=collider.y-55;
      arrow_y=arrow.y;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==Drop_to_testube){
      cap.reset(cap.xp,cap.yp);
      arrow.visible=false;
      Drop_to_testube.x=collider.x+5;
      Drop_to_testube.y=collider.y-200;
      Drop_to_testube.animations.play('anim');
      Testube_Front.visible=false;
      AceticAcid_to_testTube.visible=true;
      AceticAcid_to_testTube.animations.play('anim');
      AceticAcid_to_testTube.animations.currentAnim.onComplete.add(function(){
        Drop_to_testube.visible=false;
        Dropper1.visible=true;
        Dropper1.inputEnabled = true;
        Dropper1.input.useHandCursor = true;
        Dropper1.events.onInputDown.add(this.clickOnDropper1, this);
        arrow.x=Dropper1.x-100;
        arrow.y=Dropper1.y-70;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
          voice=this.game.add.audio("step2_3",1);
          voice.play();
          dialog.text="Add 5 ml of water to the test tube.";
      }.bind(this));
      
    }else if(currentobj==Drop_to_testube1){
      arrow.visible=false;
      Drop_to_testube1.x=collider.x+5;
      Drop_to_testube1.y=collider.y-200;
      Drop_to_testube1.animations.play('anim');
      Testube_Front.visible=false;
      AceticAcid_to_testTube.visible=false;
      Wter_to_aceticAcid.visible=true;
      Wter_to_aceticAcid.animations.play('anim');
      Wter_to_aceticAcid.animations.currentAnim.onComplete.add(function(){
        Drop_to_testube1.visible=false;
        Testtube_holder.inputEnabled = true;
        Testtube_holder.input.useHandCursor = true;
        Testtube_holder.input.enableDrag(true);
        arrow.x=Testtube_holder.x+200;
        arrow.y=Testtube_holder.y-80;
        arrow_y=arrow.y;
        arrow.visible=true;
        collider.x=Testube_Front.x+30;
        collider.y=Testube_Front.y+40;
        //collider.alpha=.5;
        voice.destroy(true);
          voice=this.game.add.audio("step2_4",1);
          voice.play();
          dialog.text="Shake the test tube with a test tube holder.";
      }.bind(this));
      
    }else if(currentobj==Testtube_holder){
      arrow.visible=false;
      Testtube_holder.visible=false;
      Testtube_holder.reset(Testtube_holder.xp,Testtube_holder.yp);
      Testtube_holder.visible=false;
      Wter_to_aceticAcid.visible=false;
      Testube_Back.visible=false;
      Acid_with_water_shake.visible=true;
      tween1=this.game.add.tween(Acid_with_water_shake).to( { y:200}, 800, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        Acid_with_water_shake.animations.play('anim');
        Acid_with_water_shake.animations.currentAnim.onComplete.add(function(){
          tween2=this.game.add.tween(Acid_with_water_shake).to( { y:450}, 800, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            Testtube_holder.visible=true;
            Acid_with_water_shake.visible=false;
            Testube_Back.visible=true;
            Wter_to_aceticAcid.visible=true;
            voice.destroy(true);
          voice=this.game.add.audio("step2_5",1);
          voice.play();
          dialog.text="Observe, the acetic acid dissolve in water";
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.nextAction, this);
          }.bind(this));
        }.bind(this));
      }.bind(this));  
      //Drop_to_testube1
      
      
    }
    
  },
  nextAction:function(){
    play.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_6",1);
    voice.play();
    dialog.text="Click on the next button to do the test of effect on litmus.";
  },

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }/**/
    
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==Drop_to_testube){
        arrow.x=Drop_to_testube.x+40;
        arrow.y=Drop_to_testube.y-100;
        arrow_y=arrow.y;
        
      }else if(currentobj==Drop_to_testube1){
        arrow.x=Drop_to_testube1.x+40;
        arrow.y=Drop_to_testube1.y-100;
        arrow_y=arrow.y;
        
      }else if(currentobj==Testtube_holder){
        arrow.x=Testtube_holder.x+200;
        arrow.y=Testtube_holder.y-80;
        arrow_y=arrow.y;
        
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      voice.destroy();
      
      this.state.start("Procedure_3", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_2",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
