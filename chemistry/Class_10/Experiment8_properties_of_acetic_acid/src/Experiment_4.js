var experiment_4 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;

var testubeDrag;

}

experiment_4.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("step_0",1);

 lemonDrag=false;
 testubeDrag=false;
 
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  lablel_fontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  
  this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment2, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
        
        rack_Back=this.game.add.sprite(1000,620, 'rack_Back');
        rack_Back.scale.set(.7);

        Stand=this.game.add.sprite(350,360,'Stand');
        Stand.scale.setTo(.9,.9);
        Stand1=this.game.add.sprite(620,570,'Stand1');
        Stand1.scale.setTo(2,.9);
        Testube_Back1=this.game.add.sprite(1335,500, 'Testube_Back');
        Testube_Back1.scale.setTo(.85,.85);
        Testube_Back1.xp=1335;
        Testube_Back1.yp=500;
        //Testube_Back1.visible=false;
        Lemon0001=this.game.add.sprite(1335,500, 'Lemon0001');
        Lemon0001.visible=false;
        Lemon0001.scale.set(.85);
        Lemon0001.xp=1335;
        Lemon0001.yp=500;
        Lemon0001.nam="Lemon";
        Lemon0001.events.onDragStart.add(function() {this.gameC(Lemon0001)}, this);
        Lemon0001.events.onDragStop.add(function() {this.gameC1(Lemon0001)}, this);
        this.game.physics.arcade.enable(Lemon0001);
        Testube=this.game.add.sprite(1335,500, 'Testube_Front');
        Testube.scale.setTo(.85,.85);
        //Testube.visible=false;
        Testube.nam="Tt";
        Testube.events.onDragStart.add(function() {this.gameC(Testube)}, this);
        Testube.events.onDragStop.add(function() {this.gameC1(Testube)}, this);
        this.game.physics.arcade.enable(Testube);
        Testube.xp=1335;
        Testube.yp=510;
        
        rack_front=this.game.add.sprite(1000,620, 'rack_front');
        rack_front.scale.set(.7);

        
        rackGroup=this.game.add.group();
        rackGroup.add(rack_Back);
        rackGroup.add(Testube_Back1);
        rackGroup.add(Lemon0001);
        rackGroup.add(Testube);
        rackGroup.add(rack_front);
//////////////////////////////////////////////////////////////////////////////////
        

        
        /////////////////////////////////////////////////////////////////////////
        

        Testube_Back=this.game.add.sprite(540,465, 'Testube_Back');
        Testube_Back.scale.setTo(.85,.85);
        Testube_Back.visible=false;
        Testube_Back2=this.game.add.sprite(710,465, 'Testube_Back');
        Testube_Back2.scale.setTo(.85,.85);
        Testube_Back2.visible=false;
        
        Rod=this.game.add.sprite(522,278, 'Rod','Rod0001.png');
        Rod.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],10, false, true);
        Rod.scale.set(1.3);
        Rod.visible=false;
        Fog=this.game.add.sprite(550,325, 'Fog','Fog0001.png');
        Fog.scale.setTo(.85,.85);
        Fog.alpha=.5;
        Fog.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,46,47,48,49,50],10, true, true);
        Fog.animations.play('anim');
        Fog.visible=false;
        AceticAcid_to_Testtube_4=this.game.add.sprite(540,465, 'AceticAcid_to_Testtube_4','AceticAcid_to_Testtube0001.png');
        AceticAcid_to_Testtube_4.scale.setTo(.85,.85);
        AceticAcid_to_Testtube_4.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],10, false, true);
        AceticAcid_to_Testtube_4.visible=false;
        NaHCO3_to_Acid=this.game.add.sprite(540,465, 'NaHCO3_to_Acid','NaHCO3_to_Acid0001.png');
        NaHCO3_to_Acid.scale.setTo(.85,.85);
        NaHCO3_to_Acid.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],6, false, true);
        NaHCO3_to_Acid.visible=false;
        NaHCO3_and_Acid_with_holder=this.game.add.sprite(505,420, 'NaHCO3_and_Acid_with_holder','NaHCO3_and_Acid_with_holder0001.png');
        NaHCO3_and_Acid_with_holder.scale.setTo(1.2,1.2);
        NaHCO3_and_Acid_with_holder.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14],10, false, true);
        NaHCO3_and_Acid_with_holder.visible=false;
        Brisk_Effervesence=this.game.add.sprite(540,465, 'Brisk_Effervesence','NaHCO3_to_acid_reaction0001.png');
        Brisk_Effervesence.scale.setTo(.85,.85);
        Brisk_Effervesence.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
        Brisk_Effervesence.animations.add('anim2',[16,17,18,19,20,21,22,23,24,25],10, true, true);
        Brisk_Effervesence.visible=false;


        Lemon_reaction=this.game.add.sprite(710,465, 'Lemon_reaction','Lemon_reaction0001.png');
        Lemon_reaction.scale.setTo(.85,.85);
        Lemon_reaction.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24],10, false, true);
        Lemon_reaction.animations.add('anim2',[18,19,20,21,22,23,24],10, false, true);
        Lemon_reaction.visible=false;

        Clamp=this.game.add.sprite(540,570,'Clamp');
        Clamp.scale.set(.9);
        Clamp1=this.game.add.sprite(705,570,'Clamp');
        Clamp1.scale.set(.9);
      //////  /////////////////////////////////////////////////////////////////////////////////
        Jar_Back=this.game.add.sprite(1120,645, 'Jar_Back');
        Jar_Back.scale.set(.35);
        /*Acid_to_dropper=this.game.add.sprite(1175,400, 'Acid_to_dropper','Acid_to_dropper0016.png');
        Acid_to_dropper.scale.set(1.25);
        Acid_to_dropper.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        Acid_to_dropper.visible=false;
        Acid_to_dropper.xp=1175;
        Acid_to_dropper.yp=630;
        Acid_to_dropper.events.onDragStart.add(function() {this.gameC(Acid_to_dropper)}, this);
        Acid_to_dropper.events.onDragStop.add(function() {this.gameC1(Acid_to_dropper)}, this);
        this.game.physics.arcade.enable(Acid_to_dropper);*/

        Drop_to_testube=this.game.add.sprite(1175,400, 'Drop_to_testube','Aluminium_Sulphate_Dropper0001.png');
        Drop_to_testube.scale.set(1.25);
        Drop_to_testube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],10, false, true);
        Drop_to_testube.visible=false;
        Drop_to_testube.xp=1175;
        Drop_to_testube.yp=630;
        Drop_to_testube.events.onDragStart.add(function() {this.gameC(Drop_to_testube)}, this);
        Drop_to_testube.events.onDragStop.add(function() {this.gameC1(Drop_to_testube)}, this);
        this.game.physics.arcade.enable(Drop_to_testube);
        Drop_to_testube.nam="Tt";
        cap=this.game.add.sprite(1168,635, 'cap');
        cap.xp=1168;
        cap.yp=635;
        aceticAcid=this.game.add.sprite(1100,630, 'aceticAcid');
        aceticAcid.scale.set(1.4);
        Dropper=this.game.add.sprite(1180,900, 'Dropper');
        Dropper.scale.set(1.25);
        Dropper.angle=90;
        NaHCO3=this.game.add.sprite(1300,800, 'NaHCO3');
        NaHCO3.scale.set(.8);
        Spoon_4=this.game.add.sprite(1375,720, 'Spoon_4','Spoon0001.png');
        Spoon_4.animations.add('anim',[1,2,3,4,5,6,7,8],6, false, true);
        Spoon_4.scale.set(.8);
        Spoon_4.xp=1375;
        Spoon_4.yp=720;
        Spoon_4.nam="Tt";
        Spoon_4.events.onDragStart.add(function() {this.gameC(Spoon_4)}, this);
        Spoon_4.events.onDragStop.add(function() {this.gameC1(Spoon_4)}, this);
        this.game.physics.arcade.enable(Spoon_4);
        Testtube_holder=this.game.add.sprite(1175,950, 'Testtube_holder');
        Testtube_holder.scale.set(.8);
        Testtube_holder.xp=1175;
        Testtube_holder.yp=950;
        Testtube_holder.nam="Tt";
        Testtube_holder.events.onDragStart.add(function() {this.gameC(Testtube_holder)}, this);
        Testtube_holder.events.onDragStop.add(function() {this.gameC1(Testtube_holder)}, this);
        this.game.physics.arcade.enable(Testtube_holder);
        Testtube_holder.visible=false;
        
        bottleGroup=this.game.add.group();
        bottleGroup.add(Jar_Back);
        bottleGroup.add(Drop_to_testube);
        bottleGroup.add(cap)
        bottleGroup.add(aceticAcid);
        bottleGroup.add(Dropper);
        bottleGroup.add(NaHCO3);
        bottleGroup.add(Spoon_4);
        bottleGroup.add(Testtube_holder);
        bottleGroup.x=3000;

        Rod1=this.game.add.sprite(3000,930, 'Rod');
        Rod1.scale.set(1.3);
        Rod1.anchor.set(.5);
        Rod1.angle=30;
        Rod1.xp=1300;
        Rod1.yp=930;
        Rod1.visible=false;
      //////////////////////////////////////////////////////////////////////////////  
      collider=this.game.add.sprite(560,550, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(1.5,2);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;//.2
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      
      arrow=this.game.add.sprite(1220,780, 'arrow');
      arrow.angle=90;
      arrow_y=830;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);

        /*bmd=this.game.add.bitmapData(800,600);
        bmd.ctx.beginPath();
        bmd.ctx.lineWidth = 4;
        bmd.ctx.stroeStyle = 'white';
        bmd.ctx.setLineDash([20,5]);
        bmd.ctx.moveTo(10,10);
        bmd.ctx.lineTo(400,400);
        bmd.ctx.stroke();
        bmd.ctx.closePath();
        l1=this.game.add.sprite(300,300,bmd);*/

    },
    loadScene:function(){
      //console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
      voice.destroy(true);
      voice=this.game.add.audio("step4_1",1);
      voice.play();
      dialog.text="Take the test tube to the clamp stand.";
      Testube.inputEnabled = true;
      Testube.input.useHandCursor = true;
      Testube.input.enableDrag(true);
      arrow.x=Testube.x+75;
      arrow.y=Testube.y-70;
      arrow_y=arrow.y;
      arrow.visible=true;
      // tween1=this.game.add.tween(rackGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
      // tween1.onComplete.add(function () {
      //   this.game.time.events.add(Phaser.Timer.SECOND*.3,this.takeDropper, this);
        
      // }.bind(this));
      //////////////////////////////////////////
      //this.ShowReactions();
    },
    takeDropper:function(){
      
    },
  
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    //arrow.visible=false;
    console.log("drag"+"//"+obj.nam);
    if(obj==Lemon0001){
      arrow.x=collider.x+45;
      arrow.y=collider.y-70;
      arrow_y=arrow.y;
      lemonDrag=true;
    }else if(obj==Testube || obj==Drop_to_testube || obj==Spoon_4 || obj==Testtube_holder){
      arrow.x=collider.x+45;
      arrow.y=collider.y-70;
      arrow_y=arrow.y;
      if(obj==Testube){
        testubeDrag=true;
      }
    }else if(obj==Rod1){
      arrow.x=collider.x+45;
      arrow.y=collider.y-70;
      arrow_y=arrow.y;
      Rod1.angle=0;
    }
    
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
    lemonDrag=false;
    testubeDrag=false;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==Testube){
      Testube.visible=false;
      arrow.visible=false;
      Testube_Back1.reset(Testube_Back1.xp,Testube_Back1.yp);
      Testube_Back1.visible=false;
      Testube_Back.visible=true;
      AceticAcid_to_Testtube_4.visible=true;
      tween1=this.game.add.tween(rackGroup).to( {x:3000, }, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        tween2=this.game.add.tween(bottleGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
        tween2.onComplete.add(function () {
          arrow.x=Dropper.x-100;
          arrow.y=Dropper.y-70;
          arrow_y=arrow.y;
          arrow.visible=true;
          voice.destroy(true);
          voice=this.game.add.audio("step1_2",1);
          voice.play();
          dialog.text="Take the Dropper";
          Dropper.inputEnabled = true;
          Dropper.input.useHandCursor = true;
          Dropper.events.onInputDown.add(this.clickOnDropper, this);
        }.bind(this));
        
      }.bind(this));
    }else if(currentobj==Drop_to_testube){
      cap.reset(cap.xp,cap.yp);
      arrow.visible=false;
      Drop_to_testube.x=collider.x+5;
      Drop_to_testube.y=collider.y-200;
      Drop_to_testube.animations.play('anim');
      //Testube_Front.visible=false;
      //AceticAcid_to_testTube.visible=true;
      AceticAcid_to_Testtube_4.animations.play('anim');
      AceticAcid_to_Testtube_4.animations.currentAnim.onComplete.add(function(){
        Drop_to_testube.visible=false;
        
        arrow.x=Spoon_4.x+100;
        arrow.y=Spoon_4.y-50;
        arrow_y=arrow.y;
        arrow.visible=true;
        voice.destroy(true);
          voice=this.game.add.audio("step4_3",1);
          voice.play();
          dialog.text="Add a small amount of solid sodium hydrogen carbonate to it.";
          Spoon_4.inputEnabled = true;
          Spoon_4.input.useHandCursor = true;
          Spoon_4.input.enableDrag(true);
      }.bind(this));
      
    }else if(currentobj==Spoon_4){   
      arrow.visible=false;
      Spoon_4.x=collider.x+10;
      Spoon_4.y=collider.y-100;
      Spoon_4.animations.play('anim');
      AceticAcid_to_Testtube_4.visible=false;
      NaHCO3_to_Acid.visible=true;
      NaHCO3_to_Acid.animations.play('anim');
      NaHCO3_to_Acid.animations.currentAnim.onComplete.add(function(){
        Spoon_4.reset(Spoon_4.xp+50,Spoon_4.yp+20);
        Spoon_4.angle=30;
        //////////////////////////////////
        NaHCO3_to_Acid.visible=false;
        Brisk_Effervesence.visible=true;
            Fog.animations.play('anim');
            Fog.visible=true;
            Brisk_Effervesence.animations.play('anim');
            voice.destroy(true);
            voice=this.game.add.audio("step4_4",1);
            voice.play();
            dialog.text="Observe the brisk effervescence.";
            Brisk_Effervesence.animations.currentAnim.onComplete.add(function(){
              Brisk_Effervesence.animations.play('anim2');
              tween1=this.game.add.tween(bottleGroup).to( {x:3000, }, 1000, Phaser.Easing.Out, true);
              tween1.onComplete.add(function () {
                Lemon0001.visible=true;
                Testube_Back1.visible=true;
                tween2=this.game.add.tween(rackGroup).to( {x:0, }, 1000, Phaser.Easing.Out, true);
                tween2.onComplete.add(function () {
                  voice.destroy(true);
                  voice=this.game.add.audio("step4_5",1);
                  voice.play();
                  dialog.text="Take the lime water to the clamp stand.";
                  collider.x+=200;
                  Lemon0001.inputEnabled = true;
                  Lemon0001.input.useHandCursor = true;
                  Lemon0001.input.enableDrag(true);
                  arrow.x=Lemon0001.x+75;
                  arrow.y=Lemon0001.y-70;
                  arrow_y=arrow.y;
                  arrow.visible=true;
                }.bind(this));
              }.bind(this));
            }.bind(this));  
            //this.game.time.events.add(Phaser.Timer.SECOND*2,this.observeBrisk, this); 
            ////////////////////////////////////////////
        // arrow.x=Testtube_holder.x+200;
        // arrow.y=Testtube_holder.y-100;
        // arrow_y=arrow.y;
        // arrow.visible=true;
        // voice.destroy(true);
        //   voice=this.game.add.audio("step_2",1);
        //   //voice.play();
        //   dialog.text="Shake well with a test tube holder and observe.";
        //   Testtube_holder.inputEnabled = true;
        //   Testtube_holder.input.useHandCursor = true;
        //   Testtube_holder.input.enableDrag(true);
        // //NaHCO3_to_Acid.visible=false;
        // //Brisk_Effervesence.visible=true;
        // //Brisk_Effervesence.animations.play('anim');
        
      }.bind(this));  
    }else if(currentobj==Testtube_holder){   
      arrow.visible=false;
      Testube_Back.visible=false;
      NaHCO3_to_Acid.visible=false;
      Testtube_holder.visible=false;
      NaHCO3_and_Acid_with_holder.visible=true;
      tween1=this.game.add.tween(NaHCO3_and_Acid_with_holder).to( {y:120, }, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        NaHCO3_and_Acid_with_holder.animations.play('anim');
        NaHCO3_and_Acid_with_holder.animations.currentAnim.onComplete.add(function(){
          tween1=this.game.add.tween(NaHCO3_and_Acid_with_holder).to( {y:420, }, 1000, Phaser.Easing.Out, true);
          tween1.onComplete.add(function () {
            Testtube_holder.reset(Testtube_holder.xp,Testtube_holder.yp);
            //Testtube_holder.visible=true;
            NaHCO3_and_Acid_with_holder.visible=false;
            Testube_Back.visible=true;
            Brisk_Effervesence.visible=true;
            Fog.animations.play('anim');
            Fog.visible=true;
            Brisk_Effervesence.animations.play('anim');
            voice.destroy(true);
            voice=this.game.add.audio("step_2",1);
            //voice.play();
            dialog.text="Observe the brisk effervescence.";
            
            //this.game.time.events.add(Phaser.Timer.SECOND*2,this.observeBrisk, this); 
          }.bind(this));  
        }.bind(this));  
      }.bind(this));   
      
    }else if(currentobj==Lemon0001){   
      arrow.visible=false;
      Testube_Back1.visible=false;
      Testube_Back2.visible=true;
      Lemon0001.visible=false;
      Lemon0002=this.game.add.sprite(1335,500, 'Lemon0001');
      //Lemon0002.visible=false;
      Lemon0002.scale.set(.85);
      Lemon0002.x=Lemon_reaction.x;
      Lemon0002.y=Lemon_reaction.y;
      tween1=this.game.add.tween(rackGroup).to( {x:3000, }, 1000, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        Rod1.visible=true;
        Rod1.events.onDragStart.add(function() {this.gameC(Rod1)}, this);
        Rod1.events.onDragStop.add(function() {this.gameC1(Rod1)}, this);
        this.game.physics.arcade.enable(Rod1);
        tween2=this.game.add.tween(Rod1).to( {x:1300, }, 1000, Phaser.Easing.Out, true);
        tween2.onComplete.add(function () {
          voice.destroy(true);
          voice=this.game.add.audio("step4_6",1);
          voice.play();
          dialog.text="Take the cork with delivery tube.";
          collider.x-=150;
          collider.scale.x=5;
          arrow.x=Rod1.x;
          arrow.y=Rod1.y-250;
          arrow_y=arrow.y;
          arrow.visible=true;
          Rod1.inputEnabled = true;
          Rod1.input.useHandCursor = true;
          Rod1.input.enableDrag(true);
        }.bind(this)); 
      }.bind(this)); 
    }else if(currentobj==Rod1){
      arrow.visible=false;
      Rod1.visible=false;
      Rod.visible=true;
      Rod.animations.play('anim');
      Rod.animations.currentAnim.onComplete.add(function(){
        Lemon_reaction.visible=true;
        Lemon0002.visible=false;
        Lemon_reaction.animations.play('anim');
        voice.destroy(true);
            voice=this.game.add.audio("step4_7",1);
            voice.play();
            dialog.text="Observe that the limewater truns milky.";
        Lemon_reaction.animations.currentAnim.onComplete.add(function(){
          Lemon_reaction.animations.play('anim2');
          this.game.time.events.add(Phaser.Timer.SECOND*3,this.nextAction, this); 
          
        }.bind(this)); 
      }.bind(this));   
      
    }
  },
  nextAction:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_8",1);
    voice.play();
    dialog.text="Click on the next button to see the observation.";
    play. visible=true;
  },
  clickOnDropper:function(){
    Dropper.angle=0;
    arrow.visible=false;
    tween1=this.game.add.tween(Dropper).to( {x:Drop_to_testube.x, y:Drop_to_testube.y}, 400, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      cap.reset(920,830);
      Dropper.visible=false;
      Drop_to_testube.visible=true;
      tween2=this.game.add.tween(Drop_to_testube).to( {y:630}, 400, Phaser.Easing.Out, true);
      tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step4_2",1);
        voice.play();
        dialog.text="Add diluted acetic acid to the test tube.";
        arrow.x=Dropper.x+40;
        arrow.y=Dropper.y+120;
        arrow_y=arrow.y;
        arrow.visible=true;
        Drop_to_testube.inputEnabled = true;
        Drop_to_testube.input.useHandCursor = true;
        Drop_to_testube.input.enableDrag(true);
        collider.x=AceticAcid_to_Testtube_4.x+30;
        collider.y=AceticAcid_to_Testtube_4.y-20;
        //collider.alpha=0;//.04
      }.bind(this));
    }.bind(this));
  },

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }/**/
    if(lemonDrag){
      Testube_Back1.x=Lemon0001.x;
      Testube_Back1.y=Lemon0001.y;
      //console.log(Testube_Back1.x+"="+Lemon0001.x);
    }
    if(testubeDrag){
      Testube_Back1.x=Testube.x;
      Testube_Back1.y=Testube.y;
      //console.log(Testube_Back1.x+"="+Lemon0001.x);
    }   
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==Testube){
        Testube_Back1.x=Testube.x;
        Testube_Back1.y=Testube.y;
        arrow.x=Testube.x+75;
        arrow.y=Testube.y-70;
        arrow_y=arrow.y;
        
      }else if(currentobj==Drop_to_testube){
        arrow.x=Drop_to_testube.x+40;
        arrow.y=Drop_to_testube.y-100;
        arrow_y=arrow.y;
        
      }else if(currentobj==Spoon_4){
        arrow.x=Spoon_4.x+100;
        arrow.y=Spoon_4.y-50;
        arrow_y=arrow.y;
        
      }else if(currentobj==Testtube_holder){
        arrow.x=Testtube_holder.x+200;
        arrow.y=Testtube_holder.y-100;
        arrow_y=arrow.y;
        
      }else if(currentobj==Lemon0001){
        Testube_Back1.x=Lemon0001.x;
        Testube_Back1.y=Lemon0001.y;
        arrow.x=Lemon0001.x+75;
        arrow.y=Lemon0001.y-70;
        arrow_y=arrow.y;
      }else if(currentobj==Rod1){
        arrow.x=Rod1.x;
        arrow.y=Rod1.y-250;
        arrow_y=arrow.y;
      }
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
    toExperiment2:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  // 
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
