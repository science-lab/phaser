var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var exp_Name;
  var currentScene;
  var previousScene;
  
  }
  
  materials.prototype ={
  
  init: function(exp_nam) 
  {
    //ip = ipadrs;
    //exp_Name=exp_nam;

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  // muteButton.inputEnabled = true;
  // muteButton.input.useHandCursor = true;
  // muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1610,880,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,880,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(310,880,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,880,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScenes, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
        //if(exp_Name=="Iron with copper sulphate"){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                
              }
              mat_img_1 = this.game.add.sprite(370,550,'M_Testube');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Test tube:",headfontStyle);
              mat_content_1="Test tube is a common piece of laboratory glassware used to,\nhold, mix, or heat small quantities of solid or liquid chemicals.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
              
              break;
          case 2:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Testtube_Stand');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.55,.7);
              mat_name_1=this.game.add.text(550,370,"Test tube stand:",headfontStyle);
              mat_content_1="Test tube racks are laboratory equipment used to hold upright\nmultiple test tubes at the same time. They are most commonly\nused when various different solutions are needed to work with\nsimultaneously, for safety reasons, for safe storage of test tubes,\nand to ease the transport of multiple tubes.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
              
          break;
          case 3:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'M_Dropper');
            mat_img_1.anchor.setTo(.5,.5);
            mat_img_1.scale.setTo(.7,.7);
            mat_name_1=this.game.add.text(550,370,"Dropper:",headfontStyle);
            mat_content_1="A dropper, also known as a Pasteur pipette, is a device used\nto transfer small quantities of liquids. They are used in the\nlaboratory.";
            mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
        break;
          case 4:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_pH_paper');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"pH Paper:",headfontStyle);
              mat_content_1="pH paper is used to determine if a solution is acidic, basic or\nneutral. This is determined by either dipping part of the paper\ninto a solution or pour drops of solution ont the paper and\nwatching the color change.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
         
          case 5:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Ui');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Universal Indicator",headfontStyle);
              mat_content_1="A universal indicator is a pH indicator made of a solution of\nseveral compounds that exhibits several smooth colour changes\nover a wide range pH values to indicate the acidity or alkalinity\nof solutions.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 6:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(980,750,'M_pH_scale');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.6,.6);
              mat_name_1=this.game.add.text(550,320,"Standard color chart",headfontStyle);
              mat_content_1="The pH indicator chart shows colours of different pH values.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 7:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_HCL');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Hydrochloric acid:",headfontStyle);
              mat_content_1="Dilute hydrochloric acid is often used in the extraction of\nbasic substances from mixtures or in the removal of basic\nimpurities. The dilute acid converts the base such as ammonia\nor an organic amine into water-soluble chloride salt.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 8:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_NaOH');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Sodium Hydroxide(NaOH):",headfontStyle);
              mat_content_1="Sodium hydroxide, also known as lye and caustic soda,\nis an inorganic compound with the formula NaOH.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          
          case 9:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Ethanoic_acid');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Ethanoic acid(Acetic acid):",headfontStyle);
              mat_content_1="Acetic acid, systematically named ethanoic acid, is a colourless\nliquid organic compound with the chemical formula CH₃COOH.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 10:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Lemon_juice');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Lemon juice:",headfontStyle);
              mat_content_1="The lemon juice contains 5% to 6% citric acid, with a pH of\naround 2.2, giving it a sour taste. ";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 11:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'M_Water');
            mat_img_1.anchor.setTo(.5,.5);
            mat_img_1.scale.setTo(.7,.7);
            mat_name_1=this.game.add.text(550,370,"Distilled Water:",headfontStyle);
            mat_content_1="Distilled water is water that has been boiled into vapor and\ncondensed back into liquid in a separate container.\nImpurities in the original water that do not boil below or near the\nboiling point of water remain in the original container. Thus,\ndistilled water is a type of purified water.";
            mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
        break;
          case 12:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_NaHCO3');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Sodium bicarbonate solution:",headfontStyle);
            mat_content_1="Sodium bicarbonate is a salt that dissociates to form sodium\nand bicarbonate ions in water. This breakdown makes a\nsolution alkaline, meaning it is able to neutralize acid.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 13:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_tile');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.2,.2);
              mat_name_1=this.game.add.text(550,370,"Glazed tile:",headfontStyle);
              mat_content_1="Glazed tiles are most often made of ceramic, which doesn't\nreact with acid/alkali.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;

          
          
          }
          

        //}
      },
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==13){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
        toNextScenes:function()
        {
        voice.destroy();
        this.state.start("Theory", true, false, ip);
        
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  