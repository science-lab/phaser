var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  var sceneCount;
  var count;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {
  
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
 muted = false;
 voice=this.game.add.audio("A_observation1",1);
 //voice1=this.game.add.audio("A_observation2",1);
 //voice2=this.game.add.audio("A_observation3",1);
 voice.play();

 //this.game.time.events.add(Phaser.Timer.SECOND*16,this.nextSound, this);
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);
 sceneCount=1;
 count=0;
 //voice2.stop();
  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "36px Segoe UI", fill: "#ffffff", align: "centre" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(110,80,'observation_table')
  //switch(exp_Name){
   // case "Iron with copper sulphate":
    sno_1="A";
    sample1="Dil HCl";
    paper_color1="Deep Red";
    paper_value1="1";
    solution_color1="Deep Red";
    solution_value1="1";

    sno_2="B";
    sample2="Dil NaOH solution";
    paper_color2="Purple";
    paper_value2="14";
    solution_color2="Purple";
    solution_value2="14";

    
    sno_3="C";
    sample3="Dil Ethanoic acid";
    paper_color3="Orange";
    paper_value3="4";
    solution_color3="Orange";
    solution_value3="4";

    
    sno_4="D";
    sample4="Lemon juice";
    paper_color4="Red";
    paper_value4="2";
    solution_color4="Red";
    solution_value4="2";

    sno_5="E";
    sample5="Water";
    paper_color5="Green";
    paper_value5="7";
    solution_color5="Green";
    solution_value5="7";

    sno_6="F";
    sample6="Dil NaHCO\u2083";
    paper_color6="Blue";
    paper_value6="10";
    solution_color6="Blue";
    solution_value6="10";


    
    //inference_3="Fe(s) + Cu\u00B2\u207A(aq)---->Fe\u00B2\u207A(aq) + Cu(s)";
    //inference_4="Grey          Blue                    Pale green    Reddish brown";/**/
    
    sno_text_1=this.game.add.text(175,400,sno_1,fontStyle);
    sample1_text_1=this.game.add.text(250,400,sample1,fontStyle);
    paper_color1_text_1=this.game.add.text(600,400,paper_color1,fontStyle);
    paper_value1_text_1=this.game.add.text(950,400,paper_value1,fontStyle);
    solution_color1_text_1=this.game.add.text(1150,400,solution_color1,fontStyle);
    solution_value1_text_1=this.game.add.text(1550,400,solution_value1,fontStyle);

    sno_text_2=this.game.add.text(175,470,sno_2,fontStyle);
    sample1_text_2=this.game.add.text(250,470,sample2,fontStyle);
    paper_color1_text_2=this.game.add.text(600,470,paper_color2,fontStyle);
    paper_value1_text_2=this.game.add.text(950,470,paper_value2,fontStyle);
    solution_color1_text_2=this.game.add.text(1150,470,solution_color2,fontStyle);
    solution_value1_text_2=this.game.add.text(1550,470,solution_value2,fontStyle);

    sno_text_3=this.game.add.text(175,540,sno_3,fontStyle);
    sample1_text_3=this.game.add.text(250,540,sample3,fontStyle);
    paper_color1_text_3=this.game.add.text(600,540,paper_color3,fontStyle);
    paper_value1_text_3=this.game.add.text(950,540,paper_value3,fontStyle);
    solution_color1_text_3=this.game.add.text(1150,540,solution_color3,fontStyle);
    solution_value1_text_3=this.game.add.text(1550,540,solution_value3,fontStyle);

    sno_text_4=this.game.add.text(175,610,sno_4,fontStyle);
    sample1_text_4=this.game.add.text(250,610,sample4,fontStyle);
    paper_color1_text_4=this.game.add.text(600,610,paper_color4,fontStyle);
    paper_value1_text_4=this.game.add.text(950,610,paper_value4,fontStyle);
    solution_color1_text_4=this.game.add.text(1150,610,solution_color4,fontStyle);
    solution_value1_text_4=this.game.add.text(1550,610,solution_value4,fontStyle);

    sno_text_5=this.game.add.text(175,680,sno_5,fontStyle);
    sample1_text_5=this.game.add.text(250,680,sample5,fontStyle);
    paper_color1_text_5=this.game.add.text(600,680,paper_color5,fontStyle);
    paper_value1_text_5=this.game.add.text(950,680,paper_value5,fontStyle);
    solution_color1_text_5=this.game.add.text(1150,680,solution_color5,fontStyle);
    solution_value1_text_5=this.game.add.text(1550,680,solution_value5,fontStyle);

    sno_text_6=this.game.add.text(175,750,sno_6,fontStyle);
    sample1_text_6=this.game.add.text(250,750,sample6,fontStyle);
    paper_color1_text_6=this.game.add.text(600,750,paper_color6,fontStyle);
    paper_value1_text_6=this.game.add.text(950,750,paper_value6,fontStyle);
    solution_color1_text_6=this.game.add.text(1150,750,solution_color6,fontStyle);
    solution_value1_text_6=this.game.add.text(1550,750,solution_value6,fontStyle);
   
    /*inference_text_3=this.game.add.text(950,700,inference_3,fontStyle);
    inference_text_4=this.game.add.text(950,750,inference_4,fontStyle_1);*/
    

   // break;
 // }
//  next_btn = this.game.add.sprite(1590,860,'components','next_disabled.png');
//  next_btn.scale.setTo(.7,.7);
//  next_btn = this.game.add.sprite(1590,860,'components','next_pressed.png');
//  next_btn.scale.setTo(.7,.7);
//  next_btn.inputEnabled = true;
//  //next_btn.input.priorityID = 3;
//  next_btn.input.useHandCursor = true;
//  next_btn.events.onInputDown.add(this.toNext, this);
//  prev_btn = this.game.add.sprite(230,860,'components','next_disabled.png');
//  prev_btn.scale.setTo(-.7,.7);
//  prev_btn = this.game.add.sprite(230,860,'components','next_pressed.png');
//  prev_btn.scale.setTo(-.7,.7);
//  prev_btn.inputEnabled = true;
//  //prev_btn.input.priorityID = 3;
//  prev_btn.input.useHandCursor = true;
//  prev_btn.events.onInputDown.add(this.toPrev, this);
//  prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=true;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },

   
    toNext:function(){
         sceneCount=2;
         voice.destroy();
         voice=this.game.add.audio("A_observation3");
         voice.play();
          //voice.stop();
         // voice1.stop();
          //voice2.play();
          sno_text_1.text="3";
          experiment_text_1.text="Reaction of dil. HCl \nwith sodium \ncarbonate ";
          observation_text_1.text="A brisk effervescence \nis observed. A colorless \nand odourless gas \nevolves. The gas turns \nlime water milky";
          inference_text_1.text="Acids on reaction with sodium \ncarbonate liberate CO\u2082, gas \nwith brisk effervescence.";
          
          sno_text_2.text="";
          experiment_text_2.text="";
          observation_text_2.text="";
          inference_text_2.text="";
          
          play.visible=true;
          next_btn.visible=false;
          prev_btn.visible=true;
        },
    toPrev:function(){
      count=0;
      sceneCount=1;
      voice.destroy();
      voice=this.game.add.audio("A_observation1");
      voice.play();
         // voice2.stop();
          //voice.play();
          //voice1.stop();
          //voice.onStop.add(this.nextSound,this);
          sno_text_1.text=sno_1;
          experiment_text_1.text=experiment_1;
          observation_text_1.text=observation_1;
          inference_text_1.text=inference_1;
          
          sno_text_2.text=sno_2;
          experiment_text_2.text=experiment_2;
          observation_text_2.text=observation_2;
          inference_text_2.text=inference_2;
          next_btn.visible=true;
          prev_btn.visible=false;
        },
    //For to next scene   
    update: function()
    {
      // if(sceneCount==1){
      //   count++;
      //   console.log(count);
      //   if(count==950){
      //     voice.destroy();
      //     voice=this.game.add.audio("A_observation2",1);
      //     voice.play();
      //   }
      //   if(count==1750){
      //     voice.destroy();
      //     voice=this.game.add.audio("A_observation3",1);
      //     voice.play();
      //   }
      // }
    },
        toResult:function()
        {
        voice.destroy();
        //voice1.destroy();
       // voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
         muted = true;
        // if(sceneCount==1)
        // {
        //   voice.stop();
        //   voice2.stop();
        //   voice1.play();
        // }
        // else if(sceneCount==2)
        // {
        //   voice.stop();
        //   voice1.stop();
        //   voice2.play();
        // }
        //voice.destroy();
        //voice2.destroy();
        // voice.destroy();
        // voice1.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    //voice.destroy();
        //voice2.destroy();
        voice.destroy();
        voice1.destroy();
        voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
    voice.destroy();
    voice1.destroy();
    voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  