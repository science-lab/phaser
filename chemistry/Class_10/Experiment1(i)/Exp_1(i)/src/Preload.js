var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        this.game.load.atlasJSONHash('Dropper_Blue_OUT', 'assets/Dropper_Blue_OUT.png', 'assets/Dropper_Blue_OUT.json');
        this.game.load.atlasJSONHash('Dropper_HCl_OUT', 'assets/Dropper_HCl_OUT.png', 'assets/Dropper_HCl_OUT.json');
        this.game.load.atlasJSONHash('Dropper_Red_OUT', 'assets/Dropper_Red_OUT.png', 'assets/Dropper_Red_OUT.json');
        this.game.load.atlasJSONHash('TestTube_Blue_in', 'assets/TestTube_Blue_in.png', 'assets/TestTube_Blue_in.json');
        this.game.load.atlasJSONHash('TestTube_HCl_in', 'assets/TestTube_HCl_in.png', 'assets/TestTube_HCl_in.json');
        this.game.load.atlasJSONHash('TestTube_Red_in', 'assets/TestTube_Red_in.png', 'assets/TestTube_Red_in.json');
        //this.game.load.atlasJSONHash('Blue_to_Red', 'assets/Blue_to_Red.png', 'assets/Blue_to_Red.json');
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('burner','assets/burner.png');
        //this.game.load.image('ferrous_sulphate1','assets/ferrous_sulphate_1.png');
        this.game.load.image('spoon','assets/spoon.png');
        this.game.load.image('stand','assets/stand.png');
        this.game.load.image('test_tube','assets/test_tube.png');
        this.game.load.image('testube_stand','assets/testube_stand.png');
        this.game.load.image('observation_table','assets/observation_new.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        //////////////////titles////////////////////////////////////////
        this.game.load.image('exp1_title','assets/titles/exp1_title.png');
        this.game.load.image('exp2_title','assets/titles/exp2_title.png');
        this.game.load.image('exp3_title','assets/titles/exp3_title.png');
        this.game.load.image('exp4_title','assets/titles/exp4_title.png');
        this.game.load.image('exp5_title','assets/titles/exp5_title.png');
        //////////////////materials////////////////////////////////////////
        //this.game.load.image('iron_nails','assets/materials/iron_nails.jpeg');
        //this.game.load.image('copper_suplphate_solution','assets/materials/copper_suplphate_solution.jpg');
        this.game.load.image('M_Testube','assets/materials/M_Testube.png');
        this.game.load.image('M_Testtube_Stand','assets/materials/M_TesttubeStand.png');
        this.game.load.image('M_Dropper','assets/materials/M_Dropper.png');
        this.game.load.image('M_pH_paper','assets/materials/M_pH_paper.png');
        this.game.load.image('M_Ui','assets/materials/M_Ui.png');
        this.game.load.image('M_pH_scale','assets/materials/M_pH_scale.png');
        // this.game.load.image('M_Cork','assets/materials/M_Cork.png');
        // this.game.load.image('M_Delivery_tube','assets/materials/M_Delivery_tube.png');
        // this.game.load.image('M_Burner','assets/materials/M_Burner.png');
        this.game.load.image('M_HCL','assets/materials/M_HCL.png');
        this.game.load.image('M_NaOH','assets/materials/M_NaOH.png');
        this.game.load.image('M_Ethanoic_acid','assets/materials/M_CH3COOH.png');
        this.game.load.image('M_Lemon_juice','assets/materials/M_Lemon.png');
        this.game.load.image('M_Water','assets/materials/M_Water.png');
        this.game.load.image('M_NaHCO3','assets/materials/M_NaHCO3.png');
        this.game.load.image('M_tile','assets/materials/M_tile.png');
///////////////////////////////Experiment_A1///////////////////////////////////////////////////////////////////////
      this.game.load.image('empty_testtube','assets/pH/TestTube_Nill.png');
      this.game.load.image('testtube_head_back','assets/pH/TestTube_Nill_Back.png');
      this.game.load.image('bottle_head_back','assets/pH/Jar_Back.png');
      this.game.load.image('testtube_stant_front','assets/pH/Wooden_stand_front.png');
      this.game.load.image('testtube_stand_back','assets/pH/Wooden_stand_back.png');
      this.game.load.image('blue_text_A','assets/pH/A_Blue.png');
      this.game.load.image('blue_text_B','assets/pH/B_Blue.png');
      this.game.load.image('blue_text_C','assets/pH/C_Blue.png');
      this.game.load.image('blue_text_D','assets/pH/D_Blue.png');
      this.game.load.image('blue_text_E','assets/pH/E_Blue.png');
      this.game.load.image('blue_text_F','assets/pH/F_Blue.png');
      this.game.load.image('green_text_A','assets/pH/A_Green.png');
      this.game.load.image('green_text_B','assets/pH/B_Green.png');
      this.game.load.image('green_text_C','assets/pH/C_Green.png');
      this.game.load.image('green_text_D','assets/pH/D_Green.png');
      this.game.load.image('green_text_E','assets/pH/E_Green.png');
      this.game.load.image('green_text_F','assets/pH/F_Green.png');
      //this.game.load.image('blue_ring','assets/pH/Circle.png');
      this.game.load.image('glazed_tile','assets/pH/Tile.png');
      this.game.load.image('HCl_bottle','assets/pH/HCL.png');
      this.game.load.image('NaOH_bottle','assets/pH/NaOH.png');
      this.game.load.image('CH3COOH_bottle','assets/pH/CH3COOH.png');
      this.game.load.image('Lemon_bottle','assets/pH/Lemon.png');
      this.game.load.image('Water_bottle','assets/pH/Water.png');
      this.game.load.image('NaHCO3_bottle','assets/pH/NaHCO3.png');
      this.game.load.image('UI_bottle','assets/pH/UI.png');
      this.game.load.image('color_chart','assets/pH/pH_color_chart.png');
      this.game.load.image('color1','assets/pH/1.png');
      this.game.load.image('color2','assets/pH/2.png');
      this.game.load.image('color3','assets/pH/3.png');
      this.game.load.image('color4','assets/pH/4.png');
      this.game.load.image('color5','assets/pH/5.png');
      this.game.load.image('color6','assets/pH/6.png');
      this.game.load.image('color7','assets/pH/7.png');
      this.game.load.image('color8','assets/pH/8.png');
      this.game.load.image('color9','assets/pH/9.png');
      this.game.load.image('color10','assets/pH/10.png');
      this.game.load.image('color11','assets/pH/11.png');
      this.game.load.image('color12','assets/pH/12.png');
      this.game.load.image('color13','assets/pH/13.png');
      this.game.load.image('color14','assets/pH/14.png');

      this.game.load.image('cork','assets/pH/cap.png');
      this.game.load.atlasJSONHash('HCl_litmus_sprite','assets/pH/anim/HCL_litmus_sprite.png', 'assets/pH/anim/HCL_litmus_sprite.json');
      this.game.load.atlasJSONHash('NaOH_litmus_sprite','assets/pH/anim/NaOH_litmus_sprite.png', 'assets/pH/anim/NaOH_litmus_sprite.json');
      this.game.load.atlasJSONHash('CH3COOH_litmus_sprite','assets/pH/anim/CH3COOH_litmus_sprite.png', 'assets/pH/anim/CH3COOH_litmus_sprite.json');
      this.game.load.atlasJSONHash('Lemon_litmus_sprite','assets/pH/anim/Lemon_litmus_sprite.png', 'assets/pH/anim/Lemon_litmus_sprite.json');
      this.game.load.atlasJSONHash('Water_litmus_sprite','assets/pH/anim/Water_litmus_sprite.png', 'assets/pH/anim/Water_litmus_sprite.json');
      this.game.load.atlasJSONHash('NaHCO3_litmus_sprite','assets/pH/anim/NaHCO3_litmus_sprite.png', 'assets/pH/anim/NaHCO3_litmus_sprite.json');
      this.game.load.atlasJSONHash('HCl_dropper_sprite','assets/pH/anim/HCL_dropper_out_sprite.png', 'assets/pH/anim/HCL_dropper_out_sprite.json');
      this.game.load.atlasJSONHash('Lemon_dropper_sprite','assets/pH/anim/Lemon_dropper_out_sprite.png', 'assets/pH/anim/Lemon_dropper_out_sprite.json');
      this.game.load.atlasJSONHash('Water_dropper_sprite','assets/pH/anim/Water_dropper_out_sprite.png', 'assets/pH/anim/Water_dropper_out_sprite.json');
      this.game.load.atlasJSONHash('NaHCO3_dropper_sprite','assets/pH/anim/NaHCO3_dropper_out_sprite.png', 'assets/pH/anim/NaHCO3_dropper_out_sprite.json');
      this.game.load.atlasJSONHash('UI_dropper_sprite','assets/pH/anim/UI_dropper_out_sprite.png', 'assets/pH/anim/UI_dropper_out_sprite.json');

      this.game.load.atlasJSONHash('HCl_to_testtube_sprite','assets/pH/anim/HCL_to_testtube_sprite.png', 'assets/pH/anim/HCL_to_testtube_sprite.json');
      this.game.load.atlasJSONHash('Lemon_to_testtube_sprite','assets/pH/anim/Lemon_to_testtube_sprite.png', 'assets/pH/anim/Lemon_to_testtube_sprite.json');
      this.game.load.atlasJSONHash('Water_to_testtube','assets/pH/anim/Water_to_testtube.png', 'assets/pH/anim/Water_to_testtube.json');
      this.game.load.atlasJSONHash('NaHCO3_to_testtube_sprite','assets/pH/anim/NaHCO3_to_testtube_sprite.png', 'assets/pH/anim/NaHCO3_to_testtube_sprite.json');
      
      this.game.load.atlasJSONHash('UI_to_hcl_sprite','assets/pH/anim/UI_to_hcl_sprite.png', 'assets/pH/anim/UI_to_hcl_sprite.json');
      this.game.load.atlasJSONHash('UI_to_naoh_sprite','assets/pH/anim/UI_to_naoh_sprite.png', 'assets/pH/anim/UI_to_naoh_sprite.json');
      this.game.load.atlasJSONHash('UI_to_ch3cooh_sprite','assets/pH/anim/UI_to_ch3cooh_sprite.png', 'assets/pH/anim/UI_to_ch3cooh_sprite.json');
      this.game.load.atlasJSONHash('UI_to_lemon_sprite','assets/pH/anim/UI_to_lemon_sprite.png', 'assets/pH/anim/UI_to_lemon_sprite.json');
      this.game.load.atlasJSONHash('UI_to_water_sprite','assets/pH/anim/UI_to_water_sprite.png', 'assets/pH/anim/UI_to_water_sprite.json');
      this.game.load.atlasJSONHash('UI_to_nahco3_sprite','assets/pH/anim/UI_to_nahco3_sprite.png', 'assets/pH/anim/UI_to_nahco3_sprite.json');




      this.game.load.atlasJSONHash('HCl_drop_sprite','assets/pH/anim/HCL_drop_sprite.png', 'assets/pH/anim/HCL_drop_sprite.json');
      this.game.load.atlasJSONHash('Lemon_drop_sprite','assets/pH/anim/Lemon_drop_sprite.png', 'assets/pH/anim/Lemon_drop_sprite.json');
      this.game.load.atlasJSONHash('Water_drop_sprite','assets/pH/anim/Water_drop_sprite.png', 'assets/pH/anim/Water_drop_sprite.json');
      this.game.load.atlasJSONHash('NaHCO3_drop_sprite','assets/pH/anim/NaHCO3_drop_sprite.png', 'assets/pH/anim/NaHCO3_drop_sprite.json');

      this.game.load.atlasJSONHash('blue_ring_sprite','assets/pH/anim/Circle_sprite.png', 'assets/pH/anim/Circle_sprite.json');
///////////////////////////////Experiment_A2///////////////////////////////////////////////////////////////////////
        this.game.load.atlasJSONHash('Black_smoke', 'assets/HCL_zinc/Black_smoke.png', 'assets/HCL_zinc/Black_smoke.json');
        this.game.load.atlasJSONHash('Flame', 'assets/HCL_zinc/Flame.png', 'assets/HCL_zinc/Flame.json'); 
        this.game.load.atlasJSONHash('Blue_flame', 'assets/HCL_zinc/Blue_flame.png', 'assets/HCL_zinc/Blue_flame.json'); 
        this.game.load.atlasJSONHash('Cork_tube_testtube', 'assets/HCL_zinc/Cork_tube_testtube.png', 'assets/HCL_zinc/Cork_tube_testtube.json'); 
        this.game.load.atlasJSONHash('White_smoke', 'assets/HCL_zinc/White_smoke.png', 'assets/HCL_zinc/White_smoke.json'); 
        this.game.load.atlasJSONHash('Zn_testtube', 'assets/HCL_zinc/Zn_testtube.png', 'assets/HCL_zinc/Zn_testtube.json'); 
        this.game.load.atlasJSONHash('HCl_testtube', 'assets/HCL_zinc/HCl_testtube.png', 'assets/HCL_zinc/HCl_testtube.json');
        this.game.load.image('Testtube_a2','assets/HCL_zinc/Testtube.png');
        this.game.load.image('Spoon','assets/HCL_zinc/Spoon.png');
        this.game.load.image('Spoon_with_Zn','assets/HCL_zinc/Spoon_with_Zn.png');
        this.game.load.image('Zn_plate','assets/HCL_zinc/Zn_plate.png');
        this.game.load.image('Testtube_Cork_rod','assets/HCL_zinc/Testtube_Cork_rod.png');
        this.game.load.image('Match_Box','assets/HCL_zinc/Match_Box.png');
        this.game.load.image('Match_Stick','assets/HCL_zinc/Match_Stick.png');
        this.game.load.image('Burned_Match_Stick','assets/HCL_zinc/Burned_Match_Stick.png');
///////////////////////////////////////////////HCl_NaCO3//////////////////////////////////////////////////////
        this.game.load.image('A3_testtube1','assets/HCl_NaCO3/Testtube_full.png');
        this.game.load.image('A3_testtube_front','assets/HCl_NaCO3/Testtube_front.png');
        this.game.load.image('A3_testtube_back','assets/HCl_NaCO3/Testtube_back.png');
        this.game.load.image('A3_Spoon','assets/HCl_NaCO3/Spoon.png');
        this.game.load.image('A3_Spoon_NaCO3','assets/HCl_NaCO3/Spoon_NaCO3.png');
        this.game.load.image('A3_Cork_tube','assets/HCl_NaCO3/Cork_tube.png');
        this.game.load.image('A3_tube','assets/HCl_NaCO3/Delivery_tube.png');
        this.game.load.image('A3_Lime_testtube_full','assets/HCl_NaCO3/Lime_water_Testube_top.png');
        this.game.load.image('A3_Lime_testtube','assets/HCl_NaCO3/Lime_water_Testube.png');
        this.game.load.image('A3_NaCO3','assets/HCl_NaCO3/NaCO3_plate.png');

        this.game.load.atlasJSONHash('Spoon_sprite','assets/HCl_NaCO3/Spoon_sprite/Spoon.png','assets/HCl_NaCO3/Spoon_sprite/Spoon.json');
        this.game.load.atlasJSONHash('NaCO3_testtube_sprite','assets/HCl_NaCO3/NaCO3_testtube_sprite/NaCO3_testtube.png','assets/HCl_NaCO3/NaCO3_testtube_sprite/NaCO3_testtube.json');
        this.game.load.atlasJSONHash('Lime_sprite','assets/HCl_NaCO3/Lime_sprite/Lime.png','assets/HCl_NaCO3/Lime_sprite/Lime.json');
        this.game.load.atlasJSONHash('HCl_testtube_sprite','assets/HCl_NaCO3/HCl_testtube_sprite/HCl_testtube.png','assets/HCl_NaCO3/HCl_testtube_sprite/HCl_testtube.json');
        this.game.load.atlasJSONHash('Glass_tube_sprite','assets/HCl_NaCO3/Glass_tube_sprite/Glass_tube.png','assets/HCl_NaCO3/Glass_tube_sprite/Glass_tube.json');
        this.game.load.atlasJSONHash('A3_Dropper_HCl_OUT', 'assets/HCl_NaCO3/Dropper_dorp_sprite/Dropper_drop.png', 'assets/HCl_NaCO3/Dropper_dorp_sprite/Dropper_drop.json');
        this.game.load.atlasJSONHash('A3_white_smoke', 'assets/HCl_NaCO3/White_smoke_sprite/White_smoke.png', 'assets/HCl_NaCO3/White_smoke_sprite/White_smoke.json');
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




  /////////////////////Experiment_A1_sounds////////////////////////////////////
        this.game.load.audio('A_aim','assets/audio/Experiment_A1/A_aim.mp3');
        this.game.load.audio('A_aim2','assets/audio/Experiment_A1/A_aim2.mp3');
        this.game.load.audio('A_precaution1','assets/audio/Experiment_A1/A_precaution1.mp3');
        this.game.load.audio('A_precaution2','assets/audio/Experiment_A1/A_precaution2.mp3');
        this.game.load.audio('A_procedure_voice1','assets/audio/Experiment_A1/A_procedure_voice1.mp3');
        this.game.load.audio('A_procedure_voice2','assets/audio/Experiment_A1/A_procedure_voice2.mp3');
        this.game.load.audio('A_procedure_voice3','assets/audio/Experiment_A1/A_procedure_voice3.mp3');
        this.game.load.audio('A_procedure_voice4','assets/audio/Experiment_A1/A_procedure_voice4.mp3');
        

        this.game.load.audio('A_procedure1','assets/audio/Experiment_A1/A_procedure1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Experiment_A1/A_procedure2.mp3');
        this.game.load.audio('A_procedure3','assets/audio/Experiment_A1/A_procedure3.mp3');
        this.game.load.audio('A_procedure4','assets/audio/Experiment_A1/A_procedure4.mp3');
        this.game.load.audio('A_procedure5','assets/audio/Experiment_A1/A_procedure5.mp3');
        this.game.load.audio('A_procedure6','assets/audio/Experiment_A1/A_procedure6.mp3');
        this.game.load.audio('A_procedure7','assets/audio/Experiment_A1/A_procedure7.mp3');
        this.game.load.audio('A_procedure8','assets/audio/Experiment_A1/A_procedure8.mp3');
        this.game.load.audio('A_procedure9','assets/audio/Experiment_A1/A_procedure9.mp3');
        this.game.load.audio('A_procedure10','assets/audio/Experiment_A1/A_procedure10.mp3');
        this.game.load.audio('A_procedure11','assets/audio/Experiment_A1/A_procedure11.mp3');
        this.game.load.audio('A_procedure12','assets/audio/Experiment_A1/A_procedure12.mp3');
        this.game.load.audio('A_procedure13','assets/audio/Experiment_A1/A_procedure13.mp3');
        this.game.load.audio('A_procedure14','assets/audio/Experiment_A1/A_procedure14.mp3');
        this.game.load.audio('A_procedure15','assets/audio/Experiment_A1/A_procedure15.mp3');
        this.game.load.audio('A_procedure16','assets/audio/Experiment_A1/A_procedure16.mp3');
        this.game.load.audio('A_procedure17','assets/audio/Experiment_A1/A_procedure17.mp3');
        this.game.load.audio('A_procedure18','assets/audio/Experiment_A1/A_procedure18.mp3');
        this.game.load.audio('A_procedure19','assets/audio/Experiment_A1/A_procedure19.mp3');
        this.game.load.audio('A_procedure20','assets/audio/Experiment_A1/A_procedure20.mp3');
        this.game.load.audio('A_procedure21','assets/audio/Experiment_A1/A_procedure21.mp3');
        this.game.load.audio('A_procedure22','assets/audio/Experiment_A1/A_procedure22.mp3');
        this.game.load.audio('A_procedure23','assets/audio/Experiment_A1/A_procedure23.mp3');
        this.game.load.audio('A_procedure24','assets/audio/Experiment_A1/A_procedure24.mp3');
        this.game.load.audio('A_procedure25','assets/audio/Experiment_A1/A_procedure25.mp3');
        this.game.load.audio('A_procedure26','assets/audio/Experiment_A1/A_procedure26.mp3');
        this.game.load.audio('A_procedure27','assets/audio/Experiment_A1/A_procedure27.mp3');
        this.game.load.audio('A_procedure28','assets/audio/Experiment_A1/A_procedure28.mp3');
        this.game.load.audio('A_procedure29','assets/audio/Experiment_A1/A_procedure29.mp3');
        this.game.load.audio('A_procedure30','assets/audio/Experiment_A1/A_procedure30.mp3');
        this.game.load.audio('A_procedure31','assets/audio/Experiment_A1/A_procedure31.mp3');
        this.game.load.audio('A_procedure32','assets/audio/Experiment_A1/A_procedure32.mp3');
        this.game.load.audio('A_procedure33','assets/audio/Experiment_A1/A_procedure33.mp3');



        this.game.load.audio('A_observation1','assets/audio/Experiment_A1/A_observation1.mp3');
        this.game.load.audio('A_observation2','assets/audio/Experiment_A1/A_observation2.mp3');
        this.game.load.audio('A_observation3','assets/audio/Experiment_A1/A_observation3.mp3');
        this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
  /////////////////////Experiment_A2_sounds////////////////////////////////////      
        this.game.load.audio('A2_procedure1','assets/audio/Experiment_A2/A2_procedure1.mp3');
        this.game.load.audio('A2_procedure2','assets/audio/Experiment_A2/A2_procedure2.mp3');
        this.game.load.audio('A2_procedure3','assets/audio/Experiment_A2/A2_procedure3.mp3');
        this.game.load.audio('A2_procedure4','assets/audio/Experiment_A2/A2_procedure4.mp3');
        this.game.load.audio('A2_procedure5','assets/audio/Experiment_A2/A2_procedure5.mp3');
        this.game.load.audio('A2_procedure6','assets/audio/Experiment_A2/A2_procedure6.mp3');
        this.game.load.audio('A2_procedure7','assets/audio/Experiment_A2/A2_procedure7.mp3');
        this.game.load.audio('A2_procedure8','assets/audio/Experiment_A2/A2_procedure8.mp3');
        this.game.load.audio('A2_procedure9','assets/audio/Experiment_A2/A2_procedure9.mp3');
        this.game.load.audio('A2_procedure10','assets/audio/Experiment_A2/A2_procedure10.mp3');
        this.game.load.audio('A2_procedure11','assets/audio/Experiment_A2/A2_procedure11.mp3');
        this.game.load.audio('A2_procedure12','assets/audio/Experiment_A2/A2_procedure12.mp3');
        this.game.load.audio('A2_procedure13','assets/audio/Experiment_A2/A2_procedure13.mp3');
        this.game.load.audio('A2_procedure14','assets/audio/Experiment_A2/A2_procedure14.mp3');
        this.game.load.audio('A2_procedure15','assets/audio/Experiment_A2/A2_procedure15.mp3');
        this.game.load.audio('A2_procedure16','assets/audio/Experiment_A2/A2_procedure16.mp3');
        this.game.load.audio('A2_procedure17','assets/audio/Experiment_A2/A2_procedure17.mp3');
        this.game.load.audio('A2_procedure18','assets/audio/Experiment_A2/A2_procedure18.mp3');
        this.game.load.audio('A2_procedure19','assets/audio/Experiment_A2/A2_procedure19.mp3');
        this.game.load.audio('A2_procedure20','assets/audio/Experiment_A2/A2_procedure20.mp3');
        this.game.load.audio('A2_procedure21','assets/audio/Experiment_A2/A2_procedure21.mp3');
        this.game.load.audio('A2_procedure22','assets/audio/Experiment_A2/A2_procedure22.mp3');
        this.game.load.audio('A2_procedure23','assets/audio/Experiment_A2/A2_procedure23.mp3');
        this.game.load.audio('A2_procedure24','assets/audio/Experiment_A2/A2_procedure24.mp3');
        this.game.load.audio('A2_procedure25','assets/audio/Experiment_A2/A2_procedure25.mp3');
        this.game.load.audio('A2_procedure26','assets/audio/Experiment_A2/A2_procedure26.mp3');
        this.game.load.audio('A2_procedure27','assets/audio/Experiment_A2/A2_procedure27.mp3');
        this.game.load.audio('A2_procedure28','assets/audio/Experiment_A2/A2_procedure28.mp3');
        this.game.load.audio('A2_procedure29','assets/audio/Experiment_A2/A2_procedure29.mp3');
        this.game.load.audio('A2_procedure30','assets/audio/Experiment_A2/A2_procedure30.mp3');
        this.game.load.audio('A2_procedure31','assets/audio/Experiment_A2/A2_procedure31.mp3');
        this.game.load.audio('A2_procedure32','assets/audio/Experiment_A2/A2_procedure32.mp3');
        this.game.load.audio('A2_procedure33','assets/audio/Experiment_A2/A2_procedure33.mp3');


        this.game.load.audio('A2_procedure_voice1','assets/audio/Experiment_A2/A2_procedure_voice1.mp3');
/////////////////////Experiment_A3_sounds////////////////////////////////////           
        this.game.load.audio('A3_procedure1','assets/audio/Experiment_A3/A_procedure1.mp3');
        this.game.load.audio('A3_procedure2','assets/audio/Experiment_A3/A_procedure2.mp3');
        this.game.load.audio('A3_procedure3','assets/audio/Experiment_A3/A_procedure3.mp3');
        this.game.load.audio('A3_procedure4','assets/audio/Experiment_A3/A_procedure4.mp3');
        this.game.load.audio('A3_procedure5','assets/audio/Experiment_A3/A_procedure5.mp3');
        this.game.load.audio('A3_procedure6','assets/audio/Experiment_A3/A_procedure6.mp3');
        this.game.load.audio('A3_procedure7','assets/audio/Experiment_A3/A_procedure7.mp3');
        this.game.load.audio('A3_procedure8','assets/audio/Experiment_A3/A_procedure8.mp3');
        this.game.load.audio('A3_procedure9','assets/audio/Experiment_A3/A_procedure9.mp3');
        this.game.load.audio('popsound','assets/audio/Experiment_A2/pop_sound.mp3');
        
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
     // loc="https://scienceapp.in/swadhyaya/science_lab_V/lab_main.php?parent_id=14";
      //this.game.state.start("Exp_Selection");//Starting the gametitle state

      //this.game.state.start("Experiment_A2");//Starting the gametitle state
     // this.game.state.start("Aim");//Simulation_hot1

      //this.game.state.start("Experiment_A1");//Starting the gametitle state
      this.game.state.start("Aim");//Simulation_hot1

      //this.game.state.start("Theory");//Simulation_hot1
     // this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_A2");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Materials");//Starting the gametitle state
      //this.game.state.start("Viva");//Starting the gametitle state
	}
}

