var experiment_a1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var HCl_step;
var colorChangingAnimstart;
var TostartTesttubeB;

var isHCl_dropper1_taken;
var isHCl_dropper2_taken;
var isNaOH_dropper1_taken;
var isNaOH_dropper2_taken;
var isCH3COOH_dropper1_taken;
var isCH3COOH_dropper2_taken;
var isLemon_dropper1_taken;
var isLemon_dropper2_taken;
var isWater_dropper1_taken;
var isWater_dropper2_taken;
var isNaHCO3_dropper1_taken;
var isNaHCO3_dropper2_taken;
var count;
var colorval;
var iscolorCheking;
var isHClColor_checked;
var isNaOHColor_checked;
var isCH3COOHColor_checked;
var isLemonColor_checked;
var isWaterColor_checked;
var isNaHCO3Color_checked;
var colors;
}

experiment_a1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
  voice=this.game.add.audio('A_procedure1',1);
  voice.play();
//  voice2=this.game.add.audio('sim_2',1);
//  procedure_voice1=this.game.add.audio('A_procedure1',1);
//  procedure_voice2=this.game.add.audio('A_procedure2',1);
//  procedure_voice3=this.game.add.audio('A_procedure3',1);
//  procedure_voice4=this.game.add.audio('A_procedure4',1);
//  procedure_voice5=this.game.add.audio('A_procedure5',1);
//  procedure_voice6=this.game.add.audio('A_procedure6',1);
//  procedure_voice7=this.game.add.audio('A_procedure7',1);
//  procedure_voice8=this.game.add.audio('A_procedure8',1);
//  procedure_voice9=this.game.add.audio('A_procedure9',1);
//  procedure_voice10=this.game.add.audio('A_procedure10',1);
//  procedure_voice11=this.game.add.audio('A_procedure11',1);
 
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


//  HCl_step=0;
//  colorChangingAnimstart=false;
//  delay=0;
//  TostartTesttubeB=false;
isHCl_dropper1_taken=false;
isHCl_dropper2_taken=false;
isNaOH_dropper1_taken=false;
isNaOH_dropper2_taken=false;
isCH3COOH_dropper1_taken=false;
isCH3COOH_dropper2_taken=false;
isLemon_dropper1_taken=false;
isLemon_dropper2_taken=false;
isWater_dropper1_taken=false;
isWater_dropper2_taken=false;
isNaHCO3_dropper1_taken=false;
isNaHCO3_dropper2_taken=false;
count=0;
iscolorCheking=false;
isHClColor_checked=false;
isNaOHColor_checked=false;
isCH3COOHColor_checked=false;
isLemonColor_checked=false;
isWaterColor_checked=false;
isNaHCO3Color_checked=false;

colors=['color1','color2','color3','color4','color5','color6','color7','color8','color9','color10','color11','color12','color13','color14'];
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
  fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

  this.arrangeScene();

      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.82,.8);
      dialog_text="Take 6 test tubes and label them as A, B, C, D, E, and F";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.HCl_group_tween_in, this);
      //procedure_voice1.play();
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
////////////////////////Add Items on the scene///////////////////////////
arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
 //holder1.visible=false;
 //holder2.visible=false;
 var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.lineStyle(10,0xff704d,1);
  maskBg1.beginFill(0xffffcc);
  maskBg1.drawRect(10, 200, 300, 680);
  maskBg1.alpha=1;

  var line1=this.game.add.graphics(0, 0);
      line1.lineStyle(6,0xff704d,1);
      line1.moveTo(10, 260);
      line1.lineTo(315, 260);
  var line2=this.game.add.graphics(0, 0);
      line2.lineStyle(6,0xff704d,1);
      line2.moveTo(100, 200);
      line2.lineTo(100, 880);
  var line3=this.game.add.graphics(0, 0);
      line3.lineStyle(6,0xff704d,1);
      line3.moveTo(210, 200);
      line3.lineTo(210, 880);
      heading1=this.game.add.text(110,210,"Color",fontStyle3);
      heading2=this.game.add.text(240,210,"pH",fontStyle3);
      heading3=this.game.add.text(30,210,"Key",fontStyle3);

      table_group=this.game.add.group();
      table_group.add(maskBg1);
      table_group.add(line1);
      table_group.add(line2);
      table_group.add(line3);
      table_group.add(heading1);
      table_group.add(heading2);
      table_group.add(heading3);
      table_group.x=-2500;//-2500

      A_color=this.game.add.text(102,280,"",fontStyle4);
     // A_color.visible=false;
      A_color_val=this.game.add.text(240,280,"",fontStyle4);
     // A_color_val.visible=false;
      B_color=this.game.add.text(102,400,"",fontStyle4);
     // B_color.visible=false;
      B_color_val=this.game.add.text(240,400,"",fontStyle4);
     // B_color_val.visible=false;
      C_color=this.game.add.text(102,490,"",fontStyle4);
     // C_color.visible=false;
      C_color_val=this.game.add.text(240,490,"",fontStyle4);
     // C_color_val.visible=false;
      D_color=this.game.add.text(102,590,"",fontStyle4);
     // D_color.visible=false;
      D_color_val=this.game.add.text(240,590,"",fontStyle4);
     // D_color_val.visible=false;
      E_color=this.game.add.text(102,700,"",fontStyle4);
     // E_color.visible=false;
      E_color_val=this.game.add.text(240,700,"",fontStyle4);
     // E_color_val.visible=false;
      F_color=this.game.add.text(102,800,"",fontStyle4);
     // F_color.visible=false;
      F_color_val=this.game.add.text(240,800,"",fontStyle4);
     // F_color_val.visible=false;

 
 glazed_tile1=this.game.add.sprite(150,810,'glazed_tile');
 glazed_tile1.scale.setTo(1,.6);
 
 HCl_litmus=this.game.add.sprite(310,880,'HCl_litmus_sprite','LSPaper_Hcl_colour0001.png');
 HCl_litmus.scale.setTo(.5,.5);
 HCl_litmus.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11],24,false,true);
 
 HCl_litmus_A_text=this.game.add.sprite(385,940,'blue_text_A');
 HCl_litmus_A_text.scale.setTo(.5,.5);
 HCl_litmus_A_text2=this.game.add.sprite(385,940,'green_text_A');
 HCl_litmus_A_text2.scale.setTo(.5,.5);
 HCl_litmus_A_text2.visible=false;

 HCl_litmus_collider=this.game.add.sprite(340,790,'collider');
 HCl_litmus_collider.scale.setTo(3,3);
 HCl_litmus_collider.alpha=0;
//  HCl_litmus_label=this.game.add.text(390,860,'HCl',labelfontStyle1);
//  HCl_litmus_label.visible=false;

 NaOH_litmus=this.game.add.sprite(505,880,'NaOH_litmus_sprite','NaOH_LSPaper_colour0001.png');
 NaOH_litmus.scale.setTo(.5,.5);
 NaOH_litmus.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11],24,false,true);
 
 NaOH_litmus_B_text=this.game.add.sprite(580,940,'blue_text_B');
 NaOH_litmus_B_text.scale.setTo(.5,.5);
 NaOH_litmus_B_text2=this.game.add.sprite(580,940,'green_text_B');
 NaOH_litmus_B_text2.scale.setTo(.5,.5);
 NaOH_litmus_B_text2.visible=false;


 NaOH_litmus_collider=this.game.add.sprite(535,790,'collider');
 NaOH_litmus_collider.scale.setTo(3,3);
 NaOH_litmus_collider.alpha=0;
//  NaOH_litmus_label=this.game.add.text(570,860,'NaOH',labelfontStyle1);
//  NaOH_litmus_label.visible=false;

 CH3COOH_litmus=this.game.add.sprite(700,880,'CH3COOH_litmus_sprite','CH3COOH_LSPaper_colour0001.png');
 CH3COOH_litmus.scale.setTo(.5,.5);
 CH3COOH_litmus.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11],24,false,true);
 
 CH3COOH_litmus_C_text=this.game.add.sprite(775,940,'blue_text_C');
 CH3COOH_litmus_C_text.scale.setTo(.5,.5);
 CH3COOH_litmus_C_text2=this.game.add.sprite(775,940,'green_text_C');
 CH3COOH_litmus_C_text2.scale.setTo(.5,.5);
 CH3COOH_litmus_C_text2.visible=false;


 CH3COOH_litmus_collider=this.game.add.sprite(730,790,'collider');
 CH3COOH_litmus_collider.scale.setTo(3,3);
 CH3COOH_litmus_collider.alpha=0;
//  CH3COOH_litmus_label=this.game.add.text(735,860,'CH\u2083COOH',labelfontStyle1);
//  CH3COOH_litmus_label.visible=false;

 Lemon_litmus=this.game.add.sprite(895,880,'Lemon_litmus_sprite','Lemon_LSPaper_colour0001.png');
 Lemon_litmus.scale.setTo(.5,.5);
 Lemon_litmus.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11],24,false,true);
 
 Lemon_litmus_D_text=this.game.add.sprite(970,940,'blue_text_D');
 Lemon_litmus_D_text.scale.setTo(.5,.5);
 Lemon_litmus_D_text2=this.game.add.sprite(970,940,'green_text_D');
 Lemon_litmus_D_text2.scale.setTo(.5,.5);
 Lemon_litmus_D_text2.visible=false;



 Lemon_litmus_collider=this.game.add.sprite(925,790,'collider');
 Lemon_litmus_collider.scale.setTo(3,3);
 Lemon_litmus_collider.alpha=0;
//  Lemon_litmus_label=this.game.add.text(950,860,'Lemon',labelfontStyle1);
//  Lemon_litmus_label.visible=false;

 Water_litmus=this.game.add.sprite(1090,880,'Water_litmus_sprite','Water_LSPaper_colour0001.png');
 Water_litmus.scale.setTo(.5,.5);
 Water_litmus.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11],24,false,true);
 
 Water_litmus_E_text=this.game.add.sprite(1165,940,'blue_text_E');
 Water_litmus_E_text.scale.setTo(.5,.5);
 Water_litmus_E_text2=this.game.add.sprite(1165,940,'green_text_E');
 Water_litmus_E_text2.scale.setTo(.5,.5);
 Water_litmus_E_text2.visible=false;

 Water_litmus_collider=this.game.add.sprite(1120,790,'collider');
 Water_litmus_collider.scale.setTo(3,3);
 Water_litmus_collider.alpha=0;
//  Water_litmus_label=this.game.add.text(1150,860,'Water',labelfontStyle1);
//  Water_litmus_label.visible=false;

 NaHCO3_litmus=this.game.add.sprite(1285,880,'NaHCO3_litmus_sprite','NaHCo3_LSPaper_colour0001.png');
 NaHCO3_litmus.scale.setTo(.5,.5);
 NaHCO3_litmus.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11],24,false,true);
 
 NaHCO3_litmus_F_text=this.game.add.sprite(1360,940,'blue_text_F');
 NaHCO3_litmus_F_text.scale.setTo(.5,.5);
 NaHCO3_litmus_F_text2=this.game.add.sprite(1360,940,'green_text_F');
 NaHCO3_litmus_F_text2.scale.setTo(.5,.5);
 NaHCO3_litmus_F_text2.visible=false;

 NaHCO3_litmus_collider=this.game.add.sprite(1315,790,'collider');
 NaHCO3_litmus_collider.scale.setTo(3,3);
 NaHCO3_litmus_collider.alpha=0;
//  NaHCO3_litmus_label=this.game.add.text(1330,860,'NaHCO\u2083',labelfontStyle1);
//  NaHCO3_litmus_label.visible=false;

 Litmus_group=this.game.add.group();
 Litmus_group.add(glazed_tile1);
 Litmus_group.add(HCl_litmus);
 Litmus_group.add(HCl_litmus_A_text);
 Litmus_group.add(HCl_litmus_A_text2);
 Litmus_group.add(NaOH_litmus);
 Litmus_group.add(NaOH_litmus_B_text);
 Litmus_group.add(NaOH_litmus_B_text2);
 Litmus_group.add(CH3COOH_litmus);
 Litmus_group.add(CH3COOH_litmus_C_text);
 Litmus_group.add(CH3COOH_litmus_C_text2);
 Litmus_group.add(Lemon_litmus);
 Litmus_group.add(Lemon_litmus_D_text);
 Litmus_group.add(Lemon_litmus_D_text2);
 Litmus_group.add(Water_litmus);
 Litmus_group.add(Water_litmus_E_text);
 Litmus_group.add(Water_litmus_E_text2);
 Litmus_group.add(NaHCO3_litmus);
 Litmus_group.add(NaHCO3_litmus_F_text);
 Litmus_group.add(NaHCO3_litmus_F_text2);
 ////////litmus goup position must chage to -2500////
 Litmus_group.x=-2500;
 Litmus_group.visible=true;
 
 ///////////////////////////////////////////////////////////////////////////////////
 HCl_drop=this.game.add.sprite(403,850,'HCl_drop_sprite','Hcl_Drop0001.png');
 HCl_drop.scale.setTo(.8,.6);
 HCl_drop.animations.add('anim1',[0,1,2,3],10,false,true);
 HCl_drop.visible=false;
 NaOH_drop=this.game.add.sprite(600,850,'HCl_drop_sprite','Hcl_Drop0001.png');
 NaOH_drop.scale.setTo(.8,.6  );
 NaOH_drop.animations.add('anim1',[0,1,2,3],10,false,true);
 NaOH_drop.visible=false;
 CH3COOH_drop=this.game.add.sprite(794,850,'HCl_drop_sprite','Hcl_Drop0001.png');
 CH3COOH_drop.scale.setTo(.8,.6);
 CH3COOH_drop.animations.add('anim1',[0,1,2,3],10,false,true);
 CH3COOH_drop.visible=false;
 Lemon_drop=this.game.add.sprite(987,850,'Lemon_drop_sprite','Lemon_Drop0001.png');
 Lemon_drop.scale.setTo(.8,.6);
 Lemon_drop.animations.add('anim1',[0,1,2,3],10,false,true);
 Lemon_drop.visible=false;
 Water_drop=this.game.add.sprite(1182,850,'Water_drop_sprite','Water_Drop0001.png');
 Water_drop.scale.setTo(.8,.6);
 Water_drop.animations.add('anim1',[0,1,2,3],10,false,true);
 Water_drop.visible=false;
 NaHCO3_drop=this.game.add.sprite(1378,850,'NaHCO3_drop_sprite','NahCo3_Drop0001.png');
 NaHCO3_drop.scale.setTo(.8,.6);
 NaHCO3_drop.animations.add('anim1',[0,1,2,3],10,false,true);
 NaHCO3_drop.visible=false;
////////////////////////////////////////////////////////////////////////////////////

// HCl_dropper1=this.game.add.sprite(625,726,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
// HCl_dropper1.scale.setTo(1,1.3);
// HCl_dropper1.anchor.setTo(.5,.5);
// HCl_dropper1.angle=0;
///////////////////////////////////////////////////////////////////////////////////////
HCl_dropper1=this.game.add.sprite(1580,845,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
HCl_dropper1.scale.setTo(1,1.3);
HCl_dropper1.anchor.setTo(.5,.5);
HCl_dropper1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
HCl_dropper1.angle=90;

HCl_dropper2=this.game.add.sprite(2500,800,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
HCl_dropper2.scale.setTo(1,1.3);
HCl_dropper2.anchor.setTo(.5,.5);
HCl_dropper2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
HCl_dropper2.angle=90;
// HCl_dropper1.inputEnabled = false;
// HCl_dropper1.input.useHandCursor = false;
HCl_dropper1.events.onInputDown.add(this.clickOnHCl_dropper1, this);

HCl_bottle_back=this.game.add.sprite(1717,654,'bottle_head_back');
HCl_bottle_back.scale.setTo(.27,.3);

HCl_cork=this.game.add.sprite(1749,650,'cork');
HCl_cork.scale.setTo(.9,.9);
HCl_bottle=this.game.add.sprite(1700,650,'HCl_bottle');
HCl_bottle.scale.setTo(1.1,1.1);

HCl_group=this.game.add.group();
HCl_group.add(HCl_dropper1);
HCl_group.add(HCl_bottle_back);
HCl_group.add(HCl_cork);
HCl_group.add(HCl_bottle);
HCl_group.visible=true;
HCl_group.x=2500;
//////////////////////////////////////////////////////////////////////////////////
NaOH_dropper1=this.game.add.sprite(1580,845,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
NaOH_dropper1.scale.setTo(1,1.3);
NaOH_dropper1.anchor.setTo(.5,.5);
NaOH_dropper1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
NaOH_dropper1.angle=90;

NaOH_dropper2=this.game.add.sprite(2500,800,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
NaOH_dropper2.scale.setTo(1,1.3);
NaOH_dropper2.anchor.setTo(.5,.5);
NaOH_dropper2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
NaOH_dropper2.angle=90;

// NaOH_dropper1.inputEnabled = true;
// NaOH_dropper1.input.useHandCursor = true;
NaOH_dropper1.events.onInputDown.add(this.clickOnNaOH_dropper1, this);

NaOH_bottle_back=this.game.add.sprite(1717,654,'bottle_head_back');
NaOH_bottle_back.scale.setTo(.27,.3);

NaOH_cork=this.game.add.sprite(1749,650,'cork');
NaOH_cork.scale.setTo(.9,.9);
NaOH_bottle=this.game.add.sprite(1700,650,'NaOH_bottle');
NaOH_bottle.scale.setTo(1.1,1.1);

NaOH_group=this.game.add.group();
NaOH_group.add(NaOH_dropper1);
NaOH_group.add(NaOH_bottle_back);
NaOH_group.add(NaOH_cork);
NaOH_group.add(NaOH_bottle);
NaOH_group.visible=true;
NaOH_group.x=2500;
///////////////////////////////////////////////////////////////////////////
CH3COOH_dropper1=this.game.add.sprite(1580,845,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
CH3COOH_dropper1.scale.setTo(1,1.3);
CH3COOH_dropper1.anchor.setTo(.5,.5);
CH3COOH_dropper1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
CH3COOH_dropper1.angle=90;

CH3COOH_dropper2=this.game.add.sprite(2500,800,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
CH3COOH_dropper2.scale.setTo(1,1.3);
CH3COOH_dropper2.anchor.setTo(.5,.5);
CH3COOH_dropper2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
CH3COOH_dropper2.angle=90;
// NaOH_dropper1.inputEnabled = true;
// NaOH_dropper1.input.useHandCursor = true;
CH3COOH_dropper1.events.onInputDown.add(this.clickOnCH3COOH_dropper1, this);

CH3COOH_bottle_back=this.game.add.sprite(1717,654,'bottle_head_back');
CH3COOH_bottle_back.scale.setTo(.27,.3);

CH3COOH_cork=this.game.add.sprite(1749,650,'cork');
CH3COOH_cork.scale.setTo(.9,.9);
CH3COOH_bottle=this.game.add.sprite(1700,650,'CH3COOH_bottle');
CH3COOH_bottle.scale.setTo(1.1,1.1);

CH3COOH_group=this.game.add.group();
CH3COOH_group.add(CH3COOH_dropper1);
CH3COOH_group.add(CH3COOH_bottle_back);
CH3COOH_group.add(CH3COOH_cork);
CH3COOH_group.add(CH3COOH_bottle);
CH3COOH_group.visible=true;
CH3COOH_group.x=2500;
///////////////////////////////////////////////////////////////////////////
Lemon_dropper1=this.game.add.sprite(1580,845,'Lemon_dropper_sprite','Lemon_dropper_to_testube0016.png');
Lemon_dropper1.scale.setTo(1,1.3);
Lemon_dropper1.anchor.setTo(.5,.5);
Lemon_dropper1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
Lemon_dropper1.angle=90;

Lemon_dropper2=this.game.add.sprite(2500,800,'Lemon_dropper_sprite','Lemon_dropper_to_testube0016.png');
Lemon_dropper2.scale.setTo(1,1.3);
Lemon_dropper2.anchor.setTo(.5,.5);
Lemon_dropper2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
Lemon_dropper2.angle=90;
// NaOH_dropper1.inputEnabled = true;
// NaOH_dropper1.input.useHandCursor = true;
Lemon_dropper1.events.onInputDown.add(this.clickOnLemon_dropper1, this);

Lemon_bottle_back=this.game.add.sprite(1717,654,'bottle_head_back');
Lemon_bottle_back.scale.setTo(.27,.3);

Lemon_cork=this.game.add.sprite(1749,650,'cork');
Lemon_cork.scale.setTo(.9,.9);
Lemon_bottle=this.game.add.sprite(1700,650,'Lemon_bottle');
Lemon_bottle.scale.setTo(1.1,1.1);

Lemon_group=this.game.add.group();
Lemon_group.add(Lemon_dropper1);
Lemon_group.add(Lemon_bottle_back);
Lemon_group.add(Lemon_cork);
Lemon_group.add(Lemon_bottle);
Lemon_group.visible=true;
Lemon_group.x=2500;
///////////////////////////////////////////////////////////////////////////
Water_dropper1=this.game.add.sprite(1580,845,'Water_dropper_sprite','Water_dropper_to_testube0016.png');
Water_dropper1.scale.setTo(1,1.3);
Water_dropper1.anchor.setTo(.5,.5);
Water_dropper1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
Water_dropper1.angle=90;

Water_dropper2=this.game.add.sprite(2500,800,'Water_dropper_sprite','Water_dropper_to_testube0016.png');
Water_dropper2.scale.setTo(1,1.3);
Water_dropper2.anchor.setTo(.5,.5);
Water_dropper2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
Water_dropper2.angle=90;
// NaOH_dropper1.inputEnabled = true;
// NaOH_dropper1.input.useHandCursor = true;
Water_dropper1.events.onInputDown.add(this.clickOnWater_dropper1, this);

Water_bottle_back=this.game.add.sprite(1717,654,'bottle_head_back');
Water_bottle_back.scale.setTo(.27,.3);

Water_cork=this.game.add.sprite(1749,650,'cork');
Water_cork.scale.setTo(.9,.9);
Water_bottle=this.game.add.sprite(1700,650,'Water_bottle');
Water_bottle.scale.setTo(1.1,1.1);

Water_group=this.game.add.group();
Water_group.add(Water_dropper1);
Water_group.add(Water_bottle_back);
Water_group.add(Water_cork);
Water_group.add(Water_bottle);
Water_group.visible=true;
Water_group.x=2500;
///////////////////////////////////////////////////////////////////////////
NaHCO3_dropper1=this.game.add.sprite(1580,845,'NaHCO3_dropper_sprite','NaHCo3_dropper_to_testube0016.png');
NaHCO3_dropper1.scale.setTo(1,1.3);
NaHCO3_dropper1.anchor.setTo(.5,.5);
NaHCO3_dropper1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
NaHCO3_dropper1.angle=90;

NaHCO3_dropper2=this.game.add.sprite(2500,800,'NaHCO3_dropper_sprite','NaHCo3_dropper_to_testube0016.png');
NaHCO3_dropper2.scale.setTo(1,1.3);
NaHCO3_dropper2.anchor.setTo(.5,.5);
NaHCO3_dropper2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
NaHCO3_dropper2.angle=90;
// NaOH_dropper1.inputEnabled = true;
// NaOH_dropper1.input.useHandCursor = true;
NaHCO3_dropper1.events.onInputDown.add(this.clickOnNaHCO3_dropper1, this);

NaHCO3_bottle_back=this.game.add.sprite(1717,654,'bottle_head_back');
NaHCO3_bottle_back.scale.setTo(.27,.3);

NaHCO3_cork=this.game.add.sprite(1749,650,'cork');
NaHCO3_cork.scale.setTo(.9,.9);
NaHCO3_bottle=this.game.add.sprite(1700,650,'NaHCO3_bottle');
NaHCO3_bottle.scale.setTo(1.1,1.1);

NaHCO3_group=this.game.add.group();
NaHCO3_group.add(NaHCO3_dropper1);
NaHCO3_group.add(NaHCO3_bottle_back);
NaHCO3_group.add(NaHCO3_cork);
NaHCO3_group.add(NaHCO3_bottle);
NaHCO3_group.visible=true;
NaHCO3_group.x=2500;
////////////////////////////////////////////////////////////////////////////
cork=this.game.add.sprite(1749,900,'cork');
cork.scale.setTo(.9,.9);
cork.visible=false;
///////////////////table val/////////////////////////////////////////
table_A_text=this.game.add.sprite(40,280,'blue_text_A');
table_A_text.scale.setTo(.5,.5);
table_A_text.visible=false;
table_B_text=this.game.add.sprite(40,400,'blue_text_B');
table_B_text.scale.setTo(.5,.5);
table_B_text.visible=false;
table_C_text=this.game.add.sprite(40,490,'blue_text_C');
table_C_text.scale.setTo(.5,.5);
table_C_text.visible=false;
table_D_text=this.game.add.sprite(40,590,'blue_text_D');
table_D_text.scale.setTo(.5,.5);
table_D_text.visible=false;
table_E_text=this.game.add.sprite(40,700,'blue_text_E');
table_E_text.scale.setTo(.5,.5);
table_E_text.visible=false;
table_F_text=this.game.add.sprite(40,800,'blue_text_F');
table_F_text.scale.setTo(.5,.5);
table_F_text.visible=false;

//////////////////////////////////////////////////////////////////////

 ///////////////////////Test tube stand 1////////////////////////////////////
 testtube_stant1_back=this.game.add.sprite(400,380,'testtube_stand_back');
 testtube_stant1_back.scale.setTo(1,1.1);
 testtube_A_back=this.game.add.sprite(539,417,'testtube_head_back');
 testtube_A_back.scale.setTo(.6,.6);
 testtube_A=this.game.add.sprite(535,395,'HCl_to_testtube_sprite','Dill_HCL_to_testube0001.png');
 testtube_A.scale.setTo(.6,.76);
 testtube_A.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],24,false,true);
 
 testtube_A_text=this.game.add.sprite(547,470,'blue_text_A');
 testtube_A_text.scale.setTo(.5,.5);



 testtube_A_collider=this.game.add.sprite(529, 310,'collider');
 testtube_A_collider.scale.setTo(1.5,3);
 testtube_A_collider.alpha=0;
 this.game.physics.arcade.enable(testtube_A_collider);//For colling purpose
 testtube_A_collider.inputEnabled = true;
 testtube_A_collider.enableBody =true;

 testtube_B_back=this.game.add.sprite(624,417,'testtube_head_back');
 testtube_B_back.scale.setTo(.6,.6);
 testtube_B=this.game.add.sprite(620,395,'HCl_to_testtube_sprite','Dill_HCL_to_testube0001.png');
 testtube_B.scale.setTo(.6,.76);
 testtube_B.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],24,false,true);
 
 testtube_B_text=this.game.add.sprite(632,470,'blue_text_B');
 testtube_B_text.scale.setTo(.5,.5);

 testtube_B_collider=this.game.add.sprite(614, 310,'collider');
 testtube_B_collider.scale.setTo(1.5,3);
 testtube_B_collider.alpha=0;
 
 testtube_C_back=this.game.add.sprite(709,417,'testtube_head_back');
 testtube_C_back.scale.setTo(.6,.6);
 testtube_C=this.game.add.sprite(705,395,'HCl_to_testtube_sprite','Dill_HCL_to_testube0001.png');
 testtube_C.scale.setTo(.6,.76);
 testtube_C.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],24,false,true);
 
 testtube_C_text=this.game.add.sprite(717,470,'blue_text_C');
 testtube_C_text.scale.setTo(.5,.5);


 testtube_C_collider=this.game.add.sprite(699, 310,'collider');
 testtube_C_collider.scale.setTo(1.5,3);
 testtube_C_collider.alpha=0;
 testtube_stant1_front=this.game.add.sprite(400,380,'testtube_stant_front');
 testtube_stant1_front.scale.setTo(1,1.1);
///////////////////////////////////////////////////////////////////////
////////////////////////////Test tube stand 2///////////////////////////
 testtube_stant2_back=this.game.add.sprite(900,380,'testtube_stand_back');
 testtube_stant2_back.scale.setTo(1,1.1);
 testtube_D_back=this.game.add.sprite(1039,417,'testtube_head_back');
 testtube_D_back.scale.setTo(.6,.6);
 testtube_D=this.game.add.sprite(1035,395,'Lemon_to_testtube_sprite','Lemon Juice_to_testube0001.png');
 testtube_D.scale.setTo(.6,.76);
 testtube_D.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],24,false,true);
 
 testtube_D_text=this.game.add.sprite(1047,470,'blue_text_D');
 testtube_D_text.scale.setTo(.5,.5);


 testtube_D_collider=this.game.add.sprite(1030, 310,'collider');
 testtube_D_collider.scale.setTo(1.5,3);
 testtube_D_collider.alpha=0;

 testtube_E_back=this.game.add.sprite(1124,417,'testtube_head_back');
 testtube_E_back.scale.setTo(.6,.6);
 testtube_E=this.game.add.sprite(1120,395,'Water_to_testtube','Dill_Water_to_testube0001.png');
 testtube_E.scale.setTo(.6,.76);
 testtube_E.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],24,false,true);
 
 testtube_E_text=this.game.add.sprite(1132,470,'blue_text_E');
 testtube_E_text.scale.setTo(.5,.5);



 testtube_E_collider=this.game.add.sprite(1114, 310,'collider');
 testtube_E_collider.scale.setTo(1.5,3);
 testtube_E_collider.alpha=0;

 testtube_F_back=this.game.add.sprite(1209,417,'testtube_head_back');
 testtube_F_back.scale.setTo(.6,.6);
 testtube_F=this.game.add.sprite(1205,395,'NaHCO3_to_testtube_sprite','NaHCo3_to_testube0001.png');
 testtube_F.scale.setTo(.6,.76);
 testtube_F.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23],24,false,true);
 
 testtube_F_text=this.game.add.sprite(1217,470,'blue_text_F');
 testtube_F_text.scale.setTo(.5,.5);
 


 testtube_F_collider=this.game.add.sprite(1199, 310,'collider');
 testtube_F_collider.scale.setTo(1.5,3);
 testtube_F_collider.alpha=0;

 testtube_stant2_front=this.game.add.sprite(900,380,'testtube_stant_front');
 testtube_stant2_front.scale.setTo(1,1.1);

 testtube_stand_group=this.game.add.group();
 testtube_stand_group.add(testtube_stant1_back);
 testtube_stand_group.add(testtube_A_back);
 testtube_stand_group.add(testtube_A);
 testtube_stand_group.add(testtube_A_text);
 testtube_stand_group.add(testtube_B_back);
 testtube_stand_group.add(testtube_B);
 testtube_stand_group.add(testtube_B_text);
 testtube_stand_group.add(testtube_C_back);
 testtube_stand_group.add(testtube_C);
 testtube_stand_group.add(testtube_C_text);
 testtube_stand_group.add(testtube_stant1_front);
 testtube_stand_group.add(testtube_stant2_back);
 testtube_stand_group.add(testtube_D_back);
 testtube_stand_group.add(testtube_D);
 testtube_stand_group.add(testtube_D_text);
 testtube_stand_group.add(testtube_E_back);
 testtube_stand_group.add(testtube_E);
 testtube_stand_group.add(testtube_E_text);
 testtube_stand_group.add(testtube_F_back);
 testtube_stand_group.add(testtube_F);
 testtube_stand_group.add(testtube_F_text);
 testtube_stand_group.add(testtube_stant2_front);
 testtube_stand_group.x=-2500;

 tween1=this.game.add.tween(testtube_stand_group).to( {x:0}, 600, Phaser.Easing.Out, true);
 //////////////////////////////////////color chart//////////////////////////////////////////////////////
 color_chart=this.game.add.sprite(360,500,'color_chart');//x=-2500;
 color_chart.scale.setTo(.7,.7);
 color_chart.alpha=1;

 color1=this.game.add.sprite(402,577,'color1');
 color1.scale.setTo(.69,.72);
 color1.alpha=1;
 color1.colorval=1;
 color1.events.onDragStart.add(function() {this.onDragStart(color1)}, this);
 color1.events.onDragStop.add(function() {this.onDragStop(color1)}, this);
 color1.inputEnabled = true;
 color1.input.useHandCursor = true;
 color1.input.enableDrag(false);
 color1.game.physics.arcade.enable(color1);
 color1.enableBody =false;

 

 color2=this.game.add.sprite(474,577,'color2');
 color2.scale.setTo(.69,.72);
 color2.alpha=1;
 color2.colorval=2;
 color2.events.onDragStart.add(function() {this.onDragStart(color2)}, this);
 color2.events.onDragStop.add(function() {this.onDragStop(color2)}, this);
 color2.inputEnabled = true;
 color2.input.useHandCursor = true;
 color2.input.enableDrag(false);
 color2.game.physics.arcade.enable(color2);
 color2.enableBody =false;

 color3=this.game.add.sprite(547,577,'color3');
 color3.scale.setTo(.69,.72);
 color3.alpha=1;
 color3.colorval=3;
 color3.events.onDragStart.add(function() {this.onDragStart(color3)}, this);
 color3.events.onDragStop.add(function() {this.onDragStop(color3)}, this);
 color3.inputEnabled = true;
 color3.input.useHandCursor = true;
 color3.input.enableDrag(false);
 color3.game.physics.arcade.enable(color3);
 color3.enableBody =false;

 color4=this.game.add.sprite(618,577,'color4');
 color4.scale.setTo(.69,.72);
 color4.alpha=1;
 color4.colorval=4;
 color4.events.onDragStart.add(function() {this.onDragStart(color4)}, this);
 color4.events.onDragStop.add(function() {this.onDragStop(color4)}, this);
 color4.inputEnabled = true;
 color4.input.useHandCursor = true;
 color4.input.enableDrag(false);
 color4.game.physics.arcade.enable(color4);
 color4.enableBody =false;

 color5=this.game.add.sprite(691,577,'color5');
 color5.scale.setTo(.69,.72);
 color5.alpha=1;
 color5.colorval=5;
 color5.events.onDragStart.add(function() {this.onDragStart(color5)}, this);
 color5.events.onDragStop.add(function() {this.onDragStop(color5)}, this);
 color5.inputEnabled = true;
 color5.input.useHandCursor = true;
 color5.input.enableDrag(false);
 color5.game.physics.arcade.enable(color5);
 color5.enableBody =false;

 color6=this.game.add.sprite(763,577,'color6');
 color6.scale.setTo(.69,.72);
 color6.alpha=1;
 color6.colorval=6;
 color6.events.onDragStart.add(function() {this.onDragStart(color6)}, this);
 color6.events.onDragStop.add(function() {this.onDragStop(color6)}, this);
 color6.inputEnabled = true;
 color6.input.useHandCursor = true;
 color6.input.enableDrag(false);
 color6.game.physics.arcade.enable(color6);
 color6.enableBody =false;

 color7=this.game.add.sprite(835,577,'color7');
 color7.scale.setTo(.69,.72);
 color7.alpha=1;
 color7.colorval=7;
 color7.events.onDragStart.add(function() {this.onDragStart(color7)}, this);
 color7.events.onDragStop.add(function() {this.onDragStop(color7)}, this);
 color7.inputEnabled = true;
 color7.input.useHandCursor = true;
 color7.input.enableDrag(false);
 color7.game.physics.arcade.enable(color7);
 color7.enableBody =false;

 color8=this.game.add.sprite(907,577,'color8');
 color8.scale.setTo(.69,.72);
 color8.alpha=1;
 color8.colorval=8;
 color8.events.onDragStart.add(function() {this.onDragStart(color8)}, this);
 color8.events.onDragStop.add(function() {this.onDragStop(color8)}, this);
 color8.inputEnabled = true;
 color8.input.useHandCursor = true;
 color8.input.enableDrag(false);
 color8.game.physics.arcade.enable(color8);
 color8.enableBody =false;

 color9=this.game.add.sprite(980,577,'color9');
 color9.scale.setTo(.69,.72);
 color9.alpha=1;
 color9.colorval=9;
 color9.events.onDragStart.add(function() {this.onDragStart(color9)}, this);
 color9.events.onDragStop.add(function() {this.onDragStop(color9)}, this);
 color9.inputEnabled = true;
 color9.input.useHandCursor = true;
 color9.input.enableDrag(false);
 color9.game.physics.arcade.enable(color9);
 color9.enableBody =false;

 color10=this.game.add.sprite(1051,577,'color10');
 color10.scale.setTo(.69,.72);
 color10.alpha=1;
 color10.colorval=10;
 color10.events.onDragStart.add(function() {this.onDragStart(color10)}, this);
 color10.events.onDragStop.add(function() {this.onDragStop(color10)}, this);
 color10.inputEnabled = true;
 color10.input.useHandCursor = true;
 color10.input.enableDrag(false);
 color10.game.physics.arcade.enable(color10);
 color10.enableBody =false;

 color11=this.game.add.sprite(1125,577,'color11');
 color11.scale.setTo(.69,.72);
 color11.alpha=1;
 color11.colorval=11;
 color11.events.onDragStart.add(function() {this.onDragStart(color11)}, this);
 color11.events.onDragStop.add(function() {this.onDragStop(color11)}, this);
 color11.inputEnabled = true;
 color11.input.useHandCursor = true;
 color11.input.enableDrag(false);
 color11.game.physics.arcade.enable(color11);
 color11.enableBody =false;

 color12=this.game.add.sprite(1197,577,'color12');
 color12.scale.setTo(.69,.72);
 color12.alpha=1;
 color12.colorval=12;
 color12.events.onDragStart.add(function() {this.onDragStart(color12)}, this);
 color12.events.onDragStop.add(function() {this.onDragStop(color12)}, this);
 color12.inputEnabled = true;
 color12.input.useHandCursor = true;
 color12.input.enableDrag(false);
 color12.game.physics.arcade.enable(color12);
 color12.enableBody =false;

 color13=this.game.add.sprite(1270,577,'color13');
 color13.scale.setTo(.69,.72);
 color13.alpha=1;
 color13.colorval=13;
 color13.events.onDragStart.add(function() {this.onDragStart(color13)}, this);
 color13.events.onDragStop.add(function() {this.onDragStop(color13)}, this);
 color13.inputEnabled = true;
 color13.input.useHandCursor = true;
 color13.input.enableDrag(false);
 color13.game.physics.arcade.enable(color13);
 color13.enableBody =false;

 color14=this.game.add.sprite(1342,577,'color14');
 color14.scale.setTo(.69,.72);
 color14.alpha=1;
 color14.colorval=14;
 color14.events.onDragStart.add(function() {this.onDragStart(color14)}, this);
 color14.events.onDragStop.add(function() {this.onDragStop(color14)}, this);
 color14.inputEnabled = true;
 color14.input.useHandCursor = true;
 color14.input.enableDrag(false);
 color14.game.physics.arcade.enable(color14);
 color14.enableBody =false;

 color_chart_group=this.game.add.group();
 color_chart_group.add(color_chart);
 color_chart_group.add(color1);
 color_chart_group.add(color2);
 color_chart_group.add(color3);
 color_chart_group.add(color4);
 color_chart_group.add(color5);
 color_chart_group.add(color6);
 color_chart_group.add(color7);
 color_chart_group.add(color8);
 color_chart_group.add(color9);
 color_chart_group.add(color10);
 color_chart_group.add(color11);
 color_chart_group.add(color12);
 color_chart_group.add(color13);
 color_chart_group.add(color14);
 color_chart_group.x=-2500;


 
//  color_ring=this.game.add.sprite(417,630,'blue_ring');
//  color_ring.scale.setTo(.7,.7);
//  color_ring.visible=true;

 one_ring=this.game.add.sprite(417,630,'blue_ring_sprite','Circle.png');
 one_ring.scale.setTo(.7,.7);
 one_ring.animations.add('anim1',[0,1],24,true,true);
 one_ring.visible=false;
 two_ring=this.game.add.sprite(490,630,'blue_ring_sprite','Circle.png');
 two_ring.scale.setTo(.7,.7);
 two_ring.animations.add('anim1',[0,1],24,true,true);
 two_ring.visible=false;
 four_ring=this.game.add.sprite(635,630,'blue_ring_sprite','Circle.png');
 four_ring.scale.setTo(.7,.7);
 four_ring.animations.add('anim1',[0,1],24,true,true);
 four_ring.visible=false;
 seven_ring=this.game.add.sprite(850,630,'blue_ring_sprite','Circle.png');
 seven_ring.scale.setTo(.7,.7);
 seven_ring.animations.add('anim1',[0,1],24,true,true);
 seven_ring.visible=false;
 ten_ring=this.game.add.sprite(1078,630,'blue_ring_sprite','Circle.png');
 ten_ring.scale.setTo(.7,.7);
 ten_ring.animations.add('anim1',[0,1],24,true,true);
 ten_ring.visible=false;
 fourteen_ring=this.game.add.sprite(1360,630,'blue_ring_sprite','Circle.png');
 fourteen_ring.scale.setTo(.7,.7);
 fourteen_ring.animations.add('anim1',[0,1],24,true,true);
 fourteen_ring.visible=false;

///////////////////////////////////////////////////////////////////////////////////////////////
HCl_dropper3=this.game.add.sprite(565,480,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
HCl_dropper3.scale.setTo(1,1.3);
HCl_dropper3.anchor.setTo(.5,.5);
HCl_dropper3.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
HCl_dropper3.visible=true;
HCl_dropper3.alpha=0;
//HCl_dropper3.angle=90;

NaOH_dropper3=this.game.add.sprite(650,480,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
NaOH_dropper3.scale.setTo(1,1.3);
NaOH_dropper3.anchor.setTo(.5,.5);
NaOH_dropper3.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
NaOH_dropper3.visible=true;
NaOH_dropper3.alpha=0;
//NaOH_dropper3.angle=90;

CH3COOH_dropper3=this.game.add.sprite(735,480,'HCl_dropper_sprite','Dill_HCL_dropper_to_testube0016.png');
CH3COOH_dropper3.scale.setTo(1,1.3);
CH3COOH_dropper3.anchor.setTo(.5,.5);
CH3COOH_dropper3.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
CH3COOH_dropper3.visible=true;
CH3COOH_dropper3.alpha=0;

//CH3COOH_dropper3.angle=90;

Lemon_dropper3=this.game.add.sprite(1065,480,'Lemon_dropper_sprite','Lemon_dropper_to_testube0016.png');
Lemon_dropper3.scale.setTo(1,1.3);
Lemon_dropper3.anchor.setTo(.5,.5);
Lemon_dropper3.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
Lemon_dropper3.visible=true;
Lemon_dropper3.alpha=0;
//Lemon_dropper3.angle=90;

Water_dropper3=this.game.add.sprite(1150,480,'Water_dropper_sprite','Water_dropper_to_testube0016.png');
Water_dropper3.scale.setTo(1,1.3);
Water_dropper3.anchor.setTo(.5,.5);
Water_dropper3.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
Water_dropper3.visible=true;
Water_dropper3.alpha=0;
//Water_dropper3.angle=90;

NaHCO3_dropper3=this.game.add.sprite(1235,480,'NaHCO3_dropper_sprite','NaHCo3_dropper_to_testube0016.png');
NaHCO3_dropper3.scale.setTo(1,1.3);
NaHCO3_dropper3.anchor.setTo(.5,.5);
NaHCO3_dropper3.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],24,false,true);
NaHCO3_dropper3.visible=true;
NaHCO3_dropper3.alpha=0;
///NaHCO3_dropper3.angle=90;


//////////////////////////////////////////////////////////////////////////////////


 collider_A = this.game.add.sprite(315, 555,'collider');
 collider_A.alpha=0;
 this.game.physics.arcade.enable(collider_A);//For colling purpose
 collider_A.inputEnabled = true;
 collider_A.enableBody =true;
 collider_B = this.game.add.sprite(515, 555,'collider');
 collider_B.alpha=0;
 this.game.physics.arcade.enable(collider_B);//For colling purpose
  
arrow=this.game.add.sprite(1500,450, 'arrow');
      arrow.angle=90;
      arrow_y=480;
      arrow.visible=false;


      // arrow.x=1580;
      // arrow.y=650;
      // arrow_y=650;
      // arrow.visible=true;

/*arrow.visible=false;
        tween1=this.game.add.tween(rackGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
        tween1.onComplete.add(function(){this.tween_rackExit()}, this);*/

},

onDragStart:function(obj)
  {
    // //obj.angle=0;
     obj.body.enable =false;
    currentobj=obj;
     if(currentobj==HCl_dropper1){
       currentobj.xp=1780;
       currentobj.yp=720;
       arrow.x=576;
       arrow.y=250;
       arrow_y=250;
       arrow.visible=true;

       this.game.physics.arcade.enable(testtube_A_collider);//For colling purpose
       testtube_A_collider.inputEnabled = true;
       testtube_A_collider.enableBody =true;
    //   //collider=TestTubeStand_Hanger_A;
    }
    else if(currentobj==NaOH_dropper1){
      currentobj.xp=1780;
      currentobj.yp=720;
      arrow.x=665;
      arrow.y=250;
      arrow_y=250;
      arrow.visible=true;
      this.game.physics.arcade.enable(testtube_B_collider);//For colling purpose
      testtube_B_collider.inputEnabled = true;
      testtube_B_collider.enableBody =true;
      //collider=TestTubeStand_Hanger_B;
    }
    else if(currentobj==CH3COOH_dropper1)
    {
       //Dropper_Blue.frameName = "Dropper_Blue_OUT_0006.png";
      currentobj.xp=1780;
      currentobj.yp=720;
      arrow.x=750;
      arrow.y=250;
      arrow_y=250;
       arrow.visible=true;
       this.game.physics.arcade.enable(testtube_C_collider);//For colling purpose
       testtube_C_collider.inputEnabled = true;
       testtube_C_collider.enableBody =true;
     }
    else if(currentobj==Lemon_dropper1)
    {
      currentobj.xp=1780;
      currentobj.yp=720;
      arrow.x=1078;
      arrow.y=250;
      arrow_y=250;
      arrow.visible=true;
      this.game.physics.arcade.enable(testtube_D_collider);//For colling purpose
      testtube_D_collider.inputEnabled = true;
      testtube_D_collider.enableBody =true;
    }
    else if(currentobj==Water_dropper1)
    {
      currentobj.xp=1780;
      currentobj.yp=720;
      arrow.x=1164;
      arrow.y=250;
      arrow_y=250;
      arrow.visible=true;
      this.game.physics.arcade.enable(testtube_E_collider);//For colling purpose
      testtube_E_collider.inputEnabled = true;
      testtube_E_collider.enableBody =true;
    }
    else if(currentobj==NaHCO3_dropper1)
    {
      currentobj.xp=1780;
      currentobj.yp=720;
      arrow.x=1245;
      arrow.y=250;
      arrow_y=250;
      arrow.visible=true;
      this.game.physics.arcade.enable(testtube_F_collider);//For colling purpose
      testtube_F_collider.inputEnabled = true;
      testtube_F_collider.enableBody =true;
    }

    // else if(currentobj==HCl_dropper2)
    // {
    //   currentobj.xp=565;
    //   currentobj.yp=460;
    //   arrow.x=430;
    //   arrow.y=730;
    //   arrow_y=730;
    //   arrow.visible=true;
    //   this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
    //   HCl_litmus_collider.inputEnabled = true;
    //   HCl_litmus_collider.enableBody =true;
    // }

    else if(currentobj==HCl_dropper3)
    {
      currentobj.xp=565;
      currentobj.yp=460;
      arrow.x=430;
      arrow.y=730;
      arrow_y=730;
      arrow.visible=true;
      HCl_dropper3.alpha=1;
      HCl_dropper2.visible=false;
      this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
      HCl_litmus_collider.inputEnabled = true;
      HCl_litmus_collider.enableBody =true;
    }
    else if(currentobj==NaOH_dropper3)
    {
      currentobj.xp=650;
      currentobj.yp=460;
      arrow.x=625;
      arrow.y=730;
      arrow_y=730;
      arrow.visible=true;
      NaOH_dropper3.alpha=1;
      NaOH_dropper2.visible=false;
      this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
      NaOH_litmus_collider.inputEnabled = true;
      NaOH_litmus_collider.enableBody =true;
    }
    else if(currentobj==CH3COOH_dropper3)
    {
      currentobj.xp=735;
      currentobj.yp=460;
      arrow.x=820;
      arrow.y=730;
      arrow_y=730;
      arrow.visible=true;
      CH3COOH_dropper3.alpha=1;
      CH3COOH_dropper2.visible=false;
      this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
      CH3COOH_litmus_collider.inputEnabled = true;
      CH3COOH_litmus_collider.enableBody =true;
    }

    else if(currentobj==Lemon_dropper3)
    {
      currentobj.xp=1065;
      currentobj.yp=460;
      arrow.x=1010;
      arrow.y=730;
      arrow_y=730;
      arrow.visible=true;
      Lemon_dropper3.alpha=1;
      Lemon_dropper2.visible=false;
      this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
      Lemon_litmus_collider.inputEnabled = true;
      Lemon_litmus_collider.enableBody =true;
    }
    else if(currentobj==Water_dropper3)
    {
      currentobj.xp=1150;
      currentobj.yp=460;
      arrow.x=1210;
      arrow.y=730;
      arrow_y=730;
      arrow.visible=true;
      Water_dropper3.alpha=1;
      Water_dropper2.visible=false;
      this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
      Water_litmus_collider.inputEnabled = true;
      Water_litmus_collider.enableBody =true;
    }

    else if(currentobj==NaHCO3_dropper3)
    {
      currentobj.xp=1235;
      currentobj.yp=460;
      arrow.x=1400;
      arrow.y=730;
      arrow_y=730;
      arrow.visible=true;
      NaHCO3_dropper3.alpha=1;
      NaHCO3_dropper2.visible=false;
      this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
      NaHCO3_litmus_collider.inputEnabled = true;
      NaHCO3_litmus_collider.enableBody =true;
    }

    else if(currentobj==color1)
    {
      currentobj.xp=402;
        currentobj.yp=577;
        
        

      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }    
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color2)
    {
      currentobj.xp=474;
        currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color3)
    {
      currentobj.xp=547;
        currentobj.yp=577;
      if(isHClColor_checked==false)
      {        
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color4)
    {
      currentobj.xp=618;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {     
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color5)
    {
      currentobj.xp=691;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {      
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color6)
    {
      currentobj.xp=763;
        currentobj.yp=577;
      if(isHClColor_checked==false)
      {      
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color7)
    {
      currentobj.xp=835;
        currentobj.yp=577;
      if(isHClColor_checked==false)
      {       
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color8)
    {
      currentobj.xp=907;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color9)
    {
      currentobj.xp=980;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color10)
    {
      currentobj.xp=1051;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color11)
    {
      currentobj.xp=1125;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color12)
    {
      currentobj.xp=1197;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color13)
    {
      currentobj.xp=1270;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }
    else if(currentobj==color14)
    {
      currentobj.xp=1342;
      currentobj.yp=577;
      if(isHClColor_checked==false)
      {
        arrow.x=430;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(HCl_litmus_collider);//For colling purpose
        HCl_litmus_collider.inputEnabled = true;
        HCl_litmus_collider.enableBody =true;
        HCl_litmus_collider.visible=true;
      }
      else if(isNaOHColor_checked==false)
      {
        arrow.x=625;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaOH_litmus_collider);//For colling purpose
        NaOH_litmus_collider.inputEnabled = true;
        NaOH_litmus_collider.enableBody =true;
        NaOH_litmus_collider.visible=true;
      } 
      else if(isCH3COOHColor_checked==false)
      {
        arrow.x=820;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(CH3COOH_litmus_collider);//For colling purpose
        CH3COOH_litmus_collider.inputEnabled = true;
        CH3COOH_litmus_collider.enableBody =true;
        CH3COOH_litmus_collider.visible=true;
      } 
      else if(isLemonColor_checked==false)
      {
        arrow.x=1010;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Lemon_litmus_collider);//For colling purpose
        Lemon_litmus_collider.inputEnabled = true;
        Lemon_litmus_collider.enableBody =true;
        Lemon_litmus_collider.visible=true;
      } 
      else if(isWaterColor_checked==false)
      {
        arrow.x=1210;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(Water_litmus_collider);//For colling purpose
        Water_litmus_collider.inputEnabled = true;
        Water_litmus_collider.enableBody =true;
        Water_litmus_collider.visible=true;
      } 
      else if(isNaHCO3Color_checked==false)
      {
        arrow.x=1400;
        arrow.y=730;
        arrow_y=730;
        arrow.visible=true;
        this.game.physics.arcade.enable(NaHCO3_litmus_collider);//For colling purpose
        NaHCO3_litmus_collider.inputEnabled = true;
        NaHCO3_litmus_collider.enableBody =true;
        NaHCO3_litmus_collider.visible=true;
      } 
    }

    //}
    //else if(currentobj==Dropper_HCl_1){
    //   Dropper_HCl_1.frameName = "Dropper_HCl_OUT_0006.png";
    //   if(HCl_step==1){
    //     arrow.x=370;
    //   }else{
    //     arrow.x=570;
    //   }
    //   currentobj.xp=1560;
    //   currentobj.yp=550;
      
    //   arrow.y=450;
    //   arrow_y=450;
    //   arrow.visible=true;
    // } 
    
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    arrow.visible=false;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    
    if(currentobj==HCl_dropper1)
    {

      testtube_A_collider.enableBody=false;
      testtube_A_collider.inputEnabled=false;
      testtube_A_collider.visible=false;
      arrow.visible=false;
      currentobj.x=565;
      currentobj.y=340;
      HCl_dropper1.animations.play("anim1");
      testtube_A.animations.play("anim1");
      testtube_A.animations.currentAnim.onComplete.add(this.testtube_A_animation_end, this);
    }
    
   else if(currentobj==NaOH_dropper1)
      {
        arrow.visible=false;
        testtube_B_collider.enableBody=false;
        testtube_B_collider.inputEnabled=false;
        testtube_B_collider.visible=false;
        currentobj.x=650;
        currentobj.y=340;
        NaOH_dropper1.animations.play("anim1");
        testtube_B.animations.play("anim1");
        testtube_B.animations.currentAnim.onComplete.add(this.testtube_B_animation_end, this);
      }
    
    else if(currentobj==CH3COOH_dropper1){
      arrow.visible=false;
      testtube_C_collider.enableBody=false;
      testtube_C_collider.inputEnabled=false;
      testtube_C_collider.visible=false;
      currentobj.x=735;
      currentobj.y=340;
      CH3COOH_dropper1.animations.play("anim1");
      testtube_C.animations.play("anim1");
      testtube_C.animations.currentAnim.onComplete.add(this.testtube_C_animation_end, this);

      }
      else if(currentobj==Lemon_dropper1){
        arrow.visible=false;
        testtube_D_collider.enableBody=false;
        testtube_D_collider.inputEnabled=false;
        testtube_D_collider.visible=false;
        currentobj.x=1065;
        currentobj.y=340;
        Lemon_dropper1.animations.play("anim1");
        testtube_D.animations.play("anim1");
        testtube_D.animations.currentAnim.onComplete.add(this.testtube_D_animation_end, this);
  
        }
        else if(currentobj==Water_dropper1){
          arrow.visible=false;
          testtube_E_collider.enableBody=false;
          testtube_E_collider.inputEnabled=false;
          testtube_E_collider.visible=false;
          currentobj.x=1150;
          currentobj.y=340;
          Water_dropper1.animations.play("anim1");
          testtube_E.animations.play("anim1");
          testtube_E.animations.currentAnim.onComplete.add(this.testtube_E_animation_end, this);
    
          }
          else if(currentobj==NaHCO3_dropper1){
            arrow.visible=false;
            testtube_F_collider.enableBody=false;
            testtube_F_collider.inputEnabled=false;
            testtube_F_collider.visible=false;
            currentobj.x=1235;
            currentobj.y=340;
            NaHCO3_dropper1.animations.play("anim1");
            testtube_F.animations.play("anim1");
            testtube_F.animations.currentAnim.onComplete.add(this.testtube_F_animation_end, this);
      
            }

            else if(currentobj==HCl_dropper3){
              arrow.visible=false;
              HCl_litmus_collider.enableBody=false;
              HCl_litmus_collider.inputEnabled=false;
              HCl_litmus_collider.visible=false;
              currentobj.x=413;
              currentobj.y=726;
              HCl_drop.visible=true;
              HCl_dropper2.visible=false;
              HCl_dropper3.visible=true;
              HCl_drop.animations.play("anim1");
              HCl_dropper3.animations.play("anim1");
              HCl_litmus.animations.play("anim1");
              // HCl_litmus_label.visible=true;
              HCl_litmus_label=this.game.add.text(390,860,'HCl',labelfontStyle1);
              // HCl_litmus_label.visible=false;
              // testtube_F.animations.play("anim1");
              HCl_dropper3.animations.currentAnim.onComplete.add(this.HCl_dropper3_animation_end, this);
        
              }

              else if(currentobj==NaOH_dropper3){
                arrow.visible=false;
                NaOH_litmus_collider.enableBody=false;
                NaOH_litmus_collider.inputEnabled=false;
                NaOH_litmus_collider.visible=false;
                currentobj.x=610;
                currentobj.y=726;
                NaOH_drop.visible=true;
                NaOH_dropper2.visible=false;
                NaOH_dropper3.visible=true;
                NaOH_drop.animations.play("anim1");
                NaOH_dropper3.animations.play("anim1");
                NaOH_litmus.animations.play("anim1");
                NaOH_litmus_label=this.game.add.text(570,860,'NaOH',labelfontStyle1);
                NaOH_dropper3.animations.currentAnim.onComplete.add(this.NaOH_dropper3_animation_end, this);
          
                }
                else if(currentobj==CH3COOH_dropper3){
                  arrow.visible=false;
                  CH3COOH_litmus_collider.enableBody=false;
                  CH3COOH_litmus_collider.inputEnabled=false;
                  CH3COOH_litmus_collider.visible=false;
                  currentobj.x=805;
                  currentobj.y=726;
                  CH3COOH_drop.visible=true;
                  CH3COOH_dropper2.visible=false;
                  CH3COOH_dropper3.visible=true;
                  CH3COOH_drop.animations.play("anim1");
                  CH3COOH_dropper3.animations.play("anim1");
                  CH3COOH_litmus.animations.play("anim1");
                  CH3COOH_litmus_label=this.game.add.text(735,860,'CH\u2083COOH',labelfontStyle1);
                  //CH3COOH_litmus_label=this.game.add.text(570,860,'NaOH',labelfontStyle1);
                  CH3COOH_dropper3.animations.currentAnim.onComplete.add(this.CH3COOH_dropper3_animation_end, this);
            
                  }
                  else if(currentobj==Lemon_dropper3){
                    arrow.visible=false;
                    Lemon_litmus_collider.enableBody=false;
                    Lemon_litmus_collider.inputEnabled=false;
                    Lemon_litmus_collider.visible=false;
                    currentobj.x=997;
                    currentobj.y=726;
                    Lemon_drop.visible=true;
                    Lemon_dropper2.visible=false;
                    Lemon_dropper3.visible=true;
                    Lemon_drop.animations.play("anim1");
                    Lemon_dropper3.animations.play("anim1");
                    Lemon_litmus.animations.play("anim1");
                    Lemon_litmus_label=this.game.add.text(950,860,'Lemon',labelfontStyle1);
                    Lemon_dropper3.animations.currentAnim.onComplete.add(this.Lemon_dropper3_animation_end, this);
              
                    }
                    else if(currentobj==Water_dropper3){
                      arrow.visible=false;
                      Water_litmus_collider.enableBody=false;
                      Water_litmus_collider.inputEnabled=false;
                      Water_litmus_collider.visible=false;
                      currentobj.x=1192;
                      currentobj.y=726;
                      Water_drop.visible=true;
                      Water_dropper2.visible=false;
                      Water_dropper3.visible=true;
                      Water_drop.animations.play("anim1");
                      Water_dropper3.animations.play("anim1");
                      Water_litmus.animations.play("anim1");
                   
                      Water_litmus_label=this.game.add.text(1150,860,'Water',labelfontStyle1);
                      Water_dropper3.animations.currentAnim.onComplete.add(this.Water_dropper3_animation_end, this);
                
                      }

                      else if(currentobj==NaHCO3_dropper3){
                        arrow.visible=false;
                        NaHCO3_litmus_collider.enableBody=false;
                        NaHCO3_litmus_collider.inputEnabled=false;
                        NaHCO3_litmus_collider.visible=false;
                        currentobj.x=1388;
                        currentobj.y=726;
                        NaHCO3_drop.visible=true;
                        NaHCO3_dropper2.visible=false;
                        NaHCO3_dropper3.visible=true;
                        NaHCO3_drop.animations.play("anim1");
                        NaHCO3_dropper3.animations.play("anim1");
                        NaHCO3_litmus.animations.play("anim1");
                     
                        NaHCO3_litmus_label=this.game.add.text(1330,860,'NaHCO\u2083',labelfontStyle1);
                        NaHCO3_dropper3.animations.currentAnim.onComplete.add(this.NaHCO3_dropper3_animation_end, this);
                  
                        }


    
    
  },

  match_Obj_color()
  {
    // currentobj.inputEnabled=false;
    // currentobj.input.enableDrag(false);
    // currentobj.body.enable=false;

    if(isHClColor_checked==false)
    {
      if(currentobj.colorval==1)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure32',1);
        voice.play();
        dialog.text="Good job! you found the matching color";
        currentobj.x=402;
        currentobj.y=577;
        HCl_litmus_collider.inputEnabled = false;
        HCl_litmus_collider.enableBody =false;
        HCl_litmus_collider.visible=false;
        isHClColor_checked=true;
        this.colors_poperty_disable();
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.observation1, this);

      }
      else if(currentobj.colorval!=1)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure33',1);
        voice.play();
          dialog.text="You have chosen the wrong color, please try again";
          currentobj.x=currentobj.xp;
          currentobj.y=currentobj.yp;
          currentobj.inputEnabled=true;
          currentobj.input.enableDrag(true);
          currentobj.body.enable=true;
      }

    }
    else if(isNaOHColor_checked==false)
    {
      if(currentobj.colorval==14)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure32',1);
        voice.play();
        dialog.text="Good job! you found the matching color";
        currentobj.x=1342;
        currentobj.y=577;
        NaOH_litmus_collider.inputEnabled = false;
        NaOH_litmus_collider.enableBody =false;
        NaOH_litmus_collider.visible=false;
        isNaOHColor_checked=true;
        this.colors_poperty_disable();
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.observation2, this);
      }
      else if(currentobj.colorval!=14)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure33',1);
        voice.play();
          dialog.text="You have chosen the wrong color, please try again";
          currentobj.x=currentobj.xp;
          currentobj.y=currentobj.yp;
          currentobj.inputEnabled=true;
          currentobj.input.enableDrag(true);
          currentobj.body.enable=true;
      }

    }
    else if(isCH3COOHColor_checked==false)
    {
      if(currentobj.colorval==4)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure32',1);
        voice.play();
        dialog.text="Good job! you found the matching color";
        currentobj.x=618;
        currentobj.y=577;
        CH3COOH_litmus_collider.inputEnabled = false;
        CH3COOH_litmus_collider.enableBody =false;
        CH3COOH_litmus_collider.visible=false;
        isCH3COOHColor_checked=true;
        this.colors_poperty_disable();
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.observation3, this);
      }
      else if(currentobj.colorval!=4)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure33',1);
        voice.play();
          dialog.text="You have chosen the wrong color, please try again";
          currentobj.x=currentobj.xp;
          currentobj.y=currentobj.yp;
          currentobj.inputEnabled=true;
          currentobj.input.enableDrag(true);
          currentobj.body.enable=true;
      }

    }

    else if(isLemonColor_checked==false)
    {
      if(currentobj.colorval==2)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure32',1);
        voice.play();
        dialog.text="Good job! you found the matching color";
        currentobj.x=474;
        currentobj.y=577;
        Lemon_litmus_collider.inputEnabled = false;
        Lemon_litmus_collider.enableBody =false;
        Lemon_litmus_collider.visible=false;
        isLemonColor_checked=true;
        this.colors_poperty_disable();
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.observation4, this);
      }
      else if(currentobj.colorval!=2)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure33',1);
        voice.play();
          dialog.text="You have chosen the wrong color, please try again";
          currentobj.x=currentobj.xp;
          currentobj.y=currentobj.yp;
          currentobj.inputEnabled=true;
          currentobj.input.enableDrag(true);
          currentobj.body.enable=true;
      }

    }
    else if(isWaterColor_checked==false)
    {
      if(currentobj.colorval==7)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure32',1);
        voice.play();
        dialog.text="Good job! you found the matching color";
        currentobj.x=835;
        currentobj.y=577;
        Water_litmus_collider.inputEnabled = false;
        Water_litmus_collider.enableBody =false;
        Water_litmus_collider.visible=false;
        isWaterColor_checked=true;
        this.colors_poperty_disable();
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.observation5, this);
      }
      else if(currentobj.colorval!=7)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure33',1);
        voice.play();
          dialog.text="You have chosen the wrong color, please try again";
          currentobj.x=currentobj.xp;
          currentobj.y=currentobj.yp;
          currentobj.inputEnabled=true;
          currentobj.input.enableDrag(true);
          currentobj.body.enable=true;
      }

    }
    else if(isNaHCO3Color_checked==false)
    {
      if(currentobj.colorval==10)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure32',1);
        voice.play();
        dialog.text="Good job! you found the matching color";
        currentobj.x=1051;
        currentobj.y=577;
        NaHCO3_litmus_collider.inputEnabled = false;
        NaHCO3_litmus_collider.enableBody =false;
        NaHCO3_litmus_collider.visible=false;
        isNaHCO3Color_checked=true;
        this.colors_poperty_disable();
        this.game.time.events.add(Phaser.Timer.SECOND*3,this.observation6, this);
      }
      else if(currentobj.colorval!=10)
      {
        voice.destroy();
        voice=this.game.add.audio('A_procedure33',1);
        voice.play();
          dialog.text="You have chosen the wrong color, please try again";
          currentobj.x=currentobj.xp;
          currentobj.y=currentobj.yp;
          currentobj.inputEnabled=true;
          currentobj.input.enableDrag(true);
          currentobj.body.enable=true;
      }

    }


  },

  NaHCO3_dropper3_animation_end:function()
  {
    voice.destroy();
voice=this.game.add.audio('A_procedure10',1);
voice.play();
    dialog.text="Good job..."
    NaHCO3_drop.visible=false;
    tween1=this.game.add.tween(NaHCO3_dropper3).to( {x:2500}, 500, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NextStage2()}, this);
    iscolorCheking=true;
  },
  NextStage2:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure11',1);
    voice.play();
    dialog.text="Now lets evaluate the color changes with the pH color chart";
    tween1=this.game.add.tween(testtube_stand_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween2=this.game.add.tween(color_chart_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween3=this.game.add.tween(table_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.litmus_A_colorcheck, this);
 // this.game.time.events.add(Phaser.Timer.SECOND*4,this.observation1, this);
  },

  litmus_A_colorcheck:function()
  {
    voice.destroy();
voice=this.game.add.audio('A_procedure26',1);
voice.play();
    dialog.text="Drag and drop the matching color from the color chart to the\n pH paper A";
    this.colors_poperty_enable();
  },


  observation1:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure12',1);
voice.play();
 dialog.text="Observe that the color change in pH paper A is matching with the\nnumber 1 on the color chart.";
 HCl_litmus_A_text2.visible=true;
 one_ring.visible=true;
 one_ring.animations.play('anim1');
 this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation1_take_value, this);
},
observation1_take_value:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure13',1);
voice.play();
  dialog.text="It means the HCl has the pH value 1 and it is acidic in nature";
  table_A_text.visible=true;
  A_color.text="Deep\nred";
  A_color_val.text="1";
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.litmus_B_colorcheck, this);
  //this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation2, this);
},

litmus_B_colorcheck:function()
{
  voice.destroy();
  voice=this.game.add.audio('A_procedure27',1);
  voice.play();
  HCl_litmus_A_text2.visible=false;
  one_ring.visible=false;
  dialog.text="Drag and drop the matching color from the color chart to the\n pH paper B";
  this.colors_poperty_enable();
},

observation2:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure14',1);
voice.play();
  // HCl_litmus_A_text2.visible=false;
  // one_ring.visible=false;
  dialog.text="Observe that the color change in pH paper B is matching with the\nnumber 14 on the color chart.";
  NaOH_litmus_B_text2.visible=true;
  fourteen_ring.visible=true;
  fourteen_ring.animations.play('anim1');
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation2_take_value, this);
},
observation2_take_value:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure15',1);
voice.play();
  dialog.text="It means the NaOH has the pH value 14 and it is basic in nature";
  table_B_text.visible=true;
  B_color.text="Purple";
  B_color_val.text="14";
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.litmus_C_colorcheck, this);
 // this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation3, this);
},

litmus_C_colorcheck:function()
{
  voice.destroy();
  voice=this.game.add.audio('A_procedure28',1);
  voice.play();
  NaOH_litmus_B_text2.visible=false;
  fourteen_ring.visible=false;
  dialog.text="Drag and drop the matching color from the color chart to the\n pH paper C";
  this.colors_poperty_enable();
},
observation3:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure16',1);
voice.play();
  // NaOH_litmus_B_text2.visible=false;
  // fourteen_ring.visible=false;
  dialog.text="Observe that the color change in pH paper C is matching with the\nnumber 4 on the color chart."
  CH3COOH_litmus_C_text2.visible=true;
  four_ring.visible=true;
  four_ring.animations.play('anim1');
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation3_take_value, this);
},
observation3_take_value:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure17',1);
voice.play();
  dialog.text="It means the CH3COOH has the pH value 4 and it is acidic in nature";
  table_C_text.visible=true;
  C_color.text="Orange";
  C_color_val.text="4";
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.litmus_D_colorcheck, this);
  //this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation4, this);
},


litmus_D_colorcheck:function()
{
  voice.destroy();
  voice=this.game.add.audio('A_procedure29',1);
  voice.play();
  CH3COOH_litmus_C_text2.visible=false;
  four_ring.visible=false;
  dialog.text="Drag and drop the matching color from the color chart to the\n pH paper D";
  this.colors_poperty_enable();
},
observation4:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure18',1);
voice.play();
  // CH3COOH_litmus_C_text2.visible=false;
  // four_ring.visible=false;
  dialog.text="Observe that the color change in pH paper D is matching with the\nnumber 2 on the color chart."
  Lemon_litmus_D_text2.visible=true;
  two_ring.visible=true;
  two_ring.animations.play('anim1');
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation4_take_value, this);
},
observation4_take_value:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure19',1);
voice.play();
  dialog.text="It means the Lemon has the pH value 2 and it is acidic in nature";
  table_D_text.visible=true;
  D_color.text="Red";
  D_color_val.text="2";
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.litmus_E_colorcheck, this);
  //this.game.time.events.add(Phaser.Timer.SECOND*5 ,this.observation5, this);
},

litmus_E_colorcheck:function()
{
  voice.destroy();
  voice=this.game.add.audio('A_procedure30',1);
  voice.play();
  Lemon_litmus_D_text2.visible=false;
  two_ring.visible=false;
  dialog.text="Drag and drop the matching color from the color chart to the\n pH paper E";
  this.colors_poperty_enable();
},
observation5:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure20',1);
voice.play();
  // Lemon_litmus_D_text2.visible=false;
  // two_ring.visible=false;
  dialog.text="Observe that the color change in pH paper E is matching with the\nnumber 7 on the color chart."
  Water_litmus_E_text2.visible=true;
  seven_ring.visible=true;
  seven_ring.animations.play('anim1');
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation5_take_value, this);
},
observation5_take_value:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure21',1);
voice.play();
  dialog.text="It means the water has the pH value 7 and it is neutral in nature";
  table_E_text.visible=true;
  E_color.text="Green";
  E_color_val.text="7";
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.litmus_F_colorcheck, this);
  //this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation6, this);
},

litmus_F_colorcheck:function()
{
  voice.destroy();
  voice=this.game.add.audio('A_procedure31',1);
  voice.play();
  Water_litmus_E_text2.visible=false;
  seven_ring.visible=false;
  dialog.text="Drag and drop the matching color from the color chart to the\n pH paper F";
  this.colors_poperty_enable();
},
observation6:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure22',1);
voice.play();
  // Water_litmus_E_text2.visible=false;
  // seven_ring.visible=false;
  dialog.text="Observe that the color change in pH paper F is matching with the\nnumber 10 on the color chart."
  NaHCO3_litmus_F_text2.visible=true;
  ten_ring.visible=true;
  ten_ring.animations.play('anim1');
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.observation6_take_value, this);
},
observation6_take_value:function()
{
  voice.destroy();
voice=this.game.add.audio('A_procedure23',1);
voice.play();
  dialog.text="It means NaHCO3 has the pH value 10 and it is basic in nature";
  table_F_text.visible=true;
  F_color.text="Blue";
  F_color_val.text="10";
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.toNextAudio, this);
},
toNextAudio:function()
{
  NaHCO3_litmus_F_text2.visible=false;
  ten_ring.visible=false;
  voice.destroy();
  voice=this.game.add.audio('A_procedure24',1);
  voice.play();
 dialog.text="You have successfully done experiment 1, lets move on to the next\nexperiment";
 this.game.time.events.add(Phaser.Timer.SECOND*5,this.toNextExperiment, this);
},

toNextExperiment:function()
{
  voice.destroy();
  voice=this.game.add.audio('A_procedure25',1);
  voice.play();
dialog.text="Click on the next button to do the next experiment";
play.visible=true;
},


  Water_dropper3_animation_end:function()
  {
    Water_drop.visible=false;
    tween1=this.game.add.tween(Water_dropper3).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaHCO3_dropper2_tween_in()}, this);
  },
  NaHCO3_dropper2_tween_in:function()
  {
    tween1=this.game.add.tween(NaHCO3_dropper2).to( {x:1580}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaHCO3_dropper2_tween_in_end()}, this);
  },
  NaHCO3_dropper2_tween_in_end:function()
  {
    arrow.visible=true;
    arrow.x=1580;
    arrow.y=650;
    arrow_y=650;
    NaHCO3_dropper2.inputEnabled = true;
    NaHCO3_dropper2.input.useHandCursor = true;
    NaHCO3_dropper2.events.onInputDown.add(this.clickOnNaHCO3_dropper2, this);
  },
  clickOnNaHCO3_dropper2:function()
  {
    if(isNaHCO3_dropper2_taken==false)
    {
      arrow.visible=false;
      NaHCO3_dropper2.angle=0;
      tween2=this.game.add.tween(NaHCO3_dropper2).to( {x:1235,y:340}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function(){this.NaHCO3_dropper2_tween3()}, this);
      isNaHCO3_dropper2_taken=true;
    }

  },
  NaHCO3_dropper2_tween3:function()
  {
    tween3=this.game.add.tween(NaHCO3_dropper2).to( {y:480}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.NaHCO3_dropper2_tween3_end()}, this);
  },
  NaHCO3_dropper2_tween3_end:function()
  {
    arrow.visible=true;
    arrow.x=1245;
    arrow.y=250;
    arrow_y=250;
    NaHCO3_dropper3.frameName="NaHCo3_dropper_to_testube0001.png";
    NaHCO3_dropper3.events.onDragStart.add(function() {this.onDragStart(NaHCO3_dropper3)}, this);
    NaHCO3_dropper3.events.onDragStop.add(function() {this.onDragStop(NaHCO3_dropper3)}, this);
    NaHCO3_dropper3.inputEnabled = true;
    NaHCO3_dropper3.input.useHandCursor = true;
    NaHCO3_dropper3.input.enableDrag(true);
    this.game.physics.arcade.enable(NaHCO3_dropper3);
    NaHCO3_dropper3.enableBody =true;
  },

  Lemon_dropper3_animation_end:function()
  {
    Lemon_drop.visible=false;
    tween1=this.game.add.tween(Lemon_dropper3).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Water_dropper2_tween_in()}, this);
  },
  Water_dropper2_tween_in:function()
  {
    tween1=this.game.add.tween(Water_dropper2).to( {x:1580}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Water_dropper2_tween_in_end()}, this);
  },
  Water_dropper2_tween_in_end:function()
  {
    arrow.visible=true;
    arrow.x=1580;
    arrow.y=650;
    arrow_y=650;
    Water_dropper2.inputEnabled = true;
    Water_dropper2.input.useHandCursor = true;
    Water_dropper2.events.onInputDown.add(this.clickOnWater_dropper2, this);
  },
  clickOnWater_dropper2:function()
  {
    if(isWater_dropper2_taken==false)
    {
      arrow.visible=false;
      Water_dropper2.angle=0;
      tween2=this.game.add.tween(Water_dropper2).to( {x:1150,y:340}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function(){this.Water_dropper2_tween3()}, this);
      isWater_dropper2_taken=true;
    }

  },
  Water_dropper2_tween3:function()
  {
    tween3=this.game.add.tween(Water_dropper2).to( {y:480}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.Water_dropper2_tween3_end()}, this);
  },
  Water_dropper2_tween3_end:function()
  {
    arrow.visible=true;
    arrow.x=1164;
    arrow.y=250;
    arrow_y=250;
    Water_dropper3.frameName="Water_dropper_to_testube0001.png";
    Water_dropper3.events.onDragStart.add(function() {this.onDragStart(Water_dropper3)}, this);
    Water_dropper3.events.onDragStop.add(function() {this.onDragStop(Water_dropper3)}, this);
    Water_dropper3.inputEnabled = true;
    Water_dropper3.input.useHandCursor = true;
    Water_dropper3.input.enableDrag(true);
    this.game.physics.arcade.enable(Water_dropper3);
    Water_dropper3.enableBody =true;
  },



  CH3COOH_dropper3_animation_end:function()
  {
    CH3COOH_drop.visible=false;
    tween1=this.game.add.tween(CH3COOH_dropper3).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Lemon_dropper2_tween_in()}, this);
  },
  Lemon_dropper2_tween_in:function()
  {
    tween1=this.game.add.tween(Lemon_dropper2).to( {x:1580}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Lemon_dropper2_tween_in_end()}, this);
  },
  Lemon_dropper2_tween_in_end:function()
  {
    arrow.visible=true;
    arrow.x=1580;
    arrow.y=650;
    arrow_y=650;
    Lemon_dropper2.inputEnabled = true;
    Lemon_dropper2.input.useHandCursor = true;
    Lemon_dropper2.events.onInputDown.add(this.clickOnLemon_dropper2, this);
  },
  clickOnLemon_dropper2:function()
  {
    if(isLemon_dropper2_taken==false)
    {
      arrow.visible=false;
      Lemon_dropper2.angle=0;
      tween2=this.game.add.tween(Lemon_dropper2).to( {x:1065,y:340}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function(){this.Lemon_dropper2_tween3()}, this);
      isLemon_dropper2_taken=true;
    }

  },
  Lemon_dropper2_tween3:function()
  {
    tween3=this.game.add.tween(Lemon_dropper2).to( {y:480}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.Lemon_dropper2_tween3_end()}, this);
  },
  Lemon_dropper2_tween3_end:function()
  {
    arrow.visible=true;
    arrow.x=1078;
    arrow.y=250;
    arrow_y=250;
    Lemon_dropper3.frameName="Lemon_dropper_to_testube0001.png";
    Lemon_dropper3.events.onDragStart.add(function() {this.onDragStart(Lemon_dropper3)}, this);
    Lemon_dropper3.events.onDragStop.add(function() {this.onDragStop(Lemon_dropper3)}, this);
    Lemon_dropper3.inputEnabled = true;
    Lemon_dropper3.input.useHandCursor = true;
    Lemon_dropper3.input.enableDrag(true);
    this.game.physics.arcade.enable(Lemon_dropper3);
    Lemon_dropper3.enableBody =true;
  },

  NaOH_dropper3_animation_end:function()
  {
    NaOH_drop.visible=false;
    tween1=this.game.add.tween(NaOH_dropper3).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.CH3COOH_dropper2_tween_in()}, this);
  },

  CH3COOH_dropper2_tween_in:function()
  {
    tween1=this.game.add.tween(CH3COOH_dropper2).to( {x:1580}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.CH3COOH_dropper2_tween_in_end()}, this);
  },
  CH3COOH_dropper2_tween_in_end:function()
  {
    arrow.visible=true;
    arrow.x=1580;
    arrow.y=650;
    arrow_y=650;
    CH3COOH_dropper2.inputEnabled = true;
    CH3COOH_dropper2.input.useHandCursor = true;
    CH3COOH_dropper2.events.onInputDown.add(this.clickOnCH3COOH_dropper2, this);
  },
  clickOnCH3COOH_dropper2:function()
  {
    if(isCH3COOH_dropper2_taken==false)
    {
      arrow.visible=false;
      CH3COOH_dropper2.angle=0;
      tween2=this.game.add.tween(CH3COOH_dropper2).to( {x:735,y:340}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function(){this.CH3COOH_dropper2_tween3()}, this);
      isCH3COOH_dropper2_taken=true;
    }

  },
  CH3COOH_dropper2_tween3:function()
  {
    tween3=this.game.add.tween(CH3COOH_dropper2).to( {y:480}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.CH3COOH_dropper2_tween3_end()}, this);
  },
  CH3COOH_dropper2_tween3_end:function()
  {
    arrow.visible=true;
    arrow.x=750;
    arrow.y=250;
    arrow_y=250;
    CH3COOH_dropper3.frameName="Dill_HCL_dropper_to_testube0001.png";
    CH3COOH_dropper3.events.onDragStart.add(function() {this.onDragStart( CH3COOH_dropper3)}, this);
    CH3COOH_dropper3.events.onDragStop.add(function() {this.onDragStop( CH3COOH_dropper3)}, this);
    CH3COOH_dropper3.inputEnabled = true;
    CH3COOH_dropper3.input.useHandCursor = true;
    CH3COOH_dropper3.input.enableDrag(true);
    this.game.physics.arcade.enable( CH3COOH_dropper3);
    CH3COOH_dropper3.enableBody =true;
  },

  HCl_dropper3_animation_end:function()
  {
    //audio
    voice.destroy();
    voice=this.game.add.audio('A_procedure9',1);
      voice.play();
    dialog.text="Repeat the same step for each liquid with corresponding pH paper";
    HCl_drop.visible=false;
    tween1=this.game.add.tween(HCl_dropper3).to( {x:1580, y:650}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.HCl_dropper3_tween1_end()}, this);
  },
  HCl_dropper3_tween1_end:function()
  {
    tween4=this.game.add.tween(HCl_dropper3).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween4.onComplete.add(function(){this.NaOH_dropper2_tween_in()}, this);

  },
  NaOH_dropper2_tween_in:function()
  {
    tween1=this.game.add.tween(NaOH_dropper2).to( {x:1580}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaOH_dropper2_tween_in_end()}, this);
  },
  NaOH_dropper2_tween_in_end:function()
  {
    arrow.visible=true;
    arrow.x=1580;
    arrow.y=650;
    arrow_y=650;
    NaOH_dropper2.inputEnabled = true;
    NaOH_dropper2.input.useHandCursor = true;
    NaOH_dropper2.events.onInputDown.add(this.clickOnNaOH_dropper2, this);

  },

  clickOnNaOH_dropper2:function()
  {
    if(isNaOH_dropper2_taken==false)
    {
      arrow.visible=false;
      NaOH_dropper2.angle=0;
      tween2=this.game.add.tween(NaOH_dropper2).to( {x:650,y:340}, 600, Phaser.Easing.Out, true);
      tween2.onComplete.add(function(){this.NaOH_dropper2_tween3()}, this);
      isNaOH_dropper2_taken=true;
    }

  },
  NaOH_dropper2_tween3:function()
  {
    tween3=this.game.add.tween(NaOH_dropper2).to( {y:480}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.NaOH_dropper2_tween3_end()}, this);
  },
  NaOH_dropper2_tween3_end:function()
  {
    arrow.visible=true;
    arrow.x=665;
    arrow.y=250;
    arrow_y=250;
    NaOH_dropper2.frameName="Dill_HCL_dropper_to_testube0001.png";
    NaOH_dropper3.events.onDragStart.add(function() {this.onDragStart( NaOH_dropper3)}, this);
    NaOH_dropper3.events.onDragStop.add(function() {this.onDragStop( NaOH_dropper3)}, this);
    NaOH_dropper3.inputEnabled = true;
    NaOH_dropper3.input.useHandCursor = true;
    NaOH_dropper3.input.enableDrag(true);
    this.game.physics.arcade.enable( NaOH_dropper3);
    NaOH_dropper3.enableBody =true;

  },
  testtube_F_animation_end:function()
  {
    tween3=this.game.add.tween(NaHCO3_dropper1).to( {x:1780, y:720}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.NaHCO3_dropper_tween3_end()}, this);
    cork.visible=false;
  },
  NaHCO3_dropper_tween3_end:function()
  {
    tween1=this.game.add.tween(NaHCO3_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NextStage()}, this);

  },
  NextStage:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure5',1);
    voice.play();
    dialog.text="You have successfully added liquids to the test tubes";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.Litmus_group_tween_in, this);

  },
  Litmus_group_tween_in:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure6',1);
    voice.play();
    tween1=this.game.add.tween(Litmus_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    dialog.text="Take 6 pH papers on the glazed tile, and label them as A, B, C, D, E\nand F";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.Litmus_group_tween_in_end, this);
  },
  Litmus_group_tween_in_end:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure7',1);
    voice.play();
    dialog.text="Now add few drops of liquids on each of the pH paper and observe\nthe color change.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.HCl_dropper2_tween_in, this);
  },
  HCl_dropper2_tween_in:function()
  {
    tween1=this.game.add.tween(HCl_dropper2).to( {x:1580}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.HCl_dropper2_tween_in_end()}, this);
  },
  HCl_dropper2_tween_in_end:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure2',1);
    voice.play();
   dialog.text="Take the dropper";
   arrow.visible=true;
   arrow.x=1580;
   arrow.y=680;
   arrow_y=680;
   HCl_dropper2.inputEnabled = true;
   HCl_dropper2.input.useHandCursor = true;
   HCl_dropper2.events.onInputDown.add(this.clickOnHCl_dropper2, this);
  },

  clickOnHCl_dropper2:function()
  {
    if(isHCl_dropper2_taken==false)
    {
      arrow.visible=false;
      HCl_dropper2.angle=0;
      tween1=this.game.add.tween(HCl_dropper2).to( {x:565, y:340}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.HCl_dropper2_tween2()}, this);
      isHCl_dropper2_taken=true;
    }
  },

  HCl_dropper2_tween2:function()
  {
    tween2=this.game.add.tween(HCl_dropper2).to( {y:460}, 600, Phaser.Easing.Out, true);
    tween2.onComplete.add(function(){this.HCl_dropper2_tween2_end()}, this);


  },
  HCl_dropper2_tween2_end:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure8',1);
    voice.play();
    arrow.visible=true;
    arrow.x=576;
    arrow.y=250;
    arrow_y=250;
    HCl_dropper2.frameName="Dill_HCL_dropper_to_testube0001.png";
    dialog.text="Put two drops of Dil. HCl on the pH paper A, and observe";
  //  HCl_dropper3.inputEnabled = true;
  //   HCl_dropper3.input.useHandCursor = true;
  //   HCl_dropper3.events.onInputDown.add(this.clickon, this);
    
    HCl_dropper3.events.onDragStart.add(function() {this.onDragStart( HCl_dropper3)}, this);
    HCl_dropper3.events.onDragStop.add(function() {this.onDragStop( HCl_dropper3)}, this);
    HCl_dropper3.inputEnabled = true;
    HCl_dropper3.input.useHandCursor = true;
    HCl_dropper3.input.enableDrag(true);
    this.game.physics.arcade.enable( HCl_dropper3);
    HCl_dropper3.enableBody =true;

  },
  testtube_E_animation_end:function()
  {
    tween3=this.game.add.tween(Water_dropper1).to( {x:1780, y:720}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.Water_dropper_tween3_end()}, this);
    cork.visible=false;
  },
  Water_dropper_tween3_end:function()
  {
    tween1=this.game.add.tween(Water_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaHCO3_group_tween_in()}, this);

  },
  NaHCO3_group_tween_in:function()
  {
    tween1=this.game.add.tween(NaHCO3_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaHCO3_group_tween_in_end()}, this);
  },
  NaHCO3_group_tween_in_end:function()
  {
    //dialog.text="Repeat the steps and take chemicals in all test tubes";
    arrow.x=1580;
    arrow.y=700;
    arrow_y=700;
    arrow.visible=true;
    NaHCO3_dropper1.inputEnabled = true;
    NaHCO3_dropper1.input.useHandCursor = true;
  },
  testtube_D_animation_end:function()
  {
    tween3=this.game.add.tween(Lemon_dropper1).to( {x:1780, y:720}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.Lemon_dropper_tween3_end()}, this);
    cork.visible=false;
  },
  Lemon_dropper_tween3_end:function()
  {
    tween1=this.game.add.tween(Lemon_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Water_group_tween_in()}, this);

  },
  Water_group_tween_in:function()
  {
    tween1=this.game.add.tween(Water_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Water_group_tween_in_end()}, this);
  },
  Water_group_tween_in_end:function()
  {
    //dialog.text="Repeat the steps and take chemicals in all test tubes";
    arrow.x=1580;
    arrow.y=700;
    arrow_y=700;
    arrow.visible=true;
    Water_dropper1.inputEnabled = true;
    Water_dropper1.input.useHandCursor = true;
  },
  testtube_C_animation_end:function()
  {
    tween3=this.game.add.tween(CH3COOH_dropper1).to( {x:1780, y:720}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.CH3COOH_dropper_tween3_end()}, this);
    cork.visible=false;
  },
  CH3COOH_dropper_tween3_end:function()
  {
    tween1=this.game.add.tween(CH3COOH_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Lemon_group_tween_in()}, this);

  },
  Lemon_group_tween_in:function()
  {
    tween1=this.game.add.tween(Lemon_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Lemon_group_tween_in_end()}, this);
  },
  Lemon_group_tween_in_end:function()
  {
    //dialog.text="Repeat the steps and take chemicals in all test tubes";
    arrow.x=1580;
    arrow.y=700;
    arrow_y=700;
    arrow.visible=true;
    Lemon_dropper1.inputEnabled = true;
    Lemon_dropper1.input.useHandCursor = true;
  },
  testtube_B_animation_end:function()
  {
    tween3=this.game.add.tween(NaOH_dropper1).to( {x:1780, y:720}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.NaOH_dropper_tween3_end()}, this);
    cork.visible=false;
  },
  NaOH_dropper_tween3_end:function()
  {
    tween1=this.game.add.tween(NaOH_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.CH3COOH_group_tween_in()}, this);

  },
  CH3COOH_group_tween_in:function()
  {
    tween1=this.game.add.tween(CH3COOH_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.CH3COOH_group_tween_in_end()}, this);
  },
  CH3COOH_group_tween_in_end:function()
  {
 
    //dialog.text="Repeat the same steps for each liquid with corresponding\ntest tubes.";
    arrow.x=1580;
    arrow.y=700;
    arrow_y=700;
    arrow.visible=true;
    CH3COOH_dropper1.inputEnabled = true;
    CH3COOH_dropper1.input.useHandCursor = true;
  },
  testtube_A_animation_end:function()
  {
    tween3=this.game.add.tween(HCl_dropper1).to( {x:1780, y:720}, 600, Phaser.Easing.Out, true);
    tween3.onComplete.add(function(){this.HCl_dropper_tween3_end()}, this);
    cork.visible=false;
  },

  HCl_dropper_tween3_end:function()
  {
    tween1=this.game.add.tween(HCl_group).to( {x:2500}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaOH_group_tween_in()}, this);

  },

  NaOH_group_tween_in:function()
  {
    tween1=this.game.add.tween(NaOH_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaOH_group_tween_in_end()}, this);
  },

  NaOH_group_tween_in_end:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure4',1);
voice.play();
    dialog.text="Repeat the same steps for each liquid with corresponding test tubes.";
    arrow.x=1580;
    arrow.y=700;
    arrow_y=700;
    arrow.visible=true;
    NaOH_dropper1.inputEnabled = true;
    NaOH_dropper1.input.useHandCursor = true;
  },

  HCl_group_tween_in:function()
  {
    tween1=this.game.add.tween(HCl_group).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.HCl_group_tween_in_end()}, this);
  },
  HCl_group_tween_in_end:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure2',1);
    voice.play();
    dialog.text="Take the dropper";
    arrow.x=1580;
    arrow.y=700;
    arrow_y=700;
    arrow.visible=true;
    HCl_dropper1.inputEnabled = true;
    HCl_dropper1.input.useHandCursor = true;

  },

  clickOnHCl_dropper1:function()
  {
    if(isHCl_dropper1_taken==false)
    {
    arrow.visible=false;
    cork.visible=true;
    HCl_cork.visible=false;
    HCl_dropper1.angle=0;
    tween1=this.game.add.tween(HCl_dropper1).to( {x:1780, y:520}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.HCl_dropper1_tween2()}, this);
    isHCl_dropper1_taken=true;  
    HCl_dropper1.inputEnabled = false;
    HCl_dropper1.input.useHandCursor = false;
  }
  },
  HCl_dropper1_tween2:function()
  {
    tween1=this.game.add.tween(HCl_dropper1).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.HCl_dropper1_tween2_end()}, this);
  },

  HCl_dropper1_tween2_end:function()
  {
    voice.destroy();
    voice=this.game.add.audio('A_procedure3',1);
    voice.play();
    dialog.text="Add few drops of Dilute HCl to the test tube A";
    arrow.x=1794;
    arrow_y=475;
    arrow.y=475;
   
    arrow.visible=true;
    HCl_dropper1.frameName="Dill_HCL_dropper_to_testube0001.png";
    HCl_dropper1.inputEnabled = false;
    HCl_dropper1.input.useHandCursor = false;
    HCl_dropper1.events.onDragStart.add(function() {this.onDragStart( HCl_dropper1)}, this);
    HCl_dropper1.events.onDragStop.add(function() {this.onDragStop( HCl_dropper1)}, this);
    HCl_dropper1.inputEnabled = true;
    HCl_dropper1.input.useHandCursor = true;
    HCl_dropper1.input.enableDrag(true);
    this.game.physics.arcade.enable( HCl_dropper1);
    HCl_dropper1.enableBody =true;

  },

  clickOnNaOH_dropper1:function()
  {
    if(isNaOH_dropper1_taken==false)
    {
    arrow.visible=false;
    cork.visible=true;
    NaOH_cork.visible=false;
    NaOH_dropper1.angle=0;
    tween1=this.game.add.tween(NaOH_dropper1).to( {x:1780, y:520}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaOH_dropper1_tween2()}, this);
    isNaOH_dropper1_taken=true;  
    }

  },
  NaOH_dropper1_tween2:function()
  {
    tween1=this.game.add.tween(NaOH_dropper1).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaOH_dropper1_tween2_end()}, this);
  },
  NaOH_dropper1_tween2_end:function()
  {
    arrow.x=1794;
    arrow_y=475;
    arrow.y=475;
   
    arrow.visible=true;
    NaOH_dropper1.frameName="Dill_HCL_dropper_to_testube0001.png";
    NaOH_dropper1.inputEnabled = false;
    NaOH_dropper1.input.useHandCursor = false;
    NaOH_dropper1.events.onDragStart.add(function() {this.onDragStart(NaOH_dropper1)}, this);
    NaOH_dropper1.events.onDragStop.add(function() {this.onDragStop(NaOH_dropper1)}, this);
    NaOH_dropper1.inputEnabled = true;
    NaOH_dropper1.input.useHandCursor = true;
    NaOH_dropper1.input.enableDrag(true);
    this.game.physics.arcade.enable(NaOH_dropper1);
    NaOH_dropper1.enableBody =true;

  },

  clickOnCH3COOH_dropper1:function()
  {
    if(isCH3COOH_dropper1_taken==false)
    {
    arrow.visible=false;
    cork.visible=true;
    CH3COOH_cork.visible=false;
    CH3COOH_dropper1.angle=0;
    tween1=this.game.add.tween(CH3COOH_dropper1).to( {x:1780, y:520}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.CH3COOH_dropper1_tween2()}, this);
    isCH3COOH_dropper1_taken=true;  
    }

  },
  CH3COOH_dropper1_tween2:function()
  {
    tween1=this.game.add.tween(CH3COOH_dropper1).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.CH3COOH_dropper1_tween2_end()}, this);
  },
  CH3COOH_dropper1_tween2_end:function()
  {
    arrow.x=1794;
    arrow_y=475;
    arrow.y=475;
   
    arrow.visible=true;
    CH3COOH_dropper1.frameName="Dill_HCL_dropper_to_testube0001.png";
    CH3COOH_dropper1.inputEnabled = false;
    CH3COOH_dropper1.input.useHandCursor = false;
    CH3COOH_dropper1.events.onDragStart.add(function() {this.onDragStart(CH3COOH_dropper1)}, this);
    CH3COOH_dropper1.events.onDragStop.add(function() {this.onDragStop(CH3COOH_dropper1)}, this);
    CH3COOH_dropper1.inputEnabled = true;
    CH3COOH_dropper1.input.useHandCursor = true;
    CH3COOH_dropper1.input.enableDrag(true);
    this.game.physics.arcade.enable(CH3COOH_dropper1);
    CH3COOH_dropper1.enableBody =true;

  },
  clickOnLemon_dropper1:function()
  {
    if(isLemon_dropper1_taken==false)
    {
    arrow.visible=false;
    cork.visible=true;
    Lemon_cork.visible=false;
    Lemon_dropper1.angle=0;
    tween1=this.game.add.tween(Lemon_dropper1).to( {x:1780, y:520}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Lemon_dropper1_tween2()}, this);
    isLemon_dropper1_taken=true;  
    }

  },
  Lemon_dropper1_tween2:function()
  {
    tween1=this.game.add.tween(Lemon_dropper1).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Lemon_dropper1_tween2_end()}, this);
  },
  Lemon_dropper1_tween2_end:function()
  {
    arrow.x=1794;
    arrow_y=475;
    arrow.y=475;
   
    arrow.visible=true;
    Lemon_dropper1.frameName="Lemon_dropper_to_testube0001.png";
    Lemon_dropper1.inputEnabled = false;
    Lemon_dropper1.input.useHandCursor = false;
    Lemon_dropper1.events.onDragStart.add(function() {this.onDragStart(Lemon_dropper1)}, this);
    Lemon_dropper1.events.onDragStop.add(function() {this.onDragStop(Lemon_dropper1)}, this);
    Lemon_dropper1.inputEnabled = true;
    Lemon_dropper1.input.useHandCursor = true;
    Lemon_dropper1.input.enableDrag(true);
    this.game.physics.arcade.enable(Lemon_dropper1);
    Lemon_dropper1.enableBody =true;

  },
  clickOnWater_dropper1:function()
  {
    if(isWater_dropper1_taken==false)
    {
    arrow.visible=false;
    cork.visible=true;
    Water_cork.visible=false;
    Water_dropper1.angle=0;
    tween1=this.game.add.tween(Water_dropper1).to( {x:1780, y:520}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Water_dropper1_tween2()}, this);
    isWater_dropper1_taken=true;  
    }

  },
  Water_dropper1_tween2:function()
  {
    tween1=this.game.add.tween(Water_dropper1).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.Water_dropper1_tween2_end()}, this);
  },
  Water_dropper1_tween2_end:function()
  {
    arrow.x=1794;
    arrow_y=475;
    arrow.y=475;
   
    arrow.visible=true;
    Water_dropper1.frameName="Water_dropper_to_testube0001.png";
    Water_dropper1.inputEnabled = false;
    Water_dropper1.input.useHandCursor = false;
    Water_dropper1.events.onDragStart.add(function() {this.onDragStart(Water_dropper1)}, this);
    Water_dropper1.events.onDragStop.add(function() {this.onDragStop(Water_dropper1)}, this);
    Water_dropper1.inputEnabled = true;
    Water_dropper1.input.useHandCursor = true;
    Water_dropper1.input.enableDrag(true);
    this.game.physics.arcade.enable(Water_dropper1);
    Water_dropper1.enableBody =true;

  },
  clickOnNaHCO3_dropper1:function()
  {
    if(isNaHCO3_dropper1_taken==false)
    {
    arrow.visible=false;
    cork.visible=true;
    NaHCO3_cork.visible=false;
    NaHCO3_dropper1.angle=0;
    tween1=this.game.add.tween(NaHCO3_dropper1).to( {x:1780, y:520}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaHCO3_dropper1_tween2()}, this);
    isNaHCO3_dropper1_taken=true;  
    }

  },
  NaHCO3_dropper1_tween2:function()
  {
    tween1=this.game.add.tween(NaHCO3_dropper1).to( {y:720}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.NaHCO3_dropper1_tween2_end()}, this);
  },
  NaHCO3_dropper1_tween2_end:function()
  {
    arrow.x=1794;
    arrow_y=475;
    arrow.y=475;
   
    arrow.visible=true;
    NaHCO3_dropper1.frameName="NaHCo3_dropper_to_testube0001.png";
    NaHCO3_dropper1.inputEnabled = false;
    NaHCO3_dropper1.input.useHandCursor = false;
    NaHCO3_dropper1.events.onDragStart.add(function() {this.onDragStart(NaHCO3_dropper1)}, this);
    NaHCO3_dropper1.events.onDragStop.add(function() {this.onDragStop(NaHCO3_dropper1)}, this);
    NaHCO3_dropper1.inputEnabled = true;
    NaHCO3_dropper1.input.useHandCursor = true;
    NaHCO3_dropper1.input.enableDrag(true);
    this.game.physics.arcade.enable(NaHCO3_dropper1);
    NaHCO3_dropper1.enableBody =true;

  },

  colors_poperty_enable:function()
  {
    color1.inputEnabled=true;
    color1.input.enableDrag(true);
  //  color1.body.enable=true;
    color2.inputEnabled=true;
    color2.input.enableDrag(true);
    //color2.body.enable=true;
    color3.inputEnabled=true;
    color3.input.enableDrag(true);
    //color3.body.enable=true;
    color4.inputEnabled=true;
    color4.input.enableDrag(true);
   // color4.body.enable=true;
    color5.inputEnabled=true;
    color5.input.enableDrag(true);
  //  color5.body.enable=true;
    color6.inputEnabled=true;
    color6.input.enableDrag(true);
    //color6.body.enable=true;
    color7.inputEnabled=true;
    color7.input.enableDrag(true);
   // color7.body.enable=true;
    color8.inputEnabled=true;
    color8.input.enableDrag(true);
  //  color8.body.enable=true;
    color9.inputEnabled=true;
    color9.input.enableDrag(true);
   // color9.body.enable=true;
    color10.inputEnabled=true;
    color10.input.enableDrag(true);
    //color10.body.enable=true;
    color11.inputEnabled=true;
    color11.input.enableDrag(true);
   // color11.body.enable=true;
    color12.inputEnabled=true;
    color12.input.enableDrag(true);
   // color12.body.enable=true;
    color13.inputEnabled=true;
    color13.input.enableDrag(true);
   // color13.body.enable=true;
    color14.inputEnabled=true;
    color14.input.enableDrag(true);
   // color14.body.enable=true;
  },
  colors_poperty_disable:function()
  {
    color1.inputEnabled=false;
    color1.input.enableDrag(false);
    color1.body.enable=false;
    color2.inputEnabled=false;
    color2.input.enableDrag(false);
    color2.body.enable=false;
    color3.inputEnabled=false;
    color3.input.enableDrag(false);
    color3.body.enable=false;
    color4.inputEnabled=false;
    color4.input.enableDrag(false);
    color4.body.enable=false;
    color5.inputEnabled=false;
    color5.input.enableDrag(false);
    color5.body.enable=false;
    color6.inputEnabled=false;
    color6.input.enableDrag(false);
    color6.body.enable=false;
    color7.inputEnabled=false;
    color7.input.enableDrag(false);
    color7.body.enable=false;
    color8.inputEnabled=false;
    color8.input.enableDrag(false);
    color8.body.enable=false;
    color9.inputEnabled=false;
    color9.input.enableDrag(false);
    color9.body.enable=false;
    color10.inputEnabled=false;
    color10.input.enableDrag(false);
    color10.body.enable=false;
    color11.inputEnabled=false;
    color11.input.enableDrag(false);
    color11.body.enable=false;
    color12.inputEnabled=false;
    color12.input.enableDrag(false);
    color12.body.enable=false;
    color13.inputEnabled=false;
    color13.input.enableDrag(false);
    color13.body.enable=false;
    color14.inputEnabled=false;
    color14.input.enableDrag(false);
    color14.body.enable=false;

  },


  update: function()
  {
    //count++;
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible)
    {
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
 
 
  },
  nextMsgSound:function(){
    // dialog.text="Click on the next button to do the experiment of HCL with zinc metal.";
    // play.visible=true;
    // procedure_voice10.stop();
    // procedure_voice11.play();
  },
  detectCollision:function()
  {
   

    if(collider_A.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, collider_A,function() {this.match_Obj()},null,this);
        collider=collider_A;
     
    } 
    if(testtube_A_collider.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, testtube_A_collider,function() {this.match_Obj()},null,this);
        collider=testtube_A_collider;
     
    } 

    if(testtube_B_collider.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, testtube_B_collider,function() {this.match_Obj()},null,this);
        collider=testtube_B_collider;
     
    } 
    if(testtube_C_collider.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, testtube_C_collider,function() {this.match_Obj()},null,this);
        collider=testtube_C_collider;
     
    } 
    if(testtube_D_collider.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, testtube_D_collider,function() {this.match_Obj()},null,this);
        collider=testtube_D_collider;
     
    } 
    if(testtube_E_collider.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, testtube_E_collider,function() {this.match_Obj()},null,this);
        collider=testtube_E_collider;
     
    } 
    if(testtube_F_collider.enableBody && currentobj!=null)
    {
      
        this.game.physics.arcade.overlap(currentobj, testtube_F_collider,function() {this.match_Obj()},null,this);
        collider=testtube_F_collider;
     
    } 

    if(HCl_litmus_collider.enableBody && currentobj!=null)
    {
      if(iscolorCheking==true)
      {
        this.game.physics.arcade.overlap(currentobj, HCl_litmus_collider,function() {this.match_Obj_color()},null,this);
        collider=HCl_litmus_collider;
      }
      else
      {
        this.game.physics.arcade.overlap(currentobj, HCl_litmus_collider,function() {this.match_Obj()},null,this);
        collider=HCl_litmus_collider;
      }
     
    } 
    if(NaOH_litmus_collider.enableBody && currentobj!=null)
    {
      if(iscolorCheking==true)
      {
        this.game.physics.arcade.overlap(currentobj, NaOH_litmus_collider,function() {this.match_Obj_color()},null,this);
        collider=NaOH_litmus_collider;
      }
      else
      {
        this.game.physics.arcade.overlap(currentobj, NaOH_litmus_collider,function() {this.match_Obj()},null,this);
        collider=NaOH_litmus_collider;
      }
    } 
    if(CH3COOH_litmus_collider.enableBody && currentobj!=null)
    {
      if(iscolorCheking==true)
      {
        this.game.physics.arcade.overlap(currentobj, CH3COOH_litmus_collider,function() {this.match_Obj_color()},null,this);
        collider=CH3COOH_litmus_collider;
      }
      else
      {
        this.game.physics.arcade.overlap(currentobj, CH3COOH_litmus_collider,function() {this.match_Obj()},null,this);
        collider=CH3COOH_litmus_collider;
      }
    } 
    if(Lemon_litmus_collider.enableBody && currentobj!=null)
    {
      if(iscolorCheking==true)
      {
        this.game.physics.arcade.overlap(currentobj, Lemon_litmus_collider,function() {this.match_Obj_color()},null,this);
        collider=Lemon_litmus_collider;
      }
      else
      {
        this.game.physics.arcade.overlap(currentobj, Lemon_litmus_collider,function() {this.match_Obj()},null,this);
        collider=Lemon_litmus_collider;
      }
    } 
    if(Water_litmus_collider.enableBody && currentobj!=null)
    {
      if(iscolorCheking==true)
      {
        this.game.physics.arcade.overlap(currentobj, Water_litmus_collider,function() {this.match_Obj_color()},null,this);
        collider=Water_litmus_collider;
      }
      else
      {
        this.game.physics.arcade.overlap(currentobj, Water_litmus_collider,function() {this.match_Obj()},null,this);
        collider=Water_litmus_collider;
      }
    } 
    if(NaHCO3_litmus_collider.enableBody && currentobj!=null)
    {
      if(iscolorCheking==true)
      {
        this.game.physics.arcade.overlap(currentobj, NaHCO3_litmus_collider,function() {this.match_Obj_color()},null,this);
        collider=NaHCO3_litmus_collider;
      }
      else
      {
        this.game.physics.arcade.overlap(currentobj, NaHCO3_litmus_collider,function() {this.match_Obj()},null,this);
        collider=NaHCO3_litmus_collider;
      }
    } 
    if(collider_B.enableBody && currentobj!=null)
    {
        this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
        collider=collider_B;
    }

    if(currentobj!=null && currentobj.body.enable)
    {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
      
       if(currentobj==HCl_dropper1){
      //  arrow.visible=false;
      //   arrow.x=1794;
      //   arrow.y=475;
      //   arrow_y=475;
      //   arrow.visible=true;
       
        
      }
      else if(currentobj==NaOH_dropper1)
      {
        //arrow.visible=true;
        // arrow.x=1794;
        // arrow.y=475;
        // arrow_y=475;
       
      }
      else if(currentobj==CH3COOH_dropper1)
      {
       //arrow.visible=true;
        // arrow.x=1794;
        // arrow.y=475;
        // arrow_y=475;
       
      }
      else if(currentobj==Lemon_dropper1)
      {
       // arrow.visible=true;
        // arrow.x=1794;
        // arrow.y=475;
        // arrow_y=475;
       
      }
      else if(currentobj==Water_dropper1)
      {
        //arrow.visible=true;
        // arrow.x=1794;
        // arrow.y=475;
        // arrow_y=475;
       
      }
      else if(currentobj==NaHCO3_dropper1)
      {
       //arrow.visible=true;
        // arrow.x=1794;
        // arrow.y=475;
        // arrow_y=475;
       
      }
      else if(currentobj==HCl_dropper3)
      {
       // arrow.visible=true;
        // arrow.x=576;
        // arrow.y=250;
        // arrow_y=250;
        HCl_dropper3.alpha=0;
        HCl_dropper2.visible=true;
       
      }
      else if(currentobj==NaOH_dropper3)
      {
      //  arrow.visible=true;
        // arrow.x=665;
        // arrow.y=250;
        // arrow_y=250;
        NaOH_dropper3.alpha=0;
        NaOH_dropper2.visible=true;
       
      }
      else if(currentobj==CH3COOH_dropper3)
      {
        //arrow.visible=true;
        // arrow.x=750;
        // arrow.y=250;
        // arrow_y=250;
        CH3COOH_dropper3.alpha=0;
        CH3COOH_dropper2.visible=true;
       
      }
      else if(currentobj==Lemon_dropper3)
      {
       // arrow.visible=true;
        // arrow.x=1078;
        // arrow.y=250;
        // arrow_y=250;
        Lemon_dropper3.alpha=0;
        Lemon_dropper2.visible=true;
       
      }
      else if(currentobj==Water_dropper3)
      {
       // arrow.visible=true;
        // arrow.x=1164;
        // arrow.y=250;
        // arrow_y=250;
        Water_dropper3.alpha=0;
        Water_dropper2.visible=true;
       
      }
      else if(currentobj==NaHCO3_dropper3)
      {
       // arrow.visible=true;
        // arrow.x=1245;
        // arrow.y=250;
        // arrow_y=250;
        NaHCO3_dropper3.alpha=0;
        NaHCO3_dropper2.visible=true;
       
      }
      // else if(currentobj==Dropper_Blue){
      //   arrow.x=1230;
      //   arrow.y=450;
      //   arrow_y=450;
      // }else if(currentobj==Dropper_Red){
      //   arrow.x=1410;
      //   arrow.y=450;
      //   arrow_y=450;
      // }else if(currentobj==Dropper_HCl_1){
      //   arrow.x=1580;
      //   arrow.y=450;
      //   arrow_y=450;
      // }

      //   currentobj=null;
    }
    
  },
//For to next scene
 
      toNextScene:function()
      {
       voice.destroy();
      // voice2.destroy();
      // procedure_voice1.destroy();
      // procedure_voice2.destroy();
      // procedure_voice3.destroy();
      // procedure_voice4.destroy();
      // procedure_voice5.destroy();
      // procedure_voice6.destroy();
      // procedure_voice7.destroy();
      // procedure_voice8.destroy();
      // procedure_voice9.destroy();
      // procedure_voice10.destroy();
      // procedure_voice11.destroy();
      //this.state.start("Experiment_A2", true, false, ip);
      this.state.start("Procedure_A2", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        // voice2.destroy();
        // procedure_voice1.destroy();
        // procedure_voice2.destroy();
        // procedure_voice3.destroy();
        // procedure_voice4.destroy();
        // procedure_voice5.destroy();
        // procedure_voice6.destroy();
        // procedure_voice7.destroy();
        // procedure_voice8.destroy();
        // procedure_voice9.destroy();
        // procedure_voice10.destroy();
        // procedure_voice11.destroy();
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  // voice.destroy();
  // voice2.destroy();
  // procedure_voice1.destroy();
  // procedure_voice2.destroy();
  // procedure_voice3.destroy();
  // procedure_voice4.destroy();
  // procedure_voice5.destroy();
  // procedure_voice6.destroy();
  // procedure_voice7.destroy();
  // procedure_voice8.destroy();
  // procedure_voice9.destroy();
  // procedure_voice10.destroy();
  // procedure_voice11.destroy();
    this.state.start("Experiment_A1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  // voice.destroy();
  // voice2.destroy();
  // procedure_voice1.destroy();
  // procedure_voice2.destroy();
  // procedure_voice3.destroy();
  // procedure_voice4.destroy();
  // procedure_voice5.destroy();
  // procedure_voice6.destroy();
  // procedure_voice7.destroy();
  // procedure_voice8.destroy();
  // procedure_voice9.destroy();
  // procedure_voice10.destroy();
  // procedure_voice11.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
