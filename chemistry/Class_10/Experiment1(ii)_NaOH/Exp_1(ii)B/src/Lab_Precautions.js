var lab_precautions = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
var audioFlag;
//audio
var voice;

//bools
var muted;

//ip address
var ip;


}

lab_precautions.prototype ={

init: function( ipadrs) 
{
  ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  audioFlag=true;
 muted = false;
 voice=this.game.add.audio("A_precaution",1);
 //voice2=this.game.add.audio("Aim_2",1);
 voice.play();
 voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Precautions /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  precaution_text=this.game.add.text(250,230,"Precautions:",headfontStyle);
 /* switch(exp_Name){
    case "Iron with copper sulphate":*/
      precaution1="1. Wash the test tubes and other apparatus before performing any test.";
      precaution2="2. Sodium hydroxide is a strong alkali. Handle it carefully as it is corrosive in\n    nature and can burn the skin.";
      precaution3="3. Use small quantities of reagents. ";
      precaution4="4. The test for the reaction of sodium hydroxide with zinc metal should be\n    carried out strictly under the supervision of your class teacher because\n    hydrogen gas forms an explosive mixture with air.";
      precaution5="5. Wash your hands properly with soap after completing the experiments.";
      //precaution6="6. Wash your hands properly with soap after completing the experiment.";
      //precaution7="The color of the pH paper after adding sample to it, may be shows slightly \ndark or light with color of the pH color chart. So please note exact \npH value instead of range of values.";
      precaution1_text=this.game.add.text(300,330,precaution1,fontStyle);
      precaution2_text=this.game.add.text(300,410,precaution2,fontStyle);
      precaution3_text=this.game.add.text(300,550,precaution3,fontStyle);
      precaution4_text=this.game.add.text(300,650,precaution4,fontStyle);
      precaution5_text=this.game.add.text(300,860,precaution5,fontStyle);
     // precaution6_text=this.game.add.text(300,870,precaution6,fontStyle);
      //precaution7_text=this.game.add.text(300,810,precaution7,fontStyle);

      //voice=this.game.add.audio("precautions_1",1);
      //voice.play();
    //break;
  
  //}

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toProcedure, this);


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },

  ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

  // For Full screen checking.
  
      gofull: function()
      {
        if (this.game.scale.isFullScreen)
            {
            this.game.scale.stopFullScreen();
            }
        else
            {
             this.game.scale.startFullScreen(false);
            }  
      },
      nextSound:function(){
        //voice.destroy();
        if(audioFlag){
          voice=this.game.add.audio("A_precaution2",1);
          //voice2=this.game.add.audio("Aim_2",1);
          voice.play();
      }
      },
  //For to next scene   
 
      toProcedure:function()
      {
        audioFlag=false;
      voice.destroy();
      console.log("pppppppppp");
      this.state.start("Procedure_A1", true, false, ip);
      //this.state.start("Exp_Selection", true, false);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

gotoHome:function()
{
  audioFlag=false;
  voice.destroy();
  this.state.start("Aim", true, false, ip);
},

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/

// To quit the experiment
closeTheGame:function()
{
  audioFlag=false;
voice.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link
//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
