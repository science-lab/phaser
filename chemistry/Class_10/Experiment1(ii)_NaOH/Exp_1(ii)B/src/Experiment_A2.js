var experiment_a2 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;

var colorChangingAnimstart;
var TostartTesttubeB;
var isBurneron;
}

experiment_a2.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
 procedure_voice1=this.game.add.audio('step_1',1);
 procedure_voice2=this.game.add.audio('step_2',1);
 procedure_voice3=this.game.add.audio('step_3',1);
 procedure_voice4=this.game.add.audio('step_4',1);
 procedure_voice5=this.game.add.audio('step_5',1);
 procedure_voice6=this.game.add.audio('step_6',1);
 procedure_voice7=this.game.add.audio('step_7',1);
 procedure_voice8=this.game.add.audio('step_8',1);
 procedure_voice9=this.game.add.audio('step_9',1);
 procedure_voice10=this.game.add.audio('step_10',1);
 procedure_voice11=this.game.add.audio('step_11',1);
 procedure_voice12=this.game.add.audio('step_12',1);
 procedure_voice13=this.game.add.audio('step_13',1);
 procedure_voice14=this.game.add.audio('step_14',1);
 procedure_voice15=this.game.add.audio('step_15',1);
 popsound=this.game.add.audio('popsound');
 //procedure_voice10=this.game.add.audio('A_procedure10',1);
 //procedure_voice11=this.game.add.audio('A_procedure11',1);
 
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
 colorChangingAnimstart=false;
 delay=0;
 TostartTesttubeB=false;
 isBurneron=false;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();


  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toObservations, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

  this.arrangeScene();

  dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.82,.8);
      dialog_text="Take a boiling tube to the clamp stand.";
      procedure_voice1.play();
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

      // arrow.x=850;
      // arrow.y=280;
      // arrow_y=280;
      // arrow.visible=  true;


      // Burned_Match_Stick = this.game.add.sprite(822,380,"Burned_Match_Stick");
      // Burned_Match_Stick.scale.setTo(1.25,1.25);
    // Black_smoke =  this.game.add.sprite(780, 278,'Black_smoke','Black smoke0002.png');
    // Black_smoke.scale.setTo(.4,.4);

//     Flame_1 =  this.game.add.sprite(870,340,'Flame','Flame0002.png');
// //1300, 730
//   Flame_1.scale.setTo(1.25,1.25);
//   Flame_1.anchor.setTo(.5,.5);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
////////////////////////Add Items on the scene///////////////////////////
arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
 //holder1.visible=false;
 //holder2.visible=false;

 
  
  

  

  HCl_Back = this.game.add.sprite(1505, 598,'Jar_Back');
  HCl_Back.scale.setTo(.3,.4);
  HCl = this.game.add.sprite(1510, 600,'HCl');
  HCl.scale.setTo(.8,1);
  //HCL_a2 = this.game.add.sprite(1510, 600,'HCL_a2');
  Dropper_HCl_1 = this.game.add.sprite(1560, 300,'Dropper_HCl_OUT','Dropper_HCl_OUT_0016.png');
  //1560, 300
  Dropper_HCl_1.scale.setTo(.8,1);
  Dropper_HCl_1.visible=false;
  Dropper_HCl_1.events.onDragStart.add(function() {this.onDragStart(Dropper_HCl_1)}, this);
  Dropper_HCl_1.events.onDragStop.add(function() {this.onDragStop(Dropper_HCl_1)}, this);
  Dropper_HCl_1.inputEnabled = true;
  Dropper_HCl_1.input.useHandCursor = true;
  Dropper_HCl_1.input.enableDrag(true);
  this.game.physics.arcade.enable(Dropper_HCl_1);
  Dropper_HCl_1.enableBody =true;
  

  Dropper_HCl_1.animations.add('anim',[7,8,9,10,11,12,13,14,15,], 30, false, true);
  //0,1,2,3,4,5,6,

  HCl_front = this.game.add.sprite(1510, 600,'HCl_front');
  HCl_front.scale.setTo(.8,1);
  HCl_front.visible=false;
  corck = this.game.add.sprite(1470, 800,'cork');
  corck.scale.setTo(.8,1);
  corck.visible=false;
  HCl_text = this.game.add.text(1550,810,'NaOH',labelfontStyle1)

  Dropper_HCl = this.game.add.sprite(1750, 900,'Dropper_HCl_OUT','Dropper_HCl_OUT_0016.png');
  Dropper_HCl.scale.setTo(.8,1);
  Dropper_HCl.angle=80;

  Zn_plate = this.game.add.sprite(1100, 720,'Zn_plate');
  Zn_plate.scale.set(0.75,0.75);
  Spoon = this.game.add.sprite(1200, 700,'Spoon');
  Spoon.scale.set(0.75,0.75);
  Spoon_with_Zn = this.game.add.sprite(1200, 700,'Spoon_with_Zn');
  Spoon_with_Zn.scale.set(0.75,0.75);
  Spoon_with_Zn.alpha=0;
  Spoon_with_Zn.events.onDragStart.add(function() {this.onDragStart(Spoon_with_Zn)}, this);
  Spoon_with_Zn.events.onDragStop.add(function() {this.onDragStop(Spoon_with_Zn)}, this);
  Spoon_with_Zn.inputEnabled = true;
  Spoon_with_Zn.input.useHandCursor = true;
  //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
  Spoon_with_Zn.input.enableDrag(true);
  this.game.physics.arcade.enable(Spoon_with_Zn);
  Spoon_with_Zn.enableBody =true;
  // Testtube_Cork_rod = this.game.add.sprite(1250, 920,'Testtube_Cork_rod');
  // Testtube_Cork_rod.anchor.setTo(.5,.5);
  // Testtube_Cork_rod.angle=90;

  litmusGroup=this.game.add.group();
  litmusGroup.add(HCl_Back);
  litmusGroup.add(Dropper_HCl_1);
  litmusGroup.add(HCl);
  litmusGroup.add(Dropper_HCl);
  litmusGroup.add(HCl_front);
  litmusGroup.add(HCl_text);
  litmusGroup.add(Zn_plate);
  litmusGroup.add(Spoon);
  litmusGroup.add(Spoon_with_Zn);
  //litmusGroup.add(Testtube_Cork_rod);
  litmusGroup.add(corck);


  litmusGroup.visible=false;
  litmusGroup.x=2500;

  testtube_holder_heating=this.game.add.sprite(220,300,'Testtube_heating','Testtube Holder_Zin_Reaction0001.png');
  testtube_holder_heating.scale.set(.68,.9);
  testtube_holder_heating.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],10,true,true);
  testtube_holder_heating.animations.add('anim2',[15,16,17,18,19,20,21,22,23,24],10,true,true);
  testtube_holder_heating.angle=30;
  testtube_holder_heating.visible=false;

  burner=this.game.add.sprite(-350,630,'burner');
  burner_knob_back=this.game.add.sprite(90,860,'burner_knob_back');

  burner_knob_front=this.game.add.sprite(140,910,'burner_knob_front');
burner_knob_front.anchor.setTo(.5,.5);
  
burner_flame=this.game.add.sprite(105,565,'Burner_flame','Flame0001.png');
burner_flame.visible=false;
burner_flame.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,],10,true,true);

  
 /////////////////////////////////////////////////////////////////////
  rackGroup=this.game.add.group();

  holder1 = this.game.add.sprite(1350, 570,'holder1');
  
  TestTube_Nill_Back_1 = this.game.add.sprite(809, 535,'TestTube_Nill_Back');
  TestTube_Nill_Back_1.scale.setTo(.68,.8);
  TestTube_Nill_Back_1.visible=false;



  TestTube_1 = this.game.add.sprite(1455, 580,'Testtube_a2');
  TestTube_1.scale.setTo(.7,.9);

  TestTube_1.events.onDragStart.add(function() {this.onDragStart(TestTube_1)}, this);
  TestTube_1.events.onDragStop.add(function() {this.onDragStop(TestTube_1)}, this);
  TestTube_1.inputEnabled = true;
  TestTube_1.input.useHandCursor = true;
  //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
  TestTube_1.input.enableDrag(true);
  this.game.physics.arcade.enable(TestTube_1);
  TestTube_1.enableBody =true;

  TestTube_2 = this.game.add.sprite(1535, 580,'Testtube_a2');
  TestTube_2.scale.setTo(.7,.9);

 
  holder2 = this.game.add.sprite(1350, 570,'holder2');
  holder1.scale.setTo(1.25,1.25);
  holder2.scale.setTo(1.25,1.25);
  rackGroup.add(holder1);
  rackGroup.add(TestTube_1);
  rackGroup.add(TestTube_2);
  rackGroup.add(holder2);

  TestTubeStand_Emty = this.game.add.sprite(550, 440,'TestTubeStand_Emty');
  TestTubeStand_Emty.scale.setTo(.9,1);

  //TestTubeStand_Hanger1_B = this.game.add.sprite(450, 385,'TestTubeStand_Hanger_B');
  //
  
  White_smoke =  this.game.add.sprite(220,360,'White_smoke','White smoke0002.png');
White_smoke.scale.setTo(.4,.5);
White_smoke.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,], 10, true, true);
White_smoke.angle=30;
White_smoke.visible=false;

White_smoke1 =  this.game.add.sprite(812, 270,'White_smoke','White smoke0002.png');
White_smoke1.alpha=.5;
White_smoke1.scale.setTo(.3,.5);
White_smoke1.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,], 10, true, true);
White_smoke1.visible=false;

White_smoke2 =  this.game.add.sprite(810, 560,'White_smoke','White smoke0002.png');
White_smoke2.alpha=.5;
White_smoke2.scale.setTo(.3,.5);
White_smoke2.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,], 10, true, true);
White_smoke2.visible=false;
  //TestTubeStand_Hanger2_B = this.game.add.sprite(650, 385,'TestTubeStand_Hanger_B');
  TestTube_A = this.game.add.sprite(805, 523,'Testtube_a2');
  TestTube_A.scale.setTo(.7,.9);
  TestTube_A.visible=false;
 
  Spoon_sprite=this.game.add.sprite(820,380,'A2_Spoon_sprite','Spoon_to_testube0001.png')
  Spoon_sprite.scale.setTo(.75,.75);
  Spoon_sprite.animations.add('anim',[1,2,3,4], 24, false, true);
  Spoon_sprite.visible=false;
  //Spoon_sprite.animations.play('anim');

  Zn_testtube =  this.game.add.sprite(805, 511,'Zn_testtube','Zn_to_testube0001.png');
  Zn_testtube.scale.setTo(.7,.9);
  Zn_testtube.visible=false;
  Zn_testtube.animations.add('anim',[1,2,3,4,5,6,7], 8, false, true);

  Cork_tube_sprite=this.game.add.sprite(787,390,'Cork_tube_sprite','Tube and cork0001.png');
  Cork_tube_sprite.scale.setTo(.7,.7);
  Cork_tube_sprite.animations.add('anim1',[0,1,2,3,4],10,false,true);
  Cork_tube_sprite.visible=false;

  Zinc_colorchange=this.game.add.sprite(787,511,'Zinc_colorchange','Zin_Colour_Change0001.png');
  Zinc_colorchange.scale.setTo(.7,.9);
  Zinc_colorchange.animations.add('anim1',[0,1,2,3,4,5,6,7],8,true,true);
  Zinc_colorchange.animations.add('anim2',[8,9,10,11,12,13,14],8,true,true);
  Zinc_colorchange.visible=false;



  HCl_testtube =  this.game.add.sprite(787, 510,'NaOH_testtube','NaOH_to_Testtube0001.png');
  HCl_testtube.scale.setTo(.7,.9);
  HCl_testtube.visible=false;
  HCl_testtube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,17,18,19,20,21,22,23], 10, false, true);
  //HCl_testtube.animations.add('anim_1',[26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49], 10, true, true);
  // HCl_testtube.animations.add('anim_1',[26,27,36,37,38], 2, true, true);

  

  


  Cork_tube_testtube =  this.game.add.sprite(290, 370,'Cork_tube_testtube','Testtube Cork rod0001.png');
  Cork_tube_testtube.scale.setTo(.7,.9);
  Cork_tube_testtube.visible=false;
  Cork_tube_testtube.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,], 10, true, true);

  testtube_holder_zn =  this.game.add.sprite(774, 513,'Testtube_holder_Zn');
testtube_holder_zn.scale.setTo(.68,.9);
testtube_holder_zn.visible=false;

 TestTubeStand_Hanger_A = this.game.add.sprite(800, 605,'TestTubeStand_Hanger_A');
  TestTubeStand_Hanger_A.scale.setTo(.85,1);
  TestTubeStand_Hanger_B = this.game.add.sprite(950, 605,'TestTubeStand_Hanger_A');
  TestTubeStand_Hanger_B.scale.setTo(.85,1);
  

Match_Box = this.game.add.sprite(1150, 705,'Match_Box');
Match_Box.scale.setTo(1.25,1.25);
Match_Stick = this.game.add.sprite(1230, 755,'Match_Stick');
Match_Stick.scale.setTo(1.25,1.25);
//Match_Stick.angle=30;
// testtube_holder_zn =  this.game.add.sprite(774, 493,'Testtube_holder_Zn');
// testtube_holder_zn.scale.setTo(.68,.9);
// testtube_holder_zn.visible=true;

testtube_holder=this.game.add.sprite(1400,730,'Testtube_holder'); 

Testtube_Cork_rod = this.game.add.sprite(1250, 920,'Testtube_Cork_rod');
Testtube_Cork_rod.anchor.setTo(.5,.5);
Testtube_Cork_rod.angle=90;
  
Flame_1 =  this.game.add.sprite(1300, 730,'Flame','Flame0002.png');
//1300, 730
  Flame_1.scale.setTo(1.25,1.25);
  Flame_1.anchor.setTo(.5,.5);
  Flame_1.alpha=0;
  Flame_1.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,], 30, true, true);
  Flame_1.events.onDragStart.add(function() {this.onDragStart(Flame_1)}, this);
  Flame_1.events.onDragStop.add(function() {this.onDragStop(Flame_1)}, this);
  // Flame_1.inputEnabled = true;
  // Flame_1.input.useHandCursor = true;
  // Flame_1.input.enableDrag(true);
  this.game.physics.arcade.enable(Flame_1);
  Flame_1.enableBody =true;

Match_BoxGroup=this.game.add.group();
Match_BoxGroup.add(Match_Box);
Match_BoxGroup.add(Match_Stick);
Match_BoxGroup.add(Flame_1);
Match_BoxGroup.add(Testtube_Cork_rod);
Match_BoxGroup.add(testtube_holder);
Match_BoxGroup.x=2500;

 collider_A = this.game.add.sprite(790,420,'collider');
 collider_A.alpha=0;
 collider_A.scale.setTo(2,4);
 this.game.physics.arcade.enable(collider_A);//For colling purpose
collider_A.inputEnabled = true;
 collider_A.enableBody =true;


 collider_B = this.game.add.sprite(65, 320,'collider');
 collider_B.alpha=0;
 collider_B.scale.setTo(3,6);
 this.game.physics.arcade.enable(collider_B);//For colling purpose
  
arrow=this.game.add.sprite(1500,450, 'arrow');
      arrow.angle=90;
      arrow_y=480;


},

onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    if(currentobj==TestTube_1){
      currentobj.xp=1455;
      currentobj.yp=580;
      arrow.x=855;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
      //collider=TestTubeStand_Hanger_A;
    }else if(currentobj==Spoon_with_Zn){
      Spoon.visible=false
      Spoon_with_Zn.alpha=1;
      currentobj.xp=1200;
      currentobj.yp=700;
      arrow.x=855;
      arrow.y=420;
      arrow_y=420;
      arrow.visible=true;
    }
    else if(currentobj==Dropper_HCl_1){
      Dropper_HCl_1.frameName = "Dropper_HCl_OUT_0001.png";
      
      currentobj.xp=1560;
      currentobj.yp=550;
      
      arrow.x=855;
      arrow.y=420;
      arrow_y=420;
      arrow.visible=true;
    }
    
    else if(currentobj==testtube_holder){
      //Spoon.visible=false
     // Spoon_with_Zn.alpha=1;
      currentobj.xp=1400;
      currentobj.yp=730;
      arrow.x=855;
      arrow.y=420;
      arrow_y=420;
      arrow.visible=true;
    }

    else if(currentobj==testtube_holder_zn){
      //Spoon.visible=false
     // Spoon_with_Zn.alpha=1;
      currentobj.xp=774;
      currentobj.yp=513;
      arrow.x=150;
      arrow.y=400;
      arrow_y=400;
      arrow.visible=true;
    }
    else if(currentobj==testtube_holder_heating){
      //Spoon.visible=false
     // Spoon_with_Zn.alpha=1;
      currentobj.xp=75;
      currentobj.yp=300;
      arrow.x=855;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
      White_smoke.visible=false;
      testtube_holder_heating.angle=0;
      testtube_holder_heating.animations.play('anim2');
    }

    else if(currentobj==Testtube_Cork_rod){
      Testtube_Cork_rod.angle=0;
      currentobj.xp=1250;
      currentobj.yp=920;
      arrow.x=855;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
    }else if(currentobj==Flame_1){
      currentobj.xp=1300; 
      currentobj.yp=730;
      console.log("ppppppppppp")
      Match_Stick.visible=false;
      Flame_1.alpha=1;
      Flame_1.animations.play("anim");
      arrow.x=850;
      arrow.y=280;
      arrow_y=280;
      arrow.visible=  true;
    }
    
  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    
    if(currentobj==TestTube_1){//
        currentobj.visible=false;
        TestTube_A.visible=true;
        arrow.visible=false;
        tween1=this.game.add.tween(rackGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
        tween1.onComplete.add(function(){this.tween_rackExit()}, this);
      }else if(currentobj==Spoon_with_Zn){//
        
        arrow.visible=false;
        Spoon_with_Zn.visible=false;
        TestTube_A.visible=false;
        Zn_testtube.visible=true;
        TestTube_Nill_Back_1.visible=true;
        Zn_testtube.animations.play("anim");
        Spoon_sprite.visible=true;
        Spoon_sprite.animations.play('anim');
        Zn_testtube.animations.currentAnim.onComplete.add(this.animationStopped_Zn_testtube, this);
        Spoon_sprite.animations.currentAnim.onComplete.add(this.animationStopped_spoon, this);
      }else if(currentobj==Dropper_HCl_1){
        arrow.visible=false;
        Dropper_HCl_1.x=820
        Dropper_HCl_1.y=300;
        Dropper_HCl_1.animations.play("anim");
        HCl_testtube.animations.play("anim");
        HCl_testtube.animations.currentAnim.onComplete.add(this.animationStopped_HCl_testtube, this);
          

        }

        else if(currentobj==testtube_holder)
        {
          HCl_testtube.visible=false;
          testtube_holder.visible=false;
          testtube_holder_zn.visible=true;
         // arrow.visible=false;
          TestTube_Nill_Back_1.visible=false; 
          collider_A.enableBody=false;
          collider_A.visible=false;

          collider_B.inputEnabled = true;
          collider_B.enableBody =true;
          
          procedure_voice6.stop();
          procedure_voice7.play();

          dialog.text="Drag the boiling tube to the burner by using the test tube holder."
          testtube_holder_zn.events.onDragStart.add(function() {this.onDragStart(testtube_holder_zn)}, this);
          testtube_holder_zn.events.onDragStop.add(function() {this.onDragStop(testtube_holder_zn)}, this);
          testtube_holder_zn.inputEnabled = true;
          testtube_holder_zn.input.useHandCursor = true;

    //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
    testtube_holder_zn.input.enableDrag(true);
          this.game.physics.arcade.enable(testtube_holder_zn);
          testtube_holder_zn.enableBody =true;
        }


        else if(currentobj==testtube_holder_zn)
        {
          arrow.visible=false;
          testtube_holder_zn.visible=false;
          testtube_holder_heating.visible=true;
          collider_A.visible=true;

          burner_knob_front.inputEnabled = true;
          burner_knob_front.input.useHandCursor = true;
          burner_knob_front.events.onInputDown.add(this.burner_knob_front_click, this);
          arrow.x=150;
          arrow.y=780;
          arrow_y=780;
          arrow.visible=  true;
          procedure_voice7.stop();
          procedure_voice8.play();
          dialog.text="Turn on the burner by clicking on the knobe, and observe";
        }
        else if(currentobj==testtube_holder_heating)
        {
          arrow.visible=false;
          testtube_holder_heating.visible=false;
          Zinc_colorchange.visible=true;
          Zinc_colorchange.animations.play('anim1');
          TestTube_Nill_Back_1.visible=true;
          this.putCork();
         // dialog.text="Hurry up....close the mouth of the test tube by cork with delivery tube"


        }


        else if(currentobj==Testtube_Cork_rod){
          
        
          procedure_voice12.stop();
          procedure_voice13.play();
          dialog.text="Bring a light matchstick near the glass tube nozzle and observe.";
          
          arrow.visible=false;
          HCl_testtube.visible=false;
          Testtube_Cork_rod.visible=false;
          //Cork_tube_testtube.visible=true;
          //Cork_tube_testtube.animations.play("anim");
          //White_smoke.alpha=.3;
         //  White_smoke.y=270;
         Cork_tube_sprite.visible=true;
         Cork_tube_sprite.animations.play('anim1');
         White_smoke2.visible=true;
         White_smoke2.animations.play('anim1');
         White_smoke1.visible=true;
         White_smoke1.animations.play('anim1');

         Zinc_colorchange.visible=false;
         Zinc_colorchange.visible=true;
         Zinc_colorchange.animations.play('anim2');

          //collider_A.enableBody=false;
          collider_A.y=280;
          
          this.Match_BoxGroup_in();

          // collider_B.inputEnabled = true;
          // collider_B.enableBody =true;
          // tween1=this.game.add.tween(litmusGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
          // tween1.onComplete.add(function(){this.tween_litmusGroupExit()}, this);
        }else if(currentobj==Flame_1){
          arrow.visible=false;
          Flame_1.x=870;
          Flame_1.y=340;
          procedure_voice13.destroy();
          this.game.time.events.add(Phaser.Timer.SECOND*1,this.addBlueFlame, this);
          
        }


  },
  addBlueFlame:function(){
    Flame_1.visible=false;
  Blue_flame =  this.game.add.sprite(865, 280,'Blue_flame','Blue Flame0002.png');

  Blue_flame.scale.setTo(1.25,1.25);
  Blue_flame.anchor.setTo(.5,.5);

  Blue_flame.alpha=1;
  Blue_flame.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,], 30, true, true);
  Blue_flame.animations.play("anim");
  this.stopBlueFlame();
  //this.game.time.events.add(Phaser.Timer.SECOND*.3,this.stopBlueFlame, this);
  },
  stopBlueFlame:function(){
    Blue_flame.visible=false;
    Burned_Match_Stick = this.game.add.sprite(822,380,"Burned_Match_Stick");
    Burned_Match_Stick.scale.setTo(1.25,1.25);
  Black_smoke =  this.game.add.sprite(780, 278,'Black_smoke','Black smoke0002.png');
  popsound.play();

  Black_smoke.scale.setTo(.4,.4);
  //Black_smoke.anchor.setTo(.5,.5);
  Black_smoke.alpha=1;
  Black_smoke.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30], 20, true, true);
  Black_smoke.animations.play("anim");
  
  this.game.time.events.add(Phaser.Timer.SECOND*2,this.popsound_end, this);
  // procedure_voice13.stop();
  // procedure_voice14.play();
  // dialog.text="Observe that the match stick is extinguished with a pop sound.";
 
  // this.game.time.events.add(Phaser.Timer.SECOND*6,this.nextMsgSound, this);

  
  },
  popsound_end:function()
  {
    procedure_voice13.stop();
    procedure_voice14.play();
    dialog.text="Observe that the match stick is extinguished with a pop sound.";
   
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.nextMsgSound, this);

  },
  tween_litmusGroupExit:function(){
    procedure_voice5.stop();
    procedure_voice6.play();
    dialog.text="Lets see what happens when we lightly warm the boiling tube, for that\nhold boiling tube with the test tube holder";
    tween1=this.game.add.tween(Match_BoxGroup).to( {x:0}, 600, Phaser.Easing.Out, true);
    //tween1.onComplete.add(function(){this.Match_BoxGroup_in()}, this);
  },

  Match_BoxGroup_in:function()
  {
          Flame_1.inputEnabled = true;
          Flame_1.input.useHandCursor = true;
          Flame_1.input.enableDrag(true);
    arrow.x=1280;
    arrow.y=650;
    arrow_y=650;
    arrow.visible=true;

  },

  burner_knob_front_click:function()
  {
    if(isBurneron==false)
    {
      arrow.visible=false;
      burner_knob_front.angle=90;
      burner_flame.visible=true;
      burner_knob_front.inputEnabled = false;
      burner_knob_front.input.useHandCursor = false;
      burner_flame.animations.play('anim1');
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.burnerOn, this);
      
      isBurneron=true;
      
    }
    else if(isBurneron==true)
    {

      burner_knob_front.inputEnabled = false;
      burner_knob_front.input.useHandCursor = false;

      burner_knob_front.angle=0;
      burner_flame.visible=false;
      collider_A.enableBody=true;
      collider_A.inputEnabled=true;
      collider_B.inputEnabled = false;
      collider_B.enableBody =false;
      procedure_voice10.stop();
      procedure_voice11.play();
      dialog.text="Take the boiling tube back to the clamp stand";
      
      arrow.x=200;
      arrow.y=200;
      arrow_y=200;
      arrow.visible=  true;
      
      testtube_holder_heating.events.onDragStart.add(function() {this.onDragStart(testtube_holder_heating)}, this);
      testtube_holder_heating.events.onDragStop.add(function() {this.onDragStop(testtube_holder_heating)}, this);
      testtube_holder_heating.inputEnabled = true;
      testtube_holder_heating.input.useHandCursor = true;
      testtube_holder_heating.input.enableDrag(true);
      this.game.physics.arcade.enable(testtube_holder_heating);
      testtube_holder_heating.enableBody =true;

    }

  },

  turnOffBurner:function()
  {
procedure_voice9.stop();
procedure_voice10.play();
dialog.text="Now turn off the burner";
burner_knob_front.inputEnabled = true;
burner_knob_front.input.useHandCursor = true;

  },

  burnerOn:function()
  {
    White_smoke.visible=true;
    White_smoke.animations.play('anim1');
    testtube_holder_heating.animations.play('anim1');
    procedure_voice8.stop();
    procedure_voice9.play();
    dialog.text="See that the zinc started to react with NaOH.\nSee the bubbles coming from the zinc granules";
    this.game.time.events.add(Phaser.Timer.SECOND*9,this.turnOffBurner, this);

  },
  animationStopped_HCl_testtube:function(){
    dialog.text="Since the reaction of NaOH with Zn is slow,";
    procedure_voice4.stop();
    procedure_voice5.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.nextStep, this);
    //HCl_testtube.animations.play("anim_1");
    Dropper_HCl_1.visible=false;
    corck.visible=false;
    HCl.visible=true;
    HCl_front.visible=false;
    // White_smoke.visible=true;
    // White_smoke.animations.play("anim");
    arrow.visible=false;
    /**/
  },
  nextStep:function()
  {
    
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.takeHolder, this);
    tween1=this.game.add.tween(litmusGroup).to( {x:2560}, 600, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.tween_litmusGroupExit()}, this);
  
    
   

  },

  takeHolder:function()
  {
    
    testtube_holder.events.onDragStart.add(function() {this.onDragStart(testtube_holder)}, this);
    testtube_holder.events.onDragStop.add(function() {this.onDragStop(testtube_holder)}, this);
    testtube_holder.inputEnabled = true;
    testtube_holder.input.useHandCursor = true;
    //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
    testtube_holder.input.enableDrag(true);
    this.game.physics.arcade.enable(testtube_holder);
    testtube_holder.enableBody =true;
    arrow.x=1600;
    arrow.y=620;
    arrow_y=620;
    arrow.visible=true;
  },
  putCork:function(){
    procedure_voice11.stop();
    procedure_voice12.play();
    dialog.text="Hurry up....Close the mouth of the boiling tube by the cork with delivery\ntube";
    
    Testtube_Cork_rod.events.onDragStart.add(function() {this.onDragStart(Testtube_Cork_rod)}, this);
    Testtube_Cork_rod.events.onDragStop.add(function() {this.onDragStop(Testtube_Cork_rod)}, this);
    Testtube_Cork_rod.inputEnabled = true;
    Testtube_Cork_rod.input.useHandCursor = true;
    //TestTube_1.events.onInputDown.add(this.ClickOnSpoon, this);
    Testtube_Cork_rod.input.enableDrag(true);
    this.game.physics.arcade.enable(Testtube_Cork_rod);
    Testtube_Cork_rod.enableBody =true;
    arrow.x=1300;
    arrow.y=750;
    arrow_y=750;
    arrow.visible=true;
  },
  animationStopped_Zn_testtube:function(){
    //tween1=this.game.add.tween(Dropper_Blue).to( {x:1200, y:550}, 600, Phaser.Easing.Out, true);
    //tween1.onComplete.add(function(){this.tween_testTubeA_Return()}, this);
    Zn_testtube.visible=false;
    HCl_testtube.visible=true;
    Spoon.visible=true;
  
    dialog.text="Take a dropper";
    procedure_voice2.stop();
    procedure_voice3.play();
    arrow.x=1700;
    arrow.y=800;
    arrow_y=800;
    arrow.visible=true;
    Dropper_HCl.inputEnabled = true;
    Dropper_HCl.input.useHandCursor = true;
    Dropper_HCl.events.onInputDown.add(this.ClickOnDropperHCl, this);
  },
  animationStopped_spoon:function()
  {

    Spoon_sprite.visible=false;
  },
  tween_rackExit:function(){
    rackGroup.visible=false;
    litmusGroup.visible=true;
    tween2=this.game.add.tween(litmusGroup).to( {x:0}, 600, Phaser.Easing.Out, true);
    tween2.onComplete.add(function(){this.tween_solutions_in()}, this);///////tween_testTubeB_Return
  },
  tween_solutions_in:function(){
    dialog.text="Add few granules of zinc metal into the boiling tube.";
    procedure_voice1.stop();
    procedure_voice2.play();
    arrow.x=1250;
    arrow.y=650;
    arrow_y=650;
    arrow.visible=true;
    collider_A.enableBody=true;
  },
  ClickOnDropperHCl:function(){
    arrow.visible=false;
    Dropper_HCl.angle=0;
    Dropper_HCl.x=1470;
    Dropper_HCl.y=650;
    tween1=this.game.add.tween(Dropper_HCl).to( {x:1560, y:300}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.tween_DropperHCl()}, this);
  },
  tween_DropperHCl:function(){
    arrow.visible=false;
    Dropper_HCl.visible=false;
    Dropper_HCl_1.visible=true;
    corck.visible=true;
    HCl.visible=false;
    HCl_front.visible=true;/**/
    tween1=this.game.add.tween(Dropper_HCl_1).to( {y:550}, 300, Phaser.Easing.Out, true);
    tween1.onComplete.add(function(){this.tween_DropperHCl_1(1)}, this);
  },
  tween_testTube_HClA_Return:function(){
    
      tween1=this.game.add.tween(Dropper_HCl_1).to( {y:550}, 600, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.tween_DropperHCl_1(2)}, this);
    
  },
  tween_DropperHCl_1:function(count){
       dialog.text="Add dilute NaOH to the boiling tube and observe.";
      procedure_voice3.stop();
      procedure_voice4.play();
      collider_A.enableBody=true;
      arrow.x=1590;
      arrow.y=450;
      arrow_y=450;
      arrow.visible=true;
     
  },
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    if(colorChangingAnimstart==true){
      delay++;
      if(delay>50){
        colorChangingAnimstart=false;
        TestTube_A.animations.play("anim_2");
        this.game.time.events.add(Phaser.Timer.SECOND*5,this.animationStopped_TestTubeA, this);
        //TestTube_A.animations.currentAnim.onComplete.add(this.animationStopped_TestTubeA, this);
      }
    }
    
  },
  nextMsgSound:function(){
    procedure_voice14.stop();
    procedure_voice15.play();
    dialog.text="Lets move to the next experiment of NaOH with Na\u2082CO\u2083, for that\nclick on the next button";
    //dialog.text="Click on the next button to do the experiment of NaOH with sodium \ncarbonate.";
    play.visible=true;
    

  },
  detectCollision:function(){
   

    if(collider_A.enableBody && currentobj!=null){
      
        this.game.physics.arcade.overlap(currentobj, collider_A,function() {this.match_Obj()},null,this);
        collider=collider_A;
     
    } 
    if(collider_B.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
        collider=collider_B;
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
      
      
      
      if(currentobj==TestTube_1){
      
        arrow.x=1500;
        arrow.y=450;
        arrow_y=450;
        arrow.visible=true;
        
      }else if(currentobj==Spoon_with_Zn){
        Spoon.visible=true;
        Spoon_with_Zn.alpha=0;
        arrow.x=1250;
        arrow.y=650;
        arrow_y=650;
      }else if(currentobj==Dropper_HCl_1){
        arrow.x=1580;
        arrow.y=450;
        arrow_y=450;
      }
      else if(currentobj==testtube_holder){
        //Spoon.visible=false
       // Spoon_with_Zn.alpha=1;
        // currentobj.xp=1400;
        // currentobj.yp=730;
        arrow.x=1600;
    arrow.y=620;
    arrow_y=620;
    arrow.visible=true;
      }

      else if(currentobj==testtube_holder_zn){
        //Spoon.visible=false
       // Spoon_with_Zn.alpha=1;
        // currentobj.xp=1400;
        // currentobj.yp=730;
        arrow.x=855;
        arrow.y=420;
        arrow_y=420;
        arrow.visible=true;
      }

      else if(currentobj==testtube_holder_heating)
      {
        arrow.x=150;
        arrow.y=200;
        arrow_y=200;
        arrow.visible=  true;
      }
      
      else if(currentobj==Testtube_Cork_rod){
        console.log("oooooooooo");
        Testtube_Cork_rod.angle=90;
        arrow.x=1300;
        arrow.y=750;
        arrow_y=750;
      }else if(currentobj==Flame_1){
        console.log("oooooooooo");
        Flame_1.alpha=0;
        Match_Stick.visible=true;
        Flame_1.animations.stop("anim");
        Flame_1.frameName="Flame0002.png";
        
        arrow.x=1280;
        arrow.y=650;
        arrow_y=650;
      }

        currentobj=null;
      }
    
  },
//For to next scene
 
      toObservations:function()
      {
      //voice.destroy();
      //voice2.destroy();
      procedure_voice1.destroy();
      procedure_voice2.destroy();
      procedure_voice3.destroy();
      procedure_voice4.destroy();
      procedure_voice5.destroy();
      procedure_voice6.destroy();
      procedure_voice7.destroy();
      procedure_voice8.destroy();
      procedure_voice9.destroy();
      procedure_voice10.destroy();
      procedure_voice11.destroy();
      procedure_voice12.destroy();
      procedure_voice13.destroy();
      procedure_voice14.destroy();
      procedure_voice15.destroy();
     
     
      /*procedure_voice10.destroy();
      procedure_voice11.destroy();*/
      //this.state.start("Experiment_A3", true, false, ip);
      this.state.start("Procedure_A3", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        //voice.destroy();
        //voice2.destroy();
        procedure_voice1.destroy();
        procedure_voice2.destroy();
        procedure_voice3.destroy();
        procedure_voice4.destroy();
        procedure_voice5.destroy();
        procedure_voice6.destroy();
        procedure_voice7.destroy();
        procedure_voice8.destroy();
        procedure_voice9.destroy();
        /*procedure_voice10.destroy();
        procedure_voice11.destroy();*/
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  //voice.destroy();
 //voice2.destroy();
 
  procedure_voice1.destroy();
      procedure_voice2.destroy();
      procedure_voice3.destroy();
      procedure_voice4.destroy();
      procedure_voice5.destroy();
      procedure_voice6.destroy();
      procedure_voice7.destroy();
      procedure_voice8.destroy();
      procedure_voice9.destroy();
      procedure_voice10.destroy();
      procedure_voice11.destroy();
      procedure_voice12.destroy();
      procedure_voice13.destroy();
      procedure_voice14.destroy();
      procedure_voice15.destroy();
  /*procedure_voice10.destroy();
  procedure_voice11.destroy();*/
    this.state.start("Experiment_A2",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
 // voice.destroy();
  //voice2.destroy();
  procedure_voice1.destroy();
  procedure_voice2.destroy();
  procedure_voice3.destroy();
  procedure_voice4.destroy();
  procedure_voice5.destroy();
  procedure_voice6.destroy();
  procedure_voice7.destroy();
  procedure_voice8.destroy();
  procedure_voice9.destroy();
  /*procedure_voice10.destroy();
  procedure_voice11.destroy();*/
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
