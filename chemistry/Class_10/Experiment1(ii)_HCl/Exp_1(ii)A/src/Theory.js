var theory = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var pageCount;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  theory.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("result1",1);
voice2=this.game.add.audio("result2",1);
voice3=this.game.add.audio("result3",1);

 //voice.play();
 voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  // muteButton.inputEnabled = true;
  // muteButton.input.useHandCursor = true;
 // muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  subfontStyle={ font: "32px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Theory /////////////////////////////////////
  pageCount=1;
  base= this.game.add.sprite(160,140,'dialogue_box')
  
  theoryHead_text=this.game.add.text(250,235,"Theory:",headfontStyle);

 
  
 next_btn = this.game.add.sprite(1610,880,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,880,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);
 prev_btn = this.game.add.sprite(310,880,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,880,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //prev_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrev, this);
  theory_text1=this.game.add.text(300,335,"",fontStyle);
  theory_text2=this.game.add.text(300,475,"",fontStyle);
  theory_text3=this.game.add.text(300,655,"",fontStyle);
  theory_text_formula1=this.game.add.text(400,700,"",headfontStyle);
  theory_text_formula2=this.game.add.text(400,800,"",headfontStyle);
  theory_text_sub1=this.game.add.text(1050,855,"",subfontStyle);
  /*result1_text=this.game.add.text(300,500,result2,fontStyle);*/

 this.addTheory();
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 //play = this.game.add.sprite(1600,870,'components','play_disabled.png');
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        addTheory:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
             theory_text1.text = "Acid such as hydrochloric acid turns blue litmus red but does not have \nany effect on red litmus solution.";
             theory_text2.text = "Hydrochloric acid reacts with zinc metal to liberate hydrogen gas. \nHydrogen gas forms an explosive mixture with oxygen (air) and when \nignited, burns rapidly producing a ‘pop’ sound.";
             theory_text_formula1.text = "Zn(s) + 2HCl(aq) -----> ZnCl\u2082(aq) + H\u2082(g)";
             theory_text_formula1.y=700;  
             theory_text2.x=300;
             theory_text2.y=475;
             theory_text_sub1.text = "";
             theory_text_formula2.text = "";
            break;
            case 2:
             theory_text1.text = "Hydrochloric acid reacts with solid sodium carbonate and produces a gas \nwith brisk effervescence. The gas produced is carbon dioxide (CO\u2082)";
             theory_text2.text = "Carbon dioxide is a colorless and odorless gas, when the gas is passed \nthrough lime water, milkiness appears due to the formation of \ninsoluble calcium carbonate.";
             theory_text_formula1.text = "Na\u2082CO\u2083(s) + 2HCl(aq)------->2NaCl(aq) + CO\u2082(g) + H\u2082O(l)";
             theory_text_formula1.y=500; 
             theory_text2.y=600;
             theory_text_formula2.text = "Ca(OH)\u2082(aq) + CO\u2082(g)--------> CaCO\u2083(s) + H\u2082O(l)";
             theory_text_sub1.text = "White ppt.";
             theory_text_sub1.x=1050;
             theory_text_sub1.y=855;
            break;
            case 3:
              theory_text1.text = "When carbon dioxide is passed through lime water in excess, the milkiness \ndisappears due to the formation of soluble calcium hydrogen carbonate. ";
              theory_text_formula1.text = "CaCO\u2083(s) + CO\u2082(g) + H\u2082O(l) ---------> Ca(HCO\u2083)\u2082 (aq)";
              theory_text_sub1.text = "Soluble in water";
              theory_text_sub1.x=1200;
              theory_text_sub1.y=555;
              theory_text2.text = "";
              theory_text_formula2.text = "";
            break;
          }
        },
        toNext:function(){
            if(pageCount<3){
              prev_btn.visible=true;
              pageCount++;
                this.addTheory();
                if(pageCount>=3){
                  play.visible=true;
                  next_btn.visible=false;  
                }
              }
        },
        toPrev:function(){
            if(pageCount>1){
              next_btn.visible=true;
              pageCount--;
                this.addTheory();
                if(pageCount<=1){
                  prev_btn.visible=false;  
                }
              }
        },
        nextSound:function(){
          //voice2.play()
          voice2.onStop.add(this.nextSound2,this);
        },
        nextSound2:function(){
          //voice3.play()
        },
    //For to next scene   
   
        toNextScene:function()
        {
        voice.destroy();
        voice2.destroy();
        voice3.destroy();
        this.state.start("Lab_Precautions", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    voice2.destroy();
    voice3.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  voice2.destroy();
  voice3.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  