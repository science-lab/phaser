var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  var sceneCount;
  var count;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {
  
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
 muted = false;
 voice=this.game.add.audio("A_observation1",1);
 //voice1=this.game.add.audio("A_observation2",1);
 //voice2=this.game.add.audio("A_observation3",1);
 voice.play();

 //this.game.time.events.add(Phaser.Timer.SECOND*16,this.nextSound, this);
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);
 sceneCount=1;
 count=0;
 //voice2.stop();
  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(110,80,'observation_table')
  //switch(exp_Name){
   // case "Iron with copper sulphate":
    sno_1="1";
    experiment_1="The reaction of dil. \nHCl with blue and \nred litmus solutions.";
    observation_1="HCl turns blue litmus \nred HCl has no effect \non red litmus solution. ";
    inference_1="Acids turn blue litmus red but \nhave no effect on red litmus.";
    sno_2="2";
    experiment_2="Reaction of dil. HCl \nwith metal";
    observation_2="Bubbles of gas are \nformed in the test tube. \nWhen a burning match-\nstick is brought near the \nnozzle of the delivery \ntube, the matchstick is \nextinguished with a pop \nsound.";
    inference_2="Acids react with zinc metal \nand liberate hydrogen gas.";
    //inference_3="Fe(s) + Cu\u00B2\u207A(aq)---->Fe\u00B2\u207A(aq) + Cu(s)";
    //inference_4="Grey          Blue                    Pale green    Reddish brown";/**/
    
    sno_text_1=this.game.add.text(175,320,sno_1,fontStyle);
    experiment_text_1=this.game.add.text(280,320,experiment_1,fontStyle);
    observation_text_1=this.game.add.text(680,320,observation_1,fontStyle);
    inference_text_1=this.game.add.text(1140,320,inference_1,fontStyle);
    
    sno_text_2=this.game.add.text(175,540,sno_2,fontStyle);
    experiment_text_2=this.game.add.text(280,540,experiment_2,fontStyle);
    observation_text_2=this.game.add.text(680,540,observation_2,fontStyle);
    inference_text_2=this.game.add.text(1140,540,inference_2,fontStyle);
    /*inference_text_3=this.game.add.text(950,700,inference_3,fontStyle);
    inference_text_4=this.game.add.text(950,750,inference_4,fontStyle_1);*/
    

   // break;
 // }
next_btn = this.game.add.sprite(1590,860,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1590,860,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);
 prev_btn = this.game.add.sprite(230,860,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(230,860,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 //prev_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrev, this);
 prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },

   
    toNext:function(){
         sceneCount=2;
         voice.destroy();
         voice=this.game.add.audio("A_observation3");
         voice.play();
          //voice.stop();
         // voice1.stop();
          //voice2.play();
          sno_text_1.text="3";
          experiment_text_1.text="Reaction of dil. HCl \nwith sodium \ncarbonate ";
          observation_text_1.text="A brisk effervescence \nis observed. A colorless \nand odourless gas \nevolves. The gas turns \nlime water milky.";
          inference_text_1.text="Acids on reaction with sodium \ncarbonate liberate CO\u2082, gas \nwith brisk effervescence.";
          
          sno_text_2.text="";
          experiment_text_2.text="";
          observation_text_2.text="";
          inference_text_2.text="";
          
          play.visible=true;
          next_btn.visible=false;
          prev_btn.visible=true;
        },
    toPrev:function(){
      count=0;
      sceneCount=1;
      voice.destroy();
      voice=this.game.add.audio("A_observation1");
      voice.play();
         // voice2.stop();
          //voice.play();
          //voice1.stop();
          //voice.onStop.add(this.nextSound,this);
          sno_text_1.text=sno_1;
          experiment_text_1.text=experiment_1;
          observation_text_1.text=observation_1;
          inference_text_1.text=inference_1;
          
          sno_text_2.text=sno_2;
          experiment_text_2.text=experiment_2;
          observation_text_2.text=observation_2;
          inference_text_2.text=inference_2;
          next_btn.visible=true;
          prev_btn.visible=false;
        },
    //For to next scene   
    update: function()
    {
      if(sceneCount==1){
        count++;
        console.log(count);
        if(count==1050){
          voice.destroy();
          voice=this.game.add.audio("A_observation2",1);
          voice.play();
        }
      }
    },
        toResult:function()
        {
        voice.destroy();
        //voice1.destroy();
       // voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
         muted = true;
        // if(sceneCount==1)
        // {
        //   voice.stop();
        //   voice2.stop();
        //   voice1.play();
        // }
        // else if(sceneCount==2)
        // {
        //   voice.stop();
        //   voice1.stop();
        //   voice2.play();
        // }
        //voice.destroy();
        //voice2.destroy();
        // voice.destroy();
        // voice1.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    //voice.destroy();
        //voice2.destroy();
        voice.destroy();
        voice1.destroy();
        voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
    voice.destroy();
    voice1.destroy();
    voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  