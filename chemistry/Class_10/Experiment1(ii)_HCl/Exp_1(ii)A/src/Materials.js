var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var exp_Name;
  var currentScene;
  var previousScene;
  
  }
  
  materials.prototype ={
  
  init: function(exp_nam) 
  {
    //ip = ipadrs;
    //exp_Name=exp_nam;

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  // muteButton.inputEnabled = true;
  // muteButton.input.useHandCursor = true;
  // muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1610,880,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,880,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(310,880,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,880,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScenes, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
        //if(exp_Name=="Iron with copper sulphate"){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                
              }
              mat_img_1 = this.game.add.sprite(370,550,'M_Testube');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Test tube:",headfontStyle);
              mat_content_1="Test tube is common piece of laboratory glassware used to \nhold, mix, or heat small quantities of solid or liquid chemicals.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
              
              break;
          case 2:
             mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Testtube_Stand');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.55,.7);
              mat_name_1=this.game.add.text(550,370,"Clamp stand:",headfontStyle);
              mat_content_1="A retort stand, also called a clamp stand, a ring stand, or a \nsupport stand, is a piece of scientific equipment intended \nto support other pieces of equipment and glassware.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
              
          break;
          case 3:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Testube');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Boiling tube:",headfontStyle);
              mat_content_1="A boiling tube is a wide-mouthed tube made up of borosilicate \nglass that can sustain high temperature.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 4:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Dropper');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Dropper:",headfontStyle);
              mat_content_1="A dropper, also known as a Pasteur pipette, is a device used\nto transfer small quantities of liquids. They are used in the\nlaboratory.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 5:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Cork');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Cork:",headfontStyle);
              mat_content_1="Cork is a useful material to stopper the test tubes and flasks. \nCork, also known as a stopper or bung, is partially \ninserted inside of a container to form a seal.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 6:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'A3_tube');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,320,"Delivery tubes and  delivery tube with a nozzle at \none end:",headfontStyle);
              mat_content_1="The delivery tube converts any liquid or gas flowing through \na Rubber tube into a free flow. A rubber tube can \nbe connected to or from the top of the tube. The delivery tube \nis particularly useful for bubbling a gas from a gas \ncylinder or stoppered vessel through a liquid.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 7:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Burner');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Bunsen burner:",headfontStyle);
              mat_content_1="A Bunsen burner, named after Robert Bunsen, is a \nkind of gas burner used as laboratory equipment; \nit produces a single open gas flame and is used for \nheating, sterilization, and combustion.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 8:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
           
              mat_img_1 = this.game.add.sprite(370,550,'M_HCL');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Hydrochloric acid:",headfontStyle);
              mat_content_1="Dilute hydrochloric acid is often used in the \nextraction of basic substances from mixtures or \nin the removal of basic impurities. The dilute \nacid converts the base such as ammonia or an \norganic amine into water-soluble chloride salt.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 9:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(940,800,'M_Litmus_Solution_Red');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Litmus solution:",headfontStyle);
              mat_content_1="Red and blue litmus solution Indicators are used to determine\nwhether substances are acids, bases.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
             
          break;
          case 10:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
            
              mat_img_1 = this.game.add.sprite(370,550,'M_Zink_Metal');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Zinc metal granules:",headfontStyle);
              mat_content_1="Zinc granules are the solid crystals of zinc. ";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 11:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Sodium_Carbonate');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Sodium Carbonate:",headfontStyle);
              mat_content_1="Sodium carbonate is a chemical compound with the\nmolecular formula Na\u2082CO\u2083. It's commonly referred\nto as washing soda and is used in cleaning products";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;
          case 12:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'M_Lime_Water');
              mat_img_1.anchor.setTo(.5,.5);
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(550,370,"Limewater:",headfontStyle);
              mat_content_1="Limewater is the common name for a dilute aqueous\nsolution of calcium hydroxide. Calcium hydroxide, Ca(OH)\u2082";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
          break;

          
          
          }
          

        //}
      },
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==12){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
        toNextScenes:function()
        {
        voice.destroy();
        this.state.start("Theory", true, false, ip);
        
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  