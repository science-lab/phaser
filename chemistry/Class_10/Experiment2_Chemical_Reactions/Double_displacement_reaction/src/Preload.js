var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        //this.game.load.atlasJSONHash('ferrous_sulphate', 'assets/ferrous_sulphate.png', 'assets/ferrous_sulphate.json');
        //this.game.load.atlasJSONHash('burner_sprites', 'assets/burner_sprites.png', 'assets/burner_sprites.json');
      
        this.game.load.json('questions','data/questions1.json');
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
        this.game.load.image("bg","assets/Double_displacement/Bg3.jpg");
        this.game.load.image("bg1","assets/bg1.png");
        //this .game.load.image("bg2","assets/Decomposition_reaction/Bg3.jpg");
        
        this.game.load.image('transBackground','assets/transBackground.png');
        this.game.load.image("arrow","assets/arrow.png");
        this.game.load.image("collider","assets/col.png");
        this.game.load.image("Button_Bg","assets/Black_panel.png");
           
        this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');

        this.game.load.image('observation_table_decomposition','assets/observation1.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        
        this.game.load.audio('title_a','assets/audio/ttl.mp3');
        this.game.load.audio('next_snd','assets/audio/next_snd.mp3');
        this.game.load.audio('sim_1','assets/audio/inst1.mp3');
        this.game.load.audio('sim_2','assets/audio/inst2.mp3');

///////////////////////////Audio for decomposition reaction/////////////////////////
        
        this.game.load.audio('Aim_1','assets/audio/Aim_1.mp3');
        this.game.load.audio('Aim_2','assets/audio/Aim_2.mp3');
        this.game.load.audio('audio_procedure1','assets/audio/Double_displacement/A_procedure1.mp3');
        this.game.load.audio('audio_procedure2','assets/audio/Double_displacement/A_procedure2.mp3');
        this.game.load.audio('audio_procedure3','assets/audio/Double_displacement/A_procedure3.mp3');
        this.game.load.audio('audio_procedure4','assets/audio/Double_displacement/A_procedure4.mp3');
        this.game.load.audio('audio_procedure5','assets/audio/Double_displacement/A_procedure5.mp3');
        this.game.load.audio('audio_procedure6','assets/audio/Double_displacement/A_procedure6.mp3');
        this.game.load.audio('audio_procedure7','assets/audio/Double_displacement/A_procedure7.mp3');
        this.game.load.audio('audio_procedure8','assets/audio/Double_displacement/A_procedure8.mp3');
        this.game.load.audio('precautions_1','assets/audio/Double_displacement/A_precaution1.mp3');
        this.game.load.audio('procedure_voice1','assets/audio/Double_displacement/A_procedure_voice1.mp3');
        this.game.load.audio('procedure_voice2','assets/audio/Double_displacement/A_procedure_voice2.mp3');
        this.game.load.audio('observation1','assets/audio/Double_displacement/A_observation1.mp3');
        //this.game.load.audio('observation2','assets/audio/Double_displacement/A_observation2.mp3');
        this.game.load.audio('result1','assets/audio/Double_displacement/Result_1.mp3');
      
/////////////////////////////Double displacement reaction materials///////////////////////////////////////
        this.game.load.image('wood_rack','assets/Double_displacement/Materials/Wooden_stand_full.png');
        this.game.load.image('testtube','assets/Double_displacement/Materials/Testube.png');
        this.game.load.image('testtube_solution','assets/Double_displacement/Materials/m_solution.png');
     
        this.game.load.image('spoon','assets/Double_displacement/Materials/Spoon.png');
        this.game.load.image('stand','assets/Double_displacement/Materials/Testube_Stand_full.png');
       
        this.game.load.image('m_testtube_holder','assets/Double_displacement/Materials/Testube_Holder.png');
        this.game.load.image('HCl','assets/Double_displacement/Materials/M_HCL.png');
////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////graphics contents////////////////////////////////////////////////
this.game.load.image('clamp_stand','assets/Double_displacement/Clamp_stand.png');
this.game.load.image('clamp_stand_extender','assets/Double_displacement/Clamp_Stand_extender.png');
this.game.load.image('clamp_stand_holder','assets/Double_displacement/Clamp_stand_holder.png');
this.game.load.image('clock','assets/Double_displacement/Clock.png');
this.game.load.image('clock_long_needle','assets/Double_displacement/Clock_long_needle.png');
this.game.load.image('clock_short_needle','assets/Double_displacement/Clock_short_needle.png');
this.game.load.image('white_ppt_holder','assets/Double_displacement/NaSO4_BaCl2_with_holder.png');
this.game.load.image('ppt_holder','assets/Double_displacement/ppt_with_holder.png');
this.game.load.image('NaSO4_holder','assets/Double_displacement/NaSO4_with_holder.png');
this.game.load.image('BaSO4_holder','assets/Double_displacement/baso4_with_holder.png')
this.game.load.image('testtube_back','assets/Double_displacement/Testtube_back.png');
this.game.load.image('testtube_holder','assets/Double_displacement/Testtube_holder.png');
this.game.load.image('testtube_stand_back','assets/Double_displacement/Testtube_stand_back.png');
this.game.load.image('testtube_stand_front','assets/Double_displacement/Testtube_stand_front.png');
this.game.load.image('hcl_bottle','assets/Double_displacement/HCL.png');
this.game.load.image('bottle_back','assets/Double_displacement/Jar_Back.png');
this.game.load.image('cork','assets/Double_displacement/cap.png');


this.game.load.atlasJSONHash('bacl_pour_sprite', 'assets/Double_displacement/Animations/bacl_pour_sprite.png', 'assets/Double_displacement/Animations/bacl_pour_sprite.json');
this.game.load.atlasJSONHash('bacl_to_na2so4_sprite', 'assets/Double_displacement/Animations/bacl_to_na2so4_sprite.png', 'assets/Double_displacement/Animations/bacl_to_na2so4_sprite.json');
this.game.load.atlasJSONHash('hcl_to_ppt_sprite', 'assets/Double_displacement/Animations/hcl_to_ppt_sprite.png', 'assets/Double_displacement/Animations/hcl_to_ppt_sprite.json');
this.game.load.atlasJSONHash('ppt_settle_sprite', 'assets/Double_displacement/Animations/ppt_settle_sprite.png', 'assets/Double_displacement/Animations/ppt_settle_sprite.json');
this.game.load.atlasJSONHash('solution_pour_sprite', 'assets/Double_displacement/Animations/solution_pour_sprite.png', 'assets/Double_displacement/Animations/solution_pour_sprite.json');
this.game.load.atlasJSONHash('baso4_hcl_shake_sprite', 'assets/Double_displacement/Animations/baso4_shake_sprite.png', 'assets/Double_displacement/Animations/baso4_shake_sprite.json');
this.game.load.atlasJSONHash('solution_to_empty_testtube_sprite', 'assets/Double_displacement/Animations/solution_to_empty_testtube_sprite.png', 'assets/Double_displacement/Animations/solution_to_empty_testtube_sprite.json');
this.game.load.atlasJSONHash('hcl_drooper_sprite', 'assets/Double_displacement/Animations/HCL_dropper_out_sprite.png', 'assets/Double_displacement/Animations/HCL_dropper_out_sprite.json');

/////////////////////////////////////new Audios///////////////////////////////////////////
this.game.load.audio('start_audio','assets/audio/Double_displacement/start_audio.mp3');
this.game.load.audio('A_step1','assets/audio/Double_displacement/A_step1.mp3');
this.game.load.audio('A_step2','assets/audio/Double_displacement/A_step2.mp3');
this.game.load.audio('A_step3','assets/audio/Double_displacement/A_step3.mp3');
this.game.load.audio('A_step4','assets/audio/Double_displacement/A_step4.mp3');
this.game.load.audio('A_step5','assets/audio/Double_displacement/A_step5.mp3');
this.game.load.audio('A_step6','assets/audio/Double_displacement/A_step6.mp3');
this.game.load.audio('A_step7','assets/audio/Double_displacement/A_step7.mp3');
this.game.load.audio('A_step8','assets/audio/Double_displacement/A_step8.mp3');
this.game.load.audio('A_step9','assets/audio/Double_displacement/A_step9.mp3');
this.game.load.audio('A_step10','assets/audio/Double_displacement/A_step10.mp3');
this.game.load.audio('A_step11','assets/audio/Double_displacement/A_step11.mp3');
this.game.load.audio('A_step12','assets/audio/Double_displacement/A_step12.mp3');
this.game.load.audio('A_step13','assets/audio/Double_displacement/A_step13.mp3');
this.game.load.audio('A_step14','assets/audio/Double_displacement/A_step14.mp3');
this.game.load.audio('A_step15','assets/audio/Double_displacement/A_step15.mp3');
this.game.load.audio('A_step16','assets/audio/Double_displacement/A_step16.mp3');
this.game.load.audio('A_step17','assets/audio/Double_displacement/A_step17.mp3');
this.game.load.audio('A_step18','assets/audio/Double_displacement/A_step18.mp3');
/////////////////////////////////////////////////////////////////////////////////////////////////////

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
     // this.game.state.start("Experiment_1");//Starting the gametitle state
      this.game.state.start("Materials");//Simulation_hot1
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
   //   this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
     // this.game.state.start("Procedure");
      //this.game.state.start("Exp_Selection");

	}
}

