var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
//loadScene//startExperiment
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var temperatureFlag;
var dropStep;
}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("procedure_0",1);
 
 // voice=this.game.add.audio("fobj",1);
 temperatureFlag=false;
 incr=0;
 delay=15;
 dropStep=1;
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  this.addItems();

  /*base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_step_1="1. Take 2 grams of ferrous sulphate crystals in a boiling tube. ";
  procedure_step_2="2. Heat the boiling tube on the bunsen burner. ";
  procedure_step_3="3. Observe the colour of the residue.";
  procedure_text=this.game.add.text(250,250,"Procedure:",headfontStyle);
  procedure_step_text_1=this.game.add.text(300,350,procedure_step_1,fontStyle);
  procedure_step_text_2=this.game.add.text(300,430,procedure_step_2,fontStyle);
  procedure_step_text_3=this.game.add.text(300,510,procedure_step_3,fontStyle);*/



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toObservations, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      //// For Adding lab items
      addItems:function()
      {
      collider=this.game.add.sprite(680,500, 'collider');
      this.game.physics.arcade.enable(collider);
      collider.scale.setTo(3,3);
      collider.inputEnabled = true;
      collider.enableBody =true;
      collider.alpha=0;
      dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      
      dialog_text="";
      dialog=this.game.add.text(70,40,dialog_text,fontStyle);

      Beaker_back1=this.game.add.sprite(600,500, 'Beaker_back');
      
      Beaker_back=this.game.add.sprite(600,500, 'Beaker_back');  
      Beaker_back.visible=false;

          
       Stand=this.game.add.sprite(400,370,'Stand');
       Stand.scale.set(.8);
       Stand_hand=this.game.add.sprite(645,556,'Stand_hand');
       Stand_hand.scale.setTo(1.2,.8);
       Stand_holder=this.game.add.sprite(720,556,'Stand_holder');
       Stand_holder.scale.setTo(.5,.8);
       

      thermometer_Scale=this.game.add.sprite(757,875, 'thermometer_Scale');
      thermometer_Scale.anchor.setTo(.5,1);
      thermometer_Scale.visible=false;
      thermometer_Scale.scale.y=.5;
      thermometer_Scale.scale.x=1.5;
      //thermometer_Scale.angle=-18
      thermometer=this.game.add.sprite(1250,860, 'thermometer');
      thermometer.angle=90;
      thermometer.xp=1250;
      thermometer.yp=860;
      thermometer.anchor.setTo(.5,.5);
      thermometer.scale.setTo(1,.8);
      //thermometer.scale.setTo(.7,.7);
      //thermometer1=this.game.add.sprite(500,400, 'thermometer1');
      // 
      standGroup=this.game.add.group(); 
      standGroup.add(Stand);
      standGroup.add(Stand_hand);
      standGroup.add(Stand_holder);
      standGroup.x=-3000;

      Smoke=this.game.add.sprite(600,480, 'Smoke','Smoke0001.png'); 
      Smoke.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,12,19,20],16, true, true);  
      Smoke.visible=false; 
      
      Beaker1=this.game.add.sprite(600,500, 'Beaker');
      
      Beaker=this.game.add.sprite(600,500, 'Beaker');
      Beaker.visible=false;
      Powder_to_Beaker=this.game.add.sprite(600,500, 'Powder_to_Beaker','Powder_to_Beaker0001.png');
      Powder_to_Beaker.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],10, false, true);  
      Powder_to_Beaker.visible=false;
      First_Drop=this.game.add.sprite(600,500, 'First_Drop','First_Drop_to_Beaker0001.png');
      First_Drop.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14],6, false, true);  
      First_Drop.visible=false;  
      Second_Drop=this.game.add.sprite(600,500, 'Second_Drop','Second_Drop_to_Beaker0001.png');
      Second_Drop.animations.add('anim',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],6, false, true);  
      Second_Drop.visible=false; 

      
      
      Powder=this.game.add.sprite(1100,880, 'Powder');
      Spoon=this.game.add.sprite(1130,770, 'Spoon_animation','Spoon0001.png');
      //Spoon.scale.setTo(.8,.8);
      Spoon.angle=15;
      Spoon.xp=1130;
      Spoon.yp=770;
      /*Spoon.events.onDragStart.add(function() {this.gameC(Spoon)}, this);
      Spoon.events.onDragStop.add(function() {this.gameC1(Spoon)}, this);
      Spoon.inputEnabled = true;
      Spoon.input.useHandCursor = true;
      Spoon.events.onInputDown.add(this.ClickOnSpoon, this);
      this.game.physics.arcade.enable(Spoon);
      Spoon.input.enableDrag(true);  */
      Spoon.animations.add('anim',[1,2,3,4,5,6,7,8],10, false, true);  

      Beaker_back_water=this.game.add.sprite(1400,620, 'Beaker_back');  
      Beaker_back_water.scale.x=.7;
      Water_to_Beaker=this.game.add.sprite(1450,600, 'Water_to_Beaker','Water_to_beaker0001.png');
      Water_to_Beaker.animations.add('anim',[1,2,3,4,5,6],6, false, true);  
      Water_to_Beaker.scale.y=1.4;
      Water_to_Beaker.xp=1450;
      Water_to_Beaker.yp=600;
      
      beaergroup = this.game.add.group();
      beaergroup.add(Beaker_back1); 
      beaergroup.add(Beaker1); 
      beaergroup.x=-3000;  
      /**/

      Beaker_with_Water=this.game.add.sprite(1400,650, 'Beaker_with_Water1'); //cheanged to 'Beaker_with_Water'
      Beaker_with_Water.scale.x=.7;
      Beaker_with_Water.scale.y=.7;
      arrow=this.game.add.sprite(1220,780, 'arrow');
      arrow.angle=90;
      arrow_y=830;
      arrow.visible=false;
      dialog.text="Lets perform the Experiment.";
      dialog.y=60;
      this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);  
      //loadScene
      //mixture_label.scale.setTo(1.5,1.5);
    },
    loadScene:function(){
      console.log("lllllllllll");
      voice.play();
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
    },
    startExperiment:function(){
      voice.destroy(true);
      voice=this.game.add.audio("procedure_1",1);
      voice.play();
      dialog.text="Take a hard glass beaker and add about 5 g of quick lime to it.";
      tween1=this.game.add.tween(beaergroup).to( {x:0, }, 800, Phaser.Easing.Out, true);
      tween1.onComplete.add(function(){this.tweenComplete1()}, this);
    },
  tweenComplete1:function(){
    beaergroup.visible=false;
    Beaker_back.visible=true;
    Beaker.visible=true;
    arrow.visible=true;
    Spoon.events.onDragStart.add(function() {this.gameC(Spoon)}, this);
      Spoon.events.onDragStop.add(function() {this.gameC1(Spoon)}, this);
      Spoon.inputEnabled = true;
      Spoon.input.useHandCursor = true;
      this.game.physics.arcade.enable(Spoon);
      Spoon.input.enableDrag(true);
    //dialog.text="Add to beaker about 5 g of quick lime (calcium oxide).";
    //procedure_voice1.play();
    //procedure_voice1.onStop.add(this.enableProcedure2,this);
    
  },
  
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable = false;
    currentobj=obj;
    //arrow.visible=false;
    console.log("drag");
    if(obj==Spoon){
      arrow.x=770;
      arrow.y=460;
      arrow_y=460;
    }else if(obj==Water_to_Beaker){
      arrow.x=770;
      arrow.y=460;
      arrow_y=460;
      arrow.visible=true;
    }else if(obj==thermometer){
      thermometer.angle=0;
      arrow.x=770;
      arrow.y=460;
      arrow_y=460;
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
  },
  match_Obj:function(){
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==Spoon){
      Spoon.x=750;
      Spoon.y=400;
      Powder_to_Beaker.visible=true;
      Beaker.visible=false;
      Spoon.animations.play('anim');
      Spoon.animations.currentAnim.onComplete.add(this.SpoonAnimStoped, this);
      Powder_to_Beaker.animations.play('anim');
      
       arrow.visible=false;
    }else if(currentobj==Water_to_Beaker){
      if(dropStep==1){
        dialog.y=40;
        voice.destroy(true);
        voice=this.game.add.audio("procedure_4",1);
        voice.play();
        dialog.text="Observe the temperature rise in the thermometer and hissing \nsound from the beaker.";
        this.game.time.events.add(Phaser.Timer.SECOND*8,this.stopObserving, this);
        temperatureFlag=true;
        arrow.visible=false;
        Powder_to_Beaker.visible=false;
        First_Drop.visible=true; 
        Water_to_Beaker.x=705;
        Water_to_Beaker.y=260;
        Water_to_Beaker.animations.play('anim');
        Water_to_Beaker.animations.currentAnim.onComplete.add(this.Water_to_BeakerAnimStoped, this);
        First_Drop.animations.play('anim');
        First_Drop.animations.currentAnim.onComplete.add(this.First_DropAnimStoped, this);
      }else if(dropStep==2){
        temperatureFlag=true;
        delay=20;
        arrow.visible=false;
        First_Drop.visible=false; 
        Second_Drop.visible=true; 
        Water_to_Beaker.x=705;
        Water_to_Beaker.y=260;
        Water_to_Beaker.animations.play('anim');
        Water_to_Beaker.animations.currentAnim.onComplete.add(this.Water_to_BeakerAnimStoped, this);
        Second_Drop.animations.play('anim');
        Second_Drop.animations.currentAnim.onComplete.add(this.First_DropAnimStoped, this);
      }
      
    }else if(currentobj==thermometer){
      arrow.visible=false;
      thermometer.x=745;
      thermometer.y=930;//930
      thermometer.anchor.setTo(0,1);
      thermometer_Scale.visible=true;
      //thermometer.angle=-18;
      Water_to_Beaker.events.onDragStart.add(function() {this.gameC(Water_to_Beaker)}, this);
      Water_to_Beaker.events.onDragStop.add(function() {this.gameC1(Water_to_Beaker)}, this);
      Water_to_Beaker.inputEnabled = true;
      Water_to_Beaker.input.useHandCursor = true;
      this.game.physics.arcade.enable(Water_to_Beaker);
      Water_to_Beaker.input.enableDrag(true);  
      voice.destroy(true);
      voice=this.game.add.audio("procedure_3",1);
      voice.play();
      dialog.text="Add 2 ml of water into the beaker by using the dropper.";
      arrow.x=Water_to_Beaker.x+60;
      arrow.y=Water_to_Beaker.y-30;
      arrow_y=arrow.y;
      arrow.visible=true;
    }
    
  },
  stopObserving:function(){
    temperatureFlag=false;
    console.log("observeStop");
    if(dropStep==1){
      Water_to_Beaker.inputEnabled=true;
      Water_to_Beaker.input.enableDrag(true);
      Water_to_Beaker.body.enable=true;
      Water_to_Beaker.input.useHandCursor = true;
      dropStep=2;
      voice.destroy(true);
      voice=this.game.add.audio("procedure_5",1);
      voice.play();
      dialog.y=60;
      dialog.text="Come on, lets add few more drops of water one more time.";
      this.game.time.events.add(Phaser.Timer.SECOND*10,this.stopObserving, this);
    }else if(dropStep==2){
      dialog.y=35;
      dialog_box.scale.setTo(.8,.9);
      voice.destroy(true);
      voice=this.game.add.audio("procedure_6",1);
      voice.play();
      //dialog.y=60;
      dialog.text="Observe that quick lime turns into a white semi-solid after hydration.\nSince the reaction liberates heat, it is also an example of exothermic\nreaction.";
      this.game.time.events.add(Phaser.Timer.SECOND*12,this.stopObserving2, this);
    }
  },
  stopObserving2:function(){
     voice.destroy(true);
     dialog.y=60;
     dialog_box.scale.setTo(.8,.7);
     voice.destroy(true);
      voice=this.game.add.audio("procedure_7",1);
      voice.play();
    dialog.text="Click on the next button to see the observation.";
    play.visible=true;
  },
  stopSmock:function(){
    voice1.destroy(true);
     Smoke.animations.stop('anim');
     Smoke.visible=false; 

    
  },
  Water_to_BeakerAnimStoped:function(){
    this.game.time.events.add(Phaser.Timer.SECOND*.4,this.smokeShow, this);
    //Smoke.scale.setTo(.8,.8);
    // Smoke.visible=true; 
    // Smoke.animations.play('anim');
    // this.game.time.events.add(Phaser.Timer.SECOND*3,this.stopSmock, this);
  },
  smokeShow:function(){
    //this.game.time.events.add(Phaser.Timer.SECOND*1,this.smokeShow, this);
    //Smoke.scale.setTo(.8,.8);
    voice1=this.game.add.audio("Hissing",1);
    voice1.play();
    Smoke.visible=true; 
    Smoke.animations.play('anim');
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.stopSmock, this);
  },
  First_DropAnimStoped:function(){
    console.log("water");
    Water_to_Beaker.reset(Water_to_Beaker.xp,Water_to_Beaker.yp);
  },
  SpoonAnimStoped:function(){
    var t= this.game.add.tween(standGroup).to({x:0}, 100, Phaser.Easing.In, true);
    t.onComplete.add(function () {
      voice.destroy(true);
    voice=this.game.add.audio("procedure_2",1);
    voice.play();
        
    dialog.text="Drag and place the thermometer.";
    dialog.y=60;
    Spoon.reset(Spoon.xp+30,Spoon.yp);
    Spoon.angle=40;
    thermometer.events.onDragStart.add(function() {this.gameC(thermometer)}, this);
    thermometer.events.onDragStop.add(function() {this.gameC1(thermometer)}, this);
    thermometer.inputEnabled = true;
    thermometer.input.useHandCursor = true;
    this.game.physics.arcade.enable(thermometer);
    thermometer.input.enableDrag(true);
      arrow.x=1260;
      arrow.y=720;
      arrow_y=arrow.y;
      arrow.visible=true;
    }.bind(this));

    
    /**/

  },
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }/**/
    if(temperatureFlag){
      incr++;
      if(incr>=delay){
        //delay+=20;
        incr=0;
        thermometer_Scale.scale.y+=.1;
        
      }
    }
       
  },
 
  detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      console.log(currentobj.xp+"//"+currentobj.yp);
      if(currentobj==Spoon){
        //currentobj.frameName="spoon.png";
        //currentobj.scale.setTo(1,1);
        arrow.x=1220;
        arrow.y=830;
        arrow_y=830;
      }else if(currentobj==Water_to_Beaker){
        arrow.x=Water_to_Beaker.x+60;
        arrow.y=Water_to_Beaker.y-30;
        arrow_y=Water_to_Beaker.y-30;
        arrow.visible=true;
      }else if(currentobj==thermometer){
        
        arrow.x=1260;
      arrow.y=720;
      arrow_y=arrow.y;
        thermometer.angle=90;
      }

      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
//For to next scene
 
      toObservations:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        window.location.href="../index.php";
        //this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  window.open(loc,"_self");
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
//window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
