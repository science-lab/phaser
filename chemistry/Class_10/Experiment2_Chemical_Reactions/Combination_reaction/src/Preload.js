var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        // this.game.load.image('testtube_empty','assets/Decomposition_reaction/Testube.png');
        // this.game.load.image('testtube_holder','assets/Decomposition_reaction/Testube_Holder.png');
        // this.game.load.image('testtube_stand','assets/Decomposition_reaction/Testube_Stand.png');
        //this.game.load.image('testtube_stand_holder','assets/Decomposition_reaction/Testtube_Holder.png');
        //this.game.load.image('testtube','assets/Decomposition_reaction/Testube.png');
        //this.game.load.image('testtube_holder_testtube','assets/Decomposition_reaction/Testube_holder_testtube.png');


       
        this.game.load.image('observation_1','assets/observation_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        
       
//////Audio for decomposition reaction/////////////////////////
         this.game.load.audio('Aim_1','assets/audio/Aim_1.mp3');
         this.game.load.audio('Aim_2','assets/audio/Aim_2.mp3');
        

        //this.game.load.image('glass_rod','assets/D_Displacement/glass_rod.png');
        //this.game.load.image('Conical_Flask','assets/D_Displacement/Conical_Flask.jpg');
        
//////////////////////////////////Combination_Reaction//////////////////////////////////////
        this.game.load.image('borosil_beaker','assets/Combination/borosil_beaker.png');
        this.game.load.image('distilled_water','assets/Combination/distilled_water.png');
        
        this.game.load.image('lime_quicklime','assets/Combination/lime_quicklime.jpg');
        this.game.load.image('ph_paper','assets/Combination/ph_paper.jpg');
        this.game.load.image('droper','assets/Combination/droper.png');
        this.game.load.image('Spoon_M','assets/Spoon_M.png');
        ///////////////////new/////////////////
        this.game.load.image('M_Dropper','assets/Combination/M_Dropper.png');
        this.game.load.atlasJSONHash('Powder_to_Beaker', 'assets/Combination/Powder_to_Beaker.png','assets/Combination/Powder_to_Beaker.json');        
        this.game.load.atlasJSONHash('First_Drop', 'assets/Combination/First_Drop.png','assets/Combination/First_Drop.json');
        this.game.load.atlasJSONHash('Second_Drop', 'assets/Combination/Second_Drop.png','assets/Combination/Second_Drop.json');
        this.game.load.atlasJSONHash('Smoke', 'assets/Combination/Smoke.png','assets/Combination/Smoke.json');
        this.game.load.atlasJSONHash('Spoon_animation', 'assets/Combination/Spoon.png','assets/Combination/Spoon.json');
        this.game.load.atlasJSONHash('Water_to_Beaker', 'assets/Combination/Water_to_Beaker.png','assets/Combination/Water_to_Beaker.json');
        this.game.load.image('Beaker','assets/Combination/Beaker.png');
        this.game.load.image('Beaker_back','assets/Combination/Beaker_back.png');
        this.game.load.image('Beaker_with_Water','assets/Combination/Beaker_with_Water.png');
        this.game.load.image('Beaker_with_Water1','assets/Combination/Beaker_with_Water1.png');
        this.game.load.image('Powder','assets/Combination/Powder.png');
        this.game.load.image('Spoon','assets/Combination/Spoon_1.png');
        //this.game.load.image('thermometer','assets/Combination/thermo_meter.png');
        this.game.load.image('thermometer','assets/Combination/Thermometer.png');
        this.game.load.image('thermometer_Scale','assets/Combination/thermometer_Scale.png');

        this.game.load.image('Stand','assets/Combination/Stand.png');
        this.game.load.image('Stand_holder','assets/Combination/Testune_Stand_portion.png');
        this.game.load.image('Stand_hand','assets/Combination/Testune_Stand_portion_1.png');
               
        this.game.load.audio('precaution_1','assets/audio/precaution_1.mp3');    
        this.game.load.audio('procedure_C','assets/audio/procedure_C.mp3'); 

        this.game.load.audio('procedure_0','assets/audio/procedure_0.mp3'); 
        this.game.load.audio('procedure_1','assets/audio/procedure_1.mp3');
        this.game.load.audio('procedure_2','assets/audio/procedure_2.mp3');
        this.game.load.audio('procedure_3','assets/audio/procedure_3.mp3');
        this.game.load.audio('procedure_4','assets/audio/procedure_4.mp3');
        this.game.load.audio('procedure_5','assets/audio/procedure_5.mp3');
        this.game.load.audio('procedure_6','assets/audio/procedure_6.mp3');
        this.game.load.audio('procedure_7','assets/audio/procedure_7.mp3');
        this.game.load.audio('Hissing','assets/audio/Hissing.mp3');

        this.game.load.audio('observation_C','assets/audio/observation_C.mp3');
        this.game.load.audio('result_C','assets/audio/result_C.mp3');

	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Quiz");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      this.game.state.start("Materials");//Simulation_hot1//Aim
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");

	}
}

