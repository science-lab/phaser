var viva = function(game){
//Images
var background;
var popup;

var textspace;
var rightbutton_bg;

//text
var heading;
var headingtext;
var style;
var style2;
var correct;
var wrong;
var correctcnfrm;
var wrongcnfrm;
 var wrongcnfrm1;
//var option;
var selectedOption;
var displayResult;
//button
var submit;
var next;

//common buttons
var volumeButton;
var muteButton;
var restartButton;
var homeButton;
var closeButton;
var hotButton;
var graphButton;
var LCButton;

//counter
var score;

//ip address
var ip;
this.optionArray;
this.optionimgArray;
this.optedimgArray;
}

viva.prototype ={

 init: function( ipadrs) {

     ip = ipadrs;
},

 create:function(){

  if(this.game.sound.mute){

            this.game.sound.mute = false;
        }

 //option selected
 //option = 0;
 displayResult=false;
 score=0;
 selectedOption=-1;
 this.optionArray=[];
  this.optionimgArray=[];
  this.optedimgArray=[];
  this.wrongcnfrm_1=null;
  this.wrongcnfrm_2=null;
 background = this.game.add.sprite(0, 0,'bg');
 popup = this.game.add.sprite(160,140,'dialogue_box');
 //popup.scale.setTo(.5,.51);

 textspace_T = this.game.add.sprite(280,790,'rightAns_img');
 textspace_F = this.game.add.sprite(280,790,'wrongAns_img');
 textspace_T.scale.setTo(0.9,1);
 textspace_F.scale.setTo(0.9,1);
 textspace_T.visible=false;
 textspace_F.visible=false;
 submit =   this.game.add.sprite(1580,850,'components','enter_pressed.png');
 submit.scale.setTo(1.6,1.6);
 submit.inputEnabled = true;
 submit.input.useHandCursor = true;
 submit.visible=false;
 submit.events.onInputDown.add(this.validation, this);
 next =   this.game.add.sprite(1580,850,'components','next_pressed.png');
 next.scale.setTo(.7,.7);
 next.inputEnabled = true;
 next.input.useHandCursor = true;
 next.events.onInputDown.add(this.showNextQuestion, this);
 next.visible=false;


 /*p_quizt=this.game.add.sprite(1650,750,'p_charactor','q_true0.png');
   p_quizt.scale.setTo(2,2);
   p_quizt.visible=false;
 p_quizw=this.game.add.sprite(1650,750,'p_charactor','q_wrong0.png');
   p_quizw.scale.setTo(2,2);
    p_quizw.visible=false;*/

 //texts
 headingtext = "Assess yourself:";
 style1 = { font: "50px Segoe UI ", fill: "#ffffff", align: "left" };
 style2 = { font: "40px Segoe UI ", fill: "#ffffff", align: "center" };
 style3 = { font: "42px Sego UI ", fill: "#ff0000", align: "left" };//red :Font changed for better look n feel
 style4 = { font: "42px Sego UI ", fill: "#00ff00", align: "left" };// green
 style5 = { font: "46px Segoe UI ", fill: "#ffffff", align: "center" };
 //style5 = { font: "bold 16px  Segoe UI bold", fill: "#000000", align: "left" };
 heading = this.game.add.text(230,210,headingtext,style1);

 correct = this.game.add.text(817,792,"CORRECT",style4);
 wrong = this.game.add.text(825,792,"WRONG",style3);
 correct.visible=false;
 wrong.visible=false;
 correctcnfrm = this.game.add.text(700,860,"You have chosen correct answer!!",style2);
 correctcnfrm.visible=false;
// wrongcnfrm = this.game.add.text(390,440,"Oops!!!You have chosen wrong answer!!\n\n Correct answer is Ignition Temperature.",style2);
  ////////////////////////////////////////////////////////////////////////////


    var jsonData = this.game.cache.getJSON('questions');
    this.takeAnswer=false;
    this.questions = jsonData.questions;
    this.totalQuestionsToShow = jsonData.totalQuestionsToShow;
    this.currentQuestionIndex = 0;
    if(jsonData.randomize == true){
      this.shuffleArray(this.questions);
    }
    this.questions = this.questions.slice(0,this.totalQuestionsToShow);
    this.message=false;

    var style = {
      font: "40px Segoe UI",
      fill: "#ffffff",
      boundsAlignH: "left", //center
      boundsAlignV: "top",
      wordWrap:true,
      wordWrapWidth:this.game.world.width - 10
    };
    this.questionText = this.game.add.text(0,0,"",style);
    this.questionText.setTextBounds(280, 315, this.game.world.width, 250);
    this.showNextQuestion();
////////////////////////////////////////////////////////////////////////
buttonGroup=this.game.add.group();
  //buttonGroup.scale.y=-1;
  buttonGroup.state="seek";

  buttonGroup.visible=false;

  //black base
  rightbutton_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  rightbutton_bg.scale.setTo(.5,.5);
  rightbutton_bg.angle=-90;





  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);




 //1.Mute Button
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  //muteButton.input.useHandCursor = true;
  //muteButton.events.onInputDown.add(this.muteTheGame, this);



  //8.Volume Buttons
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  //volumeButton.input.useHandCursor = true;
  //volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;
    buttonGroup.add(rightbutton_bg);
    buttonGroup.add(muteButton);
    //buttonGroup.add(reset);
    buttonGroup.add(homeButton);
    buttonGroup.add(quitButton);
   // buttonGroup.add(tweenButton);
    //buttonGroup.add(hotButton);
    //buttonGroup.add(graphButton);
    //buttonGroup.add(LCButton);
    buttonGroup.add(volumeButton);

  //LEARNING ENABLE
 //Learning content


//if (!this.game.device.desktop)
       // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;

 /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
 normalScreen.input.useHandCursor = true;
 normalScreen.events.onInputDown.add(this.gonormal,this);*/

 fullScreen1 = this.game.add.sprite(1800,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
 fullScreen1.input.useHandCursor = true;
 fullScreen1.events.onInputUp.add(this.gofull,this); 

//}
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
 ////////////////////////////////////////////////////////////////////////

 check_ans : function(e){
  /*console.log(i+"/////");
  for(var i=0;i<4;i++){
          console.log(this.optionimgArray[i].indux+".....");
           }*/
           if(displayResult==false){
  if(selectedOption!=-1) {
    this.optionimgArray[selectedOption].visible=true;
  }
  selectedOption=e.indux;
  console.log(e.indux);
  this.optionimgArray[selectedOption].visible=false;
  submit.visible=true;
}
  /**/
 },
 //Validation method for 1st question
 validation:function(){
    var ans=this.currentQuestion.answers[this.currentQuestion.correctAnswerIndex];
    if(this.currentQuestion.correctAnswerIndex==selectedOption){
      score+=1;
      textspace_T.visible = true;
      //correct.visible = true;
      correctcnfrm.visible = true;
      next.visible = true;
      //p_quizt.visible = true;
      //this.P_QuizT();
    }else{
      textspace_F.visible = true;
      //wrong.visible = true;
      
      this.wrongcnfrm_1 = this.game.add.text(620,830,"Oops!!! You have chosen wrong answer!!\n Correct answer is "+ans+"!",style2);
      /*if(ans=="A yellow solution is formed and iron filings settle down."){
        this.wrongcnfrm_1.x=500;
        this.wrongcnfrm_1.y=832;
        //this.wrongcnfrm_1.scale.setTo(.95,.95);
        this.wrongcnfrm_1.text="Oops!!!You have chosen wrong answer!!\n Correct answer is A yellow solution is formed \nand iron filings settle down.";
      }*/
      next.visible = true;
      //p_quizw.visible = true;
      //this.P_QuizW();
   }
  this.currentQuestionIndex += 1;
  displayResult=true;
 },

resetTheGame:function(){
   // voice.stop();
    this.state.start("Quiz", true, false, ip);
 },

gotoHome:function(){
  window.location.href="../index.php";
   // voice.stop();
    //this.state.start("Title");
    //this.state.start("Aim",true,false,ip);
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   //voice.destroy();
//this.postData();
window.open(loc,"_self");
 //window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self"); // local test link
//   window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");   //cloud test link
 },

buttonState:function(state1,state2){

 state1.visible = false;
 state2.visible = true;
 },

buttonState2:function(state2,state1){

 state1.visible = true;
 state2.visible = false;
 },

/*P_QuizT:function()
	{
//		p_quizt=this.game.add.sprite(950,400,'p_charactor','q_true0.png');
  		p_quizt.anchor.setTo(0.5,0.5);
		p_quizt.animations.add('pl',['q_true0.png',
            'q_true1.png',
            'q_true2.png',
            'q_true3.png',
            'q_true4.png',
            'q_true5.png',
            'q_true6.png',
            'q_true7.png',
            'q_true8.png',
            'q_true9.png',
            'q_true10.png',
            'q_true11.png',
            'q_true12.png',
            'q_true13.png',
            'q_true14.png',
            'q_true15.png',
            'q_true16.png',
            'q_true17.png',
            'q_true18.png',
            'q_true19.png',
            'q_true20.png',
            'q_true21.png'

             ], 20, true, false);
		p_quizt.animations.play('pl');

	},*/
	/*P_QuizW:function()
	{
//		p_quizw=this.game.add.sprite(950,400,'p_charactor','q_wrong0.png');
  		p_quizw.anchor.setTo(0.5,0.5);
		p_quizw.animations.add('pl',['q_wrong0.png',
            'q_wrong1.png',
            'q_wrong2.png',
            'q_wrong3.png',
            'q_wrong4.png',
            'q_wrong5.png',
            'q_wrong6.png',
            'q_wrong7.png',
            'q_wrong8.png',
            'q_wrong9.png',
            'q_wrong10.png',
            'q_wrong11.png',
            'q_wrong12.png',
            'q_wrong13.png',
            'q_wrong14.png',
            'q_wrong15.png',
            'q_wrong16.png',
            'q_wrong17.png',
            'q_wrong19.png'
             ], 16, true, false);
		p_quizw.animations.play('pl');

	},*/

   /*P_Cry: function()
	{

		p_cry=this.game.add.sprite(1650,750,'p_charactor','cry0.png');
    p_cry.scale.setTo(2,2);
  		p_cry.anchor.setTo(0.5,0.5);
		p_cry.animations.add('pl',['cry0.png',
            'cry1.png'
             ], 5, true, false);
		p_cry.animations.play('pl');

	},*/

   /*P_Happy:function()
	{

		p_happy=this.game.add.sprite(1650,750,'p_charactor','happy0.png');
    p_happy.scale.setTo(2,2);
  		p_happy.anchor.setTo(0.5,0.5);
		p_happy.animations.add('pl',['happy0.png',
            'happy1.png',
            'happy2.png',
            'happy3.png',
            'happy4.png',
            'happy5.png',
            'happy6.png',
            'happy7.png',
            'happy8.png',
            'happy9.png',
            'happy10.png',
            'happy11.png',
            'happy12.png',
            'happy13.png'
             ], 11, true, false);
		p_happy.animations.play('pl');

	},*/
  ////////////////////////////////////////////////
  shuffleArray: function(array){
        for (var i = array.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    },
  showNextQuestion: function(){
    selectedOption=-1;
    displayResult=false;
     textspace_T.visible=false;
     textspace_F.visible=false;
     submit.visible=false;
     next.visible=false;
     //p_quizt.visible=false;
     //p_quizw.visible=false;
     correctcnfrm.visible=false;
     wrong.visible=false;
     correct.visible=false;
     if(this.wrongcnfrm_1!=null)
      this.wrongcnfrm_1.destroy(true);
    for (var i = 0; i < this.optionArray.length; i++) {
            var a = this.optionArray[i];
            var b=this.optionimgArray[i];
            var c=this.optedimgArray[i];
            c.destroy(true);
            b.destroy(true);
            a.destroy(true);

      }
      if(this.currentQuestionIndex >= this.totalQuestionsToShow){
        this.questionText.destroy(true);
        this.showResult();
      }else{
        this.currentQuestion = this.questions[this.currentQuestionIndex];
        this.questionText.text = (this.currentQuestionIndex + 1) + ". " +this.currentQuestion.question;
        //localStorage.setItem("qnumber",(this.currentQuestionIndex));
        this.takeAnswer=false;
        this.optionArray=[];
        this.optionimgArray=[];
        this.optedimgArray=[];

        var answers = this.currentQuestion.answers;
        var k=0;
        for(var i=0;i<answers.length;i++){
            var ans = answers[i];

          var style = { font: "40px Segoe UI", fill: "#ffffff", boundsAlignH: "left", boundsAlignV: "middle" };
          var option = this.game.add.text(0,0,ans,style);//prefix[i] + ". " +

          option.x = 500//this.game.world.centerX+5;
          if(answers.length==3){
            option.y = 520+(i*80);//450:700;//this.game.world.height - 60:this.game.world.height - 120;
            var optedimg = this.game.add.sprite(410,525+(i*80),'components','optedimg.png');
            optedimg.scale.setTo(.5,.5);
            var optionimg = this.game.add.sprite(410,525+(i*80),'components','optionimg.png');
          }else{
            option.y = 470+(i*80);//450:700;//this.game.world.height - 60:this.game.world.height - 120;
            var optedimg = this.game.add.sprite(410,475+(i*80),'components','optedimg.png');
            optedimg.scale.setTo(.5,.5);
            var optionimg = this.game.add.sprite(410,475+(i*80),'components','optionimg.png');
          }
          optionimg.scale.setTo(.5,.5);
          optionimg.indux = i;
          option.indux = i;
          option.optimg=optionimg;
          option.inputEnabled = true;
          option.input.useHandCursor = true;
          optionimg.inputEnabled = true;
          optionimg.input.useHandCursor = true;
          this.optionArray.push(option);
          this.optionimgArray.push(optionimg);
          this.optedimgArray.push(optedimg);

          option.events.onInputDown.add(this.check_ans, this);
          optionimg.events.onInputDown.add(this.check_ans, this);
          //option.events.onInputDown.add(function() {this.check_ans(option.optimg)}, this);
          //optionimg.events.onInputDown.add(function() {this.check_ans(optionimg)}, this);
        }
        /*for(var i=0;i<4;i++){
          console.log(this.optionimgArray[i].indux);
          this.optionArray[i].events.onInputDown.add(function() {this.check_ans(k)}, this);
          this.optionimgArray[i].events.onInputDown.add(function() {this.check_ans(k)}, this);
          k++;
        }*/
      }
    },
    hideButtonPanel:function(){
  /*if(buttonGroup.state=="seek"){
      this.buttontween=this.game.add.tween(buttonGroup).to( { x:160 }, 400, Phaser.Easing.Out, true);
      this.buttontween.onComplete.add(this.buttontweeningEnd,this);
  buttonGroup.state="running";
    }*/

 },
 buttontweening:function(){

  /*if(buttonGroup.state=="seek"){
    this.buttontween=this.game.add.tween(buttonGroup).to( { x:160 }, 400, Phaser.Easing.Out, true);
  }else if(buttonGroup.state=="hide"){
    this.buttontween=this.game.add.tween(buttonGroup).to( { x:0 }, 400, Phaser.Easing.Out, true);
  }
  this.buttontween.onComplete.add(this.buttontweeningEnd,this);
  buttonGroup.state="running";*/
 },
 buttontweeningEnd:function(){
  /*if(buttonGroup.x<=100){
    buttonGroup.state="seek";
  }
  else{
    buttonGroup.state="hide";
  }*/
 },
    showResult:function(){
      heading.text="Points:";
      this.game.add.text(600,550,"You have got " + score + " points out of possible 5",style5);
      //background.inputEnabled=true;
      //background.events.onInputDown.add(this.hideButtonPanel,this);
      buttonGroup.visible=true;

         /*if(score >= 30){
            this.P_Happy();
         }
         else{
            this.P_Cry();
          }*/
    },
    muteTheGame:function(){
      //this.detectCollision();
      muted = true;
    //   voice.stop();
       this.game.sound.mute = true;
       volumeButton.visible = true;
       muteButton.visible = false;

     },
     volume:function(){
       //this.detectCollision();
       this.game.sound.mute = false;
        volumeButton.visible = false;
        muteButton.visible = true;
     },

}
