var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  var count;
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var sceneNo;
 var pageCount;
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

sceneNo=1;
count=0;
  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  pageCount=1;
 muted = false;


 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  //////////////////////////////////Graph///////////////////////////
//  graph=this.game.add.sprite(1100,200,'obervation_graph');
//  graph.scale.setTo(.15,.15);

  /////////////////////////////////////////////////////////////////

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: " 40px Segoe UI", fill: "#ffffff", align: "left" };
  fontStyle_1={ font: "30px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "30px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  headfontStyle2={ font: "28px Segoe UI", fill: "#ffffff", align: "center", fontWeight:'bold' };
  fontStyle2={ font: "32px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  
  /*dialogbox1=this.game.add.sprite(50,30,'dialogue_box3');
  dialogbox1.scale.setTo(.8,.7);

  dialogbox2=this.game.add.sprite(50,900,'dialogue_box1');
  dialogbox2.scale.setTo(.65,.1);
  //dialogbox2.scale.setTo(.8,1);*/

  base= this.game.add.sprite(110,80,'observation_1');
  //base.scale.setTo(.8,1.1);
  base.scale.setTo(1,1);
  sno_text_1=this.game.add.text(175,320,"1",fontStyle);
  sno_text_2=this.game.add.text(175,600,"2",fontStyle);
  observation_text_1=this.game.add.text(280,300,"",fontStyle);
  inference_text_1=this.game.add.text(950,300,"",fontStyle);
  observation_text_2=this.game.add.text(280,580,"",fontStyle);
  inference_text_2=this.game.add.text(950,580,"",fontStyle);
  observation_text_3=this.game.add.text(280,600,"",fontStyle);
 

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=true;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

  this.addObservation();

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    
        nextSound:function()
        {

            voice.destroy();
            //voice=this.game.add.audio("A_observation2",1);
            voice2.play();

        },
        addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
             ///  voice.destroy();
        
              voice=this.game.add.audio("A_observation",1);
              voice.play();
              sno_text_1.text="1";
              sno_text_2.text="2";
              //experiment_text_1.text="On heating";
             // observation_text_1.text="Reaction of Iron with copper\n sulphate solution"
              observation_text_1.text="The colour of the solution changes\nfrom blue to pale green.";
              observation_text_2.text="Iron nail acquires reddish brown\ndeposit";
              inference_text_1.text="The colour of the solution changes to\npale green due to formation of FeSO\u2084\n\nFe(s)+CuSO\u2084(aq) -----> FeSO\u2084(aq)+Cu(s)";
              inference_text_2.text="It is a displacement reaction because iron\nhas displaced copper from copper\nsulphate. Copper liberated deposits\non the nail.";
              
              break;
            case 2:
              
            break;
            
          }
        },
        toNext:function(){
          if(pageCount<2){
            prev_btn.visible=true;
            pageCount++;
            
              if(pageCount>=2){
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      toPrev:function(){
         
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
      
       
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    //this.state.start("Aim", true, false, ip);
    window.location.href="../index.php";
  },
  update: function()
  {
    // if(sceneNo==1){
    //   count++;
    //   //console.log(count);
    //   if(count==1050){
    //     voice.destroy();
    //     voice=this.game.add.audio("A_observation2",1);
    //     voice.play();
    //   }
    // }
  },
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  //voice.destroy();
      //  voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  