var exp_selection = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;


}

exp_selection.prototype ={

init: function( ipadrs) 
{
  ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
 // leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Experiments /////////////////////////////////////
  button_Base= this.game.add.sprite(160,140,'dialogue_box')

  button1 = this.game.add.sprite(900, 275, 'exp1_title');
  button1.anchor.setTo(.5,.5);
  button1.inputEnabled = true;
  button1.input.useHandCursor = true;
  button1.events.onInputOver.add(this.toMouseOver, this);
  button1.events.onInputOut.add(this.toMouseOut, this);
  button1.events.onInputDown.add(function() {this.toMaterials("Iron with copper sulphate")}, this);

  button2 = this.game.add.sprite(900, 435, 'exp2_title');
  button2.anchor.setTo(.5,.5);
  button2.inputEnabled = true;
  button2.input.useHandCursor = true;
  button2.events.onInputOver.add(this.toMouseOver, this);
  button2.events.onInputOut.add(this.toMouseOut, this);
  button2.events.onInputDown.add(function() {this.toMaterials("Burning of magnesium")}, this);

  button3 = this.game.add.sprite(900, 595, 'exp3_title');
  button3.anchor.setTo(.5,.5);
  button3.inputEnabled = true;
  button3.input.useHandCursor = true;
  button3.events.onInputOver.add(this.toMouseOver, this);
  button3.events.onInputOut.add(this.toMouseOut, this);
  button3.events.onInputDown.add(function() {this.toMaterials("Zinc with dilute sulphuric acid")}, this);

  button4 = this.game.add.sprite(900, 755, 'exp4_title');
  button4.anchor.setTo(.5,.5);
  button4.inputEnabled = true;
  button4.input.useHandCursor = true;
  button4.events.onInputOver.add(this.toMouseOver, this);
  button4.events.onInputOut.add(this.toMouseOut, this);
  button4.events.onInputDown.add(function() {this.toMaterials("Heating of copper sulphate")}, this);
  
  button5 = this.game.add.sprite(900, 915, 'exp5_title');
  button5.anchor.setTo(.5,.5);
  button5.inputEnabled = true;
  button5.input.useHandCursor = true;
  button5.events.onInputOver.add(this.toMouseOver, this);
  button5.events.onInputOut.add(this.toMouseOut, this);
  button5.events.onInputDown.add(function() {this.toMaterials("Sodium sulphate with barium chloride")}, this);
  ///////////////////////////////////////////////////
  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 /*play = this.game.add.sprite(1600,870,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toMaterials, this);*/


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },

  ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

  // For Full screen checking.
  
      gofull: function()
      {
        if (this.game.scale.isFullScreen)
            {
            this.game.scale.stopFullScreen();
            }
        else
            {
             this.game.scale.startFullScreen(false);
            }  
      },

  //For to next scene   
 
      toMaterials:function(exp_nam)
      {
        //voice.destroy();
        exp_Name=exp_nam;
        this.state.start("Materials", true, false);
        
        //this.state.start("Procedure", true, false);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

gotoHome:function()
{
  voice.destroy();
  this.state.start("Aim", true, false);
},

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/

// To quit the experiment
closeTheGame:function()
{
voice.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link
//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },

toMouseOver:function(e){
  e.scale.x=1.02;
  e.scale.y=1.02;
  /*decomposition_btn.scale.x=1.1;
  decomposition_btn.scale.y=1.1;
  displacement_btn.scale.x=1.1;
  displacement_btn.scale.y=1.1;
  doubledisplacement_btn.scale.x=1.1;
  doubledisplacement_btn.scale.y=1.1;*/
},
toMouseOut:function(e){
  e.scale.x=1;
  e.scale.y=1;
  /*decomposition_btn.scale.x=1;
  decomposition_btn.scale.y=1;
  displacement_btn.scale.x=1;
  displacement_btn.scale.y=1;
  doubledisplacement_btn.scale.x=1;
  doubledisplacement_btn.scale.y=1;*/
},
}
