var experiment_a1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var arrow;
var colorChangingAnimstart;
var TostartTesttubeB;
var animationCounter;
var DeltaTime;
var dialogX;
var dialogY;



}

experiment_a1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
  this.arrowArr=new Array();
  this.arrowArr=[,];
  this.droperCnt=0;
  currentobj=null;
  this.dropped=false;
  dialogX=20;
  dialogY=80;
  this.nailchangeDrag=false;
  this.DeltaTime=null;
  this.changeColor=false;
 muted = false;
 voice=this.game.add.audio("obj",1);

 //this.arrangeScene();

 bg= this.game.add.sprite(0, 0,'bg');
 //bg.scale.setTo(.241,.253);




  
 


////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);

///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "38px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "32px Segoe UI", fill: "#000000", align: "left", fontWeight:"bold" };
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};

////////////////////////////////////////////////////////////////////

 

  this.arrangeScene();
  this.intDailog();
  
      

 },
 // For Full screen checking.
  
 gofull: function()
 {

 if (this.game.scale.isFullScreen)
   {
    this.game.scale.stopFullScreen();
   }
 else
   {
   this.game.scale.startFullScreen(false);
   }  
 },
 
 intDailog:function(){

    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
    dialog_text="Lets perform the experiment";
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
    voice=this.game.add.audio("lets_perform_Exp",1);
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadExp,this);
   
 },
 loadExp:function()
 {  
     voice.play();
     this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment,this);
 },
 startExperiment:function(){
  
    voice.stop();
    voice=this.game.add.audio("A_exp_procedure1",1);
    voice.play();
    dialog_text="";
    dialog.destroy();
  dialog_text="Take 10 ml of dilute copper sulphate solution from the beaker";
    dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
  
    arrow.visible=true;
    arrow.animations.play('allframes', 25,true);
   this.droper.inputEnabled = true;
    this.droper.input.useHandCursor = true;
    this.game.physics.arcade.enable(this.droper);
    this.droper.input.enableDrag(true);
    
  
 },
 arrangeScene:function(){
 
     this.droperCnt=1;
     this.beakerPart();
     this.rackPart();
     this.nailPart();
     this.standPart();
     this.ClockPart();
     this.HolderPart();
     this.ArrowPart();
   
   ///////////avoid click for nail
    this.avoidclick=this.game.add.sprite(300,800, 'collider');
    this.avoidclick.anchor.set(.5);
    this.avoidclick.scale.setTo(5,6);
    this.avoidclick.events.onInputDown.add(this.addtesttube,this)
    this.avoidclick.inputEnabled = true;
    this.avoidclick.alpha=.0001;
 },
 ArrowPart:function(){
  this.arrowArr=[[1306,620],[1045,530],[1306,620],[810,530],[1650,730],[820,530],[330,530],[1650,920],[330,520],[330,520],[1400,940],[330,520],[825,580]]
  arrow=this.game.add.sprite(this.arrowArr[0][0],this.arrowArr[0][1], 'arrow');
  arrow.anchor.set(.5);
  arrow.animations.add('allframes');
  arrow_y=arrow.y;
  arrow.visible=false;
 },
 HolderPart:function(){
  this.holder=this.game.add.sprite(1600,850,"holder");
  this.holder.anchor.set(.5);
  this.holder.scale.set(.8);

  this.holder.animations.add('frame1',[0]);
  this.holder.animations.add('frame2',[1]);
  this.holder.animations.add('frame3',[2]);
  this.holder.animations.play('frame1', 10,false);
  this.holder.xpos=1600
  this.holder.ypos=850
  this.holder.xpos1=950
  this.holder.ypos1=650
  this.holder.xpos2=460
  this.holder.ypos2=620
  this.dragForhold=false;

  this.colliderholder=this.game.add.sprite(-90,0, 'collider');
    this.game.physics.arcade.enable(this.colliderholder);
    this.colliderholder.anchor.set(.5);
    this.colliderholder.scale.setTo(1.5,.5);
    this.colliderholder.enableBody =true;
 this.colliderholder.alpha=.001;
 this.colliderholder.visible=false;
    this.holder.addChild(this.colliderholder);
  //this.holder.events.onDragStart.add(function() {this.DragOn(this.holder)}, this);
 // this.holder.events.onDragStop.add(function() {this.DropOut(this.holder)}, this);
 this.firstdrag=1;
  this.holder.events.onDragStart.add(function() {this.DragForHolder(this.holder)}, this);
  this.holder.events.onDragStop.add(function() {this.DropForHolder(this.holder)}, this);
 },
 DragForHolder:function(obj){

   this.dragForhold=true;

  if(this.firstdrag==1){
    this.dropped=false;
    this.collidertesttube11.visible=true;
    this.collidertesttube11.enableBody=true;
   
    this.collidertesttubestand.visible=false;
    this.collidertesttubestand.enableBody=false;
   
    this.arrowOnDrag(3);
  } else if(this.firstdrag==2){
    this.dropped=false;
    this.collidertesttubestandtop.visible=true;
   this.collidertesttubestandtop.enableBody=true;
 
    this.collidertesttube11.visible=false;
    this.collidertesttube11.enableBody=false;
    this.arrowOnDrag(7);
  }
 
 },
 DropForHolder:function(){
  
   if( this.dragForhold){
  if(this.firstdrag==1){
   
    this.testtubeback1.visible=true;
    this.testtubefront1.visible=true;
   
    this.collidertesttubestand.enableBody=false;
  
  }else if(this.firstdrag==2){
   
    this.testtubeonstand.alpha=1;
    this.testtubeonstandOnBack.alpha=1;
  
    this.collidertesttube11.enableBody=false;
   
  } 
  this.dropped=true;
  this.arrowPos(2);
 
  
  
  this.holder.anchor.setTo(.5,.5)
  this.holder.scale.set(.8);
  this.holder.animations.play('frame1', 10,false);
  this.holder.reset( this.holder.xpos,this.holder.ypos);
  this.dragForhold=false;
}
 },
 ClockPart:function(){
 
  this.clocks=this.game.add.sprite(550,290,"Clock");
  this.clocks.anchor.set(.5);
  this.clocks.scale.set(.7);
  this.clocksNeedleBig=this.game.add.sprite(550,290,"ClockNeedleBig");
  this.clocksNeedleBig.anchor.set(.5,.85);
  this.clocksNeedleBig.scale.set(.7);
  this.clocksNeedleSmall=this.game.add.sprite(550,290,"ClockNeedleSmall");
  this.clocksNeedleSmall.anchor.set(.5,.8);
  this.clocksNeedleSmall.scale.set(.7);
  //this.clocksNeedleSmall.visible=false;
  this.clockNum=0;
  this.clockNumForangle=0;
  
  this.changeColor=false;
 
 },
 beakerPart:function(){
  this.beakerback=this.game.add.sprite(1300,800,"beaker_back");
  this.beakerback.anchor.set(.5);
  this.beakerback.scale.set(.6);

  this.droper=this.game.add.sprite(1300,780,"droper");
  this.droper.scale.setTo(1,1.2);
  this.droper.anchor.set(.5);
  this.droper.animations.add('frame1',[0]);
  this.droper.animations.add('frameall');
  this.droper.animations.play('frame1', 10,false);
  this.droper.xpos=this.droper.x;
  this.droper.ypos=this.droper.y;
  this.droper.events.onDragStart.add(function() {this.DragOn(this.droper)}, this);
  this.droper.events.onDragStop.add(function() {this.DropOut(this.droper)}, this);

  this.beakerfront=this.game.add.sprite(1300,800,"beaker_front");
  this.beakerfront.anchor.set(.5);
  this.beakerfront.scale.set(.6);

 },
 rackPart:function(){
  this.rackback=this.game.add.sprite(923,880,"rackback");
  this.rackback.anchor.set(.5);
  this.rackback.scale.set(.55);
 
  this.testtubeback1=this.game.add.sprite(810,760,"testtubeback");
  this.testtubeback1.anchor.set(.5);
  this.testtubeback1.scale.set(.8);

  this.testtubefront1=this.game.add.sprite(810,760,"pouron_testube");
  this.testtubefront1.anchor.set(.5);
  this.testtubefront1.scale.set(.8);
  this.testtubefront1.animations.add('frame1',[0]);
  this.testtubefront1.animations.add('frameall');
  this.testtubefront1.animations.play('frame1', 10,false);

  this.testtubefrontOnLast=this.game.add.sprite(810,760,"testtubeOnRacklast");
  this.testtubefrontOnLast.anchor.set(.5);
  this.testtubefrontOnLast.scale.set(.8);
  this.testtubefrontOnLast.visible=false;

  this.testtubeback2=this.game.add.sprite(1037,760,"testtubeback");
  this.testtubeback2.anchor.set(.5);
  this.testtubeback2.scale.set(.8);

  this.collidertesttube1=this.game.add.sprite(810,580, 'collider');
  this.game.physics.arcade.enable(this.collidertesttube1);
  this.collidertesttube1.anchor.set(.5);
  this.collidertesttube1.scale.setTo(1,1);
 this.collidertesttube1.enableBody =true;
 this.collidertesttube1.alpha=.001;
 this.collidertesttube1.visible=false;

  this.collidertesttube11=this.game.add.sprite(810,670, 'collider');
  this.game.physics.arcade.enable(this.collidertesttube11);
  this.collidertesttube11.anchor.set(.5);
  this.collidertesttube11.scale.setTo(1,2.1);
  this.collidertesttube11.enableBody =true;
  this.collidertesttube11.alpha=.001;
  this.collidertesttube11.visible=false;
  this.collidertesttube11.xpos=this.collidertesttube11.x;
  this.droper1=this.game.add.sprite(810,530,"dropertotesttube");
  this.droper1.anchor.set(.5);
  this.droper1.anims=this.droper1.animations.add('frameall');
 // this.droper1.animations.play('frame1', 10,false);
 this.droper1.visible=false;
 
  this.testtubefront2=this.game.add.sprite(1037,760,"pouron_testube");
  this.testtubefront2.anchor.set(.5);
  this.testtubefront2.scale.set(.8);
  this.testtubefront2.animations.add('frame1',[0]);
  this.testtubefront2.animations.add('frameall');
  this.testtubefront2.animations.play('frame1', 10,false);


  this.collidertesttube2=this.game.add.sprite(1037,580, 'collider');
  this.game.physics.arcade.enable(this.collidertesttube2);
  this.collidertesttube2.anchor.set(.5);
  this.collidertesttube2.scale.setTo(1,1);
  this.collidertesttube2.enableBody =true;

  this.collidertesttube2.alpha=.001;
  this.collidertesttube2.visible=false;
  this.droper2=this.game.add.sprite(1037,510,"dropertotesttube");
  this.droper2.anchor.set(.5);
  this.droper2.scale.setTo(1,1.2);
  this.droper2.anims=this.droper2.animations.add('frameall');
  this.droper2.visible=false;

  this.rackfront=this.game.add.sprite(923,880,"rackfront");
  this.rackfront.anchor.set(.5);
  this.rackfront.scale.set(.55);
 },
 nailPart:function(){
    this.nailTile=this.game.add.sprite(1470,990,"nailtile");
    this.nailTile.anchor.set(.5);
    this.nailTile.scale.setTo(.25,.4);
  
    this.nailontable1=this.game.add.sprite(1430,1005,"nail");//nailontable1
    this.nailontable1.anchor.set(.5);
    //this.nailontable1.scale.set(.8);
    this.nailontable1.scale.x*=-1;

    this.nailontable2=this.game.add.sprite(1600,975,"nailontable1");
    this.nailontable2.anchor.set(.5,.8);
    //this.nailontable2.scale.set(.8);
    this.nailontable2.scale.x*=-1;
    //this.nailontable2.visible=false;
    //this.nailontable2.angle=-90
    this.nailontable21=this.game.add.sprite(1600,975,"nailontable2");
    this.nailontable21.anchor.set(.5,.8);
    //this.nailontable21.scale.set(.8);
    this.nailontable21.scale.x*=-1;
    this.nailontable21.visible=false;

    this.nailontable2.xpos=this.nailontable2.x;
    this.nailontable2.ypos=this.nailontable2.y;
   
    this.nailontable2.events.onDragStart.add(function() {this.DragOn(this.nailontable2)}, this);
    this.nailontable2.events.onDragStop.add(function() {this.DropOut(this.nailontable2)}, this);
  //  this.nailontable2.inputEnabled = true;
  //  this.nailontable2.input.enableDrag(false,true);
  //  this.nailontable2.input.useHandCursor = true;
   
    this.game.physics.arcade.enable(this.nailontable2);


    this.collidernailstand=this.game.add.sprite(1500,970, 'collider');
    this.game.physics.arcade.enable(this.collidernailstand);
    this.collidernailstand.anchor.set(.5);
    this.collidernailstand.scale.setTo(8,3);
    this.collidernailstand.enableBody =true;
    this.collidernailstand.alpha=.001;
    this.collidernailstand.visible=false;

    this.collidernail1=this.game.add.sprite(260,10, 'collider');
    this.game.physics.arcade.enable(this.collidernail1);
    this.collidernail1.anchor.set(.5);
    this.collidernail1.scale.setTo(.5,1.2);
    this.collidernail1.angle=-90
    this.collidernail1.enableBody =true;
    this.collidernail1.alpha=.001;
    this.collidernail1.visible=false;
    this.nailontable2.addChild(this.collidernail1);
 },
 standPart:function(){
   this.stand1=this.game.add.sprite(280,616,"standportion2");
    this.stand1.scale.setTo(1,.8);
  
    this.stand=this.game.add.sprite(250,750,"stand");
    this.stand.anchor.set(.5);
    this.stand.scale.set(.8);
    
    this.changecolorNum=1;
    this.testtubeonstandOnBack=this.game.add.sprite(325,720,'testtubeback'); 
    this.testtubeonstandOnBack.anchor.set(.5);
    this.testtubeonstandOnBack.scale.set(.8);
      
    this.nailcolorchange=this.game.add.sprite(365,905,'nailcolourchange'); 
    this.nailcolorchange.anchor.set(.5,1);
    this.nailcolorchange.scale.set(.85);
   
    for(var i=1;i<16;i++){
      this.nailcolorchange.animations.add('frame'+i,[i-1]);
    }
    this.nailcolorchange.animations.play('frame15', 10,false);
    this.game.physics.arcade.enable(this.nailcolorchange);
    this.nailcolorchange.xpos=this.nailcolorchange.x;
    this.nailcolorchange.ypos=this.nailcolorchange.y;
    this.nailcolorchange.events.onDragStart.add(function() {this.DragOn(this.nailcolorchange)}, this);
    this.nailcolorchange.events.onDragStop.add(function() {this.DropOut(this.nailcolorchange)}, this);

    this.naildrop=this.game.add.sprite(135,-430,'nailthread'); 
    this.naildrop.anchor.set(.5);
    this.naildrop.scale.set(1.05);
    this.stand.addChild(this.naildrop);
    this.naildrop.anims=this.naildrop.animations.add('frameall');
    this.naildrop.visible=false;
    this.naildrop.anims=this.naildrop.animations.add('frameall');
   
    this.testtubeonstand=this.game.add.sprite(325,720,'colourchange'); 
    this.testtubeonstand.anchor.set(.5);
    this.testtubeonstand.scale.set(.8);
    for(var i=1;i<16;i++){
      this.testtubeonstand.animations.add('frame'+i,[i-1]);
    }
    this.testtubeonstand.animations.play('frame1', 10,false);
    
   this.testtubeonstand.alpha=0;
    this.testtubeonstandOnBack.alpha=0;
    this.nailcolorchange.alpha=0



    this.collidertesttubestand=this.game.add.sprite(90,-20, 'collider');
    this.game.physics.arcade.enable(this.collidertesttubestand);
    this.stand.addChild(this.collidertesttubestand);
    this.collidertesttubestand.anchor.set(.5);
    this.collidertesttubestand.scale.setTo(1,6);
    this.collidertesttubestand.enableBody =true;

   this.collidertesttubestand.alpha=.001;
   this.collidernail1.visible=false;

   this.collidertesttubestandtop=this.game.add.sprite(90,-200, 'collider');
   this.game.physics.arcade.enable(this.collidertesttubestandtop);
   this.stand.addChild(this.collidertesttubestandtop);
   this.collidertesttubestandtop.anchor.set(.5);
   this.collidertesttubestandtop.scale.setTo(2,3);
   this.collidertesttubestandtop.enableBody =true;

  this.collidertesttubestandtop.alpha=.001;
  this.collidernail1.visible=false;

   
  this.standportion1=this.game.add.sprite(325,635,"standportion1");
  this.standportion1.anchor.set(.5);
  this.standportion1.scale.set(.8);
 },

 //to avoid click nail
 addtesttube:function(){

 },
 ////////////////////////////////////////////////////////////////
 //Drag&drop
  DragOn(obj){
   
    obj.body.enable =false;
    
    currentobj=obj;
    if(currentobj==this.droper&&this.droperCnt==1)
    {
      this.arrowOnDrag(1);
      this.droper.animations.play('frameall', 50,false);
    }
    else if(currentobj==this.droper&&this.droperCnt==2)
    {
      this.arrowOnDrag(2);
      this.droper.animations.play('frameall', 50,false);
    
    }else if(currentobj==this.nailontable2)
    {
      
      this.arrowOnDrag(5);
      currentobj.angle=-90
    
  
      
    }else if(currentobj==this.nailcolorchange)
    {
      this.arrowOnDrag(6);
      //this.nailchangeDrag=true;
      this.nailcolorchange.anchor.set(.3,.9);
      this.nailcolorchange.animations.play('frame15', 10,false);
     
  
      
    }
  },
  DropOut(obj){
    obj.body.enable =true;
  
  },

  //check collision
    detectCollision:function(){
      if(this.collidertesttube2.enableBody && currentobj!=null&&this.droperCnt==1&& currentobj==this.droper)
      {
          this.game.physics.arcade.overlap(this.droper, this.collidertesttube2,function() {this.match_Obj()},null,this);
        
      } 
      if(this.collidertesttube1.enableBody && currentobj!=null&&this.droperCnt==2&& currentobj==this.droper)
      {
          this.game.physics.arcade.overlap(this.droper, this.collidertesttube1,function() {this.match_Obj()},null,this);
        
      } 
   
      if(this.collidertesttubestandtop.enableBody && currentobj!=null&& currentobj==this.nailontable2)
      {
     
        if(this.collidernail1!=null){
          
      this.game.physics.arcade.overlap(this.collidernail1, this.collidertesttubestandtop,function() {this.match_Obj()},null,this);
        }

      }
      if(this.collidernailstand.enableBody && currentobj!=null&& currentobj==this.nailcolorchange)
      {
      this.game.physics.arcade.overlap(this.nailcolorchange, this.collidernailstand,function() {this.match_Obj()},null,this);
        
      }
    

      ////////////////////////////////////////
      if(currentobj!=null && currentobj.body.enable){
        currentobj.reset(currentobj.xpos,currentobj.ypos);//
      
        if(currentobj==this.droper&&this.droperCnt==1)
      {
      this.dropped=true;
      dialog.destroy();
     
      dialog_text="Take 10 ml of dilute copper sulphate solution from the beaker";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
      
        this.droper.animations.play('frame1', 10,false);
        arrow.x=this.arrowArr[0][0];
        arrow.y=this.arrowArr[0][1];
        currentobj=null;

        }else  if(currentobj==this.droper&&this.droperCnt==2)
        {
          this.dropped=true;
        
          this.holder.scale.set(.8);
        this.arrowPos(1);
          this.droper.animations.play('frame1', 10,false);
          arrow.x=this.arrowArr[0][0];
          arrow.y=this.arrowArr[0][1];
          currentobj=null;
    
            }
        else if(currentobj==this.nailontable2)
          {
            this.dropped=true;
            this.arrowPos(4);
            currentobj.angle=0
            currentobj=null;
            }else if(currentobj==this.nailcolorchange)
            {
              this.dropped=true;
              this.arrowPos(7);
              this.nailcolorchange.anchor.set(.5,1);
              this.nailcolorchange.animations.play('frame15', 10,false);
              currentobj=null;
            }
    }
    
    },

  //collision occur
    match_Obj:function(){

      currentobj.inputEnabled=false;
      currentobj.input.enableDrag(false);
      currentobj.body.enable=false;
    
      this.dropped=false;
      if(currentobj==this.droper&&this.droperCnt==1)
      {
        this.ColliderDisable()
        arrow.visible=false;
        this.droper.visible=false;
        this.droper2.visible=true;
        this.droper2.animations.play('frameall', 10,false);
        this.collidertesttube2.visible=false;
        this.testtubefront2.animations.play('frameall', 10,false);
        
        this.droper2.anims.onComplete.add(function () {
            this.droper2.visible=false;
            this.droper.visible=true;
            this.droper.animations.play('frame1', 10,false);
            var t= this.game.add.tween( this.droper).to({x:  this.droper.xpos, y:  this.droper.ypos}, 100, Phaser.Easing.In, true);
            t.onComplete.add(function () {
              this.collidertesttube1.visible=true;
              this.collidertesttube1.enableBody=true;
              this.droper.inputEnabled=true;
              this.droper.input.useHandCursor = true;
              this.droper.input.enableDrag(true);
              this.droper.body.enable=true;
              this.droperCnt=2;
              this.dropped=false;
            
              this.arrowPos(1);
              
              currentobj=null;
            }.bind(this));
          }.bind(this));
      
      }
      else if(currentobj==this.droper&&this.droperCnt==2)
      {
        this.ColliderDisable()
        arrow.visible=false;
        this.droper.visible=false;
        this.droper1.visible=true;
        this.collidertesttube1.visible=false;
        this.droper1.animations.play('frameall', 10,false);
        this.droper1.scale.setTo(1,1.2);
        this.testtubefront1.animations.play('frameall', 10,false);
        this.droper1.anims.onComplete.add(function () {
          this.droper1.visible=false;
          this.droper.visible=true;
          this.droper.animations.play('frame1', 10,false);
          var t= this.game.add.tween( this.droper).to({x:  this.droper.xpos, y:  this.droper.ypos}, 100, Phaser.Easing.In, true);
          t.onComplete.add(function () {
            this.ColliderDisable();
            this.arrowPos(2);
          
          this.droperCnt=5;
          this.droper.inputEnabled=false;
          this.colliderholder.visible=true;
          this.colliderholder.enableBody=true;

        
          this.droper.input.enableDrag(false);
          this.droper.body.enable=false;
          this.holder.inputEnabled = true;
          this.holder.input.useHandCursor = true;
          this.game.physics.arcade.enable(this.holder);
          this.holder.input.enableDrag(true);
         // this.collidertesttube11.enableBody=true;
          //this.collidertesttube11.visible=true;
          currentobj=null;
          }.bind(this));
        }.bind(this));
        
        
      
      }
     else if(currentobj==this.nailontable2)
      {
       
        this.ColliderDisable()
        arrow.visible=false;
        this.naildrop.visible=true;
     
        this.nailontable2.visible=false;
        this.collidernail1.visible=false;
        this.collidernail1.enableBody=false;
        this.collidernail1.visible=false
        this.nailontable2.removeChild( this.collidernail1);
        this.collidertesttubestandtop.visible=false;
        this.naildrop.animations.play('frameall', 10,false);
        this.naildrop.anims.onComplete.add(function () {
          this.nailcolorchange.alpha=1
         
          this.nailcolorchange.animations.play('frame1', 10,false);
          this.naildrop.alpha=0;
        // this.clockNum=0;
        this.clockNum=0;
          this.changeColor=true;
          this.arrowPos(5);
          this.angleTimer= this.game.time.events.loop(Phaser.Timer.SECOND,this.MoveAngle,this);
        }.bind(this));
        
        currentobj=null;
      
      }else if(currentobj==this.nailcolorchange)
      {
       this.ColliderDisable();
        this.nailontable21.visible=true;
        this.nailcolorchange.visible=false;
        this.collidernailstand.visible=false;
        this.collidernailstand.enableBody =false;
        this.arrowPos(2);
       // this.collidertesttubestandtop.enableBody =true;
       this.collidertesttubestandtop.visible=false;
        

        this.holder.inputEnabled = true;
        this.holder.input.useHandCursor = true;
        this.holder.input.enableDrag(true);
        this.colliderholder.visible=true;
        this.colliderholder.enableBody=true;
        this.firstdrag=2;
        this.dropped=false;
        this.droperCnt=10;
        currentobj=null;
      }
      
    },
    ///AngleMovement
    angleMovement:function(){
      if(this.clockNum>=40){
        this.game.time.events.remove(this.angleTimer);
      this.nailcolorchange.animations.play('frame15', 10,false);
      this.testtubeonstand.animations.play('frame15', 10,false);
        
        this.clocksNeedleBig.angle=90;
        this.changeColor=false;
        this.arrowPos(7);
      
        this.nailcolorchange.inputEnabled = true;
        this.nailcolorchange.input.useHandCursor = true;
        this.nailcolorchange.input.enableDrag(true);
        this.collidernailstand.visible=true;
        this.collidernailstand.enableBody=true;
      }else{
        if(this.clocksNeedleBig.angle>=90){
         
            this.clockNum=40;
          }
       
         this.clocksNeedleBig.angle += 3* DeltaTime;
      
        
        
      }
      
    },
    MoveAngle:function(){
      
      this.clockNum+= 1;
  
      if(this.clockNum%2==0){
        if( this.changecolorNum<16){
          if(this.changecolorNum==8){
            voice.stop();
          voice=this.game.add.audio("A_exp_procedure21",1);
          voice.play();
            dialog.destroy();
            dialog_text="You can now observe that the colour of the copper sulphate solution\nslowly turns green as a result of reacting with the iron nail.";
            dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
          }
      this.changecolorNum++;
      this.nailcolorchange.animations.play('frame'+ this.changecolorNum, 10,false);
      this.testtubeonstand.animations.play('frame'+ this.changecolorNum, 10,false);
    
        }
     }
    },

  //arrow,sound,dialogtext
    arrowOnDrag:function(num){
      arrow.visible=true;
      arrow.animations.play('allframes', 25,true);
    
      switch(num){
        case 1:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure2",1);
          voice.play();
          dialog.destroy();
          dialog_text="Pour the solution to the test tube";
          dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
          
          arrow.x=this.arrowArr[1][0];
          arrow.y=this.arrowArr[1][1];
          
        break;

        case 2:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure3",1);
          voice.play();
          dialog.destroy();
          dialog_text="Pour the solution to other test tube";
          dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
      
          arrow.x=this.arrowArr[3][0];
          arrow.y=this.arrowArr[3][1];
          
        break;

        case 3:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure4",1);
          voice.play();
            dialog.destroy();
            dialog_text="Pick the test tube using holder";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[5][0];
            arrow.y=this.arrowArr[5][1];
          
        break
        case 4:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure5",1);
          voice.play();
            dialog.destroy();
            dialog_text="Place the test tube on the stand";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[6][0];
            arrow.y=this.arrowArr[6][1];
          
        break;
        case 5:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure6",1);
          voice.play();
          dialog.destroy();
          dialog_text="Put the iron nail in the copper sulphate solution";
          dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
          arrow.x=this.arrowArr[8][0];
          arrow.y=this.arrowArr[8][1];
      
        break;
        case 6:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure7",1);
          voice.play();
            dialog.destroy();
            dialog_text="Place the iron nail on the nail board";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[10][0];
            arrow.y=this.arrowArr[10][1];
          
          
        break;
        case 7:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure4",1);
          voice.play();
            dialog.destroy();
            dialog_text="Pick the test tube using holder";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[11][0];
            arrow.y=this.arrowArr[11][1];
          
        break
        case 8:
          voice.stop();
          voice=this.game.add.audio("A_exp_procedure9",1);
          voice.play();
            dialog.destroy();
            dialog_text="Place the test tube in the rack";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[12][0];
            arrow.y=this.arrowArr[12][1];
          
        break
      }
    },
    arrowPos:function(num){
      arrow.visible=true;
      arrow.animations.play('allframes', 25,true);
    
      switch(num){
        case 1:
          voice.stop();
      
          if(this.dropped==false){
        
          voice=this.game.add.audio("A_exp_procedure10",1);
          voice.play();
          }
            dialog.destroy();
            dialog_text="Again take 10 ml of dilute copper sulphate solution from the beaker";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[2][0];
            arrow.y=this.arrowArr[2][1];
            
        break;

        case 2:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure11",1);
          voice.play();
          }
            dialog.destroy();
            dialog_text="Take the holder";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[4][0];
            arrow.y=this.arrowArr[4][1];
          
        break;

        case 3:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure12",1);
          voice.play();
          }
            dialog.destroy();
            dialog_text="Take the test tube from the rack";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[5][0];
            arrow.y=this.arrowArr[5][1];
        
        break
        case 4:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure13",1);
          voice.play();}
            dialog.destroy();
            dialog_text="Take one iron nail from the nail board";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[7][0];
            arrow.y=this.arrowArr[7][1];
        
        break;
        case 5:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure14",1);
          voice.play();
          }
          dialog.destroy();
          dialog_text="Now wait 15 minutes to see the reaction";
          dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
        
          arrow.visible=false;
        break;
        case 6:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure14",1);
          voice.play();
          }
            dialog.destroy();
            dialog_text="Now wait 15 minutes to see the reaction";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
          arrow.visible=false;
          
        break;
        case 7:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure15",1);
          voice.play();
          }
            dialog.destroy();
            dialog_text="Take the iron nail from the solution";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.visible=true;
            arrow.x=this.arrowArr[9][0];
            arrow.y=this.arrowArr[9][1];
          
        break
        case 8:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure16",1);
          voice.play();
          }
            dialog.destroy();
            dialog_text="Now take the holder";
            dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
            arrow.x=this.arrowArr[4][0];
            arrow.y=this.arrowArr[4][1];
        
        break
        case 9:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure17",1);
          voice.play();
          }
              dialog.destroy();
              dialog_text="Take the test tube from the stand";
              dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
              arrow.x=this.arrowArr[11][0];
              arrow.y=this.arrowArr[11][1];
          
        break
        case 10:
          voice.stop();
          if(this.dropped==false){
          voice=this.game.add.audio("A_exp_procedure18",1);
          voice.play();
          }
              dialog.destroy();
             
             // dialog_text="Now compare the color of the copper sulphate solution, observe\nthat the blue colour of copper sulphate solution has turned green\nupon reacting with the iron nail";
             // dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
             dialog_text="Now compare the colour of the copper sulphate solution, observe that the blue\ncolour of copper sulphate solution has turned green upon reacting with the\niron nail";
             dialog=this.game.add.text(dialogX,dialogY-42,dialog_text,fontStyle);
            
             
              arrow.visible=false;
              arrow1=this.game.add.sprite(1150,720, 'arrow');
              arrow1.angle=90;
              arrow1.anchor.set(.5);
              arrow1.scale.set(1.5)
              arrow1.animations.add('allframes');
              arrow1.animations.play('allframes',25,true);

              arrow2=this.game.add.sprite(700,720, 'arrow');
              arrow2.angle=-90;
              arrow2.anchor.set(.5);
              arrow2.scale.set(1.5)
              arrow2.animations.add('allframes');
              arrow2.animations.play('allframes',25,true);

              this.game.time.events.add(Phaser.Timer.SECOND*9,this.ShowCompTesttube,this);
          break;
      }
    },
    //Compare the color
    ShowCompTesttube:function(){
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure19",1);
      voice.play();
      dialog.destroy();
     

      dialog_text="Compare the nails and observe that the colour of iron nail has turned\nreddish brown on reacting with copper sulphate solution";
      dialog=this.game.add.text(dialogX,dialogY-25,dialog_text,fontStyle);
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.ShowCompNail,this);
      arrow1.destroy();
      arrow2.destroy();
      arrow1=this.game.add.sprite(1580,980, 'arrow');
      arrow1.angle=90;
      arrow1.anchor.set(.5);
      arrow1.scale.set(1.5)
      arrow1.animations.add('allframes');
      arrow1.animations.play('allframes',25,true);

      arrow2=this.game.add.sprite(1290,1000, 'arrow');
      arrow2.angle=-90;
      arrow2.anchor.set(.5);
      arrow2.scale.set(1.5)
      arrow2.animations.add('allframes');
      arrow2.animations.play('allframes',25,true);
      //arrow2.visible=false;
    },
    ShowCompNail:function(){
      arrow1.destroy();
      arrow2.destroy();
      dialog.destroy();
      voice.stop();
      voice=this.game.add.audio("A_exp_procedure20",1);
      voice.play();
      dialog_text="Click on the next button to see the observation.";
      dialog=this.game.add.text(dialogX,dialogY,dialog_text,fontStyle);
      play.visible=true;
    },
    update: function()
    {
      DeltaTime=this.game.time.elapsed/1000;
      this.detectCollision();
      if( this.changeColor){
      this.angleMovement();
      }
      if(this.naildrop.visible){
        this.naildrop.y+=300* DeltaTime;
      }
      if(this.holder.inputEnabled){
      this.detectCollisionForHolder();
      }
    
      
      
  
    },
    ColliderDisable:function(){
      this.collidertesttube11.enableBody=false;
      this.collidertesttubestand.enableBody=false;
      this.colliderholder.enableBody=false;
      this.collidertesttubestandtop.enableBody=false;
      this.collidernailstand.enableBody=false;
      this.collidernail1.enableBody=false;
      this.collidertesttube1.enableBody=false;
      
    },
    //collision for holder
    detectCollisionForHolder:function(){
      if(this.firstdrag==1){
        if(this.collidertesttube11.enableBody)
        {
          
      
        this.game.physics.arcade.overlap(this.colliderholder, this.collidertesttube11,function() {this.match_withHolder(1)},null,this);
        
        }
        if(this.collidertesttubestand.enableBody )
        {
        
        this.game.physics.arcade.overlap(this.colliderholder, this.collidertesttubestand,function() {this.match_withHolder(2)},null,this);
        
        }
    }else if(this.firstdrag==2){

      if(this.collidertesttubestandtop.enableBody)
      {
      this.game.physics.arcade.overlap(this.colliderholder, this.collidertesttubestandtop,function() {this.match_withHolder(3)},null,this);
      
      }
        if(this.collidertesttube11.enableBody)
      {
      this.game.physics.arcade.overlap(this.holder, this.collidertesttube11,function() {this.match_withHolder(4)},null,this);
      
      }
    }
    
    },
    match_withHolder(holderNum){
      switch(holderNum){
        case 1:
        
          this.ColliderDisable();
          this.collidertesttube11.visible=false;
            this.collidertesttube11.enableBody=false;
            this.testtubeback1.visible=false;
            this.testtubefront1.visible=false;
            
            this.holder.animations.play('frame2', 10,false);
            this.collidertesttubestand.visible=true;
            this.collidertesttubestand.enableBody=true;
            this.colliderholder.visible=true;
            this.colliderholder.enableBody=true
        
            this.dropped=false;
            this.arrowOnDrag(4);
              this.holder.anchor.setTo(.4,.4)
              this.holder.scale.set(1.05);
          break;
          case 2:
          
            this.ColliderDisable();
            this.collidertesttubestand.visible=false;
            this.collidertesttubestand.enableBody=false;
            this.holder.animations.play('frame1', 10,false);
            this.holder.reset( this.holder.xpos,this.holder.ypos);
            this.dropped=false;
            this.holder.anchor.setTo(.5,.5)
            this.holder.scale.set(.8);
            this.holder.inputEnabled = false;
            this.holder.input.useHandCursor = false;
            this.holder.input.enableDrag(false);
            this.colliderholder.visible=false;
          
            this.holder.reset(this.holder.xpos,this.holder.ypos);
            this.testtubeback1.visible=false;
            this.testtubefront1.visible=false;
            this.testtubeonstand.alpha=1;
            this.testtubeonstandOnBack.alpha=1;
            this.nailontable2.inputEnabled = true;
            this.nailontable2.input.enableDrag(false,true);
            this.nailontable2.input.useHandCursor = true;
           
            this.collidernail1.visible=true;
            this.collidernail1.enableBody=true;
          
            this.collidertesttubestandtop.enableBody=true;
            this.collidertesttubestandtop.visible=true;
          
            this.arrowPos(4);
            this.dragForhold=false;
            this.droperCnt=8;
            this.firstdrag=10;
          break;
          case 3:
          
            this.ColliderDisable();
            this.testtubeonstand.alpha=0;
            this.testtubeonstandOnBack.alpha=0;
            this.collidertesttubestandtop.visible=false;
            this.collidertesttubestandtop.enableBody=false;
            this.dropped=false;
            this.arrowOnDrag(8);
            this.colliderholder.visible=true;
            this.colliderholder.enableBody=true
            this.holder.animations.play('frame3', 10,false);
            this.collidertesttube11.visible=true;
            this.collidertesttube11.enableBody=true;
            this.collidertesttube11.x=this.collidertesttube11.xpos+350
          
            this.holder.anchor.setTo(.4,.4)
            this.holder.scale.set(1.05);
            break;
            case 4:
            
              this.ColliderDisable();
              this.holder.animations.play('frame1', 10,false);
              this.holder.anchor.setTo(.5,.5)
              this.holder.scale.set(.8);
              this.holder.inputEnabled = false;
              this.holder.input.useHandCursor = false;
              this.holder.input.enableDrag(false);
              this.holder.reset(this.holder.xpos,this.holder.ypos);
              this.collidertesttube11.visible=false;
              this.collidertesttube11.enableBody=false
              this.testtubefrontOnLast.visible=true;
              this.testtubefront1.visible=false;
              this.testtubeback1.visible=true;
              this.dragForhold=false;
              arrow.visible=false
              this.dropped=false;
              this.arrowPos(10);
              this.firstdrag=3
            
        break;
      }

    },
 
 
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        window.location.href="../index.php";
        //this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
