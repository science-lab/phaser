var theory = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  }
  
  theory.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_disabled.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  //quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  //muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_text=this.game.add.text(250,200,"Theory:",headfontStyle);
 // exp_Name="decomposition_reaction";
  console.log(exp_Name+"......");
  
  switch(exp_Name){
    //exp_Name
    case "decomposition_reaction":
      //procedure_audio1.play();
     // procedure_audio1.onStop.add(this.GotoNextScreen,this);
      procedure_step_1=" Ferrous sulphate crystals on heating, first lose water of crystallization, and on\n further heating anhydrous ferrous sulphate gives ferric oxide, sulphur dioxide \n and sulphur trioxide.";
      procedure_step_2=" FeSO\u2084.7H\u2082O (s)--------> FeSO\u2084 (s) + 7H\u2082O (l)";
      procedure_step_3=" 2FeSO\u2084(s)--------> Fe\u2082O\u2083(s) + SO\u2082 (g) + SO\u2083(g)";
      procedure_step_4=" It is a decomposition reaction because a single compound is giving different\n compounds on heating.";
      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,500,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(300,610,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(300,710,procedure_step_4,fontStyle);
    break;
    case "doubledisplacement_reaction":  
      procedure_step_1="1. Take two test tubes, wash them with distilled water and dry them. \nLabel the test tube as A and B. ";
      procedure_step_2="2. Add 5mL of barium chloride in the test tube A and Add 5mL of sodium \nsulphate in the test tube B. Observe the colour of the solutions.";
      procedure_step_3="3. Take a conical flask after washing with distilled water and \ntransfer carefully the solutions from both the test tubes into it.";
      procedure_step_4="4. Stir the mixture with a glass rod and keep it undisturbed.";
      procedure_step_5="5. Observe the changes.";
      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,460,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(300,620,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(300,780,procedure_step_4,fontStyle);
      procedure_step_text_5=this.game.add.text(300,860,procedure_step_5,fontStyle);
    break;
    case "combination_reaction":
      procedure_step_1="1. Wash a Borosil beaker with distilled water and dry it. ";
      procedure_step_2="2. Take a small amount of calcium oxide (quick lime) and slowly \nadd water to it.";
      procedure_step_3="3. Stir it with a clean glass rod.";
      procedure_step_4="4. Touch the beaker carefully from outside.";
      procedure_step_5="5. Observe the change.";
      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,380,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(300,520,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(300,600,procedure_step_4,fontStyle);
      procedure_step_text_5=this.game.add.text(300,680,procedure_step_5,fontStyle);
    break;
    case "displacement_reaction":  
      procedure_step_1="1. Clean two iron nails of sufficient size by rubbing them with sand paper \nso that their colour appears grayish.";
      procedure_step_2="2. Take sufficient quantity of copper sulphate solution in two different \ntest tubes. Clamp the test tubes to different stands.";
      procedure_step_3="3. Tie one nail using a thread and hang in one test tube. \nTie the other end of the thread to the stand.";
      procedure_step_4="4. Keep the other nail in a Petri dish for comparison after the experiment. \nKeep the two test tubes undisturbed for about 15 minutes.";
      procedure_step_5="5. Remove the iron nail immersed in the copper sulphate solution \nand put it in the Petridish.";

      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,440,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(300,580,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(300,720,procedure_step_4,fontStyle);
      procedure_step_text_5=this.game.add.text(300,860,procedure_step_5,fontStyle);
      
    break;
  }
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1600,870,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toLab_precautions, this);


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
  
    //For to next scene   
   
    toLab_precautions:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
         // procedure_audio2.destroy();
          
          switch(exp_Name){
            
            case "decomposition_reaction":
                // procedure_audio2.destroy();
                // procedure_audio1.destroy();
              this.state.start("Lab_Precautions", true, false);
            break;
            case "doubledisplacement_reaction": 
              //this.state.start("Experiment_2", true, false);
            break;
            case "combination_reaction":
             // this.state.start("Experiment_3", true, false);
            break;
            case "displacement_reaction": 
              //this.state.start("Experiment_4", true, false);
            break;
          }  
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    procedure_audio1.destroy();
    procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  procedure_audio1.destroy();
  procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  //window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  