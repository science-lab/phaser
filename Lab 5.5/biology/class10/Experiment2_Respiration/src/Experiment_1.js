var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;
var interval;
var content1;
var lineFlag;
var lineCount;
var line1;
var line2;
var linex1;
var clockFlag;
var dayFlag;
var clockNum;
var clockDelay;
var clockAngle;

}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;

bg= this.game.add.sprite(0, 0,'bg');

interval=0;
dayFlag=false;
lineFlag=false;
currentobj=null;
line1=null;
line2=null;
linex1=0;
lineCount=1;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/

voice=this.game.add.audio("step0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#0000FF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(800,920, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody =true;
    collider.alpha=0.0;

    
    

    beaker_back=this.game.add.sprite(1400,810,'beaker_back');
    beaker_back.anchor.set(.5);
    beaker_back.scale.set(.75);
    beaker_back.visible=false;

    flask_back=this.game.add.sprite(1000,750,'flask_back');
    flask_back.anchor.set(.5);
    cork_back=this.game.add.sprite(1003,565,'cork_back');
    cork_back.anchor.set(.5);
    cork_back.scale.set(.8);


    // thermometer_needle=this.game.add.sprite(991,530,'thermometer_needle');
    // thermometer_needle.anchor.setTo(.5,1);
    // thermometer_needle.scale.setTo(1.3,.5);
    // thermometer_needle.visible=false;
    thermometer=this.game.add.sprite(991,270,'thermometer_20');
    thermometer.anchor.set(.5);
    thermometer.scale.setTo(.8);
    thermometer.visible=false;
    thermometer_30=this.game.add.sprite(991,430,'thermometer_30');
    thermometer_30.anchor.set(.5);
    thermometer_30.scale.setTo(.8);
    thermometer_30.visible=false;
    thermometer_popup_20=this.game.add.sprite(991,430,'thermometer_popup_20');
    thermometer_popup_20.anchor.set(.5);
    thermometer_popup_20.scale.setTo(0);
    thermometer_popup_30=this.game.add.sprite(991,430,'thermometer_popup_30');
    thermometer_popup_30.anchor.set(.5);
    thermometer_popup_30.scale.setTo(0);
    

    tube=this.game.add.sprite(1205,330,'tube_animation','Tube.png');
    tube.animations.add('tube',[1,2,3,4,5,6,7,8,9,10],5, false, true);
    tube.anchor.setTo(.5,0);
    tube.scale.setTo(.9);
    tube.visible=false;
    cork=this.game.add.sprite(1003,565,'cork');
    cork.anchor.set(.5);
    cork.scale.set(.8);
    

    testube_with_tie_3=this.game.add.sprite(1005,170,'testube_with_tie_3');
    testube_with_tie_3.anchor.set(.5);
    testube_with_tie_3.scale.set(.75);
    testube_with_tie_3.visible=false;
    testube_with_tie_4=this.game.add.sprite(963,730,'testube_with_tie_4');
    testube_with_tie_4.anchor.set(.5);
    testube_with_tie_4.scale.set(.72);
    testube_with_tie_4.visible=false;
    
    flask=this.game.add.sprite(1000,750,'flask');
    flask.anchor.set(.5);
    flask1=this.game.add.sprite(1000,750,'chana','Chana_to_Flask0001.png');
    flask1.anchor.set(.5);
    flask1.visible=false;
    flask1.animations.add('chana',[7,8,9,10,11,12,13,14,15,16],5, false, true);

    cork1=this.game.add.sprite(1003,565,'cork_back');
    cork1.anchor.set(.5);
    cork1.scale.set(.8);
    c1=this.game.add.sprite(0,0,'cork');
    c1.anchor.set(.5);
    //c1.scale.set(.8);
    cork1.addChild(c1);
    cork1.events.onDragStart.add(function() {this.gameC(cork1)}, this);
    cork1.events.onDragStop.add(function() {this.gameC1(cork1)}, this);
    cork1.alpha=0;
    cork1.state="for_open";

    ////////////////////////////////
    // water_bottum=this.game.add.sprite(1400,810,'water_bottum');
    // water_bottum.anchor.set(.5);
    // water_bottum.scale.set(.75);
    // water_bottum.visible=false;
    // water_middle=this.game.add.sprite(1400,810,'water_middle');
    // water_middle.anchor.set(.5);
    // water_middle.scale.set(.75);
    // water_middle.visible=false;
    // water_top=this.game.add.sprite(1400,810,'water_top');
    // water_top.anchor.set(.5);
    // water_top.scale.set(.75);
    // water_top.visible=false;
    beaker=this.game.add.sprite(1400,810,'beaker_water','Beaker_water0001.png');
    beaker.anchor.set(.5);
    beaker.scale.set(.75);
    beaker.visible=false;

    beaker1=this.game.add.sprite(1600,750,'beaker_back');
    beaker1.anchor.set(.5);
    beaker1.scale.set(.75);
    b1=this.game.add.sprite(0,0,'beaker_water','Beaker_water0001.png');
    b1.anchor.set(.5);
    //b1.scale.set(.75);
    beaker1.addChild(b1);
    beaker1.events.onDragStart.add(function() {this.gameC(beaker1)}, this);
    beaker1.events.onDragStop.add(function() {this.gameC1(beaker1)}, this);
    this.game.physics.arcade.enable(beaker1);
    /////////////////////////////////

    chana=this.game.add.sprite(-2400,800,'chana','Chana_from_plate0001.png');
    chana.animations.add('chana',[0,1,2,,3,4,5],5, false, true);
    chana.anchor.set(.5);
    chana.events.onDragStart.add(function() {this.gameC(chana)}, this);
    chana.events.onDragStop.add(function() {this.gameC1(chana)}, this);

    ///////////////////////////////////////////////////
    lineGroup=this.game.add.group();
    rackGroup=this.game.add.group();
    testube_rack_back=this.game.add.sprite(300,800, 'stand_back');
    testube_rack_back.anchor.set(.5);
    testube_rack_back.scale.set(0.6);
    rackGroup.add(testube_rack_back);
     
     
     thread_testube=this.game.add.sprite(385,725, 'testube_with_tie');
     thread_testube.anchor.set(.5);
     thread_testube.scale.set(.75);
     rackGroup.add(thread_testube);
     
     

    testube_rack_front=this.game.add.sprite(300,800, 'stand_front');
    testube_rack_front.anchor.set(.5);
    testube_rack_front.scale.set(0.6);
    rackGroup.add(testube_rack_front);
    rackGroup.x=-2000;

    thread_testube_2=this.game.add.sprite(432,510, 'testube_with_tie_2');
     thread_testube_2.anchor.set(.5);
     thread_testube_2.scale.set(.75);
     thread_testube_2.visible=false;
     thread_testube_2.alpha=0;
     thread_testube_2.events.onDragStart.add(function() {this.gameC(thread_testube_2)}, this);
     thread_testube_2.events.onDragStop.add(function() {this.gameC1(thread_testube_2)}, this);
    this.game.physics.arcade.enable(thread_testube_2);


    //////////////////////////////////////////////////////////
    tubeGroup=this.game.add.group();
    tubeGroup.x=-2000;
    thermometer1=this.game.add.sprite(700,900, 'thermometer_20');
    thermometer1.anchor.set(.5);
    thermometer1.scale.set(.8);
    thermometer1.angle=60;
    tubeGroup.add(thermometer1);
    thermometer1.events.onDragStart.add(function() {this.gameC(thermometer1)}, this);
    thermometer1.events.onDragStop.add(function() {this.gameC1(thermometer1)}, this);
    this.game.physics.arcade.enable(thermometer1);

    // thermometer_needle1=this.game.add.sprite(0,-60, 'thermometer');
    // thermometer_needle1.anchor.set(.5);
    // thermometer_needle1.scale.setTo(1.2,1);
    // //thermometer_needle1.angle=90;
    // thermometer1.addChild(thermometer_needle1);

    tube1=this.game.add.sprite(300,830, 'tube_animation','Tube.png');
    tube1.anchor.setTo(.5,0);
    tube1.scale.setTo(.7,.5);
    tube1.angle=10;
    tubeGroup.add(tube1);
    tube1.events.onDragStart.add(function() {this.gameC(tube1)}, this);
    tube1.events.onDragStop.add(function() {this.gameC1(tube1)}, this);
    this.game.physics.arcade.enable(tube1);

    marker=this.game.add.sprite(1800,880, 'marker');
    marker.anchor.set(.5);
    marker.scale.set(.8);
    marker.scale.x*=-1;
    marker.events.onDragStart.add(function() {this.gameC(marker)}, this);
    marker.events.onDragStop.add(function() {this.gameC1(marker)}, this);
    this.game.physics.arcade.enable(marker);

    paste=this.game.add.sprite(-550,880, 'paste','Paste.png');
    paste.anchor.set(.5);
    paste.scale.set(.8);
    paste.scale.x*=-1;
    paste.events.onDragStart.add(function() {this.gameC(paste)}, this);
    paste.events.onDragStop.add(function() {this.gameC1(paste)}, this);
    this.game.physics.arcade.enable(paste);
    paste.animations.add('paste',[11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27],10, false, true);
    paste.animations.add('hand',[0,1,2,3,4,5,6,7,8,9],5, false, true);

    hand=this.game.add.sprite(915,625, 'paste','Hand0001.png');
    hand.anchor.set(.5);
    hand.scale.set(.8);
    hand.scale.x*=-1;
    hand.animations.add('hand',[0,1,2,3,4,5,6,7,8,9],5, false, true);
    hand.visible=false;
    //////////////////////////////////////////////////////////////////////////  
    clocks=this.game.add.sprite(550,260,"Clock");
    clocks.anchor.set(.5);
    clocks.scale.set(.7);
    clocksNeedleBig=this.game.add.sprite(550,265,"ClockClock_Needle_Big");
    clocksNeedleBig.anchor.set(.5,.85);
    clocksNeedleBig.scale.set(.7);
    clocksNeedleSmall=this.game.add.sprite(550,265,"Clock_Needle_Small");
    clocksNeedleSmall.anchor.set(.5,.8);
    clocksNeedleSmall.scale.set(.7);
    //clocks.visible=false;
    //clocksNeedleBig.visible=false;
    //clocksNeedleSmall.visible=false;
    clockFlag=false;
    clockNum=0;
    clockDelay=1;
    clockAngle=0;
    //////////////////////////////////////////////////////////////////////////////

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Lets perform the Experiment.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

    darkBg = this.game.add.graphics(0, 0);
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1",1);
    voice.play();
    dialog.text="Take 30-40 sprouted seeds of pea.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    
    ///////////////////////shortcut////////////////////
    
  },
  TakeSeed:function(){
    var tween1=this.game.add.tween(chana).to({x:400}, 800, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2",1);
      voice.play();
      dialog.text="Remove the cork from the flask.";
      cork1.inputEnabled = true;
      cork1.input.useHandCursor = true;
      cork1.input.enableDrag(true);
      cork1.xp=cork1.x;
      cork1.yp=cork1.y;
      this.game.physics.arcade.enable(cork1);
      arrow.x=cork1.x;
      arrow.y=cork1.y-70;//340
      arrow.visible=true;
    }.bind(this))
    //clockFlag=true;  
        
        // darkBg.visible=true;
        // darkBg.state="up";
        // dayFlag=true;
        // water_top.visible=true;
        // beaker.visible=true;
        // tween1=this.game.add.tween(darkBg).to({alpha:1}, 2000, Phaser.Easing.Linear.Out, true);
        // tween1.onComplete.add(function () {
        //   tween2=this.game.add.tween(darkBg).to({alpha:0}, 2000, Phaser.Easing.Linear.Out, true);
        //   tween2.onComplete.add(function () {
        //     console.log("/////////////");
        //   }.bind(this)) 
        // }.bind(this))  
       
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    arrow.visible=false;
    if(obj==cork1){
      if(cork1.state=="for_open"){
        cork1.alpha=1;
        arrow.x=collider.x;
        arrow.y=collider.y-30;//370
        arrow.visible=true;
        cork_back.visible=false;
        cork.visible=false;
      }else{
        arrow.x=collider.x;
        arrow.y=collider.y-30;//370
        arrow.visible=true;
      }
    }else if(obj==chana || obj==beaker1){
      arrow.x=collider.x;
      arrow.y=collider.y-30;//370
      arrow.visible=true;
      
    }else if(obj==thread_testube_2){
      thread_testube_2.alpha=1;
      arrow.x=collider.x;
      arrow.y=collider.y-30;//370
      arrow.visible=true;
      thread_testube.visible=false;
    }else if(obj==thermometer1){
      arrow.x=collider.x-10;
      arrow.y=collider.y-60;//370
      arrow.visible=true;
      thermometer1.angle=0;
    }else if(obj==tube1){
      arrow.x=collider.x+20;
      arrow.y=collider.y-60;//370
      arrow.visible=true;
      tube1.angle=0;
      tube1.scale.set(.9);
    
    }else if(obj==marker){
      arrow.x=collider.x;
      if(lineCount==1){
        arrow.y=collider.y-90;//370
      }else{
        arrow.y=collider.y-120;//370
      }
      arrow.visible=true;
      
    }else if(obj==paste){
      arrow.x=collider.x+20;
      arrow.y=collider.y-60;//370
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
    
  },
  match_Obj:function(){
    arrow.visible=false;
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==cork1){
      if(cork1.state=="for_open"){
        cork1.x=collider.x;
        cork1.y=collider.y;
        voice.destroy(true);
        voice=this.game.add.audio("step3",1);
        voice.play();
        dialog.text="Add the sprouted seeds into the conical flask.";
        chana.inputEnabled = true;
        chana.input.useHandCursor = true;
        chana.input.enableDrag(true);
        chana.xp=chana.x;
        chana.yp=chana.y;
        this.game.physics.arcade.enable(chana);
        arrow.x=chana.x+50;
        arrow.y=chana.y;//340
        arrow.visible=true;
        collider.x=flask.x;
        collider.y=550;
      }else{
        cork1.visible=false;
        cork_back.visible=true;
        cork.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step6",1);
        voice.play();
        dialog.text="Take one thermometer and a delivery tube.";
        this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeTube, this);
        

      }
    }else if(currentobj==chana){
      
      chana.x=collider.x+130;
      chana.y=collider.y-100;
      chana.animations.play('chana');
      chana.animations.currentAnim.onComplete.add(function(){
        chana.frameName="Chana_from_plate0007.png";
        flask.visible=false;
        flask1.visible=true;
        flask1.animations.play('chana');
        flask1.animations.currentAnim.onComplete.add(function(){
          chana.visible=false;
          voice.destroy(true);
          voice=this.game.add.audio("step4",1);
          voice.play();
          //dialog.y=40;
          dialog.text="Take some potassium hydroxide solution in the small test tube.";
          var tween1=this.game.add.tween(rackGroup).to({x:0}, 800, Phaser.Easing.Linear.Out, true);
          tween1.onComplete.add(function () {
            this.game.time.events.add(Phaser.Timer.SECOND*4,this.HangTesttube, this);
            
          }.bind(this))   
        }.bind(this))  
      }.bind(this))
    }else if(currentobj==thread_testube_2){
      thread_testube_2.visible=false;
      testube_with_tie_3.visible=true;
      var tween1=this.game.add.tween(testube_with_tie_3).to({y:630}, 800, Phaser.Easing.Linear.Out, true);
          tween1.onComplete.add(function () {
            testube_with_tie_3.visible=false;
            testube_with_tie_4.visible=true;
            voice.destroy(true);
            voice=this.game.add.audio("step5",1);
            voice.play();
            dialog.y=60;
            dialog.text="Now close the conical flask with the two hole rubber cork.";
            cork1.state="for_close";
            cork1.inputEnabled = true;
            cork1.input.useHandCursor = true;
            cork1.input.enableDrag(true);
            cork1.xp=cork1.x;
            cork1.yp=cork1.y-20;
            this.game.physics.arcade.enable(cork1);
            arrow.x=cork1.x;
            arrow.y=cork1.y-50;//340
            arrow.visible=true;
          }.bind(this)) 
    }else if(currentobj==thermometer1){
      thermometer1.visible=false;
      thermometer.visible=true;
      //thermometer_needle.visible=true;
      //tween1=this.game.add.tween(thermometer_needle).to({y:730}, 800, Phaser.Easing.Linear.Out, true);
      tween2=this.game.add.tween(thermometer).to({y:430}, 800, Phaser.Easing.Linear.Out, true);
        tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step8",1);
        voice.play();
        dialog.text="Place the delivery tube in another hole in the cork.";
        tube1.inputEnabled = true;
        tube1.input.useHandCursor = true;
        tube1.input.enableDrag(true);
        tube1.xp=tube1.x;
        tube1.yp=tube1.y;
        arrow.x=tube1.x;
        arrow.y=tube1.y-50;//340
        arrow.visible=true;
      }.bind(this)) 
    }else if(currentobj==tube1){
      tube1.visible=false;
      tube.visible=true;
      voice.destroy(true);
        voice=this.game.add.audio("step9",1);
        voice.play();
        dialog.text="Move the beaker to one end of the delivery tube.";
        beaker1.inputEnabled = true;
        beaker1.input.useHandCursor = true;
        beaker1.input.enableDrag(true);
        beaker1.xp=beaker1.x;
        beaker1.yp=beaker1.y;
        arrow.x=beaker1.x;
        arrow.y=beaker1.y-50;//340
        arrow.visible=true;
        collider.x=beaker.x;
        collider.y=beaker.y;
    }else if(currentobj==beaker1){
      beaker1.visible=false;
      beaker.visible=true;
      beaker_back.visible=true;
      // water_bottum.visible=true;
      // water_middle.visible=true;
      // water_top.visible=true;
      voice.destroy(true);
        voice=this.game.add.audio("step10",1);
        voice.play();
        dialog.y=40;
        dialog.text="Click on the delivery tube to insert one into the conical flask through \nthe hole in the cork and the other into the beaker with water. ";
        tube.inputEnabled = true;
        tube.input.useHandCursor = true;
        tube.events.onInputDown.add(this.tubeDown, this);
        
        arrow.x=tube.x;
        arrow.y=tube.y-50;//340
        arrow.visible=true;
        
    }else if(currentobj==marker){
      if(lineCount==1){
        marker.x=1450;
        marker.y=710;
        linex1=1370;
        lineFlag=true;
      }else{
        marker.x=1450;
        marker.y=730;
        linex1=1370;
        thermometer.visible=false;
        thermometer_30.visible=true;
        lineFlag=true;
      }
        
    }else if(currentobj==paste){
      paste.x=collider.x+85;
      paste.y=collider.y-100;
      paste.animations.play('paste');
      paste.animations.currentAnim.onComplete.add(function(){
        paste.animations.play('hand');
        paste.x= 915;
        paste.y=630;
        paste.animations.currentAnim.onComplete.add(function(){
          paste.reset(paste.xp,paste.yp);
          paste.frameName="Paste.png";
          voice.destroy(true);
          voice=this.game.add.audio("step11",1);
          voice.play();
          dialog.y=60;
          dialog.text="Mark the level of water in the beaker.";
          marker.inputEnabled = true;
          marker.input.useHandCursor = true;
          marker.input.enableDrag(true);
          marker.xp=marker.x;
          marker.yp=marker.y;
          arrow.x=marker.x;
          arrow.y=marker.y-50;//340
          arrow.visible=true;
          collider.x=beaker.x;
          collider.y=beaker.y;
        }.bind(this))  
      }.bind(this)) 
    }
    
  },
  HangTesttube:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_1",1);
    voice.play();
    //dialog.y=40;
    dialog.text="Hang the KOH solution in the conical flask using the thread.";
    
    thread_testube_2.visible=true;
    thread_testube_2.inputEnabled = true;
    thread_testube_2.input.useHandCursor = true;
    thread_testube_2.input.enableDrag(true);
    thread_testube_2.xp=thread_testube_2.x;
    thread_testube_2.yp=thread_testube_2.y;
    this.game.physics.arcade.enable(thread_testube_2);
    arrow.x=thread_testube.x+45;
    arrow.y=thread_testube.y-170;//340
    arrow.visible=true;
  },
  TakeTube:function(){
    var tween1=this.game.add.tween(rackGroup).to({x:-2000}, 800, Phaser.Easing.Linear.Out, true);
    var tween2=this.game.add.tween(tubeGroup).to({x:0}, 800, Phaser.Easing.Linear.Out, true);
      tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step7",1);
        voice.play();
        dialog.text="Place the thermometer in a hole in the cork.";
        thermometer1.inputEnabled = true;
        thermometer1.input.useHandCursor = true;
        thermometer1.input.enableDrag(true);
        thermometer1.xp=thermometer1.x;
        thermometer1.yp=thermometer1.y;
        arrow.x=thermometer1.x;
        arrow.y=thermometer1.y-50;//340
        arrow.visible=true;
      }.bind(this))   
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
  },
  tubeDown:function(){
    tube.inputEnabled = false;
    tube.input.useHandCursor = false;
    arrow.visible=false;
    tween1=this.game.add.tween(tube).to({y:420}, 800, Phaser.Easing.Linear.Out, true);
      tween1.onComplete.add(function () {
        //tube.frameName='Tube_animation0001.png';
        voice.destroy(true);
        voice=this.game.add.audio("step17",1);
        voice.play();
        dialog.y=60;
        dialog.text="Make the apparatus air-tight by applying Vaseline on the cork.";
        var tween1=this.game.add.tween(paste).to({x:550}, 800, Phaser.Easing.Linear.Out, true);
        tween1.onComplete.add(function () {
          paste.inputEnabled = true;
          paste.input.useHandCursor = true;
          paste.input.enableDrag(true);
          paste.xp=paste.x;
          paste.yp=paste.y;
          arrow.x=paste.x;
          arrow.y=paste.y-70;//340
          arrow.visible=true;
          collider.x=flask.x;
          collider.y=550;
        }.bind(this)) 
        /////////////////////////////
        
      }.bind(this))     
  },
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    if(lineFlag){
      marker.x+=5;
      linex1+=5;
      //1370//790
      
      if(lineCount==1){
        if(line1!=null){
          line1.destroy(true);
        }
        line1=this.drawVLine(1370,770,linex1,770);
      }else{
        if(line2!=null){
          line2.destroy(true);
        }
        line2=this.drawVLine(1370,790,linex1,790);
      }
      
      if(linex1>=1440){
        lineFlag=false;
        marker.reset(marker.xp,marker.yp);
        if(lineCount==1){
          voice.destroy(true);
          voice=this.game.add.audio("step12",1);
          voice.play();
          dialog.text="Now note that the temperature is 28\u00BA.";
          tween1=this.game.add.tween(thermometer_popup_20.scale).to({x:1.1, y:1.1}, 200, Phaser.Easing.Linear.Out, true);
          tween1.onComplete.add(function () {
            arrow.x=thermometer_popup_20.x;
            arrow.y=thermometer_popup_20.y-40;
            arrow.visible=true;
            this.game.time.events.add(Phaser.Timer.SECOND*4,this.popup20Out, this); 
          }.bind(this))
        }else{
          voice.destroy(true);
          voice=this.game.add.audio("step15",1);
          voice.play();
          dialog.y=40;
          dialog.text="Observe the temperature is 30° now and also note that the level of water \nin one end of delivery tube inserted in the beaker with water has risen. ";
          tween1=this.game.add.tween(thermometer_popup_30.scale).to({x:1.1, y:1.1}, 200, Phaser.Easing.Linear.Out, true);
          tween1.onComplete.add(function () {
            arrow.x=beaker.x+50;
            arrow.y=tube.y+120;
            arrow.angle=90;
            arrow.visible=true;
            this.game.time.events.add(Phaser.Timer.SECOND*10,this.popup30Out, this);
          }.bind(this)) 
        }
      }
    }
    if(dayFlag){
      clockNum++;
      if(clockNum>=2){//clockDelay
        clockNum=0;
        //clockDelay=10;
        clockAngle++;
        if(clockAngle%15==0){
          beaker.frame++;
          tube.frame++;
        }
        //console.log(clockAngle);
        clocksNeedleBig.angle+=4;//8
        clocksNeedleSmall.angle+=.3;//2.0;
        console.log(clocksNeedleSmall.angle);
        //water_top.scale.y-=.004;
        if(darkBg.state=="up"){
          darkBg.alpha+=.01;
          if(darkBg.alpha>=.98){
            darkBg.state="down";
          }
        }else{
          darkBg.alpha-=.01;
          if(darkBg.alpha<=.2){
            clocksNeedleBig.angle=0;
            clocksNeedleSmall.angle=55;
            darkBg.vi=false;
            dayFlag=false;
            voice.destroy(true);
            voice=this.game.add.audio("step15",1);
            voice.play();
            dialog.y=40;
            dialog.text="Observe the temperature is 30° now and also note that the level of water \nin one end of delivery tube inserted in the beaker with water has risen. ";
            tween1=this.game.add.tween(thermometer_popup_30.scale).to({x:1.1, y:1.1}, 200, Phaser.Easing.Linear.Out, true);
            tween1.onComplete.add(function () {
              arrow.x=beaker.x+50;
              arrow.y=tube.y+120;
              arrow.angle=90;
              arrow.visible=true;
              this.game.time.events.add(Phaser.Timer.SECOND*10,this.popup30Out, this);
            }.bind(this)) 
            // voice.destroy(true);
            // voice=this.game.add.audio("step14",1);
            // voice.play();
            // dialog.text="Mark the water level in the beaker again.";
            // dialog.y=60;
            // lineCount=2;
            // marker.inputEnabled = true;
            // marker.input.useHandCursor = true;
            // marker.input.enableDrag(true);
            // marker.xp=marker.x;
            // marker.yp=marker.y;
            // arrow.x=marker.x;
            // arrow.y=marker.y-50;//340
            // arrow.visible=true;
          }
         }
       }
    }
    
	},
  popup20Out:function(){
    arrow.visible=false;
    tween1=this.game.add.tween(thermometer_popup_20.scale).to({x:0, y:0}, 200, Phaser.Easing.Linear.Out, true);
      tween1.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step13",1);
        voice.play();
        dialog.y=40;
        dialog.text="Wait for 2 hours, and observe the level of water and \nthe thermometer reading.";
        //clocks.visible=true;
        //clocksNeedleBig.visible=true;
        //clocksNeedleSmall.visible=true;
        clockFlag=true;  
        darkBg.state="up";
        dayFlag=true;
        //water_top.visible=true;
        //beaker.visible=true;
        //darkBg.visible=true;
        // tween1=this.game.add.tween(darkBg).to({alpha:1}, 2000, Phaser.Easing.Linear.Out, true);
        // tween1.onComplete.add(function () {
        //   tween2=this.game.add.tween(darkBg).to({alpha:0}, 2000, Phaser.Easing.Linear.Out, true);
        //   tween2.onComplete.add(function () {
        //     voice.destroy(true);
        //     voice=this.game.add.audio("step14",1);
        //     voice.play();
        //     dialog.text="Mark the water level in the beaker again.";
        //     dialog.y=60;
        //     lineCount=2;
        //     marker.inputEnabled = true;
        //     marker.input.useHandCursor = true;
        //     marker.input.enableDrag(true);
        //     marker.xp=marker.x;
        //     marker.yp=marker.y;
        //     arrow.x=marker.x;
        //     arrow.y=marker.y-50;//340
        //     arrow.visible=true;
        //   }.bind(this)) 
        // }.bind(this))  
      }.bind(this))  
  },
  popup30Out:function(){
    arrow.visible=false;
    tween1=this.game.add.tween(thermometer_popup_30.scale).to({x:0, y:0}, 200, Phaser.Easing.Linear.Out, true);
      tween1.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step16",1);
        voice.play();
        dialog.y=60;
        dialog.text="Click on the next button to see the observations.";
        play.visible=true;
        //clocks.visible=true;
        //clocksNeedleBig.visible=true;
        //clocksNeedleSmall.visible=true;
        //clockFlag=true;  
      }.bind(this))  
  },
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==cork1){
        if(cork1.state=="for_open"){
          arrow.x=cork1.x;
          arrow.y=cork1.y-70;//325
          cork1.alpha=0;
          cork_back.visible=true;
          cork.visible=true;
        }else{
          arrow.x=cork1.x;
          arrow.y=cork1.y-70;//325
        }
      }else if(currentobj==chana){
        arrow.x=chana.x+50;
        arrow.y=chana.y;//325
        
      }else if(currentobj==thread_testube_2){
        arrow.x=thread_testube.x+45;
        arrow.y=thread_testube.y-170;//325
        thread_testube_2.alpha=0;
        thread_testube.visible=true;
      }else if(currentobj==thermometer1){
        arrow.x=thermometer1.x;
        arrow.y=thermometer1.y-50;//325
        thermometer1.angle=60;
      }else if(currentobj==tube1){
        arrow.x=tube1.x;
        arrow.y=tube1.y-50;//325
        tube1.scale.setTo(.7,.5);
        tube1.angle=10;
      }else if(currentobj==beaker1){
        arrow.x=beaker1.x;
        arrow.y=beaker1.y-50;//325
      }else if(currentobj==marker){
        arrow.x=marker.x;
        arrow.y=marker.y-50;//325
      }else if(currentobj==paste){
        arrow.x=paste.x;
        arrow.y=paste.y-50;//325
      }
      
      arrow.visible=true;
      currentobj=null;
    }
    
  },
	NextExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step20",1);
    voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
