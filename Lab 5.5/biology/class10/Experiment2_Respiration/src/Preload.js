var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Respiration//////////////////////////////////////////////////////////
this.game.load.image('flask','assets/Respiration/Flask.png');
this.game.load.image('flask_back','assets/Respiration/Flask_back.png');
this.game.load.image('cork','assets/Respiration/Cork.png');
this.game.load.image('cork_back','assets/Respiration/Cork_back.png');
this.game.load.image('beaker','assets/Respiration/Beaker.png');
this.game.load.image('beaker1','assets/Respiration/Beaker_with_water.png');
this.game.load.image('beaker_back','assets/Respiration/Beaker_back.png');
this.game.load.image('stand_back','assets/Respiration/Testube_stand_Back.png');
this.game.load.image('stand_front','assets/Respiration/Testube_stand_front.png');
this.game.load.image('testube_with_tie','assets/Respiration/Testube_with_tie.png');
this.game.load.image('testube_with_tie_2','assets/Respiration/Testube_with_tie_2.png');
this.game.load.image('testube_with_tie_3','assets/Respiration/Testube_with_tie_3.png');
this.game.load.image('testube_with_tie_4','assets/Respiration/Testube_with_tie_4.png');

this.game.load.image('thermometer_popup_30','assets/Respiration/Termometer_popup_30.png');
this.game.load.image('thermometer_popup_20','assets/Respiration/Termometer_popup_20.png');
this.game.load.image('thermometer_30','assets/Respiration/Thermometer_30.png');
this.game.load.image('thermometer_20','assets/Respiration/Thermometer_20.png');
this.game.load.image('thermometer','assets/Respiration/Thermometer.png');
this.game.load.image('thermometer_needle','assets/Respiration/Thermometer_needle.png');
this.game.load.image('tube','assets/Respiration/Tube.png');
this.game.load.image('water_bottum','assets/Respiration/Water_bottum.png');
this.game.load.image('water_middle','assets/Respiration/Water_middle.png');
this.game.load.image('water_top','assets/Respiration/Water_Top.png');
this.game.load.image('marker','assets/Respiration/Marker.png');



this.game.load.atlasJSONHash('paste', 'assets/Respiration/Paste.png', 'assets/Respiration/Paste.json');
this.game.load.atlasJSONHash('beaker_water', 'assets/Respiration/Beaker_water.png', 'assets/Respiration/Beaker_water.json');
this.game.load.atlasJSONHash('tube_animation', 'assets/Respiration/Tube_animation.png', 'assets/Respiration/Tube_animation.json');
this.game.load.atlasJSONHash('chana', 'assets/Respiration/chana.png', 'assets/Respiration/chana.json');
this.game.load.atlasJSONHash('arrow', 'assets/Respiration/arrow.png', 'assets/Respiration/arrow.json');

      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      
      this.game.load.audio('step0','assets/audio/step0.mp3');
      this.game.load.audio('step1','assets/audio/step1.mp3');
      this.game.load.audio('step2','assets/audio/step2.mp3');
      this.game.load.audio('step3','assets/audio/step3.mp3');
      this.game.load.audio('step4','assets/audio/step4.mp3');
      this.game.load.audio('step4_1','assets/audio/step4_1.mp3');
      this.game.load.audio('step5','assets/audio/step5.mp3');
      this.game.load.audio('step6','assets/audio/step6.mp3');
      this.game.load.audio('step7','assets/audio/step7.mp3');
      this.game.load.audio('step8','assets/audio/step8.mp3');
      this.game.load.audio('step9','assets/audio/step9.mp3');
      this.game.load.audio('step10','assets/audio/step10.mp3');
      this.game.load.audio('step11','assets/audio/step11.mp3');
      this.game.load.audio('step12','assets/audio/step12.mp3');
      this.game.load.audio('step13','assets/audio/step13.mp3');
      this.game.load.audio('step14','assets/audio/step14.mp3');
      this.game.load.audio('step15','assets/audio/step15.mp3');
      this.game.load.audio('step16','assets/audio/step16.mp3');
      this.game.load.audio('step17','assets/audio/step17.mp3');

      

      
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

