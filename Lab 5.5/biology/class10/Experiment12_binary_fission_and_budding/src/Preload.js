var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var theory_completed;
  var theory_Scene;
  var observation_completed;
  var observation_Scene;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Budding//////////////////////////////////////////////////////////
      this.game.load.image('bg_black','assets/Bg_black.png');
      this.game.load.image('bg_top','assets/Bg_top.png'); 
      this.game.load.image('microscope','assets/Budding/microscope.png'); 

      this.game.load.image('microscope_top_view','assets/Budding/Mocroscope_top_view.png');
        //this.game.load.image('Muscle_tissue','assets/Budding/Muscle_tissue.png');
        //this.game.load.image('Nueron_tissue','assets/Budding/Nueron_tissue.png');
        
        
        this.game.load.image('microscope_holder_right','assets/Budding/mocroscope_right_lock.png');
        this.game.load.image('microscope_holder_left','assets/Budding/mocroscope_left_lock.png');
        
        this.game.load.image('Circle_Blue','assets/Budding/Circle_Blue.png');
        this.game.load.image('Circle_Red','assets/Budding/Circle_Red.png');
        
        
      this.game.load.atlasJSONHash('slide_views', 'assets/Budding/slide_views.png', 'assets/Budding/slide_views.json');
      this.game.load.atlasJSONHash('arrow', 'assets/Budding/arrow.png', 'assets/Budding/arrow.json');
      this.game.load.atlasJSONHash('eyePiece_rotating_sprite', 'assets/Budding/eyePiece_rotating_sprite.png', 'assets/Budding/eyePiece_rotating_sprite.json');
      this.game.load.atlasJSONHash('Amoeba', 'assets/Budding/Amoeba.png', 'assets/Budding/Amoeba.json');
      this.game.load.atlasJSONHash('Yeast', 'assets/Budding/Yeast.png', 'assets/Budding/Yeast.json');
      
      
      this.game.load.atlasJSONHash('fission_1', 'assets/Budding/fission_1.png', 'assets/Budding/fission_1.json');
      this.game.load.atlasJSONHash('fission_2', 'assets/Budding/fission_2.png', 'assets/Budding/fission_2.json');
      this.game.load.atlasJSONHash('fission_3', 'assets/Budding/fission_3.png', 'assets/Budding/fission_3.json');
      this.game.load.atlasJSONHash('fission_4', 'assets/Budding/fission_4.png', 'assets/Budding/fission_4.json');
      this.game.load.atlasJSONHash('fission_5', 'assets/Budding/fission_5.png', 'assets/Budding/fission_5.json');
      
      this.game.load.atlasJSONHash('budding_1', 'assets/Budding/budding_1.png', 'assets/Budding/budding_1.json');
      this.game.load.atlasJSONHash('budding_2', 'assets/Budding/budding_2.png', 'assets/Budding/budding_2.json');
      this.game.load.atlasJSONHash('budding_3', 'assets/Budding/budding_3.png', 'assets/Budding/budding_3.json');
      this.game.load.atlasJSONHash('budding_4', 'assets/Budding/budding_4.png', 'assets/Budding/budding_4.json');
      this.game.load.atlasJSONHash('budding_5', 'assets/Budding/budding_5.png', 'assets/Budding/budding_5.json');

      ///////////////materials//////////////////////
      this.game.load.image('m_microscope','assets/Budding/m_microscope.png');
      this.game.load.image('Permenat_Slides','assets/Budding/Permenat_Slides.png');

      this.game.load.image('image1','assets/Budding/image1.png');
      this.game.load.image('image1_1','assets/Budding/image1_1.png');
      this.game.load.image('image2','assets/Budding/image2.png');
      this.game.load.image('image2_1','assets/Budding/image2_1.png');

      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Observation_3','assets/audio/Observation_3.mp3');
      this.game.load.audio('Observation_4','assets/audio/Observation_4.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      
      
      this.game.load.audio('step1_0','assets/audio/step1_0.mp3');
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');
      this.game.load.audio('step1_7','assets/audio/step1_7.mp3');
      this.game.load.audio('step1_8','assets/audio/step1_8.mp3');
      this.game.load.audio('step1_9','assets/audio/step1_9.mp3');
      this.game.load.audio('step1_10','assets/audio/step1_10.mp3');
      this.game.load.audio('step1_11','assets/audio/step1_11.mp3');
      this.game.load.audio('step1_12','assets/audio/step1_12.mp3');
      this.game.load.audio('step1_13','assets/audio/step1_13.mp3');
      this.game.load.audio('step1_14','assets/audio/step1_14.mp3');
      
      this.game.load.audio('step2_0','assets/audio/step2_0.mp3');
      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step2_5','assets/audio/step2_5.mp3');
      this.game.load.audio('step2_6','assets/audio/step2_6.mp3');
      this.game.load.audio('step2_7','assets/audio/step2_7.mp3');
      this.game.load.audio('step2_8','assets/audio/step2_8.mp3');
      this.game.load.audio('step2_9','assets/audio/step2_9.mp3');
      this.game.load.audio('step2_10','assets/audio/step2_10.mp3');
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	  hotflag=1;
      ip = location.host; 
      theory_completed=false;
      theory_Scene=1;
      observation_completed=false;
      observation_Scene=1;
      
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_2");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

