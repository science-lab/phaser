var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;

var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
var tissue;
var circle_bg;
var slideNo;
var currentScale;
}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;
tissue=null;
bg= this.game.add.sprite(0, 0,'bg');

bgTopView= this.game.add.sprite(0,0,'bg_top');
bgTopView.scale.setTo(1,1);
bgTopView.visible=false;

microscopeTopview=this.game.add.sprite(900,480,'microscope_top_view');
microscopeTopview.scale.setTo(1.2,1.2);
microscopeTopview.anchor.setTo(.5,.4);
microscopeTopview.visible=false;

bgBlack= this.game.add.sprite(0,0,'bg_black');
bgBlack.scale.setTo(1,1);
bgBlack.alpha=0;
bgBlack.visible=false;

Circle_Red= this.game.add.sprite(902,494,'Circle_Red');
Circle_Red.anchor.setTo(.5,.5);
Circle_Red.scale.set(0);
Circle_Red.alpha=0;


fission_1=this.game.add.sprite(902,494,'fission_1','Single_Ameba0001.png');
fission_1.anchor.setTo(.5,.5);
fission_1.scale.set(0);
fission_1.alpha=0;
 fission_1.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1],4, true, true);
 //fission_1.animations.play('anim');
 tissue=fission_1;
fission_2=this.game.add.sprite(902,494,'fission_2','Elongating_Nucleus0001.png');
fission_2.anchor.setTo(.5,.5);
fission_2.scale.set(0);
fission_2.alpha=0;
fission_2.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],4, false, true);
fission_2.animations.add('anim2',[20,21,22,23,24,25,26,27,28,29],3, true, true);
//fission_2.animations.play('anim');

fission_3=this.game.add.sprite(902,494,'fission_3','Elongating_parent_cell0001.png');
fission_3.anchor.setTo(.5,.5);
fission_3.scale.set(0);
fission_3.alpha=0;
fission_3.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],4, false, true);
fission_3.animations.add('anim2',[20,21,22,23,24,25,26,27,28,29],3, true, true);
// fission_3.animations.play('anim');

fission_4=this.game.add.sprite(902,494,'fission_4','Nucler_Division0001.png');
fission_4.anchor.setTo(.5,.5);
fission_4.scale.set(0);
fission_4.alpha=0;
fission_4.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],4, false, true);
fission_4.animations.add('anim2',[20,21,22,23,24,25,26,27,28,29],3, true, true);
// fission_4.animations.play('anim');

fission_5=this.game.add.sprite(902,494,'fission_5','New_Ameba0001.png');
fission_5.anchor.setTo(.5,.5);
fission_5.scale.set(0);
fission_5.alpha=0;
fission_5.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],5, false, true);
fission_5.animations.add('anim2',[20,21,22,23,24,25,26,27,28,29],3, true, true);
// fission_5.animations.play('anim');


circle1 = this.game.add.graphics(0, 0);
////circle1.lineStyle(8, 0xffd900, 1);
circle1.beginFill(0xFF0000, .3);
circle1.drawCircle(902, 494, 430);
//circle1.scale.set(0.5);
Circle_Red.mask=circle1;
circle1.visible=false;

currentScale=0;

slideNo=1;
/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/

voice=this.game.add.audio("step1_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
prev_btn.scale.setTo(-.7,.7);
prev_btn.inputEnabled = true;
//prev_btn.input.priorityID = 3;
prev_btn.input.useHandCursor = true;
prev_btn.events.onInputDown.add(this.backToExp, this);
prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Procedure_2", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    microscopeGroup=this.game.add.group();
    microscopeGroup.x=-2000;
    microscope=this.game.add.sprite(700,220,'microscope');
    microscope.scale.setTo(-1,1);
    microscopeGroup.add(microscope);
    micro_click=this.game.add.sprite(460,330,'collider');
    micro_click.scale.y=2.75;
    micro_click.anchor.set(.5);
    micro_click.alpha=0;
    microscopeGroup.add(micro_click);
    micro_click.inputEnabled = true;
    micro_click.input.useHandCursor = true;
    micro_click.events.onInputDown.add(this.clickOnEyePiece, this);
    micro_click.visible=false;
    eyePiece=this.game.add.sprite(575,575,'eyePiece_rotating_sprite','Microscope_Lenz0015.png');
    eyePiece.scale.setTo(-1,1);
    eyePiece.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
    eyePiece.animations.add('anim2',[14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],10, false, true);
    eyePiece.magnification=45;
    //eyePiece.animations.play('anim');
    microscopeGroup.add(eyePiece);
    

    glass_slide_view=this.game.add.sprite(450,740,'slide_views',"slide_view_3.png");
    glass_slide_view.scale.set(-.6,.6);
    glass_slide_view.anchor.set(.5);
    glass_slide_view.events.onDragStart.add(function() {this.gameC(glass_slide_view)}, this);
    glass_slide_view.events.onDragStop.add(function() {this.gameC1(glass_slide_view)}, this);
    this.game.physics.arcade.enable(glass_slide_view);
    microscopeGroup.add(glass_slide_view);
    glass_slide_view.visible=false;



    label_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    label_box1.beginFill(0xE67E22,1);//9B59B6
    label_box1.drawRect(850, 800, 300, 60);
    label_1=this.game.add.text(940,808,"Amoeba",lablel_fontStyle);//870,930
    microscopeGroup.add(label_box1);
    microscopeGroup.add(label_1);

    // label_box2 = this.game.add.graphics(0, 0);
    // //box1.lineStyle(5,0xffffff,.6);
    // label_box2.beginFill(0xE67E22,1);//9B59B6
    // label_box2.drawRect(1250, 800, 300, 60);
    // label_2=this.game.add.text(1305,808,"Yeast",lablel_fontStyle);
    // microscopeGroup.add(label_box2);
    // microscopeGroup.add(label_2);

    glass_slide_1=this.game.add.sprite(1000,900,'slide_views',"slide_view_1.png");
    glass_slide_1.scale.set(.8);
    glass_slide_1.anchor.set(.5);
    microscopeGroup.add(glass_slide_1);
    glass_slide_1.events.onDragStart.add(function() {this.gameC(glass_slide_1)}, this);
    glass_slide_1.events.onDragStop.add(function() {this.gameC1(glass_slide_1)}, this);
    this.game.physics.arcade.enable(glass_slide_1);
    

    // glass_slide_2=this.game.add.sprite(1400,900,'slide_views',"slide_view_1.png");
    // glass_slide_2.scale.set(.8);
    // glass_slide_2.anchor.set(.5);
    // microscopeGroup.add(glass_slide_2);
    // glass_slide_2.events.onDragStart.add(function() {this.gameC(glass_slide_2)}, this);
    // glass_slide_2.events.onDragStop.add(function() {this.gameC1(glass_slide_2)}, this);
    // this.game.physics.arcade.enable(glass_slide_2);
    
    
    

    microscopeLeftHolder=this.game.add.sprite(585,700,'microscope_holder_left');
    microscopeLeftHolder.scale.setTo(-1,1);
    microscopeGroup.add(microscopeLeftHolder);
    microscopeRightHolder=this.game.add.sprite(450,740,'microscope_holder_right');
    microscopeRightHolder.scale.setTo(-1,1);
    microscopeGroup.add(microscopeRightHolder);

    boxGroup=this.game.add.group();
    box = this.game.add.graphics(0, 0);
    box.lineStyle(5,0xE67E22,.6);
    box.beginFill(0x000000,1);//9B59B6
    box.drawRect(30, 200, 325, 240);
    testHead=this.game.add.text(40,210,"Select lens:",headfontStyle);
    box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    box1.beginFill(0xffffff,1);//9B59B6
    box1.drawRect(40, 275, 300, 50);
    box1.inputEnabled = true;
    box1.input.useHandCursor = true;
    box1.events.onInputDown.add(this.clickOnbox1, this);
    box2 = this.game.add.graphics(0, 0);
    //box2.lineStyle(5,0xffffff,.6);
    box2.beginFill(0xffffff,1);//9B59B6
    box2.drawRect(40, 350, 300, 50);
    box2.inputEnabled = true;
    box2.input.useHandCursor = true;
    box2.events.onInputDown.add(this.clickOnbox2, this);
    boxR = this.game.add.graphics(0, 0);
    //boxR.lineStyle(5,0xffffff,.6);
    boxR.beginFill(0x00ff00,.6);//9B59B6
    boxR.drawRect(0, 0, 300, 50);
    boxR.x=40;
    boxR.y=275;
    test1=this.game.add.text(50,280,"10X (Low power)",fontStyle_b);
    test2=this.game.add.text(50,355,"45X (High power)",fontStyle_b);

    boxGroup.add(box);
    boxGroup.add(box1);
    boxGroup.add(box2);
    boxGroup.add(boxR);
    boxGroup.add(testHead);
    boxGroup.add(test1);
    boxGroup.add(test2);
    boxGroup.visible=false;

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe the binary fission in Amoeba under the microscope.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

    darkBg = this.game.add.graphics(0, 0);
    
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_1",1);
    voice.play();
    dialog.text="Take a compound microscope and a slide of Amoeba.";
    tween1=this.game.add.tween(microscopeGroup).to( {x:0}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.ExplainMicroscope, this);
      
    }.bind(this));
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
  clickOnbox1:function(){
    if(eyePiece.magnification==45){
      eyePiece.magnification=10;
      boxR.y=275;
      tween1=this.game.add.tween(tissue.scale).to( {x:1,y:1}, 2000, Phaser.Easing.Linear.Out, true);
    }
  },
  clickOnbox2:function(){
    if(eyePiece.magnification==10){
      eyePiece.magnification=45;
      boxR.y=350;
      tween1=this.game.add.tween(tissue.scale).to( {x:1.6,y:1.6}, 2000, Phaser.Easing.Linear.Out, true);
     
    }
  },
  ExplainMicroscope:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_2",1);
    voice.play();
    dialog.y=40;
    dialog.text="This is a compound microscope. We can change the magnification of \nthis lens to 10X and 45X by clicking on it.";
    arrow.x=eyePiece.x-100;
      arrow.y=eyePiece.y-50;
      arrow.visible=true;  
      arrow.state="lensRotation" ; 
      eyePiece.inputEnabled = true;
    eyePiece.input.useHandCursor = true;
    eyePiece.events.onInputDown.add(this.RotateEyePiece, this);
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    arrow.visible=false;
    if(obj==glass_slide_1){
      obj.frameName="slide_view_2.png";
      collider.x=470;
      collider.y=740;
      if(slideNo<3){
        arrow.x=collider.x;
        arrow.y=collider.y-50;//370
        arrow.visible=true;
      }
    }else if(obj==glass_slide_view){
         collider.x=glass_slide_1.x;
        collider.y=glass_slide_1.y;
      
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
    
  },
  match_Obj:function(){
    arrow.visible=false;
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==glass_slide_1){
      
        label_box1.visible=false;
        label_1.visible=false;
        tissue=fission_1;
        circle_bg=Circle_Red;
        if(glass_slide_view.visible){
          label_box2.visible=true;
          label_2.visible=true;
          glass_slide_2.visible=true;
        }
      
      tissue.scale.set(0);
      currentobj.visible=false;
      currentobj.reset(currentobj.xp,currentobj.yp);
      currentobj.visible=false;
      currentobj.frameName="slide_view_1.png";
      
      glass_slide_view.visible=true;
      if(slideNo<3){
        voice.destroy(true);
        voice=this.game.add.audio("step1_4",1);
        voice.play();
        dialog.text="Click on the eyepiece of the microscope to see the structure of the cell.";
        arrow.x=micro_click.x-80;
        arrow.y=micro_click.y;
        arrow.angle=-90;
        arrow.visible=true;
        micro_click.visible=true;
      }
      
    }else if(currentobj==glass_slide_view){
      currentobj.visible=false;
      currentobj.reset(currentobj.xp,currentobj.yp);
      currentobj.visible=false;
      currentobj.frameName="slide_view_3.png";
     
        glass_slide_1.visible=true;
        label_1.visible=true;
        label_box1.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step1_14",1);
        voice.play();
        dialog.text="Click on the next button to see the budding in yeast.";
        play.visible=true;
      
    }
    
  },
  RotateEyePiece:function(){
    if(arrow.state=="lensRotation"){
      arrow.visible=false;
      arrow.state="";
      voice.destroy(true);
      voice=this.game.add.audio("step1_3",1);
      voice.play();
      dialog.y=60;
      dialog.text="Place the slide of Amoeba to the microscope.";
      arrow.x=glass_slide_1.x;
      arrow.y=glass_slide_1.y-50;
      arrow.visible=true;
      glass_slide_1.inputEnabled = true;
      glass_slide_1.input.useHandCursor = true;
      glass_slide_1.input.enableDrag(true);
      glass_slide_1.xp=glass_slide_1.x;
      glass_slide_1.yp=glass_slide_1.y;
    }
    if(eyePiece.magnification==10){
      eyePiece.animations.play('anim1');
      eyePiece.magnification=45;
    }else{
      eyePiece.animations.play('anim2');
      eyePiece.magnification=10;
    }
  },
  clickOnEyePiece:function()
  {
    if(glass_slide_view.visible){
      arrow.visible=false;
      bg.visible=false;
      microscopeGroup.visible=false;
      

      bgTopView.visible=true;
      microscopeTopview.visible=true;
      
      this.game.time.events.add(Phaser.Timer.SECOND*1,this.microscopicViewTweens, this);
    }
  },
  microscopicViewTweens:function()
  {

    microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
    bgBlack.visible=true;
    bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Linear.Out, true);
    tween1=this.game.add.tween(circle_bg.scale).to( {x:1,y:1}, 2000, Phaser.Easing.Linear.Out, true);
    tissue.visible=true;
    if(eyePiece.magnification==10){
       tween2=this.game.add.tween(tissue.scale).to( {x:1,y:1}, 2000, Phaser.Easing.Linear.Out, true);
       tween2.onComplete.add(function () {
        voice.destroy(true);
        voice=this.game.add.audio("step1_5",1);
        voice.play();
        dialog.y=60;
        
          dialog.text="Now let's see the structure of Amoeba.";
          tissue.animations.play('anim');
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.showSelectBox, this);
        
       }.bind(this));  
    }else{
      tween2=this.game.add.tween(tissue.scale).to( {x:1,y:1}, 2000, Phaser.Easing.Linear.Out, true);
      tween2.onComplete.add(function () {
        tween3=this.game.add.tween(tissue.scale).to( {x:1.6,y:1.6}, 2000, Phaser.Easing.Linear.Out, true);
        tween3.onComplete.add(function () {
          voice.destroy(true);
          voice=this.game.add.audio("step1_5",1);
          voice.play();
          dialog.y=60;
          dialog.text="Now let's see the structure of Amoeba.";
          tissue.animations.play('anim');
          this.game.time.events.add(Phaser.Timer.SECOND*5,this.showSelectBox, this);
          
        }.bind(this));  
      }.bind(this)); 
    }
    tween4=this.game.add.tween(circle_bg).to( {alpha:.5}, 2500, Phaser.Easing.Linear.Out, true);
    twee5=this.game.add.tween(tissue).to( {alpha:1}, 2500, Phaser.Easing.Linear.Out, true);
    
  },
  showSelectBox:function(){
    if(eyePiece.magnification==10){
      boxR.y=275;
    }else{
      boxR.y=350;
    }
    boxGroup.visible=true;
    if(slideNo<3){
      voice.destroy(true);
      voice=this.game.add.audio("step1_6",1);
      voice.play();
      dialog.text="Select the lens options to observe the cell structure in low/high power.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.goFission1, this);
    }
  },
  goFission1:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_7",1);
    voice.play();
    dialog.y=40;
    dialog.text="Now lets see different steps of Binary fission in amoeba. Initially we have \nthe parent amoeba cell. ";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.goFission2, this);
  },
  goFission2:function(){
    tissue.scale.set(0);
    tissue.alpha=0;
    tissue=fission_2;
    tissue.alpha=1;
    tissue.scale.set(currentScale);
    // if(eyePiece.magnification==10){
    //   tissue.alpha=1;
    //   tissue.scale.set(1);
    // }else{
    //   tissue.alpha=1;
    //   tissue.scale.set(1.5);
    // }
    fission_2.animations.play('anim');
    voice.destroy(true);
    voice=this.game.add.audio("step1_8",1);
    voice.play();
    dialog.y=60;
    dialog.text="The nucleus of the parent amoeba then starts to elongate.";
    fission_2.animations.currentAnim.onComplete.add(function(){
      fission_2.animations.play('anim2');
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.goFission3, this);
     }.bind(this)); 
  },
  goFission3:function(){
    tissue.scale.set(0);
    tissue.alpha=0;
    tissue=fission_3;
    tissue.alpha=1;
    tissue.scale.set(currentScale);
    // if(eyePiece.magnification==10){
    //   tissue.alpha=1;
    //   tissue.scale.set(1);
    // }else{
    //   tissue.alpha=1;
    //   tissue.scale.set(1.5);
    // }
    fission_3.animations.play('anim');
    voice.destroy(true);
    voice=this.game.add.audio("step1_9",1);
    voice.play();
    dialog.y=60;
    dialog.text="Also note that the parent cell elongates.";
    fission_3.animations.currentAnim.onComplete.add(function(){
      fission_3.animations.play('anim2');
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.goFission4, this);
     }.bind(this)); 
  },
  goFission4:function(){
    tissue.scale.set(0);
    tissue.alpha=0;
    tissue=fission_4;
    tissue.alpha=1;
    tissue.scale.set(currentScale);
    // if(eyePiece.magnification==10){
    //   tissue.alpha=1;
    //   tissue.scale.set(1);
    // }else{
    //   tissue.alpha=1;
    //   tissue.scale.set(1.5);
    // }
    fission_4.animations.play('anim');
    voice.destroy(true);
    voice=this.game.add.audio("step1_10",1);
    voice.play();
    dialog.y=40;
    dialog.text="Now you can see that the nucleus divides into 2 nuclei, this is called \nnuclear division.";
    fission_4.animations.currentAnim.onComplete.add(function(){
      fission_4.animations.play('anim2');
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.goFission5, this);
     }.bind(this)); 
  },
  goFission5:function(){
    tissue.scale.set(0);
    tissue.alpha=0;
    tissue=fission_5;
    tissue.alpha=1;
    tissue.scale.set(currentScale);
    // if(eyePiece.magnification==10){
    //   tissue.alpha=1;
    //   tissue.scale.set(1);
    // }else{
    //   tissue.alpha=1;
    //   tissue.scale.set(1.5);
    // }
    fission_5.animations.play('anim');
    voice.destroy(true);
    voice=this.game.add.audio("step1_11",1);
    voice.play();
    dialog.y=40;
    dialog.text="Finally the cytoplasm also divides. This divides the parent cell into two \ndifferent daughter cells as you can see.";
    fission_5.animations.currentAnim.onComplete.add(function(){
      fission_5.animations.play('anim2');
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.showBackBtn, this);
     }.bind(this)); 
  },
  showBackBtn:function(){
    prev_btn.visible=true;
    if(slideNo<3){
      voice.destroy(true);
      voice=this.game.add.audio("step1_12",1);//"step6"
      voice.play();
      dialog.y=60;
      dialog.text="Click on the back button to go back to the experiment.";
    }
  },
  backToExp:function(){
    bgTopView.visible=false;
    microscopeTopview.visible=false;
    microscopeTopview.scale.setTo(1.2,1.2);
    bgBlack.alpha=0;
    bgBlack.visible=false;
    tissue.scale.set(0);
    tissue.alpha=0;
    circle_bg.scale.set(0);
    circle_bg.alpha=0;
    circle1.visible=false;
    boxGroup.visible=false;
    prev_btn.visible=false;
    bg.visible=true;
    eyePiece.magnification=10;
    eyePiece.frameName="Microscope_Lenz0001.png";
    microscopeGroup.visible=true;
    arrow.angle=0;
    ///////////////////
    if(slideNo==1){
      voice.destroy(true);
      voice=this.game.add.audio("step1_13",1);//"step7"
      voice.play();
      dialog.y=60;
      dialog.text="Remove the slide from the microscope.";
      arrow.x=glass_slide_view.x;
      arrow.y=glass_slide_view.y-50;
      
      arrow.visible=true;
      glass_slide_view.inputEnabled = true;
      glass_slide_view.input.useHandCursor = true;
      glass_slide_view.input.enableDrag(true);
      glass_slide_view.xp=glass_slide_view.x;
      glass_slide_view.yp=glass_slide_view.y;
      collider.x=glass_slide_1.x;
      collider.y=glass_slide_1.y;
      slideNo=2;
    }else if(slideNo>=2){
      
      glass_slide_view.inputEnabled = true;
      glass_slide_view.input.useHandCursor = true;
      glass_slide_view.input.enableDrag(true);
      
      glass_slide_1.inputEnabled = true;
      glass_slide_1.input.useHandCursor = true;
      glass_slide_1.input.enableDrag(true);
      
      
      play.visible=true;
      if(slideNo<3){
        voice.destroy(true);
        voice=this.game.add.audio("step1_14",1);
        voice.play();
        dialog.y=60;
        dialog.text="Click on the next button to see the budding in yeast.";
      }
      slideNo=3;
    }
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    interval++;
    currentScale=tissue.scale.x;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==glass_slide_1){
        currentobj.frameName="slide_view_1.png";
          arrow.x=currentobj.x;
          arrow.y=currentobj.y-50;//325
        
      }else if(currentobj==glass_slide_view){
        currentobj.frameName="slide_view_3.png";
          arrow.x=glass_slide_view.x;
          arrow.y=glass_slide_view.y-50;//325
        
      }
      if(slideNo<3){
       arrow.visible=true;
      }
      currentobj=null;
    }
    
  },
	NextExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step20",1);
    voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
