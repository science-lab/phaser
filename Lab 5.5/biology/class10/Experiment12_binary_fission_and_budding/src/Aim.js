var aim = function(game)
{
///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////

var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
 
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var fontStyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

}

aim.prototype = 
{
init: function( ipadrs) 
{

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("Aim",1);
 voice.play();
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_disabled.png');
  homeButton.scale.setTo(.7,.7);
  //homeButton.inputEnabled = true;
  //homeButton.input.useHandCursor = true;
  //homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  aim_Base= this.game.add.sprite(160,140,'dialogue_box');
  aim_Exp="Aim:";
  aim_Exp_1="Studying ";
  aim_Exp_2="(a) Binary fission in Amoeba and  ";
  aim_Exp_3="(b) Budding in yeast with the help of prepared slides.";
  //aim_Exp_4="(iii) Behaviour towards carbon disulphide as a solvent";
  //aim_Exp_5="(iv) Effect of heat";
  //aim_Exp_6="    (iii) Effect on litmus ";
  //aim_Exp_7="    (iv) Reaction with sodium hydrogen carbonate ";
  //aim_Exp_8="    Arranging Zn, Fe, Cu and Al (metals) in the decreasing order of reactivity based \n    on the above result. ";
  //aim_Exp_9="    (iv) Reaction between sodium sulphate and barium chloride solutions.";
  aim_text=this.game.add.text(250,200,aim_Exp,headfontStyle);
  aim_text_1=this.game.add.text(250,300,aim_Exp_1,fontStyle);
  aim_text_2=this.game.add.text(250,380,aim_Exp_2,fontStyle);
  aim_text_3=this.game.add.text(250,460,aim_Exp_3,fontStyle);
  //aim_text_4=this.game.add.text(250,620,aim_Exp_4,fontStyle);
  //aim_text_5=this.game.add.text(250,720,aim_Exp_5,fontStyle);
  //aim_text_6=this.game.add.text(250,520,aim_Exp_6,fontStyle);
  //aim_text_7=this.game.add.text(250,600,aim_Exp_7,fontStyle);
  //aim_text_8=this.game.add.text(250,680,aim_Exp_8,fontStyle);
  //aim_text_9=this.game.add.text(250,840,aim_Exp_9,fontStyle);



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1600,870,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExp_selection, this);


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },

////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

// For Full screen checking.
  
      gofull: function()
      {
    
       if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      }
      else
      {
        this.game.scale.startFullScreen(false);
      }  
      
      },

      nextSound:function(){
        //voice2.play()
      },
//For to next scene
 
      toExp_selection:function()
      {
 
      voice.destroy();
      //voice2.destroy();
      //if (!this.game.device.desktop)
       // {
        if (!this.game.scale.isFullScreen)
          {
          this.gofull();
          }
      //}
      this.state.start("Materials", true, false);
      //this.state.start("Observations", true, false);
      },

// For mute the audio
    
      muteTheGame:function()
      { 
      muted = true;

      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
      this.game.sound.mute = false;
      volumeButton.visible = false;
      muteButton.visible = true;
      },

// For Goto title screen
      
      gotoHome:function()
      {
      voice.destroy();
      //voice2.destroy();
      this.state.start("Title", true, false);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/

 // To quit the experiment
    closeTheGame:function()
    {
    voice.destroy();
      //voice2.destroy();
    //this.postData();
    //local cloud instance test
    //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
    window.open(loc,"_self");                  // local test link

    //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
    },


}
