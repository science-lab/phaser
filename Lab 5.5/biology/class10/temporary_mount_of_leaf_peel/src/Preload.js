var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var theory_completed;
  var theory_Scene;
  var observation_completed;
  var observation_Scene;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Microscope//////////////////////////////////////////////////////////
      this.game.load.image('bg_black','assets/Bg_black.png');
      this.game.load.image('bg_top','assets/Bg_top.png'); 
      this.game.load.image('microscope','assets/Leaf/microscope.png'); 

      this.game.load.image('microscope_top_view','assets/Leaf/Mocroscope_top_view.png');
        
        this.game.load.image('microscope_holder_right','assets/Leaf/mocroscope_right_lock.png');
        this.game.load.image('microscope_holder_left','assets/Leaf/mocroscope_left_lock.png');
        
        this.game.load.image('Microscopic_view_1','assets/Leaf/Microscopic_view_1.png');
        this.game.load.image('Microscopic_view_2','assets/Leaf/Microscopic_view_2.png');
        
        
      this.game.load.atlasJSONHash('arrow', 'assets/Leaf/arrow.png', 'assets/Leaf/arrow.json');
      this.game.load.atlasJSONHash('eyePiece_rotating_sprite', 'assets/Leaf/eyePiece_rotating_sprite.png', 'assets/Leaf/eyePiece_rotating_sprite.json');
    //////////////////////////////////Leaf//////////////////////////////////////////   
      this.game.load.atlasJSONHash('Leaf', 'assets/Leaf/Leaf.png', 'assets/Leaf/Leaf.json');
      this.game.load.atlasJSONHash('glycerine_to_glassplate', 'assets/Leaf/glycerine_to_glassplate.png', 'assets/Leaf/glycerine_to_glassplate.json');
      this.game.load.atlasJSONHash('Peel_to_water', 'assets/Leaf/Peel_to_water.png', 'assets/Leaf/Peel_to_water.json');
      this.game.load.atlasJSONHash('glycerine_to_glassplate', 'assets/Leaf/glycerine_to_glassplate.png', 'assets/Leaf/glycerine_to_glassplate.json');
      this.game.load.atlasJSONHash('Suferine_to watch_glass', 'assets/Leaf/Suferine_to watch_glass.png', 'assets/Leaf/Suferine_to watch_glass.json');
      this.game.load.atlasJSONHash('water_dropper_sprite', 'assets/Leaf/water_dropper_sprite.png', 'assets/Leaf/water_dropper_sprite.json');
      this.game.load.atlasJSONHash('water_drop_sprite', 'assets/Leaf/water_drop_sprite.png', 'assets/Leaf/water_drop_sprite.json');
      this.game.load.atlasJSONHash('Peel_from_water_with_brush', 'assets/Leaf/Peel_from_water_with_brush.png', 'assets/Leaf/Peel_from_water_with_brush.json');
      this.game.load.atlasJSONHash('Peel_to_stain', 'assets/Leaf/Peel_to_stain.png', 'assets/Leaf/Peel_to_stain.json');
      this.game.load.atlasJSONHash('safranine_pour_sprite', 'assets/Leaf/safranine_pour_sprite.png', 'assets/Leaf/safranine_pour_sprite.json');
      this.game.load.atlasJSONHash('safranine_drop_sprite', 'assets/Leaf/safranine_drop_sprite.png', 'assets/Leaf/safranine_drop_sprite.json');
      this.game.load.atlasJSONHash('Peel_From_Stain', 'assets/Leaf/Peel_From_Stain.png', 'assets/Leaf/Peel_From_Stain.json');
      this.game.load.atlasJSONHash('filter_paper_sprite', 'assets/Leaf/filter_paper_sprite.png', 'assets/Leaf/filter_paper_sprite.json');
      this.game.load.atlasJSONHash('slide_views', 'assets/Leaf/slide_views.png', 'assets/Leaf/slide_views.json');



      this.game.load.image('cover_slip','assets/Leaf/cover_slip.png');
      this.game.load.image('forceps','assets/Leaf/forceps.png');
      this.game.load.image('glycerin_bottle','assets/Leaf/glycerin_bottle.png');
      this.game.load.image('glycerin_bottle_back','assets/Leaf/glycerin_bottle_back.png');
      this.game.load.image('Brush','assets/Leaf/Brush.png');
      this.game.load.image('needle','assets/Leaf/needle.png');
      this.game.load.image('Beaker','assets/Leaf/Beaker.png');
      this.game.load.image('Beaker1','assets/Leaf/Beaker1.png');
      this.game.load.image('Beaker_back','assets/Leaf/Beaker_back.png');
      this.game.load.image('Glass_plate','assets/Leaf/Glass_plate.png');
      this.game.load.image('Glass_plate_2','assets/Leaf/Glass_plate_2.png');
      this.game.load.image('Diagram_1','assets/Leaf/Diagram_1.png');
      this.game.load.image('Diagram_2','assets/Leaf/Diagram_2.png');

      

      ///////////material required/////

      this.game.load.image('m_safranin','assets/materials/m_safranin.png');
      this.game.load.image('m_glycerinBottle','assets/materials/m_glycerin_bottle.png');
      this.game.load.image('m_dropper','assets/materials/m_dropper.png');
      this.game.load.image('m_petridish','assets/materials/m_petridish_water.png');
      this.game.load.image('m_forceps','assets/materials/m_forceps.png');
      this.game.load.image('m_glassSlide','assets/materials/m_glass_slide.png');
      this.game.load.image('m_coverslip','assets/materials/m_cover_slip.png');
      this.game.load.image('m_filterPaper','assets/materials/m_filter_paper.png');
      this.game.load.image('m_microscope','assets/materials/m_microscope.png');
      this.game.load.image('m_needle','assets/materials/m_needle.png');
      this.game.load.image('m_brush','assets/materials/m_brush.png');
      
      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution_1','assets/audio/Precaution_1.mp3');
      this.game.load.audio('Precaution_2','assets/audio/Precaution_2.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');
      this.game.load.audio('Procedure_3','assets/audio/Procedure_3.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Observation_3','assets/audio/Observation_3.mp3');
      
      //this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      //this.game.load.audio('Observation_3','assets/audio/Observation_3.mp3');
      //this.game.load.audio('Observation_4','assets/audio/Observation_4.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      
      
      this.game.load.audio('step0','assets/audio/step0.mp3');
      this.game.load.audio('step1','assets/audio/step1.mp3');
      this.game.load.audio('step2','assets/audio/step2.mp3');
      this.game.load.audio('step3','assets/audio/step3.mp3');
      this.game.load.audio('step4','assets/audio/step4.mp3');
      this.game.load.audio('step5','assets/audio/step5.mp3');
      this.game.load.audio('step6','assets/audio/step6.mp3');
      this.game.load.audio('step7','assets/audio/step7.mp3');
      this.game.load.audio('step8','assets/audio/step8.mp3');
      this.game.load.audio('step9','assets/audio/step9.mp3');
      this.game.load.audio('step10','assets/audio/step10.mp3');
      this.game.load.audio('step11','assets/audio/step11.mp3');
      this.game.load.audio('step12','assets/audio/step12.mp3');
      this.game.load.audio('step13','assets/audio/step13.mp3');
      this.game.load.audio('step14','assets/audio/step14.mp3');
      this.game.load.audio('step15','assets/audio/step15.mp3');
      this.game.load.audio('step16','assets/audio/step16.mp3');
      this.game.load.audio('step17','assets/audio/step17.mp3');
      this.game.load.audio('step18','assets/audio/step18.mp3');
      this.game.load.audio('step19','assets/audio/step19.mp3');
      this.game.load.audio('step20','assets/audio/step20.mp3');
      this.game.load.audio('step21','assets/audio/step21.mp3');
      this.game.load.audio('step22','assets/audio/step22.mp3');
      this.game.load.audio('step23','assets/audio/step23.mp3');
      this.game.load.audio('step24','assets/audio/step24.mp3');
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	  hotflag=1;
      ip = location.host; 
      theory_completed=false;
      theory_Scene=1;
      observation_completed=false;
      observation_Scene=1;
      
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

