var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;

var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
var tissue;
var circle_bg;
var currentScale;
}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;
tissue=null;
bg= this.game.add.sprite(0, 0,'bg');

bgTopView= this.game.add.sprite(0,0,'bg_top');
bgTopView.scale.setTo(1,1);
bgTopView.visible=false;

microscopeTopview=this.game.add.sprite(900,480,'microscope_top_view');
microscopeTopview.scale.setTo(1.2,1.2);
microscopeTopview.anchor.setTo(.5,.4);
microscopeTopview.visible=false;

bgBlack= this.game.add.sprite(0,0,'bg_black');
bgBlack.scale.setTo(1,1);
bgBlack.alpha=0;
bgBlack.visible=false;

// Circle_Red= this.game.add.sprite(902,494,'Circle_Red');
// Circle_Red.anchor.setTo(.5,.5);
// Circle_Red.scale.set(0);
// Circle_Red.alpha=0;



tissue=this.game.add.sprite(902,494,'Microscopic_view_1');
tissue.anchor.setTo(.5,.5);
tissue.scale.set(0);
tissue.alpha=0;

tissue1=this.game.add.sprite(912,490,'Microscopic_view_2');
tissue1.anchor.setTo(.5,.5);
tissue1.scale.set(0.5);
tissue1.alpha=0;

// fission_1.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,8,7,6,5,4,3,2,1],4, true, true);
 //fission_1.animations.play('anim');
 //tissue=fission_1;
circle1 = this.game.add.graphics(0, 0);
////circle1.lineStyle(8, 0xffd900, 1);
circle1.beginFill(0xFF0000, .3);
circle1.drawCircle(902, 494, 430);
//circle1.scale.set(0.5);
tissue1.mask=circle1;
circle1.visible=false;

circle2 = this.game.add.graphics(0, 0);
////circle2.lineStyle(8, 0xffd900, 1);
circle2.beginFill(0xFF0000, .3);
circle2.drawCircle(902, 494, 430);
//circle2.scale.set(0.5);
tissue.mask=circle2;
circle2.visible=false;

currentScale=0;


/*var blackBox1 = this.game.add.graphics(0, 0);
  blackBox1.lineStyle(5,0x808B96,1);
  blackBox1.beginFill(0x000000);
  blackBox1.drawRoundedRect(0, 0, 360, 150,15);//760
  blackBox1.x=1500;
  blackBox1.y=915;
var blackBox = this.game.add.graphics(0, 0);
  blackBox.lineStyle(5,0x808B96,1);
  blackBox.beginFill(0x000000);
  blackBox.drawRoundedRect(0, 0, 360, 760,15);//760
  blackBox.x=1500;
  blackBox.y=150;*/

voice=this.game.add.audio("step1_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
prev_btn.scale.setTo(-.7,.7);
prev_btn.inputEnabled = true;
//prev_btn.input.priorityID = 3;
prev_btn.input.useHandCursor = true;
prev_btn.events.onInputDown.add(this.backToExp, this);
prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    microscopeGroup=this.game.add.group();
    microscopeGroup.x=-2000;
    microscope=this.game.add.sprite(700,220,'microscope');
    microscope.scale.setTo(-1,1);
    microscopeGroup.add(microscope);
    micro_click=this.game.add.sprite(460,330,'collider');
    micro_click.scale.y=2.75;
    micro_click.anchor.set(.5);
    micro_click.alpha=0;
    microscopeGroup.add(micro_click);
    micro_click.inputEnabled = true;
    micro_click.input.useHandCursor = true;
    micro_click.events.onInputDown.add(this.clickOnEyePiece, this);
    micro_click.visible=false;
    eyePiece=this.game.add.sprite(575,575,'eyePiece_rotating_sprite','Microscope_Lenz0015.png');
    eyePiece.scale.setTo(-1,1);
    eyePiece.animations.add('anim1',[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
    eyePiece.animations.add('anim2',[14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],10, false, true);
    eyePiece.magnification=45;
    //eyePiece.animations.play('anim');
    microscopeGroup.add(eyePiece);
    

    glass_slide_view=this.game.add.sprite(450,740,'Glass_plate_2');
    glass_slide_view.scale.set(-.6,.6);
    glass_slide_view.anchor.set(.5);
    // glass_slide_view.events.onDragStart.add(function() {this.gameC(glass_slide_view)}, this);
    // glass_slide_view.events.onDragStop.add(function() {this.gameC1(glass_slide_view)}, this);
    // this.game.physics.arcade.enable(glass_slide_view);
    microscopeGroup.add(glass_slide_view);
    glass_slide_view.visible=false;



    // label_box1 = this.game.add.graphics(0, 0);
    // //box1.lineStyle(5,0xffffff,.6);
    // label_box1.beginFill(0xE67E22,1);//9B59B6
    // label_box1.drawRect(850, 800, 300, 60);
    // label_1=this.game.add.text(940,808,"Amoeba",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    
    microscopeLeftHolder=this.game.add.sprite(585,700,'microscope_holder_left');
    microscopeLeftHolder.scale.setTo(-1,1);
    microscopeGroup.add(microscopeLeftHolder);
    microscopeRightHolder=this.game.add.sprite(450,740,'microscope_holder_right');
    microscopeRightHolder.scale.setTo(-1,1);
    microscopeGroup.add(microscopeRightHolder);

    boxGroup=this.game.add.group();
    box = this.game.add.graphics(0, 0);
    box.lineStyle(5,0xE67E22,.6);
    box.beginFill(0x000000,1);//9B59B6
    box.drawRect(30, 200, 325, 240);
    testHead=this.game.add.text(40,210,"Select lens:",headfontStyle);
    box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    box1.beginFill(0xffffff,1);//9B59B6
    box1.drawRect(40, 275, 300, 50);
    box1.inputEnabled = true;
    box1.input.useHandCursor = true;
    box1.events.onInputDown.add(this.clickOnbox1, this);
    box2 = this.game.add.graphics(0, 0);
    //box2.lineStyle(5,0xffffff,.6);
    box2.beginFill(0xffffff,1);//9B59B6
    box2.drawRect(40, 350, 300, 50);
    box2.inputEnabled = true;
    box2.input.useHandCursor = true;
    box2.events.onInputDown.add(this.clickOnbox2, this);
    boxR = this.game.add.graphics(0, 0);
    //boxR.lineStyle(5,0xffffff,.6);
    boxR.beginFill(0x00ff00,.6);//9B59B6
    boxR.drawRect(0, 0, 300, 50);
    boxR.x=40;
    boxR.y=275;
    test1=this.game.add.text(50,280,"10X (Low power)",fontStyle_b);
    test2=this.game.add.text(50,355,"45X (High power)",fontStyle_b);

    boxGroup.add(box);
    boxGroup.add(box1);
    boxGroup.add(box2);
    boxGroup.add(boxR);
    boxGroup.add(testHead);
    boxGroup.add(test1);
    boxGroup.add(test2);
    boxGroup.visible=false;

    

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe a temporary mount of a leaf peel.";
    this.game.time.events.add(Phaser.Timer.SECOND*.2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

    darkBg = this.game.add.graphics(0, 0);
    
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
     //////////////////////////////////////////////////////////////////////////  
     clocks=this.game.add.sprite(550,260,"Clock");
     clocks.anchor.set(.5);
     clocks.scale.set(.7);
     clocksNeedleBig=this.game.add.sprite(550,265,"ClockClock_Needle_Big");
     clocksNeedleBig.anchor.set(.5,.85);
     clocksNeedleBig.scale.set(.7);
     clocksNeedleSmall=this.game.add.sprite(550,265,"Clock_Needle_Small");
     clocksNeedleSmall.anchor.set(.5,.8);
     clocksNeedleSmall.scale.set(.7);
     //clocks.visible=false;
     //clocksNeedleBig.visible=false;
     clocksNeedleSmall.visible=false;
     clockFlag=false;
     clockNum=0;
     clockDelay=4;
     clockAngle=0;
     //////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////
    ObjectGroup=this.game.add.group();
    Leaf=this.game.add.sprite(3250,900, 'Leaf','Leaf0001.png');
    Leaf.animations.add('anim',[0,1,2,3,4,5,6,7,8,9],4, false, true);
    Leaf.anchor.set(.5);
    Leaf.scale.set(.75);
    //Leaf.alpha=0.4;
    //Leaf.visible=false;
    Leaf1=this.game.add.sprite(1160,764, 'Leaf','Peel_from_leaf0001.png');
    Leaf1.animations.add('anim_2',[10,11,12,13,14,15,16,17,18,19],4, false, true);
    Leaf1.anchor.set(.5);
    Leaf1.scale.set(1.18);
    //Leaf1.alpha=.4;
    Leaf1.visible=false;

    //change-3200
    
    watchGlass=this.game.add.sprite(3200,900, 'Suferine_to watch_glass','Suferine_to watch_glass0001.png');
    watchGlass.animations.add('anim',[0,1,2,3,4,5,6,7],10, false, true);
    watchGlass.animations.add('anim_2',[10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25],20, false, true);
    watchGlass.anchor.set(.5);
    watchGlass.scale.set(.95);
    water_drop=this.game.add.sprite(1200,875, 'water_drop_sprite','Water_Drop0001.png');
    water_drop.animations.add('anim',[0,1,2],6, true, true);
    water_drop.anchor.set(.5);
    //water_drop.scale.set(.8);
    water_drop.visible=false;

    safranine_drop=this.game.add.sprite(1200,850, 'safranine_drop_sprite','Liquid_Drop0001.png');
    safranine_drop.animations.add('anim',[0,1,2,3,4,5],6, false, true);
    safranine_drop.anchor.set(.5);
    safranine_drop.scale.set(.7);
    safranine_drop.visible=false;

    Peel_to_watchGlass=this.game.add.sprite(1342,750, 'Peel_from_water_with_brush','Peel_from_water_with_brush0015.png');
    Peel_to_watchGlass.animations.add('anim',[15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],10, false, true);
    Peel_to_watchGlass.anchor.set(.5);
    Peel_to_watchGlass.scale.set(.95);
    Peel_to_watchGlass.visible=false;

    Peel_From_Stain=this.game.add.sprite(1342,750, 'Peel_From_Stain','Peel_From_Stain0001.png');
    Peel_From_Stain.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
    Peel_From_Stain.animations.add('anim_2',[15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],10, false, true);
    Peel_From_Stain.anchor.set(.5);
    Peel_From_Stain.scale.set(.95);
    Peel_From_Stain.visible=false;
    
    

    Peel_in_water=this.game.add.sprite(900,920, 'Peel_to_water','Peel_in_water0001.png');//Peel_to_water_red0003.png
    Peel_in_water.animations.add('anim',[0,1],10, false, true);
    Peel_in_water.animations.add('anim_2',[11,12,13],10, false, true);
    Peel_in_water.anchor.set(.5);
    Peel_in_water.scale.set(.95);
    Peel_to_water=this.game.add.sprite(1160,765, 'Peel_to_water','Peel_to_water0001.png');
    Peel_to_water.animations.add('anim',[2,3,4,5,6,7,8,9,10],10, false, true);
    Peel_to_water.anchor.set(.5);
    Peel_to_water.scale.set(1.2);
    Peel_to_water.visible=false;

    Peel_From_Stain1=this.game.add.sprite(1040,760, 'Peel_From_Stain','Peel_From_Stain0001.png');//1342,760
    Peel_From_Stain1.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
    Peel_From_Stain1.animations.add('anim_2',[15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],10, false, true);
    Peel_From_Stain1.anchor.set(.5);
    Peel_From_Stain1.scale.set(.95);
    Peel_From_Stain1.visible=false;

Peel_in_slide1=this.game.add.sprite(1362,820, 'Peel_to_water','Small_glassplate_to_Glassplate0000.png');
    Peel_in_slide1.anchor.set(.5);
    Peel_in_slide1.scale.set(.94);

    
    // Peel_in_slide1.events.onDragStart.add(function() {this.gameC(Peel_in_slide1)}, this);
    // Peel_in_slide1.events.onDragStop.add(function() {this.gameC1(Peel_in_slide1)}, this);
    // this.game.physics.arcade.enable(Peel_in_slide1);
    
    Peel_in_slide1.visible=false;
Peel_in_slide=this.game.add.sprite(3360,900, 'Peel_to_water','s_glass_plate.png');//s_glass_plate_water.png
   Peel_in_slide1.animations.add('anim',[18,19,20,21,22,23,24,25,26],10, false, true);
   Peel_in_slide.anchor.set(.5);
    //Peel_in_slide.scale.set(.8);
    //Peel_in_slide.visible=false;
    cover_slip=this.game.add.sprite(3660,900,'cover_slip');
    cover_slip.anchor.set(.5);
    filter_paper=this.game.add.sprite(2660,900, 'filter_paper_sprite','Paper0001.png');
    filter_paper.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12],10, false, true);
    filter_paper.anchor.set(.5);
    filter_paper.scale.set(.8);

    water_drop1=this.game.add.sprite(1360,875, 'water_drop_sprite','Water_Drop0001.png');
    water_drop1.animations.add('anim',[0,1,2],6, false, true);
    water_drop1.anchor.set(.5);
    //water_drop1.scale.set(.8);
    water_drop1.visible=false;


    Peel_from_water=this.game.add.sprite(1050,760, 'Peel_from_water_with_brush','Peel_from_water_with_brush0001.png');
    Peel_from_water.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15],10, false, true);
    Peel_from_water.anchor.set(.5);
    Peel_from_water.scale.set(.95);
    //Peel_from_water.scale.x=-.95;
    Peel_from_water.visible=false;

    Beaker_back=this.game.add.sprite(157,805, 'Beaker_back');
    Beaker_back.anchor.set(.5);
    Beaker_back.scale.set(.8);
    Beaker=this.game.add.sprite(150,800, 'Beaker');
    Beaker.anchor.set(.5);
    Beaker.scale.set(.8);

    glycerin_bottle_back=this.game.add.sprite(200,630, 'glycerin_bottle_back');
    glycerin_bottle_back.anchor.set(.5);
    glycerin_bottle_back.scale.set(.8);
    water_dropper3=this.game.add.sprite(200,435, 'water_dropper_sprite','Water_To_Dropper0001.png');
    water_dropper3.animations.add('anim',[4,3,2,1,0],4, false, true);//
    water_dropper3.anchor.set(.5);
    water_dropper3.scale.set(.8);
    //water_dropper3.angle=-70;
    water_dropper3.visible=false;
    glycerin_bottle=this.game.add.sprite(200,800, 'glycerin_bottle');
    glycerin_bottle.anchor.set(.5);
    glycerin_bottle.scale.set(.8);
    glycerin_bottle_back.visible=false;
    glycerin_bottle.visible=false;

    safranine=this.game.add.sprite(150,750, 'safranine_pour_sprite','Liquid_bottle0001.png');
    safranine.animations.add('anim',[0,1,2,3,4,5],4, false, true);//
    safranine.anchor.setTo(.6,.5);
    safranine.scale.set(.6);
    
    safranine.visible=false;
    
    needle=this.game.add.sprite(650,850, 'needle');
    needle.anchor.set(.5);
    needle.scale.set(.8);
    needle.angle=30;
    
    //needle.visible=false;
    forceps=this.game.add.sprite(600,900, 'forceps');
    forceps.anchor.set(.5);
    forceps.scale.set(.75);
    
    //forceps.visible=false;
    Brush=this.game.add.sprite(500,900, 'Brush');
    Brush.anchor.set(.5);
    Brush.scale.set(.6);
    Brush.angle=-45;
    
    //Brush.visible=false;
    Brush1=this.game.add.sprite(500,900, 'Brush');
    Brush1.anchor.set(.5);
    Brush1.scale.set(.6);
    Brush1.angle=-45;
    Brush1.visible=false;
    Brush2=this.game.add.sprite(500,900, 'Brush');
    Brush2.anchor.set(.5);
    Brush2.scale.set(.6);
    Brush2.angle=-45;
    Brush2.visible=false;
    
    water_dropper=this.game.add.sprite(400,900, 'water_dropper_sprite','Water_To_Dropper0001.png');
    water_dropper.anchor.set(.5);
    water_dropper.scale.set(.8);
    water_dropper.angle=-45;
    // water_dropper.inputEnabled = true;
    // water_dropper.input.useHandCursor = true;
    // water_dropper.events.onInputDown.add(this.clickOnWater_dropper, this);
    
    //water_dropper.visible=false;
    water_dropper1=this.game.add.sprite(150,485, 'water_dropper_sprite','Water_To_Dropper0001.png');
    water_dropper1.animations.add('anim',[4,3,2,1,0],4, false, true);//
    water_dropper1.anchor.set(.5);
    water_dropper1.scale.set(.8);
    //water_dropper1.angle=-70;
    water_dropper1.visible=false;

    water_dropper2=this.game.add.sprite(400,900, 'water_dropper_sprite','Water_To_Dropper0001.png');
    water_dropper2.anchor.set(.5);
    water_dropper2.scale.set(.8);
    water_dropper2.angle=-45;
    water_dropper2.visible=false;
    // water_dropper2.inputEnabled = true;
    // water_dropper2.input.useHandCursor = true;
    // water_dropper2.events.onInputDown.add(this.clickOnWater_dropper2, this);
    Glass_plate_2=this.game.add.sprite(1360,900, 'Glass_plate');
    Glass_plate_2.anchor.set(.5);
    Glass_plate_2.scale.set(.94);
    
        
    Glass_plate_2.events.onDragStart.add(function() {this.gameC(Glass_plate_2)}, this);
    Glass_plate_2.events.onDragStop.add(function() {this.gameC1(Glass_plate_2)}, this);
        this.game.physics.arcade.enable(Glass_plate_2);
    Glass_plate_2.visible=false;
        
    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";
    
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice=this.game.add.audio("step0",1);
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1",1);
    voice.play();
    // dialog.text="Take a compound microscope and a slide of Amoeba.";
    // tween1=this.game.add.tween(microscopeGroup).to( {x:0}, 800, Phaser.Easing.Out, true);
    // tween1.onComplete.add(function () {
    //   this.game.time.events.add(Phaser.Timer.SECOND*4,this.ExplainMicroscope, this);
      
    // }.bind(this));
    dialog.text="Take a fresh leaf of a dicot plant.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeLeef, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
  TakeLeef:function(){
    tween1=this.game.add.tween(Leaf).to( {x:1250}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      Leaf.x=1250;
      voice.destroy(true);
      voice=this.game.add.audio("step2",1);
      voice.play();
      dialog.text="Fold the leaf and then tear.";
      Leaf.inputEnabled = true;
      Leaf.input.useHandCursor = true;
      Leaf.events.onInputDown.add(this.clickOnLeaf, this);
      arrow.x=Leaf.x;
      arrow.y=Leaf.y-100;
      arrow.visible=true;
      
    }.bind(this));
  },
  clickOnLeaf:function(){
    arrow.visible=false;
    Leaf.inputEnabled = false;
    Leaf.input.useHandCursor = false;
    Leaf.animations.play('anim');
    Leaf.animations.currentAnim.onComplete.add(function(){
      voice.destroy(true);
      voice=this.game.add.audio("step3",1);
      voice.play();
      dialog.y=40;
      dialog.text="Take a forceps and pull out the thin membranous transparent peels \nfrom the torn epidermis.";
      forceps.events.onDragStart.add(function() {this.gameC(forceps)}, this);
      forceps.events.onDragStop.add(function() {this.gameC1(forceps)}, this);
      this.game.physics.arcade.enable(forceps);
      forceps.inputEnabled = true;
      forceps.input.useHandCursor = true;
      forceps.input.enableDrag(true);
      arrow.x=forceps.x;
      arrow.y=forceps.y-50;
      forceps.xp=forceps.x;
      forceps.yp=forceps.y;
      arrow.visible=true;
      collider.x=Leaf.x;
      collider.y=Leaf.y-100;
    }.bind(this));
    // tween1=this.game.add.tween(Leaf).to( {y:600}, 400, Phaser.Easing.Out, true);
    // tween1.onComplete.add(function () {
    //    
    // }.bind(this));
   
  },
  clickOnbox1:function(){
    if(eyePiece.magnification==45){
      eyePiece.magnification=10;
      boxR.y=275;
      tween1=this.game.add.tween(tissue1).to( {alpha:0}, 800, Phaser.Easing.Linear.Out, true);

      tween1.onComplete.add(function () {
        tween2=this.game.add.tween(tissue).to( {x:902,y:494}, 2000, Phaser.Easing.Linear.Out, true);
        tween3=this.game.add.tween(tissue.scale).to( {x:.8,y:.8}, 2000, Phaser.Easing.Linear.Out, true);
      }.bind(this));  
      
      // tween2.onComplete.add(function () {
      //   tissue1.alpha=.4;
      // }.bind(this));
    }
  },
  clickOnbox2:function(){
    if(eyePiece.magnification==10){
      eyePiece.magnification=45;
      boxR.y=350;
      //902,494
      tween1=this.game.add.tween(tissue).to( {x:850,y:390}, 2000, Phaser.Easing.Linear.Out, true);
      tween2=this.game.add.tween(tissue.scale).to( {x:1.7,y:1.7}, 2000, Phaser.Easing.Linear.Out, true);
      tween2.onComplete.add(function () {
        tween1=this.game.add.tween(tissue1).to( {alpha:1}, 800, Phaser.Easing.Linear.Out, true);
        //tissue1.alpha=1;
      }.bind(this));  
      // //
      // tween3=this.game.add.tween(tissue1.scale).to( {x:.5,y:.5}, 2000, Phaser.Easing.Linear.Out, true);
      // tween3.onComplete.add(function () {
      //   tween2=this.game.add.tween(tissue).to( {alpha:0}, 400, Phaser.Easing.Linear.Out, true);
      //   tween5=this.game.add.tween(tissue1).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
      //   tween4=this.game.add.tween(tissue1.scale).to( {x:.5,y:.5}, 2000, Phaser.Easing.Linear.Out, true);
      // }.bind(this));  
    }
  },
  ExplainMicroscope:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step19",1);
    voice.play();
    dialog.y=40;
    dialog.text="This is a compound microscope. We can change the magnification of \nthis lens to 10X and 45X by clicking on it.";
    arrow.x=eyePiece.x-100;
      arrow.y=eyePiece.y-50;
      arrow.visible=true;  
      arrow.state="lensRotation" ; 
      eyePiece.inputEnabled = true;
    eyePiece.input.useHandCursor = true;
    eyePiece.events.onInputDown.add(this.RotateEyePiece, this);
  },
  gameC:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    arrow.visible=false;
    if(obj==forceps){
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==Peel_to_water){
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==water_dropper1){
      //water_dropper1.angle=0;
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==Brush){
      Brush.angle=0;
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==safranine){
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==Brush1 || obj==Brush2){
      obj.angle=0;
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==water_dropper3){
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==cover_slip){
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==filter_paper){
      arrow.x=collider.x;
      arrow.y=collider.y-50;//370
      arrow.visible=true;
    }else if(obj==Glass_plate_2){
      collider.x=470;
      collider.y=740;
      
    }
  },
  gameC1:function(obj)
  {
    obj.body.enable =true;
    
  },
  match_Obj:function(){
    arrow.visible=false;
    console.log("pppppppppp");
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    if(currentobj==forceps){
      forceps.visible=false;
      Leaf.visible=false;
      Leaf1.visible=true;
      Leaf1.animations.play('anim_2');
      Leaf1.animations.currentAnim.onComplete.add(function(){
        Peel_to_water.visible=true;
        Leaf1.frameName="Peel_from_leaf0011.png";
        voice.destroy(true);
        voice=this.game.add.audio("step4",1);
        voice.play();
        dialog.text="Put the epidermal peels into a petridish full of water. Peels should not \nget dried.";
        
        Peel_to_water.events.onDragStart.add(function() {this.gameC(Peel_to_water)}, this);
        Peel_to_water.events.onDragStop.add(function() {this.gameC1(Peel_to_water)}, this);
        this.game.physics.arcade.enable(Peel_to_water);
        Peel_to_water.inputEnabled = true;
        Peel_to_water.input.useHandCursor = true;
        Peel_to_water.input.enableDrag(true);
        arrow.x=Peel_to_water.x;
        arrow.y=Peel_to_water.y-150;
        Peel_to_water.xp=Peel_in_water.x;
        Peel_to_water.yp=Peel_in_water.y;
        arrow.visible=true;
        collider.x=Peel_in_water.x;
        collider.y=Peel_in_water.y-150;
      }.bind(this));  
    }else if(currentobj==Peel_to_water){
      //Peel_to_water.frameName="Peel_to_water0009.png";
      //
      Peel_to_water.x=collider.x-80;
      Peel_to_water.y=collider.y+45;
      Peel_to_water.animations.play('anim');
      Peel_to_water.animations.currentAnim.onComplete.add(function(){
        Peel_to_water.visible=false;
        Peel_in_water.frameName="Peel_in_water0002.png";
        voice.destroy(true);
        voice=this.game.add.audio("step5",1);
        voice.play();
        dialog.y=60;
        dialog.text="Take a watchglass.";
        
        tween1=this.game.add.tween(Leaf1).to( {x:2500}, 800, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          tween2=this.game.add.tween(watchGlass).to( {x:1200}, 800, Phaser.Easing.Out, true);
          tween2.onComplete.add(function () {
            watchGlass.x=1200;
            // voice.destroy(true);
            // voice=this.game.add.audio("step6",1);
            // voice.play();
            dialog.text="Click on the dropper.";
            // console.log("-----------");
            water_dropper.inputEnabled = true;
            water_dropper.input.useHandCursor = true;
            water_dropper.events.onInputDown.add(this.clickOnWater_dropper, this);  
            arrow.x=water_dropper.x;
            arrow.y=water_dropper.y-50;
            arrow.visible=true;
          }.bind(this));  
        }.bind(this));  
      }.bind(this));   
    }else if(currentobj==water_dropper1){
      water_dropper1.x=collider.x;
      water_dropper1.y=collider.y-130;
      water_dropper1.animations.play('anim');
      water_dropper1.animations.currentAnim.onComplete.add(function(){
        water_drop.visible=true;
        water_drop.animations.play('anim');
        watchGlass.animations.play('anim');
        watchGlass.animations.currentAnim.onComplete.add(function(){
          water_drop.animations.stop(true);
          water_drop.visible=false;
          water_dropper1.visible=false;
          water_dropper2.visible=true;
          voice.destroy(true);
          voice=this.game.add.audio("step8",1);
          voice.play();
          dialog.text="Transfer the peel into the watch glass with a brush.";
          Brush.events.onDragStart.add(function() {this.gameC(Brush)}, this);
          Brush.events.onDragStop.add(function() {this.gameC1(Brush)}, this);
          this.game.physics.arcade.enable(Brush);
          Brush.inputEnabled = true;
          Brush.input.useHandCursor = true;
          Brush.input.enableDrag(true); 
          Brush.xp=Brush.x;
          Brush.yp=Brush.y; 
          arrow.x=Brush.x;
          arrow.y=Brush.y-50;
          arrow.visible=true;
          collider.x=Peel_in_water.x;
        }.bind(this));  
      }.bind(this));  
    }else if(currentobj==Brush){
      Brush.reset(500,900);
      Brush.visible=false;
      Brush.angle=-45;
      Peel_from_water.visible=true;
      Peel_from_water.animations.play('anim');
      Peel_in_water.frameName="Peel_in_water0001.png";
      Peel_from_water.animations.currentAnim.onComplete.add(function(){
        tween1=this.game.add.tween(Peel_from_water).to( {x:1350, y:750}, 800, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Beaker.visible=false;
          Beaker_back.visible=false;
          safranine.visible=true;
          Peel_from_water.x=1350;
          Peel_from_water.visible=false;
          Peel_to_watchGlass.visible=true;
          Peel_to_watchGlass.animations.play('anim');
          Peel_to_watchGlass.animations.currentAnim.onComplete.add(function(){
            Brush1.visible=true;
            Peel_to_watchGlass.visible=false;
            watchGlass.frameName='Suferine_to watch_glass0009.png';
            voice.destroy(true);
            voice=this.game.add.audio("step9",1);
            voice.play();
            dialog.text="Add 1-2 drops of safranine to stain the epidermal peel.";
            safranine.events.onDragStart.add(function() {this.gameC(safranine)}, this);
            safranine.events.onDragStop.add(function() {this.gameC1(safranine)}, this);
            this.game.physics.arcade.enable(safranine);
            safranine.inputEnabled = true;
            safranine.input.useHandCursor = true;
            safranine.input.enableDrag(true); 
            safranine.xp=safranine.x;
            safranine.yp=safranine.y; 
            arrow.x=safranine.x;
            arrow.y=safranine.y-100;
            arrow.visible=true;
            collider.x=watchGlass.x;
          }.bind(this));  
        }.bind(this));  
      }.bind(this)); 
    }else if(currentobj==safranine){
      safranine.x=1317;
      safranine.y=800;
      safranine.animations.play('anim');
      safranine.animations.currentAnim.onComplete.add(function(){
        safranine_drop.visible=true;
        safranine_drop.animations.play('anim');
         watchGlass.animations.play('anim_2');
         
          watchGlass.animations.currentAnim.onComplete.add(function(){
           safranine_drop.visible=false;
           //safranine.reset(safranine.xp,safranine.yp);
           //safranine.frameName='Liquid_bottle0001.png';
           voice.destroy(true);
            voice=this.game.add.audio("step10",1);
            voice.play();
            dialog.text="After two minutes, the peel turns red in colour.";
           clockFlag=true;
          safranine.visible=false;
          }.bind(this));  

         
      }.bind(this));  
    }else if(currentobj==Brush1){
      console.log("pppppppptttttttttttt");
      Brush1.reset(500,900);
      Brush1.visible=false;
      Brush1.angle=-45;
      Peel_From_Stain.visible=true;
      Peel_From_Stain.animations.play('anim');
      //Peel_in_water.frameName="Peel_in_water0001.png";
      watchGlass.frameName="Suferine_to watch_glass0027.png";
      Peel_From_Stain.animations.currentAnim.onComplete.add(function(){
        tween1=this.game.add.tween(Peel_From_Stain).to( {x:1050, y:750}, 800, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Peel_From_Stain.x=1050;
          Peel_From_Stain.y=760;
          Peel_From_Stain.animations.play('anim_2');
          Peel_From_Stain.animations.currentAnim.onComplete.add(function(){
            Peel_from_water.visible=false;
            Peel_in_water.animations.play('anim_2');
            Peel_in_water.animations.currentAnim.onComplete.add(function(){
              Brush2.visible=true;
              Peel_From_Stain.visible=false;
              Peel_to_watchGlass.visible=false;
              tween2=this.game.add.tween(watchGlass).to( {x:3050}, 800, Phaser.Easing.Out, true);
              tween2.onComplete.add(function () {
                voice.destroy(true);
                voice=this.game.add.audio("step12",1);
                voice.play();
                dialog.y=60;
                dialog.text="Take a clean slide. ";
                tween2=this.game.add.tween(Peel_in_slide).to( {x:1360}, 800, Phaser.Easing.Out, true);
                tween3=this.game.add.tween(cover_slip).to( {x:1660}, 800, Phaser.Easing.Out, true);
                tween2.onComplete.add(function () {

                  // voice.destroy(true);
                  // voice=this.game.add.audio("step1_1",1);
                  // voice.play();
                  // dialog.text="Add 1-2 drops of glycerine to the slide.";
                  glycerin_bottle_back.visible=true;
                  glycerin_bottle.visible=true;  
                  // voice.destroy(true);
                  // voice=this.game.add.audio("step13",1);
                  // voice.play();
                   dialog.text="Click on the dropper.";
                  // console.log("////////");
                  water_dropper2.inputEnabled = true;
                  water_dropper2.input.useHandCursor = true;
                  water_dropper2.events.onInputDown.add(this.clickOnWater_dropper2, this);  
                  arrow.x=water_dropper2.x;
                  arrow.y=water_dropper2.y-50;
                  arrow.visible=true;
                }.bind(this));
              }.bind(this));  
            }.bind(this));
          }.bind(this));  
        }.bind(this));  
      }.bind(this)); 
    }else if(currentobj==water_dropper3){
      water_dropper3.x=collider.x;
      water_dropper3.y=660;
      water_dropper3.animations.play('anim');
      water_dropper3.animations.currentAnim.onComplete.add(function(){
        water_drop1.visible=true;
        water_drop1.animations.play('anim');
        water_drop1.animations.currentAnim.onComplete.add(function(){
          Peel_in_slide.frameName='s_glass_plate_water.png';
          water_drop1.visible=false;
          water_dropper2.visible=false;
          water_dropper3.visible=false;
          voice.destroy(true);
          voice=this.game.add.audio("step15",1);
          voice.play();
          dialog.text="Transfer the peel into the slide with a brush.";
          Brush2.events.onDragStart.add(function() {this.gameC(Brush2)}, this);
          Brush2.events.onDragStop.add(function() {this.gameC1(Brush2)}, this);
          this.game.physics.arcade.enable(Brush2);
          Brush2.visible=true;
          Brush2.inputEnabled = true;
          Brush2.input.useHandCursor = true;
          Brush2.input.enableDrag(true); 
          Brush2.xp=Brush2.x;
          Brush2.yp=Brush2.y; 
          arrow.x=Brush2.x;
          arrow.y=Brush2.y-50;
          arrow.visible=true;
          collider.x=Peel_in_water.x;
        }.bind(this));  
      }.bind(this));  
    }else if(currentobj==Brush2){
      Brush2.reset(500,900);
      Brush2.visible=false;
      Brush2.angle=-45;
      Peel_From_Stain1.x=1040;
      Peel_From_Stain1.visible=true;
      Peel_From_Stain1.animations.play('anim');
      // //Peel_in_water.frameName="Peel_in_water0001.png";
      Peel_in_water.frameName="Peel_to_water_red0004.png";
      Peel_From_Stain1.animations.currentAnim.onComplete.add(function(){
        
        tween1=this.game.add.tween(Peel_From_Stain1).to( {x:1360, y:750}, 800, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          Peel_From_Stain1.x=1515;
          Peel_From_Stain1.y=750;
          Peel_From_Stain1.animations.play('anim_2');
          Peel_From_Stain1.animations.currentAnim.onComplete.add(function(){
            Peel_in_slide1.visible=true;
            Peel_in_slide.visible=false;
            Peel_From_Stain1.visible=false;
            voice.destroy(true);
            voice=this.game.add.audio("step16",1);
            voice.play();
            dialog.y=60;
            dialog.text="Cover it with a cover slip.";
            cover_slip.events.onDragStart.add(function() {this.gameC(cover_slip)}, this);
            cover_slip.events.onDragStop.add(function() {this.gameC1(cover_slip)}, this);
            this.game.physics.arcade.enable(cover_slip);
            cover_slip.visible=true;
            cover_slip.inputEnabled = true;
            cover_slip.input.useHandCursor = true;
            cover_slip.input.enableDrag(true); 
            cover_slip.xp=cover_slip.x;
            cover_slip.yp=cover_slip.y; 
            arrow.x=cover_slip.x;
            arrow.y=cover_slip.y-50;
            arrow.visible=true;
            collider.x=Peel_in_slide.x;
            
          }.bind(this));  
        }.bind(this));  
      }.bind(this)); 
    }else if(currentobj==cover_slip){
      cover_slip.visible=false;
      needle.visible=false;
      Peel_in_slide1.animations.play('anim');
      Peel_in_slide1.animations.currentAnim.onComplete.add(function(){
        voice.destroy(true);
        voice=this.game.add.audio("step17",1);
        voice.play();
        dialog.text="Drain out excess glycerine from the slide with the help of blotting paper.";
        tween1=this.game.add.tween(filter_paper).to( {x:1720}, 800, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          filter_paper.events.onDragStart.add(function() {this.gameC(filter_paper)}, this);
          filter_paper.events.onDragStop.add(function() {this.gameC1(filter_paper)}, this);
          this.game.physics.arcade.enable(filter_paper);
          filter_paper.visible=true;
          filter_paper.inputEnabled = true;
          filter_paper.input.useHandCursor = true;
          filter_paper.input.enableDrag(true); 
          filter_paper.xp=filter_paper.x;
          filter_paper.yp=filter_paper.y; 
          arrow.x=filter_paper.x;
          arrow.y=filter_paper.y-100;
          arrow.visible=true;
          collider.x=Peel_in_slide.x;
        }.bind(this));   
      }.bind(this));   
    }else if(currentobj==filter_paper){
      filter_paper.x=Peel_in_slide1.x-10;
      filter_paper.y=Peel_in_slide1.y+50;
      filter_paper.animations.play('anim');
      filter_paper.animations.currentAnim.onComplete.add(function(){
        tween1=this.game.add.tween(filter_paper).to( {x:2720}, 800, Phaser.Easing.Out, true);
        tween1.onComplete.add(function () {
          tween2=this.game.add.tween(glycerin_bottle).to( {x:-600}, 800, Phaser.Easing.Out, true);
          tween3=this.game.add.tween(glycerin_bottle_back).to( {x:-600}, 800, Phaser.Easing.Out, true);
          tween4=this.game.add.tween(Peel_to_water).to( {x:-600}, 800, Phaser.Easing.Out, true);
          tween5=this.game.add.tween(Peel_in_water).to( {x:-600}, 800, Phaser.Easing.Out, true);
          tween5.onComplete.add(function () {
            voice.destroy(true);
            clocks.visible=false;
            clocksNeedleBig.visible=false;
            voice=this.game.add.audio("step18",1);
            voice.play();
            dialog.text="Take a compound microscope.";
            tween6=this.game.add.tween(microscopeGroup).to( {x:0}, 800, Phaser.Easing.Out, true);
            tween6.onComplete.add(function () {
              this.game.time.events.add(Phaser.Timer.SECOND*1,this.ExplainMicroscope, this);
              
            }.bind(this));
          }.bind(this));   
        }.bind(this));   
      }.bind(this));   
    }else if(currentobj==Glass_plate_2){
      
      
    currentobj.visible=false;
    currentobj.reset(currentobj.xp,currentobj.yp);
    currentobj.visible=false;
    //currentobj.frameName="Yeast_slide_1.png";
    
    glass_slide_view.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step21",1);
    voice.play();
    dialog.y=40;
    dialog.text="Click on the eyepiece of the microscope to observe the stomata, \nguard cells and epidermal cells.";
    arrow.x=micro_click.x-80;
    arrow.y=micro_click.y;
    arrow.angle=-90;
    arrow.visible=true;
    micro_click.visible=true;
    
  }
    
    
  },
  clickOnBrush:function(){
    arrow.visible=false;
    Brush.angle=0;
    tween1=this.game.add.tween(Peel_from_water).to( {x:1050, y:760}, 400, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      Peel_from_water=1050;
      Brush.reset(500,900);
      Brush.visible=false;
      Brush.angle=-45;
      Peel_from_water.visible=true;
      Peel_from_water.animations.play('anim');
      Peel_in_water.frameName="Peel_in_water0001.png";
      Peel_from_water.animations.currentAnim.onComplete.add(function(){
        
      }.bind(this));  
    }.bind(this));  
  },
  clickOnWater_dropper2:function(){
    console.log("clickOnWater_dropper");
    arrow.visible=false;
    water_dropper2.inputEnabled = false;
    water_dropper2.input.useHandCursor = false;
    water_dropper2.angle=0;
    tween1=this.game.add.tween(water_dropper2).to( {x:200, y:435}, 800, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      water_dropper2.x=200;
      water_dropper2.y=435;
      water_dropper2.visible=false;
      water_dropper3.visible=true;
      tween1=this.game.add.tween(water_dropper3).to( {y:755}, 800, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        water_dropper3.frameName="Water_To_Dropper0005.png";
        voice.destroy(true);
        voice=this.game.add.audio("step14",1);
        voice.play();
        dialog.text="Add 1-2 drops of glycerine to the slide.";
        water_dropper3.events.onDragStart.add(function() {this.gameC(water_dropper3)}, this);
        water_dropper3.events.onDragStop.add(function() {this.gameC1(water_dropper3)}, this);
        this.game.physics.arcade.enable(water_dropper3);
        water_dropper3.inputEnabled = true;
        water_dropper3.input.useHandCursor = true;
        water_dropper3.input.enableDrag(true); 
        arrow.x=water_dropper3.x;
        arrow.y=water_dropper3.y-240;
        water_dropper3.xp=water_dropper3.x;
        water_dropper3.yp=water_dropper3.y;
        arrow.visible=true;
        collider.x=Peel_in_slide.x;
        collider.y=Peel_in_slide.y-50;
      }.bind(this));
    }.bind(this));
  },
  clickOnWater_dropper:function(){
    arrow.visible=false;
    console.log("clickOnWater_dropper");
    water_dropper.inputEnabled = false;
    water_dropper.input.useHandCursor = false;
    water_dropper.angle=0;
    tween1=this.game.add.tween(water_dropper).to( {x:150, y:480}, 400, Phaser.Easing.Out, true);
    tween1.onComplete.add(function () {
      water_dropper.x=150;
      water_dropper.y=480;
      water_dropper.visible=false;
      water_dropper1.visible=true;
      tween1=this.game.add.tween(water_dropper1).to( {y:755}, 800, Phaser.Easing.Out, true);
      tween1.onComplete.add(function () {
        water_dropper1.frameName="Water_To_Dropper0005.png";
        water_dropper.x=400;
        water_dropper.y=900;
        voice.destroy(true);
        voice=this.game.add.audio("step7",1);
        voice.play();
        dialog.text="Put a small amount of water into the watch glass.";
        water_dropper1.events.onDragStart.add(function() {this.gameC(water_dropper1)}, this);
        water_dropper1.events.onDragStop.add(function() {this.gameC1(water_dropper1)}, this);
        this.game.physics.arcade.enable(water_dropper1);
        water_dropper1.inputEnabled = true;
        water_dropper1.input.useHandCursor = true;
        water_dropper1.input.enableDrag(true); 
        arrow.x=water_dropper1.x;
        arrow.y=water_dropper1.y-250;
        water_dropper1.xp=water_dropper1.x;
        water_dropper1.yp=water_dropper1.y;
        arrow.visible=true;
        collider.x=watchGlass.x;
        collider.y=watchGlass.y-100;
      }.bind(this));  
    }.bind(this));
  },
  RotateEyePiece:function(){
    if(arrow.state=="lensRotation"){
       eyePiece.inputEnabled = false;
       eyePiece.input.useHandCursor = false;
      arrow.visible=false;
      arrow.state="";
      voice.destroy(true);
      voice=this.game.add.audio("step20",1);
      voice.play();
      dialog.y=60;
      dialog.text="Place the slide to the microscope.";
      arrow.x=Glass_plate_2.x;
      arrow.y=Glass_plate_2.y-50;
      Peel_in_slide1.visible=false;
      arrow.visible=true;
      Glass_plate_2.inputEnabled = true;
      Glass_plate_2.input.useHandCursor = true;
      Glass_plate_2.input.enableDrag(true);
      Glass_plate_2.xp=Glass_plate_2.x;
      Glass_plate_2.yp=Glass_plate_2.y;
      Glass_plate_2.visible=true;
    }
    if(eyePiece.magnification==10){
      eyePiece.animations.play('anim1');
      eyePiece.magnification=45;
    }else{
      eyePiece.animations.play('anim2');
      eyePiece.magnification=10;
    }
  },
  clickOnEyePiece:function()
  {
    if(glass_slide_view.visible){
      arrow.visible=false;
      bg.visible=false;
      microscopeGroup.visible=false;
      

      bgTopView.visible=true;
      microscopeTopview.visible=true;
      
      this.game.time.events.add(Phaser.Timer.SECOND*1,this.microscopicViewTweens, this);
    }
  },
  microscopicViewTweens:function()
  {
    tissue.x=902;
    tissue.y=494;
    tissue.alpha=0;
    tissue.scale.set(0);
    tissue1.alpha=0;
    tissue.visible=true;
    tissue1.visible=true;
    microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
    bgBlack.visible=true;
    bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Linear.Out, true);
    
    tissue.visible=true;
    if(eyePiece.magnification==10){
      tween1=this.game.add.tween(tissue.scale).to( {x:.8,y:.8}, 2000, Phaser.Easing.Linear.Out, true);
      //tween2=this.game.add.tween(tissue1.scale).to( {x:.1,y:.1}, 2000, Phaser.Easing.Linear.Out, true);
      //tissue1.alpha=.4;
    }else{
      tween1=this.game.add.tween(tissue.scale).to( {x:.8,y:.8}, 2000, Phaser.Easing.Linear.Out, true);
      tween1.onComplete.add(function () {
        tween1=this.game.add.tween(tissue).to( {x:850,y:390}, 2000, Phaser.Easing.Linear.Out, true);
        tween2=this.game.add.tween(tissue.scale).to( {x:1.7,y:1.7}, 2000, Phaser.Easing.Linear.Out, true);
        tween2.onComplete.add(function () {
          tween3=this.game.add.tween(tissue1).to( {alpha:1}, 800, Phaser.Easing.Linear.Out, true);
          //tissue1.alpha=1;
        }.bind(this));   
      }.bind(this));
      
    }
    
    tween3=this.game.add.tween(tissue).to( {alpha:1}, 2500, Phaser.Easing.Linear.Out, true);
    tween3.onComplete.add(function () {
      if(eyePiece.magnification==10){
        boxR.y=275;
      }else{
        boxR.y=350;
      }
      boxGroup.visible=true;
      this.game.time.events.add(Phaser.Timer.SECOND*1,this.showSelectBox, this);
        
    }.bind(this));
    //tween4=this.game.add.tween(tissue1).to( {alpha:1}, 2500, Phaser.Easing.Linear.Out, true);
  },
  showSelectBox:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step22",1);
    voice.play();
    dialog.y=60;
    dialog.text="Select the lens options to observe the cell structure in low/high power.";
  
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.showBackBtn, this);
  },
  showBackBtn:function(){
    prev_btn.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step23",1);//"step6"
    voice.play();
    dialog.y=60;
    dialog.text="Click on the back button to go back to the experiment.";
    
  },
  backToExp:function(){
    eyePiece.inputEnabled = true;
    eyePiece.input.useHandCursor = true;
    bgTopView.visible=false;
    microscopeTopview.visible=false;
    microscopeTopview.scale.setTo(1.2,1.2);
    bgBlack.alpha=0;
    bgBlack.visible=false;
    tissue.scale.set(0);
    tissue.alpha=0;
    //tissue1.scale.set(0);
    tissue1.alpha=0;
    tissue.visible=false;
    tissue1.visible=false;
    //circle_bg.scale.set(0);
    //circle_bg.alpha=0;
    tissue.x=902;
    tissue.y=494;
    circle1.visible=false;
    boxGroup.visible=false;
    prev_btn.visible=false;
    bg.visible=true;
    eyePiece.magnification=10;
    eyePiece.frameName="Microscope_Lenz0001.png";
    microscopeGroup.visible=true;
    arrow.angle=0;
    ///////////////////
    play.visible=true;
        voice.destroy(true);
        voice=this.game.add.audio("step24",1);
        voice.play();
        dialog.y=60;
        dialog.text="Click on the next button to see the observations.";
   
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    interval++;
    currentScale=tissue.scale.x;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    if(clockFlag){
      clockNum++;
      if(clockNum>=clockDelay){
        clockNum=0;
        clocksNeedleBig.angle+=10;
        clockAngle+=5;
        if(clockAngle>=360){
          clockFlag=false;
          voice.destroy(true);
          voice=this.game.add.audio("step11",1);
          voice.play();
          dialog.y=40;
          dialog.text="Remove the excess stain by transferring the peel to the petri dish with \na brush.";
          Brush1.events.onDragStart.add(function() {this.gameC(Brush1)}, this);
          Brush1.events.onDragStop.add(function() {this.gameC1(Brush1)}, this);
          this.game.physics.arcade.enable(Brush1);
          Brush1.inputEnabled = true;
          Brush1.input.useHandCursor = true;
          Brush1.input.enableDrag(true); 
          Brush1.xp=Brush.x;
          Brush1.yp=Brush.y; 
          arrow.x=Brush1.x;
          arrow.y=Brush1.y-50;
          arrow.visible=true;
          collider.x=watchGlass.x;
          
        }
      }
    }
    
	},
  
	detectCollision:function(){
    if(collider.enableBody && currentobj!=null){
        this.game.physics.arcade.overlap(currentobj, collider,function() {this.match_Obj()},null,this);
    }
    if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
      //console.log(currentobj.xp+"//"+currentobj.yp);
      
      if(currentobj==forceps){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-50;//325
      }else if(currentobj==Peel_to_water){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-150;//325
      }else if(currentobj==water_dropper1){
        //water_dropper1.angle=-70;
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-250;//325
      }else if(currentobj==Brush){
        Brush.angle=-45;
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-50;//325
      }else if(currentobj==safranine){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-100;//325

      }else if(currentobj==Brush1 || currentobj==Brush2){
        currentobj.angle=-45;
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-50;//325
      }else if(currentobj==water_dropper3){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-240;//325
      }else if(currentobj==cover_slip){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-100;//325
      }else if(currentobj==filter_paper){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-100;//325
      }else if(currentobj==Peel_in_slide1){
        arrow.x=currentobj.x;
        arrow.y=currentobj.y-50;//325
        
      }
       arrow.visible=true;
       currentobj=null;
    }
    
  },
	NextExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step20",1);
    voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
