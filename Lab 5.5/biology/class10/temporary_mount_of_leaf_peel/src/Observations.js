var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation_1",1);
    //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,80,'observation_table');
  base.scale.setTo(1.1,1.1);
  tableGroup=this.game.add.group();
  tableGroup.visible=false;
  base_line1= this.game.add.sprite(240,190,'observation_table_line');
  base_line1.scale.y=.95;
  tableGroup.add(base_line1);
  base_line2= this.game.add.sprite(750,190,'observation_table_line');
  base_line2.scale.y=.95;
  tableGroup.add(base_line2);
  base_line3= this.game.add.sprite(1240,190,'observation_table_line');
  base_line3.scale.y=.95;
  tableGroup.add(base_line3);
  // base_line4= this.game.add.sprite(1330,190,'observation_table_line');
  // base_line4.scale.y=.95;
  // base_line4= this.game.add.sprite(1760,240,'observation_table_line');
  //  base_line4.angle=90;
  //  base_line4.scale.setTo(1.1,2.035);
  //  tableGroup.add(base_line4);
   base_line5= this.game.add.sprite(1760,250,'observation_table_line');
   base_line5.angle=90;
   base_line5.scale.setTo(1.1,2.035);
   tableGroup.add(base_line5);
  //////////////////////////////////////////////////////////////////////////
   //test1_text0="(A) Binary Fission in Amoeba";
   test1_text1="1.	The epidermal peel of leaf consists of stomata embedded in a single layer of \n    epidermal cells which are irregular in outline with no intercellular spaces.";
   test1_text2="2.	The stomata of dicots consist of two kidney-shaped guard cells (stomata of \n    monocots are dumb-bell shaped).";
   test1_text3="3.	A guard cell consists of a nucleus and many chloroplasts.";
   test1_text4="4.	The central pore between two guard cells is known as stomatal pore.";




   //test2_0=this.game.add.text(450,200,"Differences between Monocot and Dicot plants",headfontStyle);
   test2_1=this.game.add.text(145,210,"S.No.",headfontStyle);
   test2_2=this.game.add.text(370,210,"Observations",headfontStyle);
   test2_3=this.game.add.text(920,210,"Low power",headfontStyle);
   test2_4=this.game.add.text(1380,210,"High power",headfontStyle);
   test2_5=this.game.add.text(190,320,"1",fontStyle);
   test2_6=this.game.add.text(280,320,"Shape of guard cells",fontStyle);
   test2_7=this.game.add.text(570,320,"",fontStyle);
   test2_8=this.game.add.text(1115,320,"",fontStyle);
   test2_9=this.game.add.text(190,420,"2",fontStyle);
   test2_10=this.game.add.text(280,420,"Number of stomata in the \nfield of microscope",fontStyle);
   test2_11=this.game.add.text(570,420,"",fontStyle);
   test2_12=this.game.add.text(1115,420,"",fontStyle);
   test2_13=this.game.add.text(190,570,"3",fontStyle);
   test2_14=this.game.add.text(280,570,"Number of epidermal cells \nin the field of microscope",fontStyle);
   test2_15=this.game.add.text(570,570,"",fontStyle);
   test2_16=this.game.add.text(1115,570,"",fontStyle);
   
   tableGroup.add(test2_1);
   tableGroup.add(test2_2);
   tableGroup.add(test2_3);
   tableGroup.add(test2_4);
   tableGroup.add(test2_5);
   tableGroup.add(test2_6);
   tableGroup.add(test2_7);
   tableGroup.add(test2_8);
   tableGroup.add(test2_9);
   tableGroup.add(test2_10);
   tableGroup.add(test2_11);
   tableGroup.add(test2_12);
   tableGroup.add(test2_13);
   tableGroup.add(test2_14);
   tableGroup.add(test2_15);
   tableGroup.add(test2_16);
   

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
  next_btn = this.game.add.sprite(1640,830,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1640,830,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
  prev_btn = this.game.add.sprite(250,830,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(250,830,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  //prev_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrev, this);
  prev_btn.visible=false;
play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toResult, this);
//if(!observation_completed)
play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_1",1);
              voice.play();
               
              img=this.game.add.sprite(950,510,'Diagram_1'); 
              img.anchor.set(.5);
              img.scale.set(.5);
              test1_1=this.game.add.text(470,820,"Dicot stomata under low power of microscope \n             (with more cells, small size)",headfontStyle);
              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_2",1);
                voice.play();
               
              img=this.game.add.sprite(950,510,'Diagram_2'); 
              img.anchor.set(.5);
              img.scale.set(.5);
              test1_1=this.game.add.text(470,820,"Dicot stomata under high power of microscope \n           (with large cells, less in number)",headfontStyle);
              break;
            case 3:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_3",1);
                voice.play();
               
                test1_1=this.game.add.text(240,260,test1_text1,fontStyle);
                test1_2=this.game.add.text(240,400,test1_text2,fontStyle);
                test1_3=this.game.add.text(240,540,test1_text3,fontStyle);
                test1_4=this.game.add.text(240,620,test1_text4,fontStyle);
              
              break;
            case 4:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_2",1);
                //voice.play();
               
                tableGroup.visible=true;
              
              ///////////////////////////////////////////
              
              break;
            
          }
        },
        removeTexts:function(){
          if(pageCount<=2){
            img.destroy(true);
            test1_1.destroy(true);
          }
          if(pageCount==3){
            test1_1.destroy(true);
            test1_2.destroy(true);
            test1_3.destroy(true);
            test1_4.destroy(true);
          }
          if(pageCount==4){
            tableGroup.visible=false; 
          }
        
        },
        toNext:function(){
          this.removeTexts();
          if(pageCount<4){
            prev_btn.visible=true;
            pageCount++;
              this.addObservation();
              if(pageCount>=4){
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      toPrev:function(){
        this.removeTexts();
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  