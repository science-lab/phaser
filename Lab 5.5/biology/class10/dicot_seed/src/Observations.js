var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation_4",1);
    voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,80,'observation_table');
  base.scale.setTo(1.1,1.1);
  
  //  base_line1= this.game.add.sprite(500,350,'observation_table_line');
  //  base_line1.scale.y=.6;
  // base_line2= this.game.add.sprite(1375,350,'observation_table_line');
  // base_line2.scale.y=.6;
  // base_line3= this.game.add.sprite(970,190,'observation_table_line');
  // base_line3.scale.y=.95;
  // base_line4= this.game.add.sprite(1330,190,'observation_table_line');
  // base_line4.scale.y=.95;
  
  //   base_line5= this.game.add.sprite(1402,320,'observation_table_line');
  //   base_line5.angle=90;
  //   base_line5.scale.setTo(1.1,1.1);
  // base_line6= this.game.add.sprite(1402,800,'observation_table_line');
  // base_line6.angle=90;
  // base_line6.scale.setTo(1.1,1.1);
  // base_line7= this.game.add.sprite(1760,490,'observation_table_line');
  // base_line7.angle=90;
  // base_line7.scale.setTo(1.1,2.08);
  // base_line8= this.game.add.sprite(1760,580,'observation_table_line');
  // base_line8.angle=90;
  // base_line8.scale.setTo(1.1,2.08);
  // base_line9= this.game.add.sprite(1760,670,'observation_table_line');
  // base_line9.angle=90;
  // base_line9.scale.setTo(1.1,2.08);
  // base_line10= this.game.add.sprite(1760,760,'observation_table_line');
  // base_line10.angle=90;
  // base_line10.scale.setTo(1.1,2.08);

  //base_line.scale.setTo(1,1.15);
    //head1_text=this.game.add.text(150,220,"Material (A) Spirogyra (Pond silk)",headfontStyle);
    //head2_text=this.game.add.text(200,320,"Classification",headfontStyle);
    //head3_text=this.game.add.text(1100,220,"Striated muscle fibres",headfontStyle); 
  //  //head4_text=this.game.add.text(1080,170,"Relative Size of \n    the Image",headfontStyle); 
  //  head5_text=this.game.add.text(1350,180,"Inference",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
   test1_text1="Dry seeds when soaked in water swell up.";
   test1_text2="Water enters the seed through a minute pore called micropyle.";
   test1_text3="There is a scar on the seed coat called hilum.";
   test1_text4="Seed is attached to the fruit with the help of hilum.";
   test1_text5="Gram and other dicot seeds contain two cotyledons which store food.";
   test1_text6="Outer covering of the seed is called seed coat.";
   test1_text7="There is an embryo inside the seed which is the future plant.";
   test1_text8="Embryo has plumule (future shoot) at its upper end and radicle (future root) \nat its lower end, embroyal axis and two fleshy cotyledons are attached on its \nsides which store food.";
   test1_text9="";
   
   
   this.img1=null;
   
  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
 next_btn = this.game.add.sprite(1640,830,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1640,830,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(250,830,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(250,830,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
     prev_btn.visible=false;
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 //if(!observation_completed)
  play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_1",1);
              voice.play();
              if( this.img1!=null){
                this.img1.destroy();
              }
              test1_1=this.game.add.text(200,220,test1_text1,fontStyle);
              test1_2=this.game.add.text(200,280,test1_text2,fontStyle);
              test1_3=this.game.add.text(200,350,test1_text3,fontStyle);
              test1_4=this.game.add.text(200,410,test1_text4,fontStyle);
              test1_5=this.game.add.text(200,470,test1_text5,fontStyle);
              test1_6=this.game.add.text(200,530,test1_text6,fontStyle);
              this.img1 = this.game.add.sprite(650,600,'Grams-01');
              this.img1.scale.set(.6);
              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_2",1);
              voice.play();
              this.img1.destroy();
               test1_1=this.game.add.text(200,220,test1_text7,fontStyle);
              test1_2=this.game.add.text(200,300,test1_text8,fontStyle);
              test1_3=this.game.add.text(200,400,"",fontStyle);
              test1_4=this.game.add.text(200,480,"",fontStyle);
              test1_5=this.game.add.text(200,560,"",fontStyle);
              test1_6=this.game.add.text(200,640,"",fontStyle);
              this.img1 = this.game.add.sprite(250,500,'Grams_Disection-01');
       this.img1.scale.set(.9);
            break;
          
          }
        },
        removeTexts:function(){
          
            test1_1.destroy(true);
            test1_2.destroy(true);
            test1_3.destroy(true);
            test1_3.destroy(true);
            test1_4.destroy(true);
            test1_5.destroy(true);
            test1_6.destroy(true);
          
        },
        toNext:function(){
          this.removeTexts();
         // if(pageCount<7){
          //  prev_btn.visible=true;
            pageCount++;
              console.log(pageCount);
              if(pageCount>=2){
                prev_btn.visible=true;
                // voice.destroy(true);
                // this.state.start("Observations_1", true, false, ip);
                play.visible=true;
                next_btn.visible=false; 
                this.addObservation(); 
              }else {
              
              }
           // }
      },
      toPrev:function(){
        this.removeTexts();
          if(pageCount>1){
            play.visible=false;
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  