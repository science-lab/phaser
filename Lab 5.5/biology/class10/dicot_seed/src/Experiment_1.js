var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;
bg1= this.game.add.sprite(0, 0,'bg1');

bg= this.game.add.sprite(0, 0,'bg');


voice=this.game.add.audio("step4_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

dialogx=20;
dialogy=80;

this.intDailog();
this.startExperiment();

/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  intDailog:function(){

    dialog_box=this.game.add.sprite(0,30, 'dialogue_box1');
    dialog_box.scale.setTo(.85,.8);
    dialog_text="Lets perform the experiment";
    dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
    
   
 },
 

  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
   
  },
  startExperiment:function(){
    

    dialog.text="Take the seeds and put it into the beaker.";
    this.game.time.events.add(Phaser.Timer.SECOND,this.ArrangeItems, this); 
    // Beak.inputEnabled = true;
    // Beak.input.useHandCursor = true;
    // Beak.events.onInputDown.add(this.Exp_Pigeon_Beak, this);
    // arrow.x=Beak.x-50;
    // arrow.y=Beak.y;
    // arrow.angle=-90;
    // arrow.visible=true;
    
    
  },
  ArrangeItems:function(){
    this.beakerback=this.game.add.sprite(950,750,"Beaker_back");
    this.beakerback.anchor.set(.5);
    this.Beaker=this.game.add.sprite(950,750,"Beaker");
    this.Beaker.anchor.set(.5); 
    this.beakerX=950;
    this.beakerY=750;
    this.Gramspetridish=this.game.add.sprite(650,850,"Gramspetridish");
    this.Gramspetridish.anchor.set(.5);
    this.Gramspetridish.xpos=this.Gramspetridish.x;
    this.Gramspetridish.ypos=this.Gramspetridish.y;
    this.Gramspetridish.inputEnabled = true;
    this.Gramspetridish.input.useHandCursor = true;
    this.game.physics.arcade.enable(this.Gramspetridish);
    this.Gramspetridish.input.enableDrag(true);
    this.Gramspetridish.events.onDragStart.add(function() {this.DragOn(this.Gramspetridish)}, this);
    this.Gramspetridish.events.onDragStop.add(function() {this.DropOut(this.Gramspetridish)}, this);
  
    this.collideronbeaker=this.game.add.sprite(950,580, 'collider');
    this.game.physics.arcade.enable(this.collideronbeaker);
    this.collideronbeaker.anchor.set(.5); 
    this.collideronbeaker.scale.setTo(4,2);
    this.collideronbeaker.enableBody =true;
    this.collideronbeaker.alpha=.001
    this.watchglass=this.game.add.sprite(2200,980,"Watchglass");
    this.watchglass.anchor.set(.5); 
    this.watchglass.xpos=950;
    this.watchglass.ypos=980;
    this.watchglass.visible=false;

    this.forceps=this.game.add.sprite(-200,900,"forceps");
    this.forceps.anchor.set(.5); 
    this.forceps.scale.set(.6); 
    this.forceps.xpos=650;
    this.forceps.ypos=900;
    

   
    this.game.physics.arcade.enable(this.forceps);
  
    this.forceps.events.onDragStart.add(function() {this.DragOn(this.forceps)}, this);
    this.forceps.events.onDragStop.add(function() {this.DropOut(this.forceps)}, this);
    this.forceps.visible=false;

    this.needle=this.game.add.sprite(-200,950,"needle");
    this.needle.anchor.set(.5); 
    this.needle.xpos=350;
    this.needle.ypos=950;
 
   
    this.game.physics.arcade.enable(this.needle);
   
    this.needle.events.onDragStart.add(function() {this.DragOn(this.needle)}, this);
    this.needle.events.onDragStop.add(function() {this.DropOut(this.needle)}, this);
     this.needle.visible=false;

     this.needle1=this.game.add.sprite(350,950,"needle");
     this.needle1.anchor.set(.5); 
     this.needle1.xpos=350;
     this.needle1.ypos=950;
  
    
     this.game.physics.arcade.enable(this.needle1);
    
     this.needle1.events.onDragStart.add(function() {this.DragOn(this.needle1)}, this);
     this.needle1.events.onDragStop.add(function() {this.DropOut(this.needle1)}, this);
      this.needle1.visible=false;

  

    this.Glassplate=this.game.add.sprite(2300,980,"Glassplate");
    this.Glassplate.anchor.set(.5); 
    this.Glassplate.xpos=1000;
    this.Glassplate.ypos=980;
   this.Glassplate.visible=false;

   this.arrow1=this.game.add.sprite(0,0, 'arrow');
   this.arrow1.anchor.set(.5);
   this.arrow1.animations.add("frameall");
   this.arrow1.animations.add("frameidle",[0]);
   this.arrowShow=1;
   this.ProcedureText(1);
   this.ClockPart();
   this.ArowShowing();
   
  },
  ArowShowing:function(){
    this.arrow1.visible=true;
  console.log(this.arrowShow+" __ ");
    this.arrow1.animations.play("frameall",10,true);
    switch(this.arrowShow){
            case 1:
            this.arrow1.x= this.Gramspetridish.x;
            this.arrow1.y= this.Gramspetridish.y-50;
           
            break;
            case 2:
            this.arrow1.x= this.Beaker.x;
            this.arrow1.y= this.Beaker.y-250;
            break;
            case 3:
            this.arrow1.x= this.forceps.x;
            this.arrow1.y= this.forceps.y-50;
          
            break;
            case 4:
            this.arrow1.x= this.seedtobeaker.x;
            this.arrow1.y= this.seedtobeaker.y-250;
            break;   
            case 5:
            this.arrow1.x= this.needle.x;
            this.arrow1.y= this.needle.y-50;
            break; 
            case 6:
            this.arrow1.x= this.watchglass.x;
            this.arrow1.y= this.watchglass.y-50;
            break; 
            case 7:
            this.arrow1.x= this.Seedseparation.x;
            this.arrow1.y= this.Seedseparation.y-50;
            break; 
            case 8:
            this.arrow1.x= this.Glassplate.x;
            this.arrow1.y= this.Glassplate.y-50;
            break; 
            case 9:
            this.arrow1.x= this.needle1.x;
            this.arrow1.y= this.needle1.y-50;
            break; 
            case 10:
              this.arrow1.visible=false;
              this.arrow2=this.game.add.sprite(0,0, 'arrow');
   this.arrow2.anchor.set(.5);
   this.arrow2.animations.add("frameall");
   this.arrow2.animations.play("frameall",10,true);
           this.arrow2.x= this.microscope.x+50
            this.arrow2.y= this.microscope.y-120;
            break; 
            case 11:
              this.arrow1.x= this.microscope.x
              this.arrow1.y= this.microscope.y-250;
            break;      
    }
    },
    ProcedureText:function(vals){
      switch(vals){
            case 1:
            voice.stop();
            voice=this.game.add.audio("step0",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Take the seeds and put it into the beaker.";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 2:
            voice.stop();
            voice=this.game.add.audio("step1",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Soak the seeds and keep them overnight";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 3:
            voice.stop();
            voice=this.game.add.audio("step2",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Take the forceps to pick one seed from the beaker";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;

            case 4:
            voice.stop();
            voice=this.game.add.audio("step3",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Take the needle and place on the watchglass";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 5:
            voice.stop();
            voice=this.game.add.audio("step4",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Now peel off the outer skin of the seed";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 6:
            voice.stop();
            voice=this.game.add.audio("step5",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Now place the seed on to the slide";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 7:
            voice.stop();
            voice=this.game.add.audio("step6",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Take the needle and place it on the slide";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 8:
            voice.stop();
            voice=this.game.add.audio("step7",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Cut and open the seed using forceps and needle ";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 9:
            voice.stop();
            voice=this.game.add.audio("step8",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Take the slide and place it under the dissection microscope ";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 10:
            voice.stop();
            voice=this.game.add.audio("step9",1);
            voice.play();
            dialog_text="";
            dialog.destroy();
            dialog_text="Click on the eyepiece of the microscope to position";
            dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
            break;
            case 11:
              voice.stop();
              voice=this.game.add.audio("step10",1);
              voice.play();
              dialog_text="";
              dialog.destroy();
              dialog_text="Observe the parts of the embryo";
              dialog=this.game.add.text(dialogx,dialogy,dialog_text,fontStyle);
              break;
      }
    },
  ClockPart:function(){
  this.clocks=this.game.add.sprite(550,290,"Clock");
  this.clocks.anchor.set(.5);
  this.clocks.scale.set(.7);
  this.clocksNeedleBig=this.game.add.sprite(550,290,"ClockNeedleBig");
  this.clocksNeedleBig.anchor.set(.5,.85);
  this.clocksNeedleBig.scale.set(.7);
  this.clocksNeedleSmall=this.game.add.sprite(550,290,"ClockNeedleSmall");
  this.clocksNeedleSmall.anchor.set(.5,.8);
  this.clocksNeedleSmall.scale.set(.7);
  this.clockNum=0;
  this.clockStart=false
  },
  //Drag&drop
  DragOn(obj){
   
    obj.body.enable =false;
    
    currentobj=obj;
   if(currentobj==this.Gramspetridish){
      this.arrowShow=2;
      this.ArowShowing();
    }
    if(currentobj==this.forceps){
      this.arrowShow=4;
      this.ArowShowing();
    }
    if(currentobj==this.needle){
      this.arrowShow=6;
      this.ArowShowing();
    }
    if(currentobj==this.Seedseparation){
      this.arrowShow=8;
      this.ArowShowing();
    }
    if(currentobj==this.needle1){
      this.arrowShow=8;
      this.ArowShowing();
    }
    if(currentobj==this.Glassplate){
      this.arrowShow=10;
      this.ArowShowing();
    }
  },
  DropOut(obj){
   
    obj.body.enable =true;
  
  },

  //check collision
    detectCollision:function(){
      if(this.collideronbeaker!=null&&this.collideronbeaker.enableBody && currentobj!=null)
      {
          this.game.physics.arcade.overlap(this.Gramspetridish, this.collideronbeaker,function() {this.match_Obj()},null,this);
        
      } 
      if(this.collideronbeaker!=null&&this.collideronbeaker.enableBody && currentobj!=null)
      {
          this.game.physics.arcade.overlap(this.forceps, this.collideronbeaker,function() {this.match_Obj()},null,this);
        
      }
      if(this.collideronbeaker!=null&&this.collideronbeaker.enableBody && currentobj!=null)
      {
          this.game.physics.arcade.overlap(this.needle, this.collideronbeaker,function() {this.match_Obj()},null,this);
        
      }  
      if(this.collideronbeaker!=null&&this.collideronbeaker.enableBody&&this.Seedseparation!=null && currentobj!=null)
      {
          this.game.physics.arcade.overlap(this.Seedseparation, this.collideronbeaker,function() {this.match_Obj()},null,this);
        
      }  
      if(this.collideronbeaker!=null&&this.collideronbeaker.enableBody&&this.Seedseparation!=null && currentobj!=null)
      {
          this.game.physics.arcade.overlap(this.needle1, this.collideronbeaker,function() {this.match_Obj()},null,this);
        
      } 
         if(this.collideronbeaker!=null&&this.collideronbeaker.enableBody&&this.Seedseparation!=null && currentobj!=null)
      {
          this.game.physics.arcade.overlap(this.Glassplate, this.collideronbeaker,function() {this.match_Obj()},null,this);
        
      } 
      ////////////////////////////////////////
      if(currentobj!=null && currentobj.body.enable){
        currentobj.reset(currentobj.xpos,currentobj.ypos);//
       if(currentobj==this.Gramspetridish){
          this.arrowShow=1;
         this.ArowShowing()
        }
        if(currentobj==this.forceps){
          this.arrowShow=3;
         this.ArowShowing()
        }
        if(currentobj==this.needle){
          this.arrowShow=5;
         this.ArowShowing()
        }
        if(currentobj==this.Seedseparation){
          this.arrowShow=7;
         this.ArowShowing()
        }
        if(currentobj==this.needle1){
          this.arrowShow=9;
         this.ArowShowing()
        }
        if(currentobj==this.Glassplate){
          if(this.arrow2!=null){
          this.arrow2.destroy();
          }
          this.arrowShow=8;
         this.ArowShowing()
        }
       }
    
    },

  //collision occur
    match_Obj:function(){
      if(currentobj.inputEnabled){
      currentobj.inputEnabled=false;
      currentobj.input.enableDrag(false);
      currentobj.body.enable=false;
      this.arrow1.visible=false;
      this.arrow1.animations.play("frameidle",10,false);
      if(currentobj==this.Gramspetridish)
      {
        this.collideronbeaker.visible=false;
        this.Gramspetridish.visible=false;
        this.dishseed=this.game.add.sprite(1050,520,"GramsSeed_from_dish");
        this.dishseed.anchor.set(.5);
        this.dishseed.scale.set(.8);
        this.anims1=this.dishseed.animations.add('frameall');
        this.dishseed.animations.play('frameall', 10,false);
        this.grampetrTimer= this.game.time.events.add(500,this.showSeedbeaker,this);
        this.anims1.onComplete.add(function () {
          
          var t= this.game.add.tween( this.dishseed).to({x:  -100, y:  this.dishseed.y}, 500, Phaser.Easing.In, true);
          
          t.onComplete.add(function () {
            this.dishseed.destroy();
            currentobj=null;
          }.bind(this));
          
        }.bind(this));
    
      }
      if(currentobj==this.forceps)
      {
        this.forceps.visible=false;
        this.seedtobeaker.destroy();
        this.seedtobeaker=this.game.add.sprite(this.beakerX,this.beakerY-160,"beakergrams");
        this.seedtobeaker.anchor.set(.5);
       
        this.anims1=this.seedtobeaker.animations.add('frameall');
        this.seedtobeaker.animations.play('frameall', 10,false);
        this.anims1.onComplete.add(function () {
          this.seedtobeaker.destroy();
          this.seedtobeaker=this.game.add.sprite(this.beakerX,this.beakerY,"Grams_Germination");
          this.seedtobeaker.anchor.set(.5);
          this.seedtobeaker.animations.add('frameall',[8]);
          this.removecover=this.game.add.sprite(this.beakerX,this.beakerY-450,"Removing_cover");
          this.removecover.anchor.set(.5);
          this.removecover.animations.add('frameidle',[0]);
         
          var t1= this.game.add.tween( this.seedtobeaker).to({x: -200, y:  this.beakerY}, 1300, Phaser.Easing.In, true);
          var t11= this.game.add.tween( this.beakerback).to({x:-200, y:  this.beakerY}, 1300, Phaser.Easing.In, true);
          var t2= this.game.add.tween( this.watchglass).to({x: this.watchglass.xpos, y:  this.watchglass.ypos-100}, 500, Phaser.Easing.In, true);
          var t22= this.game.add.tween( this.removecover).to({x: this.beakerX-50, y:  this.beakerY-10}, 500, Phaser.Easing.In, true);
          t22.onComplete.add(function () {
            this.collideronbeaker.y=this.collideronbeaker.y+300
            this.needle.inputEnabled = true;
            this.needle.input.useHandCursor = true;
            this.needle.input.enableDrag(true);
            this.ProcedureText(4);
            this.arrowShow=5;
            this.ArowShowing()
            currentobj=null;
         
          }.bind(this));
        }.bind(this));
      }
      if(currentobj==this.needle)
      {
        this.needle.visible=false;
        this.anims2= this.removecover.animations.add('frame2',[0,1]);
        this.removecover.animations.play('frame2', 10,false);
        this.ProcedureText(5);
        this.zoomSeed();
       
      }
      if(currentobj==this.Seedseparation)
      {
        this.Seedseparation.x= this.Glassplate.x;
        this.Seedseparation.y= this.Glassplate.y-150;
        this.needle1.inputEnabled = true;
        this.needle1.input.useHandCursor = true;
        this.needle1.input.enableDrag(true);
        this.collideronbeaker.visible=true;
       this.collideronbeaker.alpha=.001;
        var t1= this.game.add.tween( this.watchglass).to({alpha:0}, 500, Phaser.Easing.In, true);
        var t111= this.game.add.tween( this.seedonplate).to({alpha:0}, 100, Phaser.Easing.In, true);
        this.ProcedureText(7);
        this.arrowShow=9;
        this.ArowShowing()
        currentobj=null;
      }
      if(currentobj==this.needle1)
      {
        console.log("dddeeeee");
        this.needle1.visible=false;
       
        this.zoomSeedWithglass();
        this.anims2=this.Seedseparation.animations.add('frame2',[0,1]);
        this.Seedseparation.animations.play('frame2',20,false);
        this.ProcedureText(8);
      /*  this.timerseed= this.game.time.events.add(1000,this.Seedsepearted,this);
        this.anims2.onComplete.add(function () {
         
          currentobj=null;
        }.bind(this));*/
      }
      if(currentobj==this.Glassplate)
      {
        if(this.arrow2!=null){
          this.arrow2.destroy();
          }
        this.Glassplate.destroy();
        this.Glass3.visible=true;
      //  this.collideronbeaker.destroy();
       
      this.collideronbeaker.inputEnabled = true;
        this.collideronbeaker.input.useHandCursor = true;
        this.arrowShow=11;
        this.ArowShowing(); 
        this.ProcedureText(10);
        
        this.collideronbeaker.x=this.microscope.x;
        this.collideronbeaker.y=this.microscope.y-180;
        this.collideronbeaker.scale.setTo(5,2);
        this.collideronbeaker.alpha=.001
        this.collideronbeaker.events.onInputDown.add(function () {
          this.arrow1.visible=false;
          currentobj=null;
          this.collideronbeaker.input.useHandCursor = false;
          this.collideronbeaker.inputEnabled = false;
          this.collideronbeaker.destroy();
      this.arrow1.animations.play("frameidle",10,false);
          this.anims2=this.microscopeClipAnim.animations.add('frameall');
          this.microscopeClipAnim.animations.play('frameall',20,false);
          this.anims2.onComplete.add(function () {
         
             this.ZoomMicroscope();
          }.bind(this));
        }, this)
      //  this.game.time.events.add(1000,this.TimerforMicroscopeon,this);
       // this.microscopeClipAnim.animations.play('frameall',20,false);
        currentobj=null;
      }
    }
    },
   
  zoomSeed:function(){
    this.watchglass.yposed=this.watchglass.y;
    this.removecover.yposed=this.removecover.y;
    var t1= this.game.add.tween( this.watchglass.scale).to({x:2,y:2.5}, 500, Phaser.Easing.In, true);
    var t11= this.game.add.tween( this.watchglass).to({x: this.watchglass.x,y: this.watchglass.y+50}, 500, Phaser.Easing.In, true);
    var t2= this.game.add.tween( this.removecover.scale).to({x:1.8,y:1.8}, 500, Phaser.Easing.In, true);
    var t2= this.game.add.tween( this.removecover).to({x: this.removecover.x,y: this.removecover.y-50}, 500, Phaser.Easing.In, true);
    t2.onComplete.add(function () {
      this.anims2= this.removecover.animations.add('frameall');
      this.removecover.animations.play('frameall', 10,false);
      this.collideronbeaker.visible=false;
        this.anims2.onComplete.add(function () {
         
          this.game.time.events.add(1000,this.zoomOutSeed,this);
         
        }.bind(this));
      }.bind(this));
  },
  zoomOutSeed: function(){
    var t1= this.game.add.tween( this.watchglass.scale).to({x:1,y:1}, 500, Phaser.Easing.In, true);
    var t11= this.game.add.tween( this.watchglass).to({x: this.watchglass.x,y:this.watchglass.yposed}, 500, Phaser.Easing.In, true);
    var t2= this.game.add.tween( this.removecover.scale).to({x:1,y:1}, 500, Phaser.Easing.In, true);
    var t21= this.game.add.tween( this.removecover).to({x: this.removecover.x,y: this.removecover.yposed}, 500, Phaser.Easing.In, true);
    t2.onComplete.add(function () {
      this.game.time.events.add(1000,this.deleayForafterzoomoutseed,this);
       
    }.bind(this));
  },
  deleayForafterzoomoutseed: function(){
    this.watchglass.scale.set(1);
    this.seedonplate=this.game.add.sprite(this.beakerX-50,this.beakerY+130,"seedwithcover");
    this.seedonplate.anchor.set(.5);
    this.removecover.visible=false;
  
    this.Seedseparation=this.game.add.sprite(this.beakerX,this.beakerY-10,"Seed_separation");
    this.Seedseparation.anchor.set(.5);
    this.Seedseparation.xpos= this.Seedseparation.x;
    this.Seedseparation.ypos= this.Seedseparation.y;
    this.Seedseparation.animations.add('frameidle',[0]);
    this.Seedseparation.inputEnabled = true;
    this.game.physics.arcade.enable(this.Seedseparation);
    this.Seedseparation.input.useHandCursor = true;
    this.Seedseparation.input.enableDrag(true);
    this.Seedseparation.events.onDragStart.add(function() {this.DragOn(this.Seedseparation)}, this);
    this.Seedseparation.events.onDragStop.add(function() {this.DropOut(this.Seedseparation)}, this);
   
    this.Glassplate.visible=true;
    this.Seedseparation.collidername=this.colliderOnSeed;
    this.collideronbeaker.visible=true;
    this.collideronbeaker.scale.setTo(6,1);
    this.collideronbeaker.y=this.Glassplate.ypos;
    var t1= this.game.add.tween( this.seedtobeaker).to({x: -200, y:  this.beakerY}, 500, Phaser.Easing.In, true);
    var t11= this.game.add.tween( this.beakerback).to({x: -200, y:  this.beakerY}, 500, Phaser.Easing.In, true);
    var t2= this.game.add.tween(  this.Glassplate).to({x:  this.Glassplate.xpos, y:   this.Glassplate.ypos}, 500, Phaser.Easing.In, true);
    this.needle1.visible=true;
    this.needle1.x=this.needle.x;
    this.needle1.y=this.needle.y;
  
    this.needle.visible=false;
    var t3= this.game.add.tween(  this.needle1).to({x:  350, y:   this.needle1.ypos}, 500, Phaser.Easing.In, true);
    t1.onComplete.add(function () {
      this.seedtobeaker.destroy();
      this.beakerback.destroy();
      this.needle.destroy();
      this.ProcedureText(6);
      this.arrowShow=7;
      this.ArowShowing()
     currentobj=null;
    }.bind(this));
  },
  zoomSeedWithglass: function(){
   
  
    this.Seedseparation.yposed=this.Seedseparation.y;
    var t1= this.game.add.tween( this.Glassplate.scale).to({x:2,y:2.5}, 500, Phaser.Easing.In, true);
   // var t11= this.game.add.tween( this.Glassplate).to({x: this.Glassplate.x,y: this.watchglass.y+100}, 500, Phaser.Easing.In, true);
    var t2= this.game.add.tween( this.Seedseparation.scale).to({x:1.8,y:1.8}, 500, Phaser.Easing.In, true);

    var t21= this.game.add.tween( this.Seedseparation).to({x:this.Seedseparation.x,y:this.Seedseparation.y-100}, 500, Phaser.Easing.In, true);
    t2.onComplete.add(function () {
      this.anims2=this.Seedseparation.animations.add('frameall');
      this.Seedseparation.animations.play('frameall',20,false);
      this.anims2.onComplete.add(function () {
        this.timerseed= this.game.time.events.add(1000,this.Seedsepearted,this);
        //  currentobj=null;
        }.bind(this));
      }.bind(this));
    
  // 
  
  },
  Seedsepearted: function(){
    this.zoomOutSeedWithglass();
   
   
  },
  zoomOutSeedWithglass: function(){
   
    var t1= this.game.add.tween( this.Glassplate.scale).to({x:1,y:1}, 500, Phaser.Easing.In, true);
   // var t11= this.game.add.tween( this.Glassplate).to({x: this.Glassplate.x,y: this.watchglass.y+100}, 500, Phaser.Easing.In, true);
    var t2= this.game.add.tween( this.Seedseparation.scale).to({x:1,y:1}, 500, Phaser.Easing.In, true);

    var t21= this.game.add.tween( this.Seedseparation).to({x:this.Seedseparation.x,y:this.Seedseparation.yposed}, 500, Phaser.Easing.In, true);
    t2.onComplete.add(function () {
      this.timerseed= this.game.time.events.add(1000,this.AdingMicroscopefunc,this);
  
     }.bind(this));
  },
  AdingMicroscopefunc: function(){
    this.Glassplate.destroy();
    this.Seedseparation.destroy();
    this.seedonplate.destroy();
    this.needle1.visible=true;
    this.forceps.visible=true;
    this.forceps.x= this.needle1.x;
    this.forceps.y= this.needle1.y;
    var t1= this.game.add.tween( this.needle1).to({alpha:0}, 200, Phaser.Easing.In, true);
    var t2= this.game.add.tween( this.forceps).to({alpha:0}, 200, Phaser.Easing.In, true);
    this.AddMicroscope();
    currentobj=null;
   
    var t21= this.game.add.tween( this.watchglass).to({alpha:0}, 200, Phaser.Easing.In, true);
    t1.onComplete.add(function () {
      this.needle1.destroy();
      this.forceps.destroy();
      this.watchglass.destroy();
     
    }.bind(this));
  },
  AddMicroscope: function(){
    this.microscope=this.game.add.sprite(this.beakerX,this.beakerY,"Micro_body");
    this.microscope.anchor.set(.5);
    this.microscope.scale.set(.8);
    this.microscope.alpha=0;
    this.Glass3=this.game.add.sprite(-10,-150,"Glass3");
    this.Glass3.anchor.set(.5); 
    this.Glass3.scale.set(.8);
    this.Glass3.visible=false;
    this.microscope.addChild(this.Glass3);
  

    this.microscopeClip1=this.game.add.sprite(-65,-185,"Micro_Clip_right");
    this.microscopeClip1.anchor.set(.5);
   // this.microscopeClip1.alpha=0;
    this.microscopeClip2=this.game.add.sprite(75,-160,"Micro_Clip_right");
    this.microscopeClip2.anchor.set(.5);
    this.microscope.addChild(this.microscopeClip1);
    this.microscope.addChild(this.microscopeClip2);
    this.microscopeClipAnim=this.game.add.sprite(5,-230,"Micro_animation");
    this.microscopeClipAnim.anchor.set(.5);
    this.microscopeClipAnim.scale.set(.8);
    this.microscope.addChild(this.microscopeClipAnim);
   
    this.anims2=this.microscopeClipAnim.animations.add('frameall');
    //this.Seedseparation.animations.play('frameall',20,false);
   
    this.collideronbeaker.x=this.microscope.x;
    this.collideronbeaker.y=this.microscope.y-100;
    this.collideronbeaker.scale.setTo(5,2);
    this.collideronbeaker.alpha=.001
    this.Glassplate=this.game.add.sprite(1000,980,"Glass1");
    this.Glassplate.anchor.set(.5); 
    this.Glassplate.scale.set(.8);
    var t6= this.game.add.tween( this.Glassplate).to({x:this.Glassplate.x-400,y:this.Glassplate.y}, 200, Phaser.Easing.In, true);
    this.Glassplate.xpos=  this.Glassplate.x-400
    this.Glassplate.ypos=  this.Glassplate.y;
    this.Glassplate.inputEnabled = true;
    this.game.physics.arcade.enable(this.Glassplate);
    this.Glassplate.input.useHandCursor = true;
    this.Glassplate.input.enableDrag(true);
    this.Glassplate.events.onDragStart.add(function() {this.DragOn(this.Glassplate)}, this);
    this.Glassplate.events.onDragStop.add(function() {this.DropOut(this.Glassplate)}, this);
    var t1= this.game.add.tween( this.microscope).to({alpha:1}, 1000, Phaser.Easing.In, true);
   // var t2= this.game.add.tween( this.microscopeClip1).to({alpha:1}, 1000, Phaser.Easing.In, true);
   // var t3= this.game.add.tween( this.microscopeClip2).to({alpha:1}, 1000, Phaser.Easing.In, true);
    t6.onComplete.add(function () {
      this.ProcedureText(9);
      this.arrowShow=8;
      this.ArowShowing(); 
  
  }.bind(this));
  
  },
  ZoomMicroscope: function(){

    this.clocksNeedleSmall.visible=false;
   this.clocksNeedleBig.visible=false;
   this.clocks.destroy();
  //  var t1= this.game.add.tween( this.microscopeClipAnim.scale).to({x:2,y:2.5}, 500, Phaser.Easing.In, true);
    var t1= this.game.add.tween( this.microscope.scale).to({x:1.5,y:1.5}, 1000, Phaser.Easing.In, true);
 //  var t2= this.game.add.tween( this.microscope).to({x:this.microscope.x-100,y:this.microscope.y+1200}, 1500, Phaser.Easing.In, true);
   t1.onComplete.add(function () {
      //	A mask is a Graphics object
      this.game.time.events.add(500,this.ShowSeedZoom,this);
   
      }.bind(this));
  },
  ShowTopMicroscope:function(){
    var t2= this.game.add.tween( this.microscope).to({alpha:0}, 1000, Phaser.Easing.In, true);
    //var t3= this.game.add.tween( bg).to({alpha:0}, 1500, Phaser.Easing.In, true);  
   
    var t21= this.game.add.tween(bg).to({alpha:0}, 500, Phaser.Easing.In, true);

    this.seedtopmicro=this.game.add.sprite(this.beakerX,this.beakerY-200,"Mocroscope_top_lens");
    this.seedtopmicro.anchor.set(.5);
    var t22= this.game.add.tween(this.seedtopmicro.scale).to({x:2,y:2}, 500, Phaser.Easing.In, true);
    t22.onComplete.add(function () {
    this.game.time.events.add(500,this.ShowSeedZoom,this);
       }.bind(this));
  },
  ShowSeedZoom:function(){
   //var t21= this.game.add.tween(this.seedtopmicro).to({alpha:0}, 500, Phaser.Easing.In, true);
  var t2= this.game.add.tween( this.microscope).to({alpha:0}, 1000, Phaser.Easing.In, true);
    //var t3= this.game.add.tween( bg).to({alpha:0}, 1500, Phaser.Easing.In, true);  
   
    var t21= this.game.add.tween(bg).to({alpha:0}, 500, Phaser.Easing.In, true);

    t21.onComplete.add(function () {
      this.plate1=this.game.add.sprite(850,600,"Grams_Disection");
     this.plate1.anchor.set(.5); 
     this.plate1.scale.set(.5);
     this.plate1.alpha=.7;
     this.ProcedureText(11);
     this.pic1=this.game.add.sprite(160,-50,"Plumule");
     this.pic1.anchor.set(.5); 
     this.plate1.addChild(this.pic1);
   
     this.pic2=this.game.add.sprite(160,50,"Radicle");
     this.pic2.anchor.set(.5); 
     this.plate1.addChild(this.pic2);
     this.pic3=this.game.add.sprite(-275,0,"Cotyledon");
     this.pic3.anchor.set(.5); 
     this.plate1.addChild(this.pic3);
     this.pic1.alpha=0;
     this.pic2.alpha=0;
     this.pic3.alpha=0;
     var t41= this.game.add.tween( this.plate1).to({alpha:1}, 2000, Phaser.Easing.In, true); 
     var t4= this.game.add.tween( this.plate1.scale).to({x:2,y:2}, 2000, Phaser.Easing.In, true);   
     t4.onComplete.add(function () {
      var t51= this.game.add.tween( this.pic1).to({alpha:1}, 1000, Phaser.Easing.In, true); 
      this.game.time.events.add(2000,this.ShowNextPic,this);
    }.bind(this));
      }.bind(this));
    

  },
  ShowNextPic: function(){
    var t51= this.game.add.tween( this.pic2).to({alpha:1}, 1000, Phaser.Easing.In, true); 
      this.game.time.events.add(2000,this.ShowNextPic1,this);
  },  
  ShowNextPic1: function(){
    var t51= this.game.add.tween( this.pic3).to({alpha:1}, 1000, Phaser.Easing.In, true); 
    play.visible=true;
  }, 
  showSeedbeaker: function()
    {
      this.game.time.events.remove(this.grampetrTimer);
      this.Beaker.destroy();
        this.seedtobeaker=this.game.add.sprite(this.beakerX,this.beakerY,"GramsSeed_to_Beaker");
        this.seedtobeaker.anchor.set(.5);
      
        this.anims=this.seedtobeaker.animations.add('frameall');
        this.seedtobeaker.animations.play('frameall', 10,false);
        this.anims.onComplete.add(function () {
          this.Gramspetridish.destroy();
          this.seedtobeaker.destroy();
          this.seedtobeaker=this.game.add.sprite(this.beakerX,this.beakerY,"Grams_Germination");
          this.seedtobeaker.anchor.set(.5);
          this.ProcedureText(2);
          this.anims=this.seedtobeaker.animations.add('frameall');
          this.seedtobeaker.animations.play('frameall', 1,false);
            this.clockStart=true;
        }.bind(this));
    },

  
    MoveAngle:function(){
      

      this.clocksNeedleBig.angle += 100* DeltaTime;
     
      if(this.clocksNeedleBig.angle>=-10&&this.clocksNeedleBig.angle<=-2){
        this.clockNum++;
       
      }
      if( this.clockNum>=1){
        this.clocksNeedleBig.angle=0;
        this.AddForceps();
        this.clockStart=false;
      }
    },
    AddForceps: function(){
      this.forceps.visible=true;
      var t1= this.game.add.tween( this.forceps).to({x: this.forceps.xpos, y:  this.forceps.ypos}, 1000, Phaser.Easing.In, true);
      this.needle.visible=true;
      var t2= this.game.add.tween( this.needle).to({x: this.needle.xpos, y:  this.needle.ypos}, 1000, Phaser.Easing.In, true);
      this.watchglass.visible=true;
      var t3= this.game.add.tween( this.watchglass).to({x: this.watchglass.xpos, y:  this.watchglass.ypos}, 1000, Phaser.Easing.In, true);

      this.forceps.inputEnabled = true;
      this.forceps.input.useHandCursor = true;
      this.forceps.input.enableDrag(true);
      this.collideronbeaker.visible=true;
      t1.onComplete.add(function () {
        this.ProcedureText(3);
        this.arrowShow=3;
        this.ArowShowing()
    
    }.bind(this));
    
    },


	update: function()
	{
    this.detectCollision();
    DeltaTime=this.game.time.elapsed/1000;
   if(this.clockStart){
    this.MoveAngle();
   }
   
    
	},
  
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
