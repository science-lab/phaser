var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
         this.game.load.image("bg1","assets/Bg_1.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('arrow_1', 'assets/Dicot_seed/arrow.png');
        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Dicot_seed//////////////////////////////////////////////////////////

this.game.load.image('Clock', 'assets/Dicot_seed/Clock.png');
this.game.load.image('ClockNeedleBig', 'assets/Dicot_seed/Clock_Needle_Big.png');
this.game.load.image('ClockNeedleSmall', 'assets/Dicot_seed/Clock_Needle_Small.png');
this.game.load.image('Beaker', 'assets/Dicot_seed/Beaker.png');
this.game.load.image('Beaker_back', 'assets/Dicot_seed/Beaker_back.png');
this.game.load.image('Glass1', 'assets/Dicot_seed/Glass 1.png');
this.game.load.image('Glass2', 'assets/Dicot_seed/Glass 2.png');
this.game.load.image('Glass3', 'assets/Dicot_seed/Glass 3.png');
this.game.load.image('Gramspetridish', 'assets/Dicot_seed/Grams Petridish.png');
this.game.load.image('Glassplate', 'assets/Dicot_seed/Glassplate.png');
this.game.load.image('Watchglass', 'assets/Dicot_seed/Watch glass.png');
this.game.load.image('Micro_body', 'assets/Dicot_seed/Micro_body.png');
this.game.load.image('Micro_body1', 'assets/Dicot_seed/Micro_body1.png');
this.game.load.image('Micro_Clip_right', 'assets/Dicot_seed/Micro_Clip_right.png');
this.game.load.image('forceps', 'assets/Dicot_seed/forceps.png');
this.game.load.image('needle', 'assets/Dicot_seed/needle.png');
this.game.load.image('seedwithcover', 'assets/Dicot_seed/seedwithcover1.png');
this.game.load.image('Grams_Disection-01', 'assets/Dicot_seed/Digram 2.png');
this.game.load.image('Grams-01', 'assets/Dicot_seed/Digram 1.png');
this.game.load.image('Grams_Disection', 'assets/Dicot_seed/Grams_Disection.png');

this.game.load.image('Cotyledon', 'assets/Dicot_seed/Cotyledon.png');

this.game.load.image('Plumule', 'assets/Dicot_seed/Plumule.png');
this.game.load.image('Radicle', 'assets/Dicot_seed/Radicle.png');
this.game.load.image('Mocroscope_top_lens', 'assets/Dicot_seed/Mocroscope_top_lens.png');

this.game.load.atlasJSONHash('arrow', 'assets/Dicot_seed/arrow.png', 'assets/Dicot_seed/arrow.json');
this.game.load.atlasJSONHash('Grams_Germination', 'assets/Dicot_seed/Grams_Germination.png', 'assets/Dicot_seed/Grams_Germination.json');
this.game.load.atlasJSONHash('GramsSeed_from_dish', 'assets/Dicot_seed/GramsSeed_from_dish.png', 'assets/Dicot_seed/GramsSeed_from_dish.json');
this.game.load.atlasJSONHash('GramsSeed_to_Beaker', 'assets/Dicot_seed/GramsSeed_to_Beaker.png', 'assets/Dicot_seed/GramsSeed_to_Beaker.json');
this.game.load.atlasJSONHash('Micro_animation', 'assets/Dicot_seed/Micro_animation.png', 'assets/Dicot_seed/Micro_animation.json');
this.game.load.atlasJSONHash('Removing_cover', 'assets/Dicot_seed/Removing_cover.png', 'assets/Dicot_seed/Removing_cover.json');
this.game.load.atlasJSONHash('Seed_separation', 'assets/Dicot_seed/Seed_separation.png', 'assets/Dicot_seed/Seed_separation.json');
this.game.load.atlasJSONHash('beakergrams', 'assets/Dicot_seed/beakergrams.png', 'assets/Dicot_seed/beakergrams.json');

      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      //this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('step0','assets/audio/step0.mp3');
      this.game.load.audio('step1','assets/audio/step1.mp3');
      this.game.load.audio('step2','assets/audio/step2.mp3');
      this.game.load.audio('step3','assets/audio/step3.mp3');
      this.game.load.audio('step4','assets/audio/step4.mp3');
      this.game.load.audio('step5','assets/audio/step5.mp3');
      this.game.load.audio('step6','assets/audio/step6.mp3');
      this.game.load.audio('step7','assets/audio/step7.mp3');
      this.game.load.audio('step8','assets/audio/step8.mp3');
      this.game.load.audio('step9','assets/audio/step9.mp3');
      this.game.load.audio('step10','assets/audio/step10.mp3');
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
 ///this.game.state.start("Experiment_1");//Starting the gametitle state
    //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
    //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
     // this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

