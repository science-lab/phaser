var boot = function(game){
	//this.orientated: false
}

boot.prototype = {

preload: function (){

},
init: function () {

        this.input.maxPointers = 1;
        this.stage.disableVisibilityChange = true;
        this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;//SHOW_ALL;
        //this.scale.setMinMax(400, 200, 800, 400);
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;

        if (!this.game.device.desktop)
        {
            this.scale.forceOrientation(true, false);
            this.scale.setResizeCallback(this.gameResized, this);
            this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
            this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
        }
         //fps
    this.game.time.advancedTiming = true;

    //HACK TO PRELOAD A CUSTOM FONT
    this.game.add.text(0, 0, "hack", {font:"1px Segoe UI", fill:"#FFFFFF"}); 
    this.game.add.text(0, 0, "hack", {font:"1px Segoe UI Bold", fill:"#FFFFFF"}); 
    },
create: function (){

/*this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
this.scale.pageAlighnHorizontally = true;
this.scale.pageAlignVertically = true;
//this.scale.setScreenSize(true);
if (!this.game.device.desktop)
{
    this.scale.forceOrientation(true, false);
    this.scale.setResizeCallback(this.gameResized, this);
    this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
    this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
}
*/

this.state.start("Preload");
},
gameResized: function (width, height) {
        //  This could be handy if you need to do any extra processing if the game resizes.
        //  A resize could happen if for example swapping orientation on a device or resizing the browser window.
        //  Note that this callback is only really useful if you use a ScaleMode of RESIZE and place it inside your main game state.
    },
enterIncorrectOrientation: function () {
        //this.orientated = false;
        //console.log("oooooooooooooo");
        document.getElementById('orientation').style.display = 'block';
    },

    leaveIncorrectOrientation: function () {
        //this.orientated = true;
        //console.log("kkkkkkkkkkkkk");
        document.getElementById('orientation').style.display = 'none';
    }
}