var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;

}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        //this.game.load.atlasJSONHash('ferrous_sulphate', 'assets/ferrous_sulphate.png', 'assets/ferrous_sulphate.json');
        //this.game.load.atlasJSONHash('burner_sprites', 'assets/burner_sprites.png', 'assets/burner_sprites.json');
      
        this.game.load.json('questions','data/questions1.json');
    
        
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
     
        
        
        this.game.load.image('transBackground','assets/transBackground.png');
         this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");   
        this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
   
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        ///////////////observation///////////////
        this.game.load.image('obs_base','assets/onion_peel/table_base.png');
        this.game.load.image('obs_table','assets/onion_peel/obs_table.png');
        this.game.load.image('obs_base_head','assets/onion_peel/dialogbox_head.png');
        this.game.load.image('obs_line','assets/onion_peel/obs_table_line.png');
        this.game.load.image('cell_image1','assets/onion_peel/cell_structure_1.png');
        this.game.load.image('cell_image2','assets/onion_peel/cell_structure_2.png');

        ///////////material required/////

        this.game.load.image('m_safranin','assets/onion_peel/materials/m_safranin.png');
        this.game.load.image('m_glycerinBottle','assets/onion_peel/materials/m_glycerin_bottle.png');
        this.game.load.image('m_dropper','assets/onion_peel/materials/m_dropper.png');
        this.game.load.image('m_petridish','assets/onion_peel/materials/m_petridish_water.png');
        this.game.load.image('m_forceps','assets/onion_peel/materials/m_forceps.png');
        this.game.load.image('m_glassSlide','assets/onion_peel/materials/m_glass_slide.png');
        this.game.load.image('m_coverslip','assets/onion_peel/materials/m_cover_slip.png');
        this.game.load.image('m_filterPaper','assets/onion_peel/materials/m_filter_paper.png');
        this.game.load.image('m_microscope','assets/onion_peel/materials/m_microscope.png');
        this.game.load.image('m_needle','assets/onion_peel/materials/m_needle.png');
        this.game.load.image('m_brush','assets/onion_peel/materials/m_brush.png');






        ////////////////////Experiment items////////////////
        this.game.load.image('bg','assets/onion_peel/Bg.png');
        this.game.load.image('bg_black','assets/onion_peel/Bg_black.png');
        this.game.load.image('bg_top','assets/onion_peel/Bg_top.png');
        this.game.load.image('microscope_top_view','assets/onion_peel/Mocroscope_top_view.png');
        this.game.load.image('microscope_cell_view','assets/onion_peel/onion_cell.png');

        this.game.load.image('microscope','assets/onion_peel/microscope.png');
        this.game.load.image('glass_slide','assets/onion_peel/glass_slide.png');
        this.game.load.image('petridish_with_water','assets/onion_peel/petridish_water.png');

        this.game.load.image('glycerine_bottle_back','assets/onion_peel/glycerin_bottle_back.png');
        this.game.load.image('glycerin_bottle','assets/onion_peel/glycerin_bottle.png');
        this.game.load.image('glass_slide_view2','assets/onion_peel/glassplate_Side_view.png');

        this.game.load.image('forceps','assets/onion_peel/forceps.png');
        this.game.load.image('onion_peel','assets/onion_peel/Onion_pice.png');
        this.game.load.image('forceps_peel','assets/onion_peel/forceps_peel.png');
        this.game.load.image('petridish_peel','assets/onion_peel/petridish_with_peel.png');
        this.game.load.image('cover_slip','assets/onion_peel/cover_slip.png');
        this.game.load.image('needle','assets/onion_peel/needle.png');
        this.game.load.image('glass_slide_glycerin','assets/onion_peel/glassSlide_glycerin.png');
        this.game.load.image('glass_slide_peel','assets/onion_peel/glass_slide_peel.png');
        this.game.load.image('brush','assets/onion_peel/Brush.png');
        this.game.load.image('glass_slide_final','assets/onion_peel/glassSlide_final.png');
        this.game.load.image('microscope_holder_left','assets/onion_peel/mocroscope_left_lock.png');
        this.game.load.image('microscope_holder_right','assets/onion_peel/mocroscope_right_lock.png');
        this.game.load.image('onion','assets/onion_peel/onion.png');


       
        

       this.game.load.atlasJSONHash('eyePiece_lens_sprite', 'assets/onion_peel/animations/eyePiece_rotating_sprite.png', 'assets/onion_peel/animations/eyePiece_rotating_sprite.json');
       this.game.load.atlasJSONHash('safranin_bottle_sprite', 'assets/onion_peel/animations/safranine_pour_sprite.png', 'assets/onion_peel/animations/safranine_pour_sprite.json');
       this.game.load.atlasJSONHash('water_dropper', 'assets/onion_peel/animations/water_dropper_sprite.png', 'assets/onion_peel/animations/water_dropper_sprite.json');
       this.game.load.atlasJSONHash('water_drop', 'assets/onion_peel/animations/water_drop_sprite.png', 'assets/onion_peel/animations/water_drop_sprite.json');
       this.game.load.atlasJSONHash('glass_slide_brush_sprite', 'assets/onion_peel/animations/glassSlide_brush_sprite.png', 'assets/onion_peel/animations/glassSlide_brush_sprite.json');
       this.game.load.atlasJSONHash('petridish_liquid_sprite', 'assets/onion_peel/animations/safranine_petridish_sprite.png', 'assets/onion_peel/animations/safranine_petridish_sprite.json');
       this.game.load.atlasJSONHash('safranin_drop', 'assets/onion_peel/animations/safranine_drop_sprite.png', 'assets/onion_peel/animations/safranine_drop_sprite.json');
       this.game.load.atlasJSONHash('forceps_peeling_sprite', 'assets/onion_peel/animations/onion_peel_sprite.png', 'assets/onion_peel/animations/onion_peel_sprite.json');
       this.game.load.atlasJSONHash('forceps_peel_drop', 'assets/onion_peel/animations/onion_peel_drop_sprite.png', 'assets/onion_peel/animations/onion_peel_drop_sprite.json');
       this.game.load.atlasJSONHash('forceps_peel_take', 'assets/onion_peel/animations/onion_peel_lift_sprite.png', 'assets/onion_peel/animations/onion_peel_lift_sprite.json');
       this.game.load.atlasJSONHash('glassSlide_forceps_peel_sprite', 'assets/onion_peel/animations/glassSlide_onion_sprite.png', 'assets/onion_peel/animations/glassSlide_onion_sprite.json');
       this.game.load.atlasJSONHash('filter_paper', 'assets/onion_peel/animations/filter_paper_sprite.png', 'assets/onion_peel/animations/filter_paper_sprite.json');  
       this.game.load.atlasJSONHash('glassSlide_needle_coverslip_sprite', 'assets/onion_peel/animations/glassSlide_coverSlip_sprite.png', 'assets/onion_peel/animations/glassSlide_coverSlip_sprite.json');  
       this.game.load.atlasJSONHash('onion_knife_sprite', 'assets/onion_peel/animations/onion_knife_sprite.png', 'assets/onion_peel/animations/onion_knife_sprite.json');                          

       this.game.load.atlasJSONHash('arrow', 'assets/onion_peel/animations/arrow.png', 'assets/onion_peel/animations/arrow.json');  
////////////////////////////////////Audio for concavemirror/////////////////////////

        this.game.load.audio('A_aim','assets/audio/onion_peel/A_aim.mp3');
        this.game.load.audio('A_exp_procedure1','assets/audio/onion_peel/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/onion_peel/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/onion_peel/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/onion_peel/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/onion_peel/A_exp_procedure5.mp3');
        this.game.load.audio('A_exp_procedure6','assets/audio/onion_peel/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/onion_peel/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/onion_peel/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/onion_peel/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/onion_peel/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/onion_peel/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/onion_peel/A_exp_procedure12.mp3');
        this.game.load.audio('A_exp_procedure13','assets/audio/onion_peel/A_exp_procedure13.mp3');
        this.game.load.audio('A_exp_procedure14','assets/audio/onion_peel/A_exp_procedure14.mp3');
        this.game.load.audio('A_exp_procedure15','assets/audio/onion_peel/A_exp_procedure15.mp3');
        this.game.load.audio('A_exp_procedure16','assets/audio/onion_peel/A_exp_procedure16.mp3');
        this.game.load.audio('A_exp_procedure17','assets/audio/onion_peel/A_exp_procedure17.mp3');
        this.game.load.audio('A_exp_procedure18','assets/audio/onion_peel/A_exp_procedure18.mp3');
        this.game.load.audio('A_exp_procedure19','assets/audio/onion_peel/A_exp_procedure19.mp3');
        this.game.load.audio('A_precautions','assets/audio/onion_peel/A_precautions.mp3');


        this.game.load.audio('A_procedure1','assets/audio/onion_peel/A_procedure1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/onion_peel/A_procedure2.mp3');

        this.game.load.audio('A_observations1','assets/audio/onion_peel/A_observations1.mp3');
        this.game.load.audio('A_observations2','assets/audio/onion_peel/A_observations2.mp3');
        this.game.load.audio('A_result','assets/audio/onion_peel/A_result.mp3');
 
  




       


	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
     //this.game.state.start("Viva");//Starting the gametitle state
   // this.game.state.start("Experiment_1");//Starting the gametitle state
     // this.game.state.start("Materials");//Simulation_hot1
     //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
   //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
     // this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");
      this.game.state.start("Aim");
      //this.game.state.start("Exp_Selection");

	}
}

