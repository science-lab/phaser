var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////

var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;

var fNames;
var isWatertaken;
var isGlycerineTaken;
var isLiquidTaken;
var isCellTaken;

var isTakenOnionPeel;
var isOnionPeelLifted;
var isItFirst;




}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
//  voice=this.game.add.audio("A_exp_procedure1",1);

 //this.arrangeScene();

 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0,0,'bg');
 bg.scale.setTo(1,1);

  // maskBg1 = this.game.add.graphics(0, 0);
  // maskBg1.lineStyle(10,0xff704d,1);
  // maskBg1.beginFill(0xffffcc);
  // maskBg1.drawRect(20, 40, 300, 700);
  // maskBg1.alpha=1;
 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////

  fNames=[];

   isWatertaken=false;
   
   isLiquidTaken=false;
  isCellTaken=false;

  isOnionPeelLifted=false;
  isTakenOnionPeel=false;
  isGlycerineTaken=false;
  isItFirst=true;

  // isMirrorStandMoving=false;
  // isScreenMoving=false;
  // pointerx=0;
  // screen_stand_scale_val=0;
  // mirror_stand_scale_val=0;
  // mirror_stand_minPos=30;
  // screen_stand_maxPos=1343;
  // image_maxPos=1289;
  // screen_and_stand_distance=70;
  currentobj=null;

  // isreading1Taken=false;
  // isreading2Taken=false;
  // isreading3Taken=false;
  // isAnyStandMoved=false;
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  // dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
  //     dialog_box.scale.setTo(.8,.7);
  //     dialog_text="Let's perform the experiment.";
      
  //     //procedure_voice1.play();
  //     dialog=this.game.add.text(70,40,dialog_text,fontStyle);
  //     this.game.time.events.add(Phaser.Timer.SECOND*.5,this.loadScene, this);

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "28px Segoe UI", fill: "#000000", align: "center"};//fontWeight:"bold" 
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};








   


////////////////////////////////////////////////////////////////////

 },





arrangeScene:function()
{
 microscope=this.game.add.sprite(500,220,'microscope');
 microscope.scale.setTo(-1,1);

 microscopeLens=this.game.add.sprite(290,650,'eyePiece_lens_sprite','Microscope_Lenz0001.png');
 microscopeLens.anchor.set(.5);
 microscopeLens.scale.setTo(-1,1);
 microscopeLens.animations.add('anim1',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],22,false,true);
 microscopeLens.animations.add('anim2',[14,13,12,11,10,9,8,7,6,5,4,3,2,1,0],22,false,true);
 microscopeLens.magnification=10;

//  microscopeLens.inputEnabled = true;
//  microscopeLens.input.useHandCursor = true;
//  microscopeLens.events.onInputDown.add(this.rotateObjectiveLens, this);
 //microscopeLens.animations.play('anim');

 glassSlide=this.game.add.sprite(500,900,'glass_slide');
 glassSlide.scale.set(.9);

 glassSlidePeelDropping=this.game.add.sprite(480,675,'glassSlide_forceps_peel_sprite','OnionCell_to_GlassPlate0001.png');
 glassSlidePeelDropping.scale.setTo(.7,.7);
 glassSlidePeelDropping.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],12,false,true);
//  glassSlidePeelDropping.animations.play('anim');
 glassSlidePeelDropping.visible=false;

 glassSlideBrush=this.game.add.sprite(500,604,'glass_slide_brush_sprite','Brush_to_glass_plate0024.png');
 glassSlideBrush.scale.setTo(.86,.85);
 glassSlideBrush.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27],22,false,true);
 //glassSlideBrush.animations.play('anim');
 glassSlideBrush.visible=false;

 glassSlideGlycerin=this.game.add.sprite(500,900,'glass_slide_glycerin');
 glassSlideGlycerin.scale.setTo(.9,.9);
 glassSlideGlycerin.visible=false;

 glassSlidePeel=this.game.add.sprite(520,913,'glass_slide_peel');
 glassSlidePeel.scale.setTo(.87,.89);
 glassSlidePeel.visible=false;

 glassSlideCoverSlipSprite=this.game.add.sprite(502,791,'glassSlide_needle_coverslip_sprite','Small_glassplate_to_glass0001.png');
 glassSlideCoverSlipSprite.animations.add('anim',[0,1,2,3,4,5,6],8,false,true);
 glassSlideCoverSlipSprite.scale.setTo(.86,.8);
 glassSlideCoverSlipSprite.visible=false;


 glassSlideFinal=this.game.add.sprite(510,899,'glass_slide_final');
 glassSlideFinal.scale.setTo(.865,.88);
 glassSlideFinal.visible=false;

 coverSlip=this.game.add.sprite(1000,900,'cover_slip');
 coverSlip.scale.setTo(.9,.9);

 filterPaperSprite=this.game.add.sprite(1300,950,'filter_paper','Paper0001.png');
 filterPaperSprite.anchor.set(.5);
 filterPaperSprite.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11],8,false,true);
 //filterPaperSprite.animations.play('anim');

 brush=this.game.add.sprite(1150,860,'brush');
 brush.scale.setTo(.5,.49);
 brush.anchor.set(.5);
 brush.angle=90;

 needle=this.game.add.sprite(1050,1000,'needle');
 needle.anchor.set(.5);
 needle.scale.setTo(.76,.8);



 coverSlipGroup=this.game.add.group();
 coverSlipGroup.add(coverSlip);
 coverSlipGroup.add(filterPaperSprite);
 coverSlipGroup.add(needle);
 coverSlipGroup.add(brush);
 coverSlipGroup.x=2500;

 petriDishWater=this.game.add.sprite(900,880,'petridish_with_water');

 petriDishLiquid=this.game.add.sprite(915,882,'petridish_liquid_sprite','Liquid_to_Watchglass0001.png');
 petriDishLiquid.animations.add('anim',[0,1,2,3,4,5],8,false,true);
 petriDishLiquid.scale.setTo(.9,.9);
 petriDishLiquid.visible=false;

 petriDishPeel=this.game.add.sprite(896,877,'petridish_peel');
 petriDishPeel.scale.setTo(1,1);
 petriDishPeel.visible=false;



 glycerineBottleBack=this.game.add.sprite(1556,634,'glycerine_bottle_back');
 glycerineBottleBack.scale.setTo(.8,1);

 glycerineBottle=this.game.add.sprite(1500,640,'glycerin_bottle');
 glycerineBottle.scale.setTo(.8,.8);

 glycerineDropper=this.game.add.sprite(1256,1004,'water_dropper','Water_To_Dropper0001.png');
 glycerineDropper.scale.set(.75,.75);
 glycerineDropper.anchor.set(.5);
 glycerineDropper.angle=90;
 glycerineDropper.animations.add('anim',[4,3,2,1,0],24,false,true);
 //glycerineDropper.visible=false;

 glycerineGroup=this.game.add.group();
 glycerineGroup.add(glycerineBottleBack);
 glycerineGroup.add(glycerineBottle);
 glycerineGroup.add(glycerineDropper);
 glycerineGroup.x=2500;



 safraninBottle=this.game.add.sprite(1350,760,'safranin_bottle_sprite','Liquid_bottle0001.png');
 safraninBottle.animations.add('anim',[0,1,2,3,4],20,false,true);
 safraninBottle.anchor.set(.5);
 safraninBottle.scale.set(.8);
 //safraninBottle.visible=false;

 safraninDrop1=this.game.add.sprite(1035,845,'safranin_drop','Liquid_Drop0001.png');
 safraninDrop1.scale.set(.5);
 safraninDrop1.animations.add('anim',[0,1,2,3,4],8,false,true);
 safraninDrop1.visible=false;
 //safraninDrop1.animations.play('anim');



 onionPeel=this.game.add.sprite(1510,873,'onion_peel');
 onionPeel.scale.setTo(.47,.5);
 onionPeel.alpha=1;
 onionPeel.visible=false;


 onionKnife=this.game.add.sprite(1375,520,'onion_knife_sprite','Onion_cell_cut0001.png');
 onionKnife.scale.setTo(1.75,1.7);
 onionKnife.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,
                          31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54],24,false,true);
 onionKnife.visible=false;
 //onionKnife.animations.play('anim');
  

 onion=this.game.add.sprite(1582,555,'onion');
 onion.scale.setTo(1.5,1.5);
 onion.alpha=1;


 forceps=this.game.add.sprite(1200,920,'forceps');//1256//1004
 forceps.scale.setTo(.5,.5);
 forceps.angle=-35;

 onionGroup=this.game.add.group();
 onionGroup.add(onionPeel);
 onionGroup.add(forceps);
 onionGroup.add(onion);
 onionGroup.add(onionKnife);
onionGroup.x=3500;

 forcepsPeelDrop=this.game.add.sprite(880,535,'forceps_peel_drop','Onion_drop0001.png');
 forcepsPeelDrop.scale.setTo(.55,.6);
 forcepsPeelDrop.animations.add('anim',[0,1,2,3,4,5],12,false,true);
 forcepsPeelDrop.visible=false;

 forcepsPeelTake=this.game.add.sprite(840,656,'forceps_peel_take','Onion_lift0001.png');
 forcepsPeelTake.scale.setTo(.73,.73);
 forcepsPeelTake.animations.add('anim',[0,1,2,3,4,5,6,7,8,9],12,false,true);
 forcepsPeelTake.visible=false;

 forcepsPeel=this.game.add.sprite(1200,950,'forceps_peel');
 forcepsPeel.scale.setTo(.6,.6);
 forcepsPeel.angle=-35;
 forcepsPeel.visible=false;

 forcepsPeelingSprite=this.game.add.sprite(1300,760,'forceps_peeling_sprite','Onion_pealing0001.png');
 forcepsPeelingSprite.scale.setTo(.6,.6);
 forcepsPeelingSprite.alpha=1;
 forcepsPeelingSprite.animations.add('anim',[0,1,2,3,4,5],12,false,true);
 forcepsPeelingSprite.visible=false;

 glycerineDrop=this.game.add.sprite(682,870,'water_drop','Water_Drop0001.png');
 glycerineDrop.animations.add('anim',[0,1,2],8,false,true);
 glycerineDrop.visible=false;
//glucerineDrop.animations.play('anim');



//  glassSlideWater=this.game.add.sprite(700,900,'glass_slide_water');
//  glassSlideWater.scale.set(.9);
//  glassSlideWater.visible=false;   

//this.game.time.events.add(Phaser.Timer.SECOND*5,this.loadScene, this);

glassSlide2=this.game.add.sprite(410,690,'glass_slide_view2');
glassSlide2.scale.setTo(-.6,.6);
glassSlide2.visible=false;

microscopeLeftHolder=this.game.add.sprite(385,700,'microscope_holder_left');
 microscopeLeftHolder.scale.setTo(-1,1);
 microscopeRightHolder=this.game.add.sprite(250,740,'microscope_holder_right');
 microscopeRightHolder.scale.setTo(-1,1);

colliderA=this.game.add.sprite(950,700,'collider');
colliderA.scale.set(4);
colliderA.alpha=0;
colliderA.visible=false;
this.game.physics.arcade.enable(colliderA);

colliderB=this.game.add.sprite(600,730,'collider');
colliderB.scale.set(4);
colliderB.alpha=0;
colliderB.visible=false;
this.game.physics.arcade.enable(colliderB);


colliderC=this.game.add.sprite(180,600,'collider');
colliderC.scale.set(4);
colliderC.alpha=0;
colliderC.visible=false;
this.game.physics.arcade.enable(colliderC);

colliderD=this.game.add.sprite(215,210,'collider');
colliderD.scale.setTo(2,4);
colliderD.alpha=0;
colliderD.visible=false;

  

colliderE=this.game.add.sprite(1500,750,'collider');
colliderE.scale.setTo(3,3);
colliderE.alpha=0;
colliderE.visible=false;
this.game.physics.arcade.enable(colliderE);

arrow=this.game.add.sprite(400,640,'arrow','arrow_0001.png');//170,200//200,330
//arrow.angle=90;
arrow.anchor.set(.5);
arrow.animations.add('anim',[0,1,2,3,4,5,6,7,8,9],28,true,true);
arrow.animations.play('anim');
arrow.visible=false;


bgTopView= this.game.add.sprite(0,0,'bg_top');
bgTopView.scale.setTo(1,1);
bgTopView.visible=false;



microscopeTopview=this.game.add.sprite(900,480,'microscope_top_view');
microscopeTopview.scale.setTo(1.2,1.2);
microscopeTopview.anchor.setTo(.5,.4);
// microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
microscopeTopview.visible=false;

bgBlack= this.game.add.sprite(0,0,'bg_black');
bgBlack.scale.setTo(1,1);
bgBlack.alpha=0;
// bgBlack.visible=false;
// bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Out, true);
 bgBlack.visible=false;

microscopeCellView=this.game.add.sprite(902,495,'microscope_cell_view');
microscopeCellView.anchor.set(.5);
microscopeCellView.scale.set(0);
microscopeCellView.alpha=0;

circle1 = this.game.add.graphics(0, 0);
////circle1.lineStyle(8, 0xffd900, 1);
circle1.beginFill(0xFF0000, .3);
circle1.drawCircle(902, 494, 430);
//circle1.scale.set(0.5);
microscopeCellView.mask=circle1;
circle1.visible=false;

// microscopeCellViewTween=this.game.add.tween(microscopeCellView.scale).to( {x:1,y:1}, 2500, Phaser.Easing.Out, true);
// microscopeCellViewTween=this.game.add.tween(microscopeCellView).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
 microscopeCellView.visible=false;
 boxGroup=this.game.add.group();
 box = this.game.add.graphics(0, 0);
 box.lineStyle(5,0xE67E22,.6);
 box.beginFill(0x000000,1);//9B59B6
 box.drawRect(30, 200, 325, 240);
 testHead=this.game.add.text(40,210,"Select lens:",headfontStyle);
 box1 = this.game.add.graphics(0, 0);
 //box1.lineStyle(5,0xffffff,.6);
 box1.beginFill(0xffffff,1);//9B59B6
 box1.drawRect(40, 275, 300, 50);
 box1.inputEnabled = true;
 box1.input.useHandCursor = true;
box1.events.onInputDown.add(this.clickOnbox1, this);
 box2 = this.game.add.graphics(0, 0);
 //box2.lineStyle(5,0xffffff,.6);
 box2.beginFill(0xffffff,1);//9B59B6
 box2.drawRect(40, 350, 300, 50);
 box2.inputEnabled = true;
 box2.input.useHandCursor = true;
box2.events.onInputDown.add(this.clickOnbox2, this);
 boxR = this.game.add.graphics(0, 0);
 //boxR.lineStyle(5,0xffffff,.6);
 boxR.beginFill(0x00ff00,.6);//9B59B6
 boxR.drawRect(0, 0, 300, 50);
 boxR.x=40;
 boxR.y=275;
 test1=this.game.add.text(50,280,"10X (Low power)",fontStyle_b);
 test2=this.game.add.text(50,355,"45X (High power)",fontStyle_b);

 backButton = this.game.add.sprite(130,450,'components','previuos_pressed.png');
 backButton.scale.setTo(.7,.7);
 backButton.inputEnabled = true;
 backButton.input.useHandCursor = true;
 backButton.events.onInputDown.add(this.toPrevious, this);


 boxGroup.add(box);
 boxGroup.add(box1);
 boxGroup.add(box2);
 boxGroup.add(boxR);
 boxGroup.add(testHead);
 boxGroup.add(test1);
 boxGroup.add(test2);
 boxGroup.add(backButton);
 boxGroup.visible=false;

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;


 dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
 dialog_box.scale.setTo(.8,.7);
 dialog_text="Let's perform the experiment.";
 
 //procedure_voice1.play();
 dialog=this.game.add.text(70,40,dialog_text,fontStyle);
 this.game.time.events.add(Phaser.Timer.SECOND*.5,this.loadScene, this);

//  colliderD.visible=true;
//       colliderD.inputEnabled = true;
//       colliderD.input.useHandCursor = true;
//       colliderD.events.onInputDown.add(this.clickOnEyePiece,this);
},






loadScene:function()
{
  voice=this.game.add.audio("A_exp_procedure1",1);
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.startExperiment, this);
},

startExperiment:function()
{
 voice.destroy();
  voice=this.game.add.audio("A_exp_procedure2",1);
  voice.play();
dialog.text="Add few drops of Safranin liquid to the petri dish containing water.";
arrow.x=1395;
arrow.y=620;
arrow.visible=true;
safraninBottle.events.onDragStart.add(function() {this.onDragStart(safraninBottle)}, this);
safraninBottle.events.onDragStop.add(function() {this.onDragStop(safraninBottle)}, this);
    safraninBottle.inputEnabled = true;
    safraninBottle.input.useHandCursor = true;
    safraninBottle.input.enableDrag(true);
    this.game.physics.arcade.enable(safraninBottle);
    safraninBottle.enableBody =true;

  // iceScreamStick.inputEnabled = true;
  // iceScreamStick.input.useHandCursor = true;
  // iceScreamStick.events.onInputDown.add(this.clickOnStick,this);
  // arrow.visible=true;
  // arrow.x=605;
  // arrow.y=880;

  // voice.destroy();
  // voice=this.game.add.audio("A_exp_procedure2",1);
  // voice.play();
  // dialog.text="Drag and drop scale on to the table.";
  // temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
  // temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
  // temp_scale.inputEnabled = true;
  // temp_scale.input.useHandCursor = true;
  // this.game.physics.arcade.enable(temp_scale);
  // temp_scale.input.enableDrag(true);

},

clickOnbox1:function(){
  if(microscopeLens.magnification==45){
    microscopeLens.magnification=10;
    boxR.y=275;
    cellTween1=this.game.add.tween(microscopeCellView.scale).to( {x:.42,y:.42}, 2000, Phaser.Easing.Linear.Out, true);
  }
},
clickOnbox2:function(){
  if(microscopeLens.magnification==10){
    microscopeLens.magnification=45;
    boxR.y=350;
    cellTween1=this.game.add.tween(microscopeCellView.scale).to( {x:1,y:1}, 2000, Phaser.Easing.Linear.Out, true);
  }
},



onDragStart:function(obj)
  {
    //obj.angle=0;
    console.log("iiiiii"+obj);
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==safraninBottle)
    {
      arrow.visible=false;
      arrow.x=1050;
      arrow.y=850;
      arrow.visible=true;

       currentobj.xp=1350;
       currentobj.yp=760;
      

        colliderA.inputEnabled=true;
        colliderA.enableBody=true;
        colliderA.visible=true;

        // waterDropper.frameName="Water_To_Dropper0005.png";
       
    }
    if(currentobj==forceps)
    {

      if(isTakenOnionPeel==false)
      {
        colliderE.inputEnabled=true;
        colliderE.enableBody=true;
        colliderE.visible=true;

        arrow.visible=false;
        arrow.x=1580;
        arrow.y=850;
        arrow.visible=true;

        currentobj.xp=1200;
        currentobj.yp=920;
        //isTakenOnionPeel=true;
      }
      else if(isOnionPeelLifted==false)
      {
      arrow.visible=false;
      arrow.x=1050;
      arrow.y=850;
      arrow.visible=true;

       currentobj.xp=1200;
       currentobj.yp=920;
      

        colliderA.inputEnabled=true;
        colliderA.enableBody=true;
        colliderA.visible=true;


      }


    }
    if(currentobj==forcepsPeel)
    {

      arrow.visible=false;
      arrow.x=1050;
      arrow.y=850;
      arrow.visible=true;

      colliderA.inputEnabled=true;
      colliderA.enableBody=true;
      colliderA.visible=true;
      currentobj.xp=1200;
      currentobj.yp=950;

    }
    if(currentobj==glycerineDropper)
    {

      arrow.visible=false;
      arrow.x=700;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1585;
       currentobj.yp=690;
       //waterDropper.angle=0;

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

        glycerineDropper.frameName="Water_To_Dropper0005.png";
       
    }
    if(currentobj==forcepsPeelTake)
    {

      arrow.visible=false;
      arrow.x=700;
      arrow.y=870;
      arrow.visible=true;

      currentobj.xp=840;
      currentobj.yp=656;

      colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

    }
    if(currentobj==brush)
    {
      arrow.visible=false;
      arrow.x=700;
      arrow.y=870;
      arrow.visible=true;

      currentobj.xp=1150;
      currentobj.yp=860;
      brush.angle=0;

      colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

    }

    if(currentobj==coverSlip)
    {
      arrow.visible=false;
      arrow.x=700;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1000;
       currentobj.yp=900;
       

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

    }

    if(currentobj==filterPaperSprite)
    {
      arrow.visible=false;
      arrow.x=700;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1300;
       currentobj.yp=950;
       

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

    
       
    }

    if(currentobj==glassSlideFinal)
    {
      arrow.visible=false;
      arrow.x=265;
      arrow.y=690;
      arrow.visible=true;

       currentobj.xp=510;
       currentobj.yp=899;
       

        colliderC.inputEnabled=true;
        colliderC.enableBody=true;
        colliderC.visible=true;

    
       
    }


       


  },

  onDragStop:function(obj)
  {
    obj.body.enable =true;

    console.log("jjjjj"+obj);
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==safraninBottle)

    {
      arrow.visible=false;
      colliderA.inputEnabled=false;
      colliderA.enableBody=false;
      colliderA.visible=false;
      
      safraninBottle.x=1155;
      safraninBottle.y=865;
      safraninBottle.animations.play('anim');
      safraninBottle.animations.currentAnim.onComplete.add(this.safraninBottleAnimationEnd, this);


      // waterDropper.x=900;
      // waterDropper.y=705;

    //   waterDropper.animations.play('anim');
    //   waterDrop.visible=true;

    //  waterDrop.animations.play('anim');
    //  waterDrop.animations.currentAnim.onComplete.add(this.waterDropAnimationEnd, this);

      // glassSlide.visible=false;
      // glassSlideWater.visible=true;
       
    }
    if(currentobj==forceps)
    {
      arrow.visible=false;
      if(isTakenOnionPeel==false)
      {
        colliderE.inputEnabled=false;
        colliderE.enableBody=false;
        colliderE.visible=false;
        arrow.visible=false;

        forceps.x=2500;
        forceps.y=920;
        forceps.visible=false;
        onionPeel.visible=false;

        forcepsPeelingSprite.visible=true;
        forcepsPeelingSprite.animations.play('anim');

        isTakenOnionPeel=true;

        forcepsPeelingSprite.animations.currentAnim.onComplete.add(this.forcepsPeelingAnimationEnd, this);
      }

    else if(isOnionPeelLifted==false)
      {
      arrow.visible=false;
  
        colliderA.inputEnabled=false;
        colliderA.enableBody=false;
        colliderA.visible=false;

        forceps.visible=false;
        forcepsPeelTake.visible=true;
        forcepsPeelTake.animations.play('anim');
        isOnionPeelLifted=true;
        petriDishPeel.visible=false;
        petriDishLiquid.visible=true;
        forcepsPeelTake.animations.currentAnim.onComplete.add(this.forcepsPeelTakeAnimationEnd, this);


      }
    }
  

    if(currentobj==forcepsPeel)
    {
      arrow.visible=false;

      colliderA.inputEnabled=false;
      colliderA.enableBody=false;
      colliderA.visible=false;

      forcepsPeel.visible=false;
      forcepsPeelDrop.visible=true;
      forcepsPeelDrop.animations.play('anim');
      forcepsPeelDrop.animations.currentAnim.onComplete.add(this.forcepsPeelDropAnimationEnd, this);

    }
    if(currentobj==glycerineDropper)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;

      glycerineDropper.x=700;
      glycerineDropper.y=705;

      glycerineDropper.animations.play('anim');
      glycerineDrop.visible=true;

     glycerineDrop.animations.play('anim');

     //glassSlideStickSprite.visible=false;
    //  glassSlideGlycerineSprite.visible=true;
    //  glassSlideGlycerineSprite.animations.play('anim'); 
     glycerineDrop.animations.currentAnim.onComplete.add(this.glycerineDropAnimationEnd1, this);
    }

    if(currentobj==forcepsPeelTake)
    {
      arrow.visible=false;
      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      forcepsPeelTake.visible=false;
      //forcepsPeelDrop.visible=true;
      //forcepsPeelDrop.animations.play('anim');
      glassSlidePeelDropping.visible=true;
      glassSlidePeelDropping.animations.play('anim');
      glassSlidePeelDropping.animations.currentAnim.onComplete.add(this.glassSlidePeelDroppingAnimationEnd, this);
    }

    if(currentobj==brush)
    {
      arrow.visible=false;

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      brush.visible=false;
      glassSlidePeel.visible=false;
      glassSlideBrush.visible=true;


      glassSlideBrush.animations.play('anim');
      glassSlideBrush.animations.currentAnim.onComplete.add(this.glassSlideBrushAnimationEnd, this);
    }

    if(currentobj==coverSlip)
    {
      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;

    
      coverSlip.visible=false;
      needle.visible=false;

      glassSlideBrush.visible=false;
      glassSlideCoverSlipSprite.visible=true;
      glassSlideCoverSlipSprite.animations.play('anim');
      glassSlideCoverSlipSprite.animations.currentAnim.onComplete.add(this.glassSlideCoverSlipSpriteEnd, this);
    
     
    }
    if(currentobj==filterPaperSprite)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;
      filterPaperSprite.x=710;
      filterPaperSprite.y=900;
      filterPaperSprite.animations.play('anim');
     filterPaperSprite.animations.currentAnim.onComplete.add(this.filterPaperAnimationEnd, this);
    }

    if(currentobj==glassSlideFinal)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure13",1);
       voice.play();
      arrow.visible=false;
      colliderC.inputEnabled=false;
      colliderC.enableBody=false;
      colliderC.visible=false;

      glassSlide2.visible=true;

      glassSlideFinal.visible=false;

      dialog.text="Click on the eyepiece of the microscope to see the structure of the\ncell.";
      
      arrow.x=200;
      arrow.y=330;
      arrow.angle=-90;
      arrow.visible=true;

      colliderD.visible=true;
      colliderD.inputEnabled = true;
      colliderD.input.useHandCursor = true;
      colliderD.events.onInputDown.add(this.clickOnEyePiece,this);

    }

    
       
  },

  rotateObjectiveLens:function()
  {
    arrow.angle=0;
    arrow.visible=false;
    if(microscopeLens.magnification==10)
    {
      microscopeLens.animations.play('anim1');
      microscopeLens.magnification=45;

    }
    else if(microscopeLens.magnification==45)
    {
      microscopeLens.animations.play('anim2');
      microscopeLens.magnification=10;
    }
    
  },

  filterPaperAnimationEnd:function()
  {
    coverSlipGroupTweenOut=this.game.add.tween(coverSlipGroup).to( {x:2500}, 1500, Phaser.Easing.Out, true);
  
    coverSlipGroupTweenOut.onComplete.add(function(){this.gotoMicroscopicView()}, this);

  },

  gotoMicroscopicView:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure12",1);
    voice.play();
    dialog.text="Nice... Now take the glass slide to the microscope and observe the\nmagnified cell structure.";

    glassSlideFinal.events.onDragStart.add(function() {this.onDragStart(glassSlideFinal)}, this);
    glassSlideFinal.events.onDragStop.add(function() {this.onDragStop(glassSlideFinal)}, this);
    glassSlideFinal.inputEnabled = true;
    glassSlideFinal.input.useHandCursor = true;
    glassSlideFinal.input.enableDrag(true);
    this.game.physics.arcade.enable(glassSlideFinal);
    glassSlideFinal.enableBody =true;
    arrow.x=700;
    arrow.y=870;
    arrow.visible=true;

  },

  glassSlideCoverSlipSpriteEnd:function()
  {
    glassSlideCoverSlipSprite.visible=false;
    glassSlideFinal.visible=true;
    needle.visible=true;
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure11",1);
    voice.play();
    dialog.text="Good job...Now gently wipe the excess glycerine from the glass slide by\nusing the filter paper.";
    arrow.x=1280;
    arrow.y=850;
    arrow.visible=true;

   filterPaperSprite.events.onDragStart.add(function() {this.onDragStart(filterPaperSprite)}, this);
   filterPaperSprite.events.onDragStop.add(function() {this.onDragStop(filterPaperSprite)}, this);
   filterPaperSprite.inputEnabled = true;
   filterPaperSprite.input.useHandCursor = true;
   filterPaperSprite.input.enableDrag(true);
   this.game.physics.arcade.enable(filterPaperSprite);
   filterPaperSprite.enableBody =true;

  },
  glassSlideBrushAnimationEnd:function()
  {
    arrow.x=1050;
    arrow.y=850;
    arrow.visible=true;

    brush.x=1150;
    brush.y=860;
    brush.angle=90;
    brush.visible=true;
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure10",1);
    voice.play();
    dialog.text="Take the coverslip and place it on the glass slide carefully with help\nof the needle.";

    coverSlip.events.onDragStart.add(function() {this.onDragStart(coverSlip)}, this);
    coverSlip.events.onDragStop.add(function() {this.onDragStop(coverSlip)}, this);
    coverSlip.inputEnabled = true;
    coverSlip.input.useHandCursor = true;
    coverSlip.input.enableDrag(true);
    this.game.physics.arcade.enable(coverSlip);
    coverSlip.enableBody =true;

  },

  glassSlidePeelDroppingAnimationEnd:function()
  {

    glassSlidePeelDropping.visible=false;
    glassSlidePeel.visible=true;
    forceps.x=1200;
    forceps.y=920;
    forceps.visible=true;
    glassSlideGlycerin.visible=false;

    forcepsTweenOut=this.game.add.tween(forceps).to( {x:2500}, 600, Phaser.Easing.Out, true);
    petriDishLiquidTweenOut=this.game.add.tween(petriDishLiquid).to( {x:2500}, 600, Phaser.Easing.Out, true);
    forcepsTweenOut.onComplete.add(function(){this.forcepsTweenOutEnd()}, this);

  },

  forcepsTweenOutEnd:function()
  {

  coverSlipGroup.visible=true;
  coverSlipGroupTweenIn=this.game.add.tween(coverSlipGroup).to( {x:0}, 600, Phaser.Easing.Out, true);
  coverSlipGroupTweenIn.onComplete.add(function(){this.coverSlipGroupTweenInEnd()}, this);

  },

  coverSlipGroupTweenInEnd:function()
  {
//     voice.destroy();
//  voice=this.game.add.audio("A_exp_procedure11",1);
//   voice.play();

      arrow.x=1150;
      arrow.y=810;
      arrow.visible=true;
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure9",1);
      voice.play();
      dialog.text="Make the mounting perfect by using the brush.";

    brush.events.onDragStart.add(function() {this.onDragStart(brush)}, this);
    brush.events.onDragStop.add(function() {this.onDragStop(brush)}, this);
    brush.inputEnabled = true;
    brush.input.useHandCursor = true;
    brush.input.enableDrag(true);
    this.game.physics.arcade.enable(brush);
    brush.enableBody =true;

  },

  forcepsPeelTakeAnimationEnd:function()
  {
    arrow.visible=false;
    arrow.x=1000;
    arrow.y=forcepsPeelTake.y-20;
    arrow.visible=true;
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure8",1);
    voice.play();
    dialog.text="Now mount the onion peel to the glass slide.";
    forcepsPeelTake.events.onDragStart.add(function() {this.onDragStart(forcepsPeelTake)}, this);
    forcepsPeelTake.events.onDragStop.add(function() {this.onDragStop(forcepsPeelTake)}, this);
    forcepsPeelTake.inputEnabled = true;
    forcepsPeelTake.input.useHandCursor = true;
    forcepsPeelTake.input.enableDrag(true);
    this.game.physics.arcade.enable(forcepsPeelTake);
    forcepsPeelTake.enableBody=true;
  },
  glycerineDropAnimationEnd1:function()
  {
    glycerineDrop.visible=false;
    arrow.visible=false;
    glassSlide.visible=false;
    glassSlideGlycerin.visible=true;
    glycerineDropperTween4=this.game.add.tween(glycerineDropper).to( {x:1585,y:490}, 600, Phaser.Easing.Out, true);
    glycerineDropperTween4.onComplete.add(function(){this.glycerineDropperTween4End()}, this);

  },

  glycerineDropperTween4End:function()
  {
    arrow.visible=false;
    glycerineDropperTween5=this.game.add.tween(glycerineDropper).to( {y:690}, 600, Phaser.Easing.Out, true);
    glycerineDropperTween5.onComplete.add(function(){this.glycerineDropperTween5End()}, this);
  },

  glycerineDropperTween5End:function()
  {
    arrow.visible=false;
    // glycerineGroup.add(glycerineDropper);
    // glycerineGroup.add(petriDishMixSprite);
    glycerineGroupTweenOut=this.game.add.tween(glycerineGroup).to( {x:2500}, 600, Phaser.Easing.Out, true);
    glycerineGroupTweenOut.onComplete.add(function(){this.glycerineGroupTweenOutEnd()}, this);


  },

  glycerineGroupTweenOutEnd:function()
  {
    forceps.visible=true;
    forcepsTweenIn=this.game.add.tween(forceps).to( {x:1200}, 600, Phaser.Easing.Out, true);
    forcepsTweenIn.onComplete.add(function(){this.forcepsTweenInEnd()}, this);
  },

  forcepsTweenInEnd:function()
  {
    arrow.x=1300;
    arrow.y=850;
    arrow.visible=true;
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure7",1);
    voice.play();
    dialog.text="Take the onion peel from the stain by using the forceps.";
    forceps.events.onDragStart.add(function() {this.onDragStart(forceps)}, this);
    forceps.events.onDragStop.add(function() {this.onDragStop(forceps)}, this);
    forceps.inputEnabled = true;
    forceps.input.useHandCursor = true;
    forceps.input.enableDrag(true);
    this.game.physics.arcade.enable(forceps);
    forceps.enableBody=true;
  },

  forcepsPeelDropAnimationEnd:function()
  {
    forcepsPeelDrop.visible=false;
    petriDishPeel.visible=true;
    petriDishLiquid.visible=false;

    glycerineGroupTween1=this.game.add.tween(glycerineGroup).to( {x:0}, 400, Phaser.Easing.Out, true);
    glycerineGroupTween1.onComplete.add(function(){this.glycerineGroupTween1End()}, this);


  },

  glycerineGroupTween1End:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure5",1);
    voice.play();
    dialog.text="Now take the dropper.";
    arrow.x=1250;
    arrow.y=950;
    arrow.visible=true;

    glycerineDropper.inputEnabled = true;
    glycerineDropper.input.useHandCursor = true;
    glycerineDropper.events.onInputDown.add(this.clickOnGlycerineDropper,this);
  },

  clickOnGlycerineDropper:function()
  {

    if(isGlycerineTaken==false)
    {
      arrow.visible=false;
      glycerineDropper.angle=0;
      glycerineDropperTween1=this.game.add.tween(glycerineDropper).to( {x:1585,y:490}, 600, Phaser.Easing.Out, true);
      glycerineDropperTween1.onComplete.add(function(){this.glycerineDropperTween1End()}, this);

     isGlycerineTaken=true;
    }
  },

  glycerineDropperTween1End:function()
  {
    glycerineDropperTween2=this.game.add.tween(glycerineDropper).to( {y:690}, 600, Phaser.Easing.Out, true);
    glycerineDropperTween2.onComplete.add(function(){this.glycerineDropperTween2End()}, this);

  },

  glycerineDropperTween2End:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure6",1);
    voice.play();

    dialog.text="Add some glycerine to the glass slide.";

    arrow.x=1590;
    arrow.y=490;
    arrow.visible=true;

    glycerineDropper.events.onDragStart.add(function() {this.onDragStart(glycerineDropper)}, this);
    glycerineDropper.events.onDragStop.add(function() {this.onDragStop(glycerineDropper)}, this);
    glycerineDropper.inputEnabled = true;
    glycerineDropper.input.useHandCursor = true;
    glycerineDropper.input.enableDrag(true);
    this.game.physics.arcade.enable(glycerineDropper);
    glycerineDropper.enableBody =true;

  },

  forcepsPeelingAnimationEnd:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure4",1);
    voice.play();
 forcepsPeelingSprite.visible=false;
 forcepsPeel.visible=true;
 dialog.text="Take the onion peel to the Petri dish containing stain.";

 arrow.visible=false;
 arrow.x=1300;
 arrow.y=870;
 arrow.visible=true;

    forcepsPeel.events.onDragStart.add(function() {this.onDragStart(forcepsPeel)}, this);
    forcepsPeel.events.onDragStop.add(function() {this.onDragStop(forcepsPeel)}, this);
    forcepsPeel.inputEnabled = true;
    forcepsPeel.input.useHandCursor = true;
    forcepsPeel.input.enableDrag(true);
    this.game.physics.arcade.enable(forcepsPeel);
    forcepsPeel.enableBody =true;


  },

  safraninBottleAnimationEnd:function()
  {
    safraninDrop1.visible=true;
    safraninDrop1.animations.play('anim');
    safraninDrop1.animations.currentAnim.onComplete.add(this.safraninDrop1AnimationEnd, this);
  },

  safraninDrop1AnimationEnd:function()
  {
    safraninDrop1.visible=false;
    petriDishWater.visible=false;
    petriDishLiquid.visible=true;
    petriDishLiquid.animations.play('anim');
    petriDishLiquid.animations.currentAnim.onComplete.add(this.petriDishAnimationEnd, this);
  },

  petriDishAnimationEnd:function()
  {
    safraninBottle.frameName='Liquid_bottle0001.png';
    safraninBottle.x=1550;
    safraninBottle.y=760;
    safraninBottleTween1=this.game.add.tween(safraninBottle).to( {x:2000}, 400, Phaser.Easing.Out, true);
    safraninBottleTween1.onComplete.add(function(){this.safraninBottleTween1End()}, this);
  },

  safraninBottleTween1End:function()
  {
     voice.destroy();
    voice=this.game.add.audio("A_exp_procedure18",1);
    voice.play();
    dialog.text="Take an onion.";
    onionGroupTween1=this.game.add.tween(onionGroup).to( {x:0}, 1200, Phaser.Easing.Out, true);
    onionGroupTween1.onComplete.add(function(){this.onionGroupTween1End()}, this);
  },

  onionGroupTween1End:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure19",1);
    voice.play();
    dialog.text="Now lets cut an onion leaf by using a knife.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.cutTheOnion, this);
    // voice.destroy();
    // voice=this.game.add.audio("A_exp_procedure3",1);
    // voice.play();
    // arrow.visible=false;
    // arrow.x=1300;
    // arrow.y=850;
    // arrow.visible=true;
    // dialog.text="Come on let’s take the onion peel by using the forceps.";
    // forceps.events.onDragStart.add(function() {this.onDragStart(forceps)}, this);
    // forceps.events.onDragStop.add(function() {this.onDragStop(forceps)}, this);
    // forceps.inputEnabled = true;
    // forceps.input.useHandCursor = true;
    // forceps.input.enableDrag(true);
    // this.game.physics.arcade.enable(forceps);
    // forceps.enableBody =true;
  },

  cutTheOnion:function()
  {
    onion.visible=false;
    onionKnife.visible=true;
    onionKnife.animations.play('anim');
    onionKnife.animations.currentAnim.onComplete.add(this.onionKnifeAnimationEnd, this);

  },

  onionKnifeAnimationEnd:function()
  {
    onionKnife.visible=false;
    onionPeel.visible=true;
        voice.destroy();
    voice=this.game.add.audio("A_exp_procedure3",1);
    voice.play();
    arrow.visible=false;
    arrow.x=1300;
    arrow.y=850;
    arrow.visible=true;
    dialog.text="Come on let’s take the onion peel by using the forceps.";
    forceps.events.onDragStart.add(function() {this.onDragStart(forceps)}, this);
    forceps.events.onDragStop.add(function() {this.onDragStop(forceps)}, this);
    forceps.inputEnabled = true;
    forceps.input.useHandCursor = true;
    forceps.input.enableDrag(true);
    this.game.physics.arcade.enable(forceps);
    forceps.enableBody =true;
  },

  clickOnEyePiece:function()
  {
    voice.destroy();
    boxGroup.visible=true;

    arrow.visible=false;
    bg.visible=false;
    microscope.visible=false;
    colliderD.inputEnabled=false;
    colliderD.input.useHandCursor =false;
    colliderD.visible=false;

    bgTopView.visible=true;
    microscopeTopview.visible=true;
    dialog_box.visible=false;
    dialog.text="";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.microscopicViewTweens, this);
    

  },
  microscopicViewTweens:function()
  {
    if(microscopeLens.magnification==10)
    {
      boxR.y=275;
      microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
      bgBlack.visible=true;
      bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Out, true);
      
      microscopeCellView.visible=true;
      microscopeCellViewTween=this.game.add.tween(microscopeCellView.scale).to( {x:.42,y:.42}, 2500, Phaser.Easing.Out, true);
      microscopeCellViewTween2=this.game.add.tween(microscopeCellView).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
          microscopeCellViewTween2.onComplete.add(function(){this.observeThecell()}, this);

    }
    else if(microscopeLens.magnification==45)
    {
      boxR.y=350;
      microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
      bgBlack.visible=true;
      bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Out, true);
      
      microscopeCellView.visible=true;
      microscopeCellViewTween=this.game.add.tween(microscopeCellView.scale).to( {x:.42,y:.42}, 2500, Phaser.Easing.Out, true);
      microscopeCellViewTween2=this.game.add.tween(microscopeCellView).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
          microscopeCellViewTween2.onComplete.add(function(){
            microscopeCellViewTween=this.game.add.tween(microscopeCellView.scale).to( {x:1,y:1}, 2500, Phaser.Easing.Out, true);
          }.bind(this))//this.observeThecell()}, this);

          microscopeCellViewTween.onComplete.add(function(){this.observeThecell()}, this);
    }
   


  },

  observeThecell:function()
  {
    play.visible=true;
    if(isItFirst==true)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure14",1);
      voice.play();
      dialog_box.visible=true;
      dialog_box.scale.setTo(.8,.7);
      dialog.text="Select the lens from the options to see the different magnified views\nof the cell.";
      isItFirst=false;
     this.game.time.events.add(Phaser.Timer.SECOND*6,this.instruction2,this);
    }
    else if(isItFirst==false)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure16",1);
      voice.play();
      dialog_box.visible=true;
      dialog.text="Click on the next button to see the observations.";

    }
//     voice.destroy();
//  voice=this.game.add.audio("A_exp_procedure15",1);
//   voice.play();
    // dialog_box.visible=true;
    // dialog.text="Observe and understand the structure of the cheek cell.";
    // this.game.time.events.add(Phaser.Timer.SECOND*1,this.toObservation, this);

  },
  instruction2:function()
  {
    voice.destroy();
      voice=this.game.add.audio("A_exp_procedure17",1);
      voice.play();
    dialog_box.visible=true;
    dialog.text="Click on the back button to proceed manually, or else click on the next\nbutton to see the observations.";

  },

  toPrevious:function()
  {
    microscopeTopview.scale.set(1.2);
    bgBlack.alpha=0;
    microscopeCellView.scale.set(0);
    microscopeCellView.alpha=1;

    boxGroup.visible=false;
    bg.visible=true;
    microscope.visible=true;
    colliderD.inputEnabled=true;
    colliderD.input.useHandCursor =true;
    colliderD.visible=true;

    bgTopView.visible=false;
    microscopeTopview.visible=false;
    microscopeCellView.visible=false;
    bgBlack.visible=false;
    dialog_box.visible=true;
    dialog_box.scale.setTo(.8,.7);

    arrow.visible=false;
    arrow.x=400;
    arrow.y=640;
    arrow.angle=90;
    arrow.visible=true;
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure15",1);
    voice.play();
    dialog.text="Click on the objective lenses to change lens then click on the eyepiece\nlens.";
    microscopeLens.inputEnabled = true;
    microscopeLens.input.useHandCursor = true;
    microscopeLens.events.onInputDown.add(this.rotateObjectiveLens, this);
    play.visible=false;

    if(microscopeLens.magnification==10)
    {
      microscopeLens.frameName="Microscope_Lenz0001.png";
      microscopeLens.magnification=10;
    }
    else if(microscopeLens.magnification==45)
    {
      microscopeLens.frameName="Microscope_Lenz0015.png";
      microscopeLens.magnification=45;
    }

  },

  toObservation:function()
  {
//     voice.destroy();
//  voice=this.game.add.audio("A_exp_procedure16",1);
//   voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;

  },
  








 

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
   
    

  },

  
 
  detectCollision:function(){
   

    if(colliderA.enableBody && currentobj!=null)
    {
      this.game.physics.arcade.overlap(currentobj, colliderA,function() {this.match_Obj()},null,this);
      collider=colliderA;
    }

    if(colliderB.enableBody && currentobj!=null)
    {
      this.game.physics.arcade.overlap(currentobj,colliderB,function() {this.match_Obj()},null,this);
      collider=colliderB;
    }
    if(colliderC.enableBody && currentobj!=null)
    {
    this.game.physics.arcade.overlap(currentobj, colliderC,function() {this.match_Obj()},null,this);
    collider=colliderC;
    }

    if(colliderE.enableBody && currentobj!=null)
    {
    this.game.physics.arcade.overlap(currentobj, colliderE,function() {this.match_Obj()},null,this);
    collider=colliderE;
    }
    // if(collider_scale.enableBody && currentobj!=null)
    // {
    //   this.game.physics.arcade.overlap(currentobj, collider_scale,function() {this.match_Obj()},null,this);
    //   collider=collider_scale;
    // }
  

    
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
       if(currentobj==safraninBottle)
       {
        arrow.visible=false;
        arrow.x=1395;
        arrow.y=620;
        arrow.visible=true;
        // waterDropper.angle=80;
        // waterDropper.frameName="Water_To_Dropper0001.png"; 
       }
       if(currentobj==forceps)
       {

        arrow.visible=false;
        arrow.x=1300;
        arrow.y=850;
        arrow.visible=true;
       }
       if(currentobj==forcepsPeel)
       {

        arrow.visible=false;
        arrow.x=1300;
        arrow.y=870;
        arrow.visible=true;
       }
       if(currentobj==glycerineDropper)
       {
        arrow.visible=false;
        arrow.x=1590;
        arrow.y=490;
        arrow.visible=true;

       }
       if(currentobj==brush)
       {
        arrow.visible=false;
        arrow.x=1150;
        arrow.y=810;
        arrow.visible=true;
        brush.angle=90;
       }
       if(currentobj==coverSlip)
       {
        arrow.visible=false;
        arrow.x=1050;
        arrow.y=850;
        arrow.visible=true;

       }
       if(currentobj==filterPaperSprite)
       {

        arrow.visible=false;
        arrow.x=1280;
        arrow.y=850;
        arrow.visible=true;
       }
       if(currentobj==glassSlideFinal)
       {
         arrow.visible=false;
        arrow.x=700;
        arrow.y=870;
        arrow.visible=true;
       }

     
      
       
        // arrow.x=1500;
        // arrow.y=450;
        // arrow_y=450;
        // arrow.visible=true;
        currentobj=null;
      }


      
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
