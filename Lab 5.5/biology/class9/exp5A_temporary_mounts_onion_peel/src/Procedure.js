var procedure = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  var pageCount;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  }
  
  procedure.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("A_procedure1",1);
 voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);
 pageCount=1;

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/
  //procedure_audio1=this.game.add.audio('procedure_voice1',1);
  //procedure_audio2=this.game.add.audio('procedure_voice2',1);

 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_text=this.game.add.text(250,200,"Procedure:",headfontStyle);
  //exp_Name="decomposition_reaction";
  //console.log(exp_Name+"......");
  
      procedure_step_1="1. Add few drops of safranin to the petri dish contains water.";
      procedure_step_2="2. With the help of a forceps take thin onion peel and put it on the petri dish\n    contains stain.";
      procedure_step_3="3. Mount the stained onion peel into the glass slide with help of glycerine and\n    brush.";
      procedure_step_4="4. Place the cover slip over the stained peel carefully with help of the needle.";
      procedure_step_5="5. Remove the excess of glycerine from the slide by using the filter paper.";
      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,380,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(300,500,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(300,640,procedure_step_4,fontStyle);
      procedure_step_text_5=this.game.add.text(300,720,procedure_step_5,fontStyle);
      procedure_step_text_6=this.game.add.text(300,300,"",fontStyle);
      procedure_step_text_7=this.game.add.text(300,440,"",fontStyle);
      procedure_step_text_8=this.game.add.text(300,520,"",fontStyle);
      //procedure_step_text_9=this.game.add.text(300,590,"",fontStyle);
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  next_btn = this.game.add.sprite(1600,870,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1600,870,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 
  prev_btn = this.game.add.sprite(300,870,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(300,870,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  prev_btn.visible=false;
  //next_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrevious, this);
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment, this);
 play.visible=false;

 diagram=this.game.add.sprite(500,280,'diagram');
 diagram.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
    
    // For Full screen checking.
    toNext:function(){
      pageCount+=1;
      if(pageCount==2)
      {
        voice.destroy(true);
        voice=this.game.add.audio("A_procedure2",1);
          voice.play();
            procedure_step_text_1.text="";
            procedure_step_text_2.text="";
            procedure_step_text_3.text="";
            procedure_step_text_4.text="";
            procedure_step_text_5.text="";
            procedure_step_text_6.text="6. Mount and focus the glass slide on the compound microscope and observe\n    the cell under 10X and 45X magnification. ";
            procedure_step_text_7.text="7. Record the observations.";
            procedure_step_text_8.text="8. Draw the diagram of the cells and label its parts.";
            //procedure_step_text_9.text="9. Draw the diagram of the cells and label its parts.";
            
            
            prev_btn.visible=true;
            next_btn.visible=false;
            play.visible=true;
            
      }
      // else if(pageCount==3)
      // {
      //   voice.destroy();
      //   // voice.destroy(true);
      //       voice=this.game.add.audio("A_procedure3",1);
      //         voice.play();
       
      //   procedure_step_text_1.text="";
      //   procedure_step_text_2.text="";
      //   procedure_step_text_3.text="";
      //   procedure_step_text_4.text="";
      //   procedure_step_text_5.text="";
      //   procedure_step_text_6.text="";
      //   procedure_step_text_7.text="";
      //   diagram.visible=true;
      //   procedure_step_text_9=this.game.add.text(300,300,"9. Understand the ray diagram of concave mirror.",fontStyle);
      //   prev_btn.visible=true;
      //   next_btn.visible=false;
      //   play.visible=true;

      // }
     
        },
        toPrevious:function(){
           pageCount-=1;
           if(pageCount==1)
           {
            voice.destroy(true);
            voice=this.game.add.audio("A_procedure1",1);
              voice.play();
                procedure_step_text_1.text=procedure_step_1;
                procedure_step_text_2.text=procedure_step_2;
                procedure_step_text_3.text=procedure_step_3;
                procedure_step_text_4.text=procedure_step_4;
                procedure_step_text_5.text="";
                procedure_step_text_6.text="";
                procedure_step_text_7.text="";
                procedure_step_text_8.text="";
                next_btn.visible=true;
                prev_btn.visible=false;
                play.visible=false;
           }

        //    else if(pageCount==2)
        //    {
        //     voice.destroy(true);
        // voice=this.game.add.audio("A_procedure2",1);
        //   voice.play();
        //     procedure_step_text_1.text="";
        //     procedure_step_text_2.text="";
        //     procedure_step_text_3.text="";
        //     procedure_step_text_4.text="";
        //     procedure_step_text_5.text="6. Note down the position of the screen and concave mirror stand with the \n    help of a metre scale. Calculate the distance between the position of \n    the mirror stand and the screen. This distance is equal to the focal length \n    of the concave mirror.";
        //     procedure_step_text_6.text="7. Repeat the above experiment twice by changing the position of the mirror \n    stand. Note the corresponding change in the position of the screen. Record \n    the focal length in each case.";
        //     procedure_step_text_7.text="8. Calculate the mean value of the focal length.";
            
            
        //     prev_btn.visible=true;
        //     next_btn.visible=true;
        //     play.visible=false;
        //     diagram.visible=false;
        //     procedure_step_text_9.text="";
            

        //    }
    
        },
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
  
    //For to next scene   
   
        toExperiment:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
          //procedure_audio2.destroy();
          this.state.start("Experiment_1", true, false);//Experiment_3
           
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    // procedure_audio1.destroy();
    // procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    // procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  // procedure_audio1.destroy();
  // procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  