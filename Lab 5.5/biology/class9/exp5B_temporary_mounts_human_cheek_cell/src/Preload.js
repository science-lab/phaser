var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var mirrorPositionValue1;
  var mirrorPositionValue2;
  var mirrorPositionValue3;
  var screenPositionValue1;
  var screenPositionValue2;
  var screenPositionValue3;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        //this.game.load.atlasJSONHash('ferrous_sulphate', 'assets/ferrous_sulphate.png', 'assets/ferrous_sulphate.json');
        //this.game.load.atlasJSONHash('burner_sprites', 'assets/burner_sprites.png', 'assets/burner_sprites.json');
      
        this.game.load.json('questions','data/questions1.json');
    
        
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
     
        
        
        this.game.load.image('transBackground','assets/transBackground.png');
         this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");   
        this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
   
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        ///////////////observation///////////////
        this.game.load.image('obs_base','assets/cheek_cell/table_base.png');
        this.game.load.image('obs_table','assets/cheek_cell/obs_table.png');
        this.game.load.image('obs_base_head','assets/cheek_cell/dialogbox_head.png');
        this.game.load.image('obs_line','assets/cheek_cell/obs_table_line.png');
        this.game.load.image('cell_image','assets/cheek_cell/cell_diagram_color.png');
        this.game.load.image('cell_image_bw','assets/cheek_cell/cell_diagram_bw.png');

        ///////////material required/////

        this.game.load.image('m_methyleneBlue','assets/cheek_cell/materials/m_methylene_blue.png');
        this.game.load.image('m_glycerinBottle','assets/cheek_cell/materials/m_glycerin_bottle.png');
        this.game.load.image('m_dropper','assets/cheek_cell/materials/m_dropper.png');
        this.game.load.image('m_petridish','assets/cheek_cell/materials/m_petridish_water.png');
        this.game.load.image('m_ice_cream_stick','assets/cheek_cell/materials/m_icecream_stick1.png');
        this.game.load.image('m_glassSlide','assets/cheek_cell/materials/m_glass_slide.png');
        this.game.load.image('m_coverslip','assets/cheek_cell/materials/m_cover_slip.png');
        this.game.load.image('m_filterPaper','assets/cheek_cell/materials/m_filter_paper.png');
        this.game.load.image('m_microscope','assets/cheek_cell/materials/m_microscope.png');






        ////////////////////Experiment items////////////////
        this.game.load.image('bg','assets/cheek_cell/Bg.png');
        this.game.load.image('bg_black','assets/cheek_cell/Bg_black.png');
        this.game.load.image('bg_top','assets/cheek_cell/Bg_top.png');
       
        this.game.load.image('microscope','assets/cheek_cell/microscope.png');
        this.game.load.image('glass_plate_water','assets/cheek_cell/petridish_water.png');
        this.game.load.image('petridish_water','assets/cheek_cell/petridish_water.png');
        this.game.load.image('glycerin_bottle','assets/cheek_cell/glycerin_bottle.png');
        this.game.load.image('glycerine_bottle_back','assets/cheek_cell/glycerin_bottle_back.png');
       //this.game.load.image('methylene_bottle','assets/cheek_cell/petridish_water.png');
        this.game.load.image('needle','assets/cheek_cell/needle.png');
        this.game.load.image('cover_slip','assets/cheek_cell/cover_slip.png');
        this.game.load.image('petridish_water','assets/cheek_cell/petridish_water.png');

        this.game.load.image('microscope_top_view','assets/cheek_cell/Mocroscope_top_view.png');
        this.game.load.image('microscope_cell_view','assets/cheek_cell/microscopic_view.png');
        this.game.load.image('microscope_holder_right','assets/cheek_cell/mocroscope_right_lock.png');
        this.game.load.image('microscope_holder_left','assets/cheek_cell/mocroscope_left_lock.png');

        
        



        this.game.load.image('glass_slide','assets/cheek_cell/glass_slide.png');
        this.game.load.image('glass_slide_water','assets/cheek_cell/glass_slide_with_water.png');
        this.game.load.image('glass_slide_final','assets/cheek_cell/glass_slide_final.png');
        this.game.load.image('glass_slide_view2','assets/cheek_cell/glass_slide_view2.png');
        // this.game.load.image('ice_scream_stick','assets/cheek_cell/icecream_stick.png');
        // this.game.load.image('ice_scream_stick','assets/cheek_cell/icecream_stick.png');

        this.game.load.atlasJSONHash('face_sprite', 'assets/cheek_cell/animations/cell_scrapping_sprite.png', 'assets/cheek_cell/animations/cell_scrapping_sprite.json');
        this.game.load.atlasJSONHash('water_dropper', 'assets/cheek_cell/animations/water_dropper_sprite.png', 'assets/cheek_cell/animations/water_dropper_sprite.json');
        this.game.load.atlasJSONHash('water_drop', 'assets/cheek_cell/animations/water_drop_sprite.png', 'assets/cheek_cell/animations/water_drop_sprite.json');
        this.game.load.atlasJSONHash('glass_slide_liquid_sprite', 'assets/cheek_cell/animations/liquid_glassPlate_sprite.png', 'assets/cheek_cell/animations/liquid_glassPlate_sprite.json');
        this.game.load.atlasJSONHash('stick_glass_slide', 'assets/cheek_cell/animations/stick_glassPlate_sprite.png', 'assets/cheek_cell/animations/stick_glassPlate_sprite.json');
        this.game.load.atlasJSONHash('glycerine_glass_slide', 'assets/cheek_cell/animations/glycerin_to_glassPlate.png', 'assets/cheek_cell/animations/glycerin_to_glassPlate.json');
        this.game.load.atlasJSONHash('coverSlip_glassSlide', 'assets/cheek_cell/animations/coverSlip_to_glassPlate_sprite.png', 'assets/cheek_cell/animations/coverSlip_to_glassPlate_sprite.json');

        this.game.load.atlasJSONHash('filter_paper', 'assets/cheek_cell/animations/filter_paper_sprite.png', 'assets/cheek_cell/animations/filter_paper_sprite.json');

        this.game.load.atlasJSONHash('ice_scream_stick', 'assets/cheek_cell/animations/ice_scream_stick_sprite.png', 'assets/cheek_cell/animations/ice_scream_stick_sprite.json');
        this.game.load.atlasJSONHash('methylene_dropper','assets/cheek_cell/animations/methylene_dropper_sprite.png', 'assets/cheek_cell/animations/methylene_dropper_sprite.json');
        this.game.load.atlasJSONHash('methylene_drop', 'assets/cheek_cell/animations/methylene_drop_sprite.png', 'assets/cheek_cell/animations/methylene_drop_sprite.json');
        this.game.load.atlasJSONHash('arrow','assets/cheek_cell/animations/arrow.png', 'assets/cheek_cell/animations/arrow.json');
        this.game.load.atlasJSONHash('methylene_bottle','assets/cheek_cell/animations/methylene_pour_sprite.png', 'assets/cheek_cell/animations/methylene_pour_sprite.json');
        this.game.load.atlasJSONHash('petridish_liquid_mix','assets/cheek_cell/animations/methylene_petridish_sprite.png', 'assets/cheek_cell/animations/methylene_petridish_sprite.json');


////////////////////////////////////Audio for concavemirror/////////////////////////

        this.game.load.audio('A_aim','assets/audio/Human_cheek_cell/A_aim.mp3');
        this.game.load.audio('A_exp_procedure1','assets/audio/Human_cheek_cell/A_exp_procedure1.mp3');
        this.game.load.audio('A_exp_procedure2','assets/audio/Human_cheek_cell/A_exp_procedure2.mp3');
        this.game.load.audio('A_exp_procedure3','assets/audio/Human_cheek_cell/A_exp_procedure3.mp3');
        this.game.load.audio('A_exp_procedure4','assets/audio/Human_cheek_cell/A_exp_procedure4.mp3');
        this.game.load.audio('A_exp_procedure5','assets/audio/Human_cheek_cell/A_exp_procedure5.mp3');
        this.game.load.audio('A_exp_procedure6','assets/audio/Human_cheek_cell/A_exp_procedure6.mp3');
        this.game.load.audio('A_exp_procedure7','assets/audio/Human_cheek_cell/A_exp_procedure7.mp3');
        this.game.load.audio('A_exp_procedure8','assets/audio/Human_cheek_cell/A_exp_procedure8.mp3');
        this.game.load.audio('A_exp_procedure9','assets/audio/Human_cheek_cell/A_exp_procedure9.mp3');
        this.game.load.audio('A_exp_procedure10','assets/audio/Human_cheek_cell/A_exp_procedure10.mp3');
        this.game.load.audio('A_exp_procedure11','assets/audio/Human_cheek_cell/A_exp_procedure11.mp3');
        this.game.load.audio('A_exp_procedure12','assets/audio/Human_cheek_cell/A_exp_procedure12.mp3');
        this.game.load.audio('A_exp_procedure13','assets/audio/Human_cheek_cell/A_exp_procedure13.mp3');
        this.game.load.audio('A_exp_procedure14','assets/audio/Human_cheek_cell/A_exp_procedure14.mp3');
        this.game.load.audio('A_exp_procedure15','assets/audio/Human_cheek_cell/A_exp_procedure15.mp3');
        this.game.load.audio('A_exp_procedure16','assets/audio/Human_cheek_cell/A_exp_procedure16.mp3');
        this.game.load.audio('A_precautions','assets/audio/Human_cheek_cell/A_precautions.mp3');


        this.game.load.audio('A_procedure1','assets/audio/Human_cheek_cell/A_procedure1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Human_cheek_cell/A_procedure2.mp3');

        this.game.load.audio('A_observations1','assets/audio/Human_cheek_cell/A_observations1.mp3');
        this.game.load.audio('A_observations2','assets/audio/Human_cheek_cell/A_observations2.mp3');
        this.game.load.audio('A_result','assets/audio/Human_cheek_cell/A_result.mp3');
 
  




       


	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
     //this.game.state.start("Viva");//Starting the gametitle state
   // this.game.state.start("Experiment_1");//Starting the gametitle state
     // this.game.state.start("Materials");//Simulation_hot1
     // this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
   //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Title",true,false,ip);//Starting the gametitle state
     // this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");
      this.game.state.start("Aim");
      //this.game.state.start("Exp_Selection");

	}
}

