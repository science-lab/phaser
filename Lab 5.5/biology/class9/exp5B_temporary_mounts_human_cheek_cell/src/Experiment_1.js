var experiment_1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////

var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;
var ratio;
//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;

var fNames;
var isWatertaken;
var isGlycerineTaken;
var isLiquidTaken;
var isCellTaken;




}

experiment_1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
//  voice=this.game.add.audio("A_exp_procedure1",1);

 this.arrangeScene();

 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0,0,'bg');
 bg.scale.setTo(1,1);

  // maskBg1 = this.game.add.graphics(0, 0);
  // maskBg1.lineStyle(10,0xff704d,1);
  // maskBg1.beginFill(0xffffcc);
  // maskBg1.drawRect(20, 40, 300, 700);
  // maskBg1.alpha=1;
 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////

  fNames=[];

   isWatertaken=false;
   isGlycerineTaken=false;
   isLiquidTaken=false;
  isCellTaken=false;

  // isMirrorStandMoving=false;
  // isScreenMoving=false;
  // pointerx=0;
  // screen_stand_scale_val=0;
  // mirror_stand_scale_val=0;
  // mirror_stand_minPos=30;
  // screen_stand_maxPos=1343;
  // image_maxPos=1289;
  // screen_and_stand_distance=70;
  currentobj=null;

  // isreading1Taken=false;
  // isreading2Taken=false;
  // isreading3Taken=false;
  // isAnyStandMoved=false;
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

 

  this.arrangeScene();
  // voice=this.game.add.audio("A_exp_procedure1",1);
  // voice.play();
 
  dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.8,.7);
      dialog_text="Let's perform the experiment.";
      
      //procedure_voice1.play();
      dialog=this.game.add.text(70,40,dialog_text,fontStyle);
      this.game.time.events.add(Phaser.Timer.SECOND*.5,this.loadScene, this);

      
//////////////////////////////Values///////////////////////////////
fontStyle3={ font: "28px Segoe UI", fill: "#000000", align: "center"};//fontWeight:"bold" 
fontStyle4={ font: "32px Segoe UI", fill: "#000000", align: "left"};








   


////////////////////////////////////////////////////////////////////

 },





arrangeScene:function()
{
 microscope=this.game.add.sprite(500,220,'microscope');
 microscope.scale.setTo(-1,1);




 faceAnimation=this.game.add.sprite(500,293,'face_sprite','Face_Animation0001.png');
 faceAnimation.scale.set(2);
 faceAnimation.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14],8,false,true);
 //faceAnimation.animations.play('anim');
 faceAnimation.visible=false;


 glassSlide=this.game.add.sprite(700,900,'glass_slide');
 glassSlide.scale.set(.9);

 glassSlideWater=this.game.add.sprite(700,900,'glass_slide_water');
 glassSlideWater.scale.set(.9);
 glassSlideWater.visible=false;   

 glassSlideLiquid=this.game.add.sprite(700,900,'glass_slide_liquid_sprite','Liquid_to_Glass_plate0001.png');
 glassSlideLiquid.scale.set(.9);
 glassSlideLiquid.animations.add('anim',[0,1,2,3,4],8,false,true);
 glassSlideLiquid.visible=false;

 glassSlideStickSprite=this.game.add.sprite(702,891,'stick_glass_slide','IceCreamStic_on_glassplate0001.png');
 glassSlideStickSprite.scale.set(.9);
 glassSlideStickSprite.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11],12,false,true);
//  glassSlideStickSprite.animations.play('anim');
 glassSlideStickSprite.visible=false;

 glassSlideGlycerineSprite=this.game.add.sprite(700,900,'glycerine_glass_slide','Glycerin_to_glassPlate0001.png');
 glassSlideGlycerineSprite.scale.setTo(.9,.9);
 glassSlideGlycerineSprite.animations.add('anim',[0,1,2,3,4],8,false,true);
 glassSlideGlycerineSprite.visible=false;

 glassSlideCoverSlipSprite=this.game.add.sprite(667,755,'coverSlip_glassSlide','Small_glassPlate_and _Needle0020.png');
 glassSlideCoverSlipSprite.scale.setTo(.9,.9);
 glassSlideCoverSlipSprite.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31 ],10,false,true);
 glassSlideCoverSlipSprite.visible=false;


 glassSlideFinal=this.game.add.sprite(700,900,'glass_slide_final');
 glassSlideFinal.scale.set(.9);
 glassSlideFinal.visible=false;

 needle=this.game.add.sprite(1250,1000,'needle');
 needle.anchor.set(.5);
 needle.scale.setTo(.76,.8);
 

 coverSlip=this.game.add.sprite(1200,900,'cover_slip');
 coverSlip.scale.setTo(.9,.9);

 filterPaperSprite=this.game.add.sprite(1500,950,'filter_paper','Paper0001.png');
 filterPaperSprite.anchor.set(.5);
 filterPaperSprite.animations.add('anim',[0,1,2,3,4,5,6,7,8,9,10,11],8,false,true);
 //filterPaperSprite.animations.play('anim');

 coverSlipGroup=this.game.add.group();
 coverSlipGroup.add(coverSlip);
 coverSlipGroup.add(filterPaperSprite);
 coverSlipGroup.add(needle);
 coverSlipGroup.x=2500;

 iceScreamStick=this.game.add.sprite(550,890,'ice_scream_stick','icecream_stick1.png');

 

 petriDishWater=this.game.add.sprite(1100,880,'petridish_water');
 petriDishWater.scale.set(1);

 petriDishMixSprite=this.game.add.sprite(1100,880,'petridish_liquid_mix','Liquid_to_Watchglass0001.png');
 petriDishMixSprite.animations.add('anim',[0,1,2,3,4],8,false,true);
 petriDishMixSprite.visible=false;

 glycerineBottleBack=this.game.add.sprite(1556,634,'glycerine_bottle_back');
 glycerineBottleBack.scale.setTo(.8,1);

 glycerineBottle=this.game.add.sprite(1500,640,'glycerin_bottle');
 glycerineBottle.scale.setTo(.8,.8);

 glycerineGroup=this.game.add.group();
 glycerineGroup.add(glycerineBottleBack);
 glycerineGroup.add(glycerineBottle);
 glycerineGroup.x=2500;

 methyleneBottle=this.game.add.sprite(1550,760,'methylene_bottle','Liquid_bottle0001.png');
 methyleneBottle.animations.add('anim',[0,1,2,3,4],20,false,true);
 methyleneBottle.anchor.set(.5);
 methyleneBottle.scale.set(.8);
 //methyleneBottle.angle=120;


 waterDropper=this.game.add.sprite(1256,1004,'water_dropper','Water_To_Dropper0001.png');
 waterDropper.scale.set(.75,.75);
 waterDropper.anchor.set(.5);
 waterDropper.angle=90;
 waterDropper.animations.add('anim',[4,3,2,1,0],24,false,true);

 glycerineDropper=this.game.add.sprite(1256,1004,'water_dropper','Water_To_Dropper0001.png');
 glycerineDropper.scale.set(.75,.75);
 glycerineDropper.anchor.set(.5);
 glycerineDropper.angle=90;
 glycerineDropper.animations.add('anim',[4,3,2,1,0],24,false,true);
 glycerineDropper.visible=false;

 waterDrop=this.game.add.sprite(882,870,'water_drop','Water_Drop0001.png');
 waterDrop.animations.add('anim',[0,1,2],8,false,true);
 waterDrop.visible=false;
//  waterDrop.animations.play('anim');



glycerineDrop=this.game.add.sprite(882,870,'water_drop','Water_Drop0001.png');
glycerineDrop.animations.add('anim',[0,1,2],8,false,true);
glycerineDrop.visible=false;
//glucerineDrop.animations.play('anim');
 
 methyleneDropper=this.game.add.sprite(1256,1004,'methylene_dropper','Liquid_to_dropper0001.png');
 methyleneDropper.scale.set(.75);
 methyleneDropper.anchor.set(.5);
 methyleneDropper.angle=90;

 methyleneDropper.animations.add('anim',[4,3,2,1,0],24,false,true);

 methyleneDropper.visible=false;

 methyleneDrop2=this.game.add.sprite(887,875,'methylene_drop','Liquid_dropp0001.png');
 methyleneDrop2.scale.set(.5);
 methyleneDrop2.animations.add('anim',[0,1,2,3],8,false,true);
 methyleneDrop2.visible=false;
 //methyleneDrop2.animations.play('anim');

 methyleneDrop1=this.game.add.sprite(1235,845,'methylene_drop','Liquid_dropp0001.png');
 methyleneDrop1.scale.set(.7);
 methyleneDrop1.animations.add('anim',[0,1,2,0,1,2],8,false,true);
 methyleneDrop1.visible=false;
 //methyleneDrop1.animations.play('anim');
 
 glassSlide2=this.game.add.sprite(410,690,'glass_slide_view2');
 glassSlide2.scale.setTo(-.6,.6);
 glassSlide2.visible=false;

 microscopeLeftHolder=this.game.add.sprite(385,700,'microscope_holder_left');
 microscopeLeftHolder.scale.setTo(-1,1);
 microscopeRightHolder=this.game.add.sprite(250,740,'microscope_holder_right');
 microscopeRightHolder.scale.setTo(-1,1);


 

//this.game.time.events.add(Phaser.Timer.SECOND*5,this.loadScene, this);



colliderA=this.game.add.sprite(1150,700,'collider');
colliderA.scale.set(4);
colliderA.alpha=0;
colliderA.visible=false;
this.game.physics.arcade.enable(colliderA);

colliderB=this.game.add.sprite(800,730,'collider');
colliderB.scale.set(4);
colliderB.alpha=0;
colliderB.visible=false;
this.game.physics.arcade.enable(colliderB);


colliderC=this.game.add.sprite(180,600,'collider');
colliderC.scale.set(4);
colliderC.alpha=0;
colliderC.visible=false;
this.game.physics.arcade.enable(colliderC);

colliderD=this.game.add.sprite(215,210,'collider');
colliderD.scale.setTo(2,4);
colliderD.alpha=0;
colliderD.visible=false;

arrow=this.game.add.sprite(200,330,'arrow','arrow_0001.png');//170,200
//arrow.angle=-90;
arrow.anchor.set(.5);
arrow.animations.add('anim',[0,1,2,3,4,5,6,7,8,9],28,true,true);
arrow.animations.play('anim');
arrow.visible=false;


bgTopView= this.game.add.sprite(0,0,'bg_top');
bgTopView.scale.setTo(1,1);
bgTopView.visible=false;



microscopeTopview=this.game.add.sprite(900,480,'microscope_top_view');
microscopeTopview.scale.setTo(1.2,1.2);
microscopeTopview.anchor.setTo(.5,.4);
// microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
microscopeTopview.visible=false;

bgBlack= this.game.add.sprite(0,0,'bg_black');
bgBlack.scale.setTo(1,1);
bgBlack.alpha=0;
// bgBlack.visible=false;
// bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Out, true);
 bgBlack.visible=false;

microscopeCellView=this.game.add.sprite(902,495,'microscope_cell_view');
microscopeCellView.anchor.set(.5);
microscopeCellView.scale.set(0);
microscopeCellView.alpha=0;
// microscopeCellViewTween=this.game.add.tween(microscopeCellView.scale).to( {x:1,y:1}, 2500, Phaser.Easing.Out, true);
// microscopeCellViewTween=this.game.add.tween(microscopeCellView).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
 microscopeCellView.visible=false;

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;


},






loadScene:function()
{
  voice=this.game.add.audio("A_exp_procedure1",1);
  voice.play();
  this.game.time.events.add(Phaser.Timer.SECOND*5,this.startExperiment, this);
},

startExperiment:function()
{
 voice.destroy();
  voice=this.game.add.audio("A_exp_procedure2",1);
  voice.play();
  dialog.text="Let's take human cheek cells by scrapping the inner side of the mouth,\nfor that click on the ice cream stick.";


  iceScreamStick.inputEnabled = true;
  iceScreamStick.input.useHandCursor = true;
  iceScreamStick.events.onInputDown.add(this.clickOnStick,this);
  arrow.visible=true;
  arrow.x=605;
  arrow.y=880;

  // voice.destroy();
  // voice=this.game.add.audio("A_exp_procedure2",1);
  // voice.play();
  // dialog.text="Drag and drop scale on to the table.";
  // temp_scale.events.onDragStart.add(function() {this.onDragStart(temp_scale)}, this);
  // temp_scale.events.onDragStop.add(function() {this.onDragStop(temp_scale)}, this);
  // temp_scale.inputEnabled = true;
  // temp_scale.input.useHandCursor = true;
  // this.game.physics.arcade.enable(temp_scale);
  // temp_scale.input.enableDrag(true);

},

clickOnStick:function()
{

  if(isCellTaken==false)
  {


  
  arrow.visible=false;
 
  iceScreamStick.visible=false;
  faceAnimation.visible=true;
  faceAnimation.animations.play('anim');
  isCellTaken=true;
  faceAnimation.animations.currentAnim.onComplete.add(this.faceAnimationEnd, this);

  }
},

faceAnimationEnd:function()
{
  // voice.destroy();
  // voice=this.game.add.audio("A_exp_procedure3",1);
  // voice.play();
  faceAnimation.visible=false;
  iceScreamStick.frameName="icecream_stick2.png";
  iceScreamStick.visible=true;

  // dialog.text="Now take the dropper.";
  // arrow.x=1250;
  // arrow.y=950;
  // arrow.visible=true;

  // waterDropper.inputEnabled = true;
  // waterDropper.input.useHandCursor = true;
  // waterDropper.events.onInputDown.add(this.clickOnWaterDropper,this);
  this.game.time.events.add(Phaser.Timer.SECOND*1,this.addMethyleneToWater, this);



},

clickOnWaterDropper:function()
{
  if(isWatertaken==false)
  {
    arrow.visible=false;
    waterDropperTween1=this.game.add.tween(waterDropper).to( {x:1310,y:890}, 600, Phaser.Easing.Out, true);
    waterDropperTween1.onComplete.add(function(){this.waterDropperTween1End()}, this);
    waterDropper.inputEnabled = false;
    waterDropper.input.useHandCursor = false;
    isWatertaken=true;
  }

  // else if(isGlycerineTaken==false)
  // {
  //   arrow.visible=false;
  //   waterDropper.angle=0;
  //   waterDropperTween2=this.game.add.tween(waterDropper).to( {x:1585,y:290}, 600, Phaser.Easing.Out, true);
  //   waterDropperTween2.onComplete.add(function(){this.waterDropperTween2End()}, this);

  //   waterDropper.inputEnabled = false;
  //   waterDropper.input.useHandCursor = false;
  //   isGlycerineTaken=true;
  // }

},

// waterDropperTween2End:function()
// {
//   waterDropperTween3=this.game.add.tween(waterDropper).to( {y:490}, 600, Phaser.Easing.Out, true);
//   waterDropperTween3.onComplete.add(function(){this.waterDropperTween3End()}, this);

// },

// waterDropperTween3End:function()
// {
//   dialog.text="Add some glycerine to the glass slide";

// },

waterDropperTween1End:function()
{
  voice.destroy();
  voice=this.game.add.audio("A_exp_procedure4",1);
  voice.play();
  waterDropper.angle=80;
  arrow.x=1250;
  arrow.y=850;
  arrow.visible=true;
  dialog.text="Drag the dropper and put a drop of water on the glass slide.";

  waterDropper.events.onDragStart.add(function() {this.onDragStart(waterDropper)}, this);
  waterDropper.events.onDragStop.add(function() {this.onDragStop(waterDropper)}, this);
  waterDropper.inputEnabled = true;
  waterDropper.input.useHandCursor = true;
  waterDropper.input.enableDrag(true);
  this.game.physics.arcade.enable(waterDropper);
  waterDropper.enableBody =true;

},

// drawVLine:function(xp,yp,val){
//   var line1=this.game.add.graphics(0, 0);
//       line1.lineStyle(2,0xFFFFFF,1);
//       line1.moveTo(xp, yp);
//       yp+=20;
//       line1.lineTo(xp, yp);
//       fontStyle2={ font: "20px Segoe UI", fill: "#ffffff", align: "center" };
//   var text1 = this.game.add.text(0,0,val,fontStyle2);
//       text1.x=xp-10;
// //       text1.y=yp+10;

// },
onDragStart:function(obj)
  {
    //obj.angle=0;
    console.log("iiiiii"+obj);
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==waterDropper)
    {
      arrow.visible=false;
      arrow.x=900;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1310;
       currentobj.yp=890;
       waterDropper.angle=0;

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

        waterDropper.frameName="Water_To_Dropper0005.png";
       
    }


    if(currentobj==glycerineDropper)
    {
      arrow.visible=false;
      arrow.x=900;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1585;
       currentobj.yp=690;
       //waterDropper.angle=0;

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

        glycerineDropper.frameName="Water_To_Dropper0005.png";
       
    }
    if(currentobj==methyleneBottle)
    {
      arrow.visible=false;
      arrow.x=1250;
      arrow.y=850;
      arrow.visible=true;

       currentobj.xp=1550;
       currentobj.yp=760;
       

        colliderA.inputEnabled=true;
        colliderA.enableBody=true;
        colliderA.visible=true;

    }

        
    if(currentobj==methyleneDropper)
    {
      arrow.visible=false;
      arrow.x=900;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1310;
       currentobj.yp=890;
       methyleneDropper.angle=0;

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

        methyleneDropper.frameName="Liquid_to_dropper0004.png";
        // glassSlideWater.visible=false;
        // glassSlide.visible=false;
       
    }

           
    if(currentobj==coverSlip)
    {
      arrow.visible=false;
      arrow.x=900;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1200;
       currentobj.yp=900;
       

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

    
       
    }

            
    if(currentobj==iceScreamStick)
    {
      arrow.visible=false;
      arrow.x=900;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=550;
       currentobj.yp=890;
       

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

       
       
    }

    if(currentobj==filterPaperSprite)
    {
      arrow.visible=false;
      arrow.x=900;
      arrow.y=870;
      arrow.visible=true;

       currentobj.xp=1500;
       currentobj.yp=950;
       

        colliderB.inputEnabled=true;
        colliderB.enableBody=true;
        colliderB.visible=true;

    
       
    }

    if(currentobj==glassSlideFinal)
    {
      arrow.visible=false;
      arrow.x=265;
      arrow.y=690;
      arrow.visible=true;

       currentobj.xp=700;
       currentobj.yp=900;
       

        colliderC.inputEnabled=true;
        colliderC.enableBody=true;
        colliderC.visible=true;

    
       
    }
       


  },

  onDragStop:function(obj)
  {
    obj.body.enable =true;

    console.log("jjjjj"+obj);
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    //currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==waterDropper)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;

      waterDropper.x=900;
      waterDropper.y=705;

      waterDropper.animations.play('anim');
      waterDrop.visible=true;

     waterDrop.animations.play('anim');
     waterDrop.animations.currentAnim.onComplete.add(this.waterDropAnimationEnd, this);

      glassSlide.visible=false;
      glassSlideWater.visible=true;
       
    }

    if(currentobj==glycerineDropper)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;

      glycerineDropper.x=900;
      glycerineDropper.y=705;

      glycerineDropper.animations.play('anim');
      glycerineDrop.visible=true;

     glycerineDrop.animations.play('anim');

     glassSlideStickSprite.visible=false;
     glassSlideGlycerineSprite.visible=true;
     glassSlideGlycerineSprite.animations.play('anim'); 
     glycerineDrop.animations.currentAnim.onComplete.add(this.glycerineDropAnimationEnd2, this);

      // glassSlide.visible=false;
      // glassSlideWater.visible=true;
       
    }

     
    if(currentobj==methyleneDropper)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;

      methyleneDropper.x=900;
      methyleneDropper.y=705;

      methyleneDropper.animations.play('anim');
      methyleneDrop2.visible=true;

      methyleneDrop2.animations.play('anim');

      glassSlideWater.visible=false;
      glassSlide.visible=false;

      glassSlideLiquid.visible=true;
      glassSlideLiquid.animations.play('anim');


      methyleneDrop2.animations.currentAnim.onComplete.add(this.methyleDrop2AnimationEnd, this);

    //   glassSlide.visible=false;
    //   glassSlideWater.visible=true;
       
    }

    // if(currentobj==waterDropper)
    // {

    //   colliderB.inputEnabled=false;
    //   colliderB.enableBody=false;
    //   colliderB.visible=false;

    //   arrow.visible=false;

    //   waterDropper.x=900;
    //   waterDropper.y=705;

    //   waterDropper.animations.play('anim');
    //   waterDrop.visible=true;

    //  waterDrop.animations.play('anim');
    //  waterDrop.animations.currentAnim.onComplete.add(this.waterDropAnimationEnd, this);

    //   glassSlide.visible=false;
    //   glassSlideWater.visible=true;
       
    // }

    if(currentobj==methyleneBottle)
    {

      colliderA.inputEnabled=false;
      colliderA.enableBody=false;
      colliderA.visible=false;
      arrow.visible=false;

      methyleneBottle.x=1385;
      methyleneBottle.y=895;

      methyleneBottle.animations.play('anim');

      methyleneBottle.animations.currentAnim.onComplete.add(this.methyleneBottleAnimationEnd, this);
     
     
       
    }

    if(currentobj==iceScreamStick)

    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;
      iceScreamStick.visible=false;
      glassSlideLiquid.visible=false;
      glassSlideStickSprite.visible=true;
      glassSlideStickSprite.animations.play('anim');
      glassSlideStickSprite.animations.currentAnim.onComplete.add(this.cellTransferAnimationEnd, this);
    }
    if(currentobj==coverSlip)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;

    
      coverSlip.visible=false;
      needle.visible=false;
     glassSlideGlycerineSprite.visible=false;
      glassSlideCoverSlipSprite.visible=true;
      glassSlideCoverSlipSprite.animations.play('anim');
      glassSlideCoverSlipSprite.animations.currentAnim.onComplete.add(this.glassSlideCoverSlipAnimationEnd, this);

       
    }

    if(currentobj==filterPaperSprite)
    {

      colliderB.inputEnabled=false;
      colliderB.enableBody=false;
      colliderB.visible=false;

      arrow.visible=false;
      filterPaperSprite.x=910;
      filterPaperSprite.y=900;
      filterPaperSprite.animations.play('anim');
      filterPaperSprite.animations.currentAnim.onComplete.add(this.filterPaperAnimationEnd, this);
    }
    if(currentobj==glassSlideFinal)
    {
      voice.destroy();
      voice=this.game.add.audio("A_exp_procedure14",1);
       voice.play();
      arrow.visible=false;
      colliderC.inputEnabled=false;
      colliderC.enableBody=false;
      colliderC.visible=false;

      glassSlide2.visible=true;

      glassSlideFinal.visible=false;

      dialog.text="Click on the eyepiece of the microscope to see the structure of\nthe cell.";
      
      arrow.x=200;
      arrow.y=330;
      arrow.angle=-90;
      arrow.visible=true;

      colliderD.visible=true;
      colliderD.inputEnabled = true;
      colliderD.input.useHandCursor = true;
      colliderD.events.onInputDown.add(this.clickOnEyePiece,this);

    }

    
       
  },
  clickOnEyePiece:function()
  {
    arrow.visible=false;
    bg.visible=false;
    microscope.visible=false;
    colliderD.inputEnabled=false;
    colliderD.input.useHandCursor =false;
    colliderD.visible=false;

    bgTopView.visible=true;
    microscopeTopview.visible=true;
    dialog_box.visible=false;
    dialog.text="";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.microscopicViewTweens, this);
    

  },
  microscopicViewTweens:function()
  {

    microscopeTopviewTween=this.game.add.tween(microscopeTopview.scale).to( {x:7,y:7}, 1000, Phaser.Easing.Out, true);
    bgBlack.visible=true;
    bgBlackTween=this.game.add.tween(bgBlack).to( {alpha:1}, 900, Phaser.Easing.Out, true);
    
    microscopeCellView.visible=true;
    microscopeCellViewTween=this.game.add.tween(microscopeCellView.scale).to( {x:1,y:1}, 2500, Phaser.Easing.Out, true);
    microscopeCellViewTween2=this.game.add.tween(microscopeCellView).to( {alpha:1}, 2000, Phaser.Easing.Out, true);
    microscopeCellViewTween2.onComplete.add(function(){this.observeThecell()}, this);

  },

  observeThecell:function()
  {
    voice.destroy();
 voice=this.game.add.audio("A_exp_procedure15",1);
  voice.play();
    dialog_box.visible=true;
    dialog.text="Observe and understand the structure of the cheek cell.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.toObservation, this);

  },

  toObservation:function()
  {
    voice.destroy();
 voice=this.game.add.audio("A_exp_procedure16",1);
  voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;

  },
  

  filterPaperAnimationEnd:function()
  {
    // filterPaperSprite.visible=false;
    filterPaperSpriteTween=this.game.add.tween(filterPaperSprite).to( {x:2500}, 1500, Phaser.Easing.Out, true);
    glassSlideCoverSlipSprite.visible=false;
    glassSlideFinal.visible=true;
    filterPaperSpriteTween.onComplete.add(function(){this.gotoMicroscopicView()}, this);
    //this.game.time.events.add(Phaser.Timer.SECOND*1,this.gotoMicroscopicView, this);

  },

  gotoMicroscopicView:function()
  {
    voice.destroy();
 voice=this.game.add.audio("A_exp_procedure13",1);
  voice.play();
    dialog.text="Nice.... Now take the glass slide to the microscope and observe the\nmagnified cell structure.";

    glassSlideFinal.events.onDragStart.add(function() {this.onDragStart(glassSlideFinal)}, this);
    glassSlideFinal.events.onDragStop.add(function() {this.onDragStop(glassSlideFinal)}, this);
    glassSlideFinal.inputEnabled = true;
    glassSlideFinal.input.useHandCursor = true;
    glassSlideFinal.input.enableDrag(true);
    this.game.physics.arcade.enable(glassSlideFinal);
    glassSlideFinal.enableBody =true;
    arrow.x=900;
    arrow.y=870;
    arrow.visible=true;

  },

  glassSlideCoverSlipAnimationEnd:function()
  {
    voice.destroy();
 voice=this.game.add.audio("A_exp_procedure12",1);
  voice.play();
     dialog.text="Good job....now gently wipe the excess water from the glass slide by\nusing the filter paper.";
     arrow.x=1480;
     arrow.y=850;
     arrow.visible=true;

    filterPaperSprite.events.onDragStart.add(function() {this.onDragStart(filterPaperSprite)}, this);
    filterPaperSprite.events.onDragStop.add(function() {this.onDragStop(filterPaperSprite)}, this);
    filterPaperSprite.inputEnabled = true;
    filterPaperSprite.input.useHandCursor = true;
    filterPaperSprite.input.enableDrag(true);
    this.game.physics.arcade.enable(filterPaperSprite);
    filterPaperSprite.enableBody =true;
  },

  glycerineDropAnimationEnd2:function()
  {
    glycerineDrop.visible=false;
    arrow.visible=false;
    glycerineDropperTween4=this.game.add.tween(glycerineDropper).to( {x:1585,y:490}, 600, Phaser.Easing.Out, true);
    glycerineDropperTween4.onComplete.add(function(){this.glycerineDropperTween4End()}, this);

  },

  glycerineDropperTween4End:function()
  {
    arrow.visible=false;
    glycerineDropperTween5=this.game.add.tween(glycerineDropper).to( {y:690}, 600, Phaser.Easing.Out, true);
    glycerineDropperTween5.onComplete.add(function(){this.glycerineDropperTween5End()}, this);
  },

  glycerineDropperTween5End:function()
  {
    arrow.visible=false;
    glycerineGroup.add(glycerineDropper);
    glycerineGroup.add(petriDishMixSprite);
    glycerineGroupTweenOut=this.game.add.tween(glycerineGroup).to( {x:2500}, 600, Phaser.Easing.Out, true);
    glycerineGroupTweenOut.onComplete.add(function(){this.glycerineGroupTweenOutEnd()}, this);


  },

  glycerineGroupTweenOutEnd:function()
  {
  coverSlipGroup.visible=true;
  coverSlipGroupTweenIn=this.game.add.tween(coverSlipGroup).to( {x:0}, 600, Phaser.Easing.Out, true);
  coverSlipGroupTweenIn.onComplete.add(function(){this.coverSlipGroupTweenInEnd()}, this);
  },

  coverSlipGroupTweenInEnd:function()
  {
    voice.destroy();
 voice=this.game.add.audio("A_exp_procedure11",1);
  voice.play();

    arrow.x=1250;
    arrow.y=850;
    arrow.visible=true;

    dialog.text="Take the cover slip and place it on the glass slide carefully with help\nof the needle and then give a tap on the cover slip.";

    coverSlip.events.onDragStart.add(function() {this.onDragStart(coverSlip)}, this);
    coverSlip.events.onDragStop.add(function() {this.onDragStop(coverSlip)}, this);
    coverSlip.inputEnabled = true;
    coverSlip.input.useHandCursor = true;
    coverSlip.input.enableDrag(true);
    this.game.physics.arcade.enable(coverSlip);
    coverSlip.enableBody =true;

  },

  cellTransferAnimationEnd:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure9",1);
    voice.play();
    dialog.text="Good job, you have successfully transfered the scrapping to the staine.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.addGlycerine, this);

  },

  addGlycerine:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure3",1);
    voice.play();
    dialog.text="Now take the dropper.";
    arrow.x=1250;
    arrow.y=950;
    arrow.visible=true;

    glycerineDropper.inputEnabled = true;
    glycerineDropper.input.useHandCursor = true;
    glycerineDropper.events.onInputDown.add(this.clickOnGlycerineDropper,this);
  },

  methyleDrop2AnimationEnd:function()
  {
    methyleneDropper.visible=false;
    waterDropper.visible=false;
    glycerineDropper.visible=true;
    methyleneDrop2.visible=false;

  glycerineGroupTweenIn=this.game.add.tween(glycerineGroup).to( {x:0}, 800, Phaser.Easing.Out, true);
  glycerineGroupTweenIn.onComplete.add(function(){this.glycerineGroupTweenInEnd()}, this);

  },

  glycerineGroupTweenInEnd:function()
  {
    voice.destroy();

    voice=this.game.add.audio("A_exp_procedure8",1);
    voice.play();
    dialog.text="Now transfer the scrapping from the ice cream stick to the stain.";
    arrow.x=605;
    arrow.y=880;
    arrow.visible=true;

    iceScreamStick.events.onDragStart.add(function() {this.onDragStart(iceScreamStick)}, this);
    iceScreamStick.events.onDragStop.add(function() {this.onDragStop( iceScreamStick)}, this);
    iceScreamStick.inputEnabled = true;
    iceScreamStick.input.useHandCursor = true;
    iceScreamStick.input.enableDrag(true);
    this.game.physics.arcade.enable( iceScreamStick);
    iceScreamStick.enableBody =true;




  },

  methyleneBottleAnimationEnd:function()
  {
    methyleneDrop1.visible=true;
    methyleneDrop1.animations.play('anim');
    methyleneDrop1.animations.currentAnim.onComplete.add(this.methyleneDrop1AnimationEnd, this);
  },

  methyleneDrop1AnimationEnd:function()
  {
    methyleneDrop1.visible=false;
    petriDishWater.visible=false;
    petriDishMixSprite.visible=true;
    petriDishMixSprite.animations.play('anim');
    petriDishMixSprite.animations.currentAnim.onComplete.add(this.petriDishAnimationEnd, this);
    // methyleneBottle.angle=120;
  

  },

  petriDishAnimationEnd:function()
  {
    methyleneBottle.frameName='Liquid_bottle0001.png';
    methyleneBottle.x=1550;
    methyleneBottle.y=760;
    methyleneBottleTween1=this.game.add.tween(methyleneBottle).to( {x:2000}, 400, Phaser.Easing.Out, true);
    methyleneBottleTween1.onComplete.add(function(){this.methyleneBottleTween1End()}, this);
   
  },
  methyleneBottleTween1End:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure6",1);
    voice.play();
    dialog.text="Good... Now lets add a drop of methylene blue liquid on to the glass\nslide using the dropper.";
    waterDropper.visible=false;
    methyleneDropper.visible=true;
    arrow.x=1250;
    arrow.y=950;
    arrow.visible=true;

  methyleneDropper.inputEnabled = true;
  methyleneDropper.input.useHandCursor = true;
  methyleneDropper.events.onInputDown.add(this.clickOnMethyleneDropper,this);

  },

  clickOnMethyleneDropper:function()
  {

    if(isLiquidTaken==false)
    {
      arrow.visible=false;
      methyleneDropperTween1=this.game.add.tween(methyleneDropper).to( {x:1310,y:890}, 600, Phaser.Easing.Out, true);
      methyleneDropperTween1.onComplete.add(function(){this.methyleneDropperTween1End()}, this);
      isLiquidTaken=true;
    }

    // else if(isGlycerineTaken==false)
    // {
    //   arrow.visible=false;
    //   methyleneDropperTween1=this.game.add.tween(methyleneDropper).to( {x:1585,y:490}, 600, Phaser.Easing.Out, true);
    //   isGlycerineTaken=true;
    // //  methyleneDropperTween1.onComplete.add(function(){this.methyleneDropperTween1End()}, this);

    // }
  },

  clickOnGlycerineDropper:function()
  {
    if(isGlycerineTaken==false)
    {
      arrow.visible=false;
      glycerineDropper.angle=0;
      glycerineDropperTween1=this.game.add.tween(glycerineDropper).to( {x:1585,y:490}, 600, Phaser.Easing.Out, true);
      glycerineDropperTween1.onComplete.add(function(){this.glycerineDropperTween1End()}, this);

     isGlycerineTaken=true;
    }

  },

  glycerineDropperTween1End:function()
  {
    glycerineDropperTween2=this.game.add.tween(glycerineDropper).to( {y:690}, 600, Phaser.Easing.Out, true);
    glycerineDropperTween2.onComplete.add(function(){this.glycerineDropperTween2End()}, this);

  },

  glycerineDropperTween2End:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure10",1);
    voice.play();

    dialog.text="Add some glycerine to the glass slide.";

    arrow.x=1590;
    arrow.y=490;
    arrow.visible=true;

    glycerineDropper.events.onDragStart.add(function() {this.onDragStart(glycerineDropper)}, this);
    glycerineDropper.events.onDragStop.add(function() {this.onDragStop(glycerineDropper)}, this);
    glycerineDropper.inputEnabled = true;
    glycerineDropper.input.useHandCursor = true;
    glycerineDropper.input.enableDrag(true);
    this.game.physics.arcade.enable(glycerineDropper);
    glycerineDropper.enableBody =true;

  },


  methyleneDropperTween1End:function()
{
  voice.destroy();
  voice=this.game.add.audio("A_exp_procedure7",1);
  voice.play();
  methyleneDropper.inputEnabled = false;
  methyleneDropper.input.useHandCursor = false;

  methyleneDropper.angle=80;
  arrow.x=1250;
  arrow.y=850;
  arrow.visible=true;
  dialog.text="Drag the dropper and put a drop of methylene blue on the glass slide.";

  methyleneDropper.events.onDragStart.add(function() {this.onDragStart(methyleneDropper)}, this);
  methyleneDropper.events.onDragStop.add(function() {this.onDragStop(methyleneDropper)}, this);
  methyleneDropper.inputEnabled = true;
  methyleneDropper.input.useHandCursor = true;
  methyleneDropper.input.enableDrag(true);
  this.game.physics.arcade.enable(methyleneDropper);
  methyleneDropper.enableBody =true;

},


  waterDropAnimationEnd:function()
  {
    
    waterDrop.visible=false;

    waterDropper.inputEnabled = false;
    waterDropper.input.useHandCursor = false;

    waterDropper.x=1250;
    waterDropper.y=1004;
    waterDropper.angle=90;

    this.game.time.events.add(Phaser.Timer.SECOND*1,this.addMethyleneToWater, this);

  },

  addMethyleneToWater:function()
  {
    voice.destroy();
    voice=this.game.add.audio("A_exp_procedure5",1);
    voice.play();
    dialog.text="Add few drops of Methylene blue liquid to the petridish containing\nwater.";
    arrow.x=1590;
    arrow.y=600;
    arrow.visible=true;

    methyleneBottle.events.onDragStart.add(function() {this.onDragStart(methyleneBottle)}, this);
    methyleneBottle.events.onDragStop.add(function() {this.onDragStop(methyleneBottle)}, this);
    methyleneBottle.inputEnabled = true;
    methyleneBottle.input.useHandCursor = true;
    methyleneBottle.input.enableDrag(true);
    this.game.physics.arcade.enable(methyleneBottle);
    methyleneBottle.enableBody =true;

  },

 

  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
   
    

  },

  
 
  detectCollision:function(){
   

    if(colliderA.enableBody && currentobj!=null)
    {
      this.game.physics.arcade.overlap(currentobj, colliderA,function() {this.match_Obj()},null,this);
      collider=colliderA;
    }

    if(colliderB.enableBody && currentobj!=null)
    {
      this.game.physics.arcade.overlap(currentobj,colliderB,function() {this.match_Obj()},null,this);
      collider=colliderB;
    }
    if(colliderC.enableBody && currentobj!=null)
    {
    this.game.physics.arcade.overlap(currentobj, colliderC,function() {this.match_Obj()},null,this);
    collider=colliderC;
    }
    // if(collider_scale.enableBody && currentobj!=null)
    // {
    //   this.game.physics.arcade.overlap(currentobj, collider_scale,function() {this.match_Obj()},null,this);
    //   collider=collider_scale;
    // }
  

    
     if(currentobj!=null && currentobj.body.enable)
     {
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
       if(currentobj==waterDropper)
       {
        arrow.visible=false;
        arrow.x=1250;
        arrow.y=850;
        arrow.visible=true;
        waterDropper.angle=80;
        waterDropper.frameName="Water_To_Dropper0001.png"; 
       }

       if(currentobj==methyleneDropper)
       {
        arrow.visible=false;
        arrow.x=1250;
        arrow.y=850;
        arrow.visible=true;
        methyleneDropper.angle=80;
        methyleneDropper.frameName="Liquid_to_dropper0001.png"; 
       }


       if(currentobj==methyleneBottle)
       {
         arrow.visible=false;
         arrow.x=1590;
         arrow.y=600;
         arrow.visible=true;
         methyleneBottle.frameName='Liquid_bottle0001.png';
       }

       if(currentobj==iceScreamStick)
       {
         arrow.visible=false;
         arrow.x=605;
         arrow.y=880;
         arrow.visible=true;
         
       }
       if(currentobj==glycerineDropper)
       {

        arrow.visible=false;
        arrow.x=1590;
        arrow.y=490;
        arrow.visible=true;
       }
       if(currentobj==coverSlip)
       {

        arrow.visible=false;
        arrow.x=1250;
        arrow.y=850;
        arrow.visible=true;

       }

       if(currentobj==filterPaperSprite)
       {

        arrow.visible=false;
        arrow.x=1480;
        arrow.y=850;
        arrow.visible=true;
       }
       if(currentobj==glassSlideFinal)
       {
        arrow.visible=false;
        arrow.x=900;
        arrow.y=870;
        arrow.visible=true;

       }
     
      
       
        // arrow.x=1500;
        // arrow.y=450;
        // arrow_y=450;
        // arrow.visible=true;
        currentobj=null;
      }


      
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Observations", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
