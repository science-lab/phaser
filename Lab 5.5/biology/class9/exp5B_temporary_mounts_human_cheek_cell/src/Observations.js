var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var previousPageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("A_observations1",1);
    voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  previousPageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  //base= this.game.add.sprite(180,80,'observation_table');


 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  this.arrangeScene();
  next_btn = this.game.add.sprite(1645,870,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1645,870,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
  prev_btn = this.game.add.sprite(210,870,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(210,870,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  //prev_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrev, this);
  prev_btn.visible=false;
 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    

    arrangeScene:function()
    {
      baseHead=this.game.add.sprite(80,90,'obs_base_head');

      ///////page1//////////////

      base=this.game.add.sprite(75,160,'obs_base');
      base.scale.setTo(1,1.8);

      obs1_text="1. A Large number of flat and irregular-shaped cells are seen with a thin cell membrane.";
      obs1=this.game.add.text(150,250,obs1_text,fontStyle);

      obs2_text="2. Each cell has a thin cell (plasma) membrane. Cell wall is absent.";
      obs2=this.game.add.text(150,360,obs2_text,fontStyle);

      obs3_text="3. A darkly stained nucleus lies in the center of the cell.";
      obs3=this.game.add.text(150,470,obs3_text,fontStyle);

      obs4_text="4. Cytoplasm lies inside the cell membrane which is granular in appearance.";
      obs4=this.game.add.text(150,580,obs4_text,fontStyle);
      
      obs5_text="5. There are no inter cellular spaces in the cell.";
      obs5=this.game.add.text(150,690,obs5_text,fontStyle);

      //page2////////////////////
      cellImage=this.game.add.sprite(440,180,'cell_image');
      cellImage.scale.set(1.5);
      cellImage.visible=false;

      markLine1=this.game.add.sprite(865,380,'obs_line');
      markLine1.scale.setTo(1,.3);//01//.3
      markLine1.angle=90;
      markLine1.anchor.setTo(.5,1);
      markLine1.visible=false;

      label1=this.game.add.text(1120,350,"Cell membrane",fontStyle);
      label1.alpha=0;
     label1.visible=false;


      markLine2=this.game.add.sprite(938,520,'obs_line');
      markLine2.scale.setTo(1,.3);//01//.3
      markLine2.angle=90;
      markLine2.anchor.setTo(.5,1);
      markLine2.visible=false;

      label2=this.game.add.text(1190,495,"Nucleus",fontStyle);
      label2.alpha=0;
      label2.visible=false;


      markLine3=this.game.add.sprite(920,770,'obs_line');
      markLine3.scale.setTo(1,.3);//01//.3
      markLine3.angle=90;
      markLine3.anchor.setTo(.5,1);
      markLine3.visible=false;

      label3=this.game.add.text(1180,748,"Cytoplasm",fontStyle);
      label3.alpha=0;
      label3.visible=false;

      label4=this.game.add.text(680,880,"Human cheek cell",fontStyle);
      label4.visible=false;

      /////page3///////////////

      // line1=this.game.add.sprite(230,596,'obs_line');
      // line1.anchor.set(.5);

      // line2=this.game.add.sprite(830,596,'obs_line');
      // line2.anchor.set(.5);

      // line3=this.game.add.sprite(925,280,'obs_line');
      // line3.scale.setTo(1,2.09);
      // line3.anchor.set(.5);
      // line3.angle=90;

      // obsHead1=this.game.add.text(120,215,"S/N",fontStyle);
      // obsHead2=this.game.add.text(420,215,"Character",fontStyle);
      // obsHead3=this.game.add.text(1160,215,"Observation",fontStyle);

      // obsSn1=this.game.add.text(140,295,"1",fontStyle);
      // obsSn1=this.game.add.text(140,370,"2",fontStyle);
      // obsSn1=this.game.add.text(140,445,"3",fontStyle);
      // obsSn1=this.game.add.text(140,520,"4",fontStyle);
      // obsSn1=this.game.add.text(140,595,"5",fontStyle);
      // obsSn1=this.game.add.text(140,670,"6",fontStyle);
      // obsSn1=this.game.add.text(140,745,"7",fontStyle);
      // obsSn1=this.game.add.text(140,820,"8",fontStyle);

      // obsCharacter1=this.game.add.text(400,295,"Shape of cell",fontStyle);
      // obsCharacter2=this.game.add.text(400,370,"Arrangement",fontStyle);
      // obsCharacter3=this.game.add.text(400,445,"Inter cellular spaces",fontStyle);
      // obsCharacter4=this.game.add.text(400,520,"Cell membrane",fontStyle);
      // obsCharacter5=this.game.add.text(400,595,"Cell wall",fontStyle);
      // obsCharacter6=this.game.add.text(400,670,"Cytoplasm",fontStyle);
      // obsCharacter7=this.game.add.text(400,745,"Nucleus",fontStyle);
      // obsCharacter8=this.game.add.text(400,820,"Vacuoles",fontStyle);

      // obsObservation1=this.game.add.text(845,295,"Irregular shape",fontStyle);
      // obsObservation1=this.game.add.text(845,370,"Flat compactly arranged",fontStyle);
      // obsObservation1=this.game.add.text(845,445,"Absent",fontStyle);
      // obsObservation1=this.game.add.text(845,520,"Prominent",fontStyle);
      // obsObservation1=this.game.add.text(845,595,"Absent",fontStyle);
      // obsObservation1=this.game.add.text(845,670,"Granular",fontStyle);
      // obsObservation1=this.game.add.text(845,745,"Prominent darkly stained in the center of cytoplasm",fontStyle);
      // obsObservation1=this.game.add.text(845,820,"Absent or very small",fontStyle);
      
     

    },

        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy();
                voice=this.game.add.audio("A_observations1",1);
                voice.play();
              if(previousPageCount==2)
              {
                
                cellImage.visible=false;
                markLine1.visible=false;
                label1.visible=false;
                markLine2.visible=false;
                label2.visible=false;
                markLine3.visible=false;
                label3.visible=false;
                label4.visible=false;

              }
            previousPageCount=1;

              obs1_text="1. A Large number of flat and irregular-shaped cells are seen with a thin cell membrane.";
              obs1=this.game.add.text(150,250,obs1_text,fontStyle);
        
              obs2_text="2. Each cell has a thin cell (plasma) membrane. The Cell wall is absent.";
              obs2=this.game.add.text(150,360,obs2_text,fontStyle);
        
              obs3_text="3. A darkly stained nucleus lies in the center of the cell.";
              obs3=this.game.add.text(150,470,obs3_text,fontStyle);
        
              obs4_text="4. Cytoplasm lies inside the cell membrane which is granular in appearance.";
              obs4=this.game.add.text(150,580,obs4_text,fontStyle);
              
              obs5_text="5. There are no inter cellular spaces in the cell.";
              obs5=this.game.add.text(150,690,obs5_text,fontStyle);
              // voice.destroy(true);
              // voice=this.game.add.audio("Observation_1",1);
              //   voice.play();
              
              ///////////////////////////////////////////
              
              break;
            case 2:
              voice.destroy();
              if(previousPageCount==3)
              {
             
                line1.visible=false;
                line2.visible=false;
                line3.visible=false;

                obsHead1.text="";
                obsHead2.text="";
                obsHead3.text="";

                obsSn1.text="";
                obsSn2.text="";
                obsSn3.text="";
                obsSn4.text="";
                obsSn5.text="";
                obsSn6.text="";
                obsSn7.text="";
                obsSn8.text="";

                obsCharacter1.text="";
                obsCharacter2.text="";
                obsCharacter3.text="";
                obsCharacter4.text="";
                obsCharacter5.text="";
                obsCharacter6.text="";
                obsCharacter7.text="";
                obsCharacter8.text="";

                obsObservation1.text="";
                obsObservation2.text="";
                obsObservation3.text="";
                obsObservation4.text="";
                obsObservation5.text="";
                obsObservation6.text="";
                obsObservation7.text="";
                obsObservation8.text="";
              }
              
              
                obs1.text="";
                obs2.text="";
                obs3.text="";
                obs4.text="";
                obs5.text="";

                // cellImage.visible=true;
                // label4.visible=true;

                cellImage=this.game.add.sprite(500,260,'cell_image');
                cellImage.scale.set(1.3);
                cellImage.visible=true;
                markLine1=this.game.add.sprite(800,380,'obs_line');
                markLine1.scale.setTo(1,.3);//01//.3
                markLine1.angle=90;
                markLine1.anchor.setTo(.5,1);
                label1=this.game.add.text(1050,350,"Cell membrane",fontStyle);
               
  
                markLine2=this.game.add.sprite(938,560,'obs_line');
                markLine2.scale.setTo(1,.3);//01//.3
                markLine2.angle=90;
                markLine2.anchor.setTo(.5,1);
          
                label2=this.game.add.text(1180,535,"Nucleus",fontStyle);
          
        
                markLine3=this.game.add.sprite(920,770,'obs_line');
                markLine3.scale.setTo(1,.3);//01//.3
                markLine3.angle=90;
                markLine3.anchor.setTo(.5,1);

                label3=this.game.add.text(1180,748,"Cytoplasm",fontStyle);
            
   
          
                label4=this.game.add.text(700,880,"Human cheek cell",fontStyle);
                previousPageCount=2;
              
             
                
                
              
               
              
        
              // voice.destroy(true);
              // voice=this.game.add.audio("Observation_2",1);
              //   voice.play();
     
            break;
            case 3:
                voice.destroy();
                voice=this.game.add.audio("A_observations2",1);
                voice.play();
                    if(previousPageCount==2)
                    {

                cellImage.visible=false;
                markLine1.visible=false;
                label1.visible=false;
                markLine2.visible=false;
                label2.visible=false;
                markLine3.visible=false;
                label3.visible=false;
                label4.visible=false;
               
                    
                  previousPageCount=3;
                  line1=this.game.add.sprite(230,596,'obs_line');
                  line1.anchor.set(.5);

                  line2=this.game.add.sprite(830,596,'obs_line');
                  line2.anchor.set(.5);

                  line3=this.game.add.sprite(925,280,'obs_line');
                  line3.scale.setTo(1,2.09);
                  line3.anchor.set(.5);
                  line3.angle=90;

                  obsHead1=this.game.add.text(120,215,"S/N",fontStyle);
                  obsHead2=this.game.add.text(420,215,"Character",fontStyle);
                  obsHead3=this.game.add.text(1160,215,"Observation",fontStyle);

                  obsSn1=this.game.add.text(140,295,"1",fontStyle);
                  obsSn2=this.game.add.text(140,370,"2",fontStyle);
                  obsSn3=this.game.add.text(140,445,"3",fontStyle);
                  obsSn4=this.game.add.text(140,520,"4",fontStyle);
                  obsSn5=this.game.add.text(140,595,"5",fontStyle);
                  obsSn6=this.game.add.text(140,670,"6",fontStyle);
                  obsSn7=this.game.add.text(140,745,"7",fontStyle);
                  obsSn8=this.game.add.text(140,820,"8",fontStyle);

                  obsCharacter1=this.game.add.text(400,295,"Shape of cell",fontStyle);
                  obsCharacter2=this.game.add.text(400,370,"Arrangement",fontStyle);
                  obsCharacter3=this.game.add.text(400,445,"Inter cellular spaces",fontStyle);
                  obsCharacter4=this.game.add.text(400,520,"Cell membrane",fontStyle);
                  obsCharacter5=this.game.add.text(400,595,"Cell wall",fontStyle);
                  obsCharacter6=this.game.add.text(400,670,"Cytoplasm",fontStyle);
                  obsCharacter7=this.game.add.text(400,745,"Nucleus",fontStyle);
                  obsCharacter8=this.game.add.text(400,820,"Vacuoles",fontStyle);

                  obsObservation1=this.game.add.text(845,295,"Irregular shape",fontStyle);
                  obsObservation2=this.game.add.text(845,370,"Flat compactly arranged",fontStyle);
                  obsObservation3=this.game.add.text(845,445,"Absent",fontStyle);
                  obsObservation4=this.game.add.text(845,520,"Prominent",fontStyle);
                  obsObservation5=this.game.add.text(845,595,"Absent",fontStyle);
                  obsObservation6=this.game.add.text(845,670,"Granular",fontStyle);
                  obsObservation7=this.game.add.text(845,745,"Prominent darkly stained in the center of cytoplasm",fontStyle);
                  obsObservation8=this.game.add.text(845,820,"Absent or very small",fontStyle);
                    }
            break;
            
          }
        },
        toNext:function(){
          if(pageCount>=1){
            prev_btn.visible=true;
            pageCount++;
              this.addObservation();
              if(pageCount>=3){
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      toPrev:function(){
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },

      markLine1TweenEnd:function()
      {
        label1.visible=true;
        label1Tween=this.game.add.tween(label1).to( {alpha:1}, 500, Phaser.Easing.Out, true);
        label1Tween.onComplete.add(function(){this.markLine2Tween()}, this);

      },
      markLine2Tween:function()
      {
        markLine2.visible=true;
        markLine2Tween=this.game.add.tween(markLine2.scale).to( {y:.3}, 500, Phaser.Easing.Out, true);
        markLine2Tween.onComplete.add(function(){this.markLine2TweenEnd()}, this);
      },

      markLine2TweenEnd:function()
      {
        label2.visible=true;
        label2Tween=this.game.add.tween(label2).to( {alpha:1}, 500, Phaser.Easing.Out, true);
        label2Tween.onComplete.add(function(){this.markLine3Tween()}, this);

      },

      markLine3Tween:function()
      {
        markLine3.visible=true;
        markLine3Tween=this.game.add.tween(markLine3.scale).to( {y:.3}, 500, Phaser.Easing.Out, true);
        markLine3Tween.onComplete.add(function(){this.markLine3TweenEnd()}, this);
      },
      markLine3TweenEnd:function()
      {
        label3.visible=true;
        label3Tween=this.game.add.tween(label3).to( {alpha:1}, 500, Phaser.Easing.Out, true);
        //label3Tween.onComplete.add(function(){this.markLine3Tween()}, this);

      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  