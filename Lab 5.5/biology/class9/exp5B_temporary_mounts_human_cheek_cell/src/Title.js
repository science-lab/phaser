var title = function(game)
{

///////////////////////////////////////////// VARIABLES DECLARATION ///////////////////////////////////////////// 

var background;
var next;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

var voice;
/*var voice2;*/
var timer;
//ip address
var ip;
}

title.prototype ={
init: function( ipadrs) 
{
ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
 
create:function()
{
if(this.game.sound.mute)
    {
      this.game.sound.mute = false;
    }

  timer = 0;
  hotflag=0;
  bg=this.game.add.sprite(0, 0,"bg1");
  //title=this.game.add.sprite(250, 780,'title');
  //bg.scale.setTo(.625,.6);

  voice=this.game.add.audio("title_a",1);
  voice2=this.add.audio("next_snd",1);
  voice.play();
  voice.onStop.add(this.nextSound2,this);

/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

  play = this.game.add.sprite(1420,870,'components','play_pressed.png');
  play.scale.setTo(.7,.7);
  play.inputEnabled = true;
  //play.input.priorityID = 3;
  play.input.useHandCursor = true;
  play.events.onInputDown.add(this.toAim, this);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////
 
//if (!this.game.device.desktop)
//   {
     	this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;

    /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
	  normalScreen.scale.setTo(2.5,2.5);	
    normalScreen.inputEnabled = true;
    normalScreen.input.useHandCursor = true;
    normalScreen.events.onInputUp.add(this.gonormal,this);*/

    fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
	  fullScreen1.scale.setTo(2.5,2.5);
    fullScreen1.inputEnabled = true;
    fullScreen1.input.useHandCursor = true;
    fullScreen1.events.onInputUp.add(this.gofull,this);	
    //}
 },

 ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

 gofull: function()
        {
        if (this.game.scale.isFullScreen)
           {
            this.game.scale.stopFullScreen();
           }
        else
           {
        this.game.scale.startFullScreen(false);
           }  
      
        },
 toAim:function()
      {
      voice.destroy();
      voice2.destroy();
      //if (!this.game.device.desktop)
       // {
       if (!this.game.scale.isFullScreen)
          {
			    this.gofull();
		      }
    //}
      this.state.start("Aim", true, false, ip);
      //this.state.start("Objective");

      },
nextSound2:function()
      {
      voice2.play();
      },
 buttonState:function()
      {
      //next.visible = false;
      //next_pressed.visible = true;
      },

 buttonState2:function()
      {
      //next.visible = true;
      //next_pressed.visible = false;
      }
}
