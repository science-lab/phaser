var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  var currentScene;
  var previousScene;
  var total_scene;
  
  }
  
  materials.prototype ={
  
  init: function(ipadrs) 
  {
    ip = ipadrs;
    

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();
  pageHeading="Materials required:";
  pageHeadingText=this.game.add.text(250,200,pageHeading,headfontStyle);



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1600,870,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1600,870,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(300,870,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(300,870,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toTheory, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
       
        total_scene=9;
        this.show_materials();
       

        
      },
      
      show_materials:function(){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
              }
              mat_img_1 = this.game.add.sprite(370,550,'m_microscope');
              mat_img_1.scale.setTo(.5,.5);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Microscope:",headfontStyle);
              mat_content_1="A microscope is an instrument that can be used to observe\nsmall objects, even cells. The image of an object is magnified\nthrough at least one lens in the microscope. This lens bends\nlight toward the eye and makes an object appear larger than it\nactually is.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

              // mat_img_2 = this.game.add.sprite(330,550,'test_tube');
              // mat_img_2.scale.setTo(.9,1);
              // mat_name_2=this.game.add.text(550,570,"Test tube:",headfontStyle);
              // mat_content_2="A test tube, also known as a culture tube or sample tube, \nis a common piece of laboratory glassware consisting of \na finger-like length of glass or clear plastic tubing, open \nat the top and closed at the bottom. Test tubes are \nusually placed in special-purpose racks.";
              // mat_text_2=this.game.add.text(550,670,mat_content_2,fontStyle);
          break;
          case 2:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              // mat_img_1 = this.game.add.sprite(230,350,'burner');

              mat_img_1 = this.game.add.sprite(370,550,'m_glycerinBottle');
              //mat_img_1.scale.setTo(.8,.8);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Glycerine:",headfontStyle);
              mat_content_1="Glycerine is used to expel air bubbles which can be found\nbetween slides.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

              //mat_img_1.scale.setTo(.9,1);
              // mat_name_1=this.game.add.text(650,400,"Bunsen burner:",headfontStyle);
              // mat_content_1="A Bunsen burner, named after Robert Bunsen, is a \nkind of gas burner used as laboratory equipment; \nit produces a single open gas flame and is used for \nheating, sterilization, and combustion.";
              // mat_text_1=this.game.add.text(650,500,mat_content_1,fontStyle);

              // mat_img_2.destroy(true);
              // mat_name_2.destroy(true);
              // mat_text_2.destroy(true);
          break;
          case 3:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);

              mat_img_1 = this.game.add.sprite(370,550,'m_dropper');
             // mat_img_2 = this.game.add.sprite(370,550,'m_mirror_side');
              mat_img_1.scale.setTo(.7,.7);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Dropper:",headfontStyle);
              mat_content_1="A dropper, also known as a Pasteur pipette, or dropper, is a \ndevice used to transfer small quantities of liquids. They are\nused in the laboratory.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);
              if(previousScene==4){
                // mat_img_2.destroy(true);
                // mat_name_2.destroy(true);
                // mat_text_2.destroy(true);
              }
              // mat_img_2 = this.game.add.sprite(250,670,'spoon');
              // mat_img_2.scale.setTo(.9,1);
              // mat_img_2.angle=45;
              // mat_name_2=this.game.add.text(540,660,"Spoon:",headfontStyle);
              // mat_content_2="A spoon is a utensil consisting of a small shallow bowl, \noval or round, at the end of a handle. A type of cutlery, \nespecially as part of a place setting, it is used \nprimarily for transferring items.";
              // mat_text_2=this.game.add.text(540,750,mat_content_2,fontStyle);
          break;
          case 4:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'m_petridish');
              mat_img_1.scale.setTo(.8,.8);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Petridish with water:",headfontStyle);
              mat_content_1="A petri dish is a flat, shallow dish made of glass or plastic\nwith a suitable lid. A petri dish is used to culture different types\nof cells, including bacteria and molds.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);
            
              // mat_img_2.destroy(true);
              // mat_name_2.destroy(true);
              // mat_text_2.destroy(true);
              
          break;
          case 5:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'m_ice_cream_stick');
              mat_img_1.scale.setTo(1.5,1.5);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Ice cream stick:",headfontStyle);
              //mat_content_1="In chemistry, a retort stand, also called a clamp stand, \na ring stand, or a support stand, is a piece of scientific \nequipment intended to support other pieces of \nequipment and glassware for instance, burettes, \ntest tubes and flasks.";
              mat_content_1="It is a wooden stick, which is used here to scrap the cheek cells.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

        //       // mat_img_2.destroy(true);
        //       // mat_name_2.destroy(true);
        //       // mat_text_2.destroy(true);
              
          break;

          case 6:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'m_glassSlide');
            mat_img_1.scale.setTo(.7,.7);
              mat_img_1.anchor.setTo(.5,.5);
            mat_name_1=this.game.add.text(600,270,"Glass slide:",headfontStyle);
            mat_content_1="A glass slide is a thin, flat, rectangular piece of glass that is\nused as a platform for microscopic specimen observation.";
            mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

        //     // mat_img_2.destroy(true);
        //     // mat_name_2.destroy(true);
        //     // mat_text_2.destroy(true);
            
         break;
         case 7:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'m_coverslip');
            mat_img_1.scale.setTo(1.5,1.5);
              mat_img_1.anchor.setTo(.5,.5);
            mat_name_1=this.game.add.text(600,270,"Cover slip:",headfontStyle);
            mat_content_1="A small square or circle of thin glass called a cover slip is placed\nover the specimen. It protects the microscope and prevents\nthe slide from drying out when it's being examined.";
            mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

        //     // mat_img_2.destroy(true);
        //     // mat_name_2.destroy(true);
        //     // mat_text_2.destroy(true);
            
         break;
         case 8:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'m_filterPaper');
            mat_img_1.scale.setTo(.7,.7);
              mat_img_1.anchor.setTo(.5,.5);
            mat_name_1=this.game.add.text(600,270,"Filter paper:",headfontStyle);
            mat_content_1="A Filter paper is a laboratory supply which is a semi-permeable\npaper barrier used to filter solutions.";
            mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

        //     // mat_img_2.destroy(true);
        //     // mat_name_2.destroy(true);
        //     // mat_text_2.destroy(true);
            
         break;

         case 9:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'m_methyleneBlue');
            mat_img_1.scale.setTo(.9,.9);
              mat_img_1.anchor.setTo(.5,.5);
            mat_name_1=this.game.add.text(600,270,"Methylene blue:",headfontStyle);
            mat_content_1="Methylene Blue solution has been used to stain cells to\ndetermine cell viability.";
            mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

        //     // mat_img_2.destroy(true);
        //     // mat_name_2.destroy(true);
        //     // mat_text_2.destroy(true);
            
         break;


        }
      },
      
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==total_scene){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
      toTheory:function()
        {
        voice.destroy();
        this.state.start("Theory", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  