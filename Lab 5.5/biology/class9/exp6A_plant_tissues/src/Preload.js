var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var theory_completed;
  var theory_Scene;
  var observation_completed;
  var observation_Scene;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Plant_tissues//////////////////////////////////////////////////////////
      this.game.load.image('bg_black','assets/Bg_black.png');
      this.game.load.image('bg_top','assets/Bg_top.png'); 
      this.game.load.image('microscope','assets/Plant_tissues/microscope.png'); 

      this.game.load.image('microscope_top_view','assets/Plant_tissues/Mocroscope_top_view.png');
        this.game.load.image('Parenchyma_1','assets/Plant_tissues/Parenchyma_1.png');
        this.game.load.image('Parenchyma_2','assets/Plant_tissues/Parenchyma_2.png');
        this.game.load.image('Collenchyma_1','assets/Plant_tissues/Collenchyma_1.png');
        this.game.load.image('Collenchyma_2','assets/Plant_tissues/Collenchyma_2.png');
        this.game.load.image('Sclerenchyma_1','assets/Plant_tissues/Sclerenchyma_1.png');
        this.game.load.image('Sclerenchyma_2','assets/Plant_tissues/Sclerenchyma_2.png');


        
        this.game.load.image('microscope_holder_right','assets/Plant_tissues/mocroscope_right_lock.png');
        this.game.load.image('microscope_holder_left','assets/Plant_tissues/mocroscope_left_lock.png');
        
        
      this.game.load.atlasJSONHash('slide_views', 'assets/Plant_tissues/slide_views.png', 'assets/Plant_tissues/slide_views.json');
        this.game.load.atlasJSONHash('arrow', 'assets/Plant_tissues/arrow.png', 'assets/Plant_tissues/arrow.json');
      this.game.load.atlasJSONHash('eyePiece_rotating_sprite', 'assets/Plant_tissues/eyePiece_rotating_sprite.png', 'assets/Plant_tissues/eyePiece_rotating_sprite.json');
      
      ///////////////materials//////////////////////
      this.game.load.image('m_microscope','assets/Plant_tissues/m_microscope.png');
      this.game.load.image('paranchyma_cell_structure','assets/Plant_tissues/paranchyma_cell_structure.png');
      this.game.load.image('Collenchyma_cell_structure','assets/Plant_tissues/Collenchyma_cell_structure.png');
      this.game.load.image('sclerencyma_cell_structure','assets/Plant_tissues/sclerencyma_cell_structure.png');
      
      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Observation_3','assets/audio/Observation_3.mp3');
      this.game.load.audio('Observation_4','assets/audio/Observation_4.mp3');
      this.game.load.audio('Observation_5','assets/audio/Observation_5.mp3');
      this.game.load.audio('Observation_6','assets/audio/Observation_6.mp3');
      this.game.load.audio('Observation_7','assets/audio/Observation_7.mp3');
      this.game.load.audio('Observation_8','assets/audio/Observation_8.mp3');
      this.game.load.audio('Observation_9','assets/audio/Observation_9.mp3');
      this.game.load.audio('Observation_10','assets/audio/Observation_10.mp3');
      this.game.load.audio('Observation_11','assets/audio/Observation_11.mp3');
      
      
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('Result_1','assets/audio/Result_1.mp3');
      this.game.load.audio('Result_2','assets/audio/Result_2.mp3');
      
      this.game.load.audio('step0','assets/audio/step0.mp3');
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');
      this.game.load.audio('step1_7','assets/audio/step1_7.mp3');
      this.game.load.audio('step1_8','assets/audio/step1_8.mp3');
      this.game.load.audio('step1_9','assets/audio/step1_9.mp3');
      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step3_1','assets/audio/step3_1.mp3');
      this.game.load.audio('step3_2','assets/audio/step3_2.mp3');
      this.game.load.audio('step3_3','assets/audio/step3_3.mp3');
      this.game.load.audio('step3_4','assets/audio/step3_4.mp3');
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      theory_completed=false;
      theory_Scene=1;
      observation_completed=false;
      observation_Scene=1;
      
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_3");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
     this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

