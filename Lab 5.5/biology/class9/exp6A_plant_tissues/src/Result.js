var result = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  var current_scene;
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  result.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  subfontStyle={ font: "26px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  result_text=this.game.add.text(250,200,"Result:",headfontStyle);
  
      result0="Slide 1 (Parenchyma tissue)";
      result1="1. Cells seen in this slide are isodiametric, have large vacuole and cytoplasm \n    with a prominent nucleus on the periphery (side) of the cell.";
      result2="2. Cell walls are thin.";
      result3="3. These are living cells and are found in the soft parts of the plant like stem, \n    root, leaves, flowers and fruits.";
      result4="4. They serve the function of storage, synthesis of food and help the aquatic \n    plants to float in water.";
      result5="5. The identifying features show the slide to be of parenchyma tissue.";

      result6="Slide 2 (Collenchyma tissue)";
      result7="1. Cells seen in this slide are spherical, oval and has large central vacuole.";
      result8="2. Cell wall is unevenly thickened. Corners are thickened due to deposition of \n    cellulose, hemicellulose and pectin.";
      result9="3. These are living cells.";
      result10="4. It is found below epidermis in petiole, leaves and stems in dicot.";
      result11="5. They serve the function of giving flexibility and mechanical strength to the \n    aerial parts of plants.";
      result12="6. The identifying features show the slide to be collenchyma tissue.";

      result13="Slide 3 (Sclerenchyma tissue)";
      result14="1. Cells seen in this slide have thickened hard cell walls with thickening of \n    lignin (a chemical).";
      result15="2. There is little or no protoplasm or cytoplasm.";
      result16="3. These are dead cells because nucleus is also absent.";
      result17="4. The cells are connected by oblique pits or perforations.";
      result18="5. They serve the function of giving mechanical strength to the plant and are \n    found in hard parts of the plant.";
      result19="6. The identifying features show the slide to be of sclerenchyma tissue.";

      

     
  voice=this.game.add.audio("Result",1);
  //voice.play();
  current_scene=1;
   this.addResult();

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  next_btn = this.game.add.sprite(1600,870,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1600,870,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 
  prev_btn = this.game.add.sprite(300,870,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(300,870,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  prev_btn.visible=false;
  //next_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrevious, this);
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toViva, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
    addResult:function(){
      switch(current_scene){
        case 1:
          voice.destroy(true);
              voice=this.game.add.audio("Result",1);
              voice.play();
          result0_text=this.game.add.text(250,300,result0,fontStyle);
          result1_text=this.game.add.text(300,375,result1,fontStyle);
          result2_text=this.game.add.text(300,500,result2,fontStyle);
          result3_text=this.game.add.text(300,575,result3,fontStyle);
          result4_text=this.game.add.text(300,700,result4,fontStyle);
          result5_text=this.game.add.text(300,825,result5,fontStyle);
          result6_text=this.game.add.text(300,900,"",fontStyle);
        break;
        case 2:
          voice.destroy(true);
          voice=this.game.add.audio("Result_1",1);
          voice.play();
          result0_text=this.game.add.text(250,300,result6,fontStyle);
          result1_text=this.game.add.text(300,375,result7,fontStyle);
          result2_text=this.game.add.text(300,450,result8,fontStyle);
          result3_text=this.game.add.text(300,575,result9,fontStyle);
          result4_text=this.game.add.text(300,650,result10,fontStyle);
          result5_text=this.game.add.text(300,725,result11,fontStyle);
          result6_text=this.game.add.text(300,850,result12,fontStyle);
        break;
        case 3:
          voice.destroy(true);
          voice=this.game.add.audio("Result_2",1);
          voice.play();
          result0_text=this.game.add.text(250,300,result13,fontStyle);
          result1_text=this.game.add.text(300,375,result14,fontStyle);
          result2_text=this.game.add.text(300,500,result15,fontStyle);
          result3_text=this.game.add.text(300,575,result16,fontStyle);
          result4_text=this.game.add.text(300,650,result17,fontStyle);
          result5_text=this.game.add.text(300,725,result18,fontStyle);
          result6_text=this.game.add.text(300,850,result19,fontStyle);
        break;
        
      }
    },
    toNext:function(){
      this.removeTexts();
      if(current_scene<3){
        prev_btn.visible=true;
        current_scene++;
          this.addResult();
          if(current_scene>=3){
            play.visible=true;
            next_btn.visible=false;  
          }
        }
  },
  toPrevious:function(){
    this.removeTexts();
      if(current_scene>1){
        next_btn.visible=true;
        current_scene--;
        this.addResult();
          if(current_scene<=1){
            prev_btn.visible=false;  
          }
        }
  },
  removeTexts(){
    result0_text.destroy(true);
    result1_text.destroy(true);
    result2_text.destroy(true);
    result3_text.destroy(true);
    result4_text.destroy(true);
    result5_text.destroy(true);
    result6_text.destroy(true);
  },
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
  
        
    //For to next scene   
   
        toViva:function()
        {
        voice.destroy();
        
        this.state.start("Viva", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  