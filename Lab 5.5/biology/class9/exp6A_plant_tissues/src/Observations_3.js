var observations_3 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations_3.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation_1",1);
    voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,80,'observation_table');
  base.scale.setTo(1.1,1.1);
  base_line1= this.game.add.sprite(220,190,'observation_table_line');
  base_line1.scale.y=.95;
  base_line2= this.game.add.sprite(580,190,'observation_table_line');
  base_line2.scale.y=.95;
  base_line3= this.game.add.sprite(970,190,'observation_table_line');
  base_line3.scale.y=.95;
  base_line4= this.game.add.sprite(1330,190,'observation_table_line');
  base_line4.scale.y=.95;
  
   base_line5= this.game.add.sprite(1760,290,'observation_table_line');
   base_line5.angle=90;
   base_line5.scale.setTo(1.1,2.035);
  // base_line6= this.game.add.sprite(1760,400,'observation_table_line');
  // base_line6.angle=90;
  // base_line6.scale.setTo(1.1,2.08);
  // base_line7= this.game.add.sprite(1760,490,'observation_table_line');
  // base_line7.angle=90;
  // base_line7.scale.setTo(1.1,2.08);
  // base_line8= this.game.add.sprite(1760,580,'observation_table_line');
  // base_line8.angle=90;
  // base_line8.scale.setTo(1.1,2.08);
  // base_line9= this.game.add.sprite(1760,670,'observation_table_line');
  // base_line9.angle=90;
  // base_line9.scale.setTo(1.1,2.08);
  // base_line10= this.game.add.sprite(1760,760,'observation_table_line');
  // base_line10.angle=90;
  // base_line10.scale.setTo(1.1,2.08);

  //base_line.scale.setTo(1,1.15);
    head1_text=this.game.add.text(140,220,"S.No",headfontStyle);
    head2_text=this.game.add.text(260,190,"Identifying feat-\nures/characters",headfontStyle);
    head3_text=this.game.add.text(680,220,"Parenchyma",headfontStyle); 
    head4_text=this.game.add.text(1040,220,"Collenchyma",headfontStyle); 
    head5_text=this.game.add.text(1430,220,"Sclerenchyma",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
  
   this.addObservation();
   
   
   
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  //
 next_btn = this.game.add.sprite(1640,830,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1640,830,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(240,830,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(240,830,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      //prev_btn.visible=false;
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 if(!observation_completed)
    play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(observation_Scene+"//observation_Scene");
          switch(observation_Scene){
            case 9:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_9",1);
                voice.play();
              
              test1_1=this.game.add.text(170,340,"1",fontStyle);
              test1_2=this.game.add.text(260,340,"Cell wall",fontStyle);
              test1_3=this.game.add.text(615,340,"Thin",fontStyle);
              test1_4=this.game.add.text(1010,340,"Unevenly thick",fontStyle);
              test1_5=this.game.add.text(1370,340,"Very thick",fontStyle);
              //test1_6=this.game.add.text(1150,460,test1_text41,fontStyle);

              test2_1=this.game.add.text(170,420,"2",fontStyle);
              test2_2=this.game.add.text(260,420,"Intercellular spaces",fontStyle);
              test2_3=this.game.add.text(615,420,"Present",fontStyle);
              test2_4=this.game.add.text(1010,420,"Very less",fontStyle);
              test2_5=this.game.add.text(1370,420,"Absent",fontStyle);
              
              test3_1=this.game.add.text(170,500,"3",fontStyle);
              test3_2=this.game.add.text(260,500,"Protoplasm",fontStyle);
              test3_3=this.game.add.text(620,500,"Present",fontStyle);
              test3_4=this.game.add.text(1010,500,"Present",fontStyle);
              test3_5=this.game.add.text(1370,500,"Very little or absent",fontStyle);

              test4_1=this.game.add.text(170,580,"4",fontStyle);
              test4_2=this.game.add.text(260,580,"Nucleus",fontStyle);
              test4_3=this.game.add.text(615,580,"Present",fontStyle);
              test4_4=this.game.add.text(1010,580,"Present",fontStyle);
              test4_5=this.game.add.text(1370,580,"Absent",fontStyle);

              test5_1=this.game.add.text(170,660,"5",fontStyle);
              test5_2=this.game.add.text(260,660,"Cytoplasm",fontStyle);
              test5_3=this.game.add.text(615,660,"Peripheral cytoplasm",fontStyle);
              test5_4=this.game.add.text(1010,660,"Present",fontStyle);
              test5_5=this.game.add.text(1370,660,"Little or no cytoplasm",fontStyle);

              test6_1=this.game.add.text(170,740,"6",fontStyle);
              test6_2=this.game.add.text(260,740,"Lumen/cavity",fontStyle);
              test6_3=this.game.add.text(615,740,"Central vacuole lies \nin the centre",fontStyle);
              test6_4=this.game.add.text(1010,740,"Present",fontStyle);
              test6_5=this.game.add.text(1370,740,"Very narrow",fontStyle);
              ///////////////////////////////////////////
              
              break;
              case 10:
                voice.destroy(true);
                voice=this.game.add.audio("Observation_10",1);
                  voice.play();
                  

               test1_1=this.game.add.text(170,340,"7",fontStyle);
              test1_2=this.game.add.text(260,340,"Cell type",fontStyle);
              test1_3=this.game.add.text(615,340,"Living",fontStyle);
              test1_4=this.game.add.text(1010,340,"Living",fontStyle);
              test1_5=this.game.add.text(1370,340,"Dead",fontStyle);
              //test1_6=this.game.add.text(1150,460,test1_text41,fontStyle);

              test2_1=this.game.add.text(170,420,"8",fontStyle);
              test2_2=this.game.add.text(260,420,"Cell shape",fontStyle);
              test2_3=this.game.add.text(615,420,"Isodiametric",fontStyle);
              test2_4=this.game.add.text(1010,420,"Spherical, oval or \npolygonal.",fontStyle);
              test2_5=this.game.add.text(1370,420,"Elongated, oval, \npolygonal or \ndifferent shapes.",fontStyle);
              
              test3_1=this.game.add.text(170,600,"9",fontStyle);
              test3_2=this.game.add.text(260,600,"Function",fontStyle);
              test3_3=this.game.add.text(620,600,"Storage, synthesis \nof food and help in \nfloating of aquatic \nplants.",fontStyle);
              test3_4=this.game.add.text(1010,600,"Provides flexibility \nand mechanical \nsupport to the \naerial parts of \nplants and allows \nthem to bend.",fontStyle);
              test3_5=this.game.add.text(1370,600,"Give strength to the \nparts of the plants \nwhere occur.",fontStyle);

              // test4_1=this.game.add.text(170,580,"10",fontStyle);
              // test4_2=this.game.add.text(260,580,"Occurrence",fontStyle);
              // test4_3=this.game.add.text(615,580,"They occur in the soft parts of the plant like stem, roots, flowers, fruits and leaves, etc.",fontStyle);
              // test4_4=this.game.add.text(1010,580,"Found below the epidermis in dicots",fontStyle);
              // test4_5=this.game.add.text(1370,580,"They occur in the hard parts of the plant",fontStyle);
                
              break;  
            case 11:
              voice.destroy(true);
              voice=this.game.add.audio("Observation_11",1);
                voice.play();
               
              
              test1_1=this.game.add.text(170,340,"10",fontStyle);
              test1_2=this.game.add.text(260,340,"Occurrence",fontStyle);
              test1_3=this.game.add.text(615,340,"They occur in the \nsoft parts of the \nplant like stem, \nroots, flowers, fruits \nand leaves, etc.",fontStyle);
              test1_4=this.game.add.text(1010,340,"Found below the \nepidermis in dicots.",fontStyle);
              test1_5=this.game.add.text(1370,340,"They occur in the \nhard parts of the \nplant.",fontStyle);
              
            break;
            
          }
        },
        toNext:function(){
          this.removeTexts();
          if(observation_Scene<11){
            prev_btn.visible=true;
            observation_Scene++;
              this.addObservation();
              if(observation_Scene>=11){
                observation_completed=true;
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      
      toPrev:function(){
        this.removeTexts();
          observation_Scene--;
          if(observation_Scene<=8){
            observation_Scene=8;
            voice.destroy(true);
            this.state.start("Observations_2", true, false, ip);
          }else{
            next_btn.visible=true;
            this.addObservation();
          }
      },
      removeTexts(){
        test1_1.destroy(true);
        test1_2.destroy(true);
        test1_3.destroy(true);
        test1_4.destroy(true);
        test1_5.destroy(true);
        
      if(observation_Scene<=10){
        test2_1.destroy(true);
        test2_2.destroy(true);
        test2_3.destroy(true);
        test2_4.destroy(true);
        test2_5.destroy(true);

        test3_1.destroy(true);
        test3_2.destroy(true);
        test3_3.destroy(true);
        test3_4.destroy(true);
        test3_5.destroy(true);
      }

        if(observation_Scene==9){
          test4_1.destroy(true);
          test4_2.destroy(true);
          test4_3.destroy(true);
          test4_4.destroy(true);
          test4_5.destroy(true);

          test5_1.destroy(true);
          test5_2.destroy(true);
          test5_3.destroy(true);
          test5_4.destroy(true);
          test5_5.destroy(true);
          
          test6_1.destroy(true);
          test6_2.destroy(true);
          test6_3.destroy(true);
          test6_4.destroy(true);
          test6_5.destroy(true);
        }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  