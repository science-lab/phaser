var procedure_1 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  var current_scene;
  var previous_scene;
  //bools
  var muted;
  
  //ip address
  var ip;
  
  }
  
  procedure_1.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("Procedure_1",1);
 voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/
  //procedure_audio1=this.game.add.audio('procedure_voice1',1);
  //procedure_audio2=this.game.add.audio('procedure_voice2',1);

 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}
  current_scene=1;
  previous_scene=0;
  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_text=this.game.add.text(250,200,"Procedure:",headfontStyle);
  //exp_Name="decomposition_reaction";
  //console.log(exp_Name+"......");
      procedure_step_1="1. Focus the prepared slides of parenchyma (slide 1), collenchyma (slide 2), and \n    sclerenchyma (slide 3) tissues under the low power of a compound \n    microscope (10X magnification).";
      procedure_step_2="2. Observe these slides and study the important identifying features under two \n    different microscopes.";
      procedure_step_3="3. Draw the neat diagrams from these slides and label them correctly.";
      procedure_step_4="4. Note down the important features and list the differences between cells of \n    parenchyma and sclerenchyma tissues.";
      procedure_step_5="";
      procedure_step_6="";
      procedure_step_7="";
      procedure_step_8="";
      procedure_step_text_1=this.game.add.text(300,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(300,490,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(300,630,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(300,710,procedure_step_4,fontStyle);
      //procedure_step_text_5=this.game.add.text(300,820,procedure_step_5,fontStyle);
      //procedure_step_text_6=this.game.add.text(300,880,procedure_step_6,fontStyle);
      
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  // next_btn = this.game.add.sprite(1620,890,'components','next_disabled.png');
  // next_btn.scale.setTo(.7,.7);
  // next_btn = this.game.add.sprite(1620,890,'components','next_pressed.png');
  // next_btn.scale.setTo(.7,.7);
  // next_btn.inputEnabled = true;
  // //next_btn.input.priorityID = 3;
  // next_btn.input.useHandCursor = true;
  // next_btn.events.onInputDown.add(this.toNext, this);
 
  // prev_btn = this.game.add.sprite(300,890,'components','next_disabled.png');
  // prev_btn.scale.setTo(-.7,.7);
  // prev_btn = this.game.add.sprite(300,890,'components','next_pressed.png');
  // prev_btn.scale.setTo(-.7,.7);
  // prev_btn.inputEnabled = true;
  // prev_btn.visible=false;
  // //next_btn.input.priorityID = 3;
  // prev_btn.input.useHandCursor = true;
  // prev_btn.events.onInputDown.add(this.toPrevious, this);
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment, this);
 //play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
    
    // For Full screen checking.
    toNext:function(){
      previous_scene=current_scene;
        current_scene++;
          this.addProcedure();
          // prev_btn.visible=true;
          // next_btn.visible=false;
          // play.visible=true;
        },
        toPrevious:function(){
          previous_scene=current_scene;
          current_scene--;
          this.addProcedure();
          // next_btn.visible=true;
          // prev_btn.visible=false;
        },
        addProcedure:function(){
          switch(current_scene){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Procedure_1",1);
              voice.play();
              procedure_step_text_1.destroy(true);
              procedure_step_text_2.destroy(true);
              procedure_step_text_3.destroy(true);
              //img_1.destroy(true);
              procedure_step_text_0=this.game.add.text(300,300,procedure_step_0,fontStyle);
              procedure_step_text_1=this.game.add.text(300,380,procedure_step_1,fontStyle);
              procedure_step_text_2=this.game.add.text(300,460,procedure_step_2,fontStyle);
              procedure_step_text_3=this.game.add.text(300,600,procedure_step_3,fontStyle);
              procedure_step_text_4=this.game.add.text(300,680,procedure_step_4,fontStyle);
              procedure_step_text_5=this.game.add.text(300,820,procedure_step_5,fontStyle);

              prev_btn.visible=false;
              next_btn.visible=true;
              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Procedure_2",1);
              voice.play();
              procedure_step_text_0.destroy(true);
              procedure_step_text_1.destroy(true);
              procedure_step_text_2.destroy(true);
              procedure_step_text_3.destroy(true);
              procedure_step_text_4.destroy(true);
              procedure_step_text_5.destroy(true);
              procedure_step_text_1=this.game.add.text(300,300,procedure_step_6,fontStyle);
              procedure_step_text_2=this.game.add.text(300,380,procedure_step_7,fontStyle);
              procedure_step_text_3=this.game.add.text(300,460,procedure_step_8,fontStyle);

              
              prev_btn.visible=true;
              next_btn.visible=false;
              play.visible=true;
              break;  
            case 3:
              voice.destroy(true);
              voice=this.game.add.audio("Procedure_3",1);
              voice.play();
              procedure_step_text_1.destroy(true);
              procedure_step_text_2.destroy(true);
              procedure_step_text_3.destroy(true);
              
              procedure_step_text_1=this.game.add.text(300,300,procedure_step_7,fontStyle);
              procedure_step_text_2=this.game.add.text(300,380,procedure_step_8,fontStyle);
              procedure_step_text_3=this.game.add.text(300,500,procedure_step_9,fontStyle);
              next_btn.visible=false;
              play.visible=true;
              break;
            
          }
        },
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
  
    //For to next scene   
   
        toExperiment:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
          //procedure_audio2.destroy();
          this.state.start("Experiment_1", true, false);//Experiment_3
           
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    //procedure_audio1.destroy();
    //procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    //procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //procedure_audio1.destroy();
  //procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  