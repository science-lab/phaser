var theory_1 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  theory_1.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }
//console.log(theory_completed+"//theory_completed");
 muted = false;
 
  //voice=this.game.add.audio("Observation_1",1);
    //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,140,'dialogue_box');
  base.scale.setTo(1.05,1.00);
  //procedure_text=this.game.add.text(250,200,"Theory:",headfontStyle);
  //base.scale.setTo(1.13,1.17);
  this.drawVLine(280,182,280,1005);
  this.drawVLine(770,182,770,1005);
  this.drawVLine(1270,182,1270,1005);
  this.drawVLine(150,280,1770,280);
  
  // base_line1= this.game.add.sprite(200,173,'observation_table_line');
  // base_line1.tint="0xE5E8E8";
  // console.log(base_line1.tint+"...");
  //base_line2= this.game.add.sprite(450,173,'observation_table_line');
  //base_line3= this.game.add.sprite(1030,173,'observation_table_line');
  //base_line4= this.game.add.sprite(1110,173,'observation_table_line');
  
   //base_line5= this.game.add.sprite(1760,230,'observation_table_line');
   //base_line5.angle=90;
   //base_line5.scale.setTo(1.1,2.08);
  // base_line6= this.game.add.sprite(1760,400,'observation_table_line');
  // base_line6.angle=90;
  // base_line6.scale.setTo(1.1,2.08);
  // base_line7= this.game.add.sprite(1760,490,'observation_table_line');
  // base_line7.angle=90;
  // base_line7.scale.setTo(1.1,2.08);
  // base_line8= this.game.add.sprite(1760,580,'observation_table_line');
  // base_line8.angle=90;
  // base_line8.scale.setTo(1.1,2.08);
  // base_line9= this.game.add.sprite(1760,670,'observation_table_line');
  // base_line9.angle=90;
  // base_line9.scale.setTo(1.1,2.08);
  // base_line10= this.game.add.sprite(1760,760,'observation_table_line');
  // base_line10.angle=90;
  // base_line10.scale.setTo(1.1,2.08);

  //base_line.scale.setTo(1,1.15);
   head1_text=this.game.add.text(165,200,"S.No",headfontStyle);
   head2_text=this.game.add.text(320,200,"Parenchyma Tissue",headfontStyle);
   head3_text=this.game.add.text(820,200,"Collenchyma Tissue",headfontStyle); 
   head4_text=this.game.add.text(1310,200,"Sclerenchyma Tissue",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
    test1_text1="1";
   test1_text2="These are thin walled cells.";
   test1_text3="These are thick walled cells.";
   test1_text4="These are thick walled cells.";

   test2_text1="2";
   test2_text2="These are loosely arranged \ncells.";
   test2_text3="These are compactly \narranged cells.";
   test2_text4="These are compactly \narranged cells.";

   test3_text1="3";
   test3_text2="Intercellular spaces are \npresent.";
   test3_text3="Intercellular spaces are \nvery less.";
   test3_text4="No intercellular spaces.";

   test4_text1="4";
   test4_text2="These are living cells.";
   test4_text3="These are living cells.";
   test4_text4="These are dead cells.";
   
   test5_text1="5";
   test5_text2="Main function is storage \nof food, water, etc.";
   test5_text3="Main function is to provide \nflexibility and mechanical \nsupport.";
   test5_text4="Main function is to give \nmechanical strength \nto plant parts.";
   
   

   test1_1=this.game.add.text(200,300,test1_text1,fontStyle);
   test1_2=this.game.add.text(290,300,test1_text2,fontStyle);
   test1_3=this.game.add.text(780,300,test1_text3,fontStyle);
   test1_4=this.game.add.text(1280,300,test1_text4,fontStyle);

   test2_1=this.game.add.text(200,400,test2_text1,fontStyle);
   test2_2=this.game.add.text(290,400,test2_text2,fontStyle);
   test2_3=this.game.add.text(780,400,test2_text3,fontStyle);
   test2_4=this.game.add.text(1280,400,test2_text4,fontStyle);
  
   test3_1=this.game.add.text(200,550,test3_text1,fontStyle);
   test3_2=this.game.add.text(290,550,test3_text2,fontStyle);
   test3_3=this.game.add.text(780,550,test3_text3,fontStyle);
   test3_4=this.game.add.text(1280,550,test3_text4,fontStyle);

   test4_1=this.game.add.text(200,700,test4_text1,fontStyle);
   test4_2=this.game.add.text(290,700,test4_text2,fontStyle);
   test4_3=this.game.add.text(780,700,test4_text3,fontStyle);
   test4_4=this.game.add.text(1280,700,test4_text4,fontStyle);

   test5_1=this.game.add.text(200,800,test5_text1,fontStyle);
   test5_2=this.game.add.text(290,800,test5_text2,fontStyle);
   test5_3=this.game.add.text(780,800,test5_text3,fontStyle);
   test5_4=this.game.add.text(1280,800,test5_text4,fontStyle); 

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  //this.addObservation();
 next_btn = this.game.add.sprite(1660,890,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1660,890,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(260,890,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(260,890,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      //prev_btn.visible=false;
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 if(!theory_completed)
    play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
drawVLine:function(xp,yp,xp1,yp1)
  {
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0xffffff,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
      
       // lineGroup.add(line1);
        
  },
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              //voice.destroy(true);
              //voice=this.game.add.audio("Observation_1",1);
                //voice.play();
               test1_1.destroy(true);
               test1_2.destroy(true);
               test1_3.destroy(true);
               test1_31.destroy(true);
               test1_4.destroy(true);
               test1_41.destroy(true);
              
               test2_1.destroy(true);
               test2_2.destroy(true);
               test2_3.destroy(true);
               test2_31.destroy(true);
               test2_4.destroy(true);
               test2_41.destroy(true);

              test1_1=this.game.add.text(145,280,test1_text1,fontStyle);
              test1_2=this.game.add.text(240,280,test1_text2,fontStyle);
              test1_3=this.game.add.text(490,280,test1_text3,fontStyle);
              test1_31=this.game.add.text(490,460,test1_text31,fontStyle);
              test1_4=this.game.add.text(1150,280,test1_text4,fontStyle);
              test1_41=this.game.add.text(1150,460,test1_text41,fontStyle);
              
              test2_1=this.game.add.text(145,700,test2_text1,fontStyle);
              test2_2=this.game.add.text(240,700,test2_text2,fontStyle);
              test2_3=this.game.add.text(490,700,test2_text3,fontStyle);
              test2_31=this.game.add.text(490,840,test2_text31,fontStyle);
              test2_4=this.game.add.text(1150,700,test2_text4,fontStyle);
              test2_41=this.game.add.text(1150,840,test2_text41,fontStyle);
              ///////////////////////////////////////////
              
              break;
            case 2:
              //voice.destroy(true);
              //voice=this.game.add.audio("Observation_2",1);
                //voice.play();
                test1_1.destroy(true);
               test1_2.destroy(true);
               test1_3.destroy(true);
               test1_31.destroy(true);
               test1_4.destroy(true);
               test1_41.destroy(true);
              
               test2_1.destroy(true);
               test2_2.destroy(true);
               test2_3.destroy(true);
               test2_31.destroy(true);
               test2_4.destroy(true);
               test2_41.destroy(true);

              test1_1=this.game.add.text(145,280,test3_text1,fontStyle);
              test1_2=this.game.add.text(240,280,test3_text2,fontStyle);
              test1_3=this.game.add.text(490,280,test3_text3,fontStyle);
              test1_31=this.game.add.text(490,460,test3_text31,fontStyle);
              test1_4=this.game.add.text(1150,280,test3_text4,fontStyle);
              test1_41=this.game.add.text(1150,460,test3_text41,fontStyle);
              
              test2_1=this.game.add.text(145,670,test4_text1,fontStyle);
              test2_2=this.game.add.text(240,670,test4_text2,fontStyle);
              test2_3=this.game.add.text(490,650,test4_text3,fontStyle);
              test2_31=this.game.add.text(490,830,test4_text31,fontStyle);
              test2_4=this.game.add.text(1150,650,test4_text4,fontStyle);
              test2_41=this.game.add.text(1150,830,test4_text41,fontStyle);
            break;
            
          }
        },
        toNext:function(){
          theory_Scene=4;
          this.state.start("Theory_2", true, false);
          // if(pageCount<2){
          //   prev_btn.visible=true;
          //   pageCount++;
          //     this.addObservation();
          //     if(pageCount>=2){
          //       play.visible=true;
          //       next_btn.visible=false;  
          //     }
          //   }
      },
      toPrev:function(){
        theory_Scene=3;
        this.state.start("Theory", true, false);
          // if(pageCount>1){
          //   next_btn.visible=true;
          //   pageCount--;
          //   this.addObservation();
          //     if(pageCount<=1){
          //       prev_btn.visible=false;  
          //     }
          //   }
      },
    //For to next scene   
   
        toResult:function()
        {
        //voice.destroy();
        //voice2.destroy();
        this.state.start("Lab_Precautions", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    //voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  //voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  