var theory_2 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var current_scene;
  var previous_scene;
  }
  
  theory_2.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  //muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  subfontStyle={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,140,'dialogue_box');
  base.scale.setTo(1.05,1.00);
  procedure_text=this.game.add.text(210,200,"Theory:",headfontStyle);
 // exp_Name="decomposition_reaction";
 theory_Scene=4;
 previous_scene=0;
      procedure_step_1="Parenchyma is a type of simple permanent tissue. ";
      procedure_step_2="It is made up of thin walled, living cells which are usually loosely arranged and have \nintercellular spaces.";
      procedure_step_3="These are found in the soft parts of the plant like roots, stem, leaves, flowers and \nfruits. They are called Chlorenchyma when contain chloroplast and prepare food by \nphotosynthesis.";
      procedure_step_4="In aquatic plants they have large air cavities and are called Aerenchyma. It helps \nthese plants to float in water.";
      procedure_step_5="Parenchyma also stores food in phloem parenchyma and stores water in xylem \nparenchyma.";
      
      procedure_step_6="Collenchyma is a type of simple permanent tissue.";
      procedure_step_7="It has very less intercellular spaces.";
      procedure_step_8="Collenchyma is found below epidermis in petiole, leaves and stems in dicot.";
      procedure_step_9="It provides flexibility and mechanical support to the aerial parts of plants and allows \nthem to bend.";
      procedure_step_10="Cells perform photosynthesis when chloroplasts is present.";
        
      procedure_step_11="Sclerenchyma is also a simple permanent tissue.";
      procedure_step_12="It is made up of hard thick walled, dead cells. These cells have no or little \nprotoplasm.";
      procedure_step_13="Their walls are highly thickened due to chemical lignin.";
      procedure_step_14="Sclerenchyma is present in hard tissues, in the veins of leaves, in the hard covering \nof seeds and nuts.";
      procedure_step_15="They give mechanical strength to the plant and its parts.";
      
      procedure_step_text_1=this.game.add.text(260,300,procedure_step_1,fontStyle);
      procedure_step_text_2=this.game.add.text(260,375,procedure_step_2,fontStyle);
      procedure_step_text_3=this.game.add.text(260,505,procedure_step_3,fontStyle);
      procedure_step_text_4=this.game.add.text(260,685,procedure_step_4,fontStyle);
      procedure_step_text_5=this.game.add.text(260,820,procedure_step_5,fontStyle);
      //procedure_step_text_6=this.game.add.text(260,840,procedure_step_6,fontStyle);
    
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  next_btn = this.game.add.sprite(1660,890,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1660,890,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 
  prev_btn = this.game.add.sprite(260,890,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(260,890,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  //prev_btn.visible=false;
  //next_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrevious, this);
 
 
  
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toLab_precautions, this);
 if(!theory_completed)
  play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);4
              }  
        },
        //to go to next popup
        toNext:function(){
          previous_scene=theory_Scene;
          theory_Scene++;
        prev_btn.visible=true;
        if(theory_Scene>=6){
          next_btn.visible=false;
          play.visible=true;
          theory_completed=true;
        }
          this.showTheory();
          
        },
        toPrevious:function(){
          previous_scene=theory_Scene;
          theory_Scene--;
        next_btn.visible=true;
        if(theory_Scene<=3){
          theory_Scene=3;
          this.state.start("Theory_1", true, false);
          //prev_btn.visible=false;
        }
          this.showTheory();
        },
        showTheory:function(){
          console.log(theory_Scene);
          switch(theory_Scene){
            case 4:
                procedure_step_text_1.destroy(true);
                procedure_step_text_2.destroy(true);
                procedure_step_text_3.destroy(true);
                procedure_step_text_4.destroy(true);
                procedure_step_text_5.destroy(true);
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_1,fontStyle);
              procedure_step_text_2=this.game.add.text(260,375,procedure_step_2,fontStyle);
              procedure_step_text_3=this.game.add.text(260,505,procedure_step_3,fontStyle);
              procedure_step_text_4=this.game.add.text(260,685,procedure_step_4,fontStyle);
              procedure_step_text_5=this.game.add.text(260,820,procedure_step_5,fontStyle);
              
              break;
            case 5:
              
              procedure_step_text_1.destroy(true);
              procedure_step_text_2.destroy(true);
              procedure_step_text_3.destroy(true);
              procedure_step_text_4.destroy(true);
              procedure_step_text_5.destroy(true);
              
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_6,fontStyle);
              procedure_step_text_2=this.game.add.text(260,380,procedure_step_7,fontStyle);
              procedure_step_text_3=this.game.add.text(260,460,procedure_step_8,fontStyle);
              procedure_step_text_4=this.game.add.text(260,540,procedure_step_9,fontStyle);
              procedure_step_text_5=this.game.add.text(260,680,procedure_step_10,fontStyle);
              
              
          break;
          case 6:
            procedure_step_text_1.destroy(true);
              procedure_step_text_2.destroy(true);
              procedure_step_text_3.destroy(true);
              procedure_step_text_4.destroy(true);
              procedure_step_text_5.destroy(true);
              
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_11,fontStyle);
              procedure_step_text_2=this.game.add.text(260,380,procedure_step_12,fontStyle);
              procedure_step_text_3=this.game.add.text(260,520,procedure_step_13,fontStyle);
              procedure_step_text_4=this.game.add.text(260,600,procedure_step_14,fontStyle);
              procedure_step_text_5=this.game.add.text(260,740,procedure_step_15,fontStyle);
            
          break;
          

          }
        },

    //For to next scene   
   
    toLab_precautions:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
         // procedure_audio2.destroy();
         this.state.start("Lab_Precautions", true, false);
         
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    //procedure_audio1.destroy();
    //procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    //procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //procedure_audio1.destroy();
  //procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  