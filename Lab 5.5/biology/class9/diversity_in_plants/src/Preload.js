var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var theory_completed;
  var theory_Scene;
  var observation_completed;
  var observation_Scene;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
//    this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

         this.game.load.image('zoom_out','assets/zoom_out.png');
        this.game.load.image('zoom_in','assets/zoom_in.png');

        
        
        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Animal_tissues//////////////////////////////////////////////////////////
      this.game.load.image('flow_chart','assets/flow_chart.png');

      this.game.load.image('bg_black','assets/Bg_black.png');
      this.game.load.image('bg_top','assets/Bg_top.png'); 
      this.game.load.atlasJSONHash('arrow', 'assets/plant/arrow.png', 'assets/plant/arrow.json');
      this.game.load.image('fixed_arrow','assets/arrow.png');

      this.game.load.image('microscope','assets/plant/spyrogyra/microscope.png');
      this.game.load.atlasJSONHash('eyePiece_rotating_sprite','assets/plant/spyrogyra/eyePiece_rotating_sprite.png','assets/plant/spyrogyra/eyePiece_rotating_sprite.json');
      this.game.load.atlasJSONHash('slide_views','assets/plant/spyrogyra/slide_views.png','assets/plant/spyrogyra/slide_views.json');
      this.game.load.image('microscope_holder_left','assets/plant/spyrogyra/mocroscope_left_lock.png');
      this.game.load.image('microscope_holder_right','assets/plant/spyrogyra/mocroscope_right_lock.png');
      this.game.load.image('microscope_top_view','assets/plant/spyrogyra/Mocroscope_top_view.png');
      this.game.load.image('spyrogyra','assets/plant/spyrogyra/spyrogyra.png');

      ////////////////////////////diversity in plants//////////////

this.game.load.image('agaricus_specimen','assets/plant/agaricus_specimen.png');
this.game.load.atlasJSONHash('agaricus', 'assets/plant/agaricus_sprite.png', 'assets/plant/agaricus_sprite.json');

this.game.load.image('moss_specimen','assets/plant/moss_specimen.png');
this.game.load.atlasJSONHash('moss', 'assets/plant/moss.png', 'assets/plant/moss.json');

//this.game.load.image('fern_specimen','assets/plant/fern_specimen.png');
this.game.load.atlasJSONHash('fern', 'assets/plant/fern.png', 'assets/plant/fern.json');

this.game.load.image('female_cones', 'assets/plant/female_cones.png');
this.game.load.atlasJSONHash('pinus_sprite', 'assets/plant/pinus_sprite.png', 'assets/plant/pinus_sprite.json');
/////////Spyrogyra////////
this.game.load.image('bg','assets/plant/Bg.png');

this.game.load.image('bottle', 'assets/plant/bottle.png');
this.game.load.image('mark', 'assets/plant/mark.png');
this.game.load.image('arrow_1','assets/arrow.png');

this.game.load.atlasJSONHash('brassica_sprite', 'assets/plant/brassica_sprite.png', 'assets/plant/brassica_sprite.json');


      ///////////////materials//////////////////////
      this.game.load.image('m_microscope', 'assets/plant/spyrogyra/m_microscope.png');
      this.game.load.image('Permenat_Slides', 'assets/plant/spyrogyra/Permenat_Slides.png');



      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Observation_3','assets/audio/Observation_3.mp3');
      this.game.load.audio('Observation_4','assets/audio/Observation_4.mp3');
      this.game.load.audio('Observation1_1','assets/audio/Observation1_1.mp3');
      this.game.load.audio('Observation1_2','assets/audio/Observation1_2.mp3');
      this.game.load.audio('Observation1_3','assets/audio/Observation1_3.mp3');
      this.game.load.audio('Observation1_4','assets/audio/Observation1_4.mp3');
      this.game.load.audio('Observation2_1','assets/audio/Observation2_1.mp3');
      this.game.load.audio('Observation2_2','assets/audio/Observation2_2.mp3');
      this.game.load.audio('Observation2_3','assets/audio/Observation2_3.mp3');
      this.game.load.audio('Observation2_4','assets/audio/Observation2_4.mp3');
      this.game.load.audio('Observation2_5','assets/audio/Observation2_5.mp3');
      this.game.load.audio('Observation3_1','assets/audio/Observation3_1.mp3');
      this.game.load.audio('Observation3_2','assets/audio/Observation3_2.mp3');
      this.game.load.audio('Observation3_3','assets/audio/Observation3_3.mp3');
      this.game.load.audio('Observation3_4','assets/audio/Observation3_4.mp3');
      this.game.load.audio('Observation4_1','assets/audio/Observation4_1.mp3');
      this.game.load.audio('Observation4_2','assets/audio/Observation4_2.mp3');
      this.game.load.audio('Observation4_3','assets/audio/Observation4_3.mp3');
      this.game.load.audio('Observation4_4','assets/audio/Observation4_4.mp3');
      this.game.load.audio('Observation4_5','assets/audio/Observation4_5.mp3');
      this.game.load.audio('Observation4_6','assets/audio/Observation4_6.mp3');
      this.game.load.audio('Observation4_7','assets/audio/Observation4_7.mp3');
      this.game.load.audio('Observation4_8','assets/audio/Observation4_8.mp3');
      this.game.load.audio('Observation5_1','assets/audio/Observation5_1.mp3');
      this.game.load.audio('Observation5_2','assets/audio/Observation5_2.mp3');
      this.game.load.audio('Observation5_3','assets/audio/Observation5_3.mp3');
      this.game.load.audio('Observation5_4','assets/audio/Observation5_4.mp3');
      
      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('Result_1','assets/audio/Result_1.mp3');
      this.game.load.audio('Result_2','assets/audio/Result_2.mp3');
      this.game.load.audio('Result_3','assets/audio/Result_3.mp3');
      this.game.load.audio('Result_4','assets/audio/Result_4.mp3');
      this.game.load.audio('Result_5','assets/audio/Result_5.mp3');

      this.game.load.audio('step1_0','assets/audio/step1_0.mp3');
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');

      this.game.load.audio('step2_0','assets/audio/step2_0.mp3');
      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step2_5','assets/audio/step2_5.mp3');
      this.game.load.audio('step2_6','assets/audio/step2_6.mp3');
      this.game.load.audio('step2_7','assets/audio/step2_7.mp3'); 
      this.game.load.audio('step2_8','assets/audio/step2_8.mp3');
      this.game.load.audio('step2_9','assets/audio/step2_9.mp3');
      this.game.load.audio('step2_10','assets/audio/step2_10.mp3');
      this.game.load.audio('step2_11','assets/audio/step2_11.mp3');
      this.game.load.audio('step2_12','assets/audio/step2_12.mp3');
      
      
      this.game.load.audio('step3_0','assets/audio/step3_0.mp3');
      this.game.load.audio('step3_1','assets/audio/step3_1.mp3');
      this.game.load.audio('step3_2','assets/audio/step3_2.mp3');
      this.game.load.audio('step3_3','assets/audio/step3_3.mp3');
      this.game.load.audio('step3_4','assets/audio/step3_4.mp3');
      this.game.load.audio('step3_5','assets/audio/step3_5.mp3');
      this.game.load.audio('step3_6','assets/audio/step3_6.mp3');
      this.game.load.audio('step3_7','assets/audio/step3_7.mp3');
      this.game.load.audio('step3_8','assets/audio/step3_8.mp3');
      this.game.load.audio('step3_9','assets/audio/step3_9.mp3');
      this.game.load.audio('step3_10','assets/audio/step3_10.mp3');
      this.game.load.audio('step3_11','assets/audio/step3_11.mp3');
      this.game.load.audio('step3_12','assets/audio/step3_12.mp3');
      this.game.load.audio('step3_13','assets/audio/step3_13.mp3');
      this.game.load.audio('step3_14','assets/audio/step3_14.mp3');
      this.game.load.audio('step3_15','assets/audio/step3_15.mp3'); 

      this.game.load.audio('step5_0','assets/audio/step5_0.mp3');
      this.game.load.audio('step5_1','assets/audio/step5_1.mp3');
      this.game.load.audio('step5_2','assets/audio/step5_2.mp3');
      this.game.load.audio('step5_3','assets/audio/step5_3.mp3');
      this.game.load.audio('step5_4','assets/audio/step5_4.mp3');
      this.game.load.audio('step5_5','assets/audio/step5_5.mp3');
      this.game.load.audio('step5_6','assets/audio/step5_6.mp3');
      this.game.load.audio('step5_7','assets/audio/step5_7.mp3'); 
      // this.game.load.audio('step5_8','assets/audio/step5_8.mp3');
      // this.game.load.audio('step5_9','assets/audio/step5_9.mp3');
      // this.game.load.audio('step5_10','assets/audio/step5_10.mp3');
      // this.game.load.audio('step5_11','assets/audio/step5_11.mp3');
      // this.game.load.audio('step5_12','assets/audio/step5_12.mp3');
      

      this.game.load.audio('step4_0','assets/audio/step4_0.mp3');
      this.game.load.audio('step4_1','assets/audio/step4_1.mp3');
      this.game.load.audio('step4_2','assets/audio/step4_2.mp3');
      this.game.load.audio('step4_3','assets/audio/step4_3.mp3');
      this.game.load.audio('step4_4','assets/audio/step4_4.mp3');
      this.game.load.audio('step4_5','assets/audio/step4_5.mp3');
      this.game.load.audio('step4_6','assets/audio/step4_6.mp3');
      this.game.load.audio('step4_7','assets/audio/step4_7.mp3');
      this.game.load.audio('step4_8','assets/audio/step4_8.mp3');
      this.game.load.audio('step4_9','assets/audio/step4_9.mp3');
      this.game.load.audio('step4_10','assets/audio/step4_10.mp3');
      this.game.load.audio('step4_11','assets/audio/step4_11.mp3');
      this.game.load.audio('step4_12','assets/audio/step4_12.mp3');
      this.game.load.audio('step4_13','assets/audio/step4_13.mp3');
      this.game.load.audio('step4_14','assets/audio/step4_14.mp3');
      
      this.game.load.audio('step5_0','assets/audio/step5_0.mp3');
      this.game.load.audio('step5_1','assets/audio/step5_1.mp3');
      this.game.load.audio('step5_2','assets/audio/step5_2.mp3');
      this.game.load.audio('step5_3','assets/audio/step5_3.mp3');
      this.game.load.audio('step5_4','assets/audio/step5_4.mp3');
      this.game.load.audio('step5_5','assets/audio/step5_5.mp3');
      this.game.load.audio('step5_6','assets/audio/step5_6.mp3');
      this.game.load.audio('step5_7','assets/audio/step5_7.mp3');

      this.game.load.audio('step6_0','assets/audio/step6_0.mp3');
      this.game.load.audio('step6_1','assets/audio/step6_1.mp3');
      this.game.load.audio('step6_2','assets/audio/step6_2.mp3');
      this.game.load.audio('step6_3','assets/audio/step6_3.mp3');
      this.game.load.audio('step6_4','assets/audio/step6_4.mp3');
      this.game.load.audio('step6_5','assets/audio/step6_5.mp3');
      this.game.load.audio('step6_6','assets/audio/step6_6.mp3');
      this.game.load.audio('step6_7','assets/audio/step6_7.mp3'); 
      this.game.load.audio('step6_8','assets/audio/step6_8.mp3');
      this.game.load.audio('step6_9','assets/audio/step6_9.mp3');
      this.game.load.audio('step6_10','assets/audio/step6_10.mp3');
      this.game.load.audio('step6_11','assets/audio/step6_11.mp3');
      this.game.load.audio('step6_12','assets/audio/step6_12.mp3');
      this.game.load.audio('step6_13','assets/audio/step6_13.mp3');
      this.game.load.audio('step6_14','assets/audio/step6_14.mp3');
      this.game.load.audio('step6_15','assets/audio/step6_15.mp3');   
      this.game.load.audio('step6_16','assets/audio/step6_16.mp3');
      this.game.load.audio('step6_17','assets/audio/step6_17.mp3');
      this.game.load.audio('step6_18','assets/audio/step6_18.mp3');
      this.game.load.audio('step6_19','assets/audio/step6_19.mp3');
      this.game.load.audio('step6_20','assets/audio/step6_20.mp3');
      this.game.load.audio('step6_21','assets/audio/step6_21.mp3');
      this.game.load.audio('step6_22','assets/audio/step6_22.mp3');
      this.game.load.audio('step6_23','assets/audio/step6_23.mp3'); 
      this.game.load.audio('step6_24','assets/audio/step6_24.mp3');
      this.game.load.audio('step6_25','assets/audio/step6_25.mp3');  
      this.game.load.audio('step6_26','assets/audio/step6_26.mp3'); 
      this.game.load.audio('step6_27','assets/audio/step6_27.mp3');
      this.game.load.audio('step6_28','assets/audio/step6_28.mp3'); 
      this.game.load.audio('step6_29','assets/audio/step6_29.mp3');

      this.game.load.audio('step_end','assets/audio/step_end.mp3');
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	  hotflag=1;
      ip = location.host; 
      theory_completed=false;
      theory_Scene=1;
      observation_completed=false;
      observation_Scene=1;
      
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_2");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations_4");//hot
      //this.game.state.start("Result_2");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

