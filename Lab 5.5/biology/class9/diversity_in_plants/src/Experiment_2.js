var experiment_2 = function(game){

var inst;


var delay;
var incr;
var currentobj;

var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
var tissue;
var slideNo;
}


experiment_2.prototype = {
  	create: function(){

hotFlag=false;
tissue=null;
bg= this.game.add.sprite(0, 0,'bg');






voice=this.game.add.audio("step2_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toObservation, this);
play.visible=false;

prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
prev_btn.scale.setTo(-.7,.7);
prev_btn.inputEnabled = true;
//prev_btn.input.priorityID = 3;
prev_btn.input.useHandCursor = true;
//prev_btn.events.onInputDown.add(this.backToExp, this);
prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toObservation:function()
 {
 voice.destroy();
 
 this.state.start("Observations_1", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    white_box1 = this.game.add.graphics(0, 0);
    white_box1.lineStyle(5,0xE67E22,.6);
    white_box1.beginFill(0xffffff,1);//9B59B60xffffff
    white_box1.drawRect(800, 240, 1000,800);

    agaricusSpecimen=this.game.add.sprite(10,280, 'agaricus_specimen');
    agaricusSpecimen.scale.set(.7);

    agaricusFull=this.game.add.sprite(600,180, 'agaricus','mashroom_full.png');
    agaricusFull.alpha=1;

    agaricusPileus=this.game.add.sprite(840,310, 'agaricus','pileus.png');
    agaricusPileus.alpha=0;
    agaricusPileusLabel=this.game.add.text(1320,440,"Pileus",fontStyle_b);
    agaricusPileusLabel.visible=false;
  

    agaricusGills=this.game.add.sprite(865,485, 'agaricus','gills.png');
    agaricusGills.alpha=0;
    agaricusGillsLabel=this.game.add.text(1330,540,"Gills",fontStyle_b);
    agaricusGillsLabel.visible=false;

    agaricusAnnulus=this.game.add.sprite(1034,555, 'agaricus','annulus.png');
    agaricusAnnulus.alpha=0;
    agaricusAnnulusLabel=this.game.add.text(1150,590,"Annulus",fontStyle_b);
    agaricusAnnulusLabel.visible=false;

    agaricusStipe=this.game.add.sprite(874,645, 'agaricus','stipe.png');
    agaricusStipe.alpha=0;
    agaricusStipeLabel=this.game.add.text(1150,670,"Stipe (stalk)",fontStyle_b);
    agaricusStipeLabel.visible=false;

    agaricusBasidiocarp=this.game.add.sprite(1170,720,'agaricus', 'young_bacidiocarp.png');
    agaricusBasidiocarp.alpha=0;
    agaricusBasidiocarpLabel=this.game.add.text(1250,730,"Young basidiocarp",fontStyle_b);
    agaricusBasidiocarpLabel.visible=false;

    agaricusMycelium=this.game.add.sprite(886,840,'agaricus', 'mycelium.png');
    agaricusMycelium.alpha=0;
    agaricusMyceliumLabel=this.game.add.text(1330,860,"Mycelium",fontStyle_b);
    agaricusMyceliumLabel.visible=false;

   


    label_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    label_box1.beginFill(0xE67E22,1);//9B59B6
    label_box1.drawRect(180, 960, 360, 60);
    label_1=this.game.add.text(200,970,"Agaricus (Mushroom)",lablel_fontStyle);//870,930
   

    
    
    arrow=this.game.add.sprite(1330,450, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.angle=90;
    arrow.visible=false;
   // arrow.state="";

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe and study the parts of agaricus.";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

   
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
    agaricusFull.alpha=.5;
  },
  startExperiment:function(){
    arrow.visible=true;
    agaricusPileus.inputEnabled = true;
    agaricusPileus.input.useHandCursor = true;
    agaricusPileus.events.onInputDown.add(this.clickOnPileus, this);
    voice.destroy(true);
    voice=this.game.add.audio("step2_1",1);
    voice.play();
    dialog.text="Click on pileus.";
    // tween1=this.game.add.tween(microscopeGroup).to( {x:0}, 800, Phaser.Easing.Out, true);
    // tween1.onComplete.add(function () {
    //   this.game.time.events.add(Phaser.Timer.SECOND*4,this.ExplainMicroscope, this);
      
    // }.bind(this));
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
 
  clickOnPileus:function()
  {
    arrow.visible=false;
    agaricusPileus.alpha=1;
    agaricusPileusLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_2",1);
    voice.play();
    dialog.text="The umbrella like head is called pileus.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.showPileusEnd, this); 
  },
  showPileusEnd:function()
  {
    agaricusPileus.inputEnabled = false;
    agaricusPileus.input.useHandCursor = false;
    
    arrow.x=1360;
    arrow.y=550;
    arrow.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_3",1);
    voice.play();
    dialog.text="Click on gills.";
    agaricusGills.inputEnabled = true;
    agaricusGills.input.useHandCursor = true;
    agaricusGills.events.onInputDown.add(this.clickOnGills, this);
  },

  clickOnGills:function()
  {
    arrow.visible=false;
    agaricusGills.alpha=1;
    agaricusGillsLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_4",1);
    voice.play();
    dialog.y=40;
    dialog.text="On the lower side of the pileus are present a number of vertical plate\nlike structure called gills or lamellae.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.showGillsEnd, this); 
  },
  showGillsEnd:function()
  {
    agaricusGills.inputEnabled = false;
    agaricusGills.input.useHandCursor = false;
    
    arrow.x=1160;
    arrow.y=600;
    arrow.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_5",1);
    voice.play();
    dialog.text="Click on annulus.";
    dialog.y=60;
    agaricusAnnulus.inputEnabled = true;
    agaricusAnnulus.input.useHandCursor = true;
    agaricusAnnulus.events.onInputDown.add(this.clickOnAnnulus, this);

  },

  clickOnAnnulus:function()
  {
    arrow.visible=false;
    agaricusAnnulus.alpha=1;
    agaricusAnnulusLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_6",1);
    voice.play();
    dialog.y=40;
    dialog.text="Annulus, when pileus grows, velum is ruptured and a part of it remains\nattached to the stipe. This form is called annulus.";
    this.game.time.events.add(Phaser.Timer.SECOND*9,this.showAnnulusEnd, this); 
  },
  showAnnulusEnd:function()
  {
    agaricusAnnulus.inputEnabled = false;
    agaricusAnnulus.input.useHandCursor = false;
    
    arrow.x=1160;
    arrow.y=690;
    arrow.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_7",1);
    voice.play();
    dialog.text="Click on stipe.";
    dialog.y=60;
    agaricusStipe.inputEnabled = true;
    agaricusStipe.input.useHandCursor = true;
    agaricusStipe.events.onInputDown.add(this.clickOnStipe, this);

  },
  clickOnStipe:function()
  {
    arrow.visible=false;
    agaricusStipe.alpha=1;
    agaricusStipeLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_8",1);
    voice.play();
    dialog.text="A fleshy stalk on the basidiocarp is called stipe.";
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.showStipeEnd, this); 

  },
  showStipeEnd:function()
  {
    agaricusStipe.inputEnabled = false;
    agaricusStipe.input.useHandCursor = false;
    
    arrow.x=1280;
    arrow.y=750;
    arrow.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_9",1);
    voice.play();
    dialog.text="Click on basidiocarp.";
    agaricusBasidiocarp.inputEnabled = true;
    agaricusBasidiocarp.input.useHandCursor = true;
    agaricusBasidiocarp.events.onInputDown.add(this.clickOnBasidiocarp, this);
  },
  clickOnBasidiocarp:function()
  {
    arrow.visible=false;
    agaricusBasidiocarp.alpha=1;
    agaricusBasidiocarpLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_10",1);
    voice.play();
    dialog.text="Young basidiocarp, it is made up of fleshy stalk.";
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.showBasidiocarpEnd, this); 

  },
  showBasidiocarpEnd:function()
  {
    agaricusBasidiocarp.inputEnabled = false;
    agaricusBasidiocarp.input.useHandCursor = false;
    
    arrow.x=1350;
    arrow.y=890;
    arrow.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_11",1);
    voice.play();
    dialog.text="Click on mycelium.";
    agaricusMycelium.inputEnabled = true;
    agaricusMycelium.input.useHandCursor = true;
    agaricusMycelium.events.onInputDown.add(this.clickOnMycelium, this);
  },  
  clickOnMycelium:function()
  {
    arrow.visible=false;
    agaricusMycelium.alpha=1;
    agaricusMyceliumLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_12",1);
    voice.play();
    dialog.text="Mycelium, the root system of agaricus.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.nextStage, this); 
  },
  
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    // this.detectCollision();
    // interval++;
    // if(interval>300){
    //   interval=0;
    //   //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    // }
    
    
	},
  
	
	  nextStage:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step_end",1);
    voice.play();
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);
//
    this.state.start("Experiment_2",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
