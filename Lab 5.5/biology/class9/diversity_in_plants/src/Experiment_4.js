var experiment_4 = function(game){

var inst;


var delay;
var incr;
var currentobj;

var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
var tissue;
var slideNo;
}


experiment_4.prototype = {
  	create: function(){

hotFlag=false;
tissue=null;
bg= this.game.add.sprite(0, 0,'bg');






voice=this.game.add.audio("step4_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toObservation, this);
play.visible=false;

// prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
// prev_btn.scale.setTo(-.7,.7);
// prev_btn.inputEnabled = true;
//prev_btn.input.priorityID = 3;
// prev_btn.input.useHandCursor = true;
// prev_btn.events.onInputDown.add(this.backToExp, this);
// prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toObservation:function()
 {
 voice.destroy();
 
 this.state.start("Observations_3", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    white_box1 = this.game.add.graphics(0, 0);
    white_box1.lineStyle(5,0xE67E22,.6);
    white_box1.beginFill(0xffffff,1);//9B59B60xffffff
    white_box1.drawRect(800,240,1000,800);

   

    fernSpecimen=this.game.add.sprite(10,280, 'fern','Specimen.png');
    fernSpecimen.scale.set(.7);

    fernFull=this.game.add.sprite(1050,640, 'fern','Fern_full.png');
    fernFull.scale.set(.7);
    fernFull.anchor.set(.5);
    fernFull.alpha=.5;
    leafFull=this.game.add.sprite(1600,580, 'fern','Leaf_section.png');
    leafFull.scale.set(1.5);
    leafFull.alpha=.5;
    leafFull.anchor.set(.5);

    

    Sporophyil=this.game.add.sprite(1040,515, 'fern','Sporophyil.png');
    Sporophyil.scale.set(.7);
    Sporophyil.anchor.set(.5);
    Sporophyil.alpha=0;
    arrow_Sporophyil=this.game.add.sprite(1080,400,'fixed_arrow');
    arrow_Sporophyil.scale.setTo(-2,1);
    arrow_Sporophyil.anchor.set(.5);
    arrow_Sporophyil.visible=false;
    arrow_Sporophyil_1=this.game.add.sprite(1130,410,'fixed_arrow');
    arrow_Sporophyil_1.scale.setTo(-1.5,1);
    arrow_Sporophyil_1.angle=-70;
    arrow_Sporophyil_1.visible=false;
    Sporophyil_Label=this.game.add.text(1130,370,"Sporophyil",fontStyle_b);
    Sporophyil_Label.visible=false;

    Sori=this.game.add.sprite(1600,580, 'fern','Sori.png');
    Sori.scale.set(1.5);
    Sori.anchor.set(.5);
    Sori.alpha=0;
    arrow_Sori=this.game.add.sprite(1603,525,'fixed_arrow');
    arrow_Sori.scale.setTo(2,1);
    arrow_Sori.anchor.set(.5);
    arrow_Sori.angle=90;
    arrow_Sori.visible=false;
    Sori_Label=this.game.add.text(1570,440,"Sori",fontStyle_b);
    Sori_Label.visible=false;

    Ramenta=this.game.add.sprite(1485,630, 'fern','Ramenta.png');
    Ramenta.scale.set(1.5);
    Ramenta.anchor.set(.5);
    Ramenta.alpha=0;
    arrow_Ramenta=this.game.add.sprite(1460,680,'fixed_arrow');
    arrow_Ramenta.scale.setTo(2,1);
    arrow_Ramenta.anchor.set(.5);
    arrow_Ramenta.angle=-70;
    arrow_Ramenta.visible=false;
    Ramenta_Label=this.game.add.text(1350,720,"Ramenta",fontStyle_b);
    Ramenta_Label.visible=false;

    arrow_Pinnule=this.game.add.sprite(1580,678,'fixed_arrow');
    arrow_Pinnule.scale.setTo(2,1);
    arrow_Pinnule.anchor.set(.5);
    arrow_Pinnule.angle=-90;
    arrow_Pinnule.visible=false;
    Pinnule_Label=this.game.add.text(1540,720,"Pinnule",fontStyle_b);
    Pinnule_Label.visible=false;

    Adventitious_root=this.game.add.sprite(1097,960, 'fern','Adventitious_root.png');
    Adventitious_root.scale.set(.7);
    Adventitious_root.anchor.set(.5);
    Adventitious_root.alpha=0;
    arrow_Adventitious_root=this.game.add.sprite(1180,950,'fixed_arrow');
    arrow_Adventitious_root.scale.setTo(-1.5,1);
    arrow_Adventitious_root.anchor.set(.5);
    arrow_Adventitious_root.visible=false;
    Adventitious_root_Label=this.game.add.text(1215,927,"Adventitious root",fontStyle_b);
    Adventitious_root_Label.visible=false;

    Rhizome=this.game.add.sprite(1095,930, 'fern','Rhizome.png');
    Rhizome.scale.set(.7);
    Rhizome.anchor.set(.5);
    Rhizome.alpha=0;
    arrow_Rhizome=this.game.add.sprite(1020,903,'fixed_arrow');
    arrow_Rhizome.scale.setTo(2,1);
    arrow_Rhizome.anchor.set(.5);
    arrow_Rhizome.angle=30;
    arrow_Rhizome.visible=false;
    Rhizome_Label=this.game.add.text(880,837,"Rhizome",fontStyle_b);
    Rhizome_Label.visible=false;
    
    Circinate_leaf=this.game.add.sprite(1095,835, 'fern','Circinate_leaf.png');
    Circinate_leaf.scale.set(.7);
    Circinate_leaf.anchor.set(.5);
    Circinate_leaf.alpha=0;
    arrow_Circinate_leaf=this.game.add.sprite(1160,850,'fixed_arrow');
    arrow_Circinate_leaf.scale.setTo(-1.5,1);
    arrow_Circinate_leaf.anchor.set(.5);
    arrow_Circinate_leaf.visible=false;
    Circinate_leaf_Label=this.game.add.text(1195,827,"Circinate leaf",fontStyle_b);
    Circinate_leaf_Label.visible=false;

    


    label_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    label_box1.beginFill(0xE67E22,1);//9B59B6
    label_box1.drawRect(200, 960, 340, 60);
    label_1=this.game.add.text(240,970,"Dryopteris (Fern)",lablel_fontStyle);//870,930
   

    
    
    arrow=this.game.add.sprite(990,795, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    //arrow.angle=90;
   arrow.visible=false;
   // arrow.state="";

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe and study the parts of fern.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

   
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_1",1);
    voice.play();

    dialog.text="Click on the sporophyll.";
    arrow.x=Sporophyil.x+55;
    arrow.y=Sporophyil.y-80;

    arrow.visible=true;

    Sporophyil.inputEnabled = true;
    Sporophyil.input.useHandCursor = true;
    Sporophyil.events.onInputUp.add(this.clickOnSporophyil,this);
    
    
    ///////////////////////shortcut////////////////////
    
  },

  clickOnSporophyil:function()
  {
    Sporophyil.inputEnabled = false;
    Sporophyil.input.useHandCursor = false;
    Sporophyil.alpha=1;
    arrow_Sporophyil.visible=true;
    arrow_Sporophyil_1.visible=true;
    Sporophyil_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_2",1);
    voice.play();
    dialog.y=60;
    dialog.text="A sporophyll is a leaf that bears sporangia.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.showSori, this); 
  },
  showSori:function()
  {
    arrow.visible=true;
    arrow.x=Sori.x;
    arrow.y=Sori.y-40;
    voice.destroy(true);
    voice=this.game.add.audio("step4_3",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the sori.";
    Sori.inputEnabled = true;
    Sori.input.useHandCursor = true;
    Sori.events.onInputDown.add(this.clickOnSori, this);

  },
  clickOnSori:function()
  {
    Sori.inputEnabled = false;
    Sori.input.useHandCursor = false;
    Sori.alpha=1;
    arrow_Sori.visible=true;
    Sori_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_4",1);
    voice.play();
    dialog.y=60;
    dialog.text="In mature plants, the leaves bear reproductive structures called sori.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.showRamenta, this); 

  },

  showRamenta:function()
  {
    arrow.visible=true;
    arrow.x=Ramenta.x-50;
    arrow.y=Ramenta.y;
    arrow.angle=-90;
    voice.destroy(true);
    voice=this.game.add.audio("step4_5",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the Ramenta.";
    Ramenta.inputEnabled = true;
    Ramenta.input.useHandCursor = true;
    Ramenta.events.onInputDown.add(this.clickOnRamenta, this);

  },
  clickOnRamenta:function()
  {
    Ramenta.inputEnabled = false;
    Ramenta.input.useHandCursor = false;
    Ramenta.alpha=1;
    arrow_Ramenta.visible=true;
    Ramenta_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_6",1);
    voice.play();
    dialog.y=60;
    dialog.text="Multicellular scales called ramenta are present on the petiole of leaf.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.showleafFull, this);
  },

  showleafFull:function()
  {
    arrow.visible=true;
    arrow.x=leafFull.x-30;
    arrow.y=leafFull.y+80;
    arrow.angle=-170;
    voice.destroy(true);
    voice=this.game.add.audio("step4_7",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the pinnule.";
    leafFull.inputEnabled = true;
    leafFull.input.useHandCursor = true;
    leafFull.events.onInputDown.add(this.clickOnleafFull, this);

  },

  clickOnleafFull:function()
  {
    leafFull.inputEnabled = false;
    leafFull.input.useHandCursor = false;
    leafFull.alpha=1;
    arrow_Pinnule.visible=true;
    Pinnule_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_8",1);
    voice.play();
    dialog.y=60;
    dialog.text="The ultimate free division of a compound leaf is called pinnule.";
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.showRhizome, this);
  },
  showRhizome:function()
  {
    arrow.visible=true;
    arrow.x=Rhizome.x-50;
    arrow.y=Rhizome.y-40;
    arrow.angle=-30;
    voice.destroy(true);
    voice=this.game.add.audio("step4_9",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the Rhizome.";
    Rhizome.inputEnabled = true;
    Rhizome.input.useHandCursor = true;
    Rhizome.events.onInputDown.add(this.clickOnRhizome, this);
  },

  clickOnRhizome:function()
  {
    Rhizome.inputEnabled = false;
    Rhizome.input.useHandCursor = false;
    Rhizome.alpha=1;
    arrow_Rhizome.visible=true;
    Rhizome_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_10",1);
    voice.play();
    dialog.y=60;
    dialog.text="The underground stems are called rhizomes.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.showCircinate_leaf, this);
  },
  showCircinate_leaf:function()
  {
    arrow.visible=true;
    arrow.x=Circinate_leaf.x+60;
    arrow.y=Circinate_leaf.y+10;
    arrow.angle=90;
    voice.destroy(true);
    voice=this.game.add.audio("step4_11",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the Circinate leaf.";
    Circinate_leaf.inputEnabled = true;
    Circinate_leaf.input.useHandCursor = true;
    Circinate_leaf.events.onInputDown.add(this.clickOnCircinate_leaf, this);
  },

  clickOnCircinate_leaf:function()
  {
    Circinate_leaf.inputEnabled = false;
    Circinate_leaf.input.useHandCursor = false;
    Circinate_leaf.alpha=1;
    arrow_Circinate_leaf.visible=true;
    Circinate_leaf_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_12",1);
    voice.play();
    dialog.y=40;
    dialog.text="Young leaves which arise from the rhizome remain coiled like a spring \nand are called circinate leaves.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.showAdventitious_root, this);
  },
  showAdventitious_root:function()
  {
    arrow.visible=true;
    arrow.x=Adventitious_root.x+100;
    arrow.y=Adventitious_root.y;
    voice.destroy(true);
    voice=this.game.add.audio("step4_13",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the Adventitious root.";
    Adventitious_root.inputEnabled = true;
    Adventitious_root.input.useHandCursor = true;
    Adventitious_root.events.onInputDown.add(this.clickOnAdventitious_root, this);
  },

  clickOnAdventitious_root:function()
  {
    Adventitious_root.inputEnabled = false;
    Adventitious_root.input.useHandCursor = false;
    Adventitious_root.alpha=1;
    arrow_Adventitious_root.visible=true;
    Adventitious_root_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step4_14",1);
    voice.play();
    dialog.y=60;
    dialog.text="Roots that arise directly from the rhizomes are called adventitious roots.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.showFull, this);
    
    
  },
 
  showFull: function()
	{
    
   fernFull.alpha=1;
    leafFull.alpha=1;
    Sporophyil.visible=false;
    Circinate_leaf.visible=false;
    Rhizome.visible=false;
    Adventitious_root.visible=false;
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.nextStage, this);
	},
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
   
    
	},
  
	
	nextStage:function(){
    

    voice.destroy(true);
    voice=this.game.add.audio("step_end",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_4",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
