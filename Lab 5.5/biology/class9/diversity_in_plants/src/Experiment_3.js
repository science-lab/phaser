var experiment_3 = function(game){

var inst;


var delay;
var incr;
var currentobj;

var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
var tissue;
var slideNo;
}


experiment_3.prototype = {
  	create: function(){

hotFlag=false;
tissue=null;
bg= this.game.add.sprite(0, 0,'bg');






voice=this.game.add.audio("step3_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toObservation, this);
play.visible=false;

// prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
// prev_btn.scale.setTo(-.7,.7);
// prev_btn.inputEnabled = true;
//prev_btn.input.priorityID = 3;
// prev_btn.input.useHandCursor = true;
// prev_btn.events.onInputDown.add(this.backToExp, this);
// prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toObservation:function()
 {
 voice.destroy();
 
 this.state.start("Observations_2", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    white_box1 = this.game.add.graphics(0, 0);
    white_box1.lineStyle(5,0xE67E22,.6);
    white_box1.beginFill(0xffffff,1);//9B59B60xffffff
    white_box1.drawRect(800,240,1000,800);

   

    agaricusSpecimen=this.game.add.sprite(10,280, 'moss_specimen');
    agaricusSpecimen.scale.set(.7);

    mossFull=this.game.add.sprite(900,280, 'moss','moss_full.png');
    mossFull.scale.set(.7);
    mossFull.alpha=.5;

    mossCalyptra=this.game.add.sprite(1065,300, 'moss','calyptra.png');
    mossCalyptra.scale.set(.7);
    mossCalyptra.alpha=0;
    arrowCalyptra=this.game.add.sprite(1160,290,'fixed_arrow');
    arrowCalyptra.scale.setTo(-2,1);
    arrowCalyptra.visible=false;
    calyptraLabel=this.game.add.text(1170,280,"Calyptra",fontStyle_b);
    calyptraLabel.visible=false;

    mossCapsule=this.game.add.sprite(1082,320, 'moss','capsule.png');
    mossCapsule.scale.set(.7);
    mossCapsule.alpha=0;
    arrowCapsule=this.game.add.sprite(1210,350,'fixed_arrow');
    arrowCapsule.scale.setTo(-2,1);
    arrowCapsule.visible=false;
    capsuleLabel=this.game.add.text(1220,340,"Capsule",fontStyle_b);
    capsuleLabel.visible=false;

    mossSeta=this.game.add.sprite(1092,365, 'moss','seta.png');
    mossSeta.scale.set(.7);
    mossSeta.alpha=0;
    arrowSeta=this.game.add.sprite(1220,480,'fixed_arrow');
    arrowSeta.scale.setTo(-2,1);
    arrowSeta.visible=false;
    setaLabel=this.game.add.text(1220,470,"Seta",fontStyle_b);
    setaLabel.visible=false;

    mossFoot=this.game.add.sprite(1065,605, 'moss','foot.png');
    mossFoot.scale.set(.7);
    mossFoot.alpha=0;
    arrowFoot=this.game.add.sprite(1240,600,'fixed_arrow');
    arrowFoot.scale.setTo(-2,1);
    arrowFoot.visible=false;
    footLabel=this.game.add.text(1250,590,"Foot",fontStyle_b);
    footLabel.visible=false;
    markSporophyte=this.game.add.sprite(1050,300, 'moss','mark.png');
    markSporophyte.scale.setTo(-.5,.85);
    markSporophyte.visible=false;
    markSporophyteLabel=this.game.add.text(840,440,"Sporophyte",fontStyle_b);
    markSporophyteLabel.visible=false;


    mossMaleBranch=this.game.add.sprite(954,678, 'moss','male_branch.png');
    mossMaleBranch.scale.set(.7);
    mossMaleBranch.alpha=0;
    arrowMaleBranch=this.game.add.sprite(930,780,'fixed_arrow');
    arrowMaleBranch.scale.setTo(2,1);
    arrowMaleBranch.visible=false;
    maleBranchLabel=this.game.add.text(830,750,"Male\nbranch",fontStyle_b);
    maleBranchLabel.visible=false;

    mossGametophyte=this.game.add.sprite(998,600, 'moss','gametophyte.png');
    mossGametophyte.scale.set(.7);
    mossGametophyte.alpha=0;
    arrowGametophyte=this.game.add.sprite(1250,760,'fixed_arrow');
    arrowGametophyte.scale.setTo(-2,1);
    arrowGametophyte.visible=false;
    GametophyteLabel=this.game.add.text(1260,730,"Female\nbranch",fontStyle_b);
    GametophyteLabel.visible=false;

    mossRhizoids=this.game.add.sprite(998,904, 'moss','rhizoid.png');
    mossRhizoids.scale.set(.7);
    mossRhizoids.alpha=0;
    arrowRhizoids=this.game.add.sprite(1350,920,'fixed_arrow');
    arrowRhizoids.scale.setTo(-2,1);
    arrowRhizoids.visible=false;
    RhizoidsLabel=this.game.add.text(1350,910,"Rhizoids",fontStyle_b);
    RhizoidsLabel.visible=false;

    markGametophyte=this.game.add.sprite(1450,580, 'moss','mark.png');
    markGametophyte.scale.setTo(.5,.7);
    markGametophyte.visible=false;
    markGametophyteLabel=this.game.add.text(1480,700,"Gametophyte",fontStyle_b);
    markGametophyteLabel.visible=false;
   


    
    label_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    label_box1.beginFill(0xE67E22,1);//9B59B6
    label_box1.drawRect(200, 960, 340, 60);
    label_1=this.game.add.text(210,970,"Funaria ( Moss/Fern )",lablel_fontStyle);//870,930
   

    
    
    arrow=this.game.add.sprite(990,795, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.angle=90;
   arrow.visible=false;
   // arrow.state="";

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe and study the parts of moss.";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

   
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_1",1);
    voice.play();

    dialog.text="Click on the calyptra.";
    arrow.x=1120;
    arrow.y=310;
    arrow.visible=true;

    mossCalyptra.inputEnabled = true;
    mossCalyptra.input.useHandCursor = true;
    mossCalyptra.events.onInputDown.add(this.clickOnMossCalyptra, this);
    // tween1=this.game.add.tween(microscopeGroup).to( {x:0}, 800, Phaser.Easing.Out, true);
    // tween1.onComplete.add(function () {
    //   this.game.time.events.add(Phaser.Timer.SECOND*4,this.ExplainMicroscope, this);
      
  
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },

  clickOnMossCalyptra:function()
  {
    mossCalyptra.inputEnabled = false;
    mossCalyptra.input.useHandCursor = false;
    mossCalyptra.alpha=1;
    arrowCalyptra.visible=true;
    calyptraLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_2",1);
    voice.play();
    dialog.y=40;
    dialog.text="Calyptra is an enlarged archegonial venter that protects the capsule\ncontaining the embryonic sporophyte.";
    this.game.time.events.add(Phaser.Timer.SECOND*8,this.showMossCalyptraEnd, this); 


  },
  showMossCalyptraEnd:function()
  {
    arrow.visible=true;
    arrow.x=1150;
    arrow.y=360;
    voice.destroy(true);
    voice=this.game.add.audio("step3_3",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the capsule.";
    mossCapsule.inputEnabled = true;
    mossCapsule.input.useHandCursor = true;
    mossCapsule.events.onInputDown.add(this.clickOnmossCapsule, this);

  },
  clickOnmossCapsule:function()
  {
    mossCapsule.inputEnabled = false;
    mossCapsule.input.useHandCursor = false;
    mossCapsule.alpha=1;
    arrowCapsule.visible=true;
    capsuleLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_4",1);
    voice.play();
    dialog.y=40;
    dialog.text="A moss capsule is a part of the moss located at the tip of talk that\ncontains pollen.";
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.showMossCapsuleEnd, this); 

  },

  showMossCapsuleEnd:function()
  {
    arrow.visible=true;
    arrow.x=1155;
    arrow.y=495;
    voice.destroy(true);
    voice=this.game.add.audio("step3_5",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the Seta.";
    mossSeta.inputEnabled = true;
    mossSeta.input.useHandCursor = true;
    mossSeta.events.onInputDown.add(this.clickOnmossSeta, this);

  },
  clickOnmossSeta:function()
  {
    mossSeta.inputEnabled = false;
    mossSeta.input.useHandCursor = false;
    mossSeta.alpha=1;
    arrowSeta.visible=true;
    setaLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_6",1);
    voice.play();
    dialog.y=40;
    dialog.text="The seta connects the foot, where nutrients are absorbed, to the\ndeveloping capsule.";
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.showMossSetaEnd, this);
  },

  showMossSetaEnd:function()
  {
    arrow.visible=true;
    arrow.x=1165;
    arrow.y=615;
    voice.destroy(true);
    voice=this.game.add.audio("step3_7",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the foot.";
    mossFoot.inputEnabled = true;
    mossFoot.input.useHandCursor = true;
    mossFoot.events.onInputDown.add(this.clickOnmossFoot, this);

  },

  clickOnmossFoot:function()
  {
    mossFoot.inputEnabled = false;
    mossFoot.input.useHandCursor = false;
    mossFoot.alpha=1;
    arrowFoot.visible=true;
    footLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_8",1);
    voice.play();
    dialog.y=40;
    dialog.text="Foot is the region where the unbranched sporophyte is physically\nattached to the leafy gametophyte.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.showMossFootEnd, this);
  },
  showMossFootEnd:function()
  {
    markSporophyte.visible=true;
    markSporophyteLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step3_9",1);
    voice.play();
    dialog.text="A moss sporophyte consists of a spore-containing capsule, possibly\nsitting atop a stalk called seta.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.showMossSporophyteEnd, this);
  },
  showMossSporophyteEnd:function()
  {
    arrow.visible=true;
    arrow.x=1185;
    arrow.y=770;
    voice.destroy(true);
    voice=this.game.add.audio("step3_10",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the female branch.";
    mossGametophyte.inputEnabled = true;
    mossGametophyte.input.useHandCursor = true;
    mossGametophyte.events.onInputDown.add(this.clickOnmossGametophyte, this);
  },

  clickOnmossGametophyte:function()
  {
    mossGametophyte.inputEnabled = false;
    mossGametophyte.input.useHandCursor = false;
    mossGametophyte.alpha=1;
    arrowGametophyte.visible=true;
    GametophyteLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_11",1);
    voice.play();
    dialog.text="A moss plant form male and female structures either on the same plant.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.showMossGametophyteEnd, this);
    
  },

  showMossGametophyteEnd:function()
  {
    arrow.visible=true;
    arrow.x=990;
    arrow.y=795;
    arrow.angle=-90;
    voice.destroy(true);
    voice=this.game.add.audio("step3_12",1);
    voice.play();
    dialog.text="Click on the male branch.";

    mossMaleBranch.inputEnabled = true;
    mossMaleBranch.input.useHandCursor = true;
    mossMaleBranch.events.onInputDown.add(this.clickOnmossMaleBranch, this);
  },

  clickOnmossMaleBranch:function()
  {
    mossMaleBranch.inputEnabled = false;
    mossMaleBranch.input.useHandCursor = false;
    mossMaleBranch.alpha=1;
    arrowMaleBranch.visible=true;
    maleBranchLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_11",1);
    voice.play();
    dialog.text="A moss plant form male and female structures either on the same plant.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.showMossMaleBranchEnd, this);
  },

  showMossMaleBranchEnd:function()
  {
    markGametophyte.visible=true;
    markGametophyteLabel.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step3_13",1);
    voice.play();
    dialog.y=40;
    dialog.text="The gametophyte of a ferns is a cellular monolayer structure, whoes \nmore important functions is to form the gametes.";
    this.game.time.events.add(Phaser.Timer.SECOND*8,this.showMossRhizoids, this);
  },

  showMossRhizoids:function()
  {
    arrow.visible=true;
    arrow.x=1300;
    arrow.y=930;
    arrow.angle=90;
    voice.destroy(true);
    voice=this.game.add.audio("step3_14",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the Rhizoids.";

    mossRhizoids.inputEnabled = true;
    mossRhizoids.input.useHandCursor = true;
    mossRhizoids.events.onInputDown.add(this.clickOnmossRhizoids, this);

  },

  clickOnmossRhizoids:function()
  {
    mossRhizoids.inputEnabled = false;
    mossRhizoids.input.useHandCursor = false;
    mossRhizoids.alpha=1;
    arrowRhizoids.visible=true;
    RhizoidsLabel.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step3_15",1);
    voice.play();
    dialog.y=40;
    dialog.text="The Rhizoids attach the gametophyte to the substratum and facilitate \nthe absorption of minerals and water.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.nextStage, this);
  },
  
  
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    // this.detectCollision();
    // interval++;
    // if(interval>300){
    //   interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    // }
    
    
	},
  
	
	nextStage:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step_end",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_3",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
