var observations_3 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations_3.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation3_1",1);
    voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,80,'observation_table');
  base.scale.setTo(1.1,1.1);
  
  //  base_line1= this.game.add.sprite(500,350,'observation_table_line');
  //  base_line1.scale.y=.6;
  // base_line2= this.game.add.sprite(1375,350,'observation_table_line');
  // base_line2.scale.y=.6;
  // base_line3= this.game.add.sprite(970,190,'observation_table_line');
  // base_line3.scale.y=.95;
  // base_line4= this.game.add.sprite(1330,190,'observation_table_line');
  // base_line4.scale.y=.95;
  
  //   base_line5= this.game.add.sprite(1402,320,'observation_table_line');
  //   base_line5.angle=90;
  //   base_line5.scale.setTo(1.1,1.1);
  // base_line6= this.game.add.sprite(1402,800,'observation_table_line');
  // base_line6.angle=90;
  // base_line6.scale.setTo(1.1,1.1);
  // base_line7= this.game.add.sprite(1760,490,'observation_table_line');
  // base_line7.angle=90;
  // base_line7.scale.setTo(1.1,2.08);
  // base_line8= this.game.add.sprite(1760,580,'observation_table_line');
  // base_line8.angle=90;
  // base_line8.scale.setTo(1.1,2.08);
  // base_line9= this.game.add.sprite(1760,670,'observation_table_line');
  // base_line9.angle=90;
  // base_line9.scale.setTo(1.1,2.08);
  // base_line10= this.game.add.sprite(1760,760,'observation_table_line');
  // base_line10.angle=90;
  // base_line10.scale.setTo(1.1,2.08);

  //base_line.scale.setTo(1,1.15);
    //head1_text=this.game.add.text(150,220,"Material (A) Spirogyra (Pond silk)",headfontStyle);
    //head2_text=this.game.add.text(200,320,"Classification",headfontStyle);
    //head3_text=this.game.add.text(1100,220,"Striated muscle fibres",headfontStyle); 
  //  //head4_text=this.game.add.text(1080,170,"Relative Size of \n    the Image",headfontStyle); 
  //  head5_text=this.game.add.text(1350,180,"Inference",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
   test1_text1="Kingdom              -        Plantae";
   test1_text2="Sub-kingdom       -        Cryptogamae";
   test1_text3="Division                -        Pteridophyta";
   test1_text4="";//Group                   -        Fungi
   test1_text5="Genus                   -        Dryopteris";
   
   test1_text6="I. Characteristic features of Dryopteris (Fern)";
   test1_text7="1. Dryopteris is a fern found in humid and shady places.";
   test1_text8="2. It is a vascular, sporophyte plant.";
   test1_text9="3. The adult sporophyte is differentiated into stem, leaves and roots.";
   test1_text10="4. The stem is underground and is called rhizome.";
   test1_text11="5. The leaves are large and pinnately compound. Entire leaf is called frond. Leaves \n    are photosynthetic.";
   test1_text12="6. In mature plants, the leaves bear reproductive structures called sori (which are \n    group of sporangia) on their ventral surface. Such a leaf is called sporophyll.";
   test1_text13="7. Young leaves which arise from the rhizome remain coiled like a spring and are \n    called circinate leaves (This protects the young delicate tip of the leaf).";
   test1_text14="8. Roots are adventitious (which arise from part other than radicle).";
   test1_text15="9. Multicellular scales called ramenta are present on the petiole (stalk) of leaf.";
   test1_text16="10. Fern plants lack flowers and seeds.";
   test1_text17="II. Identifying features of Group Pteridophyta and Genus Dryopteris";
   test1_text18="1. Plants have stems, leaf and roots.";
   test1_text19="2. Ferns are vascular plants with xylem and phloem for the conduction of water and \n    food.";
   test1_text20="3. Adult plant body is sporophytic and dominant.";
   test1_text21="4. Reproductive structures (sori) are present on the leaves.";
  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
 next_btn = this.game.add.sprite(1640,830,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1640,830,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(250,830,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(250,830,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 if(!observation_completed)
  play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observation3_1",1);
              voice.play();
              base_line1= this.game.add.sprite(500,370,'observation_table_line');
              base_line1.scale.y=.6;
              base_line2= this.game.add.sprite(1375,370,'observation_table_line');
              base_line2.scale.y=.6;

              base_line5= this.game.add.sprite(1402,340,'observation_table_line');
              base_line5.angle=90;
              base_line5.scale.setTo(1.1,1.1);
              base_line6= this.game.add.sprite(1402,820,'observation_table_line');
              base_line6.angle=90;
              base_line6.scale.setTo(1.1,1.1);

              head1_text=this.game.add.text(180,240,"Material (D) Dryopteris (Fern)",headfontStyle);
              head2_text=this.game.add.text(200,340,"Classification",headfontStyle);

              test1_1=this.game.add.text(600,420,test1_text1,fontStyle);
              test1_2=this.game.add.text(600,500,test1_text2,fontStyle);
              test1_3=this.game.add.text(600,580,test1_text3,fontStyle);
              test1_4=this.game.add.text(600,660,test1_text4,fontStyle);
              test1_5=this.game.add.text(600,660,test1_text5,fontStyle);
              test1_6=this.game.add.text(600,740,"",fontStyle);

              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observation3_2",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text6,headfontStyle);
               test1_2=this.game.add.text(240,340,test1_text7,fontStyle);
               test1_3=this.game.add.text(240,420,test1_text8,fontStyle);
               test1_4=this.game.add.text(240,500,test1_text9,fontStyle);
               test1_5=this.game.add.text(240,580,test1_text10,fontStyle);
               test1_6=this.game.add.text(240,660,test1_text11,fontStyle);

            break;
            case 3:
              voice.destroy(true);
              voice=this.game.add.audio("Observation3_3",1);
              voice.play();
               test1_1=this.game.add.text(240,260,test1_text12,fontStyle);
               test1_2=this.game.add.text(240,390,test1_text13,fontStyle);
               test1_3=this.game.add.text(240,520,test1_text14,fontStyle);
               test1_4=this.game.add.text(240,600,test1_text15,fontStyle);
               test1_5=this.game.add.text(240,680,test1_text16,fontStyle);
               test1_6=this.game.add.text(240,760,"",fontStyle);
            break;
            case 4:
              voice.destroy(true);
              voice=this.game.add.audio("Observation3_4",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text17,headfontStyle);
               test1_2=this.game.add.text(240,340,test1_text18,fontStyle);
               test1_3=this.game.add.text(240,420,test1_text19,fontStyle);
               test1_4=this.game.add.text(240,550,test1_text20,fontStyle);
               test1_5=this.game.add.text(240,630,test1_text21,fontStyle);
               test1_6=this.game.add.text(240,730,"",fontStyle);
            break;
            case 5:

               
            break;
          }
        },
        removeTexts:function(){
          
          if(pageCount==1){
            head1_text.destroy(true);
            head2_text.destroy(true);
            base_line1.destroy(true);
            base_line2.destroy(true);
            base_line5.destroy(true);
            base_line6.destroy(true);
          }
            test1_1.destroy(true);
            test1_2.destroy(true);
            test1_3.destroy(true);
            test1_3.destroy(true);
            test1_4.destroy(true);
            test1_5.destroy(true);
            test1_6.destroy(true);
          
        },
        toNext:function(){
          this.removeTexts();
          if(pageCount<4){
            prev_btn.visible=true;
            pageCount++;
              console.log(pageCount);
              if(pageCount>3){
                this.addObservation();
                // voice.destroy(true);
                // this.state.start("Observations_1", true, false, ip);
                play.visible=true;
                next_btn.visible=false;  
              }else {
                this.addObservation();
              }
            }
      },
      toPrev:function(){
        this.removeTexts();
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result_3", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  