var experiment_5 = function(game){

var inst;


var delay;
var incr;
var currentobj;

var clockFlag;
var clockNum;
var clockDelay;
var clockAngle;
var tissue;
var slideNo;
}


experiment_5.prototype = {
  	create: function(){

hotFlag=false;
tissue=null;
bg= this.game.add.sprite(0, 0,'bg');






voice=this.game.add.audio("step5_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "34px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toObservation, this);
play.visible=false;

// prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
// prev_btn.scale.setTo(-.7,.7);
// prev_btn.inputEnabled = true;
//prev_btn.input.priorityID = 3;
// prev_btn.input.useHandCursor = true;
// prev_btn.events.onInputDown.add(this.backToExp, this);
// prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toObservation:function()
 {
 voice.destroy();
 
 this.state.start("Observations_4", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    white_box1 = this.game.add.graphics(0, 0);
    white_box1.lineStyle(5,0xE67E22,.6);
    white_box1.beginFill(0xffffff,1);//9B59B60xffffff
    white_box1.drawRect(800,240,1000,800);

   
    fernSpecimen=this.game.add.sprite(360,700,'pinus_sprite','pinus_full.png');
    fernSpecimen.anchor.set(.5);
    fernSpecimen.scale.setTo(.3,.5);

    bottle=this.game.add.sprite(360,630,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(.8,.7);

    // fernSpecimen=this.game.add.sprite(10,280, 'fern','Specimen.png');
    // fernSpecimen.scale.set(.7);

    PinusFull=this.game.add.sprite(1300,640, 'pinus_sprite','pinus_full.png');
    PinusFull.scale.set(.7);
    PinusFull.anchor.set(.5);
    PinusFull.alpha=.4;
    

    

    female_cone=this.game.add.sprite(1155,650, 'pinus_sprite','female_cone.png');
    female_cone.scale.set(.7);
    female_cone.anchor.set(.5);
    female_cone.alpha=0;
    arrow_female_cone=this.game.add.sprite(1040,720,'fixed_arrow');
    arrow_female_cone.scale.setTo(2,1);
    arrow_female_cone.anchor.set(.5);
    arrow_female_cone.angle=-30;
    arrow_female_cone.visible=false;
    female_cone_Label=this.game.add.text(890,690,"Female \n cones",fontStyle_b);
    female_cone_Label.visible=false;

    male_cone=this.game.add.sprite(1335,610, 'pinus_sprite','male_cone.png');
    male_cone.scale.set(.7);
    male_cone.anchor.set(.5);
    male_cone.alpha=0;
    

    foliage_leaves=this.game.add.sprite(1295,640, 'pinus_sprite','foliage_leaves.png');
    foliage_leaves.scale.set(.7);
    foliage_leaves.anchor.set(.5);
    foliage_leaves.alpha=0;
    arrow_foliage_leaves=this.game.add.sprite(1460,390,'fixed_arrow');
    arrow_foliage_leaves.scale.setTo(-1.5,1);
    arrow_foliage_leaves.anchor.set(.5);
    arrow_foliage_leaves.angle=-60;
    arrow_foliage_leaves.visible=false;
    foliage_leaves_Label=this.game.add.text(1375,315,"Foliage leaves",fontStyle_b);
    foliage_leaves_Label.visible=false;

    arrow_male_cone=this.game.add.sprite(1452,550,'fixed_arrow');
    arrow_male_cone.scale.setTo(-3,1);
    arrow_male_cone.anchor.set(.5);
    arrow_male_cone.angle=-30;
    arrow_male_cone.visible=false;
    male_cone_Label=this.game.add.text(1510,475,"Male cones",fontStyle_b);
    male_cone_Label.visible=false;

    label_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    label_box1.beginFill(0xE67E22,1);//9B59B6
    label_box1.drawRect(200, 960, 340, 60);
    label_1=this.game.add.text(225,970,"Pinus (Pine or Chir)",lablel_fontStyle);//870,930
   

    
    
    arrow=this.game.add.sprite(700,880, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.angle=-90;
   arrow.visible=false;
   // arrow.state="";

    
    dia_box=this.game.add.sprite(30,20, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe and study the parts of pinus.";
    this.game.time.events.add(Phaser.Timer.SECOND*1,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=60;

   
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
     voice.destroy(true);
     voice=this.game.add.audio("step5_1",1);
     voice.play();

    dialog.text="Click on the female cone.";
    arrow.x=female_cone.x-100;
    arrow.y=female_cone.y;

    arrow.visible=true;

    female_cone.inputEnabled = true;
    female_cone.input.useHandCursor = true;
    female_cone.events.onInputUp.add(this.clickOnfemale_cone,this);
    
    
    ///////////////////////shortcut////////////////////
    
  },

  clickOnfemale_cone:function()
  {
    female_cone.inputEnabled = false;
    female_cone.input.useHandCursor = false;
    female_cone.alpha=1;
    arrow_female_cone.visible=true;
    female_cone_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step5_2",1);
    voice.play();
    dialog.y=60;
    dialog.text="Male and female cones are the reproductive parts of the Pinus.";
     this.game.time.events.add(Phaser.Timer.SECOND*4,this.Exp_femaleCone, this);
  },
  Exp_femaleCone:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step5_3",1);
    voice.play();
    dialog.y=40;
    dialog.text="Female cones arise in groups of 2-4 in the axil of scale leaves of \nlong shoots.";
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.showmale_cone, this);
  },
  showmale_cone:function()
  {
    arrow.visible=true;
    arrow.x=male_cone.x;
    arrow.y=male_cone.y-40;
    arrow.angle=0;
    voice.destroy(true);
    voice=this.game.add.audio("step5_4",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the male cone.";
    male_cone.inputEnabled = true;
    male_cone.input.useHandCursor = true;
    male_cone.events.onInputDown.add(this.clickOnmale_cone, this);

  },
  clickOnmale_cone:function()
  {
    male_cone.inputEnabled = false;
    male_cone.input.useHandCursor = false;
    male_cone.alpha=1;
    arrow_male_cone.visible=true;
    male_cone_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step5_5",1);
    voice.play();
    dialog.y=60;
    dialog.text="Male cones are present in clusters on long branches.";
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.showfoliage_leaves, this); 

  },

  showfoliage_leaves:function()
  {
    arrow.visible=true;
    arrow.x=1460;
    arrow.y=390;
    arrow.angle=0;
    voice.destroy(true);
    voice=this.game.add.audio("step5_6",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the foliage leaves.";
    foliage_leaves.inputEnabled = true;
    foliage_leaves.input.useHandCursor = true;
    foliage_leaves.events.onInputDown.add(this.clickOnfoliage_leaves, this);

  },
  clickOnfoliage_leaves:function()
  {
    foliage_leaves.inputEnabled = false;
    foliage_leaves.input.useHandCursor = false;
    foliage_leaves.alpha=1;
    arrow_foliage_leaves.visible=true;
    foliage_leaves_Label.visible=true;
    arrow.visible=false;
    voice.destroy(true);
    voice=this.game.add.audio("step5_7",1);
    voice.play();
    dialog.y=60;
    dialog.text="Foliage leaves or needles are green, needle-like, photosynthetic.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.showFull, this);
  },

 
  showFull: function()
	{
    
   PinusFull.alpha=1;
    
    female_cone.visible=false;
    male_cone.visible=false;
    foliage_leaves.visible=false;
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.nextStage, this);
	},
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
   
    
	},
  
	
	nextStage:function(){
    

    voice.destroy(true);
    voice=this.game.add.audio("step_end",1);
    voice.play();
    dialog.y=60;
    dialog.text="Click on the next button to see the observations.";
    play.visible=true;
  },
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_5",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
