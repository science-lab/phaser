var observations_4 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var img;
  
  }
  
  observations_4.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observation4_1",1);
    voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,80,'observation_table');
  base.scale.setTo(1.1,1.1);
  
  //  base_line1= this.game.add.sprite(500,350,'observation_table_line');
  //  base_line1.scale.y=.6;
  // base_line2= this.game.add.sprite(1375,350,'observation_table_line');
  // base_line2.scale.y=.6;
  // base_line3= this.game.add.sprite(970,190,'observation_table_line');
  // base_line3.scale.y=.95;
  // base_line4= this.game.add.sprite(1330,190,'observation_table_line');
  // base_line4.scale.y=.95;
  
  //   base_line5= this.game.add.sprite(1402,320,'observation_table_line');
  //   base_line5.angle=90;
  //   base_line5.scale.setTo(1.1,1.1);
  // base_line6= this.game.add.sprite(1402,800,'observation_table_line');
  // base_line6.angle=90;
  // base_line6.scale.setTo(1.1,1.1);
  // base_line7= this.game.add.sprite(1760,490,'observation_table_line');
  // base_line7.angle=90;
  // base_line7.scale.setTo(1.1,2.08);
  // base_line8= this.game.add.sprite(1760,580,'observation_table_line');
  // base_line8.angle=90;
  // base_line8.scale.setTo(1.1,2.08);
  // base_line9= this.game.add.sprite(1760,670,'observation_table_line');
  // base_line9.angle=90;
  // base_line9.scale.setTo(1.1,2.08);
  // base_line10= this.game.add.sprite(1760,760,'observation_table_line');
  // base_line10.angle=90;
  // base_line10.scale.setTo(1.1,2.08);

  //base_line.scale.setTo(1,1.15);
    //head1_text=this.game.add.text(150,220,"Material (A) Spirogyra (Pond silk)",headfontStyle);
    //head2_text=this.game.add.text(200,320,"Classification",headfontStyle);
    //head3_text=this.game.add.text(1100,220,"Striated muscle fibres",headfontStyle); 
  //  //head4_text=this.game.add.text(1080,170,"Relative Size of \n    the Image",headfontStyle); 
  //  head5_text=this.game.add.text(1350,180,"Inference",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
   test1_text1="Kingdom              -        Plantae";
   test1_text2="Sub-kingdom       -        Phanerogamae";
   test1_text3="Division                -        Gymnosperms";
   test1_text4="";//Group                   -        Fungi
   test1_text5="Genus                   -        Pinus";
   
   test1_text6="I. Characteristic feature of Pinus";
   test1_text7="1. Plant body of Pinus is a tall, eract, evergreen three usually found in hilly areas.";
   test1_text8="2. Plant body is differentiated into stem, roots and leaves.";
   test1_text9="3. Pinusis a vascular plant with well developed xylem and phloem.";
   test1_text10="4. The trees appear conical and pyramidal in shape and have spirally grown lateral \n    branches. Branches are of two types.";
   test1_text11="(a) Long branches which arise on main stem and have unlimited growth.";
   test1_text12="(b) Dwarf branches which arise in the axil of scale leaves of long branches. These \n      branches bear needle like leaves.";
   test1_text13="5. Pinus has two types of leaves, scale leaves and foliage leaves.";
   test1_text14="(a) Scale leaves are small, brown, non-photosynthetic and spirally arranged.";
   test1_text15="(b) Foliage leaves or needles are green, needle like, photosynthetic and present \n      singly or in groups of 2 to 5 in the axil of scale leaves depending on Pinus \n      species.";
   test1_text16="6. Resin canals are found in all parts of the Pinus. These contain resin.";
   test1_text17="7. The Pinusis a monoecious (one thallus) plant bearing both male and female cones \n    on different branches of the same tree.";
   test1_text18="8. It is a heterosporous plant where male cones produce many small microspores and \n    female cones produce large megaspores.";
   test1_text19="Male and female cones are the reproductive parts of the Pinus. It forms seeds but \n    seeds are not protected in the fruit i.e., seeds are naked.";
   test1_text20="(a) Female Cones of the Pinus";
   test1_text21="1. Female cones arise in the groups of 2-4 in the axil of scale leaves of long shoots.";
   test1_text22="2. Female cones take about 3 years to mature.";
   test1_text23="In the first year, the cones are very small and greenish red in colour.";
   test1_text24="In the second year, the cones are large and woody and megasporophylls are \ncompactly arranged.";
   test1_text25="In the third year, the megasporophylls separate out from one another due to the \nelongation of the cone axis. The cone is large and woody and megasporophylls \nare loosely arranged.";
   test1_text26="3. Ovules after fertilization, mature into seeds which have wings.";
   test1_text27="4. Seeds are naked as they are not enclosed in the fruit.";
   test1_text28="(b) Male Cones of Pinus";
   test1_text29="1. Male cones are present in clusters (groups) on long branches.";
   test1_text30="2. Male cones have many microsporophylls arranged spirally around the axis.";
   test1_text31="3. Male cone is dark brown in colour and is narrow at the base and broad at the apex.";
   test1_text32="4. Each microsporangia bears many winged microspores or pollen grains.";
   test1_text33="5. The wings of the pollen grains help it to be carried by winds and reach ovules \n    (megasporangia) of female cones of Pinus and fertilise them.";
   test1_text34="II. Identifying features of Group Gymnosperms and Genus Pinus (Male Cones \n      and Female Cones) ";
   test1_text35="1. Plant body is well developed sporophytic evergreen, xerophytic tree. ";
   test1_text36="2. Vascular tissue i.e., xylem and phloem are well developed.";
   test1_text37="3. Reproductive organs are male and female cones and Pinus is heterosporous and \n    monoecious plant.";
   test1_text38="4. Seeds are naked and are not enclosed in the mature ovary or fruit.";
  
  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
 next_btn = this.game.add.sprite(1640,830,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1640,830,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(250,830,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(250,830,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 if(!observation_completed)
  play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_1",1);
              voice.play();
              base_line1= this.game.add.sprite(500,370,'observation_table_line');
              base_line1.scale.y=.6;
              base_line2= this.game.add.sprite(1375,370,'observation_table_line');
              base_line2.scale.y=.6;

              base_line5= this.game.add.sprite(1402,340,'observation_table_line');
              base_line5.angle=90;
              base_line5.scale.setTo(1.1,1.1);
              base_line6= this.game.add.sprite(1402,820,'observation_table_line');
              base_line6.angle=90;
              base_line6.scale.setTo(1.1,1.1);

              head1_text=this.game.add.text(180,240,"Material (E) Pinus (Pine or Chir)",headfontStyle);
              head2_text=this.game.add.text(200,340,"Classification",headfontStyle);

              test1_1=this.game.add.text(600,420,test1_text1,fontStyle);
              test1_2=this.game.add.text(600,500,test1_text2,fontStyle);
              test1_3=this.game.add.text(600,580,test1_text3,fontStyle);
              test1_4=this.game.add.text(600,660,test1_text4,fontStyle);
              test1_5=this.game.add.text(600,660,test1_text5,fontStyle);
              test1_6=this.game.add.text(600,740,"",fontStyle);

              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_2",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text6,headfontStyle);
               test1_2=this.game.add.text(240,340,test1_text7,fontStyle);
               test1_3=this.game.add.text(240,420,test1_text8,fontStyle);
               test1_4=this.game.add.text(240,500,test1_text9,fontStyle);
               test1_5=this.game.add.text(240,580,test1_text10,fontStyle);
               test1_6=this.game.add.text(280,710,test1_text11,fontStyle);

            break;
            case 3:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_3",1);
              voice.play();
               test1_1=this.game.add.text(280,240,test1_text12,fontStyle);
               test1_2=this.game.add.text(240,370,test1_text13,fontStyle);
               test1_3=this.game.add.text(280,450,test1_text14,fontStyle);
               test1_4=this.game.add.text(280,530,test1_text15,fontStyle);
               test1_5=this.game.add.text(240,700,test1_text16,fontStyle);
               test1_6=this.game.add.text(240,760,"",fontStyle);
            break;
            case 4:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_4",1);
              voice.play();
               test1_1=this.game.add.text(240,240,test1_text17,fontStyle);
               test1_2=this.game.add.text(240,370,test1_text18,fontStyle);
               test1_3=this.game.add.text(240,500,test1_text19,fontStyle);
               test1_4=this.game.add.text(240,630,"",fontStyle);
               test1_5=this.game.add.text(240,730,"",fontStyle);
               test1_6=this.game.add.text(240,730,"",fontStyle);
            break;
            case 5:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_5",1);
              voice.play();
               test1_1=this.game.add.text(180,230,test1_text20,headfontStyle);
               test1_2=this.game.add.text(240,310,test1_text21,fontStyle);
               test1_3=this.game.add.text(240,380,test1_text22,fontStyle);
                test1_4=this.game.add.text(280,500,"",fontStyle);
                test1_5=this.game.add.text(280,580,"",fontStyle);
               test1_6=this.game.add.text(280,710,"",fontStyle);
               img=this.game.add.sprite(950, 675,'female_cones');
               img.anchor.set(.5);
               img.scale.set(.75);
            break;
            case 6:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_6",1);
              voice.play();
               test1_1=this.game.add.text(240,240,test1_text23,fontStyle);
               test1_2=this.game.add.text(240,320,test1_text24,fontStyle);
               test1_3=this.game.add.text(240,450,test1_text25,fontStyle);
               test1_4=this.game.add.text(240,630,test1_text26,fontStyle);
               test1_5=this.game.add.text(240,710,test1_text27,fontStyle);
               test1_6=this.game.add.text(240,630,"",fontStyle);
               
               
            break;
            case 7:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_7",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text28,headfontStyle);
               test1_2=this.game.add.text(240,340,test1_text29,fontStyle);
               test1_3=this.game.add.text(240,420,test1_text30,fontStyle);
               test1_4=this.game.add.text(240,500,test1_text31,fontStyle);
               test1_5=this.game.add.text(240,580,test1_text32,fontStyle);
               test1_6=this.game.add.text(240,660,test1_text33,fontStyle);
            break;
            case 8:
              voice.destroy(true);
              voice=this.game.add.audio("Observation4_8",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text34,headfontStyle);
               test1_2=this.game.add.text(240,400,test1_text35,fontStyle);
               test1_3=this.game.add.text(240,480,test1_text36,fontStyle);
               test1_4=this.game.add.text(240,560,test1_text37,fontStyle);
               test1_5=this.game.add.text(240,690,test1_text38,fontStyle);
               test1_6=this.game.add.text(240,660,"",fontStyle);
            break;
            
          }
        },
        removeTexts:function(){
          
          if(pageCount==1){
            head1_text.destroy(true);
            head2_text.destroy(true);
            base_line1.destroy(true);
            base_line2.destroy(true);
            base_line5.destroy(true);
            base_line6.destroy(true);
          }
          if(pageCount==5){
            img.destroy(true);
          }
            test1_1.destroy(true);
            test1_2.destroy(true);
            test1_3.destroy(true);
            test1_3.destroy(true);
            test1_4.destroy(true);
            test1_5.destroy(true);
            test1_6.destroy(true);
          
        },
        toNext:function(){
          this.removeTexts();
          if(pageCount<8){
            prev_btn.visible=true;
            pageCount++;
              console.log(pageCount);
              if(pageCount>7){
                this.addObservation();
                // voice.destroy(true);
                // this.state.start("Observations_1", true, false, ip);
                play.visible=true;
                next_btn.visible=false;  
              }else {
                this.addObservation();
              }
            }
      },
      toPrev:function(){
        this.removeTexts();
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result_4", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  