var theory = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  var maskBox;
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var current_scene;
  var previous_scene;
  var xmin;
  var ymin;
  var xmax;
  var ymax;
  var imageDrag;
  var zoomed;
  var currentx;
  var previousx;
  var currenty;
  var previousy;
  }
  
  theory.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

  muted = false;
  voice=this.game.add.audio("obj",1);
  // voice=this.game.add.audio("fobj",1);
  //voice.play();
  bg= this.game.add.sprite(0, 0,'bg');
  bg.scale.setTo(1,1.3);
  xmin=200;
  ymin=200;
  xmax=200;
  ymax=200;
  imageDrag=false;
  zoomed=false;
  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  //muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}
  
  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  subfontStyle={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,140,'dialogue_box');
  base.scale.setTo(1.05,1.00);

zoomGroup=this.game.add.group();
zoomGroup.visible=false;

  procedure_text=this.game.add.text(210,200,"Theory:",headfontStyle);
 // exp_Name="decomposition_reaction";
 //current_scene=1;
 previous_scene=0;
      procedure_step_1="Angiosperms or flowering plants are the most complex group of plant kingdom.";
      procedure_step_2="Plant body is divided into true roots, stem and leaves.";
      procedure_step_3="After sexual reproduction the plants produce seeds which are enclosed within fruit.";
      procedure_step_4="Angiosperms are the largest of all the major groups of living plants.";
      procedure_step_5="Angiosperms are divided into two classes; Dicotyledonae (Dicots) and \nMonocotyledonae (Monocots).";
      procedure_step_6="Dicots have two cotyledons in the seeds. Their leaves have reticulate venation.";
      procedure_step_7="Vascular bundles consists of open vascular bundles (with cambium) and are arranged \nin a ring in dicots.";
      procedure_step_8="Dicot flowers are penta or tetramerous.";
      procedure_step_9="Monocots, on the other hand, have single cotyledon in the seed.";
      procedure_step_10="Leaves have generally parallel venation. Vascular bundles are closed and scattered \nin the stem (without cambium).";
      procedure_step_11="Flowers in monocots are trimerous.";
      procedure_step_12="In this experiment, external features of root, stem, leaf and flower of dicot and \nmonocot plants are studied from the fresh or preserved specimens.";
      procedure_step_13="";
      procedure_step_14="";
      procedure_step_15="";
      procedure_step_16="";
      procedure_step_17="";  
      procedure_step_18="";
      procedure_step_19="";
      procedure_step_20="";
      
      this.showTheory();
  /////////////////////////////////////////////////////////////////
  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  next_btn = this.game.add.sprite(1660,890,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1660,890,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 
  prev_btn = this.game.add.sprite(260,890,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(260,890,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  if(theory_Scene==1)
    prev_btn.visible=false;
  //next_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrevious, this);
 
 
  
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toLab_precautions, this);
 if(!theory_completed)
    play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

  this.drawVLine(280,182,280,1005);
  this.drawVLine(1000,182,1000,1005);
  //this.drawVLine(1270,182,1270,1005);
  this.drawVLine(150,280,1770,280);
  
  
   head1_text=this.game.add.text(165,200,"S.No",headfontStyle);
   zoomGroup.add(head1_text); 
   head2_text=this.game.add.text(520,200,"Dicots",headfontStyle);
   zoomGroup.add(head2_text); 
   head3_text=this.game.add.text(1250,200,"Monocots",headfontStyle); 
   zoomGroup.add(head3_text); 
   //head4_text=this.game.add.text(1310,200,"Sclerenchyma Tissue",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
    test1_text1="1";
   test1_text2="Two cotyledons in the seed.";
   test1_text3="One cotyledon in the seed.";
   //test1_text4="These are thick walled cells.";

   test2_text1="2";
   test2_text2="Leaves with reticulate venation.";
   test2_text3="Leaves with parallel venation.";
   //test2_text4="These are compactly \narranged cells.";

   test3_text1="3";
   test3_text2="Primary root is long-lived and with \ntaproot system.";
   test3_text3="Primary root is short-lived. Taproot is \nabsent.";
   //test3_text4="No intercellular spaces.";

   test4_text1="4";
   test4_text2="Flowers generally pentamerous or \ntetramerous (Floral parts in sets of 5 \nor 4 their multiples).";
   test4_text3="Flowers are trimerous (floral parts in sets \nof three or its multiples).";
   //test4_text4="These are dead cells.";
   
   test5_text1="5";
   test5_text2="Stem has vascular bundles present in \na ring. Examples - Brassica, Sunflower, \nPea, Rose, etc.";
   test5_text3="Stem has vascular bundles scattered in \nthe ground tissue. Example - Maize, \nGrasses, Onion, Lilies, etc.";
   //test5_text4="Main function is to give \nmechanical strength \nto plant parts.";
   
   
    test1_0=this.game.add.text(350,110,"Differences between Dicots and Monocots",headfontStyle);
    zoomGroup.add(test1_0);
   test1_1=this.game.add.text(200,300,test1_text1,fontStyle);
   zoomGroup.add(test1_1); 
   test1_2=this.game.add.text(300,300,test1_text2,fontStyle);
   zoomGroup.add(test1_2); 
   test1_3=this.game.add.text(1020,300,test1_text3,fontStyle);
   zoomGroup.add(test1_3); 
   //test1_4=this.game.add.text(1280,300,test1_text4,fontStyle);

   test2_1=this.game.add.text(200,380,test2_text1,fontStyle);
   zoomGroup.add(test2_1); 
   test2_2=this.game.add.text(300,380,test2_text2,fontStyle);
   zoomGroup.add(test2_2); 
   test2_3=this.game.add.text(1020,380,test2_text3,fontStyle);
   zoomGroup.add(test2_3); 
   //test2_4=this.game.add.text(1280,400,test2_text4,fontStyle);
  
   test3_1=this.game.add.text(200,460,test3_text1,fontStyle);
   zoomGroup.add(test3_1); 
   test3_2=this.game.add.text(300,460,test3_text2,fontStyle);
   zoomGroup.add(test3_2); 
   test3_3=this.game.add.text(1020,460,test3_text3,fontStyle);
   zoomGroup.add(test3_3); 
   //test3_4=this.game.add.text(1280,550,test3_text4,fontStyle);

   test4_1=this.game.add.text(200,590,test4_text1,fontStyle);
   zoomGroup.add(test4_1); 
   test4_2=this.game.add.text(300,590,test4_text2,fontStyle);
   zoomGroup.add(test4_2); 
   test4_3=this.game.add.text(1020,590,test4_text3,fontStyle);
   zoomGroup.add(test4_3); 
   //test4_4=this.game.add.text(1280,700,test4_text4,fontStyle);

   test5_1=this.game.add.text(200,770,test5_text1,fontStyle);
   zoomGroup.add(test5_1); 
   test5_2=this.game.add.text(300,770,test5_text2,fontStyle);
   zoomGroup.add(test5_2); 
   test5_3=this.game.add.text(1020,770,test5_text3,fontStyle);
   zoomGroup.add(test5_3); 
   //test5_4=this.game.add.text(1280,800,test5_text4,fontStyle); 
 },
 drawVLine:function(xp,yp,xp1,yp1)
  {
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0xffffff,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
      
       // lineGroup.add(line1);
       zoomGroup.add(line1);  
  }, 
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        //to go to next popup
        toNext:function(){
          this.removeTexts();
          previous_scene=theory_Scene;
          theory_Scene++;
        prev_btn.visible=true;
        if(theory_Scene>=4){
          next_btn.visible=false;
          play.visible=true;
        }
          this.showTheory();
          
        },
        toPrevious:function(){
          this.removeTexts();
          previous_scene=theory_Scene;
          theory_Scene--;
        next_btn.visible=true;
        if(theory_Scene<=1){
          prev_btn.visible=false;
        }
          this.showTheory();
        },
        removeTexts:function(){
          if(theory_Scene==3){
            zoomGroup.visible=false;
            procedure_text.visible=true;
          }else{
            procedure_step_text_1.destroy(true);
            procedure_step_text_2.destroy(true);
            procedure_step_text_3.destroy(true);
            procedure_step_text_4.destroy(true);
            procedure_step_text_5.destroy(true);
            procedure_step_text_6.destroy(true);
          }
          
        },
        showTheory:function(){
          console.log(theory_Scene);
          switch(theory_Scene){
            case 1:
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_1,fontStyle);
              procedure_step_text_2=this.game.add.text(260,380,procedure_step_2,fontStyle);
              procedure_step_text_3=this.game.add.text(260,460,procedure_step_3,fontStyle);
              procedure_step_text_4=this.game.add.text(260,540,procedure_step_4,fontStyle);
              procedure_step_text_5=this.game.add.text(260,620,procedure_step_5,fontStyle);
              procedure_step_text_6=this.game.add.text(260,750,procedure_step_6,fontStyle);
              
            break;
            case 2:
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_7,fontStyle);
              procedure_step_text_2=this.game.add.text(260,430,procedure_step_8,fontStyle);
              procedure_step_text_3=this.game.add.text(260,510,procedure_step_9,fontStyle);
              procedure_step_text_4=this.game.add.text(260,590,procedure_step_10,fontStyle);
              procedure_step_text_5=this.game.add.text(260,720,procedure_step_11,fontStyle);
              procedure_step_text_6=this.game.add.text(260,850,"",fontStyle);
              
          break;
          case 3:
            procedure_text.visible=false;
            zoomGroup.visible=true;
          break;
         case 4:
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_12,fontStyle);
              procedure_step_text_2=this.game.add.text(260,430,"",fontStyle);
              procedure_step_text_3=this.game.add.text(260,510,"",fontStyle);
              procedure_step_text_4=this.game.add.text(260,590,"",fontStyle);
              procedure_step_text_5=this.game.add.text(260,720,"",fontStyle);
              procedure_step_text_6=this.game.add.text(260,850,"",fontStyle);
              
          break;

          }
        },
     toZoomIn:function(){
      image.scale.x+=.04;
      image.scale.y+=.04;
      zoomOut_btn.visible=true;
      zoomed=true;
      if(image.scale.x>=.8){
        zoomIn_btn.visible=false;
      }
      xmax-=120;
      ymax-=80;
     },   
     toZoomOut:function(){
      image.scale.x-=.04;
      image.scale.y-=.04;
      zoomIn_btn.visible=true;
      if(image.x<200){
        image.x+=120;
        if(image.x>200){
          image.x=200;
        }
      }
      if(image.y<200){
        image.y+=80;
        if(image.y>200){
          image.y=200;
        }
      }
      if(image.scale.x<.48){
        zoomOut_btn.visible=false;
        zoomed=false;
        image.scale.x=.44;
        image.scale.y=.44;
        image.x=200;
        image.y=200;
     }
      xmax+=120;
      ymax+=80;
     }, 
     startDrag:function() {
       if(zoomed){
           imageDrag=true;
        //  You can't have a sprite being moved by physics AND input, so we disable the physics while being dragged
            currentx=this.game.input.activePointer.x;
            previousx=this.game.input.activePointer.x;
            currenty=this.game.input.activePointer.y;
            previousy=this.game.input.activePointer.y;
          }
        },
       stopDrag:function(){
        imageDrag=false;
       },
   update:function()
   {
    DeltaTime=this.game.time.elapsed/1000;
    this.imageGrpMove();

   },
   imageGrpMove:function(){
    var dist=0;
    if(zoomed){
      
      if(imageDrag){
        if(currentx>previousx){//  && image.x<xmin 
          dist=currentx-previousx;
          /////////////////////////
          image.x+=dist;
            /////////////rect.x-=dist/16.67;
             if(image.x>xmin){
               image.x=xmin;
             }
          
          previousx=currentx;  
        }else if(currentx<previousx){// && image.x>xmax
          dist=previousx-currentx;
          //////////////////////////////
          image.x-=dist;
            /////////////rect.x+=dist/16.67;
            if(image.x<xmax){
              image.x=xmax;
            }
          ////////////////////////////
          
          previousx=currentx;  
        }
        // /////////////////
        if(currenty>previousy){// && image.y<ymin
          dist=currenty-previousy;
          //////////////////////////
          image.y+=dist;
          ///////////rect.y-=dist/8;
          if(image.y>ymin){
            image.y=ymin;
          }
           previousy=currenty;   
        }else if(currenty<previousy){ // && image.y>ymax
            dist=previousy-currenty;
            image.y-=dist;
             /////////////rect.y+=dist/8;
            if(image.y<ymax){
              image.y=ymax;
              }
            previousy=currenty;
        }
        currentx=this.game.input.activePointer.x;
            //previousx=this.game.input.activePointer.x;
            currenty=this.game.input.activePointer.y;
            //previousy=this.game.input.activePointer.y;
      }
    }
   },
    //For to next scene   
   
    toLab_precautions:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
         // procedure_audio2.destroy();
         this.state.start("Lab_Precautions", true, false);
         
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    //procedure_audio1.destroy();
    //procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    //procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //procedure_audio1.destroy();
  //procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  