var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        
        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////plants//////////////////////////////////////////////////////////
this.game.load.image('bottle', 'assets/plants/bottle.png');
this.game.load.image('mark', 'assets/plants/mark.png');
this.game.load.image('arrow_1', 'assets/arrow.png');

this.game.load.atlasJSONHash('brassica_sprite', 'assets/plants/brassica_sprite.png', 'assets/plants/brassica_sprite.json');
this.game.load.atlasJSONHash('corn_sprite', 'assets/plants/corn_sprite.png', 'assets/plants/corn_sprite.json');
this.game.load.atlasJSONHash('arrow', 'assets/plants/arrow.png', 'assets/plants/arrow.json');

      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Procedure_2','assets/audio/Procedure_2.mp3');
      this.game.load.audio('Observation_1','assets/audio/Observation_1.mp3');
      this.game.load.audio('Observation_2','assets/audio/Observation_2.mp3');
      this.game.load.audio('Observation_3','assets/audio/Observation_3.mp3');
      this.game.load.audio('Observation_4','assets/audio/Observation_4.mp3');
      this.game.load.audio('Observation_5','assets/audio/Observation_5.mp3');
      this.game.load.audio('Result','assets/audio/Result.mp3');
      
      this.game.load.audio('step1_0','assets/audio/step1_0.mp3');
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');
      this.game.load.audio('step1_7','assets/audio/step1_7.mp3'); 
      this.game.load.audio('step1_8','assets/audio/step1_8.mp3');
      this.game.load.audio('step1_9','assets/audio/step1_9.mp3');
      this.game.load.audio('step1_10','assets/audio/step1_10.mp3');
      this.game.load.audio('step1_11','assets/audio/step1_11.mp3');
      this.game.load.audio('step1_12','assets/audio/step1_12.mp3');
      this.game.load.audio('step1_13','assets/audio/step1_13.mp3');
      this.game.load.audio('step1_14','assets/audio/step1_14.mp3');
      this.game.load.audio('step1_15','assets/audio/step1_15.mp3');   
      this.game.load.audio('step1_16','assets/audio/step1_16.mp3');
      this.game.load.audio('step1_17','assets/audio/step1_17.mp3');
      this.game.load.audio('step1_18','assets/audio/step1_18.mp3');
      this.game.load.audio('step1_19','assets/audio/step1_19.mp3');
      this.game.load.audio('step1_20','assets/audio/step1_20.mp3');
      this.game.load.audio('step1_21','assets/audio/step1_21.mp3');
      this.game.load.audio('step1_22','assets/audio/step1_22.mp3');
      this.game.load.audio('step1_23','assets/audio/step1_23.mp3'); 
      this.game.load.audio('step1_24','assets/audio/step1_24.mp3');
      this.game.load.audio('step1_25','assets/audio/step1_25.mp3');  
      this.game.load.audio('step1_26','assets/audio/step1_26.mp3'); 
      this.game.load.audio('step1_27','assets/audio/step1_27.mp3');
      this.game.load.audio('step1_28','assets/audio/step1_28.mp3'); 
      this.game.load.audio('step1_29','assets/audio/step1_29.mp3');

      this.game.load.audio('step2_0','assets/audio/step2_0.mp3');
      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step2_5','assets/audio/step2_5.mp3');
      this.game.load.audio('step2_6','assets/audio/step2_6.mp3');
      this.game.load.audio('step2_7','assets/audio/step2_7.mp3'); 
      this.game.load.audio('step2_8','assets/audio/step2_8.mp3');
      this.game.load.audio('step2_9','assets/audio/step2_9.mp3');
      this.game.load.audio('step2_10','assets/audio/step2_10.mp3');
      this.game.load.audio('step2_11','assets/audio/step2_11.mp3');
      this.game.load.audio('step2_12','assets/audio/step2_12.mp3');
      this.game.load.audio('step2_13','assets/audio/step2_13.mp3');
      
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_1");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
      //this.game.state.start("Observations_1");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

