var experiment_2 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_2.prototype = {
  	create: function(){

hotFlag=false;
bg= this.game.add.sprite(0, 0,'bg');


voice=this.game.add.audio("step2_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations_1", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    
    label_box1 = this.game.add.graphics(0, 0);
    label_box1.lineStyle(5,0xE67E22,.6);
    label_box1.beginFill(0xffffff,1);//9B59B6
    label_box1.drawRect(680, 240, 1100, 800);
    // label_1=this.game.add.text(910,808,"Muscle cell",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    corn=this.game.add.sprite(350,640,'corn_sprite','Corn.png');
    corn.anchor.set(.5);
    corn.scale.set(.475);

    bottle=this.game.add.sprite(350,560,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(1,.8);

    name_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    name_box1.beginFill(0xE67E22,1);//9B59B6
    name_box1.drawRect(200, 900, 300, 60);
    name_1=this.game.add.text(300,908,"Corn",lablel_fontStyle);//870,930

    corn_full=this.game.add.sprite(1000,650,'corn_sprite','Corn.png');
    corn_full.anchor.set(.5);
    corn_full.scale.set(.75);
    corn_full.alpha=.4;
    corn_fruit=this.game.add.sprite(1460,670,'corn_sprite','Corn_fruit.png');
    corn_fruit.anchor.set(.5);
    corn_fruit.scale.set(.7);
    corn_fruit.alpha=.5;

    Tessel=this.game.add.sprite(1010,348,'corn_sprite','Tessel.png');
    Tessel.anchor.set(.5);
    Tessel.scale.set(.75);
    Tessel.alpha=0.0;
    Tessel_text=this.game.add.text(700,275,"   Tessel or \n      male \ninflorescence",fontStyle_b);
    Tessel_text.visible=false;
    arrow_Tessel=this.game.add.sprite(870,350,'arrow_1');
    arrow_Tessel.visible=false;
    arrow_Tessel.scale.x=1.5;
    arrow_Tessel.alpha=.75;

    Stem=this.game.add.sprite(1010,758,'corn_sprite','Stem.png');
    Stem.anchor.set(.5);
    Stem.scale.set(.75);
    Stem.alpha=0.0;
    Stem_text=this.game.add.text(810,760,"Stem",fontStyle_b);
    Stem_text.visible=false;
    arrow_Stem=this.game.add.sprite(906,775,'arrow_1');
    arrow_Stem.visible=false;
    arrow_Stem.scale.x=2;
    arrow_Stem.alpha=.75;

    Leaf=this.game.add.sprite(913,723,'corn_sprite','Leaf.png');
    Leaf.anchor.set(.5);
    Leaf.scale.set(.75);
    Leaf.alpha=0.0;
    Leaf_text=this.game.add.text(750,665,"Leaf",fontStyle_b);
    Leaf_text.visible=false;
    arrow_Leaf=this.game.add.sprite(826,680,'arrow_1');
    arrow_Leaf.visible=false;
    arrow_Leaf.scale.x=1.5;
    arrow_Leaf.alpha=.75;

    Cob=this.game.add.sprite(1070,440,'corn_sprite','Cob.png');
    Cob.anchor.set(.5);
    Cob.scale.set(.75);
    Cob.alpha=0.0;
    Cob_text=this.game.add.text(1120,270,"Cob or female \ninflorescence",fontStyle_b);
    Cob_text.visible=false;
    arrow_Cob=this.game.add.sprite(1165,380,'arrow_1');
    arrow_Cob.visible=false;
    arrow_Cob.angle=140;
    arrow_Cob.scale.x=2;
    arrow_Cob.alpha=.75;

    Root_System=this.game.add.sprite(1007,953,'corn_sprite','Root_System.png');
    Root_System.anchor.set(.5);
    Root_System.scale.set(.75);
    Root_System.alpha=0.0;
    Root_System_text=this.game.add.text(770,900,"  Root \nsystem",fontStyle_b);
    Root_System_text.visible=false;
    arrow_Root_System=this.game.add.sprite(870,940,'arrow_1');
    arrow_Root_System.visible=false;
    arrow_Root_System.scale.x=1.5;
    arrow_Root_System.alpha=.75;

    Silky_style=this.game.add.sprite(1445,437,'corn_sprite','Silky_style.png');
    Silky_style.anchor.set(.5);
    Silky_style.scale.set(.7);
    Silky_style.alpha=0.0;
    Silky_style_text=this.game.add.text(1560,290,"Silky style of \n  individual \n   pistillate \n    flowers \n   (Female)",fontStyle_b);
    Silky_style_text.visible=false;
    arrow_Silky_style=this.game.add.sprite(1580,410,'arrow_1');
    arrow_Silky_style.visible=false;
    arrow_Silky_style.scale.x=-1.5;
    arrow_Silky_style.alpha=.75;

    Grains=this.game.add.sprite(1394,610,'corn_sprite','Grains.png');
    Grains.anchor.set(.5);
    Grains.scale.set(.7);
    Grains.alpha=0.0;
    Grains_text=this.game.add.text(1565,595,"Grains",fontStyle_b);
    Grains_text.visible=false;
    arrow_Grains=this.game.add.sprite(1550,610,'arrow_1');
    arrow_Grains.visible=false;
    arrow_Grains.scale.x=-1.5;
    arrow_Grains.alpha=.75;
    

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,5, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's study of monocot plant.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=45;

    // darkBg = this.game.add.graphics(0, 0);
    // //darkBg.lineStyle(5,0xE67E22,.6);
    // darkBg.beginFill(0x000000,1);//9B59B6
    // darkBg.drawRect(0, 0, 1920, 1080);
    // darkBg.alpha=0;
    // darkBg.visible=false;
    
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_1",1);
    voice.play();
    dialog.text="Click on the Tassels of the maize plant.";
    Tessel.inputEnabled = true;
    Tessel.input.useHandCursor = true;
    Tessel.events.onInputDown.add(this.Exp_Maize_Tessel, this);
    arrow.x=Tessel.x-30;
    arrow.y=Tessel.y-50;
    //arrow.angle=-90;
    arrow.visible=true;
  },
  Exp_Maize_Tessel:function(){
    arrow.visible=false;
    Tessel.inputEnabled = false;
    Tessel.input.useHandCursor = false;
    tween1=this.game.add.tween(Tessel).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Tessel_text.visible=true;
      arrow_Tessel.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_2",1);
      voice.play();
      dialog.y=45;
      dialog.text="Maize tassels are the male flowers of maize plants.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Maize_Leaf, this);
    }.bind(this));  
  },
  to_Maize_Leaf:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_3",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the leaf of the maize plant.";
    Leaf.inputEnabled = true;
    Leaf.input.useHandCursor = true;
    Leaf.events.onInputDown.add(this.Exp_Maize_Leaf, this);
    arrow.angle=0;
    arrow.x=Leaf.x-30;
    arrow.y=Leaf.y-50;
    arrow.visible=true;
  },
  Exp_Maize_Leaf:function(){
    arrow.visible=false;
    Leaf.inputEnabled = false;
    Leaf.input.useHandCursor = false;
    tween1=this.game.add.tween(Leaf).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Leaf_text.visible=true;
      arrow_Leaf.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_4",1);
      voice.play();
      dialog.y=20;
      dialog.text="Leaves are present on the nodes and are elongated with parallel \nvenation.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Maize_Stem, this);
    }.bind(this));  
  },
  to_Maize_Stem:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_5",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the stem of the maize plant.";
    Stem.inputEnabled = true;
    Stem.input.useHandCursor = true;
    Stem.events.onInputDown.add(this.Exp_Maize_Stem, this);
    arrow.angle=90;
    arrow.x=Stem.x+40;
    arrow.y=Stem.y-40;
    arrow.visible=true;
  },
  Exp_Maize_Stem:function(){
    arrow.visible=false;
    Stem.inputEnabled = false;
    Stem.input.useHandCursor = false;
    tween1=this.game.add.tween(Stem).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Stem_text.visible=true;
      arrow_Stem.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_6",1);
      voice.play();
      dialog.y=20;
      dialog.text="The stem is soft, green, herbaceous, erect with many nodes and \ninternodes.";
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.to_Maize_Cob, this);
    }.bind(this));  
  },
  to_Maize_Cob:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_7",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the cob of the maize plant.";
    Cob.inputEnabled = true;
    Cob.input.useHandCursor = true;
    Cob.events.onInputDown.add(this.Exp_Maize_Cob, this);
    arrow.angle=90;
    arrow.x=Cob.x+60;
    arrow.y=Cob.y-20;
    arrow.visible=true;
  },
  Exp_Maize_Cob:function(){
    arrow.visible=false;
    Cob.inputEnabled = false;
    Cob.input.useHandCursor = false;
    tween1=this.game.add.tween(Cob).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Cob_text.visible=true;
      arrow_Cob.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_8",1);
      voice.play();
      dialog.y=45;
      dialog.text="Maize cob is the female part of the plant.";
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.to_Maize_Silky_style, this);
    }.bind(this));  
  },
  to_Maize_Silky_style:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_9",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the corn silk of the maize plant.";
    Silky_style.inputEnabled = true;
    Silky_style.input.useHandCursor = true;
    Silky_style.events.onInputDown.add(this.Exp_Maize_Silky_style, this);
    arrow.angle=0;
    arrow.x=Silky_style.x;
    arrow.y=Silky_style.y-70;
    arrow.visible=true;
  },
  Exp_Maize_Silky_style:function(){
    arrow.visible=false;
    Silky_style.inputEnabled = false;
    Silky_style.input.useHandCursor = false;
    tween1=this.game.add.tween(Silky_style).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Silky_style_text.visible=true;
      arrow_Silky_style.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_10",1);
      voice.play();
      dialog.y=20;
      dialog.text="Corn silk is a common Stigma, the shiny, thread-like, weak fibres that \ngrow as part of ears of corn.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Maize_Grains, this);
    }.bind(this));  
  },
  to_Maize_Grains:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_11",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the grains of the maize plant.";
    Grains.inputEnabled = true;
    Grains.input.useHandCursor = true;
    Grains.events.onInputDown.add(this.Exp_Maize_Grains, this);
    arrow.angle=90;
    arrow.x=Grains.x+120;
    arrow.y=Grains.y;
    arrow.visible=true;
  },
  Exp_Maize_Grains:function(){
    arrow.visible=false;
    Grains.inputEnabled = false;
    Grains.input.useHandCursor = false;
    tween1=this.game.add.tween(Grains).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Grains_text.visible=true;
      arrow_Grains.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_12",1);
      voice.play();
      dialog.y=45;
      dialog.text="Corn that is harvested when fully mature and dry is considered a grain.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.NextExperiment, this);
    }.bind(this));  
  },
  NextExperiment:function(){
    Tessel.visible=false;
    Leaf.visible=false;
    Stem.visible=false;
    Cob.visible=false;
    Root_System.visible=false;
    Silky_style.visible=false;
    Grains.visible=false;
    corn_full.alpha=1;
    corn_fruit.alpha=1;
    voice.destroy(true);
    voice=this.game.add.audio("step2_13",1);
    voice.play();
    dialog.y=20;
    dialog.text="Click on the next button to see the external features of the monocot \nplant.";
    play.visible=true;
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    //this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_2",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
