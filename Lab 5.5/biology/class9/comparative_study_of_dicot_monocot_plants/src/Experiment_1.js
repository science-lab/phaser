var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;
bg= this.game.add.sprite(0, 0,'bg');


voice=this.game.add.audio("step1_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    
    label_box1 = this.game.add.graphics(0, 0);
    label_box1.lineStyle(5,0xE67E22,.6);
    label_box1.beginFill(0xffffff,1);//9B59B6
    label_box1.drawRect(680, 240, 1100, 800);
    // label_1=this.game.add.text(910,808,"Muscle cell",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    brassica=this.game.add.sprite(350,640,'brassica_sprite','full.png');
    brassica.anchor.set(.5);
    brassica.scale.set(.475);

    bottle=this.game.add.sprite(350,560,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(1,.8);

    name_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    name_box1.beginFill(0xE67E22,1);//9B59B6
    name_box1.drawRect(200, 900, 300, 60);
    name_1=this.game.add.text(280,908,"Brassica",lablel_fontStyle);//870,930

    brassica_full=this.game.add.sprite(1250,650,'brassica_sprite','full.png');
    brassica_full.anchor.set(.5);
    brassica_full.scale.set(.8);
    brassica_full.alpha=.3;
    
    stem=this.game.add.sprite(1190,600,'brassica_sprite','stem.png');
    stem.anchor.set(.5);
    stem.scale.set(.8);
    stem.alpha=0.0;
    stem_text=this.game.add.text(1270,455,"Stem",fontStyle_b);
    stem_text.visible=false;
    arrow_stem=this.game.add.sprite(1258,470,'arrow_1');
    arrow_stem.visible=false;
    arrow_stem.angle=-30;
    arrow_stem.scale.x=-2;
    arrow_stem.alpha=.75;

    terminal_bud=this.game.add.sprite(1173,300,'brassica_sprite','terminal_bud.png');
    terminal_bud.anchor.set(.5);
    terminal_bud.scale.set(.8);
    terminal_bud.alpha=0.0;
    terminal_bud_text=this.game.add.text(1280,265,"Terminal bud",fontStyle_b);
    terminal_bud_text.visible=false;
    arrow_terminal_bud=this.game.add.sprite(1280,276,'arrow_1');
    arrow_terminal_bud.visible=false;
    arrow_terminal_bud.scale.x=-2;
    arrow_terminal_bud.alpha=.75;

    flower=this.game.add.sprite(1137,367,'brassica_sprite','flower.png');
    flower.anchor.set(.5);
    flower.scale.set(.8);
    flower.alpha=0.0;
    flower_text=this.game.add.text(1260,330,"Flower",fontStyle_b);
    flower_text.visible=false;
    arrow_flower=this.game.add.sprite(1250,340,'arrow_1');
    arrow_flower.visible=false;
    arrow_flower.scale.x=-2;
    arrow_flower.alpha=.75;
    
    fruit_right=this.game.add.sprite(1225,433,'brassica_sprite','fruit_right.png');
    fruit_right.anchor.set(.5);
    fruit_right.scale.set(.8);
    fruit_right.alpha=0.0;
    fruit_right_text=this.game.add.text(1350,400,"Fruit",fontStyle_b);
    fruit_right_text.visible=false;
    arrow_fruit_right=this.game.add.sprite(1340,410,'arrow_1');
    arrow_fruit_right.visible=false;
    arrow_fruit_right.scale.x=-2;
    arrow_fruit_right.alpha=.75;

    leaf_lamina=this.game.add.sprite(1365,705,'brassica_sprite','leaf_lamina.png');
    leaf_lamina.anchor.set(.5);
    leaf_lamina.scale.set(.8);
    leaf_lamina.alpha=0.0;
    leaf_lamina_text=this.game.add.text(1470,720,"Leaf \nlamina",fontStyle_b);
    leaf_lamina_text.visible=false;
    

    midrib=this.game.add.sprite(1365,705,'brassica_sprite','midrib.png');
    midrib.anchor.set(.5);
    midrib.scale.set(.8);
    midrib.alpha=0.0;
    midrib_text=this.game.add.text(1440,600,"Midrib",fontStyle_b);
    midrib_text.visible=false;
    
    vien=this.game.add.sprite(1365,705,'brassica_sprite','vien.png');
    vien.anchor.set(.5);
    vien.scale.set(.8);
    vien.alpha=0.0;
    vien_text=this.game.add.text(1485,650,"Vein",fontStyle_b);
    vien_text.visible=false;
    arrow_vien=this.game.add.sprite(1475,660,'arrow_1');
    arrow_vien.visible=false;
    arrow_vien.angle=-20;
    arrow_vien.scale.x=-1.5;
    arrow_vien.alpha=.75;

    arrow_midrib=this.game.add.sprite(1425,630,'arrow_1');
    arrow_midrib.visible=false;
    arrow_midrib.angle=-35;
    arrow_midrib.scale.x=-2;
    arrow_midrib.alpha=.75;
    arrow_leaf_lamina=this.game.add.sprite(1470,760,'arrow_1');
    arrow_leaf_lamina.visible=false;
    arrow_leaf_lamina.angle=35;
    arrow_leaf_lamina.scale.x=-1.5;
    arrow_leaf_lamina.alpha=.75;
        
    node=this.game.add.sprite(1178,842,'brassica_sprite','node.png');
    node.anchor.set(.5);
    node.scale.set(.8);
    node.alpha=0.0;
    node_text=this.game.add.text(1275,830,"Node",fontStyle_b);
    node_text.visible=false;
    arrow_node=this.game.add.sprite(1270,845,'arrow_1');
    arrow_node.visible=false;
    arrow_node.angle=8;
    arrow_node.scale.x=-2;
    arrow_node.alpha=.75;

    petiol=this.game.add.sprite(1240,741,'brassica_sprite','petiol.png');
    petiol.anchor.set(.5);
    petiol.scale.set(.8);
    petiol.alpha=0.0;
    petiol_text=this.game.add.text(1330,780,"Petiole",fontStyle_b);
    petiol_text.visible=false;
    arrow_petiol=this.game.add.sprite(1330,790,'arrow_1');
    arrow_petiol.visible=false;
    arrow_petiol.angle=40;
    arrow_petiol.scale.x=-2;
    arrow_petiol.alpha=.75;

    

    Axillary_bud_text=this.game.add.text(1230,520,"Axillary bud",fontStyle_b);
    Axillary_bud_text.visible=false;
    arrow_Axillary_bud=this.game.add.sprite(1215,543,'arrow_1');
    arrow_Axillary_bud.visible=false;
    arrow_Axillary_bud.angle=-55;
    arrow_Axillary_bud.scale.x=-1.5;
    arrow_Axillary_bud.alpha=.75
    

    Internode_text=this.game.add.text(1250,575,"Internode",fontStyle_b);
    Internode_text.visible=false;
    arrow_Internode=this.game.add.sprite(1250,610,'arrow_1');
    arrow_Internode.visible=false;
    arrow_Internode.angle=-20;
    arrow_Internode.scale.x=-1.5;
    arrow_Internode.alpha=.75;

    mark1=this.drawVLine(1588,260,1588,875);
    mark1.visible=false;
    mark1_1=this.drawVLine(1570,260,1590,260);
    mark1_1.visible=false;
    mark1_2=this.drawVLine(1570,875,1590,875);
    mark1_2.visible=false;
    Shoot_system_text=this.game.add.text(1600,520," Shoot \nsystem",fontStyle_b);
    Shoot_system_text.visible=false;
    

    primary_root=this.game.add.sprite(1203,952,'brassica_sprite','primary_root.png');
    primary_root.anchor.set(.5);
    primary_root.scale.set(.8);
    primary_root.alpha=0.0;
    primary_root_text=this.game.add.text(890,815,"Primary root",fontStyle_b);
    primary_root_text.visible=false;
    arrow_primary_root=this.game.add.sprite(1102,840,'arrow_1');
    arrow_primary_root.visible=false;
    arrow_primary_root.angle=25;
    arrow_primary_root.scale.x=2;
    arrow_primary_root.alpha=.75;

    secondary_root=this.game.add.sprite(1203,952,'brassica_sprite','secondary_root.png');
    secondary_root.anchor.set(.5);
    secondary_root.scale.set(.8);
    secondary_root.alpha=0.0;
    secondary_root_text=this.game.add.text(890,865,"Secondary \n   root",fontStyle_b);
    secondary_root_text.visible=false;
    arrow_secondary_root=this.game.add.sprite(1065,883,'arrow_1');
    arrow_secondary_root.visible=false;
    arrow_secondary_root.angle=10;
    arrow_secondary_root.scale.x=2;
    arrow_secondary_root.alpha=.75;

    teritiary_root=this.game.add.sprite(1203,952,'brassica_sprite','teritiary_root.png');
    teritiary_root.anchor.set(.5);
    teritiary_root.scale.set(.8);
    teritiary_root.alpha=0.0;
    teritiary_root_text=this.game.add.text(875,980,"Tertiary root",fontStyle_b);
    teritiary_root_text.visible=false;
    arrow_teritiary_root=this.game.add.sprite(1080,993,'arrow_1');
    arrow_teritiary_root.visible=false;
    arrow_teritiary_root.angle=-40;
    arrow_teritiary_root.scale.x=1.5;
    arrow_teritiary_root.alpha=.75;
    arrow_teritiary_root=this.game.add.sprite(1085,990,'arrow_1');
    arrow_teritiary_root.visible=false;
    arrow_teritiary_root.scale.x=2;
    arrow_teritiary_root.alpha=.75;

    mark2=this.drawVLine(860,810,860,1025);
    mark2.visible=false;
    mark2_1=this.drawVLine(858,810,878,810);
    mark2_1.visible=false;
    mark2_2=this.drawVLine(858,1025,878,1025);
    mark2_2.visible=false;
    Root_system_text=this.game.add.text(730,870,"  Root \nsystem",fontStyle_b);
    Root_system_text.visible=false;
    

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,5, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's study of dicot plant.";
    this.game.time.events.add(Phaser.Timer.SECOND*2,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=45;

    // darkBg = this.game.add.graphics(0, 0);
    // //darkBg.lineStyle(5,0xE67E22,.6);
    // darkBg.beginFill(0x000000,1);//9B59B6
    // darkBg.drawRect(0, 0, 1920, 1080);
    // darkBg.alpha=0;
    // darkBg.visible=false;
    
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_1",1);
    voice.play();
    dialog.text="Click on the terminal bud of the brassica plant.";
    terminal_bud.inputEnabled = true;
    terminal_bud.input.useHandCursor = true;
    terminal_bud.events.onInputDown.add(this.Exp_Brassica_terminal_bud, this);
    arrow.x=terminal_bud.x;
    arrow.y=terminal_bud.y-50;
    //arrow.angle=-90;
    arrow.visible=true;
  },
  Exp_Brassica_terminal_bud:function(){
    arrow.visible=false;
    terminal_bud.inputEnabled = false;
    terminal_bud.input.useHandCursor = false;
    tween1=this.game.add.tween(terminal_bud).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      terminal_bud_text.visible=true;
      arrow_terminal_bud.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_2",1);
      voice.play();
      dialog.y=20;
      dialog.text="A terminal bud grows at the tip of a shoot and causes the shoot to \ngrow longer.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Brassica_flower, this);
    }.bind(this));  
  },
  to_Brassica_flower:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_3",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the flower of the brassica plant.";
    flower.inputEnabled = true;
    flower.input.useHandCursor = true;
    flower.events.onInputDown.add(this.Exp_Brassica_flower, this);
    arrow.angle=0;
    arrow.x=flower.x;
    arrow.y=flower.y-70;
    arrow.visible=true;
  },
  Exp_Brassica_flower:function(){
    arrow.visible=false;
    flower.inputEnabled = false;
    flower.input.useHandCursor = false;
    tween1=this.game.add.tween(flower).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      flower_text.visible=true;
      arrow_flower.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_4",1);
      voice.play();
      dialog.y=45;
      dialog.text="Flowers are borne on the stem and are bright yellow in colour.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Brassica_fruit, this);
    }.bind(this));  
  },
  to_Brassica_fruit:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_5",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the fruit of the brassica plant.";
    fruit_right.inputEnabled = true;
    fruit_right.input.useHandCursor = true;
    fruit_right.events.onInputDown.add(this.Exp_Brassica_fruit, this);
    arrow.angle=90;
    arrow.x=fruit_right.x+50;
    arrow.y=fruit_right.y;
    arrow.visible=true;
  },
  Exp_Brassica_fruit:function(){
    arrow.visible=false;
    fruit_right.inputEnabled = false;
    fruit_right.input.useHandCursor = false;
    tween1=this.game.add.tween(fruit_right).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      fruit_right_text.visible=true;
      arrow_fruit_right.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_6",1);
      voice.play();
      dialog.y=45;
      dialog.text="Seeds are enclosed inside the fruit and have two cotyledons.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Brassica_midrib, this);
    }.bind(this));  
  },
  to_Brassica_midrib:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_7",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the midrib of the brassica plant.";
    midrib.inputEnabled = true;
    midrib.input.useHandCursor = true;
    midrib.events.onInputDown.add(this.Exp_Brassica_midrib, this);
    arrow.angle=0;
    arrow.x=midrib.x;
    arrow.y=midrib.y-50;
    arrow.visible=true;
  },
  Exp_Brassica_midrib:function(){
    arrow.visible=false;
    midrib.inputEnabled = false;
    midrib.input.useHandCursor = false;
    tween1=this.game.add.tween(midrib).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      midrib_text.visible=true;
      arrow_midrib.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_8",1);
      voice.play();
      dialog.y=45;
      dialog.text="The midrib is the central vein of a leaf.";
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.to_Brassica_vien, this);
    }.bind(this));  
  },
  to_Brassica_vien:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_9",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the corn vein of the brassica plant.";
    vien.inputEnabled = true;
    vien.input.useHandCursor = true;
    vien.events.onInputDown.add(this.Exp_Brassica_vien, this);
    arrow.angle=45;
    arrow.x=vien.x+90;
    arrow.y=vien.y-50;
    arrow.visible=true;
  },
  Exp_Brassica_vien:function(){
    arrow.visible=false;
    vien.inputEnabled = false;
    vien.input.useHandCursor = false;
    tween1=this.game.add.tween(vien).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      vien_text.visible=true;
      arrow_vien.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_10",1);
      voice.play();
      dialog.y=45;
      dialog.text="The veins of the leaf have a net-like appearance as reticulate venation.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Brassica_leaf_lamina, this);
    }.bind(this));  
  },
  to_Brassica_leaf_lamina:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_11",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the leaf lamina of the brassica plant.";
    leaf_lamina.inputEnabled = true;
    leaf_lamina.input.useHandCursor = true;
    leaf_lamina.events.onInputDown.add(this.Exp_Brassica_leaf_lamina, this);
    arrow.angle=120;
    arrow.x=leaf_lamina.x+60;
    arrow.y=leaf_lamina.y+40;
    arrow.visible=true;
  },
  Exp_Brassica_leaf_lamina:function(){
    arrow.visible=false;
    leaf_lamina.inputEnabled = false;
    leaf_lamina.input.useHandCursor = false;
    tween1=this.game.add.tween(leaf_lamina).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      leaf_lamina_text.visible=true;
      arrow_leaf_lamina.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_12",1);
      voice.play();
      dialog.y=20;
      dialog.text="The lamina is the expanded, flat component of the leaf that contains \nthe chloroplasts.";
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.to_Brassica_node, this);
    }.bind(this));  
  },
  to_Brassica_node:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_13",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the node of the brassica plant.";
    node.inputEnabled = true;
    node.input.useHandCursor = true;
    node.events.onInputDown.add(this.Exp_Brassica_node, this);
    arrow.angle=-90;
    arrow.x=node.x-50;
    arrow.y=node.y+5;
    arrow.visible=true;
  },
  Exp_Brassica_node:function(){
    arrow.visible=false;
    node.inputEnabled = false;
    node.input.useHandCursor = false;
    tween1=this.game.add.tween(node).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      node_text.visible=true;
      arrow_node.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_14",1);
      voice.play();
      dialog.y=45;
      dialog.text="A node is a location on a stem to which a leaf or branch is attached.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Brassica_petiol, this);
    }.bind(this));  
  },
  to_Brassica_petiol:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_15",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the petiole of the brassica plant.";
    petiol.inputEnabled = true;
    petiol.input.useHandCursor = true;
    petiol.events.onInputDown.add(this.Exp_Brassica_petiol, this);
    arrow.angle=0;
    arrow.x=petiol.x;
    arrow.y=petiol.y-40;
    arrow.visible=true;
  },
  Exp_Brassica_petiol:function(){
    arrow.visible=false;
    petiol.inputEnabled = false;
    petiol.input.useHandCursor = false;
    tween1=this.game.add.tween(petiol).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      petiol_text.visible=true;
      arrow_petiol.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_16",1);
      voice.play();
      dialog.y=45;
      dialog.text="A petiole attaches the leaf to the stem.";
      this.game.time.events.add(Phaser.Timer.SECOND*3,this.to_Brassica_stem, this);
    }.bind(this));  
  },
  to_Brassica_stem:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_17",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the stem of the brassica plant.";
    stem.inputEnabled = true;
    stem.input.useHandCursor = true;
    stem.events.onInputDown.add(this.Exp_Brassica_stem, this);
    arrow.angle=-90;
    arrow.x=stem.x-45;
    arrow.y=530;
    arrow.visible=true;
  },
  Exp_Brassica_stem:function(){
    arrow.visible=false;
    stem.inputEnabled = false;
    stem.input.useHandCursor = false;
    tween1=this.game.add.tween(stem).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      stem_text.visible=true;
      arrow_stem.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_18",1);
      voice.play();
      dialog.y=45;
      dialog.text="Stem is soft, green, herbaceous, erect with many nodes and internodes.";
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.Exp_Brassica_Internode, this);
    }.bind(this));  
  },
  Exp_Brassica_Internode:function(){
    arrow.visible=false;
    Internode_text.visible=true;
    arrow_Internode.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step1_19",1);
    voice.play();
    dialog.y=45;
    dialog.text="Internode is the part of a plant stem between two nodes.";
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.Exp_Axillary_bud, this);
  },
  Exp_Axillary_bud:function(){
    arrow.visible=false;
    Axillary_bud_text.visible=true;
    arrow_Axillary_bud.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step1_29",1);
    voice.play();
    dialog.y=20;
    dialog.text="An axillary bud is the precursor of a branch or lateral shoot, which is \nformed at the junction between a leaf and the stem.";
    this.game.time.events.add(Phaser.Timer.SECOND*8,this.Exp_shoot_system, this);
  },
  Exp_shoot_system:function(){
    mark1.visible=true;
    mark1_1.visible=true;
    mark1_2.visible=true;
    Shoot_system_text.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step1_20",1);
    voice.play();
    dialog.y=20;
    dialog.text="The shoot system refers to the aerial and erect part of the plant body \nwhich grows upwards.";
    this.game.time.events.add(Phaser.Timer.SECOND*6,this.to_Brassica_primary_root, this);
  },
  to_Brassica_primary_root:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_21",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the primary root of the brassica plant.";
    primary_root.inputEnabled = true;
    primary_root.input.useHandCursor = true;
    primary_root.events.onInputDown.add(this.Exp_Brassica_primary_root, this);
    arrow.angle=-60;
    arrow.x=primary_root.x-50;
    arrow.y=primary_root.y-70;
    arrow.visible=true;
  },
  Exp_Brassica_primary_root:function(){
    arrow.visible=false;
    primary_root.inputEnabled = false;
    primary_root.input.useHandCursor = false;
    tween1=this.game.add.tween(primary_root).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      primary_root_text.visible=true;
      arrow_primary_root.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_22",1);
      voice.play();
      dialog.y=45;
      dialog.text="The root formed from the radicle is called the taproot or primary root.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Brassica_secondary_root, this);
    }.bind(this));  
  },
  to_Brassica_secondary_root:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_23",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the secondary root of the brassica plant.";
    secondary_root.inputEnabled = true;
    secondary_root.input.useHandCursor = true;
    secondary_root.events.onInputDown.add(this.Exp_Brassica_secondary_root, this);
    arrow.angle=-60;
    arrow.x=secondary_root.x-70;
    arrow.y=secondary_root.y-60;
    arrow.visible=true;
  },
  Exp_Brassica_secondary_root:function(){
    arrow.visible=false;
    secondary_root.inputEnabled = false;
    secondary_root.input.useHandCursor = false;
    tween1=this.game.add.tween(secondary_root).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      secondary_root_text.visible=true;
      arrow_secondary_root.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_24",1);
      voice.play();
      dialog.y=45;
      dialog.text="Secondary roots are the side branches of the primary roots.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Brassica_teritiary_root, this);
    }.bind(this));  
  },
  to_Brassica_teritiary_root:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_25",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the tertiary root of the brassica plant.";
    teritiary_root.inputEnabled = true;
    teritiary_root.input.useHandCursor = true;
    teritiary_root.events.onInputDown.add(this.Exp_Brassica_teritiary_root, this);
    arrow.angle=-90;
    arrow.x=teritiary_root.x-60;
    arrow.y=teritiary_root.y+54;
    arrow.visible=true;
  },
  Exp_Brassica_teritiary_root:function(){
    arrow.visible=false;
    teritiary_root.inputEnabled = false;
    teritiary_root.input.useHandCursor = false;
    tween1=this.game.add.tween(teritiary_root).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      teritiary_root_text.visible=true;
      arrow_teritiary_root.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_26",1);
      voice.play();
      dialog.y=45;
      dialog.text="Tertiary roots are formed by secondary roots in turn.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.Exp_root_system, this);
    }.bind(this));  
  },
  Exp_root_system:function(){
    mark2.visible=true;
    mark2_1.visible=true;
    mark2_2.visible=true;
    Root_system_text.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step1_27",1);
    voice.play();
    dialog.y=20;
    dialog.text="The root system refers to the parts of a plant that generally grow \nbeneath the ground, absorbing water and minerals.";
    this.game.time.events.add(Phaser.Timer.SECOND*8,this.NextExperiment, this);
  },
  NextExperiment:function(){
    terminal_bud.visible=false;
    flower.visible=false;
    stem.visible=false;
    node.visible=false;
    fruit_right.visible=false;
    leaf_lamina.visible=false;
    midrib.visible=false;
    vien.visible=false;
    petiol.visible=false;
    primary_root.visible=false;
    secondary_root.visible=false;
    teritiary_root.visible=false;
    brassica_full.alpha=1;
    voice.destroy(true);
    voice=this.game.add.audio("step1_28",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the next button to see the external features of dicot plant.";
    play.visible=true;
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
    line1.lineStyle(5,0x000000,.75);
    line1.moveTo(xp, yp);
    line1.lineTo(xp1, yp1);
    //lineGroup.add(line1);
    return(line1);
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    //this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
