<!doctype html>
<html lang="en">
<head>
  <link rel="stylesheet" href="fonts/SegoeUI/stylesheet.css" type="text/css" charset="utf-8" />
<link rel="stylesheet" href="css/stylesheet.css" type="text/css" charset="utf-8" />
<script type="text/javascript" src="phaser.min.js"></script>
<script src ="src/Boot.js"></script>
<script src ="src/Preload.js"></script>
<script src ="src/Aim.js"></script>
<script src ="src/Exp_Selection.js"></script>
<script src ="src/Materials.js"></script>
<script src ="src/Theory.js"></script>
<script src ="src/Theory_1.js"></script>
<script src ="src/Theory_2.js"></script>
<script src ="src/Lab_Precautions.js"></script>
<script src ="src/Procedure_1.js"></script>
<script src ="src/Procedure_2.js"></script>
<script src ="src/Experiment_1.js"></script>
<script src ="src/Experiment_2.js"></script>
<script src ="src/Experiment_3.js"></script>
<script src ="src/Experiment_4.js"></script>

<script src ="src/Observations.js"></script>
<script src ="src/Observations_1.js"></script>
<script src ="src/Observations_2.js"></script>
<script src ="src/Observations_3.js"></script>


<script src ="src/Result.js"></script>
<script src ="src/Result_1.js"></script>
<script src ="src/Result_2.js"></script>
<script src ="src/Result_3.js"></script>


<script src ="src/Viva.js"></script>





<style type="text/css">
        body{
             margin:0
            }
        /* .pageOverlay {
          background-color: #000000;
          background-image: url(https://scienceapp.in/swadhyaya/content/loading.gif);
          background-position: center;
          background-repeat: no-repeat;
            top: 0;
            left: 0;
            position: absolute;
            height: 100%;
            width: 100%;
            z-index: 1001;
            display: block;
        } */

</style>
    <!-- <script src="https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/js/jquery.min.js"></script>
<script type="text/javascript">
  jQuery(window).load(function () {
    $("#loadingOverlay").fadeOut("fast");
});
</script> -->
<!-- <script>
    localStorage.setItem('SIM_ID', '<?php echo $_GET['sim_id'];?>');
     </script> -->
    
</head>
<body>
   <!-- <div id="loadingOverlay" class="pageOverlay"></div> -->
    <div id="fontPlaceholder"></div>
     <div id="game"></div>
  <div id="orientation"></div>
    <script>
       (function (){
           var game = new Phaser.Game(1922,1081, Phaser.CANVAS, "game");
           //var game = new Phaser.Game(1200, 650, Phaser.CANVAS, "game");
           game.state.add("Boot",boot);
           game.state.add("Preload",preload);
           game.state.add("Aim",aim);
           game.state.add("Exp_Selection",exp_selection);
           game.state.add("Materials",materials);
           game.state.add("Theory",theory);
           game.state.add("Theory_1",theory_1);
           game.state.add("Theory_2",theory_2);
           game.state.add("Lab_Precautions",lab_precautions);
           game.state.add("Procedure_1",procedure_1);
           game.state.add("Procedure_2",procedure_2);
           game.state.add("Experiment_1",experiment_1);
           game.state.add("Experiment_2",experiment_2);
           game.state.add("Experiment_3",experiment_3);
           game.state.add("Experiment_4",experiment_4);
           
           game.state.add("Observations",observations);
           game.state.add("Observations_1",observations_1);
           game.state.add("Observations_2",observations_2);
           game.state.add("Observations_3",observations_3);
           
           game.state.add("Result",result);
           game.state.add("Result_1",result_1);
           game.state.add("Result_2",result_2);
           game.state.add("Result_3",result_3);
           
           game.state.add("Viva",viva);
           
          
         /*  game.state.add("Summary",summary);//*/
          
           game.state.start("Boot");
       })();    
    </script>
</body>
</html>