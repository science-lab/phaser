var experiment_4 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_4.prototype = {
  	create: function(){

hotFlag=false;
bg= this.game.add.sprite(0, 0,'bg');

// bgBlack= this.game.add.sprite(0,0,'bg_black');
// bgBlack.scale.setTo(1,1);
// bgBlack.alpha=0;
// bgBlack.visible=false;



// circle1 = this.game.add.graphics(0, 0);
// ////circle1.lineStyle(8, 0xffd900, 1);
// circle1.beginFill(0xFF0000, .3);
// circle1.drawCircle(902, 494, 430);
// //circle1.scale.set(0.5);
// Muscle_tissue.mask=circle1;
// circle1.visible=false;


voice=this.game.add.audio("step4_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations_3", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    
    label_box1 = this.game.add.graphics(0, 0);
    label_box1.lineStyle(5,0xE67E22,.6);
    label_box1.beginFill(0xffffff,1);//9B59B6
    label_box1.drawRect(780, 240, 1000, 800);
    // label_1=this.game.add.text(910,808,"Muscle cell",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    Pigeon=this.game.add.sprite(350,700,'Pigeon');
    Pigeon.anchor.set(.5);
    Pigeon.scale.set(.475);

    bottle=this.game.add.sprite(350,560,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(1.2,.8);

    name_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    name_box1.beginFill(0xE67E22,1);//9B59B6
    name_box1.drawRect(200, 900, 300, 60);
    name_1=this.game.add.text(255,908,"Pigeon",lablel_fontStyle);//870,930

    Pigeon_fullBody=this.game.add.sprite(1300,650,'Pigeon_parts','Pigeon.png');
    Pigeon_fullBody.anchor.set(.5);
    Pigeon_fullBody.scale.set(.8);
    Pigeon_fullBody.alpha=.4;

    Beak=this.game.add.sprite(1040,437,'Pigeon_parts','Beak.png');
    Beak.anchor.set(.5);
    Beak.scale.set(.8);
    Beak.alpha=0.0;
    Beak_text=this.game.add.text(900,340,"Beak",fontStyle_b);
    Beak_text.visible=false;
    arrow_Beak=this.game.add.sprite(940,400,'arrow_1');
    arrow_Beak.visible=false;
    arrow_Beak.angle=-150;
    arrow_Beak.scale.x=-2;
    arrow_Beak.alpha=.75;

    Eye=this.game.add.sprite(1095,425,'Pigeon_parts','Eye.png');
    Eye.anchor.set(.5);
    Eye.scale.set(.8);
    Eye.alpha=0.0;
    Eye_text=this.game.add.text(1070,260,"Eye",fontStyle_b);
    Eye_text.visible=false;
    
    
    Head=this.game.add.sprite(1100,425,'Pigeon_parts','P_Head.png');
    Head.anchor.set(.5);
    Head.scale.set(.8);
    Head.alpha=0.0;
    Head_text=this.game.add.text(1180,275,"Head",fontStyle_b);
    Head_text.visible=false;
    arrow_Head=this.game.add.sprite(1210,315,'arrow_1');
    arrow_Head.visible=false;
    arrow_Head.angle=-45;
    arrow_Head.scale.x=-2;
    arrow_Head.alpha=.75;

    arrow_Eye=this.game.add.sprite(1080,315,'arrow_1');
    arrow_Eye.visible=false;
    arrow_Eye.angle=-90;
    arrow_Eye.scale.x=-2;
    arrow_Eye.alpha=.75;
    
    Neck=this.game.add.sprite(1125,495,'Pigeon_parts','Neck.png');
    Neck.anchor.set(.5);
    Neck.scale.set(.8);
    Neck.alpha=0.0;
    Neck_text=this.game.add.text(880,480,"Neck",fontStyle_b);
    Neck_text.visible=false;
    arrow_Neck=this.game.add.sprite(970,520,'arrow_1');
    arrow_Neck.visible=false;
    arrow_Neck.angle=-180;
    arrow_Neck.scale.x=-2;
    arrow_Neck.alpha=.75;

    Abdomen=this.game.add.sprite(1065,730,'Pigeon_parts','Abdomen.png');
    Abdomen.anchor.set(.5);
    Abdomen.scale.set(.8);
    Abdomen.alpha=0.0;
    Abdomen_text=this.game.add.text(820,680,"Abdomen",fontStyle_b);
    Abdomen_text.visible=false;
    arrow_Abdomen=this.game.add.sprite(958,740,'arrow_1');
    arrow_Abdomen.visible=false;
    arrow_Abdomen.angle=-180;
    arrow_Abdomen.scale.x=-2;
    arrow_Abdomen.alpha=.75;
    
    Wing=this.game.add.sprite(1260,685,'Pigeon_parts','Wing.png');
    Wing.anchor.set(.5);
    Wing.scale.set(.8);
    Wing.alpha=0.0;
    Wing_text=this.game.add.text(1340,525,"Wing",fontStyle_b);
    Wing_text.visible=false;
    arrow_Wing=this.game.add.sprite(1350,560,'arrow_1');
    arrow_Wing.visible=false;
    arrow_Wing.angle=-30;
    arrow_Wing.scale.x=-2;
    arrow_Wing.alpha=.75;

    Wing_Feather=this.game.add.sprite(1330,745,'Pigeon_parts','Wing_Feather.png');
    Wing_Feather.anchor.set(.5);
    Wing_Feather.scale.set(.8);
    Wing_Feather.alpha=0.0;
    Wing_Feather_text=this.game.add.text(1400,620,"Wing feather",fontStyle_b);
    Wing_Feather_text.visible=false;
    arrow_Wing_Feather=this.game.add.sprite(1420,660,'arrow_1');
    arrow_Wing_Feather.visible=false;
    arrow_Wing_Feather.angle=-30;
    arrow_Wing_Feather.scale.x=-2;
    arrow_Wing_Feather.alpha=.75;

    Tail_Feather=this.game.add.sprite(1505,845,'Pigeon_parts','Tail_Feather.png');
    Tail_Feather.anchor.set(.5);
    Tail_Feather.scale.set(.8);
    Tail_Feather.alpha=0.0;
    Tail_Feather_text=this.game.add.text(1540,750,"Tail feather",fontStyle_b);
    Tail_Feather_text.visible=false;
    arrow_Tail_Feather=this.game.add.sprite(1620,790,'arrow_1');
    arrow_Tail_Feather.visible=false;
    arrow_Tail_Feather.angle=-30;
    arrow_Tail_Feather.scale.x=-2;
    arrow_Tail_Feather.alpha=.75;
    
    Clawed_toes=this.game.add.sprite(1130,920,'Pigeon_parts','Clawed_toes.png');
    Clawed_toes.anchor.set(.5);
    Clawed_toes.scale.set(.8);
    Clawed_toes.alpha=0.0;
    Clawed_toes_text=this.game.add.text(820,890,"Clawed toes",fontStyle_b);
    Clawed_toes_text.visible=false;
    arrow_Clawed_toes=this.game.add.sprite(1025,926,'arrow_1');
    arrow_Clawed_toes.visible=false;
    arrow_Clawed_toes.angle=-180;
    arrow_Clawed_toes.scale.x=-2;
    arrow_Clawed_toes.alpha=.75;

    Legs=this.game.add.sprite(1205,910,'Pigeon_parts','Legs.png');
    Legs.anchor.set(.5);
    Legs.scale.set(.8);
    Legs.alpha=0.0;
    Legs_text=this.game.add.text(1350,895,"Legs",fontStyle_b);
    Legs_text.visible=false;
    arrow_Legs=this.game.add.sprite(1345,910,'arrow_1');
    arrow_Legs.visible=false;
    arrow_Legs.angle=20;
    arrow_Legs.scale.x=-2;
    arrow_Legs.alpha=.75;

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,5, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe the pigeon from museum specimen.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=45;

    darkBg = this.game.add.graphics(0, 0);
    
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_1",1);
    voice.play();
    dialog.text="Click on the beak of the pigeon.";
    Beak.inputEnabled = true;
    Beak.input.useHandCursor = true;
    Beak.events.onInputDown.add(this.Exp_Pigeon_Beak, this);
    arrow.x=Beak.x-50;
    arrow.y=Beak.y;
    arrow.angle=-90;
    arrow.visible=true;
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
  Exp_Pigeon_Beak:function(){
    arrow.visible=false;
    Beak.inputEnabled = false;
    Beak.input.useHandCursor = false;
    tween1=this.game.add.tween(Beak).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Beak_text.visible=true;
      arrow_Beak.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_2",1);
      voice.play();
      dialog.y=20;
      dialog.text="The pigeon has a straight beak and its uses to eat, probe for food, \npreen, feed its young, courtship and fight. ";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.to_Pigeon_Eye, this);
    }.bind(this));  
  },
  to_Pigeon_Eye:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_3",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the eye of the pigeon.";
    Eye.inputEnabled = true;
    Eye.input.useHandCursor = true;
    Eye.events.onInputDown.add(this.Exp_Pigeon_Eye, this);
    arrow.angle=0;
    arrow.x=Eye.x;
    arrow.y=Eye.y-40;
    arrow.visible=true;
  },
  Exp_Pigeon_Eye:function(){
    arrow.visible=false;
    Eye.inputEnabled = false;
    Eye.input.useHandCursor = false;
    tween1=this.game.add.tween(Eye).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Eye_text.visible=true;
      arrow_Eye.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_4",1);
      voice.play();
      dialog.y=45;
      dialog.text="Pigeon eyes are on the side of their head and they have sharp eyesight.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Pigeon_Head, this);
    }.bind(this));  
  },
  to_Pigeon_Head:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_5",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the head of the pigeon.";
    Head.inputEnabled = true;
    Head.input.useHandCursor = true;
    Head.events.onInputDown.add(this.Exp_Pigeon_Head, this);
    arrow.angle=30;
    arrow.x=Head.x+80;
    arrow.y=Head.y-50;
    arrow.visible=true;
  },
  Exp_Pigeon_Head:function(){
    arrow.visible=false;
    Head.inputEnabled = false;
    Head.input.useHandCursor = false;
    tween1=this.game.add.tween(Head).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Head_text.visible=true;
      arrow_Head.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_6",1);
      voice.play();
      dialog.y=20;
      dialog.text="Head is flexible so that it can see in different directions during its \nmovement.";
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.to_Pigeon_Neck, this);
    }.bind(this));  
  },
  to_Pigeon_Neck:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_7",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the neck of the pigeon.";
    Neck.inputEnabled = true;
    Neck.input.useHandCursor = true;
    Neck.events.onInputDown.add(this.Exp_Pigeon_Neck, this);
    arrow.angle=-90;
    arrow.x=Neck.x-100;
    arrow.y=Neck.y+20;
    arrow.visible=true;
  },
  Exp_Pigeon_Neck:function(){
    arrow.visible=false;
    Neck.inputEnabled = false;
    Neck.input.useHandCursor = false;
    tween1=this.game.add.tween(Neck).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Neck_text.visible=true;
      arrow_Neck.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_8",1);
      voice.play();
      dialog.y=15;
      dia_box.scale.setTo(.82,.9);
      dialog.text="Pigeons are able to move their eyes, but their longer, more flexible \nnecks make it more efficient for them to do this motion tracking \nwith their necks.";
      this.game.time.events.add(Phaser.Timer.SECOND*9,this.to_Pigeon_Abdomen, this);
    }.bind(this));  
  },
  to_Pigeon_Abdomen:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_9",1);
    voice.play();
    dia_box.scale.setTo(.82,.7);
    dialog.y=45;
    dialog.text="Click on the abdomen of the pigeon.";
    Abdomen.inputEnabled = true;
    Abdomen.input.useHandCursor = true;
    Abdomen.events.onInputDown.add(this.Exp_Pigeon_Abdomen, this);
    arrow.x=Abdomen.x-50;
    arrow.y=Abdomen.y;
    arrow.visible=true;
  },
  Exp_Pigeon_Abdomen:function(){
    arrow.visible=false;
    Abdomen.inputEnabled = false;
    Abdomen.input.useHandCursor = false;
    tween1=this.game.add.tween(Abdomen).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Abdomen_text.visible=true;
      arrow_Abdomen.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_10",1);
      voice.play();
      dialog.y=20;
      dialog.text="Birds have a two-part stomach, a glandular portion known as the \nproventriculus and a muscular portion known as the gizzard.";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.to_Pigeon_Wing_Feather, this);
    }.bind(this));  
  },
  to_Pigeon_Wing_Feather:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_11",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the wing feather of the pigeon.";
    Wing_Feather.inputEnabled = true;
    Wing_Feather.input.useHandCursor = true;
    Wing_Feather.events.onInputDown.add(this.Exp_Pigeon_Wing_Feather, this);
    arrow.angle=0;
    arrow.x=Wing_Feather.x+10;
    arrow.y=Wing_Feather.y-90;
    arrow.visible=true;
  },
  Exp_Pigeon_Wing_Feather:function(){
    arrow.visible=false;
    Wing_Feather.inputEnabled = false;
    Wing_Feather.input.useHandCursor = false;
    tween1=this.game.add.tween(Wing_Feather).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Wing_Feather_text.visible=true;
      arrow_Wing_Feather.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_12",1);
      voice.play();
      dialog.y=20;
      dialog.text="Feathers allow birds to fly, but they also help them show off, blend in, \nstay warm, and keep dry.";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.to_Pigeon_Wing, this);
    }.bind(this));  
  },
  to_Pigeon_Wing:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_13",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the wings of the pigeon.";
    Wing.inputEnabled = true;
    Wing.input.useHandCursor = true;
    Wing.events.onInputDown.add(this.Exp_Pigeon_Wing, this);
    arrow.angle=0;
    arrow.x=Wing.x+50;
    arrow.y=Wing.y-90;
    arrow.visible=true;
  },
  Exp_Pigeon_Wing:function(){
    arrow.visible=false;
    Wing.inputEnabled = false;
    Wing.input.useHandCursor = false;
    tween1=this.game.add.tween(Wing).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Wing_text.visible=true;
      arrow_Wing.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_14",1);
      voice.play();
      dialog.y=20;
      dialog.text="The bird's wing is a paired forelimb in birds. The wings give the birds \nthe ability to fly, creating lift.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Pigeon_Tail_Feather, this);
    }.bind(this));  
  },
  to_Pigeon_Tail_Feather:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_15",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the tail feathers of the pigeon.";
    Tail_Feather.inputEnabled = true;
    Tail_Feather.input.useHandCursor = true;
    Tail_Feather.events.onInputDown.add(this.Exp_Pigeon_Tail_Feather, this);
    arrow.x=Tail_Feather.x;
    arrow.y=Tail_Feather.y-40;
    arrow.visible=true;
  },
  Exp_Pigeon_Tail_Feather:function(){
    arrow.visible=false;
    Tail_Feather.inputEnabled = false;
    Tail_Feather.input.useHandCursor = false;
    tween1=this.game.add.tween(Tail_Feather).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Tail_Feather_text.visible=true;
      arrow_Tail_Feather.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_16",1);
      voice.play();
      dialog.y=20;
      dialog.text="The quill feathers occurring around the uropygium to form the tail of \npigeon are called tail-quills or rectrices.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Pigeon_Clawed_toes, this);
    }.bind(this));  
  },
  to_Pigeon_Clawed_toes:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_17",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the clawed toes of the pigeon.";
    Clawed_toes.inputEnabled = true;
    Clawed_toes.input.useHandCursor = true;
    Clawed_toes.events.onInputDown.add(this.Exp_Pigeon_Clawed_toes, this);
    arrow.angle=-90;
    arrow.x=Clawed_toes.x-50;
    arrow.y=Clawed_toes.y-5;
    arrow.visible=true;
  },
  Exp_Pigeon_Clawed_toes:function(){
    arrow.visible=false;
    Clawed_toes.inputEnabled = false;
    Clawed_toes.input.useHandCursor = false;
    tween1=this.game.add.tween(Clawed_toes).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Clawed_toes_text.visible=true;
      arrow_Clawed_toes.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_18",1);
      voice.play();
      dialog.y=20;
      dialog.text="Claws on the feet are used for grasping the prey, for perching, for \nscratching the ground or for fighting.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Pigeon_Legs, this);
    }.bind(this));  
  },
  to_Pigeon_Legs:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step4_19",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the leg of the pigeon.";
    Legs.inputEnabled = true;
    Legs.input.useHandCursor = true;
    Legs.events.onInputDown.add(this.Exp_Pigeon_Legs, this);
    arrow.angle=90;
    arrow.x=Legs.x+80;
    arrow.y=Legs.y-20;
    arrow.visible=true;
  },
  Exp_Pigeon_Legs:function(){
    arrow.visible=false;
    Legs.inputEnabled = false;
    Legs.input.useHandCursor = false;
    tween1=this.game.add.tween(Legs).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Legs_text.visible=true;
      arrow_Legs.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step4_20",1);
      voice.play();
      dialog.y=45;
      dialog.text="Pigeon has two legs and uses their feet for walking or perching.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.NextExperiment, this);
    }.bind(this));  
  },
  NextExperiment:function(){
    Beak.visible=false;
    Eye.visible=false;
    Head.visible=false;
    Neck.visible=false;
    Abdomen.visible=false;
    Wing.visible=false;
    Wing_Feather.visible=false;
    Tail_Feather.visible=false;
    Clawed_toes.visible=false;
    Legs.visible=false;
    Pigeon_fullBody.alpha=1;
    voice.destroy(true);
    voice=this.game.add.audio("step4_21",1);
    voice.play();
    dialog.text="Click on the next button to see the characteristic features of the pigeon.";
    play.visible=true;
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    //this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_4",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
