var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
  var theory_completed;
  var theory_Scene;
  var observation_completed;
  var observation_Scene;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        ////
        
         //////
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.png");
       // this.game.load.image("bg1","assets/bg1.png");
        
   this.game.load.image('transBackground','assets/transBackground.png');
   //this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        
        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');

        this.game.load.image('A_Blue','assets/A_Blue.png');
        this.game.load.image('B_Blue','assets/B_Blue.png');
        this.game.load.image('C_Blue','assets/C_Blue.png');
        this.game.load.image('D_Blue','assets/D_Blue.png');
        
        this.game.load.image('Clock','assets/Clock.png');
        this.game.load.image('ClockClock_Needle_Big','assets/Clock_Needle_Big.png');
        this.game.load.image('Clock_Needle_Small','assets/Clock_Needle_Big.png');

        this.game.load.image('observation_table','assets/observation_table.png');
        this.game.load.image('observation_table_line','assets/observation_table_line.png');
////////////////////////////////////Animals//////////////////////////////////////////////////////////
      this.game.load.image('mark','assets/mark.png');
      this.game.load.image('arrow_1','assets/arrow_1.png');

      this.game.load.image('Cockroach','assets/Animals/Cockroach.png');
      this.game.load.image('Earth_worm','assets/Animals/Earth_worm.png');
      this.game.load.image('Fish','assets/Animals/Fish.png');
      this.game.load.image('Pigeon','assets/Animals/Pigeon.png');
      this.game.load.image('bottle','assets/Animals/bottle.png');

      this.game.load.atlasJSONHash('Earth_worm_parts', 'assets/Animals/Earth_worm_parts.png', 'assets/Animals/Earth_worm_parts.json');
      this.game.load.atlasJSONHash('Cokoroach_parts', 'assets/Animals/Cokoroach_parts.png', 'assets/Animals/Cokoroach_parts.json');
      this.game.load.atlasJSONHash('Fish_parts', 'assets/Animals/Fish_parts.png', 'assets/Animals/Fish_parts.json');
      this.game.load.atlasJSONHash('Pigeon_parts', 'assets/Animals/Pigeon_parts.png', 'assets/Animals/Pigeon_parts.json');


      this.game.load.image('Cockroach_Diagram','assets/Animals/Cockroach_Diagram.png');
      this.game.load.image('Earth_worm_Diagram','assets/Animals/Earth_worm_Diagram.png');
      this.game.load.image('Fish_Diagram','assets/Animals/Fish_Diagram.png');
      this.game.load.image('Pigeon_Diagram','assets/Animals/Pigeon_Diagram.png');
      this.game.load.atlasJSONHash('arrow', 'assets/Animals/arrow.png', 'assets/Animals/arrow.json');
      
      ///////////////materials//////////////////////
      

      //////Audio/////////////////////////
      this.game.load.audio('Aim','assets/audio/Aim.mp3');
      this.game.load.audio('Precaution','assets/audio/Precaution.mp3');
      this.game.load.audio('Procedure_1','assets/audio/Procedure_1.mp3');
      this.game.load.audio('Observations_1','assets/audio/Observations_1.mp3');
      this.game.load.audio('Observations_2','assets/audio/Observations_2.mp3');
      this.game.load.audio('Observations_3','assets/audio/Observations_3.mp3');
      this.game.load.audio('Observations_4','assets/audio/Observations_4.mp3');
      this.game.load.audio('Observations_5','assets/audio/Observations_5.mp3');
      this.game.load.audio('Observations_6','assets/audio/Observations_6.mp3');

      this.game.load.audio('Observations1_1','assets/audio/Observations1_1.mp3');
      this.game.load.audio('Observations1_2','assets/audio/Observations1_2.mp3');
      this.game.load.audio('Observations1_3','assets/audio/Observations1_3.mp3');
      this.game.load.audio('Observations1_4','assets/audio/Observations1_4.mp3');
      this.game.load.audio('Observations1_5','assets/audio/Observations1_5.mp3');
      this.game.load.audio('Observations1_6','assets/audio/Observations1_6.mp3');
      this.game.load.audio('Observations1_7','assets/audio/Observations1_7.mp3');     
      
      this.game.load.audio('Observations2_1','assets/audio/Observations2_1.mp3');
      this.game.load.audio('Observations2_2','assets/audio/Observations2_2.mp3');
      this.game.load.audio('Observations2_3','assets/audio/Observations2_3.mp3');
      this.game.load.audio('Observations2_4','assets/audio/Observations2_4.mp3');
      this.game.load.audio('Observations2_5','assets/audio/Observations2_5.mp3');
      
      this.game.load.audio('Observations3_1','assets/audio/Observations3_1.mp3');
      this.game.load.audio('Observations3_2','assets/audio/Observations3_2.mp3');
      this.game.load.audio('Observations3_3','assets/audio/Observations3_3.mp3');
      this.game.load.audio('Observations3_4','assets/audio/Observations3_4.mp3');
      this.game.load.audio('Observations3_5','assets/audio/Observations3_5.mp3');

      this.game.load.audio('Result','assets/audio/Result.mp3');
      this.game.load.audio('Result_1','assets/audio/Result_1.mp3');
      this.game.load.audio('Result_2','assets/audio/Result_2.mp3');
      this.game.load.audio('Result_3','assets/audio/Result_3.mp3');
      
      this.game.load.audio('step1_0','assets/audio/step1_0.mp3');
      this.game.load.audio('step1_1','assets/audio/step1_1.mp3');
      this.game.load.audio('step1_2','assets/audio/step1_2.mp3');
      this.game.load.audio('step1_3','assets/audio/step1_3.mp3');
      this.game.load.audio('step1_4','assets/audio/step1_4.mp3');
      this.game.load.audio('step1_5','assets/audio/step1_5.mp3');
      this.game.load.audio('step1_6','assets/audio/step1_6.mp3');
      this.game.load.audio('step1_7','assets/audio/step1_7.mp3');
      this.game.load.audio('step1_8','assets/audio/step1_8.mp3');
      this.game.load.audio('step1_9','assets/audio/step1_9.mp3');
      this.game.load.audio('step1_10','assets/audio/step1_10.mp3');
      this.game.load.audio('step1_11','assets/audio/step1_11.mp3');
      this.game.load.audio('step1_12','assets/audio/step1_12.mp3');
      this.game.load.audio('step1_13','assets/audio/step1_13.mp3');

      this.game.load.audio('step2_0','assets/audio/step2_0.mp3');
      this.game.load.audio('step2_1','assets/audio/step2_1.mp3');
      this.game.load.audio('step2_2','assets/audio/step2_2.mp3');
      this.game.load.audio('step2_3','assets/audio/step2_3.mp3');
      this.game.load.audio('step2_4','assets/audio/step2_4.mp3');
      this.game.load.audio('step2_5','assets/audio/step2_5.mp3');
      this.game.load.audio('step2_6','assets/audio/step2_6.mp3');
      this.game.load.audio('step2_7','assets/audio/step2_7.mp3');
      this.game.load.audio('step2_8','assets/audio/step2_8.mp3');
      this.game.load.audio('step2_9','assets/audio/step2_9.mp3');
      this.game.load.audio('step2_10','assets/audio/step2_10.mp3');
      this.game.load.audio('step2_11','assets/audio/step2_11.mp3');
      this.game.load.audio('step2_12','assets/audio/step2_12.mp3');
      this.game.load.audio('step2_13','assets/audio/step2_13.mp3');
      this.game.load.audio('step2_14','assets/audio/step2_14.mp3');
      this.game.load.audio('step2_15','assets/audio/step2_15.mp3');
      this.game.load.audio('step2_16','assets/audio/step2_16.mp3');
      this.game.load.audio('step2_17','assets/audio/step2_17.mp3');
      this.game.load.audio('step2_18','assets/audio/step2_18.mp3');
      this.game.load.audio('step2_19','assets/audio/step2_19.mp3');
      this.game.load.audio('step2_20','assets/audio/step2_20.mp3');
      this.game.load.audio('step2_21','assets/audio/step2_21.mp3');

      this.game.load.audio('step3_0','assets/audio/step3_0.mp3');
      this.game.load.audio('step3_1','assets/audio/step3_1.mp3');
      this.game.load.audio('step3_2','assets/audio/step3_2.mp3');
      this.game.load.audio('step3_3','assets/audio/step3_3.mp3');
      this.game.load.audio('step3_4','assets/audio/step3_4.mp3');
      this.game.load.audio('step3_5','assets/audio/step3_5.mp3');
      this.game.load.audio('step3_6','assets/audio/step3_6.mp3');
      this.game.load.audio('step3_7','assets/audio/step3_7.mp3');
      this.game.load.audio('step3_8','assets/audio/step3_8.mp3');
      this.game.load.audio('step3_9','assets/audio/step3_9.mp3');
      this.game.load.audio('step3_10','assets/audio/step3_10.mp3');
      this.game.load.audio('step3_11','assets/audio/step3_11.mp3');
      this.game.load.audio('step3_12','assets/audio/step3_12.mp3');
      this.game.load.audio('step3_13','assets/audio/step3_13.mp3');
      this.game.load.audio('step3_14','assets/audio/step3_14.mp3');
      this.game.load.audio('step3_15','assets/audio/step3_15.mp3');
      this.game.load.audio('step3_16','assets/audio/step3_16.mp3');
      this.game.load.audio('step3_17','assets/audio/step3_17.mp3');
      this.game.load.audio('step3_18','assets/audio/step3_18.mp3');
      this.game.load.audio('step3_19','assets/audio/step3_19.mp3');
      this.game.load.audio('step3_20','assets/audio/step3_20.mp3');
      this.game.load.audio('step3_21','assets/audio/step3_21.mp3');

      this.game.load.audio('step4_0','assets/audio/step4_0.mp3');
      this.game.load.audio('step4_1','assets/audio/step4_1.mp3');
      this.game.load.audio('step4_2','assets/audio/step4_2.mp3');
      this.game.load.audio('step4_3','assets/audio/step4_3.mp3');
      this.game.load.audio('step4_4','assets/audio/step4_4.mp3');
      this.game.load.audio('step4_5','assets/audio/step4_5.mp3');
      this.game.load.audio('step4_6','assets/audio/step4_6.mp3');
      this.game.load.audio('step4_7','assets/audio/step4_7.mp3');
      this.game.load.audio('step4_8','assets/audio/step4_8.mp3');
      this.game.load.audio('step4_9','assets/audio/step4_9.mp3');
      this.game.load.audio('step4_10','assets/audio/step4_10.mp3');
      this.game.load.audio('step4_11','assets/audio/step4_11.mp3');
      this.game.load.audio('step4_12','assets/audio/step4_12.mp3');
      this.game.load.audio('step4_13','assets/audio/step4_13.mp3');
      this.game.load.audio('step4_14','assets/audio/step4_14.mp3');
      this.game.load.audio('step4_15','assets/audio/step4_15.mp3');
      this.game.load.audio('step4_16','assets/audio/step4_16.mp3');
      this.game.load.audio('step4_17','assets/audio/step4_17.mp3');
      this.game.load.audio('step4_18','assets/audio/step4_18.mp3');
      this.game.load.audio('step4_19','assets/audio/step4_19.mp3');
      this.game.load.audio('step4_20','assets/audio/step4_20.mp3');
      this.game.load.audio('step4_21','assets/audio/step4_21.mp3');
      //////////////////////////////////////
      
	},
  	create: function(){
      level=1;
  	  hotflag=1;
      ip = location.host; 
      theory_completed=false;
      theory_Scene=1;
      observation_completed=false;
      observation_Scene=1;
      
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Viva");//Starting the gametitle state
      //this.game.state.start("Experiment_4");//Starting the gametitle state
      //this.game.state.start("Materials");//Simulation_hot1//
      //this.game.state.start("Theory");
     //this.game.state.start("Observations_3");//hot
      //this.game.state.start("Result_1");//Starting the gametitle state
      this.game.state.start("Aim");//Starting the gametitle state
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure_1");

	}
}

