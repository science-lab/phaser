var theory = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  var maskBox;
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  var current_scene;
  var previous_scene;
  var xmin;
  var ymin;
  var xmax;
  var ymax;
  var imageDrag;
  var zoomed;
  var currentx;
  var previousx;
  var currenty;
  var previousy;
  }
  
  theory.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

  muted = false;
  voice=this.game.add.audio("obj",1);
  // voice=this.game.add.audio("fobj",1);
  //voice.play();
  bg= this.game.add.sprite(0, 0,'bg');
  bg.scale.setTo(1,1.3);
  xmin=200;
  ymin=200;
  xmax=200;
  ymax=200;
  imageDrag=false;
  zoomed=false;
  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_disabled.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  //muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}
  
  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  subfontStyle={ font: "28px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,140,'dialogue_box');
  base.scale.setTo(1.05,1.00);

zoomGroup=this.game.add.group();

  procedure_text=this.game.add.text(210,200,"Theory:",headfontStyle);
 // exp_Name="decomposition_reaction";
 //current_scene=1;
 previous_scene=0;
      procedure_step_1="Depending upon the environmental conditions, certain morphological features and \nvariations are observed in different animals.";
      procedure_step_2="Animals show adaptations depending on different habitats.";
      procedure_step_3="Habitat is the place where an organism lives and plenty of food is available for them.";
      procedure_step_4="Habitat also provides safety to the organisms and its young ones.";
      procedure_step_5="By observing morphological characters the animals can be identified and studied.";
      procedure_step_6="Each animal has specific characters of the phylum to which it belongs and definite \nadaptive features to survive in a particular environment.";
      procedure_step_7="Animals do not synthesise their food but have heterotrophic mode of nutrition and \nmay be parasites or saprophytes.";
      procedure_step_8="Animals show locomotion.";
      procedure_step_9="Characters of phylum to which an animal belongs and the adaptations it shows in a \nparticular habitat are discussed after studying the characteristic features of specific \nanimals along with their diagrams.";
      procedure_step_10="Group Annelida (earthworm), Arthropoda (cockroach), Pisces (bony fish) and Aves \n(bird) have been studied from museum specimens.";
      procedure_step_11="";
      procedure_step_12="";
      procedure_step_13="";
      procedure_step_14="";
      procedure_step_15="";
      procedure_step_16="";
      procedure_step_17="";  
      procedure_step_18="";
      procedure_step_19="";
      procedure_step_20="";
      
      this.showTheory();
  /////////////////////////////////////////////////////////////////
  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  next_btn = this.game.add.sprite(1660,890,'components','next_disabled.png');
  next_btn.scale.setTo(.7,.7);
  next_btn = this.game.add.sprite(1660,890,'components','next_pressed.png');
  next_btn.scale.setTo(.7,.7);
  next_btn.inputEnabled = true;
  //next_btn.input.priorityID = 3;
  next_btn.input.useHandCursor = true;
  next_btn.events.onInputDown.add(this.toNext, this);
 
  prev_btn = this.game.add.sprite(260,890,'components','next_disabled.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn = this.game.add.sprite(260,890,'components','next_pressed.png');
  prev_btn.scale.setTo(-.7,.7);
  prev_btn.inputEnabled = true;
  if(theory_Scene==1)
    prev_btn.visible=false;
  //next_btn.input.priorityID = 3;
  prev_btn.input.useHandCursor = true;
  prev_btn.events.onInputDown.add(this.toPrevious, this);
 
 
  
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toLab_precautions, this);
 if(!theory_completed)
    play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

  
 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        //to go to next popup
        toNext:function(){
          this.removeTexts();
          previous_scene=theory_Scene;
          theory_Scene++;
        prev_btn.visible=true;
        if(theory_Scene>=2){
          next_btn.visible=false;
          play.visible=true;
        }
          this.showTheory();
          
        },
        toPrevious:function(){
          this.removeTexts();
          previous_scene=theory_Scene;
          theory_Scene--;
        next_btn.visible=true;
        if(theory_Scene<=1){
          prev_btn.visible=false;
        }
          this.showTheory();
        },
        removeTexts:function(){
          if(theory_Scene==3){
            zoomGroup.visible=false;
          }else{
            procedure_step_text_1.destroy(true);
            procedure_step_text_2.destroy(true);
            procedure_step_text_3.destroy(true);
            procedure_step_text_4.destroy(true);
            procedure_step_text_5.destroy(true);
            procedure_step_text_6.destroy(true);
          }
          
        },
        showTheory:function(){
          console.log(theory_Scene);
          switch(theory_Scene){
            case 1:
              
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_1,fontStyle);
              procedure_step_text_2=this.game.add.text(260,430,procedure_step_2,fontStyle);
              procedure_step_text_3=this.game.add.text(260,510,procedure_step_3,fontStyle);
              procedure_step_text_4=this.game.add.text(260,590,procedure_step_4,fontStyle);
              procedure_step_text_5=this.game.add.text(260,670,procedure_step_5,fontStyle);
              procedure_step_text_6=this.game.add.text(260,750,procedure_step_6,fontStyle);
              
            break;
            case 2:
             
              procedure_step_text_1=this.game.add.text(260,300,procedure_step_7,fontStyle);
              procedure_step_text_2=this.game.add.text(260,430,procedure_step_8,fontStyle);
              procedure_step_text_3=this.game.add.text(260,510,procedure_step_9,fontStyle);
              procedure_step_text_4=this.game.add.text(260,690,procedure_step_10,fontStyle);
              procedure_step_text_5=this.game.add.text(260,770,"",fontStyle);
              procedure_step_text_6=this.game.add.text(260,850,"",fontStyle);
              
          break;
          
         

          }
        },
     
    //For to next scene   
   
    toLab_precautions:function()
        {
          
          voice.destroy();
          //procedure_audio1.destroy();
         // procedure_audio2.destroy();
         //this.state.start("Lab_Precautions", true, false);
         this.state.start("Procedure_1", true, false);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
    //procedure_audio1.destroy();
    //procedure_audio2.destroy();
  },
  GotoNextScreen:function(){

    //procedure_audio2.play();
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //procedure_audio1.destroy();
  //procedure_audio2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  