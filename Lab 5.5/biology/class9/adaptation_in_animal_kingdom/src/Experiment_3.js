var experiment_3 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_3.prototype = {
  	create: function(){

hotFlag=false;
bg= this.game.add.sprite(0, 0,'bg');

// bgBlack= this.game.add.sprite(0,0,'bg_black');
// bgBlack.scale.setTo(1,1);
// bgBlack.alpha=0;
// bgBlack.visible=false;



// circle1 = this.game.add.graphics(0, 0);
// ////circle1.lineStyle(8, 0xffd900, 1);
// circle1.beginFill(0xFF0000, .3);
// circle1.drawCircle(902, 494, 430);
// //circle1.scale.set(0.5);
// Muscle_tissue.mask=circle1;
// circle1.visible=false;


voice=this.game.add.audio("step3_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations_2", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    
    label_box1 = this.game.add.graphics(0, 0);
    label_box1.lineStyle(5,0xE67E22,.6);
    label_box1.beginFill(0xffffff,1);//9B59B6
    label_box1.drawRect(780, 240, 1000, 800);
    // label_1=this.game.add.text(910,808,"Muscle cell",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    Fish=this.game.add.sprite(350,650,'Fish');
    Fish.anchor.set(.5);
    Fish.scale.set(.35);

    bottle=this.game.add.sprite(350,560,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(1.2,.8);

    name_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    name_box1.beginFill(0xE67E22,1);//9B59B6
    name_box1.drawRect(200, 900, 300, 60);
    name_1=this.game.add.text(255,908,"Bony fish",lablel_fontStyle);//870,930

    Fish_fullBody=this.game.add.sprite(1300,650,'Fish_parts','Fish.png');
    Fish_fullBody.anchor.set(.5);
    Fish_fullBody.scale.set(.8);
    Fish_fullBody.alpha=.5;

    Eye=this.game.add.sprite(995,618,'Fish_parts','Eye.png');
    Eye.anchor.set(.5);
    Eye.scale.set(.8);
    Eye.alpha=0.0;
    Eye_text=this.game.add.text(1000,465,"Eye",fontStyle_b);
    Eye_text.visible=false;
    arrow_Eye=this.game.add.sprite(1000,510,'arrow_1');
    arrow_Eye.visible=false;
    arrow_Eye.angle=-80;
    arrow_Eye.scale.x=-2;
    arrow_Eye.alpha=.75;

    Terminal_mouth=this.game.add.sprite(955,631,'Fish_parts','Terminal_mouth.png');
    Terminal_mouth.anchor.set(.5);
    Terminal_mouth.scale.set(.8);
    Terminal_mouth.alpha=0.0;
    Terminal_mouth_text=this.game.add.text(810,450,"Terminal \nmouth",fontStyle_b);
    Terminal_mouth_text.visible=false;
    arrow_Terminal_mouth=this.game.add.sprite(870,560,'arrow_1');
    arrow_Terminal_mouth.visible=false;
    arrow_Terminal_mouth.angle=-130;
    arrow_Terminal_mouth.scale.x=-2;
    arrow_Terminal_mouth.alpha=.75;

    Operculum=this.game.add.sprite(1065,635,'Fish_parts','Operculum.png');
    Operculum.anchor.set(.5);
    Operculum.scale.set(.8);
    Operculum.alpha=0.0;
    Operculum_text=this.game.add.text(810,715,"Operculum",fontStyle_b);
    Operculum_text.visible=false;
    arrow_Operculum=this.game.add.sprite(980,725,'arrow_1');
    arrow_Operculum.visible=false;
    arrow_Operculum.angle=140;
    arrow_Operculum.scale.x=-2;
    arrow_Operculum.alpha=.75;

    Pectoral_fin=this.game.add.sprite(1125,680,'Fish_parts','Pectoral_fin.png');
    Pectoral_fin.anchor.set(.5);
    Pectoral_fin.scale.set(.8);
    Pectoral_fin.alpha=0.0;
    Pectoral_fin_text=this.game.add.text(900,800,"Pectoral fin",fontStyle_b);
    Pectoral_fin_text.visible=false;
    arrow_Pectoral_fin=this.game.add.sprite(1070,800,'arrow_1');
    arrow_Pectoral_fin.visible=false;
    arrow_Pectoral_fin.angle=110;
    arrow_Pectoral_fin.scale.x=-2;
    arrow_Pectoral_fin.alpha=.75;

    Pelvic_Fin=this.game.add.sprite(1265,780,'Fish_parts','Pelvic_Fin.png');
    Pelvic_Fin.anchor.set(.5);
    Pelvic_Fin.scale.set(.8);
    Pelvic_Fin.alpha=0.0;
    Pelvic_Fin_text=this.game.add.text(1100,870,"Pelvic Fin",fontStyle_b);
    Pelvic_Fin_text.visible=false;
    arrow_Pelvic_Fin=this.game.add.sprite(1220,870,'arrow_1');
    arrow_Pelvic_Fin.visible=false;
    arrow_Pelvic_Fin.angle=110;
    arrow_Pelvic_Fin.scale.x=-2;
    arrow_Pelvic_Fin.alpha=.75;

    Dorsal_fin=this.game.add.sprite(1332,535,'Fish_parts','Dorsal_fin.png');
    Dorsal_fin.anchor.set(.5);
    Dorsal_fin.scale.set(.8);
    Dorsal_fin.alpha=0.0;
    Dorsal_fin_text=this.game.add.text(1320,340,"Dorsal fin",fontStyle_b);
    Dorsal_fin_text.visible=false;
    arrow_Dorsal_fin=this.game.add.sprite(1340,390,'arrow_1');
    arrow_Dorsal_fin.visible=false;
    arrow_Dorsal_fin.angle=-90;
    arrow_Dorsal_fin.scale.x=-2;
    arrow_Dorsal_fin.alpha=.75;

    Anal_Fin=this.game.add.sprite(1440,738,'Fish_parts','Anal_Fin.png');
    Anal_Fin.anchor.set(.5);
    Anal_Fin.scale.set(.8);
    Anal_Fin.alpha=0.0;
    Anal_Fin_text=this.game.add.text(1300,840,"Anal Fin",fontStyle_b);
    Anal_Fin_text.visible=false;
    arrow_Anal_Fin=this.game.add.sprite(1400,840,'arrow_1');
    arrow_Anal_Fin.visible=false;
    arrow_Anal_Fin.angle=110;
    arrow_Anal_Fin.scale.x=-2;
    arrow_Anal_Fin.alpha=.75;

    Caudal_Fin=this.game.add.sprite(1610,630,'Fish_parts','Caudal_Fin.png');
    Caudal_Fin.anchor.set(.5);
    Caudal_Fin.scale.set(.8);
    Caudal_Fin.alpha=0.0;
    Caudal_Fin_text=this.game.add.text(1480,810,"Caudal Fin",fontStyle_b);
    Caudal_Fin_text.visible=false;
    arrow_Caudal_Fin=this.game.add.sprite(1580,810,'arrow_1');
    arrow_Caudal_Fin.visible=false;
    arrow_Caudal_Fin.angle=110;
    arrow_Caudal_Fin.scale.x=-2;
    arrow_Caudal_Fin.alpha=.75;

    Lateral_Line=this.game.add.sprite(1315,642,'Fish_parts','Lateral_Line.png');
    Lateral_Line.anchor.set(.5);
    Lateral_Line.scale.set(.8);
    Lateral_Line.alpha=0.0;
    Lateral_Line_text=this.game.add.text(1415,445,"Lateral \nLine",fontStyle_b);
    Lateral_Line_text.visible=false;
    arrow_Lateral_Line=this.game.add.sprite(1430,535,'arrow_1');
    arrow_Lateral_Line.visible=false;
    arrow_Lateral_Line.angle=-45;
    arrow_Lateral_Line.scale.x=-3;
    arrow_Lateral_Line.alpha=.75;

    Scales=this.game.add.sprite(1277,652,'Fish_parts','Scales.png');
    Scales.anchor.set(.5);
    Scales.scale.set(.8);
    Scales.alpha=0.0;
    Scales_text=this.game.add.text(1150,430,"Scales",fontStyle_b);
    Scales_text.visible=false;
    arrow_Scales=this.game.add.sprite(1200,480,'arrow_1');
    arrow_Scales.visible=false;
    arrow_Scales.angle=-100;
    arrow_Scales.scale.x=-2;
    arrow_Scales.alpha=.75;

       

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,5, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe the fish from museum specimen.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=45;

    darkBg = this.game.add.graphics(0, 0);
    
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_1",1);
    voice.play();
    dialog.text="Click on the eye of the fish.";
    Eye.inputEnabled = true;
    Eye.input.useHandCursor = true;
    Eye.events.onInputDown.add(this.Exp_Fish_Eye, this);
    arrow.x=Eye.x;
    arrow.y=Eye.y-55;
    //arrow.angle=90;
    arrow.visible=true;
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
  Exp_Fish_Eye:function(){
    arrow.visible=false;
    Eye.inputEnabled = false;
    Eye.input.useHandCursor = false;
    tween1=this.game.add.tween(Eye).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Eye_text.visible=true;
      arrow_Eye.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_2",1);
      voice.play();
      dialog.y=15;
      dia_box.scale.setTo(.82,.9);
      dialog.text="Fish eyes are usually placed just dorsal of and above the mouth. \nThe eyes are covered by a transparent nictitating membrane which \nprotects it from water.";
      this.game.time.events.add(Phaser.Timer.SECOND*10,this.to_Fish_Terminal_mouth, this);
    }.bind(this));  
  },
  to_Fish_Terminal_mouth:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_3",1);
    voice.play();
    dia_box.scale.setTo(.82,.7);
    dialog.y=45;
    dialog.text="Click on the terminal mouth of the fish.";
    Terminal_mouth.inputEnabled = true;
    Terminal_mouth.input.useHandCursor = true;
    Terminal_mouth.events.onInputDown.add(this.Exp_Fish_Terminal_mouth, this);
    arrow.angle=-90;
    arrow.x=Terminal_mouth.x-50;
    arrow.y=Terminal_mouth.y;
    arrow.visible=true;
  },
  Exp_Fish_Terminal_mouth:function(){
    arrow.visible=false;
    Terminal_mouth.inputEnabled = false;
    Terminal_mouth.input.useHandCursor = false;
    tween1=this.game.add.tween(Terminal_mouth).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Terminal_mouth_text.visible=true;
      arrow_Terminal_mouth.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_4",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="Terminal mouths are located in the middle of the head and point \nforward.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Fish_Operculum, this);
    }.bind(this));  
  },
  to_Fish_Operculum:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_5",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the operculum of the fish.";
    Operculum.inputEnabled = true;
    Operculum.input.useHandCursor = true;
    Operculum.events.onInputDown.add(this.Exp_Fish_Operculum, this);
    arrow.angle=0;
    arrow.x=Operculum.x-20;
    arrow.y=Operculum.y-60;
    arrow.visible=true;
  },
  Exp_Fish_Operculum:function(){
    arrow.visible=false;
    Operculum.inputEnabled = false;
    Operculum.input.useHandCursor = false;
    tween1=this.game.add.tween(Operculum).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Operculum_text.visible=true;
      arrow_Operculum.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_6",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The operculum of a bony fish is the hard bony flap covering and \nprotecting the gills.";
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.to_Fish_Pectoral_fin, this);
    }.bind(this));  
  },
  to_Fish_Pectoral_fin:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_7",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the pectoral fin of the fish.";
    Pectoral_fin.inputEnabled = true;
    Pectoral_fin.input.useHandCursor = true;
    Pectoral_fin.events.onInputDown.add(this.Exp_Fish_Pectoral_fin, this);
    arrow.x=Pectoral_fin.x;
    arrow.y=Pectoral_fin.y-40;
    arrow.visible=true;
  },
  Exp_Fish_Pectoral_fin:function(){
    arrow.visible=false;
    Pectoral_fin.inputEnabled = false;
    Pectoral_fin.input.useHandCursor = false;
    tween1=this.game.add.tween(Pectoral_fin).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Pectoral_fin_text.visible=true;
      arrow_Pectoral_fin.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_8",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The pectoral fin, located on either side of the fish near the gills and \nit help a fish turn.";
      this.game.time.events.add(Phaser.Timer.SECOND*6,this.to_Fish_Pelvic_Fin, this);
    }.bind(this));  
  },
  to_Fish_Pelvic_Fin:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_9",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the pelvic fin of the fish.";
    Pelvic_Fin.inputEnabled = true;
    Pelvic_Fin.input.useHandCursor = true;
    Pelvic_Fin.events.onInputDown.add(this.Exp_Fish_Pelvic_Fin, this);
    arrow.angle=-90;
    arrow.x=Pelvic_Fin.x-70;
    arrow.y=Pelvic_Fin.y;
    arrow.visible=true;
  },
  Exp_Fish_Pelvic_Fin:function(){
    arrow.visible=false;
    Pelvic_Fin.inputEnabled = false;
    Pelvic_Fin.input.useHandCursor = false;
    tween1=this.game.add.tween(Pelvic_Fin).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Pelvic_Fin_text.visible=true;
      arrow_Pelvic_Fin.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_10",1);
      voice.play();
      dialog.y=15;
      dia_box.scale.setTo(.82,.9);
      dialog.text="Pelvic fins, located on the bottom of the fish and it assists the fish in \ngoing up or down through the water, turning sharply, and stopping \nquickly.";
      this.game.time.events.add(Phaser.Timer.SECOND*10,this.to_Fish_Dorsal_fin, this);
    }.bind(this));  
  },
  to_Fish_Dorsal_fin:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_11",1);
    voice.play();
    dialog.y=45;
    dia_box.scale.setTo(.82,.7);
    dialog.text="Click on the dorsal fin of the fish.";
    Dorsal_fin.inputEnabled = true;
    Dorsal_fin.input.useHandCursor = true;
    Dorsal_fin.events.onInputDown.add(this.Exp_Fish_Dorsal_fin, this);
    arrow.angle=0;
    arrow.x=Dorsal_fin.x;
    arrow.y=Dorsal_fin.y-80;
    arrow.visible=true;
  },
  Exp_Fish_Dorsal_fin:function(){
    arrow.visible=false;
    Dorsal_fin.inputEnabled = false;
    Dorsal_fin.input.useHandCursor = false;
    tween1=this.game.add.tween(Dorsal_fin).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Dorsal_fin_text.visible=true;
      arrow_Dorsal_fin.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_12",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="Dorsal fins are located on the back. The dorsal fins serve to protect \nthe fish against rolling and assist it in sudden turns and stops.";
      this.game.time.events.add(Phaser.Timer.SECOND*9,this.to_Fish_Anal_Fin, this);
    }.bind(this));  
  },
  to_Fish_Anal_Fin:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_13",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the anal fin of the fish.";
    Anal_Fin.inputEnabled = true;
    Anal_Fin.input.useHandCursor = true;
    Anal_Fin.events.onInputDown.add(this.Exp_Fish_Anal_Fin, this);
    arrow.angle=-120;
    arrow.x=Anal_Fin.x-70;
    arrow.y=Anal_Fin.y+20;
    arrow.visible=true;
  },
  Exp_Fish_Anal_Fin:function(){
    arrow.visible=false;
    Anal_Fin.inputEnabled = false;
    Anal_Fin.input.useHandCursor = false;
    tween1=this.game.add.tween(Anal_Fin).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Anal_Fin_text.visible=true;
      arrow_Anal_Fin.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_14",1);
      voice.play();
      dialog.y=15;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The anal fin is located on the ventral surface behind the anus. \nThis fin is used to stabilize the fish while swimming.";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.to_Fish_Caudal_Fin, this);
    }.bind(this));  
  },
  to_Fish_Caudal_Fin:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_15",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the caudal fin of the fish.";
    Caudal_Fin.inputEnabled = true;
    Caudal_Fin.input.useHandCursor = true;
    Caudal_Fin.events.onInputDown.add(this.Exp_Fish_Caudal_Fin, this);
    arrow.angle=-120;
    arrow.x=Caudal_Fin.x-40;
    arrow.y=Caudal_Fin.y+80;
    arrow.visible=true;
  },
  Exp_Fish_Caudal_Fin:function(){
    arrow.visible=false;
    Caudal_Fin.inputEnabled = false;
    Caudal_Fin.input.useHandCursor = false;
    tween1=this.game.add.tween(Caudal_Fin).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Caudal_Fin_text.visible=true;
      arrow_Caudal_Fin.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_16",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The caudal fin, or tail fin, is located at the end of a fish and provides the \npower to move a fish forward. It also acts as a rudder to help a fish steer.";
      this.game.time.events.add(Phaser.Timer.SECOND*10,this.to_Fish_Lateral_Line, this);
    }.bind(this));  
  },
  to_Fish_Lateral_Line:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_17",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the lateral line of the fish.";
    Lateral_Line.inputEnabled = true;
    Lateral_Line.input.useHandCursor = true;
    Lateral_Line.events.onInputDown.add(this.Exp_Fish_Lateral_Line, this);
    arrow.angle=0;
    arrow.x=Lateral_Line.x+30;
    arrow.y=Lateral_Line.y-40;
    arrow.visible=true;
  },
  Exp_Fish_Lateral_Line:function(){
    arrow.visible=false;
    Lateral_Line.inputEnabled = false;
    Lateral_Line.input.useHandCursor = false;
    tween1=this.game.add.tween(Lateral_Line).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Lateral_Line_text.visible=true;
      arrow_Lateral_Line.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_18",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="Lateral line present on the body helps the fish to sense the pressure of the water.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Fish_Scales, this);
    }.bind(this));  
  },
  to_Fish_Scales:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step3_19",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the scales of the fish.";
    Scales.inputEnabled = true;
    Scales.input.useHandCursor = true;
    Scales.events.onInputDown.add(this.Exp_Fish_Scales, this);
    arrow.x=Scales.x-50;
    arrow.y=Scales.y-80;
    arrow.visible=true;
  },
  Exp_Fish_Scales:function(){
    arrow.visible=false;
    Scales.inputEnabled = false;
    Scales.input.useHandCursor = false;
    tween1=this.game.add.tween(Scales).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Scales_text.visible=true;
      arrow_Scales.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step3_20",1);
      voice.play();
      dialog.y=45;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="Scales present on the body protect the fish from decay in water.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.NextExperiment, this);
      
    }.bind(this));  
  },
  NextExperiment:function(){
    Eye.visible=false;
    Terminal_mouth.visible=false;
    Operculum.visible=false;
    Pectoral_fin.visible=false;
    Pelvic_Fin.visible=false;
    Dorsal_fin.visible=false;
    Anal_Fin.visible=false;
    Caudal_Fin.visible=false;
    Lateral_Line.visible=false;
    Scales.visible=false;
    Fish_fullBody.alpha=1;
    voice.destroy(true);
    voice=this.game.add.audio("step3_21",1);
    voice.play();
    dialog.y=20;
    dialog.text="Click on the next button to see the characteristic features of the \nbony fish.";
    play.visible=true;
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    //this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_3",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
