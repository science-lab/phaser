var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
  voice=this.game.add.audio("Observations_1",1);
  //  voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(120,80,'observation_table');
  base.scale.setTo(1.1,1.1);
  
  //  base_line1= this.game.add.sprite(500,350,'observation_table_line');
  //  base_line1.scale.y=.6;
  // base_line2= this.game.add.sprite(1375,350,'observation_table_line');
  // base_line2.scale.y=.6;
  // base_line3= this.game.add.sprite(970,190,'observation_table_line');
  // base_line3.scale.y=.95;
  // base_line4= this.game.add.sprite(1330,190,'observation_table_line');
  // base_line4.scale.y=.95;
  
  //   base_line5= this.game.add.sprite(1402,320,'observation_table_line');
  //   base_line5.angle=90;
  //   base_line5.scale.setTo(1.1,1.1);
  // base_line6= this.game.add.sprite(1402,800,'observation_table_line');
  // base_line6.angle=90;
  // base_line6.scale.setTo(1.1,1.1);
  // base_line7= this.game.add.sprite(1760,490,'observation_table_line');
  // base_line7.angle=90;
  // base_line7.scale.setTo(1.1,2.08);
  // base_line8= this.game.add.sprite(1760,580,'observation_table_line');
  // base_line8.angle=90;
  // base_line8.scale.setTo(1.1,2.08);
  // base_line9= this.game.add.sprite(1760,670,'observation_table_line');
  // base_line9.angle=90;
  // base_line9.scale.setTo(1.1,2.08);
  // base_line10= this.game.add.sprite(1760,760,'observation_table_line');
  // base_line10.angle=90;
  // base_line10.scale.setTo(1.1,2.08);

  //base_line.scale.setTo(1,1.15);
    //head1_text=this.game.add.text(150,220,"Material (A) Spirogyra (Pond silk)",headfontStyle);
    //head2_text=this.game.add.text(200,320,"Classification",headfontStyle);
    //head3_text=this.game.add.text(1100,220,"Striated muscle fibres",headfontStyle); 
  //  //head4_text=this.game.add.text(1080,170,"Relative Size of \n    the Image",headfontStyle); 
  //  head5_text=this.game.add.text(1350,180,"Inference",headfontStyle); 
  //////////////////////////////////////////////////////////////////////////
   test1_text1="Phylum       :        Annelida";
   test1_text2="Habitat       :        Burrowing and Subsoil";
   test1_text3="";
   test1_text4="";
   test1_text5="";
   
   test1_text6="I. Characteristic features of the earthworm";
   test1_text7="1. Earthworm lives in soil and has burrowing habitat. So its body is elongated, \n    cylindrical and segmented which helps in easy penetration into the soil. Body \n    segments are called metameres. Body has a complete gut with mouth and anus. \n    Body cavity is true coelom. Segments are present on exterior and interior of body. \n    Segments are also called annuli or rings.";
   test1_text8="    Both male and female genital apertures are present on ventral side. Clitellum is \n    found on segments 14-16. It is glandular in nature and secretes mucus. In this \n    region segments are not distinct externally.";
   test1_text9="2. Anterior most segment of the body is pointed and it bears mouth. A small sensory \n    lobe is present over the mouth called prostomium which helps in sensing the soil \n    and its digging.";
   test1_text10="3. It has strong, circular and longitudinal body muscles which help in locomotion and \n    push the body into the burrow.";
   test1_text11="4. The colour of the earthworm is brown (like soil) which protects it from enemies.";
   test1_text12="5. Earthworm feeds upon soil with rich organic matter and also excretes fine soil and \n    undigested material as small rounded castings through anus.";
   test1_text13="6. The animal comes out of the burrow during rains and for copulation.";
   test1_text14="7. Skin is slippery and is kept moist due to mucus glands.";
   test1_text15="8. Earthworm respires through moist skin.";
   test1_text16="9. Soil does not stick to the body of the earthworm because of mucus present on its \n    skin.";
   test1_text17="10. Small chitinous hair-like bristles called setae present on the body of earthworm \n      attach to the surface of the soil and prevent backsliding of the animal.";
   test1_text18="11. The earthworm is flexible, as it has no skeleton.";
   test1_text19="12. Earthworm has no ears or eyes, but it is very sensitive to light and touch.";
   test1_text20="Features of Phylum Annelida as found in the earthworm";
   test1_text21="1. Metamerically segmented body with segments present both externally and \n    internally.";
   test1_text22="2. Setae and body wall muscles help in locomotion.";
   test1_text23="3. Moist skin helps in respiration.";
   test1_text24="Adaptive features with respect to its burrowing or subsoil habitat";
   test1_text25="1. Brown coloured skin is like the colour of the soil. It protects the animal from its \n    enemies.";
   test1_text26="2. Humus or rich organic matter is the food of the earthworm which is found in the \n    soil.";
   test1_text27="3. Long, flexible, cylindrical and symmetrical body, which helps it to make burrows in \n    the soil.";
   test1_text28="4. Longitudinal and circular muscles of the body walls help in making subsoil burrows \n    by digging the soil.";


   
  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
 next_btn = this.game.add.sprite(1640,830,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1640,830,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(250,830,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(250,830,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);
 if(!observation_completed)
  play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy(true);
              voice=this.game.add.audio("Observations_1",1);
              voice.play();
              base_line1= this.game.add.sprite(500,370,'observation_table_line');
              base_line1.scale.y=.35;
              base_line2= this.game.add.sprite(1375,370,'observation_table_line');
              base_line2.scale.y=.35;

              base_line5= this.game.add.sprite(1402,340,'observation_table_line');
              base_line5.angle=90;
              base_line5.scale.setTo(1.1,1.1);
              base_line6= this.game.add.sprite(1402,620,'observation_table_line');
              base_line6.angle=90;
              base_line6.scale.setTo(1.1,1.1);

              head1_text=this.game.add.text(180,240,"Earthworm",headfontStyle);
              //head2_text=this.game.add.text(200,340,"Classification",headfontStyle);

              test1_1=this.game.add.text(600,420,test1_text1,fontStyle);
              test1_2=this.game.add.text(600,500,test1_text2,fontStyle);
              test1_3=this.game.add.text(600,580,test1_text3,fontStyle);
              test1_4=this.game.add.text(600,660,test1_text4,fontStyle);
              test1_5=this.game.add.text(600,740,test1_text5,fontStyle);
              test1_6=this.game.add.text(220,630,"",fontStyle);

              break;
            case 2:
              voice.destroy(true);
              voice=this.game.add.audio("Observations_2",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text6,headfontStyle);
                test1_2=this.game.add.text(220,340,test1_text7,fontStyle);
                test1_3=this.game.add.text(220,630,test1_text8,fontStyle);
                test1_4=this.game.add.text(220,550,"",fontStyle);
                test1_5=this.game.add.text(220,630,"",fontStyle);
                test1_6=this.game.add.text(220,630,"",fontStyle);

            break;
            case 3:
              voice.destroy(true);
              voice=this.game.add.audio("Observations_3",1);
              voice.play();
               test1_1=this.game.add.text(220,240,test1_text9,fontStyle);
               test1_2=this.game.add.text(220,420,test1_text10,fontStyle);
               test1_3=this.game.add.text(220,550,test1_text11,fontStyle);
               test1_4=this.game.add.text(220,630,test1_text12,fontStyle);
               test1_5=this.game.add.text(220,760,test1_text13,fontStyle);
               test1_6=this.game.add.text(220,630,"",fontStyle);

            break;
            case 4:
              voice.destroy(true);
              voice=this.game.add.audio("Observations_4",1);
              voice.play();
               test1_1=this.game.add.text(220,240,test1_text14,fontStyle);
               test1_2=this.game.add.text(220,340,test1_text15,fontStyle);
               test1_3=this.game.add.text(220,420,test1_text16,fontStyle);
               test1_4=this.game.add.text(220,550,test1_text17,fontStyle);
               test1_5=this.game.add.text(220,680,test1_text18,fontStyle);
               test1_6=this.game.add.text(220,760,test1_text19,fontStyle);

            break;
            case 5:
              voice.destroy(true);
              voice=this.game.add.audio("Observations_5",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text20,headfontStyle);
               test1_2=this.game.add.text(220,340,test1_text21,fontStyle);
               test1_3=this.game.add.text(220,470,test1_text22,fontStyle);
               test1_4=this.game.add.text(220,550,test1_text23,fontStyle);
               test1_5=this.game.add.text(220,680,"",fontStyle);
               test1_6=this.game.add.text(220,760,"",fontStyle);

            break;
            case 6:
              voice.destroy(true);
              voice=this.game.add.audio("Observations_6",1);
              voice.play();
               test1_1=this.game.add.text(180,240,test1_text24,headfontStyle);
               test1_2=this.game.add.text(220,340,test1_text25,fontStyle);
               test1_3=this.game.add.text(220,470,test1_text26,fontStyle);
               test1_4=this.game.add.text(220,600,test1_text27,fontStyle);
               test1_5=this.game.add.text(220,730,test1_text28,fontStyle);
               test1_6=this.game.add.text(220,760,"",fontStyle);

            break;
          }
        },
        removeTexts:function(){
          
          if(pageCount==1){
            head1_text.destroy(true);
            //head2_text.destroy(true);
            base_line1.destroy(true);
            base_line2.destroy(true);
            base_line5.destroy(true);
            base_line6.destroy(true);
          }
            test1_1.destroy(true);
            test1_2.destroy(true);
            test1_3.destroy(true);
            test1_3.destroy(true);
            test1_4.destroy(true);
            test1_5.destroy(true);
            test1_6.destroy(true);
          
        },
      toNext:function(){
        this.removeTexts();
        if(pageCount<7){
          prev_btn.visible=true;
          pageCount++;
          console.log(pageCount);
          this.addObservation();
          if(pageCount>5){
            play.visible=true;
            next_btn.visible=false;  
          }
        }
      },
      toPrev:function(){
        this.removeTexts();
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  