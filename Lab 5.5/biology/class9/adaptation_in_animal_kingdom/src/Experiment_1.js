var experiment_1 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_1.prototype = {
  	create: function(){

hotFlag=false;
bg= this.game.add.sprite(0, 0,'bg');

// bgBlack= this.game.add.sprite(0,0,'bg_black');
// bgBlack.scale.setTo(1,1);
// bgBlack.alpha=0;
// bgBlack.visible=false;



// circle1 = this.game.add.graphics(0, 0);
// ////circle1.lineStyle(8, 0xffd900, 1);
// circle1.beginFill(0xFF0000, .3);
// circle1.drawCircle(902, 494, 430);
// //circle1.scale.set(0.5);
// Muscle_tissue.mask=circle1;
// circle1.visible=false;


voice=this.game.add.audio("step1_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// prev_btn = this.game.add.sprite(250,460,'components','next_pressed.png');
// prev_btn.scale.setTo(-.7,.7);
// prev_btn.inputEnabled = true;
// //prev_btn.input.priorityID = 3;
// prev_btn.input.useHandCursor = true;
// prev_btn.events.onInputDown.add(this.backToExp, this);
// prev_btn.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    
    label_box1 = this.game.add.graphics(0, 0);
    label_box1.lineStyle(5,0xE67E22,.6);
    label_box1.beginFill(0xffffff,1);//9B59B6
    label_box1.drawRect(780, 240, 1000, 800);
    // label_1=this.game.add.text(910,808,"Muscle cell",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    Earth_worm=this.game.add.sprite(350,670,'Earth_worm');
    Earth_worm.anchor.set(.5);
    Earth_worm.scale.set(.6);

    bottle=this.game.add.sprite(350,610,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(1.2,.8);

    name_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    name_box1.beginFill(0xE67E22,1);//9B59B6
    name_box1.drawRect(200, 920, 300, 60);
    name_1=this.game.add.text(255,928,"Earthworm",lablel_fontStyle);//870,930

    Earth_worm_fullBody=this.game.add.sprite(1330,650,'Earth_worm_parts','earth_worm.png');
    Earth_worm_fullBody.anchor.set(.5);
    Earth_worm_fullBody.scale.set(.8);
    Earth_worm_fullBody.alpha=.5;

    Mouth=this.game.add.sprite(1215,290,'Earth_worm_parts','mouth.png');
    Mouth.anchor.set(.5);
    Mouth.scale.set(.8);
    Mouth.alpha=0;
    mouth_text=this.game.add.text(1330,255,"Mouth",fontStyle_b);
    mouth_text.visible=false;
    arrow_mouth=this.game.add.sprite(1310,265,'arrow_1');
    arrow_mouth.visible=false;
    arrow_mouth.scale.x=-2;
    arrow_mouth.alpha=.75;

     female_genital_aperture=this.game.add.sprite(1214,435,'Earth_worm_parts','female_genital_aperture.png');//1283,405
     female_genital_aperture.anchor.set(.5);
     female_genital_aperture.scale.set(.8);
     female_genital_aperture.alpha=0;
     female_text=this.game.add.text(950,305,"Female genital \n     aperture",fontStyle_b);
     female_text.visible=false;
     arrow_female=this.game.add.sprite(1155,350,'arrow_1');
     arrow_female.visible=false;
     arrow_female.angle=45;
     arrow_female.scale.x=2;
     arrow_female.alpha=.75;

     clitellum_text=this.game.add.text(1330,415,"Clitellum",fontStyle_b);
     clitellum_text.visible=false;
     mark_clitellum=this.game.add.sprite(1233,426,'mark');
     mark_clitellum.scale.setTo(.5,.05);
     mark_clitellum.visible=false;
     arrow_clitellum=this.game.add.sprite(1315,425,'arrow_1');
     arrow_clitellum.visible=false;
     arrow_clitellum.scale.x=-1.5;
     arrow_clitellum.alpha=.75;

     male_genital_aperture=this.game.add.sprite(1219,475,'Earth_worm_parts','male_genital_aperture.png');
     male_genital_aperture.anchor.set(.5);
     male_genital_aperture.scale.set(.8);
     male_genital_aperture.alpha=0;
     male_text=this.game.add.text(1310,470,"Male genital \n   aperture",fontStyle_b);
     male_text.visible=false;
     arrow_male=this.game.add.sprite(1315,500,'arrow_1');
     arrow_male.visible=false;
     arrow_male.angle=20;
     arrow_male.scale.x=-2;
     arrow_male.alpha=.75;

     
     papillae_text=this.game.add.text(865,450,"Genital papillae",fontStyle_b);
     papillae_text.visible=false;
     mark_papillae=this.game.add.sprite(1200,450,'mark');
     mark_papillae.scale.setTo(-.5,.12);
     mark_papillae.visible=false;
     arrow_papillae=this.game.add.sprite(1118,462,'arrow_1');
     arrow_papillae.scale.x=1.5;
     arrow_papillae.alpha=.75;
     arrow_papillae.visible=false;
     
     Anus=this.game.add.sprite(1470,765,'Earth_worm_parts','anus.png');
     Anus.anchor.set(.5);
     Anus.scale.set(.8);
     Anus.alpha=0;
     anus_text=this.game.add.text(1580,727,"Anus",fontStyle_b);
     anus_text.visible=false;
     arrow_anus=this.game.add.sprite(1565,735,'arrow_1');
     arrow_anus.visible=false;
     arrow_anus.scale.x=-2;
     arrow_anus.alpha=.75;
     

     setae_text=this.game.add.text(1110,725,"Setae",fontStyle_b);
     setae_text.visible=false;
     arrow_setae=this.game.add.sprite(1210,735,'arrow_1');
     arrow_setae.visible=false;
     arrow_setae.scale.x=2;
     arrow_setae.alpha=.75;
     Setae=this.game.add.sprite(1330,650,'Earth_worm_parts','setae.png');
     Setae.anchor.set(.5);
     Setae.scale.set(.8);
     Setae.alpha=0;

    

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,5, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe the earthworm from museum specimen.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.loadScene, this);
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=45;

    darkBg = this.game.add.graphics(0, 0);
    
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_1",1);
    voice.play();
    dialog.text="Click on the mouth of the earthworm.";
    Mouth.inputEnabled = true;
    Mouth.input.useHandCursor = true;
    Mouth.events.onInputDown.add(this.Exp_Earth_worm_Mouth, this);
    arrow.x=Mouth.x+50;
    arrow.y=Mouth.y;
    arrow.angle=90;
    arrow.visible=true;
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
  Exp_Earth_worm_Mouth:function(){
    arrow.visible=false;
    Mouth.inputEnabled = false;
    Mouth.input.useHandCursor = false;
    tween1=this.game.add.tween(Mouth).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      mouth_text.visible=true;
      arrow_mouth.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_2",1);
      voice.play();
      dialog.y=20;
      dialog.text="The first segment of the earthworm, the peristomium, contains the \nmouth.";// There is a small tongue-like lobe just above the mouth called the prostomium.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Earth_worm_Female_genital_Aperture, this);
    }.bind(this));  
    
  },
  to_Earth_worm_Female_genital_Aperture:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step1_3",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the female genital aperture of the earthworm.";
    female_genital_aperture.inputEnabled = true;
    female_genital_aperture.input.useHandCursor = true;
    female_genital_aperture.events.onInputDown.add(this.Exp_Earth_worm_Female_genital_Aperture, this);
    arrow.x=female_genital_aperture.x+50;
    arrow.y=female_genital_aperture.y;
    arrow.visible=true;
  },
  Exp_Earth_worm_Female_genital_Aperture:function(){
    arrow.visible=false;
    female_genital_aperture.inputEnabled = false;
    female_genital_aperture.input.useHandCursor = false;
    tween1=this.game.add.tween(female_genital_aperture).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      female_text.visible=true;
      arrow_female.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_4",1);
      voice.play();
      dialog.y=15;
      dia_box.scale.setTo(.82,.9);
      dialog.text="A single medium female genital aperture is found on the ventral side \nof the 14th segment through which female reproductive bodies (ova) \nare discharged during copulation.";
      this.game.time.events.add(Phaser.Timer.SECOND*12,this.Exp_Earth_worm_Cletellum, this);
    }.bind(this));  
  },
  // to_Earth_worm_Cletellum:function(){
  //   Earth_worm_male_genital_Aperture.alpha=0.5;
  //   voice.destroy(true);
  //   voice=this.game.add.audio("step1",1);
  //   voice.play();
  //   dialog.text="Click on the clitellum of the earthworm.";
  //   Earth_worm_Cletellum.inputEnabled = true;
  //   Earth_worm_Cletellum.input.useHandCursor = true;
  //   Earth_worm_Cletellum.events.onInputDown.add(this.Exp_Earth_worm_Cletellum, this);
  //   arrow.x=Earth_worm_Cletellum.x+50;
  //   arrow.y=Earth_worm_Cletellum.y;
  //   arrow.visible=true;
  // },
  Exp_Earth_worm_Cletellum:function(){
    // arrow.visible=false;
    // Earth_worm_Cletellum.inputEnabled = false;
    // Earth_worm_Cletellum.input.useHandCursor = false;
    // tween1=this.game.add.tween(Earth_worm_Cletellum).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    mark_clitellum.visible=true;
    arrow_clitellum.visible=true;
    clitellum_text.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step1_5",1);
    voice.play();
    dialog.y=15;
    dia_box.scale.setTo(.82,1.15);
    dialog.text="The clitellum is a thickened glandular and non-segmented section of \nthe body wall near the head of the earthworm that secretes a viscid sac \nin which the eggs are deposited. The clitellum is part of the reproductive \nsystem of earthworms.";
    this.game.time.events.add(Phaser.Timer.SECOND*15,this.to_Earth_worm_male_genital_Aperture, this);
  },
  to_Earth_worm_male_genital_Aperture:function(){
    voice.destroy(true);
    //male_genital_aperture.alpha=0.5;
    voice=this.game.add.audio("step1_6",1);
    voice.play();
    dia_box.scale.setTo(.82,.7);
    dialog.y=45;
    dialog.text="Click on the male genital aperture of the earthworm.";
    male_genital_aperture.inputEnabled = true;
    male_genital_aperture.input.useHandCursor = true;
    male_genital_aperture.events.onInputDown.add(this.Exp_Earth_worm_male_genital_Aperture, this);
    arrow.x=male_genital_aperture.x+50;
    arrow.y=male_genital_aperture.y;
    arrow.visible=true;
  },
  Exp_Earth_worm_male_genital_Aperture:function(){
    arrow.visible=false;
    male_genital_aperture.inputEnabled = false;
    male_genital_aperture.input.useHandCursor = false;
    tween1=this.game.add.tween(male_genital_aperture).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      male_text.visible=true;
      arrow_male.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_7",1);
      voice.play();
      dialog.y=15;
      dia_box.scale.setTo(.82,.9);
      dialog.text="A pair of the male genital aperture is found on the ventrolateral side of \nthe 18th segment through which male reproductive bodies (sperm) are \ndischarged during copulation.";
      this.game.time.events.add(Phaser.Timer.SECOND*12,this.Exp_Earth_worm_Genital_Pappillae, this);
    }.bind(this));  
  },
  
  // to_Earth_worm_Genital_Pappillae:function(){
  //   Earth_worm_Cletellum.alpha=0.5;
  //   voice.destroy(true);
  //   voice=this.game.add.audio("step1",1);
  //   voice.play();
  //   dialog.text="Click on the genital papillae of the earthworm.";
  //   Earth_worm_Genital_Pappillae.inputEnabled = true;
  //   Earth_worm_Genital_Pappillae.input.useHandCursor = true;
  //   Earth_worm_Genital_Pappillae.events.onInputDown.add(this.Exp_Earth_worm_Genital_Pappillae, this);
  //   arrow.x=Earth_worm_Genital_Pappillae.x+50;
  //   arrow.y=Earth_worm_Genital_Pappillae.y+30;
  //   arrow.visible=true;
  // },
  Exp_Earth_worm_Genital_Pappillae:function(){
    arrow.visible=false;
    papillae_text.visible=true;
    arrow_papillae.visible=true;
    mark_papillae.visible=true;
    // Earth_worm_Genital_Pappillae.inputEnabled = false;
    // Earth_worm_Genital_Pappillae.input.useHandCursor = false;
    // tween1=this.game.add.tween(Earth_worm_Genital_Pappillae).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    voice.destroy(true);
    voice=this.game.add.audio("step1_8",1);
    voice.play();
    dia_box.scale.setTo(.82,.7);
    dialog.y=20;
    dialog.text="Genital papillae are two pairs of protuberance on segments 17 and 19 \non the ventral surface.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Earth_worm_Anus, this);
  },
  to_Earth_worm_Anus:function(){
    //Earth_worm_Genital_Pappillae.alpha=0.5;
    voice.destroy(true);
    voice=this.game.add.audio("step1_9",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the anus of the earthworm.";
    Anus.inputEnabled = true;
    Anus.input.useHandCursor = true;
    Anus.events.onInputDown.add(this.Exp_Earth_worm_Anus, this);
    arrow.x=Anus.x+50;
    arrow.y=Anus.y;
    arrow.visible=true;
  },
  Exp_Earth_worm_Anus:function(){
    arrow.visible=false;
    Anus.inputEnabled = false;
    Anus.input.useHandCursor = false;
    tween1=this.game.add.tween(Anus).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      anus_text.visible=true;
      arrow_anus.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_10",1);
      voice.play();
      dialog.y=20;
      dialog.text="The anus is a circular opening in the last segment called the anal \nsegment.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Earth_worm_Setae, this);
    }.bind(this));
  },
  to_Earth_worm_Setae:function(){
    //Earth_worm_Mouth.alpha=0.5;
    voice.destroy(true);
    voice=this.game.add.audio("step1_11",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the Setae of the earthworm.";
    Setae.inputEnabled = true;
    Setae.input.useHandCursor = true;
    Setae.events.onInputDown.add(this.Exp_Earth_worm_Setae, this);
    arrow.x=Setae.x+50;
    arrow.y=Setae.y;
    arrow.visible=true;
  },
  Exp_Earth_worm_Setae:function(){
    arrow.visible=false;
    Setae.inputEnabled = false;
    Setae.input.useHandCursor = false;
    tween1=this.game.add.tween(Earth_worm_fullBody).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      setae_text.visible=true;
      arrow_setae.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step1_12",1);
      voice.play();
      dialog.y=20;
      dialog.text="The earthworm moves using circular and longitudinal muscles, as well as \nbristles called setae.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.NextExperiment, this);
    }.bind(this));
  },
  NextExperiment:function(){
    Mouth.visible=false;
    female_genital_aperture.visible=false;
    male_genital_aperture.visible=false;
    Anus.visible=false;
    Earth_worm_fullBody.alpha=1;
    voice.destroy(true);
    voice=this.game.add.audio("step1_13",1);
    voice.play();
    dialog.text="Click on the next button to see the characteristic features of the \nearthworm.";
    play.visible=true;
  },
  
  
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    //this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  
	
	
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_1",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
