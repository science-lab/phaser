var experiment_2 = function(game){

var inst;


var delay;
var incr;
var currentobj;


}


experiment_2.prototype = {
  	create: function(){

hotFlag=false;
bg= this.game.add.sprite(0, 0,'bg');

// bgBlack= this.game.add.sprite(0,0,'bg_black');
// bgBlack.scale.setTo(1,1);
// bgBlack.alpha=0;
// bgBlack.visible=false;



// circle1 = this.game.add.graphics(0, 0);
// ////circle1.lineStyle(8, 0xffd900, 1);
// circle1.beginFill(0xFF0000, .3);
// circle1.drawCircle(902, 494, 430);
// //circle1.scale.set(0.5);
// Muscle_tissue.mask=circle1;
// circle1.visible=false;


voice=this.game.add.audio("step2_0",1);
this.game.sound.mute = false;

////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
righttop_bg.scale.setTo(.5,.5);
righttop_bg.angle=-90;

// Button panel -Quit button

quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
quitButton.scale.setTo(.7,.7);
quitButton.inputEnabled = true;
quitButton.input.useHandCursor = true;
quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button

homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
homeButton.scale.setTo(.7,.7);
homeButton.inputEnabled = true;
homeButton.input.useHandCursor = true;
homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button

muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
muteButton.scale.setTo(.7,.7);
muteButton.inputEnabled = true;
muteButton.input.useHandCursor = true;
muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button

volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
volumeButton.scale.setTo(.7,.7);
volumeButton.inputEnabled = true;
volumeButton.input.useHandCursor = true;
volumeButton.events.onInputDown.add(this.volume, this);
volumeButton.visible=false;

resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
resetButton.scale.setTo(.7,.7);
resetButton.inputEnabled = true;
resetButton.input.useHandCursor = true;
resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck

//leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
//leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

// if (!this.game.device.desktop)
// {
this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
/*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
normalScreen.scale.setTo(2.5,2.5);  
normalScreen.inputEnabled = true;
normalScreen.input.useHandCursor = true;
normalScreen.events.onInputUp.add(this.gonormal,this);*/
fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
fullScreen1.scale.setTo(2.5,2.5);
fullScreen1.inputEnabled = true;
fullScreen1.input.useHandCursor = true;
fullScreen1.events.onInputUp.add(this.gofull,this);    
//}

////////////////////////////////////////Font///////////////////////
fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
fontStyle_b={ font: "36px Segoe UI", fill: "#000000", align: "left" };
lablel_fontStyle={ font: "40px Segoe UI", fill: "#ffffFF", align: "left" };
headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };

/////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
interval=0;
incr=0;
currentobj=null;

this.addItems();



/////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////

play = this.game.add.sprite(1800,800,'components','play_pressed.png');
play.scale.setTo(.7,.7);
play.inputEnabled = true;
//play.input.priorityID = 3;
play.input.useHandCursor = true;
play.events.onInputDown.add(this.toExperiment2, this);
play.visible=false;

// Button panel group

buttonGroup=this.game.add.group();
buttonGroup.add(righttop_bg);
buttonGroup.add(muteButton);
buttonGroup.add(homeButton);
buttonGroup.add(quitButton);
buttonGroup.add(volumeButton);
buttonGroup.add(resetButton);
 
   
//}
 },
 //For to next scene
 
 toExperiment2:function()
 {
 voice.destroy();
 
 this.state.start("Observations_1", true, false, ip);
 },
gofull: function(){
    
    if (this.game.scale.isFullScreen)
      {
          this.game.scale.stopFullScreen();
      
      }else{
        this.game.scale.startFullScreen(false);
      }  
      
  },
  
  addItems:function(){
    
     
    collider=this.game.add.sprite(470,740, 'collider');//y-320
    this.game.physics.arcade.enable(collider);
    collider.anchor.set(.5);
    collider.scale.setTo(3,3);
    collider.inputEnabled = true;
    collider.enableBody = true;
    collider.alpha=0.0;

    
    label_box1 = this.game.add.graphics(0, 0);
    label_box1.lineStyle(5,0xE67E22,.6);
    label_box1.beginFill(0xffffff,1);//9B59B6
    label_box1.drawRect(780, 240, 1000, 800);
    // label_1=this.game.add.text(910,808,"Muscle cell",lablel_fontStyle);//870,930
    // microscopeGroup.add(label_box1);
    // microscopeGroup.add(label_1);

    Cockroach=this.game.add.sprite(350,650,'Cockroach');
    Cockroach.anchor.set(.5);
    Cockroach.scale.set(.6);

    bottle=this.game.add.sprite(350,580,'bottle');
    bottle.anchor.set(.5);
    bottle.scale.setTo(1.2,.8);

    name_box1 = this.game.add.graphics(0, 0);
    //box1.lineStyle(5,0xffffff,.6);
    name_box1.beginFill(0xE67E22,1);//9B59B6
    name_box1.drawRect(200, 900, 300, 60);
    name_1=this.game.add.text(255,908,"Cockroach",lablel_fontStyle);//870,930

    Cockroach_fullBody=this.game.add.sprite(1300,650,'Cokoroach_parts','Cockroach.png');
    Cockroach_fullBody.anchor.set(.5);
    Cockroach_fullBody.scale.set(.8);
    Cockroach_fullBody.alpha=.5;

    Head=this.game.add.sprite(1320,555,'Cokoroach_parts','Head.png');
    Head.anchor.set(.5);
    Head.scale.set(.8);
    Head.alpha=0.0;
    Head_text=this.game.add.text(1300,400,"Head",fontStyle_b);
    Head_text.visible=false;
    arrow_Head=this.game.add.sprite(1310,460,'arrow_1');
    arrow_Head.visible=false;
    arrow_Head.angle=-90;
    arrow_Head.scale.x=-2;
    arrow_Head.alpha=.75;

    Compound_eye_text=this.game.add.text(1425,460,"Compound eye",fontStyle_b);
    Compound_eye_text.visible=false;
    arrow_Compound_eye=this.game.add.sprite(1400,485,'arrow_1');
    arrow_Compound_eye.visible=false;
    arrow_Compound_eye.angle=-45;
    arrow_Compound_eye.scale.x=-2;
    arrow_Compound_eye.alpha=.75;

    Prothorax=this.game.add.sprite(1322,602,'Cokoroach_parts','Pro_thorax.png');
    Prothorax.anchor.set(.5);
    Prothorax.scale.set(.8);
    Prothorax.alpha=0.0;
    Prothorax_text=this.game.add.text(1430,525,"Prothorax",fontStyle_b);
    Prothorax_text.visible=false;
    arrow_Prothorax=this.game.add.sprite(1415,545,'arrow_1');
    arrow_Prothorax.visible=false;
    arrow_Prothorax.angle=-35;
    arrow_Prothorax.scale.x=-2;
    arrow_Prothorax.alpha=.75;

    Mesothorax=this.game.add.sprite(1331,673,'Cokoroach_parts','Mesothorax.png');
    Mesothorax.anchor.set(.5);
    Mesothorax.scale.set(.8);
    Mesothorax.alpha=0.0;
    Mesothorax_text=this.game.add.text(1430,585,"Mesothorax",fontStyle_b);
    Mesothorax_text.visible=false;
    arrow_Mesothorax=this.game.add.sprite(1415,610,'arrow_1');
    arrow_Mesothorax.visible=false;
    arrow_Mesothorax.angle=-20;
    arrow_Mesothorax.scale.x=-2;
    arrow_Mesothorax.alpha=.75;

    Metathorax_text=this.game.add.text(1430,635,"Metathorax",fontStyle_b);
    Metathorax_text.visible=false;
    arrow_Metathorax=this.game.add.sprite(1410,645,'arrow_1');
    arrow_Metathorax.visible=false;
    arrow_Metathorax.angle=-20;
    arrow_Metathorax.scale.x=-2;
    arrow_Metathorax.alpha=.75;

    mark_Thorax=this.game.add.sprite(1620,525,'mark');
    mark_Thorax.visible=false;
    mark_Thorax.scale.setTo(.5,.36);

    Thorax_text=this.game.add.text(1640,585,"Thorax",fontStyle_b);
    Thorax_text.visible=false;

    Walking_legs=this.game.add.sprite(1456,797,'Cokoroach_parts','Walking_legs.png');
    Walking_legs.anchor.set(.5);
    Walking_legs.scale.set(.8);
    Walking_legs.alpha=0.0;

    arrow_legs=this.game.add.sprite(1525,780,'arrow_1');
    arrow_legs.visible=false;
    arrow_legs.angle=70;
    arrow_legs.scale.x=-1.65;
    arrow_legs.alpha=.75;

    arrow_legs_1=this.game.add.sprite(1510,770,'arrow_1');
    arrow_legs_1.visible=false;
    arrow_legs_1.angle=-30;
    arrow_legs_1.scale.x=-2;
    arrow_legs_1.alpha=.75;
    legs_text=this.game.add.text(1520,755,"Walking legs",fontStyle_b);
    legs_text.visible=false;

    Antenna=this.game.add.sprite(1192,450,'Cokoroach_parts','Antenna.png');
    Antenna.anchor.set(.5);
    Antenna.scale.set(.8);
    Antenna.alpha=0.0;

    arrow_Antenna=this.game.add.sprite(1125,427,'arrow_1');
    arrow_Antenna.visible=false;
    //arrow_Antenna.angle=70;
    arrow_Antenna.scale.x=2;
    arrow_Antenna.alpha=.75;
    Antenna_text=this.game.add.text(980,415,"Antenna",fontStyle_b);
    Antenna_text.visible=false;

    Forewings=this.game.add.sprite(1130,647,'Cokoroach_parts','Forewings.png');
    Forewings.anchor.set(.5);
    Forewings.scale.set(.8);
    Forewings.alpha=0.0;
    arrow_Forewings=this.game.add.sprite(1040,550,'arrow_1');
    arrow_Forewings.visible=false;
    arrow_Forewings.angle=30;
    arrow_Forewings.scale.x=2;
    arrow_Forewings.alpha=.75;
    Forewings_text=this.game.add.text(860,530,"Forewings",fontStyle_b);
    Forewings_text.visible=false;

    Hindwings=this.game.add.sprite(1135,780,'Cokoroach_parts','Hindwings.png');
    Hindwings.anchor.set(.5);
    Hindwings.scale.set(.8);
    Hindwings.alpha=0.0;
    arrow_Hindwings=this.game.add.sprite(1000,720,'arrow_1');
    arrow_Hindwings.visible=false;
    arrow_Hindwings.angle=20;
    arrow_Hindwings.scale.x=2;
    arrow_Hindwings.alpha=.75;
    Hindwings_text=this.game.add.text(820,700,"Hindwings",fontStyle_b);
    Hindwings_text.visible=false;

    Abdomen=this.game.add.sprite(1335,790,'Cokoroach_parts','Abdomen.png');
    Abdomen.anchor.set(.5);
    Abdomen.scale.set(.8);
    Abdomen.alpha=0.0;
    arrow_Abdomen=this.game.add.sprite(1215,940,'arrow_1');
    arrow_Abdomen.visible=false;
    arrow_Abdomen.angle=-65;
    arrow_Abdomen.scale.x=3;
    arrow_Abdomen.alpha=.75;
    Abdomen_text=this.game.add.text(1060,930,"Abdomen",fontStyle_b);
    Abdomen_text.visible=false;

    Anal_Cerci=this.game.add.sprite(1320,900,'Cokoroach_parts','Anal_Cerci.png');
    Anal_Cerci.anchor.set(.5);
    Anal_Cerci.scale.set(.8);
    Anal_Cerci.alpha=0.0;

    arrow_Anal=this.game.add.sprite(1320,985,'arrow_1');
    arrow_Anal.visible=false;
    arrow_Anal.angle=-120;
    arrow_Anal.scale.x=2;
    arrow_Anal.alpha=.75;
    arrow_Anal_1=this.game.add.sprite(1320,975,'arrow_1');
    arrow_Anal_1.visible=false;
    arrow_Anal_1.angle=-75;
    arrow_Anal_1.scale.x=1.7;
    arrow_Anal_1.alpha=.75;
    Anal_text=this.game.add.text(1330,970,"Anal Cerci",fontStyle_b);
    Anal_text.visible=false; 



     //The abdomen is the final piece, and it contains reproductive organs. At the back of the abdomen are two short protrusions called cerci. 


    

    arrow=this.game.add.sprite(200,200, 'arrow','arrow_0001.png');
    arrow.animations.add('anim',[1,2,3,4,5,6,7,8,9,10],24, true, true);
    arrow.animations.play('anim');
    arrow.anchor.set(.5);
    arrow.visible=false;
    arrow.state="";

    
    dia_box=this.game.add.sprite(30,5, 'dialogue_box1');
    dia_box.scale.setTo(.82,.7);
    dia_text="Let's observe the cockroach from museum specimen.";
    this.game.time.events.add(Phaser.Timer.SECOND*3,this.loadScene, this);
    //dia_text="Take a small amount of Sulphur \ninto the empty plate using \na spoon.";
    dialog=this.game.add.text(50,50,dia_text,fontStyle);
    
    dialog.y=45;

    darkBg = this.game.add.graphics(0, 0);
    
    //darkBg.lineStyle(5,0xE67E22,.6);
    darkBg.beginFill(0x000000,1);//9B59B6
    darkBg.drawRect(0, 0, 1920, 1080);
    darkBg.alpha=0;
    darkBg.visible=false;
    //darkBg.x=600;
    //darkBg.y=570;
    //darkBg.visible=false;
    //dayFlag=true;
  },
  loadScene:function(){
    //console.log("lllllllllll");
    voice.play();
    this.game.time.events.add(Phaser.Timer.SECOND*4,this.startExperiment, this); 
  },
  startExperiment:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_1",1);
    voice.play();
    dialog.text="Click on the head of the cockroach.";
    Head.inputEnabled = true;
    Head.input.useHandCursor = true;
    Head.events.onInputDown.add(this.Exp_cockroach_Head, this);
    arrow.x=Head.x+50;
    arrow.y=Head.y;
    arrow.angle=90;
    arrow.visible=true;
    // 
    //this.game.time.events.add(Phaser.Timer.SECOND*2,this.TakeSeed, this); 
    //this.clickOnEyePiece();
    ///////////////////////shortcut////////////////////
    
  },
  Exp_cockroach_Head:function(){
    arrow.visible=false;
    Head.inputEnabled = false;
    Head.input.useHandCursor = false;
    tween1=this.game.add.tween(Head).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      Head_text.visible=true;
      arrow_Head.visible=true;
      voice.destroy(true);
      voice=this.game.add.audio("step2_2",1);
      voice.play();
      dialog.y=45;
      dialog.text="This is the head of the cockroaches. This is called Hypognathous.";
      this.game.time.events.add(Phaser.Timer.SECOND*5,this.Exp_Compound_eye, this);
    }.bind(this));  
    
   },
  
   Exp_Compound_eye:function(){
    arrow.visible=false;
    Compound_eye_text.visible=true;
    arrow_Compound_eye.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_3",1);
    voice.play();
    dialog.y=20;
    //dia_box.scale.setTo(.82,.9);
    dialog.text="The head bears compound eyes which helps the cockroaches to see in \nall directions.";
    this.game.time.events.add(Phaser.Timer.SECOND*5,this.to_Prothorax, this);
      
  },
  to_Prothorax:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_4",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the prothorax of the cockroach.";
    Prothorax.inputEnabled = true;
    Prothorax.input.useHandCursor = true;
    Prothorax.events.onInputDown.add(this.Exp_Prothorax, this);
    arrow.x=Prothorax.x+50;
    arrow.y=Prothorax.y;
    arrow.visible=true;
  },
  Exp_Prothorax:function(){
    arrow.visible=false;
    Prothorax.inputEnabled = false;
    Prothorax.input.useHandCursor = false;
    Prothorax_text.visible=true;
    arrow_Prothorax.visible=true;
    tween1=this.game.add.tween(Prothorax).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_5",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The prothorax is the anterior segment of the thorax of a cockroach \nand this segment does not bear any wings.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Mesothorax, this);
    }.bind(this));  
      
  },
  to_Mesothorax:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_6",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the mesothorax of the cockroach.";
    Mesothorax.inputEnabled = true;
    Mesothorax.input.useHandCursor = true;
    Mesothorax.events.onInputDown.add(this.Exp_Mesothorax, this);
    arrow.x=Mesothorax.x+50;
    arrow.y=Mesothorax.y;
    arrow.visible=true;
  },
  Exp_Mesothorax:function(){
    arrow.visible=false;
    Mesothorax.inputEnabled = false;
    Mesothorax.input.useHandCursor = false;
    Mesothorax_text.visible=true;
    arrow_Mesothorax.visible=true;
    tween1=this.game.add.tween(Mesothorax).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_7",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The mesothorax is the middle of the three segments in the thorax of a \ncockroach and bears the second pair of legs.";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.Exp_Metathorax, this);
    }.bind(this));    
  },
  Exp_Metathorax:function(){
    arrow.visible=false;
    Metathorax_text.visible=true;
    arrow_Metathorax.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_8",1);
    voice.play();
    dialog.y=20;
    //dia_box.scale.setTo(.82,.9);
    dialog.text="The metathorax is the posterior of the three segments in the thorax of a \ncockroach and bears the third pair of legs.";
    this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Walking_legs, this);
      
  },
  to_Walking_legs:function(){
    mark_Thorax.visible=true;
    Thorax_text.visible=true;
    voice.destroy(true);
    voice=this.game.add.audio("step2_9",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the walking legs of the cockroach.";
    Walking_legs.inputEnabled = true;
    Walking_legs.input.useHandCursor = true;
    Walking_legs.events.onInputDown.add(this.Exp_Walking_legs, this);
    arrow.angle=0;
    arrow.x=Walking_legs.x;
    arrow.y=Walking_legs.y;
    arrow.visible=true;
  },
  Exp_Walking_legs:function(){
    arrow.visible=false;
    Walking_legs.inputEnabled = false;
    Walking_legs.input.useHandCursor = false;
    legs_text.visible=true;
    arrow_legs.visible=true;
    arrow_legs_1.visible=true;
    tween1=this.game.add.tween(Walking_legs).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_10",1);
      voice.play();
      dialog.y=45;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The cockroach has three pairs of legs which help it to move.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Antenna, this);
    }.bind(this));  
  },
  to_Antenna:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_11",1);
    voice.play();
    dialog.text="Click on the antenna of the cockroach.";
    Antenna.inputEnabled = true;
    Antenna.input.useHandCursor = true;
    Antenna.events.onInputDown.add(this.Exp_Antenna, this);
    arrow.x=Antenna.x+20;
    arrow.y=Antenna.y-50;
    arrow.visible=true;
  },
  Exp_Antenna:function(){
    arrow.visible=false;
    Antenna.inputEnabled = false;
    Antenna.input.useHandCursor = false;
    Antenna_text.visible=true;
    arrow_Antenna.visible=true;
    tween1=this.game.add.tween(Antenna).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_12",1);
      voice.play();
      dialog.text="Cockroach has two long antennae which help it to sense.";
      this.game.time.events.add(Phaser.Timer.SECOND*4,this.to_Forewings, this);
    }.bind(this));
  },
  to_Forewings:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_13",1);
    voice.play();
    dialog.text="Click on the forewings of the cockroach.";
    Forewings.inputEnabled = true;
    Forewings.input.useHandCursor = true;
    Forewings.events.onInputDown.add(this.Exp_Forewings, this);
    arrow.x=Forewings.x+20;
    arrow.y=Forewings.y-80;
    arrow.visible=true;
  },
  Exp_Forewings:function(){
    arrow.visible=false;
    Forewings.inputEnabled = false;
    Forewings.input.useHandCursor = false;
    Forewings_text.visible=true;
    arrow_Forewings.visible=true;
    tween1=this.game.add.tween(Forewings).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_14",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The forewings are tough covers (tegmina) that protect the delicate \nflight wings when the cockroach is at rest.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Hindwings, this);
    }.bind(this));
  },
  to_Hindwings:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_15",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the hindwings of the cockroach.";
    Hindwings.inputEnabled = true;
    Hindwings.input.useHandCursor = true;
    Hindwings.events.onInputDown.add(this.Exp_Hindwings, this);
    arrow.x=Hindwings.x-100;
    arrow.y=Hindwings.y-50;
    arrow.visible=true;
  },
  Exp_Hindwings:function(){
    arrow.visible=false;
    Hindwings.inputEnabled = false;
    Hindwings.input.useHandCursor = false;
    Hindwings_text.visible=true;
    arrow_Hindwings.visible=true;
    tween1=this.game.add.tween(Hindwings).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_16",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The hind wings are large, thin, membranous and transparent. \nThey are kept folded below the tegmina and are used for flying.";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.to_Abdomen, this);
    }.bind(this));
  },
  to_Abdomen:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_17",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the abdomen of the cockroach.";
    Abdomen.inputEnabled = true;
    Abdomen.input.useHandCursor = true;
    Abdomen.events.onInputDown.add(this.Exp_Abdomen, this);
    arrow.angle=-110;
    arrow.x=Abdomen.x-80;
    arrow.y=Abdomen.y+50;
    arrow.visible=true;
  },
  Exp_Abdomen:function(){
    arrow.visible=false;
    Abdomen.inputEnabled = false;
    Abdomen.input.useHandCursor = false;
    Abdomen_text.visible=true;
    arrow_Abdomen.visible=true;
    tween1=this.game.add.tween(Abdomen).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_18",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The abdomen is the last part, which is divided into 10 parts, \nwhich contains the reproductive organs.";
      this.game.time.events.add(Phaser.Timer.SECOND*7,this.to_Anal_Cerci, this);
    }.bind(this));
  },
  to_Anal_Cerci:function(){
    voice.destroy(true);
    voice=this.game.add.audio("step2_19",1);
    voice.play();
    dialog.y=45;
    dialog.text="Click on the anal cerci of the cockroach.";
    Anal_Cerci.inputEnabled = true;
    Anal_Cerci.input.useHandCursor = true;
    Anal_Cerci.events.onInputDown.add(this.Exp_Anal_Cerci, this);
    arrow.angle=90;
    arrow.x=Anal_Cerci.x+70;
    arrow.y=Anal_Cerci.y-5;
    arrow.visible=true;
  },
  Exp_Anal_Cerci:function(){
    arrow.visible=false;
    Anal_Cerci.inputEnabled = false;
    Anal_Cerci.input.useHandCursor = false;
    Anal_text.visible=true;
    arrow_Anal.visible=true;
    arrow_Anal_1.visible=true;
    tween1=this.game.add.tween(Anal_Cerci).to( {alpha:1}, 400, Phaser.Easing.Linear.Out, true);
    tween1.onComplete.add(function () {
      voice.destroy(true);
      voice=this.game.add.audio("step2_20",1);
      voice.play();
      dialog.y=20;
      //dia_box.scale.setTo(.82,.9);
      dialog.text="The last segment of its body has two small wind sensitive hair called \nanal cerci which warm it against the enemy.";
      this.game.time.events.add(Phaser.Timer.SECOND*8,this.NextExperiment, this);
      
    }.bind(this));
  },
  NextExperiment:function(){
    Head.visible=false;
    Prothorax.visible=false;
    Mesothorax.visible=false;
    Walking_legs.visible=false;
    Antenna.visible=false;
    Forewings.visible=false;
    Hindwings.visible=false;
    Abdomen.visible=false;
    Anal_Cerci.visible=false;
    Cockroach_fullBody.alpha=1;
    voice.destroy(true);
    voice=this.game.add.audio("step2_21",1);
    voice.play();
    dialog.text="Click on the next button to see the characteristic features of the \ncockroach.";
    play.visible=true;
  },
  drawVLine:function(xp,yp,xp1,yp1){
    var line1=this.game.add.graphics(0, 0);
        line1.lineStyle(5,0x0000FF,1);
        line1.moveTo(xp, yp);
        line1.lineTo(xp1, yp1);
        lineGroup.add(line1);
        return(line1);
        
  },
  
  timerFunction:function(){
    TimerCounter++;
    sec=TimerCounter
    timerText.text=min+":"+sec;

    if(TimerCounter>=60){
      TimerCounter=0;
      min++;
    }
  },

	
 
	update: function()
	{
    DeltaTime=this.game.time.elapsed/1000;
    //this.detectCollision();
    interval++;
    if(interval>300){
      interval=0;
      //console.log(this.game.input.activePointer.x+"/../"+this.game.input.activePointer.y);
    }
    
    
	},
  
	muteTheGame:function(){
  muted = true;
//   voice.stop();
   this.game.sound.mute = true;
   volumeButton.visible = true;
   muteButton.visible = false;

 },
 volume:function(){

   this.game.sound.mute = false;
   volumeButton.visible = false;
   muteButton.visible = true;
 },
resetTheGame:function(){
  voice.destroy();
  //voice2.destroy();
  //voice3.destroy();
  //voice5.destroy();
  //this.state.start("Simulation", true, false, ip);

    this.state.start("Experiment_2",true,false,ip);
 },
 gotoHome:function(){


  voice.destroy();
  // voice2.destroy();
  // voice3.destroy();
  //voice5.destroy();
  this.state.start("Aim", true, false, ip);
   // window.location="../";
 },

postData:function()
 {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () {
     console.log(xhr.readyState);
     console.log(xhr.status);
       if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201))){
           var json = JSON.parse(xhr.responseText);
           console.log(json);
             window.open(loc,"_self");
       }
       else {
         console.log('fail');
         window.open(loc,"_self");
       }
   };
 },
closeTheGame:function(){

   voice.destroy();
  //  voice2.destroy();
  // voice3.destroy();
//this.postData();

  //voice5.destroy();
  window.open(loc,"_self");
  // window.open("http://swadhyaya/theme/essential/layout/creatnlrn/third_phy.php?id=4","_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },



}
