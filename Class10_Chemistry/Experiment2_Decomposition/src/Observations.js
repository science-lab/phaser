var observations = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  
  }
  
  observations.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/

  pageCount=1;
  nextSoundDelay=0;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_disabled.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  //quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(110,80,'observation_table_decomposition');
  exp_Name="decomposition_reaction";

  switch(exp_Name){
    case "decomposition_reaction":
      sno_1="1";
      experiment_1="On heating";
      observation_1="1. A colorless gas, which\nhas a smell of burning\nsulphur is produced.";
      inference_1="The production of SO\u2082 and \nSO\u2083causes the smell of burning \nsulphur duringthe experiment.";
      sno_2="2";
      experiment_2="On further heating";
      observation_2="The color of the residue \nleft behind in the test \ntube is reddish brown.";
      inference_2="Since ferrous sulphate crystals\non heating give more than one\ncompound as a product, it is a\ndecomposition reaction. The \nreaction taking place is : \n2FeSO\u2084(s)------> Fe\u2082O\u2083 (s) +\nSO\u2082(g) + SO\u2083 (g)";
      sno_text_1=this.game.add.text(175,320,"",fontStyle);
      experiment_text_1=this.game.add.text(350,320,"",fontStyle);
      observation_text_1=this.game.add.text(670,320,"",fontStyle);
      
      inference_text_1=this.game.add.text(1130,320,"",fontStyle);
      //observation_img_1=this.game.add.sprite(780,420,'observation_img_1');
      //observation_img_1.scale.setTo(1.2,1.2);

      sno_text_2=this.game.add.text(175,530,"",fontStyle);
      experiment_text_2=this.game.add.text(290,320,"",fontStyle);
      observation_text_2=this.game.add.text(670,550,"",fontStyle);
      inference_text_2=this.game.add.text(1130,550,"",fontStyle);
     // observation_img_2=this.game.add.sprite(780,730,'observation_img_2');
      //observation_img_2.scale.setTo(1.2,1.2);
      voice=this.game.add.audio("observation1_snd",1);
      //voice2=this.game.add.audio("observation2_snd",1);
      voice.play();
      //voice.onStop.add(this.nextSound,this);
    break;
    case "doubledisplacement_reaction":
    
    break;

  }  
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
  this.addObservation();
  next_btn = this.game.add.sprite(1590,850,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1590,850,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(230,850,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(230,850,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;
 play = this.game.add.sprite(1800,840,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toResult, this);


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
    addObservation:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
               voice.destroy();
              // voice=this.game.add.audio("A_procedure1",1);
              // voice.play();
              voice=this.game.add.audio("observation1_snd",1);
              voice.play();
              sno_text_1.text="1";
              experiment_text_1.text="On heating";
              observation_text_1.text="1. A colorless gas, which\n    has a smell of burning \n    sulphur is produced.";
              observation_text_2.text="2. Water droplets are \n    seen on the mouth of \n    the test tube."
              
              inference_text_1.text="1. The production of SO\u2082 and \n    SO\u2083 causes the smell of \n    burning sulphur during the \n    experiment.";
              inference_text_2.text="2. The salt contains water of \n   crystallization";
              experiment_text_2.text="";
              break;
            case 2:
              nextSoundDelay=0;
              voice.destroy();
              voice=this.game.add.audio("observation2_snd",1);
              voice.play();
              sno_text_1.text="2";
              experiment_text_2.text="On further heating";
              observation_text_1.text="The color of the residue \nleft behind in the test \ntube is reddish brown.";
              inference_text_1.text="Since ferrous sulphate crystals\non heating give more than one\ncompound as a product, it is a\ndecomposition reaction. The \nreaction taking place is : \n2FeSO\u2084(s)------> Fe\u2082O\u2083 (s) +\nSO\u2082(g) + SO\u2083 (g)";
              observation_text_2.text="";
              inference_text_2.text="";
              experiment_text_1.text="";
            break;
            
          }
        },
        toNext:function(){
          if(pageCount<3){
            prev_btn.visible=true;
            pageCount++;
              this.addObservation();
              if(pageCount>=2){
                play.visible=true;
                next_btn.visible=false;  
              }
            }
      },
      toPrev:function(){
          if(pageCount>1){
            next_btn.visible=true;
            pageCount--;
            this.addObservation();
              if(pageCount<=1){
                prev_btn.visible=false;  
              }
            }
      },
    //For to next scene   
   
        toResult:function()
        {
        voice.destroy();
        //voice2.destroy();
        this.state.start("Result", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        // voice.destroy();
        // voice2.destroy();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
        //voice2.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
        //voice2.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  //window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  