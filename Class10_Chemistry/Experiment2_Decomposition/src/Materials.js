var materials = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  var currentScene;
  var previousScene;
  var total_scene;
  
  }
  
  materials.prototype ={
  
  init: function(ipadrs) 
  {
    ip = ipadrs;
    

  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 // voice=this.game.add.audio("fobj",1);
 //voice.play();
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_disabled.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  //quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Materials /////////////////////////////////////
  currentScene=1;
  previousScene=0;
  
  base= this.game.add.sprite(160,140,'dialogue_box')
  this.showMaterials();



  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 next_btn = this.game.add.sprite(1600,870,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1600,870,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);

 prev_btn = this.game.add.sprite(300,870,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(300,870,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 prev_btn.visible=false;
 //next_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrevious, this);

 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toTheory, this);
 play.visible=false;
  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
},
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
  
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
      showMaterials:function(){
        
       if(exp_Name=="decomposition_reaction") {
        total_scene=6;
        this.show_decomposition_materials();
       }else if(exp_Name=="doubledisplacement_reaction") {
        total_scene=5;
        this.show_doubledisplacement_materials();
       }else if(exp_Name=="combination_reaction") {
        total_scene=5;
        this.show_combination_materials();
       }else if(exp_Name=="displacement_reaction") {
        total_scene=5;
        this.show_displacement_reaction();
       }

        
      },
      show_displacement_reaction:function(){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);
              }
              mat_img_1 = this.game.add.sprite(250,270,'thread');
              mat_img_1.scale.setTo(.6,.6);
              mat_name_1=this.game.add.text(650,270,"Thread:",headfontStyle);
              mat_content_1="A fibre is a thin thread of a natural or artificial \nsubstance, especially one that is used to make cloth \nor rope.";
              mat_text_1=this.game.add.text(650,370,mat_content_1,fontStyle);


              mat_img_2 = this.game.add.sprite(350,680,'sand_paper');
              mat_img_2.scale.setTo(.4,.4);
              mat_name_2=this.game.add.text(750,620,"Sand paper:",headfontStyle);
              mat_content_2="Sandpaper and glasspaper are names used \nfor a type of coated abrasive that consists \nof sheets of paper or cloth with abrasive \nmaterial glued to one face.";
              mat_text_2=this.game.add.text(750,720,mat_content_2,fontStyle);


          break;
          case 2:
              
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
                if(previousScene==1){
                  mat_img_2.destroy(true);
                  mat_name_2.destroy(true);
                  mat_text_2.destroy(true);
                }
              mat_img_1 = this.game.add.sprite(270,400,'iron_nail');
              mat_img_1.scale.setTo(.6,.6);
              mat_name_1=this.game.add.text(650,350,"Iron nail:",headfontStyle);
              mat_content_1="A nail is a small object made of metal that is used \nas a fastener, as a peg to hang something, \nor sometimes as a decoration. Generally, nails \nhave a sharp point on one end and a flattened \nhead on the other, but headless nails are available.";
              mat_text_1=this.game.add.text(650,450,mat_content_1,fontStyle);


          break;
          case 3:
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(330,350,'test_tube');
              mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Test tube:",headfontStyle);
              mat_content_1="A test tube, also known as a culture tube or sample tube, \nis a common piece of laboratory glassware consisting of \na finger-like length of glass or clear plastic tubing, open \nat the top and closed at the bottom. Test tubes are \nusually placed in special-purpose racks.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);
                
              
              
             

          break;
          case 4:

                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);

              mat_img_1 = this.game.add.sprite(270,300,'coper_sulphate');
              mat_img_1.scale.setTo(.6,.6);
              mat_name_1=this.game.add.text(650,300,"Copper sulphate:",headfontStyle);
              mat_content_1="Copper sulphate, blue stone, blue vitriol are all \ncommon names for pentahydrated cupric sulphate, \nCuSO45H2O, which is the best known and the most \nwidely used of the copper salts. Indeed it is often \nthe starting raw material for the production of many \nof the other copper salts.";
              mat_text_1=this.game.add.text(650,400,mat_content_1,fontStyle); 


              /*mat_img_1 = this.game.add.sprite(300,350,'stand');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(600,400,"Clamp stand:",headfontStyle);
              mat_content_1="In chemistry, a retort stand, also called a clamp stand, \na ring stand, or a support stand, is a piece of scientific \nequipment intended to support other pieces of \nequipment and glassware — for instance, burettes, \ntest tubes and flasks.";
              mat_text_1=this.game.add.text(600,500,mat_content_1,fontStyle);
*/
          break;
          case 5:

                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);

              mat_img_1 = this.game.add.sprite(300,350,'stand');
              //mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(600,400,"Clamp stand:",headfontStyle);
              mat_content_1="In chemistry, a retort stand, also called a clamp stand, \na ring stand, or a support stand, is a piece of scientific \nequipment intended to support other pieces of \nequipment and glassware — for instance, burettes, \ntest tubes and flasks.";
              mat_text_1=this.game.add.text(600,500,mat_content_1,fontStyle);

          break;

        }
      },
      show_decomposition_materials:function(){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
              }
              mat_img_1 = this.game.add.sprite(370,550,'wood_rack');
              mat_img_1.scale.setTo(.7,.7);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Test tube rack:",headfontStyle);
              mat_content_1="Test tube racks are laboratory equipment used to \nhold upright multiple test tubes at the same time. \nThey are most commonly used when various different \nsolutions are needed to work with simultaneously, \nfor safety reasons, for safe storage of test tubes, \nand to ease the transport of multiple tubes.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

              // mat_img_2 = this.game.add.sprite(330,550,'test_tube');
              // mat_img_2.scale.setTo(.9,1);
              // mat_name_2=this.game.add.text(550,570,"Test tube:",headfontStyle);
              // mat_content_2="A test tube, also known as a culture tube or sample tube, \nis a common piece of laboratory glassware consisting of \na finger-like length of glass or clear plastic tubing, open \nat the top and closed at the bottom. Test tubes are \nusually placed in special-purpose racks.";
              // mat_text_2=this.game.add.text(550,670,mat_content_2,fontStyle);
          break;
          case 2:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              // mat_img_1 = this.game.add.sprite(230,350,'burner');

              mat_img_1 = this.game.add.sprite(370,550,'testtube');
              //mat_img_1.scale.setTo(.8,.8);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Test tube:",headfontStyle);
              mat_content_1="A test tube, also known as a culture tube or sample tube, \nis a common piece of laboratory glassware consisting of \na finger-like length of glass or clear plastic tubing, open \nat the top and closed at the bottom. Test tubes are \nusually placed in special-purpose racks.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

              //mat_img_1.scale.setTo(.9,1);
              // mat_name_1=this.game.add.text(650,400,"Bunsen burner:",headfontStyle);
              // mat_content_1="A Bunsen burner, named after Robert Bunsen, is a \nkind of gas burner used as laboratory equipment; \nit produces a single open gas flame and is used for \nheating, sterilization, and combustion.";
              // mat_text_1=this.game.add.text(650,500,mat_content_1,fontStyle);

              // mat_img_2.destroy(true);
              // mat_name_2.destroy(true);
              // mat_text_2.destroy(true);
          break;
          case 3:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);

              mat_img_1 = this.game.add.sprite(370,550,'FS');
              mat_img_1.scale.setTo(.8,.8);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Ferrous sulphate:",headfontStyle);
              mat_content_1="Ferrous sulphate is a compound of iron, sulphur and \noxygen. It is pale green in colour.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);
              if(previousScene==4){
                // mat_img_2.destroy(true);
                // mat_name_2.destroy(true);
                // mat_text_2.destroy(true);
              }
              // mat_img_2 = this.game.add.sprite(250,670,'spoon');
              // mat_img_2.scale.setTo(.9,1);
              // mat_img_2.angle=45;
              // mat_name_2=this.game.add.text(540,660,"Spoon:",headfontStyle);
              // mat_content_2="A spoon is a utensil consisting of a small shallow bowl, \noval or round, at the end of a handle. A type of cutlery, \nespecially as part of a place setting, it is used \nprimarily for transferring items.";
              // mat_text_2=this.game.add.text(540,750,mat_content_2,fontStyle);
          break;
          case 4:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'spoon');
              //mat_img_1.scale.setTo(.8,.8);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Spatula:",headfontStyle);
              mat_content_1="Spatula is small stainless steel utensils, used for scraping,\ntransferring, or applying powders and paste-like chemicals or\ntreatments. Many spatula brands are also resistant to acids,\nbases, heat, and solvents, which make them ideal for use with\na wide range of compounds.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);
            
              // mat_img_2.destroy(true);
              // mat_name_2.destroy(true);
              // mat_text_2.destroy(true);
              
          break;
          case 5:
              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              mat_img_1 = this.game.add.sprite(370,550,'testtube_holder');
              mat_img_1.scale.setTo(.7,.7);
              mat_img_1.anchor.setTo(.5,.5);
              mat_name_1=this.game.add.text(600,270,"Test tube holder:",headfontStyle);
              //mat_content_1="In chemistry, a retort stand, also called a clamp stand, \na ring stand, or a support stand, is a piece of scientific \nequipment intended to support other pieces of \nequipment and glassware for instance, burettes, \ntest tubes and flasks.";
              mat_content_1="A test tube holder is used to hold test tubes. It is \nused for holding a test tube in place when the tube is \nhot or should not be touched.";
              mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

              // mat_img_2.destroy(true);
              // mat_name_2.destroy(true);
              // mat_text_2.destroy(true);
              
          break;

          case 6:
            mat_img_1.destroy(true);
            mat_name_1.destroy(true);
            mat_text_1.destroy(true);
            mat_img_1 = this.game.add.sprite(370,550,'burner');
            mat_img_1.scale.setTo(.7,.7);
              mat_img_1.anchor.setTo(.5,.5);
            mat_name_1=this.game.add.text(600,270,"Burner:",headfontStyle);
            mat_content_1="A Bunsen burner, named after Robert Bunsen, is a \nkind of gas burner used as laboratory equipment; \nit produces a single open gas flame and is used for \nheating, sterilization, and combustion.";
            mat_text_1=this.game.add.text(600,450,mat_content_1,fontStyle);

            // mat_img_2.destroy(true);
            // mat_name_2.destroy(true);
            // mat_text_2.destroy(true);
            
        break;


        }
      },
      show_doubledisplacement_materials:function(){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);

                
              }
              mat_img_1 = this.game.add.sprite(230,430,'testube_stand');
              mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(720,330,"Test tube rack:",headfontStyle);
              mat_content_1="Test tube racks are laboratory equipment used to \nhold upright multiple test tubes at the same time. \nThey are most commonly used when various different \nsolutions are needed to work with simultaneously, \nfor safety reasons, for safe storage of test tubes, \nand to ease the transport of multiple tubes.";
              mat_text_1=this.game.add.text(720,430,mat_content_1,fontStyle);

              
          break;
          case 2:

              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);

              mat_img_1 = this.game.add.sprite(250,360,'measuring_cylinder');
              mat_img_1.angle=-15;
              mat_img_1.scale.setTo(.9,.9);
              mat_name_1=this.game.add.text(550,330,"Measuring cylinder:",headfontStyle);
              mat_content_1="A graduated cylinder, also known as a measuring cylinder or \nmixing cylinder is a common piece of laboratory equipment \nused to measure the volume of a liquid. It has a narrow \ncylindrical shape. Each marked line on the graduated cylinder \nrepresents the amount of liquid that has been measured.";
              mat_text_1=this.game.add.text(550,430,mat_content_1,fontStyle);

              

          break;
         case 3:

              mat_img_1.destroy(true);
              mat_name_1.destroy(true);
              mat_text_1.destroy(true);
              if(previousScene==4){
                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);
              }

              mat_img_1 = this.game.add.sprite(330,350,'test_tube');
              mat_img_1.scale.setTo(.9,1);
              mat_name_1=this.game.add.text(550,370,"Test tube:",headfontStyle);
              mat_content_1="A test tube, also known as a culture tube or sample tube, \nis a common piece of laboratory glassware consisting of \na finger-like length of glass or clear plastic tubing, open \nat the top and closed at the bottom. Test tubes are \nusually placed in special-purpose racks.";
              mat_text_1=this.game.add.text(550,470,mat_content_1,fontStyle);

          break; 
          case 4:
              
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
            if(previousScene==5){
                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);
              }

              mat_img_1 = this.game.add.sprite(230,250,'Conical_Flask');
              mat_img_1.scale.setTo(.2,.2);
              mat_name_1=this.game.add.text(520,240,"Conical flask:",headfontStyle);
              mat_content_1="A Conical Flask is a cone-shaped flask with a flat round bottom \nand a cylindrical neck. It was first invented by a German \nChemist named Emil Erlenmeyer in the year 1860. \nThe name of the flask hence came out to be Erlenmeyer's Flask.";
              mat_text_1=this.game.add.text(520,340,mat_content_1,fontStyle);
              
              mat_img_2 = this.game.add.sprite(320,680,'glass_rod');
              mat_img_2.angle=-15;
              mat_name_2=this.game.add.text(450,620,"Glass rods:",headfontStyle);
              mat_content_2="Glass rods are sometimes used as stirrers in laboratory environments. \nGlass rods are commonly manufactured in diameters of 3 mm, \n6 mm, and 10 mm. Glass is used for these rods because \nit does not react with most laboratory chemicals.";
              mat_text_2=this.game.add.text(450,720,mat_content_2,fontStyle);

              
          break;
          case 5:
              
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);

                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);

              mat_img_1 = this.game.add.sprite(270,250,'barium_chloride');
              mat_img_1.scale.setTo(.4,.3);
              mat_name_1=this.game.add.text(550,240,"Barium chloride:",headfontStyle);
              mat_content_1="Barium chloride is an inorganic compound with the \nformula BaCl2. It is one of the most common \nwater-soluble salts of barium.";
              mat_text_1=this.game.add.text(550,340,mat_content_1,fontStyle);
              
              mat_img_2 = this.game.add.sprite(390,660,'sodium-sulfat');
              mat_img_2.scale.setTo(.6,.6);
              mat_name_2=this.game.add.text(620,580,"Sodium sulphate:",headfontStyle);
              mat_content_2="Sodium sulfate (also known as sodium sulphate or \nsulfate of soda) is the inorganic compound with the \nformula Na2SO4 as well as several related hydrates. \nAll forms are white solids that are highly soluble \nin water.";
              mat_text_2=this.game.add.text(620,680,mat_content_2,fontStyle);

              
          break;
        }
      },
      show_combination_materials:function(){
        switch(currentScene){
          case 1:
              if(previousScene==2){
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
              }
              mat_img_1 = this.game.add.sprite(270,400,'lime_quicklime');
              mat_img_1.scale.setTo(.6,.6);
              mat_name_1=this.game.add.text(600,400,"Quick lime (Calcium oxide):",headfontStyle);
              mat_content_1="Calcium oxide (CaO), commonly known as quicklime \nor burnt lime, is a widely used chemical compound. \nIt is a white, caustic, alkaline, crystalline solid at \nroom temperature.";
              mat_text_1=this.game.add.text(600,500,mat_content_1,fontStyle);

              
          break;
          case 2:
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
              if(previousScene==3){
                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);
              }
              mat_img_1 = this.game.add.sprite(270,370,'borosil_beaker');
              mat_img_1.scale.setTo(.9,.9);
              mat_name_1=this.game.add.text(630,350,"Borosil beaker:",headfontStyle);
              mat_content_1="Beakers are useful as a reaction container or to hold \nliquid or solid samples. They are also used to catch \nliquids from titrations and filtrates from filtering \noperations. Laboratory Burners are sources of heat. \nBurets are for addition of a precise volume of liquid.";
              mat_text_1=this.game.add.text(630,450,mat_content_1,fontStyle);

              
          break;
          case 3:
              
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
            if(previousScene==4){
                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);
              }

              mat_img_1 = this.game.add.sprite(250,280,'glass_rod');
              mat_img_1.angle=-15;
              mat_name_1=this.game.add.text(420,240,"Glass rods:",headfontStyle);
              mat_content_1="Glass rods are sometimes used as stirrers in laboratory environments. \nGlass rods are commonly manufactured in diameters of 3 mm, \n6 mm, and 10 mm. Glass is used for these rods because it does not \nreact with most laboratory chemicals.";
              mat_text_1=this.game.add.text(420,340,mat_content_1,fontStyle);
              
              mat_img_2 = this.game.add.sprite(350,630,'distilled_water');
              mat_img_2.scale.setTo(.4,.4);
              mat_name_2=this.game.add.text(600,620,"Distilled water:",headfontStyle);
              mat_content_2="Distilled water is a type of purified water that has \nhad both contaminants and minerals removed. \nPurified water has had chemicals and contaminants \nremoved";
              mat_text_2=this.game.add.text(600,720,mat_content_2,fontStyle);

              
          break;
            case 4:
              
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
            if(previousScene==3){
                mat_img_2.destroy(true);
                mat_name_2.destroy(true);
                mat_text_2.destroy(true);
              }

              mat_img_1 = this.game.add.sprite(250,280,'droper');
              mat_img_1.scale.setTo(1.3,1.3);
              //mat_img_1.angle=-15;
              mat_name_1=this.game.add.text(420,240,"Dropper:",headfontStyle);
              mat_content_1="An eye dropper, also known as a Pasteur pipette, or dropper, is a \ndevice used to transfer small quantities of liquids. \nThey are used in the laboratory.";
              mat_text_1=this.game.add.text(420,340,mat_content_1,fontStyle);
              
              mat_img_2 = this.game.add.sprite(330,600,'test_tube');
              mat_img_2.scale.setTo(.9,.9);
              mat_img_2.angle=-15;
              mat_name_2=this.game.add.text(550,570,"Test tube:",headfontStyle);
              mat_content_2="A test tube, also known as a culture tube or sample tube, \nis a common piece of laboratory glassware consisting of \na finger-like length of glass or clear plastic tubing, open \nat the top and closed at the bottom. Test tubes are \nusually placed in special-purpose racks.";
              mat_text_2=this.game.add.text(550,670,mat_content_2,fontStyle);

              
          break;
          case 5:
                mat_img_1.destroy(true);
                mat_name_1.destroy(true);
                mat_text_1.destroy(true);
               mat_img_2.destroy(true);
               mat_name_2.destroy(true);
               mat_text_2.destroy(true); 
              mat_img_1 = this.game.add.sprite(270,370,'ph_paper');
              mat_img_1.scale.setTo(.7,.7);
              mat_name_1=this.game.add.text(700,350,"Litmus paper strips:",headfontStyle);
              mat_content_1="To measure the acidity or alkalinity of a liquid \npeople use Litmus paper or pH strips. \nThe pH strips determine the pH value \nwhereas Litmus paper only indicates if the \nsubstance is acidic or basic (alkaline).";
              mat_text_1=this.game.add.text(700,450,mat_content_1,fontStyle);

              
          break;
        }
      },
      toNext:function(){
        previousScene=currentScene;
        currentScene++;
        prev_btn.visible=true;
        if(currentScene==total_scene){
          next_btn.visible=false;
          play.visible=true;
        }
        this.showMaterials();
      },
      toPrevious:function(){
        previousScene=currentScene;
        currentScene--;
        next_btn.visible=true;
        if(currentScene==1){
          prev_btn.visible=false;
        }
        this.showMaterials();
      },
    //For to next scene   
   
      toTheory:function()
        {
        voice.destroy();
        this.state.start("Theory", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  //window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  