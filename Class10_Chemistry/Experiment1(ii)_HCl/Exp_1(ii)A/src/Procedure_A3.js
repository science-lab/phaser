var procedure_a3 = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  }
  
  procedure_a3.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("A_procedure_voice1",1);
 
 
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_text=this.game.add.text(250,230,"Procedure:",headfontStyle);

  pageCount=3;

  
  //console.log(exp_Name);
  //switch(exp_Name){
   // case "Iron with copper sulphate":
       
      procedure_step_text_h1=this.game.add.text(300,330,"",headfontStyle);
      procedure_step_text_1=this.game.add.text(300,430,"",fontStyle);
      procedure_step_text_2=this.game.add.text(300,565,"",fontStyle);
      procedure_step_text_3=this.game.add.text(300,620,"",fontStyle);
      procedure_step_text_4=this.game.add.text(300,840,"",fontStyle);
      
   // break;
    
  //}
this.addProcedure();
/*next_btn = this.game.add.sprite(1610,870,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,870,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);
 prev_btn = this.game.add.sprite(310,870,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,870,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 //prev_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrev, this);
 prev_btn.visible=false;*/
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 play = this.game.add.sprite(1800,800,'components','play_disabled.png');
 play.scale.setTo(.7,.7);
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment, this);
 //play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
    
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        addProcedure:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy();
              voice=this.game.add.audio("A_procedure_voice1",1);
              voice.play();
              procedure_step_text_h1.text="The reaction of Hydrochloric acid with blue and red litmus solution.";
              procedure_step_text_1.text="1. Take two test tubes labeled as 'A' and 'B'. Add about 2 ml of blue litmus \n    solution to test tube A and 2 ml of red litmus solution to test tube B.";
              procedure_step_text_2.text="2. Add 4 to 5 drops of diluted hydrochloric acid solution to each of the \n    test tubes A and B then observe the change in color.";
              procedure_step_text_3.text="";
              procedure_step_text_4.text="";
            break;
            case 2:
              voice.destroy();
              voice=this.game.add.audio("A_procedure_voice2",1);
              voice.play();
              procedure_step_text_h1.text="The reaction of Hydrochloric acid with zinc metal.";
              procedure_step_text_1.text="1. Take a few granules of zinc metal in a test tube. Add dilute HCl to the \n    test tube. Now take a cork and put a glass tube with a fine nozzle in it \n    then place it on the mouth of the test tube. ";
              procedure_step_text_3.text="2. Bring a light matchstick near the glass tube nozzle and observe.";
              procedure_step_text_2.text="";
              procedure_step_text_4.text="";
            break;
            case 3:
              voice.destroy();
              voice=this.game.add.audio("A_procedure_voice3",1);
              voice.play();
              procedure_step_text_h1.text="The reaction of HCl with sodium carbonate.";
              procedure_step_text_1.text="1. Take about 1 g of sodium carbonate in a test tube. Add about 2 ml of \n    dilute HCl to it. Pass the gas evolved through lime water with the help \n    of a delivery tube.";
              procedure_step_text_2.text="";
              procedure_step_text_3.text="";
              procedure_step_text_4.text="";
            break;
          }
        },
        toNext:function(){
            if(pageCount<3){
              prev_btn.visible=true;
              pageCount++;
                this.addProcedure();
                if(pageCount>=3){
                  play.visible=true;
                  next_btn.visible=false;  
                }
              }
        },
        toPrev:function(){
            if(pageCount>1){
              next_btn.visible=true;
              pageCount--;
                this.addProcedure();
                if(pageCount<=1){
                  prev_btn.visible=false;  
                }
              }
        },
    //For to next scene   
   
        toExperiment:function()
        {
        voice.destroy();
        this.state.start("Experiment_A3", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  //window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  