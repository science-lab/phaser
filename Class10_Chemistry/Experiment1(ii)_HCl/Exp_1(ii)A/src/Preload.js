var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
        this.game.load.atlasJSONHash('Dropper_Blue_OUT', 'assets/Dropper_Blue_OUT.png', 'assets/Dropper_Blue_OUT.json');
        this.game.load.atlasJSONHash('Dropper_HCl_OUT', 'assets/Dropper_HCl_OUT.png', 'assets/Dropper_HCl_OUT.json');
        this.game.load.atlasJSONHash('Dropper_Red_OUT', 'assets/Dropper_Red_OUT.png', 'assets/Dropper_Red_OUT.json');
        this.game.load.atlasJSONHash('TestTube_Blue_in', 'assets/TestTube_Blue_in.png', 'assets/TestTube_Blue_in.json');
        this.game.load.atlasJSONHash('TestTube_HCl_in', 'assets/TestTube_HCl_in.png', 'assets/TestTube_HCl_in.json');
        this.game.load.atlasJSONHash('TestTube_Red_in', 'assets/TestTube_Red_in.png', 'assets/TestTube_Red_in.json');
        //this.game.load.atlasJSONHash('Blue_to_Red', 'assets/Blue_to_Red.png', 'assets/Blue_to_Red.json');
        
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
        this.game.load.image('burner','assets/burner.png');
        //this.game.load.image('ferrous_sulphate1','assets/ferrous_sulphate_1.png');
        this.game.load.image('spoon','assets/spoon.png');
        this.game.load.image('stand','assets/stand.png');
        this.game.load.image('test_tube','assets/test_tube.png');
        this.game.load.image('testube_stand','assets/testube_stand.png');
        this.game.load.image('observation_table','assets/observation_table_decomposition.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        //////////////////titles////////////////////////////////////////
        this.game.load.image('exp1_title','assets/titles/exp1_title.png');
        this.game.load.image('exp2_title','assets/titles/exp2_title.png');
        this.game.load.image('exp3_title','assets/titles/exp3_title.png');
        this.game.load.image('exp4_title','assets/titles/exp4_title.png');
        this.game.load.image('exp5_title','assets/titles/exp5_title.png');
        //////////////////materials////////////////////////////////////////
        //this.game.load.image('iron_nails','assets/materials/iron_nails.jpeg');
        //this.game.load.image('copper_suplphate_solution','assets/materials/copper_suplphate_solution.jpg');
        this.game.load.image('M_Testube','assets/materials/M_Testube.png');
        this.game.load.image('M_Testtube_Stand','assets/materials/M_Testtube_Stand.png');
        this.game.load.image('M_Dropper','assets/materials/M_Dropper.png');
        this.game.load.image('M_Cork','assets/materials/M_Cork.png');
        this.game.load.image('M_Delivery_tube','assets/materials/M_Delivery_tube.png');
        this.game.load.image('M_Burner','assets/materials/M_Burner.png');
        this.game.load.image('M_HCL','assets/materials/M_HCL.png');
        this.game.load.image('M_Litmus_Solution_Red','assets/materials/M_Litmus_Solution_Red.png');
        this.game.load.image('M_Zink_Metal','assets/materials/M_Zink_Metal.png');
        this.game.load.image('M_Sodium_Carbonate','assets/materials/M_Sodium_Carbonate.png');
        this.game.load.image('M_Lime_Water','assets/materials/M_Lime_Water.png');

        
///////////////////////////////Experiment_A1///////////////////////////////////////////////////////////////////////
        this.game.load.image('TestTubeStand_Emty','assets/TestTubeStand_Emty.png');
        this.game.load.image('TestTubeStand_Hanger_A','assets/TestTubeStand_Hanger_A.png');
        this.game.load.image('TestTubeStand_Hanger_B','assets/TestTubeStand_Hanger_B.png');
        this.game.load.image('TestTube_Nill','assets/TestTube_Nill.png');
        this.game.load.image('TestTube_Nill_Back','assets/TestTube_Nill_Back.png');
        this.game.load.image('holder1','assets/holder1.png');
        this.game.load.image('holder2','assets/holder2.png');
        this.game.load.image('Red_Litmus','assets/Jar_Red.png');
        this.game.load.image('Red_Litmus_front','assets/Jar_Red_front.png');
        this.game.load.image('Blue_Litmus','assets/Jar_Blue.png');
        this.game.load.image('Blue_Litmus_front','assets/Jar_Blue_front.png');
        this.game.load.image('HCl','assets/Jar_HCl.png');
        this.game.load.image('HCl_front','assets/Jar_HCl_front.png');
        this.game.load.image('Jar_Back','assets/Jar_Back.png');
        this.game.load.image('cork','assets/cap.png');
///////////////////////////////Experiment_A2///////////////////////////////////////////////////////////////////////
        this.game.load.atlasJSONHash('Black_smoke', 'assets/HCL_zinc/Black_smoke.png', 'assets/HCL_zinc/Black_smoke.json');
        this.game.load.atlasJSONHash('Flame', 'assets/HCL_zinc/Flame.png', 'assets/HCL_zinc/Flame.json'); 
        this.game.load.atlasJSONHash('Blue_flame', 'assets/HCL_zinc/Blue_flame.png', 'assets/HCL_zinc/Blue_flame.json'); 
        this.game.load.atlasJSONHash('Cork_tube_testtube', 'assets/HCL_zinc/Cork_tube_testtube.png', 'assets/HCL_zinc/Cork_tube_testtube.json'); 
        this.game.load.atlasJSONHash('White_smoke', 'assets/HCL_zinc/White_smoke.png', 'assets/HCL_zinc/White_smoke.json'); 
        this.game.load.atlasJSONHash('Zn_testtube', 'assets/HCL_zinc/Zn_testtube.png', 'assets/HCL_zinc/Zn_testtube.json'); 
        this.game.load.atlasJSONHash('HCl_testtube', 'assets/HCL_zinc/HCl_testtube.png', 'assets/HCL_zinc/HCl_testtube.json');
        this.game.load.image('Testtube_a2','assets/HCL_zinc/Testtube.png');
        this.game.load.image('Spoon','assets/HCL_zinc/Spoon.png');
        this.game.load.image('Spoon_with_Zn','assets/HCL_zinc/Spoon_with_Zn.png');
        this.game.load.image('Zn_plate','assets/HCL_zinc/Zn_plate.png');
        this.game.load.image('Testtube_Cork_rod','assets/HCL_zinc/Testtube_Cork_rod.png');
        this.game.load.image('Match_Box','assets/HCL_zinc/Match_Box.png');
        this.game.load.image('Match_Stick','assets/HCL_zinc/Match_Stick.png');
        this.game.load.image('Burned_Match_Stick','assets/HCL_zinc/Burned_Match_Stick.png');
///////////////////////////////////////////////HCl_NaCO3//////////////////////////////////////////////////////
        this.game.load.image('A3_testtube1','assets/HCl_NaCO3/Testtube_full.png');
        this.game.load.image('A3_testtube_front','assets/HCl_NaCO3/Testtube_front.png');
        this.game.load.image('A3_testtube_back','assets/HCl_NaCO3/Testtube_back.png');
        this.game.load.image('A3_Spoon','assets/HCl_NaCO3/Spoon.png');
        this.game.load.image('A3_Spoon_NaCO3','assets/HCl_NaCO3/Spoon_NaCO3.png');
        this.game.load.image('A3_Cork_tube','assets/HCl_NaCO3/Cork_tube.png');
        this.game.load.image('A3_tube','assets/HCl_NaCO3/Delivery_tube.png');
        this.game.load.image('A3_Lime_testtube_full','assets/HCl_NaCO3/Lime_water_Testube_top.png');
        this.game.load.image('A3_Lime_testtube','assets/HCl_NaCO3/Lime_water_Testube.png');
        this.game.load.image('A3_NaCO3','assets/HCl_NaCO3/NaCO3_plate.png');

        this.game.load.atlasJSONHash('Spoon_sprite','assets/HCl_NaCO3/Spoon_sprite/Spoon.png','assets/HCl_NaCO3/Spoon_sprite/Spoon.json');
        this.game.load.atlasJSONHash('NaCO3_testtube_sprite','assets/HCl_NaCO3/NaCO3_testtube_sprite/NaCO3_testtube.png','assets/HCl_NaCO3/NaCO3_testtube_sprite/NaCO3_testtube.json');
        this.game.load.atlasJSONHash('Lime_sprite','assets/HCl_NaCO3/Lime_sprite/Lime.png','assets/HCl_NaCO3/Lime_sprite/Lime.json');
        this.game.load.atlasJSONHash('HCl_testtube_sprite','assets/HCl_NaCO3/HCl_testtube_sprite/HCl_testtube.png','assets/HCl_NaCO3/HCl_testtube_sprite/HCl_testtube.json');
        this.game.load.atlasJSONHash('Glass_tube_sprite','assets/HCl_NaCO3/Glass_tube_sprite/Glass_tube.png','assets/HCl_NaCO3/Glass_tube_sprite/Glass_tube.json');
        this.game.load.atlasJSONHash('A3_Dropper_HCl_OUT', 'assets/HCl_NaCO3/Dropper_dorp_sprite/Dropper_drop.png', 'assets/HCl_NaCO3/Dropper_dorp_sprite/Dropper_drop.json');
        this.game.load.atlasJSONHash('A3_white_smoke', 'assets/HCl_NaCO3/White_smoke_sprite/White_smoke.png', 'assets/HCl_NaCO3/White_smoke_sprite/White_smoke.json');
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




  /////////////////////Experiment_A1_sounds////////////////////////////////////
        this.game.load.audio('A_aim','assets/audio/Experiment_A1/Aim.mp3');
        this.game.load.audio('A_precaution','assets/audio/Experiment_A1/A_precaution.mp3');
        this.game.load.audio('A_precaution2','assets/audio/Experiment_A1/A_precaution2.mp3');
        this.game.load.audio('A_procedure_voice1','assets/audio/Experiment_A1/A_procedure_voice1.mp3');
        this.game.load.audio('A_procedure_voice2','assets/audio/Experiment_A1/A_procedure_voice2.mp3');
        this.game.load.audio('A_procedure_voice3','assets/audio/Experiment_A1/A_procedure_voice3.mp3');
        
        this.game.load.audio('A_procedure1','assets/audio/Experiment_A1/A_procedure1.mp3');
        this.game.load.audio('A_procedure2','assets/audio/Experiment_A1/A_procedure2.mp3');
        this.game.load.audio('A_procedure3','assets/audio/Experiment_A1/A_procedure3.mp3');
        this.game.load.audio('A_procedure4','assets/audio/Experiment_A1/A_procedure4.mp3');
        this.game.load.audio('A_procedure5','assets/audio/Experiment_A1/A_procedure5.mp3');
        this.game.load.audio('A_procedure6','assets/audio/Experiment_A1/A_procedure6.mp3');
        this.game.load.audio('A_procedure7','assets/audio/Experiment_A1/A_procedure7.mp3');
        this.game.load.audio('A_procedure8','assets/audio/Experiment_A1/A_procedure8.mp3');
        this.game.load.audio('A_procedure9','assets/audio/Experiment_A1/A_procedure9.mp3');
        this.game.load.audio('A_procedure10','assets/audio/Experiment_A1/A_procedure10.mp3');
        this.game.load.audio('A_procedure11','assets/audio/Experiment_A1/A_procedure11.mp3');

        this.game.load.audio('A_observation1','assets/audio/Experiment_A1/A_observation1.mp3');
        this.game.load.audio('A_observation2','assets/audio/Experiment_A1/A_observation2.mp3');
        this.game.load.audio('A_observation3','assets/audio/Experiment_A1/A_observation3.mp3');
        this.game.load.audio('A_result','assets/audio/Experiment_A1/A_result.mp3');
  /////////////////////Experiment_A2_sounds////////////////////////////////////      
        this.game.load.audio('step_1','assets/audio/Experiment_A2/A2_step_1.mp3');
        this.game.load.audio('step_2','assets/audio/Experiment_A2/A2_step_2.mp3');
        this.game.load.audio('step_3','assets/audio/Experiment_A2/A2_step_3.mp3');
        this.game.load.audio('step_4','assets/audio/Experiment_A2/A2_step_4.mp3');
        this.game.load.audio('step_5','assets/audio/Experiment_A2/A2_step_5.mp3');
        this.game.load.audio('step_6','assets/audio/Experiment_A2/A2_step_6.mp3');
        this.game.load.audio('step_7','assets/audio/Experiment_A2/A2_step_7.mp3');
        this.game.load.audio('step_8','assets/audio/Experiment_A2/A2_step_8.mp3');
        this.game.load.audio('step_9','assets/audio/Experiment_A2/A2_step_9.mp3');
/////////////////////Experiment_A3_sounds////////////////////////////////////           
        this.game.load.audio('A3_procedure1','assets/audio/Experiment_A3/A_procedure1.mp3');
        this.game.load.audio('A3_procedure2','assets/audio/Experiment_A3/A_procedure2.mp3');
        this.game.load.audio('A3_procedure3','assets/audio/Experiment_A3/A_procedure3.mp3');
        this.game.load.audio('A3_procedure4','assets/audio/Experiment_A3/A_procedure4.mp3');
        this.game.load.audio('A3_procedure5','assets/audio/Experiment_A3/A_procedure5.mp3');
        this.game.load.audio('A3_procedure6','assets/audio/Experiment_A3/A_procedure6.mp3');
        this.game.load.audio('A3_procedure7','assets/audio/Experiment_A3/A_procedure7.mp3');
        this.game.load.audio('A3_procedure8','assets/audio/Experiment_A3/A_procedure8.mp3');
        this.game.load.audio('A3_procedure9','assets/audio/Experiment_A3/A_procedure9.mp3');
        this.game.load.audio('popsound','assets/audio/Experiment_A2/pop_sound.mp3');
        
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Exp_Selection");//Starting the gametitle state

      //this.game.state.start("Experiment_A2");//Starting the gametitle state
     // this.game.state.start("Aim");//Simulation_hot1

      //this.game.state.start("Experiment_A3");//Starting the gametitle state
      this.game.state.start("Aim");//Simulation_hot1

      //this.game.state.start("Theory");//Simulation_hot1
      //this.game.state.start("Lab_Precautions");
      //this.game.state.start("Procedure");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Materials");//Starting the gametitle state
      //this.game.state.start("Viva");//Starting the gametitle state
	}
}

