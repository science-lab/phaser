var preload = function(game){

  var ip; 
  var loc;
  var hotflag;
  var level;
  var exp_Name;
}

preload.prototype = {
	preload: function(){ 

              
        this.game.load.atlasJSONHash('components', 'assets/base_components.png', 'assets/base_components.json'); //Standardization Buttons
        
       
        this.game.load.json('questions','data/questions1.json');
    
        //this.game.load.atlasJSONHash('brick1', 'assets/brick1.png', 'assets/brick1.json'); 
        this.game.load.image('fullScreen1','assets/fullscreen.png');
        this.game.load.image('normalScreen','assets/normalScreen.png');
         this.game.load.image("bg","assets/bg.jpg");
        
        
   this.game.load.image('transBackground','assets/transBackground.png');
   this.game.load.image("arrow","assets/arrow.png");
         this.game.load.image("collider","assets/col.png");
         this.game.load.image("Button_Bg","assets/Black_panel.png");
           
              this.game.load.image('bullet','assets/bullet.png');
        this.game.load.image('bullet_b','assets/bullet_b.png');
        


        this.game.load.image('dialogue_box','assets/dialogue_box.png');
        this.game.load.image('dialogue_box1','assets/dialogue_box1.png');
        
         this.game.load.image('observation_table','assets/observation_table_decomposition.png');
        //this.game.load.image('observation_img_1','assets/observation_img_1.png');
        //this.game.load.image('observation_img_2','assets/observation_img_2.png');
        this.game.load.image('observation_box','assets/observation_box.png');
        this.game.load.image('rightAns_img','assets/rightAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');
        this.game.load.image('wrongAns_img','assets/wrongAns_img.png');


        

  /////////////////////Experiment_A1_sounds////////////////////////////////////
        this.game.load.audio('A_aim','assets/audio/Experiment_A1/Aim.mp3');
        this.game.load.audio('precaution_1','assets/audio/Experiment_A1/precaution_1.mp3');
        this.game.load.audio('precaution_2','assets/audio/Experiment_A1/precaution_2.mp3');
        this.game.load.audio('precaution_3','assets/audio/Experiment_A1/precaution_3.mp3');
        this.game.load.audio('precaution_4','assets/audio/Experiment_A1/precaution_4.mp3');
        this.game.load.audio('procedure_1','assets/audio/Experiment_A1/procedure_1.mp3');
        this.game.load.audio('procedure_2','assets/audio/Experiment_A1/procedure_2.mp3');
        this.game.load.audio('procedure_3','assets/audio/Experiment_A1/procedure_3.mp3');
        this.game.load.audio('procedure_4','assets/audio/Experiment_A1/procedure_4.mp3');
        this.game.load.audio('procedure_5','assets/audio/Experiment_A1/procedure_5.mp3');
        this.game.load.audio('procedure_6','assets/audio/Experiment_A1/procedure_6.mp3');
	},
  	create: function(){
      level=1;
  	   hotflag=1;
      ip = location.host; 
      loc=localStorage.getItem("exitlink");//"https://scienceapp.in/swadhyaya/theme/essential/layout/creatnlrn/third_phy_10.php?id=25";
      //this.game.state.start("Exp_Selection");//Starting the gametitle state
      //this.game.state.start("Experiment_A3");//Starting the gametitle state
      //this.game.state.start("Aim");//Simulation_hot1
      //this.game.state.start("Theory");//Simulation_hot1
      //this.game.state.start("Lab_Precautions");
      this.game.state.start("Procedure");
      //this.game.state.start("Observations");//hot
      //this.game.state.start("Result");//Starting the gametitle state
      //this.game.state.start("Materials");//Starting the gametitle state
      //this.game.state.start("Viva");//Starting the gametitle state
	}
}

