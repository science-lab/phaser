var procedure = function(game){

  ///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
  var background;
  var popup;
  var play;
  var rightbutton_bg;
  var leftbottom_bg;
  var bullet;
  var bullet2;
  var bullet3;
  
  ////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////
  
  var volumeButton;
  var muteButton;
  var homeButton;
  var closeButton;
  
  ///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////
  
  var contentstyle;
  
  //////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////
  var pageCount;
  var nextSoundDelay;
  //audio
  var voice;
  
  //bools
  var muted;
  
  //ip address
  var ip;
  
  }
  
  procedure.prototype ={
  
  init: function( ipadrs) 
  {
    ip = ipadrs;
  },
  
  ////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////
  
  create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("procedure_1",1);
 nextSoundDelay=0;
 
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Aim /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  procedure_text=this.game.add.text(250,230,"Procedure:",headfontStyle);

  pageCount=1;

  
  //console.log(exp_Name);
  //switch(exp_Name){
   // case "Iron with copper sulphate":
       
      //procedure_step_text_h1=this.game.add.text(300,330,"",headfontStyle);
      procedure_step_text_1=this.game.add.text(300,330,"",fontStyle);
      procedure_step_text_2=this.game.add.text(300,455,"",fontStyle);
      procedure_step_text_3=this.game.add.text(300,530,"",fontStyle);
      procedure_step_text_4=this.game.add.text(300,755,"",fontStyle);

       procedure_step_text_5=this.game.add.text(300,330,"",fontStyle);
       procedure_step_text_6=this.game.add.text(300,505,"",fontStyle);
       procedure_step_text_7=this.game.add.text(300,680,"",fontStyle);
       
       procedure_step_text_8=this.game.add.text(300,330,"",fontStyle);
       procedure_step_text_9=this.game.add.text(300,450,"",fontStyle);
       procedure_step_text_10=this.game.add.text(300,570,"",fontStyle);
       procedure_step_text_11=this.game.add.text(300,690,"",fontStyle);

   // break;
    
  //}
this.addProcedure();
next_btn = this.game.add.sprite(1610,870,'components','next_disabled.png');
 next_btn.scale.setTo(.7,.7);
 next_btn = this.game.add.sprite(1610,870,'components','next_pressed.png');
 next_btn.scale.setTo(.7,.7);
 next_btn.inputEnabled = true;
 //next_btn.input.priorityID = 3;
 next_btn.input.useHandCursor = true;
 next_btn.events.onInputDown.add(this.toNext, this);
 prev_btn = this.game.add.sprite(310,870,'components','next_disabled.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn = this.game.add.sprite(310,870,'components','next_pressed.png');
 prev_btn.scale.setTo(-.7,.7);
 prev_btn.inputEnabled = true;
 //prev_btn.input.priorityID = 3;
 prev_btn.input.useHandCursor = true;
 prev_btn.events.onInputDown.add(this.toPrev, this);
 prev_btn.visible=false;
  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 play = this.game.add.sprite(1800,800,'components','play_disabled.png');
 play.scale.setTo(.7,.7);
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toExperiment, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },
  
    ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////
    
    // For Full screen checking.
    
        gofull: function()
        {
          if (this.game.scale.isFullScreen)
              {
              this.game.scale.stopFullScreen();
              }
          else
              {
               this.game.scale.startFullScreen(false);
              }  
        },
        addProcedure:function(){
          console.log(pageCount+"//pageCount");
          switch(pageCount){
            case 1:
              voice.destroy();
              voice=this.game.add.audio("procedure_1",1);
              voice.play();
              nextSoundDelay=0;
              //procedure_step_text_h1.text="1. Clean the ends of the connecting wires with the help of sandpaper in order to \nremove the insulating coating on them.";
              procedure_step_text_1.text="1. Clean the ends of the connecting wires with the help of sandpaper in order \n    to remove the insulating coating on them.";
              procedure_step_text_2.text="2. Note the range and least count of the voltmeter and ammeter.";
              procedure_step_text_3.text="3. Note whether the pointers in the ammeter and voltmeter coincide with zero \n    marks on the measuring scale. If it is not so, adjust the pointer to coincide \n    with the zero marks by adjusting the screw, provided near the base of \n    the pointer, using a screwdriver.";
              procedure_step_text_4.text="4. Connect one of the two resistors in a circuit as per the circuit diagram (A).";
              procedure_step_text_5.text="";
              procedure_step_text_6.text="";
              procedure_step_text_7.text="";
              procedure_step_text_8.text="";
              procedure_step_text_9.text="";
              procedure_step_text_10.text="";
              procedure_step_text_11.text="";
              break;
            case 2:
              voice.destroy();
              voice=this.game.add.audio("procedure_3",1);
              voice.play();
              nextSoundDelay=0;
              //procedure_step_text_h1.text="The reaction of Hydrochloric acid with zinc metal.";
              procedure_step_text_1.text="";
              procedure_step_text_2.text="";
              procedure_step_text_3.text="";
              procedure_step_text_4.text="";
              procedure_step_text_5.text="5. Put the plugin the key and record the values of current and potential \n    difference from the ammeter and voltmeter. Take out the plug from \n    the key. ";
              procedure_step_text_6.text="6. Change the position of contact on the rheostat and repeat step 5 to get \n    another set of reading of current and potential difference. In this way record \n    at least three sets of readings.";
              procedure_step_text_7.text="7. Then connect the second resistor alone and take readings of current I and \n    potential difference V for it.";
              procedure_step_text_8.text="";
              procedure_step_text_9.text="";
              procedure_step_text_10.text="";
              procedure_step_text_11.text="";
              
            break;
            case 3:
              voice.destroy();
              voice=this.game.add.audio("procedure_5",1);
              voice.play();
              nextSoundDelay=0;
              procedure_step_text_1.text="";
              procedure_step_text_2.text="";
              procedure_step_text_3.text="";
              procedure_step_text_4.text="";
              procedure_step_text_5.text="";
              procedure_step_text_6.text="";
              procedure_step_text_7.text="";
              procedure_step_text_8.text="8. Find the resistances by applying Ohm’s law (R = V/R). Let these values be \n    R1 and R2.";
              procedure_step_text_9.text="9. Connect the two resistors in series between the two terminals of the \n    voltmeter as shown in the circuit diagram (B).";
              procedure_step_text_10.text="10. Note the readings of current I and potential difference V by repeating \n      steps 5 and 6.";
              procedure_step_text_11.text="11. Tabulate the observations and calculate the resistance of the combination.";
            break;
          }
        },
        toNext:function(){
            if(pageCount<3){
              prev_btn.visible=true;
              pageCount++;
                this.addProcedure();
                if(pageCount>=3){
                  play.visible=true;
                  next_btn.visible=false;  
                }
              }
        },
        toPrev:function(){
            if(pageCount>1){
              next_btn.visible=true;
              pageCount--;
                this.addProcedure();
                if(pageCount<=1){
                  prev_btn.visible=false;  
                }
              }
        },

        update: function()
        {
          if(pageCount==1){

            nextSoundDelay++;
            if(nextSoundDelay==950){
              voice.destroy();
              voice=this.game.add.audio("procedure_2",1)
              voice.play();
            }
          }else if(pageCount==2){

            nextSoundDelay++;
            if(nextSoundDelay==1110){
              voice.destroy();
              voice=this.game.add.audio("procedure_4",1)
              voice.play();
            }
          }else if(pageCount==3){

            nextSoundDelay++;
            if(nextSoundDelay==800){
              voice.destroy();
              voice=this.game.add.audio("procedure_6",1)
              voice.play();
            }
          }
        },
    //For to next scene   
   
        toExperiment:function()
        {
        voice.destroy();
        this.state.start("Experiment_A1", true, false, ip);
        },
  
  // For mute the audio
        muteTheGame:function()
        {
        muted = true;
        //   voice.stop();
        this.game.sound.mute = true;
        volumeButton.visible = true;
        muteButton.visible = false;
        },
  
  // For unmute audio
  
  volume:function()
        {
         this.game.sound.mute = false;
         volumeButton.visible = false;
         muteButton.visible = true;
        },
  
  // For Goto title screen
  
  gotoHome:function()
  {
    voice.destroy();
    this.state.start("Aim", true, false, ip);
  },
  
  ////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      
  
    /*postData:function()
    {
     // Sending and receiving data in JSON format using POST method
     //
     console.log('post');
     var xhr = new XMLHttpRequest();
     var url = "https://scienceapp.in/api/public/user-logs";
     xhr.open("POST", url, true);
     xhr.setRequestHeader("Content-Type", "application/json");
     current_timestamp=Math.floor(new Date().getTime()/1000);
     var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
     xhr.send(data);
     xhr.onreadystatechange = function () 
            {
            console.log(xhr.readyState);
            console.log(xhr.status);
            if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
              {
              var json = JSON.parse(xhr.responseText);
              console.log(json);
              window.open(loc,"_self");
              }
            else 
                {
                console.log('fail');
                window.open(loc,"_self");
                }
            };
   },*/
  
  // To quit the experiment
  closeTheGame:function()
  {
  voice.destroy();
  //this.postData();
  //local cloud instance test
  //var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
  //window.open(loc,"_self");                  // local test link
  //    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
   },
  
  
  }
  