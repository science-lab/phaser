var lab_precautions = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;

var pageCount;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;
var nextSoundDelay;

}

lab_precautions.prototype ={

init: function( ipadrs) 
{
  ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("A_precaution1",1);
 //voice1=this.game.add.audio("A_precaution2",1);
 //voice2=this.game.add.audio("A_precaution3",1);
 nextSoundDelay=0;
 
 //voice2=this.game.add.audio("Aim_2",1);
 
 //voice.onStop.add(this.nextSound,this);
 bg= this.game.add.sprite(0, 0,'bg');
 bg.scale.setTo(1,1.3);

  /*var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.beginFill(0xffffff);
  maskBg1.drawRect(0, 0, 1922, 1081);
  maskBg1.alpha=.25;*/


 
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1480, 140,"Button_Bg");
  righttop_bg.scale.setTo(.5,.4);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;



///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  /*normalScreen = this.game.add.sprite(1810,960,'normalScreen');
  normalScreen.scale.setTo(2.5,2.5);  
  normalScreen.inputEnabled = true;
  normalScreen.input.useHandCursor = true;
  normalScreen.events.onInputUp.add(this.gonormal,this);*/
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}
 
  

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  /////////////////////////////////////Precautions /////////////////////////////////////
  base= this.game.add.sprite(160,140,'dialogue_box')
  precaution_text=this.game.add.text(250,230,"Precautions:",headfontStyle);
  pageCount=1;
 /* switch(exp_Name){
    case "Iron with copper sulphate":*/
     // precaution1="1. Thick copper wires should be in the connections and ends must be cleaned\n    properly to allow the current to flow unhindered.";
     // precaution2="2. Connections must be made securely and tightly.";
     // precaution3="3. Plug key has to be inserted for a short time when you are ready to take the\n   readings. The flow of electric current for a long time may heat the wires and\n   the resistor and the change in temperature may change the result.";
     // precaution4="4. While studying the reactions of HCl with zinc, use only a small quantity of \n    HCI because a large quantity of the acid would produce a large amount of \n    hydrogen gas which may cause an explosion when ignited.";
     // precaution5="5. Observe the appearance of milkiness in lime water carefully. If C0\u2082 is passed \n    through lime water in excess, the milkiness disappears";
      //precaution6="6. Wash your hands properly with soap after completing the experiment.";
      //precaution7="The color of the pH paper after adding sample to it, may be shows slightly \ndark or light with color of the pH color chart. So please note exact \npH value instead of range of values.";
      precaution1_text=this.game.add.text(300,330,"",fontStyle);
      precaution2_text=this.game.add.text(300,460,"",fontStyle);
      precaution3_text=this.game.add.text(300,530,"",fontStyle);
      precaution4_text=this.game.add.text(300,700,"",fontStyle);
      precaution5_text=this.game.add.text(300,760,"",fontStyle);
      precaution6_text=this.game.add.text(300,880,"",fontStyle);
      
      this.addPrecautions();
      next_btn = this.game.add.sprite(1610,870,'components','next_disabled.png');
      next_btn.scale.setTo(.7,.7);
      next_btn = this.game.add.sprite(1610,870,'components','next_pressed.png');
      next_btn.scale.setTo(.7,.7);
      next_btn.inputEnabled = true;
      //next_btn.input.priorityID = 3;
      next_btn.input.useHandCursor = true;
      next_btn.events.onInputDown.add(this.toNext, this);
      prev_btn = this.game.add.sprite(310,870,'components','next_disabled.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn = this.game.add.sprite(310,870,'components','next_pressed.png');
      prev_btn.scale.setTo(-.7,.7);
      prev_btn.inputEnabled = true;
      //prev_btn.input.priorityID = 3;
      prev_btn.input.useHandCursor = true;
      prev_btn.events.onInputDown.add(this.toPrev, this);
      prev_btn.visible=false;
      //precaution1_text
      //precaution7_text=this.game.add.text(300,810,precaution7,fontStyle);

      //voice=this.game.add.audio("precautions_1",1);
      //voice.play();
    //break;
  
  //}

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toProcedure, this);
 play.visible=false;


  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);

 },

  ////////////////////////////////////////////// FUNCTION DECLARATIONS ////////////////////////////////////////////////////

  // For Full screen checking.
  
      gofull: function()
      {
        if (this.game.scale.isFullScreen)
            {
            this.game.scale.stopFullScreen();
            }
        else
            {
             this.game.scale.startFullScreen(false);
            }  
      },
      addPrecautions:function()
      {
        console.log(pageCount+"//pageCount");
        switch(pageCount){
          case 1:
              voice.destroy();
              voice=this.game.add.audio('A_precaution1',1);
              voice.play();
              nextSoundDelay=0;
              //setTimeout(this.nextSound,14500);
            //voice.destroy();
          //  voice=this.game.add.audio("A_procedure_voice1",1);
           // voice.play();
            //procedure_step_text_h1.text="1. Clean the ends of the connecting wires with the help of sandpaper in order to \nremove the insulating coating on them.";
            precaution1_text.text="1. Thick copper wires should be in the connections and ends must be cleaned\n    properly to allow the current to flow unhindered.";
            precaution2_text.y=460;
            precaution2_text.text="2. Connections must be made securely and tightly.";
            precaution3_text.text="3. Plug key has to be inserted for a short time when you are ready to take the\n    readings. The flow of electric current for a long time may heat the wires and\n    the resistor and the change in temperature may change the result.";
            precaution4_text.text="4. Ammeter is to be connected in series to the resistor with its positive terminal\n    connected to the side of wire coming from the positive terminal of the dry\n    cell.";
            precaution5_text.text="";
            
            break;
          case 2:
            voice.destroy();
            voice=this.game.add.audio("A_precaution3",1);
            voice.play();
            //procedure_step_text_h1.text="The reaction of Hydrochloric acid with zinc metal.";
            precaution1_text.text="5. Voltmeter is to be connected in parallel to the resistor with its positive\n    terminal connected to the side of wire coming from the positive terminal\n    of the dry cell";
            precaution2_text.y=510;
            precaution2_text.text="6. While taking readings from the ammeter and the voltmeter and the voltmeter,\n    ensure that your eyes are exactly above the meter to avoid error.";
            precaution3_text.text="";
            precaution4_text.text="";
            precaution5_text.text="";
            
            
          break;
          case 3:
            //voice.destroy();
            //voice=this.game.add.audio("A_procedure_voice3",1);
            //voice.play();
            precaution1_text.text="1. Thick copper wires should be in the connections and ends must be cleaned\n    properly to allow the current to flow unhindered.";
            precaution2_text.text="";
            precaution3_text.text="";
            precaution4_text.text="";
            precaution5_text.text="";
          break;
        }
      },

      toNext:function(){
        if(pageCount<3){
          prev_btn.visible=true;
          pageCount++;
            this.addPrecautions();
            if(pageCount>=2){
              play.visible=true;
              next_btn.visible=false;  
            }
          }
    },
    toPrev:function(){
        if(pageCount>1){
          next_btn.visible=true;
          pageCount--;
          this.addPrecautions();
            if(pageCount<=1){
              prev_btn.visible=false;  
            }
          }
    },
    update: function()
  {
    if(pageCount==1){

      nextSoundDelay++;
      
      if(nextSoundDelay==880){
        voice.destroy();
        voice=this.game.add.audio("A_precaution2",1)
        voice.play();
      }
    }
  },
  //For to next scene   
 
      toProcedure:function()
      {
      voice.destroy();
      //console.log("pppppppppp");
      this.state.start("Procedure", true, false, ip);
      //this.state.start("Exp_Selection", true, false);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

gotoHome:function()
{
  voice.destroy();
  this.state.start("Aim", true, false, ip);
},

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/

// To quit the experiment
closeTheGame:function()
{
voice.destroy();
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
//window.open(loc,"_self");                  // local test link
//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
