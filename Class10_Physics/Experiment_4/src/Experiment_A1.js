var experiment_a1 = function(game){

///////////////////////////////////////////// VARIABLES DECLARATION /////////////////////////////////////////////  
var background;
var popup;
var play;
var rightbutton_bg;
var leftbottom_bg;
var bullet;
var bullet2;
var bullet3;
/////////////////////////////////////////////////////////
var TestTube_1;
var TestTube_2;
var collider;
////////////////////////////////// TOP RIGHT BUTTON PANEL DECLARATION ///////////////////////////////

var volumeButton;
var muteButton;
var homeButton;
var closeButton;

///////////////////////////////////////// FOR FONT STYLE ////////////////////////////////////////////

var contentstyle;

//////////////////////////////////////// OTHER DECLARATIONS /////////////////////////////////////////

//audio
var voice;

//bools
var muted;

//ip address
var ip;

var delay;
var incr;
var currentobj;
var arrow_y;
var heatFlag;
var fNames;
var HCl_step;
var colorChangingAnimstart;
var TostartTesttubeB;
}

experiment_a1.prototype ={

init: function( ipadrs) {

     ip = ipadrs;
},

////////////////////////////////////// FOR INITIAL EXECUTION /////////////////////////////////////////

create:function()
 {

  if(this.game.sound.mute)
  {
    this.game.sound.mute = false;
  }

 muted = false;
 voice=this.game.add.audio("obj",1);
 
 this.arrangeScene();
 // voice=this.game.add.audio("fobj",1);

 //voice.play();
 bg= this.game.add.sprite(0, -50,'bg');
 bg.scale.setTo(.241,.253);

  var maskBg1 = this.game.add.graphics(0, 0);
  maskBg1.lineStyle(10,0xff704d,1);
  maskBg1.beginFill(0xffffcc);
  maskBg1.drawRect(20, 200, 300, 850);
  maskBg1.alpha=1;
  


 HCl_step=0;
 colorChangingAnimstart=false;
 delay=0;
 TostartTesttubeB=false;
////////////////////////////////////// TOP RIGHT BUTTON PANEL ///////////////////////////////////////

// Button panel backgroud-balck

  righttop_bg = this.game.add.sprite(1350, 140,"Button_Bg");//1480
  righttop_bg.scale.setTo(.5,.5);
  righttop_bg.angle=-90;
  
// Button panel -Quit button
  
  quitButton = this.game.add.sprite(1800,20, 'components', 'quit1_pressed.png');
  quitButton.scale.setTo(.7,.7);
  quitButton.inputEnabled = true;
  quitButton.input.useHandCursor = true;
  quitButton.events.onInputDown.add(this.closeTheGame, this);

// Button panel -Home button
  
  homeButton = this.game.add.sprite(1670,20, 'components', 'home_pressed.png');
  homeButton.scale.setTo(.7,.7);
  homeButton.inputEnabled = true;
  homeButton.input.useHandCursor = true;
  homeButton.events.onInputDown.add(this.gotoHome, this);

// Button panel -Mute button
  
  muteButton = this.game.add.sprite(1540,20, 'components', 'mute_pressed.png');
  muteButton.scale.setTo(.7,.7);
  muteButton.inputEnabled = true;
  muteButton.input.useHandCursor = true;
  muteButton.events.onInputDown.add(this.muteTheGame, this);

// Button panel - Volume button
  
  volumeButton = this.game.add.sprite(1540,20, 'components', 'volume.png');
  volumeButton.scale.setTo(.7,.7);
  volumeButton.inputEnabled = true;
  volumeButton.input.useHandCursor = true;
  volumeButton.events.onInputDown.add(this.volume, this);
  volumeButton.visible=false;

  resetButton = this.game.add.sprite(1410,20, 'components', 'reset_pressed.png');
  resetButton.scale.setTo(.7,.7);
  resetButton.inputEnabled = true;
  resetButton.input.useHandCursor = true;
  resetButton.events.onInputDown.add(this.resetTheGame, this);
///////////////////////////////////////////// BOTTOM LEFT BUTTON PANEL ///////////////////////////////////////////////

// Button panel backgroud-balck
  
  //leftbottom_bg = this.game.add.sprite(0, 855,'components','base_bottom.png');
  //leftbottom_bg.scale.setTo(2,2);

////////////////////////////////////////////////// FOR FULLSCREEN ////////////////////////////////////////////////////

  // if (!this.game.device.desktop)
  // {
  this.game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
  
  fullScreen1 = this.game.add.sprite(1810,960,'fullScreen1');
  fullScreen1.scale.setTo(2.5,2.5);
  fullScreen1.inputEnabled = true;
  fullScreen1.input.useHandCursor = true;
  fullScreen1.events.onInputUp.add(this.gofull,this);    
  //}

  ////////////////////////////////////////Font///////////////////////
  fontStyle={ font: "40px Segoe UI", fill: "#ffffff", align: "left" };
  headfontStyle={ font: "46px Segoe UI", fill: "#ffffff", align: "left" };
  labelfontStyle={ font: "52px Segoe UI", fill: "#000000", align: "left" };
  labelfontStyle1={ font: "30px Segoe UI", fill: "#000000", align: "center" };
  /////////////////////////////////////Experiment - decomposition_reaction/////////////////////////////////////
  interval=0;
  incr=0;
  currentobj=null;
  heatFlag=false;
  fNames=[];
  //this.addItems();

  

  /////////////////////////////////////////// PLAY BUTTON  ////////////////////////////////////////////
 
 play = this.game.add.sprite(1800,800,'components','play_pressed.png');
 play.scale.setTo(.7,.7);
 play.inputEnabled = true;
 //play.input.priorityID = 3;
 play.input.useHandCursor = true;
 play.events.onInputDown.add(this.toNextScene, this);
 play.visible=false;

  // Button panel group
  
  buttonGroup=this.game.add.group();
  buttonGroup.add(righttop_bg);
  buttonGroup.add(muteButton);
  buttonGroup.add(homeButton);
  buttonGroup.add(quitButton);
  buttonGroup.add(volumeButton);
  buttonGroup.add(resetButton);
  collider=null;

  this.arrangeScene();

  dialog_box=this.game.add.sprite(40,20, 'dialogue_box1');
      dialog_box.scale.setTo(.82,.8);
      dialog_text="Take battery set to the table.";
      //procedure_voice1.play();
      dialog=this.game.add.text(70,60,dialog_text,fontStyle);

 },


arrangeScene:function(){
  /////////////////////////////////////////////////////////////////////////////
  // circuit= this.game.add.sprite(320,0,'circuit');
  // circuit.scale.setTo(.4,.5);

//////////colliders//////////
 collider_battery = this.game.add.sprite(500,310,'collider');
 collider_battery.scale.setTo(8,3);
 collider_battery.alpha=1;
 //collider_battery.visible=false;
 this.game.physics.arcade.enable(collider_battery);//For colling purpose
 collider_battery.inputEnabled = true;
 collider_battery.enableBody =true;

 collider_rheostat = this.game.add.sprite(1410,230,'collider');
 collider_rheostat.scale.setTo(8,3);
 collider_rheostat.alpha=1;
 //collider_rheostat.visible=false;
 this.game.physics.arcade.enable(collider_rheostat);//For colling purpose
 collider_rheostat.inputEnabled = true;
 collider_rheostat.enableBody =true;

 collider_key = this.game.add.sprite(1560,600,'collider');
 collider_key.scale.setTo(5,3);
 collider_key.alpha=1;
 //collider_key.visible=false;
 this.game.physics.arcade.enable(collider_key);//For colling purpose
 collider_key.inputEnabled = true;
 collider_key.enableBody =true;

 collider_ammeter = this.game.add.sprite(955,520,'collider');
 collider_ammeter.scale.setTo(8,5);
 collider_ammeter.alpha=1;
 //collider_ammeter.visible=false;
 this.game.physics.arcade.enable(collider_ammeter);//For colling purpose
 collider_ammeter.inputEnabled = true;
 collider_ammeter.enableBody =true;

 collider_resistor = this.game.add.sprite(590,550,'collider');
 collider_resistor.scale.setTo(3,3);
 collider_resistor.alpha=1;
 //collider_resistor.visible=false;
 this.game.physics.arcade.enable(collider_resistor);//For colling purpose
 collider_resistor.inputEnabled = true;
 collider_resistor.enableBody =true;

 collider_voltmeter = this.game.add.sprite(460,800,'collider');
 collider_voltmeter.scale.setTo(8,5);
 collider_voltmeter.alpha=1;
 //collider_voltmeter.visible=false;
 this.game.physics.arcade.enable(collider_voltmeter);//For colling purpose
 collider_voltmeter.inputEnabled = true;
 collider_voltmeter.enableBody =true;

 
 ////////////////////////////////////////equipments//////////////////////////////////
 
 battery = this.game.add.sprite(450,324,'battery');
 battery.scale.setTo(.2,.25);
 battery.alpha=0;

 voltmeter = this.game.add.sprite(510,700,'voltmeter');
 voltmeter.scale.setTo(.2,.25);
 voltmeter.alpha=0;

 voltmeter_needle=this.game.add.sprite(663,867,'needle');
 voltmeter_needle.scale.setTo(.24,.27);
 voltmeter_needle.alpha=0;
 voltmeter_needle.anchor.setTo(.5,.9)
 voltmeter_needle.angle=0;

 ammeter = this.game.add.sprite(985,413,'ammeter');
 ammeter.scale.setTo(.22,.25);
 ammeter.alpha=0;

 ammeter_needle=this.game.add.sprite(1153,580,'needle');
 ammeter_needle.scale.setTo(.24,.27);
 ammeter_needle.alpha=0;
 ammeter_needle.anchor.setTo(.5,.9)
 ammeter_needle.angle=90;

 keySet = this.game.add.sprite(1559,600,'keySet');
 keySet.scale.setTo(.2,.25);
 keySet.alpha=0;

//  key = this.game.add.sprite(1670,575,'key');//key inserted diamension
//  key.scale.setTo(.2,.25);

 key = this.game.add.sprite(1670,750,'key');
 key.angle=45;
 key.scale.setTo(.2,.25);
 key.alpha=0;
 key.visible=false;
 

 rheostat = this.game.add.sprite(1350,225,'rheostat');
 rheostat.scale.setTo(.20,.25);
 rheostat.alpha=0;

 rheostat_head = this.game.add.sprite(1430,225,'rheostat_top');
 rheostat_head.scale.setTo(.20,.25);
 rheostat_head.alpha=0;

 resistor = this.game.add.sprite(574,595,'nichrome_wire');
 resistor.scale.setTo(.20,.25);
 resistor.alpha=0;

 wire1= this.game.add.sprite(368,367,'wire1');
 wire1.scale.setTo(.25,.25);
 wire1.visible=false;

 wire2 = this.game.add.sprite(445,640,'wire2');
 wire2.scale.setTo(.25,.25);
 wire2.visible=false;

 wire3 = this.game.add.sprite(776,640,'wire3');
 wire3.scale.setTo(.25,.25);
 wire3.visible=false;

 wire4 = this.game.add.sprite(735, 636,'wire4');
 wire4.scale.setTo(.21,.25);
 wire4.visible=false;

 wire5 = this.game.add.sprite(1280, 636,'wire5');
 wire5.scale.setTo(.185,.25);
 wire5.visible=false;

 wire6 = this.game.add.sprite(1360, 205,'wire6');
 wire6.scale.setTo(.20,.25);
 wire6.visible=false;

 wire7 = this.game.add.sprite(932,368,'wire7');
 wire7.scale.setTo(.20,.25);
 //wire7.alpha=0;
 wire7.visible=false;
//////////////////////////////equipments to drag//////////////////////////////

 temp_battery = this.game.add.sprite(65,220,'battery');
 temp_battery.scale.setTo(.08,.15);
 temp_battery.events.onDragStart.add(function() {this.onDragStart(temp_battery)}, this);
 temp_battery.events.onDragStop.add(function() {this.onDragStop(temp_battery)}, this);
 temp_battery.inputEnabled = true;
 temp_battery.input.useHandCursor = true;
 temp_battery.input.enableDrag(true);
 this.game.physics.arcade.enable(temp_battery);
 temp_battery.enableBody =true;
 //temp_battery.alpha=.5;

 temp_rheostat = this.game.add.sprite(40,360,'rheostat_full');
 temp_rheostat.scale.setTo(.1,.1);

//temp_rheostat.alpha=.5;

temp_keySet = this.game.add.sprite(90,470,'keySet_full');
temp_keySet.scale.setTo(.15,.15);
//temp_keySet.alpha=.5;

temp_ammeter = this.game.add.sprite(100,610,'ammeter_full');
 temp_ammeter.scale.setTo(.09,.09);
 //temp_ammeter.alpha=1;

 temp_resistor = this.game.add.sprite(110,800,'nichrome_wire');
 temp_resistor.scale.setTo(.13,.13);
//temp_resistor.alpha=.5;

 temp_voltmeter = this.game.add.sprite(100,900,'voltmeter_full');
 temp_voltmeter.scale.setTo(.09,.09);
 //temp_voltmeter.alpha=1;

 



//  key = this.game.add.sprite(1670,575,'key');//key inserted diamension
//  key.scale.setTo(.2,.25);










//  collider_B = this.game.add.sprite(515, 555,'collider');
//  collider_B.alpha=1;
//  this.game.physics.arcade.enable(collider_B);//For colling purpose
  
arrow=this.game.add.sprite(1500,450, 'arrow');
      arrow.angle=90;
      arrow_y=480;

      // circuit= this.game.add.sprite(320,0,'circuit');
      // circuit.scale.setTo(.4,.5);
},

onDragStart:function(obj)
  {
    //obj.angle=0;
    obj.body.enable =false;
    currentobj=obj;
    
    if(currentobj==(temp_battery))
    {
       currentobj.xp=65;
       currentobj.yp=220;
       battery.alpha=.5;
    }

    if(currentobj==(temp_rheostat))
    {
     currentobj.xp=40;
     currentobj.yp=360;
     rheostat.alpha=.5;

    }

    if(currentobj==(temp_keySet))
    {
     currentobj.xp=90;
     currentobj.yp=470;
    keySet.alpha=.5;

    }
 if(currentobj==(temp_ammeter))
    {
     currentobj.xp=100;
     currentobj.yp=610;
     ammeter.alpha=.5;

    }
    if(currentobj==(temp_resistor))
    {
     currentobj.xp=110;
     currentobj.yp=800;
     ammeter.alpha=.5;

    }



  },
  onDragStop:function(obj)
  {
    obj.body.enable =true;
    
  },
// For Full screen checking.
  
      gofull: function()
      {

      if (this.game.scale.isFullScreen)
        {
         this.game.scale.stopFullScreen();
        }
      else
        {
        this.game.scale.startFullScreen(false);
        }  
      },
      
  
  match_Obj:function(){
    
    currentobj.inputEnabled=false;
    currentobj.input.enableDrag(false);
    currentobj.body.enable=false;
    currentobj.visible=false;
    //currentobj.destroy();
    
    if(currentobj==temp_battery)
    {
        temp_battery.visible=false;
        //temp_battery.destroy();
        battery.alpha=1;
        battery.visible=true;
        dialog.text="Connect rheostat with the battery.";
        temp_rheostat.events.onDragStart.add(function() {this.onDragStart(temp_rheostat)}, this);
        temp_rheostat.events.onDragStop.add(function() {this.onDragStop(temp_rheostat)}, this);
        temp_rheostat.inputEnabled = true;
        temp_rheostat.input.useHandCursor = true;
        temp_rheostat.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_rheostat);
        temp_rheostat.enableBody =true;
        collider_battery.visible=false;
        collider_battery.enableBody=false;
        temp_battery.inputEnabled =false;
        temp_battery.input.useHandCursor = false;
        temp_battery.input.enableDrag(false);
        //this.game.physics.arcade.enable(temp_battery);
        temp_battery.enableBody =false;
      }

      if(currentobj==temp_rheostat)
      {
        temp_rheostat.visible=false;
        //temp_battery.destroy();
        rheostat.alpha=1;
        rheostat.visible=true;
        dialog.text="Connect the key to rheostat as series";
        wire7.visible=true;
        temp_keySet.events.onDragStart.add(function() {this.onDragStart(temp_keySet)}, this);
        temp_keySet.events.onDragStop.add(function() {this.onDragStop(temp_keySet)}, this);
        temp_keySet.inputEnabled = true;
        temp_keySet.input.useHandCursor = true;
        temp_keySet.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_keySet);
        temp_keySet.enableBody =true;
        collider_rheostat.visible=false;
        collider_rheostat.enableBody=false;
      }

      if(currentobj==temp_keySet)
      {
        temp_keySet.visible=false;
        //temp_battery.destroy();
        keySet.alpha=1;
        key.visible=true;
        dialog.text="Connect the ammeter to the circuit in series";
        wire6.visible=true;
        temp_ammeter.events.onDragStart.add(function() {this.onDragStart(temp_ammeter)}, this);
        temp_ammeter.events.onDragStop.add(function() {this.onDragStop(temp_ammeter)}, this);
        temp_ammeter.inputEnabled = true;
        temp_ammeter.input.useHandCursor = true;
        temp_ammeter.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_ammeter);
        temp_ammeter.enableBody =true;
        collider_key.visible=false;
        collider_key.enableBody=false;
      }
            if(currentobj==temp_ammeter)
      {
        temp_ammeter.visible=false;
        //temp_battery.destroy();
        ammeter.alpha=1;
        ammeter.visible=true;
        dialog.text="Connect the resistor to the circuit in series with ammeter";
        wire5.visible=true;
        temp_resistor.events.onDragStart.add(function() {this.onDragStart(temp_resistor)}, this);
        temp_ammeter.events.onDragStop.add(function() {this.onDragStop(temp_resistor)}, this);
        temp_resistor.inputEnabled = true;
        temp_resistor.input.useHandCursor = true;
        temp_resistor.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_resistor);
        temp_resistor.enableBody =true;
        collider_ammeter.visible=false;
        collider_ammeter.enableBody=false;
      }

     if(currentobj==temp_resistor)
      {
        temp_resistor.visible=false;
        //temp_battery.destroy();
        resistor.alpha=1;
        ammeter.visible=true;
        dialog.text="Connect the voltmeter parallel";
        wire5.visible=true;
        temp_resistor.events.onDragStart.add(function() {this.onDragStart(temp_resistor)}, this);
        temp_ammeter.events.onDragStop.add(function() {this.onDragStop(temp_resistor)}, this);
        temp_resistor.inputEnabled = true;
        temp_resistor.input.useHandCursor = true;
        temp_resistor.input.enableDrag(true);
        this.game.physics.arcade.enable(temp_resistor);
        temp_resistor.enableBody =true;
        collider_ammeter.visible=false;
        collider_ammeter.enableBody=false;
      }
      
  },
  
  update: function()
  {
    DeltaTime=this.game.time.elapsed/1000;
    this.detectCollision();
    if(arrow.visible){
      arrow.y+=(3+DeltaTime);
      if(arrow.y>=(arrow_y+50))
      {
        arrow.y=arrow_y;
      }
    }
    
  },
 
  detectCollision:function(){
   

     if(collider_battery.enableBody && currentobj!=null){
      
         this.game.physics.arcade.overlap(currentobj, collider_battery,function() {this.match_Obj()},null,this);
        // collider=collider_battery;
     
     } 

     if(collider_rheostat.enableBody && currentobj!=null){
      
      this.game.physics.arcade.overlap(currentobj, collider_rheostat,function() {this.match_Obj()},null,this);
      //collider=collider_battery;
  
    }

  if(collider_key.enableBody && currentobj!=null){
      
    this.game.physics.arcade.overlap(currentobj, collider_key,function() {this.match_Obj()},null,this);
    //collider=collider_battery;

    }
if(collider_ammeter.enableBody && currentobj!=null){
      
  this.game.physics.arcade.overlap(currentobj, collider_ammeter,function() {this.match_Obj()},null,this);
  //collider=collider_battery;

    }
    if(collider_resistor.enableBody && currentobj!=null){
      
      this.game.physics.arcade.overlap(currentobj, collider_resistor,function() {this.match_Obj()},null,this);
      //collider=collider_battery;
    
        }
    // if(collider_B.enableBody && currentobj!=null){
    //     this.game.physics.arcade.overlap(currentobj, collider_B,function() {this.match_Obj()},null,this);
    //     collider=collider_B;
    // }
     if(currentobj!=null && currentobj.body.enable){
      currentobj.reset(currentobj.xp,currentobj.yp);//
       
       if(currentobj==(temp_battery))
       {
        temp_battery.visible=true;

        battery.alpha=0;
        //battery.visible=false;
        
       }
       if(currentobj==(temp_rheostat))
      {
       temp_rheostat.visible=true;
       rheostat.alpha=0;
      // rheostat.visible=false;

    }
    if(currentobj==(temp_keySet))
    {
     temp_keySet.visible=true;
     keySet.alpha=0;
     //keySet.visible=false;

  }   
  if(currentobj==(temp_ammeter))
    {
     temp_ammeter.visible=true;
     ammeter.alpha=0;
     //keySet.visible=false;

  }
       if(currentobj==(temp_resistor))
    {
     temp_resistor.visible=true;
     resistor.alpha=0;
     //keySet.visible=false;

  } 
    //     arrow.x=1500;
    //     arrow.y=450;
    //     arrow_y=450;
    //     arrow.visible=true;
        
    //   }

         currentobj=null;
      }
    
  },
//For to next scene
 
      toNextScene:function()
      {
      voice.destroy();
      
      this.state.start("Experiment_A2", true, false, ip);
      },

// For mute the audio
      muteTheGame:function()
      {
      muted = true;
      //   voice.stop();
      this.game.sound.mute = true;
      volumeButton.visible = true;
      muteButton.visible = false;
      },

// For unmute audio

      volume:function()
      {
       this.game.sound.mute = false;
       volumeButton.visible = false;
       muteButton.visible = true;
      },

// For Goto title screen

      gotoHome:function()
      {
        voice.destroy();
        
        this.state.start("Aim", true, false, ip);
      },

////////////////////////////////////////////////////////// FOR TRACKING USER USAGE DATA /////////////////////////////////////////////////////////      

  /*postData:function()
  {
   // Sending and receiving data in JSON format using POST method
   //
   console.log('post');
   var xhr = new XMLHttpRequest();
   var url = "https://scienceapp.in/api/public/user-logs";
   xhr.open("POST", url, true);
   xhr.setRequestHeader("Content-Type", "application/json");
   current_timestamp=Math.floor(new Date().getTime()/1000);
   var data = JSON.stringify({"sim_id": localStorage.getItem('SIM_ID'), "child_id":localStorage.getItem('CHILD_ID'),"end_time":current_timestamp});
   xhr.send(data);
   xhr.onreadystatechange = function () 
          {
          console.log(xhr.readyState);
          console.log(xhr.status);
          if ((xhr.readyState === 4) && ((xhr.status === 200)||(xhr.status === 201)))
            {
            var json = JSON.parse(xhr.responseText);
            console.log(json);
            window.open(loc,"_self");
            }
          else 
              {
              console.log('fail');
              window.open(loc,"_self");
              }
          };
 },*/
//To reset the game
resetTheGame:function(){
  voice.destroy();
  
    this.state.start("Experiment_A1",true,false);
 },
 // To quit the experiment

closeTheGame:function()
{
  voice.destroy();
  
//this.postData();
//local cloud instance test
//var loc="http://swadhyaya.tk/theme/essential/layout/creatnlrn/third_phy.php?id=4";
//window.open(loc,"_self");                  // local test link

//    window.open("http://"+ip+"/swadhyaya/theme/essential/layout/creatnlrn/third_chem.php?id=70","_self");    // cloud test link
 },


}
